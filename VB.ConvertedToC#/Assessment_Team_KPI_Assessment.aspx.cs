using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Xml.Linq;
using System.Diagnostics;
using System.Collections.Specialized;
using System.Configuration;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Caching;
using System.Web.SessionState;
using System.Web.Security;
using System.Web.Profile;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
namespace VB
{

	public partial class Assessment_Team_KPI_Assessment : System.Web.UI.Page
	{


		HRBL BL = new HRBL();
		GenericLib GL = new GenericLib();

		textControlLib TC = new textControlLib();
		public int R_Year {
			get {
				try {
					return GL.CINT( GL.SplitString(ddlRound.Items[ddlRound.SelectedIndex].Value, "-")[0]);
				} catch (Exception ex) {
					return 0;
				}
			}
		}

		public int R_Round {
			get {
				try {
					return GL.CINT(GL.SplitString(ddlRound.Items[ddlRound.SelectedIndex].Value, "-")[1]);
				} catch (Exception ex) {
					return 0;
				}
			}
		}

		public string PSNL_NO {
			get { return lblPSNName.Attributes["PSNL_NO"]; }
			set { lblPSNName.Attributes["PSNL_NO"] = value; }
		}

		public string ASSESSOR_BY {
			get {
				try {
					string[] tmp = Strings.Split(ddlASSName.Items[ddlASSName.SelectedIndex].Value, ":::");
					return tmp[0];
				} catch (Exception ex) {
					return "";
				}
			}
		}

		public string ASSESSOR_NAME {
			get {
				try {
					return ddlASSName.Items[ddlASSName.SelectedIndex].Text;
				} catch (Exception ex) {
					return "";
				}
			}
		}

		public string ASSESSOR_POS {
			get {
				try {
					string[] tmp = Strings.Split(ddlASSName.Items[ddlASSName.SelectedIndex].Value, ":::");
					return tmp[1];
				} catch (Exception ex) {
					return "";
				}
			}
		}

		public string ASSESSOR_DEPT {
			get {
				try {
					string[] tmp = Strings.Split(ddlASSName.Items[ddlASSName.SelectedIndex].Value, ":::");
					return tmp[2];
				} catch (Exception ex) {
					return "";
				}
			}
		}

		public int KPI_Status {
			get { return GL.CINT (lblKPIStatus.Attributes["KPI_Status"]); }
			set { lblKPIStatus.Attributes["KPI_Status"] = value.ToString (); }
		}

        public string DEPT_CODE
        {
            get
            {
                try
                {
                    return lblPSNName.Attributes["DEPT_CODE"];
                }
                catch (Exception ex)
                {
                    return "";
                }
            }
            set { lblPSNName.Attributes["DEPT_CODE"] = value.ToString(); }
        }

		protected void Page_Load(object sender, System.EventArgs e)
		{
			if ((Session["USER_PSNL_NO"] == null)) {
				ScriptManager.RegisterStartupScript(this.Page, typeof(string), "Alert", "showAlert('กรุณาลงชื่อเข้าใช้ระบบ');", true);
				Response.Redirect("Login.aspx");
				return;
			}

			if (!IsPostBack) {
                // Keep Start Time
                DateTime StartTime = DateTime.Now;

				BL.BindDDlYearRound(ddlRound);
     		 
				BindDDlAssessmentStatus();
                chk_CreateHR_Actual_Assessor();

                Update_KPI_Status_All_Team(); //ถ้ายังช้า ลบบรรทัดนี้ออก

				BindPersonalList();
                //----------------- ทำสองที่ Page Load กับ Update Status ---------------
                //BL.Update_KPI_Status_To_Assessment_Period(PSNL_NO, R_Year, R_Round);


				pnlList.Visible = true;
				pnlEdit.Visible = false;

                //Report Time To Entry This Page
                DateTime EndTime = DateTime.Now;
                lblReportTime.Text = "เริ่มเข้าสู่หน้านี้ " + StartTime.ToString("HH:mm:ss.ff") + " ถึง " + EndTime.ToString("HH:mm:ss.ff") + " ใช้เวลา " + Math.Round( GL.DiffTimeDecimalSeconds(StartTime,EndTime),2) + " วินาที";
			}
		}

        private void Update_KPI_Status_All_Team()
        {
            DataTable dt = GetData ();
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                string _psnlNo = dt.Rows[i]["PSNL_NO"].ToString();
                BL.Update_KPI_Status_To_Assessment_Period(_psnlNo, R_Year, R_Round);
            }
        }

		private void BindDDlAssessmentStatus()
		{
			ddlStatus.Items.Clear();
			ddlStatus.Items.Add(new ListItem("ทั้งหมด", "0"));
			ddlStatus.Items.Add(new ListItem("ยังไม่ส่งผลการประเมิน", "1"));
			ddlStatus.Items.Add(new ListItem("อนุมัติผลการประเมินแล้ว", "2"));
		}

        private void chk_CreateHR_Actual_Assessor()
        {

            Boolean IsCompleted = BL.Is_Round_Completed(R_Year, R_Round);
            if (!IsCompleted)
            {
                SqlDataAdapter DA;
                DataTable PSN = new DataTable();
                string SQL = "";
                SQL += " select PSNL_NO from tb_HR_Actual_Assessor where MGR_PSNL_NO ='" + Session["USER_PSNL_NO"].ToString().Replace("'", "''") + "' AND R_Year='" + R_Year + "' AND R_Round ='" + R_Round + "' \n";
                SQL += " AND  PSNL_NO NOT IN (select PSN_PSNL_NO from vw_HR_ASSESSOR_PSN where MGR_PSNL_NO ='" + Session["USER_PSNL_NO"].ToString().Replace("'", "''") + "'  AND R_Year='" + R_Year + "'  AND R_Round ='" + R_Round + "') \n";
                DA = new SqlDataAdapter(SQL, BL.ConnectionString());
                DA.Fill(PSN);
                if (PSN.Rows.Count > 0)
                {
                    for (int i = 0; i < PSN.Rows.Count; i++)
                    {
                        BL.CreateHR_Actual_Assessor(R_Year, R_Round, PSN.Rows[i]["PSNL_NO"].ToString(), Session["USER_PSNL_NO"].ToString().Replace("'", "''"));
                    }
                }
            }

        }

		protected void Search(object sender, System.EventArgs e)
		{
            BindPersonalList();
            Update_KPI_Status_All_Team();
            BindPersonalList();

			if (pnlEdit.Visible) {
				BindPersonal();
				BindAssessor();
				BindMasterKPI();

				SetPrintButton();
			}
		}

		private void SetPrintButton()
		{
			btnPDF.HRef = "Print/RPT_K_PNSAss.aspx?MODE=PDF&R_Year=" + R_Year + "&R_Round=" + R_Round + "&PSNL_No=" + PSNL_NO + "&Status=" + GL.CINT(KPI_Status);
			btnExcel.HRef = "Print/RPT_K_PNSAss.aspx?MODE=EXCEL&R_Year=" + R_Year + "&R_Round=" + R_Round + "&PSNL_No=" + PSNL_NO + "&Status=" + GL.CINT(KPI_Status);
		}

        public DataTable GetData()
        {

            string SQL = "";
            SQL += " DECLARE @R_Year As Int=" + R_Year + "\n";
            SQL += " DECLARE @R_Round As Int=" + R_Round + "\n";
            SQL += " DECLARE @ASSESSOR_CODE AS nvarchar(50)='" + Session["USER_PSNL_NO"].ToString().Replace("'", "''") + "'\n";
            SQL += " \n\n";
            SQL += " SELECT DISTINCT KPI.* FROM\n";
            SQL += " (";
            SQL += " SELECT DISTINCT PSN_DEPT_CODE,PSN.PSN_DEPT_Name DEPT_Name,PSN.PSN_PSNL_NO PSNL_NO,PSN.PSN_PSNL_Fullname PSNL_Fullname,PSN.PSN_MGR_NAME POS_Name,PSN.PSN_PNPS_CLASS PSNL_CLASS,Result.POS_No,\n";
            SQL += "  ISNULL(Header.KPI_Status,0) KPI_Status,dbo.udf_GetAssessmentStatusName(Header.KPI_Status) KPI_Status_Name,Result.RESULT,Result .ASS_Status\n";
            SQL += "  FROM vw_HR_ASSESSOR_PSN PSN\n";
            SQL += "  INNER JOIN vw_RPT_KPI_Result Result ON PSN.PSN_PSNL_NO=Result.PSNL_NO \n";
            SQL += "   \tAND Result.R_Year=PSN.R_Year AND Result.R_Round=PSN.R_Round\n";
            SQL += " \tAND PSN.MGR_PSNL_NO=@ASSESSOR_CODE\n";
            SQL += "  INNER JOIN tb_HR_KPI_Header Header ON Header.R_Year=Result.R_Year AND Header.R_Round=Result.R_Round AND Header.PSNL_NO=Result.PSNL_NO \n";
            SQL += "  WHERE PSN.R_Year=@R_Year AND PSN.R_Round=@R_Round\n";
            SQL += " \n";

            SQL += " ) KPI\n";
            SQL += "  LEFT JOIN vw_PN_PSNL_ALL PSN_ALL ON KPI.PSNL_NO=PSN_ALL.PSNL_NO \n";
            SQL += " WHERE 1=1 \n";
            SQL += "  AND (ISNULL(PSN_ALL.PNPS_RESIGN_DATE,PSN_ALL.PNPS_RETIRE_DATE) > (SELECT MAX(R_End) FROM tb_HR_Round WHERE R_Year=@R_Year AND R_Round=@R_Round)) \n";
            if (!string.IsNullOrEmpty(txtName.Text))
            {
                SQL += " AND (KPI.PSNL_NO LIKE '%" + txtName.Text.Replace("'", "''") + "%' OR KPI.PSNL_Fullname LIKE '%" + txtName.Text.Replace("'", "''") + "%')";
            }
            switch (ddlStatus.SelectedIndex)
            {
                case 1:
                    SQL += " AND (ASS_Status IS NULL OR ASS_Status<5)";
                    break;
                case 2:
                    SQL += " AND ASS_Status=5";
                    break;
            }

            SQL += " ORDER BY PSN_DEPT_CODE,PSNL_CLASS DESC,KPI.POS_No\n";



            SqlDataAdapter DA = new SqlDataAdapter(SQL, BL.ConnectionString());
            DataTable DT = new DataTable();
            DA.SelectCommand.CommandTimeout = 200;

            if (ddlRound.SelectedIndex > 0)
            {
                DA.Fill(DT);
            }

            Session["Assessment_Team_KPI_Assessment"] = DT;

            return DT;
       }
         
		private void BindPersonalList()
		{
            DataTable DT = new DataTable();
            DT = GetData();
			Pager.SesssionSourceName = "Assessment_Team_KPI_Assessment";
			Pager.RenderLayout();

			if (DT.Rows.Count == 0) {
				lblCountPSN.Text = "ไม่พบรายการดังกล่าว";
			} else {
				lblCountPSN.Text = "พบ " + GL.StringFormatNumber(DT.Rows.Count, 0) + " รายการ";
			}
		}

		protected void Pager_PageChanging(PageNavigation Sender)
		{
			Pager.TheRepeater = rptKPI;
		}

		protected void rptKPI_ItemCommand(object source, System.Web.UI.WebControls.RepeaterCommandEventArgs e)
		{
			switch (e.CommandName) {
				case "Edit":
					Button btnPSNEdit =(Button) e.Item.FindControl("btnPSNEdit");

					pnlList.Visible = false;
					pnlEdit.Visible = true;
					//-------------- ดึงข้อมูลบุคคล ------------
					PSNL_NO = btnPSNEdit.CommandArgument;
					BindPersonal();
					BindAssessor();
					BindMasterKPI();
					SetPrintButton();
					break;
			}
		}

		//------------ For Grouping -----------
		string LastOrgranizeName = "";
		protected void rptKPI_ItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
		{
			if (e.Item.ItemType != ListItemType.AlternatingItem & e.Item.ItemType != ListItemType.Item)
				return;

			Label lnkPSNOrganize =(Label) e.Item.FindControl("lnkPSNOrganize");
			Label lblPSNNo = (Label)e.Item.FindControl("lblPSNNo");
			Label lblPSNName = (Label)e.Item.FindControl("lblPSNName");
			 Label lblPSNPos = (Label)e.Item.FindControl("lblPSNPos");
			 Label lblPSNClass = (Label)e.Item.FindControl("lblPSNClass");
			Label lblPSNKPIStatus =(Label) e.Item.FindControl("lblPSNKPIStatus");
			Label lblResult =(Label) e.Item.FindControl("lblResult");
			Label lblPercent =(Label) e.Item.FindControl("lblPercent");
			Button btnPSNEdit =(Button) e.Item.FindControl("btnPSNEdit");

			//lnkPSNSector.Attributes("SECTOR_CODE") = drv["SECTOR_CODE"]
            DataRowView drv = (DataRowView)e.Item.DataItem;
			if (Convert.ToString(drv["DEPT_NAME"]) != LastOrgranizeName) {
				lnkPSNOrganize.Text = drv["DEPT_NAME"].ToString ();
				LastOrgranizeName = drv["DEPT_NAME"].ToString ();
			}

			//lnkPSNOrganize.Attributes("DEPT_CODE") = drv["DEPT_CODE"]
			lblPSNNo.Text = drv["PSNL_NO"].ToString ();
			lblPSNName.Text = drv["PSNL_Fullname"].ToString ();
			lblPSNPos.Text = drv["POS_Name"].ToString ();
			lblPSNKPIStatus.Text = drv["KPI_Status_Name"].ToString ();
			if (Information.IsNumeric(GL.CINT (drv["PSNL_CLASS"]))) {
				lblPSNClass.Text = GL.CINT (drv["PSNL_CLASS"]).ToString ();
			}

			
            //------ปรับสีสถานะ-----------
            //----<0
            if (GL.IsEqualNull(drv["KPI_Status"]) || GL.CINT(drv["KPI_Status"]) == HRBL.AssessmentStatus.Creating)
            {
                lblPSNKPIStatus.ForeColor = System.Drawing.Color.Red;
                //----1
            }
            else if (GL.IsEqualNull(drv["KPI_Status"]) || GL.CINT(drv["KPI_Status"]) == HRBL.AssessmentStatus.WaitCreatingApproved)
            {
                lblPSNKPIStatus.ForeColor = System.Drawing.Color.DarkRed;
                //----2
            }
            else if (GL.IsEqualNull(drv["KPI_Status"]) || GL.CINT(drv["KPI_Status"]) == HRBL.AssessmentStatus.CreatedApproved)
            {
                lblPSNKPIStatus.ForeColor = System.Drawing.Color.Orange;
                //----3
            }
            else if (GL.IsEqualNull(drv["KPI_Status"]) || GL.CINT(drv["KPI_Status"]) == HRBL.AssessmentStatus.WaitAssessment)
            {
                lblPSNKPIStatus.ForeColor = System.Drawing.Color.DarkBlue;
                //----4
            }
            else if (GL.IsEqualNull(drv["KPI_Status"]) || GL.CINT(drv["KPI_Status"]) == HRBL.AssessmentStatus.WaitConfirmAssessment)
            {
                lblPSNKPIStatus.ForeColor = System.Drawing.Color.BlueViolet;
                //----5
            }
            else if (GL.IsEqualNull(drv["KPI_Status"]) || GL.CINT(drv["KPI_Status"]) == HRBL.AssessmentStatus.AssessmentCompleted)
            {
                lblPSNKPIStatus.ForeColor = System.Drawing.Color.Green;
            }


			if (!GL.IsEqualNull(drv["RESULT"])) {
				lblResult.Text = GL.StringFormatNumber(drv["RESULT"]);
				lblPercent.Text = GL.StringFormatNumber(GL.CDBL ( drv["RESULT"]) / 5) + " %";

				//--------------------ปรับสีคะแนนที่หน้าจอประเมินของผู้ประเมิน ให้แสดงเหมือนกับสีของสถานะ --------
				lblResult.ForeColor = lblPSNKPIStatus.ForeColor;
				lblPercent.ForeColor = lblPSNKPIStatus.ForeColor;
			} else {
				lblResult.Text = "-";
				lblPercent.Text = "-";
			}

			btnPSNEdit.CommandArgument = drv["PSNL_NO"].ToString ();

		}

		private void BindPersonal()
		{
			//----------Personal Info ----------
			HRBL.PersonalInfo PSNInfo = BL.GetAssessmentPersonalInfo(R_Year, R_Round, PSNL_NO, KPI_Status);
			lblPSNName.Text = PSNInfo.PSNL_Fullname;
			lblPSNDept.Text = PSNInfo.DEPT_NAME;
			if (!string.IsNullOrEmpty(PSNInfo.MGR_NAME)) {
				lblPSNPos.Text = PSNInfo.MGR_NAME;
			} else {
				lblPSNPos.Text = PSNInfo.FN_NAME;
			}
            DEPT_CODE = PSNInfo.DEPT_CODE;
		}

		private void BindAssessor()
		{
			BL.BindDDLAssessor(ddlASSName, R_Year, R_Round, PSNL_NO);
			ddlASSName_SelectedIndexChanged(null, null);
		}

		protected void ddlASSName_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			lblASSPos.Text = ASSESSOR_POS;
			lblASSDept.Text = ASSESSOR_DEPT;
			SaveHeader();
		}

		private void BindMasterKPI()
		{
			//---------------- Set Status -------------------
			KPI_Status = BL.GetKPIStatus(R_Year, R_Round, PSNL_NO);
			lblKPIStatus.Text = BL.GetAssessmentStatusName(KPI_Status);

            Boolean IsInPeriod = BL.IsTimeInAssessmentPeriod(DateTime.Now, R_Year, R_Round, HRBL.AssessmentType.KPI  , KPI_Status ,DEPT_CODE.ToString ());

            Boolean IsRoundCompleted = BL.Is_Round_Completed(R_Year, R_Round);

            Boolean IsEditable = (KPI_Status > HRBL.AssessmentStatus.CreatedApproved & KPI_Status < HRBL.AssessmentStatus.AssessmentCompleted) & IsInPeriod & !IsRoundCompleted;
            Boolean chkIsEditable = IsEditable;

            pnlEdit_Ass.Enabled = chkIsEditable;
            btnPreSend.Visible = chkIsEditable;
            ddlASSName.Enabled = chkIsEditable;


			//--------------- Bind Detail----------------------
			HRBL.DataManager DM = BL.GetKPIDetail(R_Year, R_Round, PSNL_NO);
			DataTable DT = DM.Table;
			SqlDataAdapter DA = DM.Adaptor;
			rptAss.DataSource = DM.Table;
			rptAss.DataBind();
		}

		protected void rptAss_ItemCommand(object source, System.Web.UI.WebControls.RepeaterCommandEventArgs e)
		{
             HtmlTableCell ass_no =(HtmlTableCell)  e.Item.FindControl("ass_no");
            int KPI_No = Convert.ToInt32(Conversion.Val(ass_no.InnerHtml));

            DataTable DT;
            SqlCommandBuilder cmd;
            //SqlDataAdapter DA;
            HRBL.DataManager DM;
            
			switch (e.CommandName) {
				case "Select":
					
					Button btn =(Button) e.CommandSource;
					int SelectedChoice = Convert.ToInt32(btn.ID.Replace("btn", ""));
                    //int KPI_No = Convert.ToInt32(Conversion.Val(ass_no.InnerHtml));
					 DM = BL.GetKPIDetail(R_Year, R_Round, PSNL_NO);
					 DT = DM.Table;
					DT.DefaultView.RowFilter = "KPI_No=" + KPI_No;
					if (DT.DefaultView.Count > 0) {
						DataRow DR = DT.DefaultView[0].Row;
						DR["KPI_Answer_MGR"] = SelectedChoice;
					}

					 cmd = new SqlCommandBuilder(DM.Adaptor);
					DM.Adaptor.Update(DT);
					DT.AcceptChanges();
					DT.DefaultView.RowFilter = "";
					rptAss.DataSource = DT;
					rptAss.DataBind();

					break;
				case "Remark":

					
					TextBox txtMGR =(TextBox) e.Item.FindControl("txtMGR");
                    //int KPI_No = Convert.ToInt32(Conversion.Val(ass_no.InnerHtml));
                    if (txtMGR.Text != "")
                    {
                        if (txtMGR.Text.Length > 3990)
                        {
                            txtMGR.Text = txtMGR.Text.Substring(0, 3990);
                        }

                    }
					DM = BL.GetKPIDetail(R_Year, R_Round, PSNL_NO);
					DT = DM.Table;
					DT.DefaultView.RowFilter = "KPI_No=" + KPI_No;
					if (DT.DefaultView.Count > 0) {
						DataRow DR = DT.DefaultView[0].Row;
						DR["KPI_Remark_MGR"] = txtMGR.Text;
					}
					 cmd = new SqlCommandBuilder(DM.Adaptor);
					DM.Adaptor.Update(DT);
					DT.AcceptChanges();
					DT.DefaultView.RowFilter = "";
					rptAss.DataSource = DT;
					rptAss.DataBind();

					break;

			}
		}

		protected void rptAss_ItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
		{
			switch (e.Item.ItemType) {

				case ListItemType.AlternatingItem:
				case ListItemType.Item:

					HtmlTableCell ass_no =(HtmlTableCell)  e.Item.FindControl("ass_no");
					HtmlTableCell job =(HtmlTableCell) e.Item.FindControl("job");
					HtmlTableCell target =(HtmlTableCell) e.Item.FindControl("target");
					HtmlTableCell weight =(HtmlTableCell) e.Item.FindControl("weight");
					HtmlTableCell cel_officer =(HtmlTableCell) e.Item.FindControl("cel_officer");
					HtmlTableCell cel_header =(HtmlTableCell) e.Item.FindControl("cel_header");
					HtmlTableCell mark =(HtmlTableCell) e.Item.FindControl("mark");

					Button btn_Remark =(Button) e.Item.FindControl("btn_Remark");
					TextBox txtPSN =(TextBox) e.Item.FindControl("txtPSN");
					TextBox txtMGR =(TextBox) e.Item.FindControl("txtMGR");

                    DataRowView drv = (DataRowView)e.Item.DataItem;
					ass_no.InnerHtml = drv["KPI_No"].ToString ();
					//.ToString().PadLeft(2, GL.chr0)
					job.InnerHtml = drv["KPI_Job"].ToString().Replace("\n", "<br>");
					target.InnerHtml = drv["KPI_Target_Text"].ToString().Replace("\n", "<br>");
					weight.InnerHtml = GL.CDBL ( drv["KPI_Weight"]).ToString ();

					txtPSN.Text = drv["KPI_Remark_PSN"].ToString();
					txtMGR.Text = drv["KPI_Remark_MGR"].ToString();

					for (int i = 1; i <= 5; i++) {
						HtmlTableCell choice =(HtmlTableCell) e.Item.FindControl("choice" + i);
						HtmlTableCell selHeaderColor =(HtmlTableCell) e.Item.FindControl("selHeaderColor" + i);
						Button btn =(Button) e.Item.FindControl("btn" + i);
						choice.InnerHtml = drv["KPI_Choice_" + i.ToString()].ToString().Replace("\n", "<br>");
					}



					int Officer = 0;
					int Manager = 0;
					 if (!GL.IsEqualNull(drv["KPI_Answer_PSN"]))
						Officer =GL.CINT ( drv["KPI_Answer_PSN"]);
					 if (!GL.IsEqualNull(drv["KPI_Answer_MGR"]))
						Manager =  GL.CINT (drv["KPI_Answer_MGR"]);

					//------------- Set Officer Selected Choice-----------
					if (Officer > 0) {
						cel_officer.Attributes["class"] = "AssLevel" + Officer;
						for (int j = 1; j <= Officer; j++) {
							HtmlTableCell _selOfficialColor =(HtmlTableCell) e.Item.FindControl("selOfficialColor" + j);
							_selOfficialColor.Attributes["class"] = "AssLevel" + drv["KPI_Answer_PSN"];
						}
						//------------- Collect value-------------
						cel_officer.Attributes["KPI_Answer_PSN"] = GL.CINT ( Officer).ToString ();
					}

					//------------ Set Manager Selected Choice -----------
					//---------- หัวหน้าประเมินแล้ว ---------
					if (Manager > 0) {
						cel_header.Attributes["class"] = "AssLevel" + Manager;
						for (int j = 1; j <= Manager; j++) {
							HtmlTableCell _selHeaderColor =(HtmlTableCell) e.Item.FindControl("selHeaderColor" + j);
							_selHeaderColor.Attributes["class"] = "AssLevel" + Manager;
						}
						weight.Attributes["class"] = "AssLevel" + Manager;
						mark.Attributes["class"] = "AssLevel" + Manager;
						//------------- Collect value-------------
						cel_header.Attributes["KPI_Answer_MGR"] =  GL.CINT (Manager).ToString ();
						//------------- Set Delelet Action --------
					}


					//--------------- Set Text ------------
					if (Manager > 0) {
						mark.InnerHtml = Manager.ToString ();
						job.Attributes["class"] = "TextLevel" + Manager;
						target.Attributes["class"] = "TextLevel" + Manager;
						HtmlTableCell _choice =(HtmlTableCell) e.Item.FindControl("choice" + Manager);
						_choice.Attributes["class"] = "TextLevel" + Manager;
					} else {
						mark.InnerHtml = "-";
					}

					//-------------- Set Enabling --------------
					if (KPI_Status >= HRBL.AssessmentStatus.WaitAssessment & KPI_Status < HRBL.AssessmentStatus.AssessmentCompleted) {
						txtMGR.Attributes["onchange"] = "document.getElementById('" + btn_Remark.ClientID + "').click();";
						for (int i = 1; i <= 5; i++) {
							HtmlTableCell choice =(HtmlTableCell) e.Item.FindControl("choice" + i);
							HtmlTableCell selHeaderColor =(HtmlTableCell) e.Item.FindControl("selHeaderColor" + i);
							Button btn =(Button) e.Item.FindControl("btn" + i);
							selHeaderColor.Attributes["onclick"] = "document.getElementById('" + btn.ClientID + "').click();";
							choice.Attributes["onclick"] = "document.getElementById('" + btn.ClientID + "').click();";
						}
					} else {
						for (int i = 1; i <= 5; i++) {
							HtmlTableCell selOfficialColor =(HtmlTableCell) e.Item.FindControl("selOfficialColor" + i);
							HtmlTableCell choice =(HtmlTableCell) e.Item.FindControl("choice" + i);
							HtmlTableCell selHeaderColor =(HtmlTableCell) e.Item.FindControl("selHeaderColor" + i);
							choice.Attributes["title"] = "";
							choice.Style["cursor"] = "default";
							selHeaderColor.Attributes["title"] = "";
							selHeaderColor.Style["cursor"] = "default";
							selOfficialColor.Attributes["title"] = "";
							selOfficialColor.Style["cursor"] = "default";
						}
						txtMGR.ReadOnly = true;
					}

					break;

				case ListItemType.Footer:
					//----------------Report Summary ------------

					//----------- Control Difinition ------------
					HtmlTableCell cell_sum_mark =(HtmlTableCell) e.Item.FindControl("cell_sum_mark");
					HtmlTableCell cell_sum_weight =(HtmlTableCell) e.Item.FindControl("cell_sum_weight");
					HtmlTableCell cell_total =(HtmlTableCell) e.Item.FindControl("cell_total");
					Label lblDone =(Label) e.Item.FindControl("lblDone");
					Label lblTotalItem =(Label) e.Item.FindControl("lblTotalItem");
					HtmlTableCell cell_sum_raw =(HtmlTableCell) e.Item.FindControl("cell_sum_raw");

					//------------ Report -------------
					DataTable DT =(DataTable) rptAss.DataSource;
					DT.Columns.Add("Final_KPI", typeof(Int32), "IIF(KPI_Answer_MGR>0,KPI_Answer_MGR,0)");


					int TotalItem = DT.Rows.Count;
					int AssCount =GL.CINT ( DT.Compute("COUNT(KPI_No)", "KPI_Answer_MGR>0"));

					if (TotalItem == 0) {
						cell_total.InnerHtml = "ไม่พบหัวข้อในแบบประเมิน";
					} else if (AssCount == 0) {
						cell_total.InnerHtml = "ยังไม่ได้ประเมิน";
					} else if (AssCount == TotalItem) {
						cell_total.InnerHtml = "ประเมินครบ " + TotalItem + " ข้อแล้ว";
					} else {
						cell_total.InnerHtml = "ประเมินแล้ว " + AssCount + " ข้อจากทั้งหมด " + TotalItem + " ข้อ";
					}

					for (int i = 1; i <= 5; i++) {
						HtmlTableCell cell_sum =(HtmlTableCell) e.Item.FindControl("cell_sum_" + i);
						int cnt = GL .CINT ( DT.Compute("COUNT(KPI_No)", "Final_KPI=" + i).ToString());
						if (cnt == 0) {
							cell_sum.InnerHtml = "-";
						} else {
							cell_sum.InnerHtml = "ได้ " + DT.Compute("COUNT(KPI_No)", "Final_KPI=" + i).ToString() + " ข้อ";
							cell_sum.Attributes["class"] = "AssLevel" + i;
						}
					}


					if (TotalItem != 0) {
						DT.Columns.Add("MARK", typeof(Int32), "Final_KPI");
						DT.Columns.Add("WEIGHT", typeof(double));

						for (int i = 0; i <= DT.Rows.Count - 1; i++) {
							if (!GL.IsEqualNull(DT.Rows[i]["KPI_Weight"]) && Information.IsNumeric(DT.Rows[i]["KPI_Weight"].ToString().Replace("%", ""))) {
								DT.Rows[i]["WEIGHT"] = DT.Rows[i]["KPI_Weight"].ToString().Replace("%", "");
							}
						}
						DT.Columns.Add("MARKxWEIGHT", typeof(double), "MARK*WEIGHT");
						DT.Columns.Add("WEIGHTx5", typeof(double), "WEIGHT*5");

						object _SUM = DT.Compute("SUM(MARKxWEIGHT)", "");
						object _TOTAL = DT.Compute("SUM(WEIGHTx5)", "");

						if (!GL.IsEqualNull(_SUM) & !GL.IsEqualNull(_TOTAL)) {
							double Result = Convert.ToDouble(_SUM) * 100 / Convert.ToDouble(_TOTAL);
							if (Strings.Len(Result) > 5)
								Result = GL.CDBL ( GL.StringFormatNumber(Result));
							cell_sum_mark.InnerHtml = Result + "%";
							cell_sum_raw.InnerHtml = (Result * 5) + "/500";

							if (Result <= 20) {
								cell_sum_mark.Attributes["class"] = "AssLevel1";
								cell_sum_raw.Attributes["class"] = "AssLevel1";
							} else if (Result <= 40) {
								cell_sum_mark.Attributes["class"] = "AssLevel2";
								cell_sum_raw.Attributes["class"] = "AssLevel2";
							} else if (Result <= 60) {
								cell_sum_mark.Attributes["class"] = "AssLevel3";
								cell_sum_raw.Attributes["class"] = "AssLevel3";
							} else if (Result <= 80) {
								cell_sum_mark.Attributes["class"] = "AssLevel4";
								cell_sum_raw.Attributes["class"] = "AssLevel4";
							} else {
								cell_sum_mark.Attributes["class"] = "AssLevel5";
								cell_sum_raw.Attributes["class"] = "AssLevel5";
							}
						} else {
							cell_sum_mark.Attributes["class"] = "";
							cell_sum_mark.InnerHtml = "0%";
							cell_sum_raw.InnerHtml = "0/500";
						}

					}
					break;
			}


		}

		protected void btnBack_Click(object sender, System.EventArgs e)
		{
			BindPersonalList();
			pnlList.Visible = true;
			pnlEdit.Visible = false;
		}

		protected void btnPreSend_Click(object sender, System.EventArgs e)
		{
			DataTable DT = BL.GetKPIDetail(R_Year, R_Round, PSNL_NO).Table;

			DT.DefaultView.RowFilter = "KPI_Answer_MGR IS NULL ";
			if (DT.DefaultView.Count > 0) {
				ScriptManager.RegisterStartupScript(this.Page, typeof(string), "Alert", "showAlert('คลิกเพื่อประเมินให้ครบทุกข้อ');", true);
				return;
			}

			ScriptManager.RegisterStartupScript(this.Page, typeof(string), "Alert", "if(confirm('ยืนยันอนุมัติผลการประเมิน ??\\nหลังจากอนุมัติผลการประเมินแล้วคุณไม่สามารถแก้ไขได้')) document.getElementById('" + btnSend.ClientID + "').click();", true);

		}

		protected void btnSend_Click(object sender, System.EventArgs e)
		{
			DataTable DT = BL.GetKPIDetail(R_Year, R_Round, PSNL_NO).Table;

			//--------------------Saving--------------------
			string SQL = "";
			SQL = " SELECT * \n";
			SQL += "  FROM tb_HR_KPI_Header\n";
			SQL += " WHERE PSNL_NO='" + PSNL_NO.Replace("'", "''") + "' AND R_Year=" + R_Year + " AND R_Round=" + R_Round;
			SqlDataAdapter DA = new SqlDataAdapter(SQL, BL.ConnectionString());
			DT = new DataTable();
			DA.Fill(DT);

			DataRow DR = DT.Rows[0];
            HRBL.PersonalInfo PSNInfo = BL.GetAssessmentPersonalInfo(R_Year, R_Round, Session["USER_PSNL_NO"].ToString (), KPI_Status);
			DR["KPI_Status"] = HRBL.AssessmentStatus.AssessmentCompleted;
			DR["Update_By"] = Session["USER_PSNL_NO"];
			DR["Update_Time"] = DateAndTime.Now;
			//------------- Set Manager ----------------
			DR["Assessment_Commit_MGR"] = true;
			DR["Assessment_Commit_Name"] = PSNInfo.PSNL_Fullname;
			DR["Assessment_Commit_DEPT"] = PSNInfo.DEPT_NAME;
			if (!string.IsNullOrEmpty(PSNInfo.MGR_NAME)) {
				DR["Assessment_Commit_POS"] = PSNInfo.MGR_NAME;
			} else {
				DR["Assessment_Commit_POS"] = PSNInfo.FN_NAME;
			}
			DR["Assessment_Commit_By_MGR"] = PSNInfo.PSNL_No;
			DR["Assessment_Commit_Time_MGR"] = DateAndTime.Now;

			if (DT.Rows.Count == 0)
				DT.Rows.Add(DR);
			SqlCommandBuilder cmd = new SqlCommandBuilder(DA);
			DA.Update(DT);

			//----------------- ทำสองที่ Page Load กับ Update Status ---------------
			BL.Update_KPI_Status_To_Assessment_Period(PSNL_NO, R_Year, R_Round);

			ScriptManager.RegisterStartupScript(this.Page, typeof(string), "Alert", "showAlert('การประเมินเสร็จสมบูรณ์');", true);
			BindMasterKPI();

		}


		private void SaveHeader()
		{
			//------------- Check Round ---------------
			if (R_Year == 0 | R_Round == 0)
				return;
			//------------- Check Progress-------------

			string SQL = "";
			SQL = " SELECT * \n";
			SQL += " FROM tb_HR_KPI_Header\n";
			SQL += " WHERE PSNL_NO='" + PSNL_NO.Replace("'", "''") + "' AND R_Year=" + R_Year + " AND R_Round=" + R_Round;
			SqlDataAdapter DA = new SqlDataAdapter(SQL, BL.ConnectionString());
			DataTable DT = new DataTable();
			DA.Fill(DT);

			DataRow DR = null;
			if (DT.Rows.Count == 0) {
				DR = DT.NewRow();
				DR["PSNL_NO"] = PSNL_NO;
				DR["R_Year"] = R_Year;
				DR["R_Round"] = R_Round;
				DR["Create_By"] = Session["USER_PSNL_NO"];
				DR["Create_Time"] = DateAndTime.Now;
			} else {
				DR = DT.Rows[0];
			}

			//------------- Round Detail------------- 
			SQL = "SELECT * FROM tb_HR_Round ";
			SQL += " WHERE R_Year=" + R_Year + " AND R_Round=" + R_Round;
			DataTable PN = new DataTable();
			SqlDataAdapter PA = new SqlDataAdapter(SQL, BL.ConnectionString());
			PA.Fill(PN);
			DR["R_Start"] = PN.Rows[0]["R_Start"];
			DR["R_End"] = PN.Rows[0]["R_End"];
			DR["R_Remark"] = "";

            //============ป้องกันการ update ตำแหน่ง ณ สิ้นรอบประเมินและปิดรอบประเมิน======

            Boolean IsInPeriod = BL.IsTimeInPeriod(DateTime.Now, R_Year, R_Round);
            Boolean IsRoundCompleted = BL.Is_Round_Completed(R_Year, R_Round);
            Boolean ckUpdate = IsInPeriod & !IsRoundCompleted;
            if (ckUpdate | DT.Rows.Count == 0 )

            {
				//------------- Personal Detail---------
				HRBL.PersonalInfo PSNInfo = BL.GetAssessmentPersonalInfo(R_Year, R_Round, PSNL_NO, KPI_Status);
				// Replace With New Updated Data If Exists
				if (!string.IsNullOrEmpty(PSNInfo.PSNL_No)) {
					if (!string.IsNullOrEmpty(PSNInfo.PSNL_No))
						DR["PSNL_No"] = PSNInfo.PSNL_No;
					else
						DR["PSNL_No"] = DBNull.Value;
					if (!string.IsNullOrEmpty(PSNInfo.PSNL_Fullname))
						DR["PSNL_Fullname"] = PSNInfo.PSNL_Fullname;
					else
						DR["PSNL_Fullname"] = DBNull.Value;
					if (!string.IsNullOrEmpty(PSNInfo.PNPS_CLASS))
						DR["PNPS_CLASS"] = PSNInfo.PNPS_CLASS;
					else
						DR["PNPS_CLASS"] = DBNull.Value;
					if (!string.IsNullOrEmpty(PSNInfo.PSNL_TYPE))
						DR["PSNL_TYPE"] = PSNInfo.PSNL_TYPE;
					else
						DR["PSNL_TYPE"] = DBNull.Value;
					if (!string.IsNullOrEmpty(PSNInfo.POS_NO))
						DR["POS_NO"] = PSNInfo.POS_NO;
					else
						DR["POS_NO"] = DBNull.Value;
					if (!string.IsNullOrEmpty(PSNInfo.WAGE_TYPE))
						DR["WAGE_TYPE"] = PSNInfo.WAGE_TYPE;
					else
						DR["WAGE_TYPE"] = DBNull.Value;
					if (!string.IsNullOrEmpty(PSNInfo.WAGE_NAME))
						DR["WAGE_NAME"] = PSNInfo.WAGE_NAME;
					else
						DR["WAGE_NAME"] = DBNull.Value;
					if (!string.IsNullOrEmpty(PSNInfo.SECTOR_CODE))
						DR["SECTOR_CODE"] = Strings.Left(PSNInfo.SECTOR_CODE, 2);
					else
						DR["SECTOR_CODE"] = DBNull.Value;
					if (!string.IsNullOrEmpty(PSNInfo.DEPT_CODE))
						DR["DEPT_CODE"] = PSNInfo.DEPT_CODE;
					else
						DR["DEPT_CODE"] = DBNull.Value;
					if (!string.IsNullOrEmpty(PSNInfo.MINOR_CODE))
						DR["MINOR_CODE"] = PSNInfo.MINOR_CODE;
					else
						DR["MINOR_CODE"] = DBNull.Value;
					if (!string.IsNullOrEmpty(PSNInfo.SECTOR_NAME))
						DR["SECTOR_NAME"] = PSNInfo.SECTOR_NAME;
					else
						DR["SECTOR_NAME"] = DBNull.Value;
					if (!string.IsNullOrEmpty(PSNInfo.DEPT_NAME))
						DR["DEPT_NAME"] = PSNInfo.DEPT_NAME;
					else
						DR["DEPT_NAME"] = DBNull.Value;
					if (!string.IsNullOrEmpty(PSNInfo.FN_ID))
						DR["FN_ID"] = PSNInfo.FN_ID;
					else
						DR["FN_ID"] = DBNull.Value;
					if (!string.IsNullOrEmpty(PSNInfo.FLD_Name))
						DR["FLD_Name"] = PSNInfo.FLD_Name;
					else
						DR["FLD_Name"] = DBNull.Value;
					if (!string.IsNullOrEmpty(PSNInfo.FN_CODE))
						DR["FN_CODE"] = PSNInfo.FN_CODE;
					else
						DR["FN_CODE"] = DBNull.Value;
					if (!string.IsNullOrEmpty(PSNInfo.FN_NAME))
						DR["FN_NAME"] = PSNInfo.FN_NAME;
					else
						DR["FN_NAME"] = DBNull.Value;
					if (!string.IsNullOrEmpty(PSNInfo.MGR_CODE))
						DR["MGR_CODE"] = PSNInfo.MGR_CODE;
					else
						DR["MGR_CODE"] = DBNull.Value;
					if (!string.IsNullOrEmpty(PSNInfo.MGR_NAME))
						DR["MGR_NAME"] = PSNInfo.MGR_NAME;
					else
						DR["MGR_NAME"] = DBNull.Value;
				}
			//------------- Assign Assessor Only-----------------------------
			DR["Create_Commit_By"] = ASSESSOR_BY;
			DR["Create_Commit_Name"] = ASSESSOR_NAME;
			DR["Create_Commit_DEPT"] = ASSESSOR_DEPT;
			DR["Create_Commit_POS"] = ASSESSOR_POS;
			DR["Assessment_Commit_By_MGR"] = ASSESSOR_BY;
			DR["Assessment_Commit_Name"] = ASSESSOR_NAME;
			DR["Assessment_Commit_DEPT"] = ASSESSOR_DEPT;
			DR["Assessment_Commit_POS"] = ASSESSOR_POS;
			}
			DR["Update_By"] = Session["USER_PSNL_NO"];
			DR["Update_Time"] = DateAndTime.Now;

			if (DT.Rows.Count == 0)
				DT.Rows.Add(DR);
			SqlCommandBuilder cmd = new SqlCommandBuilder(DA);
			DA.Update(DT);


			//----------- Prevent Save----------
			if (KPI_Status > HRBL.AssessmentStatus.WaitConfirmAssessment | BL.Is_Round_Completed(R_Year, R_Round)) {
				return;
			}

			DataTable TMP = BL.GetAssessorList(R_Year, R_Round, PSNL_NO);
			TMP.DefaultView.RowFilter = "MGR_PSNL_NO='" + ASSESSOR_BY.Replace("'", "''") + "'";

			//----------------- Update Information For History Assessor Structure ------------------
			SQL = "SELECT * ";
			SQL += " FROM tb_HR_Actual_Assessor ";
			SQL += " WHERE PSNL_NO='" + PSNL_NO + "' AND R_Year=" + R_Year + " AND R_Round=" + R_Round;
			DA = new SqlDataAdapter(SQL, BL.ConnectionString());
			DT = new DataTable();
			DA.Fill(DT);

			if (DT.Rows.Count > 0) {
				DT.Rows[0].Delete();
				cmd = new SqlCommandBuilder(DA);
				DA.Update(DT);
			}

			if (TMP.DefaultView.Count > 0) {
				DR = DT.NewRow();
				DR["PSNL_NO"] = PSNL_NO;
				DR["R_Year"] = R_Year;
				DR["R_Round"] = R_Round;
				DR["MGR_ASSESSOR_POS"] = TMP.DefaultView[0]["ASSESSOR_POS"];
				DR["MGR_PSNL_NO"] = TMP.DefaultView[0]["MGR_PSNL_NO"];
				DR["MGR_PSNL_Fullname"] = TMP.DefaultView[0]["MGR_PSNL_Fullname"];
				DR["MGR_PNPS_CLASS"] = TMP.DefaultView[0]["MGR_CLASS"];
				DR["MGR_PSNL_TYPE"] = TMP.DefaultView[0]["MGR_WAGE_TYPE"];
				DR["MGR_POS_NO"] = TMP.DefaultView[0]["MGR_POS_NO"];
				DR["MGR_WAGE_TYPE"] = TMP.DefaultView[0]["MGR_WAGE_TYPE"];
				DR["MGR_WAGE_NAME"] = TMP.DefaultView[0]["MGR_WAGE_NAME"];
				DR["MGR_DEPT_CODE"] = TMP.DefaultView[0]["MGR_DEPT_CODE"];
				//DR("MGR_MINOR_CODE") = TMP.DefaultView(0).Item("MGR_MINOR_CODE")
				DR["MGR_SECTOR_CODE"] = Strings.Left(TMP.DefaultView[0]["MGR_SECTOR_CODE"].ToString (), 2);
				DR["MGR_SECTOR_NAME"] = TMP.DefaultView[0]["MGR_SECTOR_NAME"];
				DR["MGR_DEPT_NAME"] = TMP.DefaultView[0]["MGR_DEPT_NAME"];
				//DR("MGR_FN_ID") = TMP.DefaultView(0).Item("MGR_FN_ID")
				//DR("MGR_FLD_Name") = TMP.DefaultView(0).Item("MGR_FLD_Name")
				DR["MGR_FN_CODE"] = TMP.DefaultView[0]["MGR_FN_CODE"];
				//DR("MGR_FN_TYPE") = TMP.DefaultView(0).Item("MGR_FN_TYPE")
				DR["MGR_FN_NAME"] = TMP.DefaultView[0]["MGR_FN_NAME"];
				if (!GL.IsEqualNull(TMP.DefaultView[0]["MGR_MGR_CODE"])) {
					DR["MGR_MGR_CODE"] = TMP.DefaultView[0]["MGR_MGR_CODE"];
				}
				if (!GL.IsEqualNull(TMP.DefaultView[0]["MGR_MGR_NAME"])) {
					DR["MGR_MGR_NAME"] = TMP.DefaultView[0]["MGR_MGR_NAME"];
				}
				try {
					 DR["MGR_PNPS_RETIRE_DATE"] = (BL.GetPSNRetireDate(TMP.DefaultView[0]["MGR_PSNL_NO"].ToString()));
				} catch {
				}
				DR["Update_By"] = Session["USER_PSNL_NO"];
				DR["Update_Time"] = DateAndTime.Now;
				DT.Rows.Add(DR);
				cmd = new SqlCommandBuilder(DA);
				DA.Update(DT);
			}

			//----------------- Update Information For History PSN Structure ------------------
			DataTable PSN = new DataTable();
			SQL = "SELECT * FROM vw_PN_PSNL WHERE PSNL_NO='" + PSNL_NO.Replace("'", "''") + "'";
			DA = new SqlDataAdapter(SQL, BL.ConnectionString());
			DA.Fill(PSN);

			SQL = "SELECT * ";
			SQL += " FROM tb_HR_Assessment_PSN ";
			SQL += " WHERE PSNL_NO='" + PSNL_NO.Replace("'", "''") + "' AND R_Year=" + R_Year + " AND R_Round=" + R_Round;
			DA = new SqlDataAdapter(SQL, BL.ConnectionString());
			DT = new DataTable();
			DA.Fill(DT);


			if (PSN.Rows.Count > 0) {
				if (DT.Rows.Count == 0) {
					DR = DT.NewRow();
					DR["PSNL_NO"] = PSNL_NO;
					DR["R_Year"] = R_Year;
					DR["R_Round"] = R_Round;
					if (TMP.DefaultView.Count > 0) {
						try {
							DR["PSN_ASSESSOR_POS"] = TMP.DefaultView[0]["ASSESSOR_POS"];
						} catch {
						}
					}
					DR["PSN_PSNL_NO"] = PSNL_NO;
					DR["PSN_PSNL_Fullname"] = PSN.Rows[0]["PSNL_Fullname"].ToString();
					DT.Rows.Add(DR);
				} else {
					DR = DT.Rows[0];
				}

                //============ป้องกันการ update ตำแหน่ง ณ สิ้นรอบประเมินและปิดรอบประเมิน======
                if (ckUpdate | DT.Rows.Count == 0 | string.IsNullOrEmpty(DT.Rows[0]["PSN_PNPS_CLASS"].ToString()))

                {
					DR["PSN_PNPS_CLASS"] = PSN.Rows[0]["PNPS_CLASS"].ToString();
					DR["PSN_PSNL_TYPE"] = PSN.Rows[0]["PSNL_TYPE"].ToString();
					DR["PSN_POS_NO"] = PSN.Rows[0]["POS_NO"].ToString();
					DR["PSN_WAGE_TYPE"] = PSN.Rows[0]["WAGE_TYPE"].ToString();
					DR["PSN_WAGE_NAME"] = PSN.Rows[0]["WAGE_NAME"].ToString();
					DR["PSN_DEPT_CODE"] = PSN.Rows[0]["DEPT_CODE"].ToString();
					DR["PSN_MINOR_CODE"] = PSN.Rows[0]["MINOR_CODE"].ToString();
					DR["PSN_SECTOR_CODE"] = Strings.Left(PSN.Rows[0]["SECTOR_CODE"].ToString(), 2);
					DR["PSN_SECTOR_NAME"] = PSN.Rows[0]["SECTOR_NAME"].ToString();
					DR["PSN_DEPT_NAME"] = PSN.Rows[0]["DEPT_NAME"].ToString();
					DR["PSN_FN_ID"] = PSN.Rows[0]["FN_ID"].ToString();
					DR["PSN_FLD_Name"] = PSN.Rows[0]["FLD_Name"].ToString();
					DR["PSN_FN_CODE"] = PSN.Rows[0]["FN_CODE"].ToString();
					DR["PSN_FN_TYPE"] = PSN.Rows[0]["FN_TYPE"].ToString();
					DR["PSN_FN_NAME"] = PSN.Rows[0]["FN_NAME"].ToString();
					if (!GL.IsEqualNull(PSN.Rows[0]["MGR_CODE"])) {
						DR["PSN_MGR_CODE"] = PSN.Rows[0]["MGR_CODE"];
					}
					if (!GL.IsEqualNull(PSN.Rows[0]["MGR_NAME"])) {
						DR["PSN_MGR_NAME"] = PSN.Rows[0]["MGR_NAME"];
					}
				}

				try {
					DR["PSN_PNPS_RETIRE_DATE"] = BL.GetPSNRetireDate(PSNL_NO);
				} catch {
				}
				DR["Update_By"] = Session["USER_PSNL_NO"];
				DR["Update_Time"] = DateAndTime.Now;
				cmd = new SqlCommandBuilder(DA);
				DA.Update(DT);

			}
		}

		protected void ddlForm_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if (ddlForm.SelectedIndex != 0) {
				Response.Redirect("Assessment_Team_KPI_Create.aspx");
			}
		}
		public Assessment_Team_KPI_Assessment()
		{
			Load += Page_Load;
		}

	}
}

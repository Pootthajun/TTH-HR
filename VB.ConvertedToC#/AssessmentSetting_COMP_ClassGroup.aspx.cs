using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Xml.Linq;
using System.Diagnostics;
using System.Collections.Specialized;
using System.Configuration;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Caching;
using System.Web.SessionState;
using System.Web.Security;
using System.Web.Profile;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
namespace VB
{

	public partial class AssessmentSetting_COMP_ClassGroup : System.Web.UI.Page
	{


		HRBL BL = new HRBL();
		GenericLib GL = new GenericLib();

		textControlLib TC = new textControlLib();
		public int R_Year {
			get {
				try {
					return GL.CINT( GL.SplitString(ddlRound.Items[ddlRound.SelectedIndex].Value, "-")[0]);
				} catch (Exception ex) {
					return 0;
				}
			}
		}

		public int R_Round {
			get {
				try {
					return GL.CINT(GL.SplitString(ddlRound.Items[ddlRound.SelectedIndex].Value, "-")[1]);
				} catch (Exception ex) {
					return 0;
				}
			}
		}

		public string PSNL_NO {
			get {
				try {
					return Session["USER_PSNL_NO"].ToString ();
				} catch (Exception ex) {
					return "";
				}
			}
		}

		public int MinClass()
		{
			string SQL = "SELECT ISNULL(MIN(PNPS_CLASS),'01') FROM tb_PN_PERSONAL_VIEW";
			SqlDataAdapter DA = new SqlDataAdapter(SQL, BL.ConnectionString());
			DataTable DT = new DataTable();
			DA.Fill(DT);
			return GL.CINT (DT.Rows[0][0]);
		}

		public int MaxClass()
		{
			string SQL = "SELECT ISNULL(MAX(PNPS_CLASS),'01') FROM tb_PN_PERSONAL_VIEW";
			SqlDataAdapter DA = new SqlDataAdapter(SQL, BL.ConnectionString());
			DataTable DT = new DataTable();
			DA.Fill(DT);
			return GL.CINT (DT.Rows[0][0]);
		}

		protected void Page_Load(object sender, System.EventArgs e)
		{
			if ((Session["USER_PSNL_NO"] == null)) {
				ScriptManager.RegisterStartupScript(this.Page, typeof(string), "Alert", "showAlert('กรุณาลงชื่อเข้าใช้ระบบ');", true);
				Response.Redirect("Login.aspx");
				return;
			}

			if (!IsPostBack) {
				BL.BindDDlYearRound(ddlRound);
				BindClass();
			}
		}


		private void BindClass()
		{
			string SQL = "SELECT CLSGP_ID,PNPO_CLASS_Start,PNPO_CLASS_End\n";
			SQL += " FROM tb_HR_PNPO_CLASS_GROUP \n";
			SQL += " WHERE R_Year = " + R_Year + " And R_Round = " + R_Round + "\n";
			SQL += " ORDER BY PNPO_CLASS_Start \n";
			SqlDataAdapter DA = new SqlDataAdapter(SQL, BL.ConnectionString());
			DataTable DT = new DataTable();
			DA.Fill(DT);

			rptClass.DataSource = DT;
			rptClass.DataBind();

			pnlAction.Visible = ddlRound.SelectedIndex > 0;

		}

		protected void ddlRound_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			BindClass();
		}

		protected void rptClass_ItemCommand(object source, System.Web.UI.WebControls.RepeaterCommandEventArgs e)
		{
			switch (e.CommandName) {
				case "Delete":
					DataTable DT = CurrentClassData();
					DT.Rows.RemoveAt(e.Item.ItemIndex);
					rptClass.DataSource = DT;
					rptClass.DataBind();
					break;
			}
		}

		protected void rptClass_ItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
		{
			if (e.Item.ItemType != ListItemType.Item & e.Item.ItemType != ListItemType.AlternatingItem)
				return;

			TextBox txtStart =(TextBox) e.Item.FindControl("txtStart");
			TextBox txtEnd =(TextBox) e.Item.FindControl("txtEnd");
			Button btnDelete =(Button) e.Item.FindControl("btnDelete");

            DataRowView drv = (DataRowView)e.Item.DataItem;

			if (!GL.IsEqualNull(drv["PNPO_CLASS_Start"])) {
				txtStart.Text = GL.StringFormatNumber(drv["PNPO_CLASS_Start"], 0);
			}
			TC.ImplementJavaIntegerText(txtStart);
			txtStart.Style["text-align"] = "center";

			if (!GL.IsEqualNull(drv["PNPO_CLASS_End"])) {
				txtEnd.Text = GL.StringFormatNumber(drv["PNPO_CLASS_End"], 0);
			}

			TC.ImplementJavaIntegerText(txtEnd);
			txtEnd.Style["text-align"] = "center";

			btnDelete.CommandArgument = drv["CLSGP_ID"].ToString ();
		}

		public DataTable CurrentClassData()
		{
			DataTable DT = new DataTable();
			DT.Columns.Add("CLSGP_ID", typeof(int));
			DT.Columns.Add("PNPO_CLASS_Start", typeof(int));
			DT.Columns.Add("PNPO_CLASS_End", typeof(int));
			foreach (RepeaterItem Item in rptClass.Items) {
				if (Item.ItemType != ListItemType.Item & Item.ItemType != ListItemType.AlternatingItem)
					continue;
                TextBox txtStart = (TextBox)Item.FindControl("txtStart");
                TextBox txtEnd = (TextBox)Item.FindControl("txtEnd");
                Button btnDelete = (Button)Item.FindControl("btnDelete");
				DataRow DR = DT.NewRow();
				DR["CLSGP_ID"] = btnDelete.CommandArgument;
				if (!string.IsNullOrEmpty(txtStart.Text)) {
					DR["PNPO_CLASS_Start"] = txtStart.Text;
				}
				if (!string.IsNullOrEmpty(txtEnd.Text)) {
					DR["PNPO_CLASS_End"] = txtEnd.Text;
				}
				DT.Rows.Add(DR);
			}
			return DT;
		}

		protected void btnAdd_Click(object sender, System.EventArgs e)
		{
			DataTable DT = CurrentClassData();
			DataRow DR = DT.NewRow();
			DR["CLSGP_ID"] = 0;
			DT.Rows.Add(DR);
			rptClass.DataSource = DT;
			rptClass.DataBind();
		}


		protected void btnSave_Click(object sender, System.EventArgs e)
		{
			DataTable DT = CurrentClassData();

			if (DT.Rows.Count == 0) {
				ScriptManager.RegisterStartupScript(this.Page, typeof(string), "Alert", "showAlert('กรอกข้อมูลให้สมบูรณ์');", true);
				return;
			}

			int mn = MinClass();
			int mx = MaxClass();

			//If GL.IsEqualNull(DT.Rows(0).Item("PNPO_CLASS_Start")) OrElse DT.Rows(0).Item("PNPO_CLASS_Start") <> mn Then
			//    rptClass.Items(0).FindControl("txtStart").Focus()
			//    ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "showAlert('ระดับเริ่มต้นคือระดับ " & mn & "');", True)
			//    Exit Sub
			//End If
			if (GL.IsEqualNull(DT.Rows[DT.Rows.Count - 1]["PNPO_CLASS_End"]) ||GL.CINT( DT.Rows[DT.Rows.Count - 1]["PNPO_CLASS_End"]) > mx) {
				rptClass.Items[DT.Rows.Count - 1].FindControl("txtEnd").Focus();
				ScriptManager.RegisterStartupScript(this.Page, typeof(string), "Alert", "showAlert('ระดับสุดท้ายคือระดับ " + mx + "');", true);
				return;
			}
			for (int i = 0; i <= DT.Rows.Count - 1; i++) {
				if (GL.IsEqualNull(DT.Rows[i]["PNPO_CLASS_Start"])) {
					rptClass.Items[i].FindControl("txtStart").Focus();
					ScriptManager.RegisterStartupScript(this.Page, typeof(string), "Alert", "showAlert('กรอกระดับให้สมบูรณ์');", true);
					return;
				}
				if (GL.IsEqualNull(DT.Rows[i]["PNPO_CLASS_End"])) {
					rptClass.Items[i].FindControl("txtEnd").Focus();
					ScriptManager.RegisterStartupScript(this.Page, typeof(string), "Alert", "showAlert('กรอกระดับให้สมบูรณ์');", true);
					return;
				}

				if ((i > 0) && GL.CINT (DT.Rows[i]["PNPO_CLASS_Start"]) != (GL.CINT ( DT.Rows[i - 1]["PNPO_CLASS_End"]) + 1)) {
					rptClass.Items[i].FindControl("txtStart").Focus();
					ScriptManager.RegisterStartupScript(this.Page, typeof(string), "Alert", "showAlert('การไล่ระดับไม่ถูกต้อง กรุณาตรวจสอบอีกครั้ง');", true);
					return;
				}

				if (GL.CINT (DT.Rows[i]["PNPO_CLASS_End"]) < GL.CINT (DT.Rows[i]["PNPO_CLASS_Start"])) {
					rptClass.Items[i].FindControl("txtEnd").Focus();
					ScriptManager.RegisterStartupScript(this.Page, typeof(string), "Alert", "showAlert('การไล่ระดับไม่ถูกต้อง กรุณาตรวจสอบอีกครั้ง');", true);
					return;
				}
			}

			//----------------- Remove Old Row---------------
			SqlConnection Conn = new SqlConnection(BL.ConnectionString());
			Conn.Open();
			SqlCommand Comm = new SqlCommand();
			var _with1 = Comm;
			_with1.Connection = Conn;
			_with1.CommandType = CommandType.Text;
			_with1.CommandText = "DELETE FROM tb_HR_PNPO_CLASS_GROUP WHERE R_Year = " + R_Year + " And R_Round = " + R_Round;
			_with1.ExecuteNonQuery();
			_with1.Dispose();
			//----------------- Create New Row---------------
			string SQL = "SELECT *\n";
			SQL += " FROM tb_HR_PNPO_CLASS_GROUP \n";
			SQL += " WHERE R_Year = " + R_Year + " And R_Round = " + R_Round + "\n";
			SqlDataAdapter CA = new SqlDataAdapter(SQL, Conn);
			DataTable CT = new DataTable();
			CA.Fill(CT);

			for (int i = 0; i <= DT.Rows.Count - 1; i++) {
				DataRow CR = CT.NewRow();
				CR["R_Year"] = R_Year;
				CR["R_Round"] = R_Round;
				CR["CLSGP_ID"] = i + 1;
				CR["PNPO_CLASS_Start"] = DT.Rows[i]["PNPO_CLASS_Start"].ToString().PadLeft(2, GL.chr0);
				CR["PNPO_CLASS_End"] = DT.Rows[i]["PNPO_CLASS_End"].ToString().PadLeft(2, GL.chr0);
				CR["Update_By"] = PSNL_NO;
				CR["Update_Time"] = DateAndTime.Now;
				CT.Rows.Add(CR);
			}

			SqlCommandBuilder cmd = new SqlCommandBuilder(CA);
			CA.Update(CT);
			Conn.Close();
			Conn.Dispose();

			ScriptManager.RegisterStartupScript(this.Page, typeof(string), "alert", "showAlert('บันทึกสำเร็จ');", true);
			BindClass();
		}
		public AssessmentSetting_COMP_ClassGroup()
		{
			Load += Page_Load;
		}
	}
}


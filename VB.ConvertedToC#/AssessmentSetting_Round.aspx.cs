using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Xml.Linq;
using System.Diagnostics;
using System.Collections.Specialized;
using System.Configuration;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Caching;
using System.Web.SessionState;
using System.Web.Security;
using System.Web.Profile;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
namespace VB
{

	public partial class AssessmentSetting_Round : System.Web.UI.Page
	{


		HRBL BL = new HRBL();
		GenericLib GL = new GenericLib();

		textControlLib TC = new textControlLib();
		protected void Page_Load(object sender, System.EventArgs e)
		{
			if ((Session["USER_PSNL_NO"] == null)) {
				ScriptManager.RegisterStartupScript(this.Page, typeof(string), "Alert", "showAlert('กรุณาลงชื่อเข้าใช้ระบบ');", true);
				Response.Redirect("Login.aspx");
				return;
			}

			if (!IsPostBack) {
				pnlEdit.Visible = false;
				pnlList.Visible = true;
				BindList();
			}
		}

		private void BindList()
		{
			string SQL = "SELECT * FROM vw_HR_Round ORDER BY R_Year,R_Round";
			SqlDataAdapter DA = new SqlDataAdapter(SQL, BL.ConnectionString());
			DataTable DT = new DataTable();
			DA.Fill(DT);

			Session["AssessmentSetting_Round"] = DT;

			PageNavigation1.SesssionSourceName = "AssessmentSetting_Round";
			PageNavigation1.RenderLayout();

		}

		protected void Pager_PageChanging(PageNavigation Sender)
		{
			PageNavigation1.TheRepeater = rptRound;
		}


		private void ClearEditForm()
		{
			lblEditMode.Text = "สร้าง";
			ddlEditYear.Attributes["R_Year"] = "0";
			ddlEditYear.Attributes["R_Round"] = "0";

			BL.BindDDlCreateYear(ddlEditYear);

			txtEditRound.Text = "";
			TC.ImplementJavaIntegerText(txtEditRound);
			txtEditRound.Style["text-align"] = "left";

			dtp_R_Start.Value = DateTime.FromOADate(0);
			dtp_R_End.Value = DateTime.FromOADate(0);
			dtp_KPI_CP_Start.Value = DateTime.FromOADate(0);
			dtp_KPI_CP_End.Value = DateTime.FromOADate(0);
			dtp_KPI_AP_Start.Value = DateTime.FromOADate(0);
			dtp_KPI_AP_End.Value = DateTime.FromOADate(0);
			dtp_KPI_MGR_Start.Value = DateTime.FromOADate(0);
			dtp_KPI_MGR_End.Value = DateTime.FromOADate(0);
			dtp_COMP_CP_Start.Value = DateTime.FromOADate(0);
			dtp_COMP_CP_End.Value = DateTime.FromOADate(0);
			dtp_COMP_AP_Start.Value = DateTime.FromOADate(0);
			dtp_COMP_AP_End.Value = DateTime.FromOADate(0);
			dtp_COMP_MGR_Start.Value = DateTime.FromOADate(0);
			dtp_COMP_MGR_End.Value = DateTime.FromOADate(0);

			rptLevel.DataSource = BL.PSNClassLevel();
			rptLevel.DataBind();

			DataTable TypeTB = BL.PSNTypeList();
			TypeTB.Columns.Add("Selected", typeof(bool), "true");
			rptType.DataSource = TypeTB;
			rptType.DataBind();

			txtWeightKPI.Text = "";
			TC.ImplementJavaIntegerText(txtWeightKPI);
			txtWeightKPI.Style["text-align"] = "center";

			txtWeightCOMP.Text = "";
			TC.ImplementJavaIntegerText(txtWeightCOMP);
			txtWeightCOMP.Style["text-align"] = "center";
		}

		protected void lnkAddRound_Click(object sender, System.EventArgs e)
		{
			ClearEditForm();
			pnlEdit.Visible = true;
			pnlList.Visible = false;

			//----------------- Set Default From Previous Round---------------
			string SQL = "SELECT TOP 1 * FROM tb_HR_Round ORDER BY R_Year DESC,R_Round DESC";
			DataTable DT = new DataTable();
			SqlDataAdapter DA = new SqlDataAdapter(SQL, BL.ConnectionString());
			DA.Fill(DT);
			if (DT.Rows.Count == 0)
				return;

			//---------- Set KPI Weight---------------
			if (!GL.IsEqualNull(DT.Rows[0]["Weight_KPI"])) {
				txtWeightKPI.Text = GL.StringFormatNumber(DT.Rows[0]["Weight_KPI"], 0);
			}
			//---------- Set COMP Weight---------------
			if (!GL.IsEqualNull(DT.Rows[0]["Weight_COMP"])) {
				txtWeightCOMP.Text = GL.StringFormatNumber(DT.Rows[0]["Weight_COMP"], 0);
			}

			int LastYear =GL.CINT ( DT.Rows[0]["R_Year"]);
			int LastRound =GL.CINT (DT.Rows[0]["R_Round"]);

			//---------------- Set Default Class From Previous-----------------
			SQL = "SELECT * FROM tb_HR_Round_Class WHERE R_Year=" + LastYear + " AND R_Round=" + LastRound;
			DT = new DataTable();
			DA = new SqlDataAdapter(SQL, BL.ConnectionString());
			DA.Fill(DT);
			foreach (RepeaterItem Item in rptLevel.Items) {
				if (Item.ItemType != ListItemType.Item & Item.ItemType != ListItemType.AlternatingItem)
					continue;
				CheckBox chkLevel =(CheckBox) Item.FindControl("chkLevel");
				int Level =GL.CINT ( chkLevel.Attributes["Level"]);
				DT.DefaultView.RowFilter = "PNPS_CLASS=" + Level;
				chkLevel.Checked = DT.DefaultView.Count > 0;
			}

			//---------------- Set Default Type From Previous-----------------
			SQL = "SELECT vw.PSNL_Type_Code \n";
			SQL += " FROM vw_PN_PSNL_Type vw \n";
			SQL += " INNER JOIN tb_HR_Round_Type R ON vw.PSNL_Type_Code=R.PNPS_PSNL_TYPE\n";
			SQL += " AND R.R_Year=" + LastYear + " AND R.R_Round=" + LastRound + "\n";
			DT = new DataTable();
			DA = new SqlDataAdapter(SQL, BL.ConnectionString());
			DA.Fill(DT);
			foreach (RepeaterItem Item in rptType.Items) {
				if (Item.ItemType != ListItemType.Item & Item.ItemType != ListItemType.AlternatingItem)
					continue;
				CheckBox chkType =(CheckBox) Item.FindControl("chkType");
				DT.DefaultView.RowFilter = "PSNL_Type_Code=" + chkType.Attributes["PSNL_TYPE_Code"];
				chkType.Checked = DT.DefaultView.Count > 0;
			}

		}

		protected void btnCancel_Click(object sender, System.EventArgs e)
		{
			BindList();
			pnlEdit.Visible = false;
			pnlList.Visible = true;
		}

		protected void rptLevel_ItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
		{
			if (e.Item.ItemType != ListItemType.Item & e.Item.ItemType != ListItemType.AlternatingItem)
				return;

			CheckBox chkLevel =(CheckBox) e.Item.FindControl("chkLevel");
			Label lbl = (Label)e.Item.FindControl("lbl");
            DataRowView drv = (DataRowView)e.Item.DataItem;

			lbl.Text = "ระดับ " + drv["Level"];
			chkLevel.Attributes["Level"] = drv["Level"].ToString ();
			chkLevel.Checked =(bool ) drv["Selected"];
		}

		protected void rptType_ItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
		{
			if (e.Item.ItemType != ListItemType.Item & e.Item.ItemType != ListItemType.AlternatingItem)
				return;

			CheckBox chkType =(CheckBox) e.Item.FindControl("chkType");
			Label lbl = (Label)e.Item.FindControl("lbl");
            DataRowView drv = (DataRowView)e.Item.DataItem;

			lbl.Text = drv["PSNL_Type_Name"].ToString ();
			chkType.Attributes["PSNL_TYPE_Code"] = drv["PSNL_Type_Code"].ToString ();
            chkType.Checked = GL.CBOOL(drv["Selected"]);
		}

		protected void rptRound_ItemCommand(object source, System.Web.UI.WebControls.RepeaterCommandEventArgs e)
		{
			if (e.Item.ItemType != ListItemType.Item & e.Item.ItemType != ListItemType.AlternatingItem)
				return;

			LinkButton lnkDetail =(LinkButton) e.Item.FindControl("lnkDetail");
			int R_Year =GL.CINT ( lnkDetail.Attributes["R_Year"]);
			int R_Round = GL.CINT (lnkDetail.Attributes["R_Round"]);
            SqlConnection Conn;
            DataTable DT = new DataTable();
            DataTable LT = BL.PSNClassLevel();
            DataTable TT = BL.PSNTypeList();

			switch (e.CommandName) {
				case "Edit":
					ClearEditForm();

                    //DataTable DT = new DataTable();
					string SQL = "SELECT * FROM vw_HR_Round WHERE R_Year=" + R_Year + " AND R_Round=" + R_Round;
					SqlDataAdapter DA = new SqlDataAdapter(SQL, BL.ConnectionString());
					DA.Fill(DT);
					if (DT.Rows.Count == 0) {
						ScriptManager.RegisterStartupScript(this.Page, typeof(string), "alert", "showAlert('ไม่พบรอบดังกล่าว');", true);
						BindList();
						return;
					}

					lblEditMode.Text = "ข้อมูล";
					ddlEditYear.Attributes["R_Year"] = GL.CINT (R_Year).ToString ();
					ddlEditYear.Attributes["R_Round"] = GL.CINT (R_Round).ToString ();

					//-------------------HR_Round --------------------
					BL.BindDDlCreateYear(ddlEditYear, R_Year);
					txtEditRound.Text = R_Round.ToString();
                    dtp_R_Start.Value = (DateTime)DT.Rows[0]["R_Start"];
                    dtp_R_End.Value = (DateTime)DT.Rows[0]["R_End"];
                    txtWeightKPI.Text = GL.CompactDecimal(DT.Rows[0]["Weight_KPI"].ToString());
                    txtWeightCOMP.Text = GL.CompactDecimal(DT.Rows[0]["Weight_COMP"].ToString());
					//-------------------KPI_Period--------------
                    dtp_KPI_CP_Start.Value = (DateTime)DT.Rows[0]["KPI_CP_Start"];
                    dtp_KPI_CP_End.Value = (DateTime)DT.Rows[0]["KPI_CP_End"];
                    dtp_KPI_AP_Start.Value = (DateTime)DT.Rows[0]["KPI_AP_Start"];
                    dtp_KPI_AP_End.Value = (DateTime)DT.Rows[0]["KPI_AP_End"];
                    dtp_KPI_MGR_Start.Value = (DateTime)DT.Rows[0]["KPI_MGR_Start"];
                    dtp_KPI_MGR_End.Value = (DateTime)DT.Rows[0]["KPI_MGR_End"];
					//-------------------COMP_Period-----------------
                    dtp_COMP_CP_Start.Value = (DateTime)DT.Rows[0]["COMP_CP_Start"];
                    dtp_COMP_CP_End.Value = (DateTime)DT.Rows[0]["COMP_CP_End"];
                    dtp_COMP_AP_Start.Value = (DateTime)DT.Rows[0]["COMP_AP_Start"];
                    dtp_COMP_AP_End.Value = (DateTime)DT.Rows[0]["COMP_AP_End"];
                    dtp_COMP_MGR_Start.Value = (DateTime)DT.Rows[0]["COMP_MGR_Start"];
                    dtp_COMP_MGR_End.Value = (DateTime)DT.Rows[0]["COMP_MGR_End"];
					//-------------------tb_HR_Round_Class------------------------
					DT = new DataTable();
					SQL = "SELECT PNPS_CLASS FROM tb_HR_Round_Class WHERE R_Year=" + R_Year + " AND R_Round=" + R_Round;
					DA = new SqlDataAdapter(SQL, BL.ConnectionString());
					DA.Fill(DT);
                    //DataTable LT = BL.PSNClassLevel();
					for (int i = 0; i <= LT.Rows.Count - 1; i++) {
						DT.DefaultView.RowFilter = "PNPS_CLASS=" + LT.Rows[i]["Level"];
						LT.Rows[i]["Selected"] = DT.DefaultView.Count > 0;
					}

					rptLevel.DataSource = LT;
					rptLevel.DataBind();
					//-------------------tb_HR_Round_Type-------------------------
					DT = new DataTable();
					SQL = "SELECT PNPS_PSNL_TYPE FROM tb_HR_Round_Type WHERE R_Year=" + R_Year + " AND R_Round=" + R_Round;
					DA = new SqlDataAdapter(SQL, BL.ConnectionString());
					DA.Fill(DT);
                    //DataTable TT = BL.PSNTypeList();
					TT.Columns.Add("Selected", typeof(bool));
					for (int i = 0; i <= TT.Rows.Count - 1; i++) {
                        DT.DefaultView.RowFilter = "PNPS_PSNL_TYPE =" + GL.CINT(TT.Rows[i]["PSNL_Type_Code"]) ;
                        TT.Rows[i]["Selected"] = DT.DefaultView.Count > 0;
					}

					rptType.DataSource = TT;
					rptType.DataBind();

					pnlEdit.Visible = true;
					pnlList.Visible = false;

                    pnlComplete.Enabled = true ;
                    btnSave.Visible = true ;

					break;
                case "ToggleClose":

                    Conn = new SqlConnection(BL.ConnectionString());
					Conn.Open();
					SqlCommand Comm = new SqlCommand();
					var _with1 = Comm;
					_with1.Connection = Conn;
					_with1.CommandType = CommandType.Text;
                    _with1.CommandText = "UPDATE tb_HR_Round SET R_Completed=CASE ISNULL(R_Completed,0) WHEN 0 THEN 1 ELSE 0 END \n";
                    _with1.CommandText += "WHERE R_Year=" + R_Year.ToString() + " AND R_Round=" + R_Round.ToString();
					_with1.ExecuteNonQuery();
					_with1.Dispose();
					Conn.Close();
                    BindList();
					break;
				//------------------- Check Closed Condition--------------------------

				case "Delete":

					Conn = new SqlConnection(BL.ConnectionString());
					Conn.Open();
					Comm = new SqlCommand();
					var _with2 = Comm;
                    _with2.Connection = Conn;
                    _with2.CommandType = CommandType.Text;
                    _with2.CommandText = "EXEC dbo.sp_Drop_Round " + R_Year + "," + R_Round;
                    _with2.ExecuteNonQuery();
                    _with2.CommandTimeout = 90;
                    _with2.Dispose();
					Conn.Close();

					ScriptManager.RegisterStartupScript(this.Page, typeof(string), "alert", "showAlert('ลบรอบสำเร็จ');", true);
					BindList();
					break;
                case "Complete":

                    ClearEditForm();

                    //DataTable DT = new DataTable();
                    string SQL_Complete = "SELECT * FROM vw_HR_Round WHERE R_Year=" + R_Year + " AND R_Round=" + R_Round;
                    SqlDataAdapter DA_Complete = new SqlDataAdapter(SQL_Complete, BL.ConnectionString());
                    DA_Complete.Fill(DT);
                    if (DT.Rows.Count == 0)
                    {
						ScriptManager.RegisterStartupScript(this.Page, typeof(string), "alert", "showAlert('ไม่พบรอบดังกล่าว');", true);
						BindList();
						return;
					}

					lblEditMode.Text = "ข้อมูล";
					ddlEditYear.Attributes["R_Year"] = GL.CINT (R_Year).ToString ();
					ddlEditYear.Attributes["R_Round"] = GL.CINT (R_Round).ToString ();

					//-------------------HR_Round --------------------
					BL.BindDDlCreateYear(ddlEditYear, R_Year);
					txtEditRound.Text = R_Round.ToString();
                    dtp_R_Start.Value = (DateTime)DT.Rows[0]["R_Start"];
                    dtp_R_End.Value = (DateTime)DT.Rows[0]["R_End"];
                    txtWeightKPI.Text = GL.CompactDecimal(DT.Rows[0]["Weight_KPI"].ToString());
                    txtWeightCOMP.Text = GL.CompactDecimal(DT.Rows[0]["Weight_COMP"].ToString());
					//-------------------KPI_Period--------------
                    dtp_KPI_CP_Start.Value = (DateTime)DT.Rows[0]["KPI_CP_Start"];
                    dtp_KPI_CP_End.Value = (DateTime)DT.Rows[0]["KPI_CP_End"];
                    dtp_KPI_AP_Start.Value = (DateTime)DT.Rows[0]["KPI_AP_Start"];
                    dtp_KPI_AP_End.Value = (DateTime)DT.Rows[0]["KPI_AP_End"];
                    dtp_KPI_MGR_Start.Value = (DateTime)DT.Rows[0]["KPI_MGR_Start"];
                    dtp_KPI_MGR_End.Value = (DateTime)DT.Rows[0]["KPI_MGR_End"];
					//-------------------COMP_Period-----------------
                    dtp_COMP_CP_Start.Value = (DateTime)DT.Rows[0]["COMP_CP_Start"];
                    dtp_COMP_CP_End.Value = (DateTime)DT.Rows[0]["COMP_CP_End"];
                    dtp_COMP_AP_Start.Value = (DateTime)DT.Rows[0]["COMP_AP_Start"];
                    dtp_COMP_AP_End.Value = (DateTime)DT.Rows[0]["COMP_AP_End"];
                    dtp_COMP_MGR_Start.Value = (DateTime)DT.Rows[0]["COMP_MGR_Start"];
                    dtp_COMP_MGR_End.Value = (DateTime)DT.Rows[0]["COMP_MGR_End"];
					//-------------------tb_HR_Round_Class------------------------
					DT = new DataTable();
					SQL = "SELECT PNPS_CLASS FROM tb_HR_Round_Class WHERE R_Year=" + R_Year + " AND R_Round=" + R_Round;
					DA = new SqlDataAdapter(SQL, BL.ConnectionString());
					DA.Fill(DT);
                    //DataTable LT = BL.PSNClassLevel();
					for (int i = 0; i <= LT.Rows.Count - 1; i++) {
						DT.DefaultView.RowFilter = "PNPS_CLASS=" + LT.Rows[i]["Level"];
						LT.Rows[i]["Selected"] = DT.DefaultView.Count > 0;
					}

					rptLevel.DataSource = LT;
					rptLevel.DataBind();
					//-------------------tb_HR_Round_Type-------------------------
					DT = new DataTable();
					SQL = "SELECT PNPS_PSNL_TYPE FROM tb_HR_Round_Type WHERE R_Year=" + R_Year + " AND R_Round=" + R_Round;
					DA = new SqlDataAdapter(SQL, BL.ConnectionString());
					DA.Fill(DT);
                    //DataTable TT = BL.PSNTypeList();
					TT.Columns.Add("Selected", typeof(bool));
					for (int i = 0; i <= TT.Rows.Count - 1; i++) {
                        DT.DefaultView.RowFilter = "PNPS_PSNL_TYPE =" + GL.CINT(TT.Rows[i]["PSNL_Type_Code"]);
                        TT.Rows[i]["Selected"] = DT.DefaultView.Count > 0;
                    }

					rptType.DataSource = TT;
					rptType.DataBind();

					pnlEdit.Visible = true;
					pnlList.Visible = false;

                    pnlComplete.Enabled = false;
                    btnSave.Visible = false;
                    break;

			}
		}

		protected void rptRound_ItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
		{
			if (e.Item.ItemType != ListItemType.Item & e.Item.ItemType != ListItemType.AlternatingItem)
				return;

			Label lbl_R_Year =(Label) e.Item.FindControl("lbl_R_Year");
			Label lbl_R_Round =(Label) e.Item.FindControl("lbl_R_Round");
			Label lbl_R_Start =(Label) e.Item.FindControl("lbl_R_Start");
			Label lbl_R_End =(Label)  e.Item.FindControl("lbl_R_End");
			Label lbl_KPI_CP_Start =(Label) e.Item.FindControl("lbl_KPI_CP_Start");
			Label lbl_KPI_CP_End =(Label) e.Item.FindControl("lbl_KPI_CP_End");
			Label lbl_KPI_AP_Start =(Label) e.Item.FindControl("lbl_KPI_AP_Start");
			Label lbl_KPI_AP_End =(Label) e.Item.FindControl("lbl_KPI_AP_End");
			Label lbl_COMP_CP_Start =(Label) e.Item.FindControl("lbl_COMP_CP_Start");
			Label lbl_COMP_CP_End =(Label) e.Item.FindControl("lbl_COMP_CP_End");
			Label lbl_COMP_AP_Start =(Label) e.Item.FindControl("lbl_COMP_AP_Start");
			Label lbl_COMP_AP_End =(Label) e.Item.FindControl("lbl_COMP_AP_End");
			LinkButton lnkDetail =(LinkButton) e.Item.FindControl("lnkDetail");
			LinkButton lnkCloseRound =(LinkButton) e.Item.FindControl("lnkCloseRound");
			LinkButton lnkDeleteRound =(LinkButton) e.Item.FindControl("lnkDeleteRound");
            //HtmlAnchor lnkComplete = (HtmlAnchor)e.Item.FindControl("lnkComplete");
            LinkButton lnkComplete = (LinkButton)e.Item.FindControl("lnkComplete");
			AjaxControlToolkit.ConfirmButtonExtender cfm_Close = (AjaxControlToolkit.ConfirmButtonExtender)e.Item.FindControl("cfm_Close");
			AjaxControlToolkit.ConfirmButtonExtender cfm_Delete =(AjaxControlToolkit.ConfirmButtonExtender) e.Item.FindControl("cfm_Delete");

            DataRowView drv = (DataRowView)e.Item.DataItem;

			lbl_R_Year.Text =GL.CINT ( drv["R_Year"]).ToString ();
			lbl_R_Round.Text = GL.CINT (drv["R_Round"]).ToString ();
            lbl_R_Start.Text = BL.DateTimeToThaiDate((DateTime)drv["R_Start"]);
            lbl_R_End.Text = BL.DateTimeToThaiDate((DateTime)drv["R_End"]);
            lbl_KPI_CP_Start.Text = BL.DateTimeToThaiDate((DateTime)drv["KPI_CP_Start"]);
            lbl_KPI_CP_End.Text = BL.DateTimeToThaiDate((DateTime)drv["KPI_CP_End"]);
            if ((DateTime)drv["KPI_AP_Start"] < (DateTime)drv["KPI_MGR_Start"])
            {
                lbl_KPI_AP_Start.Text = BL.DateTimeToThaiDate((DateTime)drv["KPI_AP_Start"]);
			} else {
                lbl_KPI_AP_Start.Text = BL.DateTimeToThaiDate((DateTime)drv["KPI_MGR_Start"]);
			}
            lbl_KPI_AP_End.Text = BL.DateTimeToThaiDate((DateTime)drv["KPI_MGR_End"]);
            lbl_COMP_CP_Start.Text = BL.DateTimeToThaiDate((DateTime)drv["COMP_CP_Start"]);
            lbl_COMP_CP_End.Text = BL.DateTimeToThaiDate((DateTime)drv["COMP_CP_End"]);
            if ((DateTime)drv["COMP_AP_Start"] < (DateTime)drv["COMP_MGR_Start"])
            {
                lbl_COMP_AP_Start.Text = BL.DateTimeToThaiDate((DateTime)drv["COMP_AP_Start"]);
			} else {
                lbl_COMP_AP_Start.Text = BL.DateTimeToThaiDate((DateTime)drv["COMP_MGR_Start"]);
			}
			lbl_COMP_AP_End.Text = BL.DateTimeToThaiDate((DateTime )drv["COMP_MGR_End"]);


			lnkDetail.Visible = !GL.CBOOL(drv["R_Completed"]);
            //lnkCloseRound.Visible = !GL.CBOOL(drv["R_Completed"]);
            if (!GL.CBOOL(drv["R_Completed"]))
            {
                lnkCloseRound.Text = "ปิดรอบ";
                lnkCloseRound.CssClass = "btn mini green";
                cfm_Close.ConfirmText = "ยืนยันปิดรอบประเมินปี " + drv["R_Year"] + " รอบ " + drv["R_Round"];
            }
            else 
            {
                lnkCloseRound.Text = "เปิดรอบ";
                lnkCloseRound.CssClass = "btn mini red";
                cfm_Close.ConfirmText = "ยืนยันเปิดรอบประเมินปี " + drv["R_Year"] + " รอบ " + drv["R_Round"];
            }

            lnkDeleteRound.Visible = !GL.CBOOL(drv["R_Completed"]);
            lnkComplete.Visible = GL.CBOOL(drv["R_Completed"]);

			lnkDetail.Attributes["R_Year"] = GL.CINT(drv["R_Year"]).ToString();
            lnkDetail.Attributes["R_Round"] = GL.CINT(drv["R_Round"]).ToString();

			
			cfm_Delete.ConfirmText = "ยืนยันลบรอบประเมินปี " + drv["R_Year"] + " รอบ " + drv["R_Round"];


		}

		protected void btnSave_Click(object sender, System.EventArgs e)
		{
			//----------------- Validation-------------------
			if (ddlEditYear.SelectedIndex < 1) {
				ScriptManager.RegisterStartupScript(this.Page, typeof(string), "alert", "showAlert('เลือกปีประเมิน');", true);
				ddlEditYear.Focus();
				return;
			}
			if (string.IsNullOrEmpty(txtEditRound.Text)) {
				ScriptManager.RegisterStartupScript(this.Page, typeof(string), "alert", "showAlert('กรอกรอบประเมิน');", true);
				txtEditRound.Focus();
				return;
			}

			//-------------- เชค ปี/รอบ ซ้ำ-------------------
			DataTable DT = new DataTable();
			string SQL = "SELECT * FROM tb_HR_Round WHERE R_Year=" + ddlEditYear.Items[ddlEditYear.SelectedIndex].Value + " AND R_Round=" + txtEditRound.Text + "\n";
			SQL += " AND R_Year<>" + ddlEditYear.Attributes["R_Year"] + " AND R_Round<>" + ddlEditYear.Attributes["R_Round"];
			SqlDataAdapter DA = new SqlDataAdapter(SQL, BL.ConnectionString());
			DA.Fill(DT);

			if (DT.Rows.Count != 0) {
				ScriptManager.RegisterStartupScript(this.Page, typeof(string), "alert", "showAlert('ปีและรอบประเมินซ้ำ');", true);
				txtEditRound.Focus();
				return;
			}
			//----------------- Round--------------
			if (!dtp_R_Start.IsDateSelected) {
				ScriptManager.RegisterStartupScript(this.Page, typeof(string), "alert", "showAlert('เลือกรอบการประเมิน');", true);
				dtp_R_Start.Focus();
				return;
			}
			if (!dtp_R_End.IsDateSelected) {
				ScriptManager.RegisterStartupScript(this.Page, typeof(string), "alert", "showAlert('เลือกรอบการประเมิน');", true);
				dtp_R_End.Focus();
				return;
			}
			if (dtp_R_Start.Value >= dtp_R_End.Value) {
				ScriptManager.RegisterStartupScript(this.Page, typeof(string), "alert", "showAlert('ตรวจสอบวันสิ้นสุดรอบการประเมิน');", true);
				dtp_R_End.Focus();
				return;
			}
			//----------------- KPI--------------
			if (!dtp_KPI_CP_Start.IsDateSelected) {
				ScriptManager.RegisterStartupScript(this.Page, typeof(string), "alert", "showAlert('เลือกวันกำหนดแบบประเมินตัวชี้วัด');", true);
				dtp_KPI_CP_Start.Focus();
				return;
			}
			if (!dtp_KPI_CP_End.IsDateSelected) {
				ScriptManager.RegisterStartupScript(this.Page, typeof(string), "alert", "showAlert('เลือกวันกำหนดแบบประเมินตัวชี้วัด');", true);
				dtp_KPI_CP_End.Focus();
				return;
			}
			if (dtp_KPI_CP_Start.Value >= dtp_KPI_CP_End.Value) {
				ScriptManager.RegisterStartupScript(this.Page, typeof(string), "alert", "showAlert('ตรวจสอบวันสิ้นสุดกำหนดแบบประเมินตัวชี้วัด');", true);
				dtp_KPI_CP_End.Focus();
				return;
			}
			if (!dtp_KPI_AP_Start.IsDateSelected) {
				ScriptManager.RegisterStartupScript(this.Page, typeof(string), "alert", "showAlert('เลือกวันกำหนดประเมินตัวชี้วัด');", true);
				dtp_KPI_AP_Start.Focus();
				return;
			}
			if (!dtp_KPI_AP_End.IsDateSelected) {
				ScriptManager.RegisterStartupScript(this.Page, typeof(string), "alert", "showAlert('เลือกวันกำหนดประเมินตัวชี้วัด');", true);
				dtp_KPI_AP_End.Focus();
				return;
			}
			if (dtp_KPI_AP_Start.Value >= dtp_KPI_AP_End.Value) {
				ScriptManager.RegisterStartupScript(this.Page, typeof(string), "alert", "showAlert('ตรวจสอบวันสิ้นสุดการประเมินตัวชี้วัด');", true);
				dtp_KPI_AP_End.Focus();
				return;
			}
			if (!dtp_KPI_MGR_Start.IsDateSelected) {
				ScriptManager.RegisterStartupScript(this.Page, typeof(string), "alert", "showAlert('เลือกวันกำหนดประเมินตัวชี้วัด');", true);
				dtp_KPI_MGR_Start.Focus();
				return;
			}
			if (!dtp_KPI_MGR_End.IsDateSelected) {
				ScriptManager.RegisterStartupScript(this.Page, typeof(string), "alert", "showAlert('เลือกวันกำหนดประเมินตัวชี้วัด');", true);
				dtp_KPI_MGR_End.Focus();
				return;
			}
			if (dtp_KPI_MGR_Start.Value >= dtp_KPI_MGR_End.Value) {
				ScriptManager.RegisterStartupScript(this.Page, typeof(string), "alert", "showAlert('ตรวจสอบวันสิ้นสุดการประเมินตัวชี้วัด');", true);
				dtp_KPI_MGR_End.Focus();
				return;
			}
			//----------------- COMP--------------
			if (!dtp_COMP_CP_Start.IsDateSelected) {
				ScriptManager.RegisterStartupScript(this.Page, typeof(string), "alert", "showAlert('เลือกวันกำหนดแบบประเมินสมรรถนะ');", true);
				dtp_COMP_CP_Start.Focus();
				return;
			}
			if (!dtp_COMP_CP_End.IsDateSelected) {
				ScriptManager.RegisterStartupScript(this.Page, typeof(string), "alert", "showAlert('เลือกวันกำหนดแบบประเมินสมรรถนะ');", true);
				dtp_COMP_CP_End.Focus();
				return;
			}
			if (dtp_COMP_CP_Start.Value >= dtp_COMP_CP_End.Value) {
				ScriptManager.RegisterStartupScript(this.Page, typeof(string), "alert", "showAlert('ตรวจสอบวันสิ้นสุดกำหนดแบบประเมินสมรรถนะ');", true);
				dtp_COMP_CP_End.Focus();
				return;
			}
			if (!dtp_COMP_AP_Start.IsDateSelected) {
				ScriptManager.RegisterStartupScript(this.Page, typeof(string), "alert", "showAlert('เลือกวันกำหนดประเมินสมรรถนะ');", true);
				dtp_COMP_AP_Start.Focus();
				return;
			}
			if (!dtp_COMP_AP_End.IsDateSelected) {
				ScriptManager.RegisterStartupScript(this.Page, typeof(string), "alert", "showAlert('เลือกวันกำหนดประเมินสมรรถนะ');", true);
				dtp_COMP_AP_End.Focus();
				return;
			}
			if (dtp_COMP_AP_Start.Value >= dtp_COMP_AP_End.Value) {
				ScriptManager.RegisterStartupScript(this.Page, typeof(string), "alert", "showAlert('ตรวจสอบวันสิ้นสุดการประเมินสมรรถนะ');", true);
				dtp_COMP_AP_End.Focus();
				return;
			}
			if (!dtp_COMP_MGR_Start.IsDateSelected) {
				ScriptManager.RegisterStartupScript(this.Page, typeof(string), "alert", "showAlert('เลือกวันกำหนดประเมินสมรรถนะ');", true);
				dtp_COMP_MGR_Start.Focus();
				return;
			}
			if (!dtp_COMP_MGR_End.IsDateSelected) {
				ScriptManager.RegisterStartupScript(this.Page, typeof(string), "alert", "showAlert('เลือกวันกำหนดประเมินสมรรถนะ');", true);
				dtp_COMP_MGR_End.Focus();
				return;
			}
			if (dtp_COMP_MGR_Start.Value >= dtp_COMP_MGR_End.Value) {
				ScriptManager.RegisterStartupScript(this.Page, typeof(string), "alert", "showAlert('ตรวจสอบวันสิ้นสุดการประเมินสมรรถนะ');", true);
				dtp_COMP_MGR_End.Focus();
				return;
			}

			if (string.IsNullOrEmpty(txtWeightKPI.Text)) {
				ScriptManager.RegisterStartupScript(this.Page, typeof(string), "alert", "showAlert('กำหนดน้ำหนักการประเมินตัวชี้วัด');", true);
				txtWeightKPI.Focus();
				return;
			}
			if (string.IsNullOrEmpty(txtWeightCOMP.Text)) {
				ScriptManager.RegisterStartupScript(this.Page, typeof(string), "alert", "showAlert('กำหนดน้ำหนักการประเมินสมรรถนะ');", true);
				txtWeightCOMP.Focus();
				return;
			}

			DataTable CT = CurrentSelectedClass();
			CT.DefaultView.RowFilter = "Selected=True";
			if (CT.DefaultView.Count == 0) {
				ScriptManager.RegisterStartupScript(this.Page, typeof(string), "alert", "showAlert('เลือกระดับพนักงานที่ประเมินในรอบนี้');", true);
				rptLevel.Focus();
				return;
			}

			DataTable TT = CurrentSelectedType();
			TT.DefaultView.RowFilter = "Selected=True";
			if (TT.DefaultView.Count == 0) {
				ScriptManager.RegisterStartupScript(this.Page, typeof(string), "alert", "showAlert('เลือกประเภทพนักงานที่ประเมินในรอบนี้');", true);
				rptType.Focus();
				return;
			}

			//------------------Start Save ------------------
			int R_Year = GL.CINT (ddlEditYear.Attributes["R_Year"]);
			int R_Round = GL.CINT (ddlEditYear.Attributes["R_Round"]);
			SqlCommandBuilder cmd = null;
			//----------------- Clear Recent Data------------
			DT = new DataTable();
			SQL = "SELECT * FROM tb_HR_Round WHERE R_Year=" + R_Year + " AND R_Round=" + R_Round;
			DA = new SqlDataAdapter(SQL, BL.ConnectionString());
			DA.Fill(DT);

			R_Year = GL.CINT(ddlEditYear.Items[ddlEditYear.SelectedIndex].Value);
			R_Round = GL.CINT(txtEditRound.Text);
			DataRow DR = null;
			if (DT.Rows.Count == 0) {
				DR = DT.NewRow();
				DR["Create_By"] = Session["USER_PSNL_NO"];
				DR["Create_Time"] = DateAndTime.Now;
				DR["R_Completed"] = false;
			} else {
				DR = DT.Rows[0];
			}

			DR["R_Year"] = R_Year;
			DR["R_Round"] = R_Round;
			DR["R_Start"] = dtp_R_Start.Value;
			DR["R_End"] = dtp_R_End.Value;
			DR["Weight_KPI"] = Conversion.Val(txtWeightKPI.Text);
			DR["Weight_COMP"] = Conversion.Val(txtWeightCOMP.Text);
			DR["Update_By"] = Session["USER_PSNL_NO"];
			DR["Update_Time"] = DateAndTime.Now;

			if (DT.Rows.Count == 0)
				DT.Rows.Add(DR);
			try {
				cmd = new SqlCommandBuilder(DA);
				DA.Update(DT);
			} catch (Exception ex) {
				ScriptManager.RegisterStartupScript(this.Page, typeof(string), "alert1", "showAlert('ไม่สามารถบันทึกได้');", true);
				ScriptManager.RegisterStartupScript(this.Page, typeof(string), "alert2", "showAlert('" + ex.Message.Replace("'", "\"") + "');", true);
				return;
			}

			ddlEditYear.Attributes["R_Year"] = GL.CINT (R_Year).ToString ();
			ddlEditYear.Attributes["R_Round"] = GL.CINT (R_Round).ToString ();

			//------------------- Save Round Type--------------------
			DT = new DataTable();
			SQL = "DELETE FROM tb_HR_Round_Class WHERE R_Year=" + R_Year + " AND R_Round=" + R_Round + "\n";
			SQL += "SELECT * FROM tb_HR_Round_Class WHERE R_Year=" + R_Year + " AND R_Round=" + R_Round + "\n";
			DA = new SqlDataAdapter(SQL, BL.ConnectionString());
			DA.Fill(DT);
			CT.DefaultView.RowFilter = "Selected=True";
			CT = CT.DefaultView.ToTable();
			for (int i = 0; i <= CT.Rows.Count - 1; i++) {
				DR = DT.NewRow();
				DR["R_Year"] = R_Year;
				DR["R_Round"] = R_Round;
				DR["PNPS_CLASS"] = CT.Rows[i]["Level"];
				DT.Rows.Add(DR);
			}
			try {
				cmd = new SqlCommandBuilder(DA);
				DA.Update(DT);
			} catch (Exception ex) {
				ScriptManager.RegisterStartupScript(this.Page, typeof(string), "alert1", "showAlert('ไม่สามารถบันทึกได้');", true);
				ScriptManager.RegisterStartupScript(this.Page, typeof(string), "alert2", "showAlert('" + ex.Message.Replace("'", "\"") + "');", true);
				return;
			}

			//------------------- Save Round Class-------------------
			DT = new DataTable();
			SQL = "DELETE FROM tb_HR_Round_Type WHERE R_Year=" + R_Year + " AND R_Round=" + R_Round + "\n";
			SQL += "SELECT * FROM tb_HR_Round_Type WHERE R_Year=" + R_Year + " AND R_Round=" + R_Round + "\n";
			DA = new SqlDataAdapter(SQL, BL.ConnectionString());
			DA.Fill(DT);
			TT.DefaultView.RowFilter = "Selected=True";
			TT = TT.DefaultView.ToTable();
            
            if (TT.DefaultView.Count > 0)
            {
                for (int i = 0; i <= TT.DefaultView.Count - 1; i++)
                { 
                    DR = DT.NewRow();
                    DR["R_Year"] = R_Year;
                    DR["R_Round"] = R_Round;
                    DR["PNPS_PSNL_TYPE"] = GL.CINT(TT.Rows[i]["PSNL_Type_Code"]);
                    DT.Rows.Add(DR);
                }
                    
            }

			try {
				cmd = new SqlCommandBuilder(DA);
				DA.Update(DT);
			} catch (Exception ex) {
				ScriptManager.RegisterStartupScript(this.Page, typeof(string), "alert1", "showAlert('ไม่สามารถบันทึกได้');", true);
				ScriptManager.RegisterStartupScript(this.Page, typeof(string), "alert2", "showAlert('" + ex.Message.Replace("'", "\"") + "');", true);
				return;
			}

			//------------------- Save KPI Create Period -------------------
			DT = new DataTable();
			SQL = "SELECT * FROM tb_HR_KPI_Create_Period WHERE R_Year=" + R_Year + " AND R_Round=" + R_Round + "\n";
			SQL += " AND PNDP_DEPT_CODE1='00000000' AND PNDP_MINOR_CODE2='00'";
			DA = new SqlDataAdapter(SQL, BL.ConnectionString());
			DA.Fill(DT);

			if (DT.Rows.Count == 0) {
				DR = DT.NewRow();
				DR["Create_By"] = Session["USER_PSNL_NO"];
				DR["Create_Time"] = DateAndTime.Now;
			} else {
				DR = DT.Rows[0];
			}

			DR["R_Year"] = R_Year;
			DR["R_Round"] = R_Round;
			DR["PNDP_DEPT_CODE1"] = "00000000";
			DR["PNDP_MINOR_CODE2"] = "00";
			DR["CP_ID"] = 0;
			//----------------- ครั้งที่แก้ไข  Fixed ไปก่อน -------------------
			DR["CP_Start"] = dtp_KPI_CP_Start.Value;
			DR["CP_End"] = dtp_KPI_CP_End.Value;
			DR["Update_By"] = Session["USER_PSNL_NO"];
			DR["Update_Time"] = DateAndTime.Now;
			if (DT.Rows.Count == 0)
				DT.Rows.Add(DR);
			try {
				cmd = new SqlCommandBuilder(DA);
				DA.Update(DT);
			} catch (Exception ex) {
				ScriptManager.RegisterStartupScript(this.Page, typeof(string), "alert1", "showAlert('ไม่สามารถบันทึกได้');", true);
				ScriptManager.RegisterStartupScript(this.Page, typeof(string), "alert2", "showAlert('" + ex.Message.Replace("'", "\"") + "');", true);
				return;
			}

			//------------------- Save KPI Assessment Period -------------------
			DT = new DataTable();
			SQL = "SELECT * FROM tb_HR_KPI_Assessment_Period WHERE R_Year=" + R_Year + " AND R_Round=" + R_Round + "\n";
			SQL += " AND PNDP_DEPT_CODE1='00000000' AND PNDP_MINOR_CODE2='00'";
			DA = new SqlDataAdapter(SQL, BL.ConnectionString());
			DA.Fill(DT);

			if (DT.Rows.Count == 0) {
				DR = DT.NewRow();
				DR["Create_By"] = Session["USER_PSNL_NO"];
				DR["Create_Time"] = DateAndTime.Now;
			} else {
				DR = DT.Rows[0];
			}
			DR["R_Year"] = R_Year;
			DR["R_Round"] = R_Round;
			DR["PNDP_DEPT_CODE1"] = "00000000";
			DR["PNDP_MINOR_CODE2"] = "00";
			DR["AP_ID"] = 0;
			//----------------- ครั้งที่แก้ไข  Fixed ไปก่อน -------------------
			DR["AP_Start"] = dtp_KPI_AP_Start.Value;
			DR["AP_End"] = dtp_KPI_AP_End.Value;
			DR["MGR_Start"] = dtp_KPI_MGR_Start.Value;
			DR["MGR_End"] = dtp_KPI_MGR_End.Value;
			DR["Update_By"] = Session["USER_PSNL_NO"];
			DR["Update_Time"] = DateAndTime.Now;
			if (DT.Rows.Count == 0)
				DT.Rows.Add(DR);
			try {
				cmd = new SqlCommandBuilder(DA);
				DA.Update(DT);
			} catch (Exception ex) {
				ScriptManager.RegisterStartupScript(this.Page, typeof(string), "alert1", "showAlert('ไม่สามารถบันทึกได้');", true);
				ScriptManager.RegisterStartupScript(this.Page, typeof(string), "alert2", "showAlert('" + ex.Message.Replace("'", "\"") + "');", true);
				return;
			}

			//------------------- Save COMP Create Period ------------------
			DT = new DataTable();
			SQL = "SELECT * FROM tb_HR_COMP_Create_Period WHERE R_Year=" + R_Year + " AND R_Round=" + R_Round + "\n";
			SQL += " AND PNDP_DEPT_CODE1='00000000' AND PNDP_MINOR_CODE2='00'";
			DA = new SqlDataAdapter(SQL, BL.ConnectionString());
			DA.Fill(DT);

			if (DT.Rows.Count == 0) {
				DR = DT.NewRow();
				DR["Create_By"] = Session["USER_PSNL_NO"];
				DR["Create_Time"] = DateAndTime.Now;
			} else {
				DR = DT.Rows[0];
			}

			DR["R_Year"] = R_Year;
			DR["R_Round"] = R_Round;
			DR["PNDP_DEPT_CODE1"] = "00000000";
			DR["PNDP_MINOR_CODE2"] = "00";
			DR["CP_ID"] = 0;
			//----------------- ครั้งที่แก้ไข  Fixed ไปก่อน -------------------
			DR["CP_Start"] = dtp_COMP_CP_Start.Value;
			DR["CP_End"] = dtp_COMP_CP_End.Value;
			DR["Update_By"] = Session["USER_PSNL_NO"];
			DR["Update_Time"] = DateAndTime.Now;
			if (DT.Rows.Count == 0)
				DT.Rows.Add(DR);
			try {
				cmd = new SqlCommandBuilder(DA);
				DA.Update(DT);
			} catch (Exception ex) {
				ScriptManager.RegisterStartupScript(this.Page, typeof(string), "alert1", "showAlert('ไม่สามารถบันทึกได้');", true);
				ScriptManager.RegisterStartupScript(this.Page, typeof(string), "alert2", "showAlert('" + ex.Message.Replace("'", "\"") + "');", true);
				return;
			}

			//------------------- Save COMP Assessment Period ------------------
			DT = new DataTable();
			SQL = "SELECT * FROM tb_HR_COMP_Assessment_Period WHERE R_Year=" + R_Year + " AND R_Round=" + R_Round + "\n";
			SQL += " AND PNDP_DEPT_CODE1='00000000' AND PNDP_MINOR_CODE2='00'";
			DA = new SqlDataAdapter(SQL, BL.ConnectionString());
			DA.Fill(DT);

			if (DT.Rows.Count == 0) {
				DR = DT.NewRow();
				DR["Create_By"] = Session["USER_PSNL_NO"];
				DR["Create_Time"] = DateAndTime.Now;
			} else {
				DR = DT.Rows[0];
			}
			DR["R_Year"] = R_Year;
			DR["R_Round"] = R_Round;
			DR["PNDP_DEPT_CODE1"] = "00000000";
			DR["PNDP_MINOR_CODE2"] = "00";
			DR["AP_ID"] = 0;
			//----------------- ครั้งที่แก้ไข  Fixed ไปก่อน -------------------
			DR["AP_Start"] = dtp_COMP_AP_Start.Value;
			DR["AP_End"] = dtp_COMP_AP_End.Value;
			DR["MGR_Start"] = dtp_COMP_MGR_Start.Value;
			DR["MGR_End"] = dtp_COMP_MGR_End.Value;
			DR["Update_By"] = Session["USER_PSNL_NO"];
			DR["Update_Time"] = DateAndTime.Now;
			if (DT.Rows.Count == 0)
				DT.Rows.Add(DR);
			try {
				cmd = new SqlCommandBuilder(DA);
				DA.Update(DT);
			} catch (Exception ex) {
				ScriptManager.RegisterStartupScript(this.Page, typeof(string), "alert1", "showAlert('ไม่สามารถบันทึกได้');", true);
				ScriptManager.RegisterStartupScript(this.Page, typeof(string), "alert2", "showAlert('" + ex.Message.Replace("'", "\"") + "');", true);
				return;
			}

			//----------------Do this only case add new round not for  editing----------------
			if (lblEditMode.Text == "สร้าง") {
				//----------------- Set Default From Previous Round---------------
				SQL = "SELECT TOP 1 * FROM tb_HR_Round \n";
				SQL += "WHERE CAST(R_Year AS varchar)+'-'+CASE WHEN R_Round<10 THEN '0' ELSE '' END+CAST(R_Round AS varchar) <'" + R_Year + "-" + R_Round.ToString().PadLeft(2, GL.chr0) + "'\n";
				SQL += "ORDER BY R_Year DESC,R_Round DESC";
				DT = new DataTable();
				DA = new SqlDataAdapter(SQL, BL.ConnectionString());
				DA.Fill(DT);

				if (DT.Rows.Count > 0) {
					int LastYear =GL.CINT( DT.Rows[0]["R_Year"].ToString());
                    int LastRound = GL.CINT(DT.Rows[0]["R_Round"].ToString());

					SqlConnection Conn = new SqlConnection(BL.ConnectionString());
					Conn.Open();
					SqlCommand Comm = new SqlCommand();
					var _with2 = Comm;
					_with2.Connection = Conn;
					_with2.CommandType = CommandType.Text;
					_with2.CommandText = "EXEC dbo.sp_Clone_Detail_From_Last_Round " + R_Year + "," + R_Round + "," + LastYear + "," + LastRound + ",'" + Session["USER_PSNL_NO"] + "'";
                    _with2.CommandTimeout = 100;
                    _with2.ExecuteNonQuery();
					_with2.Dispose();
					Conn.Close();
				}
			}


			ScriptManager.RegisterStartupScript(this.Page, typeof(string), "alert", "showAlert('บันทึกสำเร็จ');", true);

			BindList();
			pnlEdit.Visible = false;
			pnlList.Visible = true;

		}



		private DataTable CurrentSelectedClass()
		{
			DataTable DT = new DataTable();
			DT.Columns.Add("Level", typeof(int));
			DT.Columns.Add("Selected", typeof(bool));

			foreach (RepeaterItem Item in rptLevel.Items) {
				if (Item.ItemType != ListItemType.Item & Item.ItemType != ListItemType.AlternatingItem)
					continue;
				CheckBox chkLevel =(CheckBox) Item.FindControl("chkLevel");
				DataRow DR = DT.NewRow();
				DR["Level"] = chkLevel.Attributes["Level"];
				DR["Selected"] = chkLevel.Checked;
				DT.Rows.Add(DR);
			}
			return DT;
		}

		private DataTable CurrentSelectedType()
		{

			DataTable DT = new DataTable();
			DT.Columns.Add("PSNL_TYPE_Code", typeof(string));
			DT.Columns.Add("PSNL_TYPE_Name", typeof(string));
			DT.Columns.Add("Selected", typeof(bool));

			foreach (RepeaterItem Item in rptType.Items) {
				if (Item.ItemType != ListItemType.Item & Item.ItemType != ListItemType.AlternatingItem)
					continue;
				CheckBox chkType =(CheckBox) Item.FindControl("chkType");
                Label lbl = (Label)Item.FindControl("lbl");
				DataRow DR = DT.NewRow();
				DR["PSNL_TYPE_Code"] = chkType.Attributes["PSNL_TYPE_Code"];
				DR["PSNL_TYPE_Name"] = lbl.Text;
				DR["Selected"] = chkType.Checked;
				DT.Rows.Add(DR);
			}
			return DT;
		}
		public AssessmentSetting_Round()
		{
			Load += Page_Load;
		}



	}
}

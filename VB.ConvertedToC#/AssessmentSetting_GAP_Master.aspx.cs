using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Xml.Linq;
using System.Diagnostics;
using System.Collections.Specialized;
using System.Configuration;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Caching;
using System.Web.SessionState;
using System.Web.Security;
using System.Web.Profile;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
namespace VB
{

	public partial class AssessmentSetting_GAP_Master : System.Web.UI.Page
	{

		HRBL BL = new HRBL();

		GenericLib GL = new GenericLib();
		public int GAP_ID {
			get {
				try {
					return Convert.ToInt32(txtCode.Attributes["GAP_ID"]);
				} catch (Exception ex) {
					return 0;
				}
			}
			set {
				txtCode.Attributes["GAP_ID"] = value.ToString();
				if (Information.IsNumeric(value) && value > 0) {
					txtCode.Text = value.ToString().PadLeft(3, GL.chr0);
				} else {
					txtCode.Text = "อัตโนมัติ";
				}
			}
		}

		public string GAP_Name {
			get { return txtName.Text; }
			set { txtName.Text = value.ToString (); }
		}

		public string GAP_Desc {
			get { return txtDesc.Text; }
			set { txtDesc.Text = value.ToString (); }
		}

		public bool IsActive {
			get { return btnActive.ImageUrl == "images/check.png"; }
			set {
				switch (value) {
					case true:
						btnActive.ImageUrl = "images/check.png";
						break;
					case false:
						btnActive.ImageUrl = "images/none.png";
						break;
				}
			}
		}

		public string EXIST_Training {
			get { return lblTrainingNo.Attributes["EXIST_Training"]; }
			set { lblTrainingNo.Attributes["EXIST_Training"] = value; }
		}


		protected void Page_Load(object sender, System.EventArgs e)
		{
			if ((Session["USER_PSNL_NO"] == null)) {
				ScriptManager.RegisterStartupScript(this.Page, typeof(string), "Alert", "showAlert('กรุณาลงชื่อเข้าใช้ระบบ');", true);
				Response.Redirect("Login.aspx");
				return;
			}

			if (!IsPostBack) {
				BindGAPList();
				pnlList.Visible = true;
				pnlModal.Visible = false;
				ModalTraining.Visible = false;
			}
		}

		private void BindGAPList()
		{
			string SQL = "";
			SQL += " SELECT GAP.GAP_ID,GAP_Name,GAP_Desc,Is_Active,\n";
			SQL += " SUM(CASE COMP_Type_ID WHEN 1 THEN 1 ELSE 0 END) Core,\n";
			SQL += " SUM(CASE COMP_Type_ID WHEN 2 THEN 1 ELSE 0 END) FN,\n";
			SQL += " SUM(CASE COMP_Type_ID WHEN 3 THEN 1 ELSE 0 END) MGR\n";
			SQL += " FROM vw_HR_GAP GAP\n";
			SQL += " LEFT JOIN\n";
			SQL += " \t\t(\n";
			SQL += " \t\t\tSELECT GAP_ID,1 COMP_Type_ID FROM tb_HR_COMP_Master_Core_GAP\n";
			SQL += " \t\t\tUNION ALL \n";
			SQL += " \t\t\tSELECT GAP_ID,2 COMP_Type_ID FROM tb_HR_COMP_Master_FN_GAP\n";
			SQL += " \t\t\tUNION ALL\n";
			SQL += " \t\t\tSELECT GAP_ID,3 COMP_Type_ID FROM tb_HR_COMP_Master_MGR_GAP\n";
			SQL += " \t\t) COMP ON GAP.GAP_ID=COMP.GAP_ID\n";

			string Filter = "";
			if (!string.IsNullOrEmpty(txtSearchName.Text)) {
				Filter += " (GAP_Name LIKE '%" + txtSearchName.Text.Replace("'", "''") + "%' OR \n";
				Filter += " GAP_Desc LIKE '%" + txtSearchName.Text.Replace("'", "''") + "%' OR ";
				Filter += " GAP.GAP_ID LIKE '%" + txtSearchName.Text.Replace("'", "''") + "%') AND ";
			}
			switch (ddlSearchStatus.SelectedIndex) {
				case 1:
					Filter += " ISNULL(Is_Active,0)=1 AND ";
					break;
				case 2:
					Filter += " ISNULL(Is_Active,0)=0 AND ";
					break;
			}
			if (!string.IsNullOrEmpty(Filter)) {
				SQL += " WHERE " + Filter.Substring(0, Filter.Length - 4) + "\n";
			}
			SQL += " \n";
			SQL += " GROUP BY GAP.GAP_ID,GAP_Name,GAP_Desc,Is_Active\n";
			SQL += " ORDER BY GAP.GAP_ID\n";

			SqlDataAdapter DA = new SqlDataAdapter(SQL, BL.ConnectionString());
			DataTable DT = new DataTable();
			DA.Fill(DT);

			DT.Columns.Add("GAP_Order");
			for (int i = 0; i <= DT.Rows.Count - 1; i++) {
				DT.Rows[i]["GAP_Order"] = i + 1;
			}

			Session["AssessmentSetting_GAP_Master"] = DT;
			Pager.SesssionSourceName = "AssessmentSetting_GAP_Master";
			Pager.RenderLayout();

			if (DT.Rows.Count == 0) {
				lblCountGAP.Text = "ไม่พบรายการดังกล่าว";
			} else {
				lblCountGAP.Text = "พบ " + GL.StringFormatNumber(DT.Rows.Count, 0) + " รายการ";
			}

		}

		protected void Pager_PageChanging(PageNavigation Sender)
		{
			Pager.TheRepeater = rptGAP;
		}

		protected void Search(object sender, System.EventArgs e)
		{
			BindGAPList();
		}

		protected void rptGAP_ItemCommand(object source, System.Web.UI.WebControls.RepeaterCommandEventArgs e)
		{
            DataTable DT;
            SqlDataAdapter DA;
            SqlConnection Conn;
            SqlCommand Comm;
            string Sql = "";

			switch (e.CommandName) {
				case "Active":
					Sql = "UPDATE tb_HR_GAP SET Is_Active=CASE Is_Active WHEN 1 THEN 0 ELSE 1 END WHERE GAP_ID=" + e.CommandArgument;
					 Conn = new SqlConnection(BL.ConnectionString());
					Conn.Open();
					 Comm = new SqlCommand();
					var _with1 = Comm;
					_with1.Connection = Conn;
					_with1.CommandType = CommandType.Text;
					_with1.CommandText = Sql;
					_with1.ExecuteNonQuery();
					_with1.Dispose();
					Conn.Close();
					BindGAPList();
					break;
				case "Edit":

                    Sql = "SELECT * FROM vw_HR_GAP WHERE GAP_ID=" + e.CommandArgument;
					 DT = new DataTable();
                     DA = new SqlDataAdapter(Sql, BL.ConnectionString());
					DA.Fill(DT);
					if (DT.Rows.Count == 0) {
						ScriptManager.RegisterStartupScript(this.Page, typeof(string), "Alert", "showAlert('กำลังปรับปรุงข้อมูลล่าสุด');", true);
						BindGAPList();
						return;
					}
					GAP_ID = GL.CINT ( DT.Rows[0]["GAP_ID"]);
					GAP_Name = DT.Rows[0]["GAP_Name"].ToString ();
					GAP_Desc = DT.Rows[0]["GAP_Desc"].ToString ();
					IsActive = (bool )DT.Rows[0]["Is_Active"];
					pnlModal.Visible = true;

					break;
				case "Delete":
					 Sql = "DELETE FROM tb_HR_GAP WHERE GAP_ID=" + e.CommandArgument;
					 Conn = new SqlConnection(BL.ConnectionString());
					Conn.Open();
					 Comm = new SqlCommand();
					var _with2 = Comm;
					_with2.Connection = Conn;
					_with2.CommandType = CommandType.Text;
					_with2.CommandText = Sql;
					_with2.ExecuteNonQuery();
					_with2.Dispose();
					Conn.Close();
					BindGAPList();
					break;
			}
		}

		protected void rptGAP_ItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
		{
			if (e.Item.ItemType != ListItemType.AlternatingItem & e.Item.ItemType != ListItemType.Item)
				return;

			Label lblNo =(Label) e.Item.FindControl("lblNo");
			Label lblName =(Label) e.Item.FindControl("lblName");
			Label lblCore =(Label) e.Item.FindControl("lblCore");
			Label lblFN =(Label) e.Item.FindControl("lblFN");
			Label lblMGR =(Label) e.Item.FindControl("lblMGR");
			ImageButton btnToggle =(ImageButton) e.Item.FindControl("btnToggle");
			Button btnGAPEdit =(Button) e.Item.FindControl("btnGAPEdit");
			Button btnGAPDelete =(Button) e.Item.FindControl("btnGAPDelete");
			AjaxControlToolkit.ConfirmButtonExtender cfm_Delete =(AjaxControlToolkit.ConfirmButtonExtender) e.Item.FindControl("cfm_Delete");

            DataRowView drv = (DataRowView)e.Item.DataItem;
			lblNo.Text = drv["GAP_ID"].ToString().PadLeft(3, GL.chr0);
			lblName.Text = drv["GAP_Name"].ToString();

			if (!GL.IsEqualNull(drv["Core"]) && GL.CINT (drv["Core"]) > 0) {
				lblCore.Text = GL.StringFormatNumber(drv["Core"], 0);
				lblCore.Font.Bold = true;
			} else {
				lblCore.Text = "-";
			}

			if (!GL.IsEqualNull(drv["FN"]) && GL.CINT (drv["FN"]) > 0) {
				lblFN.Text = GL.StringFormatNumber(drv["FN"], 0);
				lblFN.Font.Bold = true;
			} else {
				lblFN.Text = "-";
			}

			if (!GL.IsEqualNull(drv["MGR"]) &&  GL.CINT (drv["MGR"]) > 0) {
				lblMGR.Text = GL.StringFormatNumber(drv["MGR"], 0);
				lblMGR.Font.Bold = true;
			} else {
				lblMGR.Text = "-";
			}

			if ((bool )drv["Is_Active"]) {
				btnToggle.ImageUrl = "images/check.png";
			} else {
				btnToggle.ImageUrl = "images/none.png";
			}

			btnToggle.CommandArgument = drv["GAP_ID"].ToString ();
			btnGAPEdit.CommandArgument = drv["GAP_ID"].ToString ();
			btnGAPDelete.CommandArgument =  drv["GAP_ID"].ToString ();

			cfm_Delete.ConfirmText = "ยืนยันลบหลักสูตร : " + drv["GAP_Name"].ToString();

		}


		//------------------------COURSE FRO ORACLE----------------------------
		#region "TrainingDialog"

		protected void rptTrainingDialog_ItemCommand(object source, System.Web.UI.WebControls.RepeaterCommandEventArgs e)
		{
            Button btnTrainingSelect =(Button) e.Item.FindControl("btnTrainingSelect");
			switch (e.CommandName) {
				case "Select":
					
					Label lblName =(Label) e.Item.FindControl("lblName");
					string COURSE_ID = btnTrainingSelect.CommandArgument;


					string SQL = "";
                    SQL = "SELECT * FROM tb_HR_GAP WHERE GAP_ID='" + COURSE_ID + "'";
					DataTable DT = new DataTable();
					SqlDataAdapter DA = new SqlDataAdapter(SQL, BL.ConnectionString());
					DA.Fill(DT);

					DataRow DR = null;
					if (DT.Rows.Count == 0) {
						//GAP_ID = COURSE_ID
						DR = DT.NewRow();
						DR["GAP_ID"] = COURSE_ID;
						DR["Create_By"] = Session["USER_PSNL_NO"];
						DR["Create_Time"] = DateAndTime.Now;
					} else {
						DR = DT.Rows[0];
					}

					DR["GAP_Name"] = lblName.Text;
					DR["GAP_Desc"] = "";
					DR["Is_Active"] = IsActive;
					DR["Update_By"] = Session["USER_PSNL_NO"];
					DR["Update_Time"] = DateAndTime.Now;
					if (DT.Rows.Count == 0)
						DT.Rows.Add(DR);

					SqlCommandBuilder cmd = new SqlCommandBuilder();
					cmd = new SqlCommandBuilder(DA);
					DA.Update(DT);

					//-----bind ตารางกำหนดหลักสูตร------
					BindGAPList();
					ModalTraining.Visible = false;
					break;
			}
		}

		protected void rptTrainingDialog_ItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
		{
			if (e.Item.ItemType != ListItemType.AlternatingItem & e.Item.ItemType != ListItemType.Item)
				return;
			Label lblNo =(Label) e.Item.FindControl("lblNo");
			Label lblName =(Label) e.Item.FindControl("lblName");
			Button btnTrainingSelect =(Button) e.Item.FindControl("btnTrainingSelect");

            DataRowView drv = (DataRowView)e.Item.DataItem;
			lblNo.Text = drv["COURSE_ID"].ToString().PadLeft(3, GL.chr0);
			lblName.Text = drv["COURSE_DESC"].ToString().ToString();
			btnTrainingSelect.CommandArgument = drv["COURSE_ID"].ToString ();
		}

		protected void SearchTraining(object sender, System.EventArgs e)
		{
			BindTrainingList();
		}

		private void BindTrainingList()
		{
			string SQL = "";
			SQL += " SELECT * FROM tb_PK_COURSE\n";
			string Filter = "";
			if (!string.IsNullOrEmpty(txtSearchTraining.Text)) {
				Filter += " (COURSE_ID LIKE '%" + txtSearchTraining.Text.Replace("'", "''") + "%' OR \n";
				Filter += " COURSE_DESC LIKE '%" + txtSearchTraining.Text.Replace("'", "''") + "%') AND ";
			}

			if (!string.IsNullOrEmpty(EXIST_Training)) {
				Filter += " COURSE_ID NOT IN (" + EXIST_Training + ") AND ";
			}

			if (!string.IsNullOrEmpty(Filter)) {
				SQL += " WHERE " + Filter.Substring(0, Filter.Length - 4) + "\n";
			}
			SQL += " \n";

			SQL += " ORDER BY COURSE_ID,COURSE_DESC\n";

			SqlDataAdapter DA = new SqlDataAdapter(SQL, BL.ConnectionString());
			DataTable DT = new DataTable();
			DA.Fill(DT);

			Session["CP_Setting_PropertyTraining"] = DT;
			Pager_Training.SesssionSourceName = "CP_Setting_PropertyTraining";
			Pager_Training.RenderLayout();

			if (DT.Rows.Count == 0) {
				lblCountTraining.Text = "ไม่พบรายการดังกล่าว";
			} else {
				lblCountTraining.Text = "พบ " + GL.StringFormatNumber(DT.Rows.Count, 0) + " รายการ";
			}

		}

		protected void Pager_Training_PageChanging(PageNavigation Sender)
		{
			Pager_Training.TheRepeater = rptTrainingDialog;
		}

		protected void btnClose_Training_Click(object sender, System.EventArgs e)
		{
			ModalTraining.Visible = false;
		}

		#endregion







		protected void btnAdd_Click(object sender, System.EventArgs e)
		{
			string SQL = "";
			SQL += " SELECT *\n";
			SQL += " FROM vw_HR_GAP \n";
			DataTable DT = new DataTable();
			SqlDataAdapter DA = new SqlDataAdapter(SQL, BL.ConnectionString());
			DA.Fill(DT);

			//------------- Get Existing Training List ---------------
			string _Traininglist = "";
			if (DT.Rows.Count > 0) {
				for (int i = 0; i <= DT.Rows.Count - 1; i++) {
					_Traininglist += "'" + DT.Rows[i]["GAP_ID"] + "'" + ",";
				}
			}

			if (!string.IsNullOrEmpty(_Traininglist)) {
				EXIST_Training = _Traininglist.Substring(0, _Traininglist.Length - 1);
			} else {
				EXIST_Training = "";
			}

			BindTrainingList();
			ModalTraining.Visible = true;
			ScriptManager.RegisterStartupScript(this.Page, typeof(string), "Focus", "document.getElementById('" + txtSearchTraining.ClientID + "').focus();", true);




			//GAP_ID = 0
			//GAP_Name = ""
			//GAP_Desc = ""
			//IsActive = True
			//pnlModal.Visible = True
			//ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Focus", "document.getElementById('" & txtName.ClientID & "').focus();", True)
			//txtName.Focus()
		}

		protected void btnClose_Click(object sender, System.EventArgs e)
		{
			pnlModal.Visible = false;
		}

		protected void btnActive_Click(object sender, System.Web.UI.ImageClickEventArgs e)
		{
			IsActive = !IsActive;
		}

		protected void btnOK_Click(object sender, System.EventArgs e)
		{
			if (string.IsNullOrEmpty(GAP_Name)) {
				ScriptManager.RegisterStartupScript(this.Page, typeof(string), "Alert", "showAlert('กรอกชื่อหลักสูตร');", true);
				return;
			}

			string SQL = "SELECT * FROM vw_HR_GAP WHERE GAP_Name='" + GAP_Name.Replace("'", "''") + "' AND GAP_ID<>" + GAP_ID;
			DataTable DT = new DataTable();
			SqlDataAdapter DA = new SqlDataAdapter(SQL, BL.ConnectionString());
			DA.Fill(DT);

			if (DT.Rows.Count > 0) {
				ScriptManager.RegisterStartupScript(this.Page, typeof(string), "Alert", "showAlert('ชื่อหลักสูตรนี้มีอยู่แล้ว');", true);
				return;
			}

            SQL = "SELECT * FROM tb_HR_GAP WHERE GAP_ID=" + GAP_ID;
			DT = new DataTable();
			DA = new SqlDataAdapter(SQL, BL.ConnectionString());
			DA.Fill(DT);

			DataRow DR = null;
			if (DT.Rows.Count == 0) {
				GAP_ID = BL.GetNewGAPID();
				DR = DT.NewRow();
				DR["GAP_ID"] = GAP_ID;
				DR["Create_By"] = Session["USER_PSNL_NO"];
				DR["Create_Time"] = DateAndTime.Now;
			} else {
				DR = DT.Rows[0];
			}

			DR["GAP_Name"] = GAP_Name;
			DR["GAP_Desc"] = GAP_Desc;
			DR["Is_Active"] = IsActive;
			DR["Update_By"] = Session["USER_PSNL_NO"];
			DR["Update_Time"] = DateAndTime.Now;
			if (DT.Rows.Count == 0)
				DT.Rows.Add(DR);

			SqlCommandBuilder cmd = new SqlCommandBuilder();
			cmd = new SqlCommandBuilder(DA);
			DA.Update(DT);
			//ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "showAlert('บันทึกสำเร็จ');", True)
			BindGAPList();
			pnlModal.Visible = false;

		}
		public AssessmentSetting_GAP_Master()
		{
			Load += Page_Load;
		}
	}
}

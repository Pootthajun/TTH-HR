﻿<%@ Page Title="" Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPage.Master" CodeBehind="RPT_K_DeptRANK.aspx.cs" Inherits="VB.RPT_K_DeptRANK" %>

<%@ Register src="WUC_DateReporter.ascx" tagname="WUC_DateReporter" tagprefix="uc1" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<%@ Register src="PageNavigation.ascx" tagname="PageNavigation" tagprefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<asp:ToolkitScriptManager ID="toolkit1" runat="server"></asp:ToolkitScriptManager>
<asp:UpdatePanel ID="UpdatePanel1" runat="server">      
<ContentTemplate>


<div class="container-fluid">
				<!-- BEGIN PAGE HEADER-->
				<div class="row-fluid" id="fixedPageHeader">
					<div class="span12"> 
						<!-- BEGIN PAGE TITLE & BREADCRUMB-->
						<h3>ผลการประเมินตัวชี้วัดตามช่วงคะแนน (KPI)<font color="blue"> (ScreenID : R-KPI-07)</font></h3>						
						<ul class="breadcrumb">
                            <li>
                            	<i class="icon-copy"></i> <a href="javascript:;">รายงาน</a><i class="icon-angle-right"></i>
                            </li>
                            <li>
                            	<i class="icon-copy"></i> <a href="javascript:;">การประเมินผลตัวชี้วัด</a><i class="icon-angle-right"></i>
                            </li>
                            <li>
                            	<i class="icon-copy"></i> <a href="javascript:;">รายงานประจำรอบ</a><i class="icon-angle-right"></i>
                            </li>
                        	<li>
                        	    <i class="icon-check"></i> <a href="javascript:;">ผลการประเมินตามช่วงคะแนน</a>
                        	</li>                      	
                        	
                            <uc1:WUC_DateReporter ID="WUC_DateReporter1" runat="server" />
                        </ul>
						<!-- END PAGE TITLE & BREADCRUMB-->
									        
				     </div>

				
			    </div>
		        <!-- END PAGE HEADER-->
				
                <asp:Panel ID="pnlList" runat="server" Visible="True" DefaultButton="btnSearch">
				<div class="row-fluid">
					
					<div class="span12">
						<!-- BEGIN SAMPLE TABLE PORTLET-->
                                        <div class="row-fluid form-horizontal">
                			                  <div class="btn-group pull-left">
                                                    <asp:Button ID="btnSearch" OnClick="btnSearch_Click"  runat="server" CssClass="btn green" Text="ค้นหา" />
                                              </div> 
				                            <div class="btn-group pull-right">                                    
								                <button data-toggle="dropdown" class="btn dropdown-toggle" id="Button1">พิมพ์ <i class="icon-angle-down"></i></button>										
								                <ul class="dropdown-menu pull-right">                                                       
                                                    <li><a href="Print/RPT_K_DeptRANK.aspx?Mode=PDF" target="_blank">รูปแบบ PDF</a></li>
                                                    <li><a href="Print/RPT_K_DeptRANK.aspx?Mode=EXCEL" target="_blank">รูปแบบ Excel</a></li>											
										            </ul>
							                </div>
									     </div>      
						                 <div class="row-fluid form-horizontal">
						                             <div class="span4 ">
														<div class="control-group">
													        <label class="control-label"> รอบการประเมิน</label>
													        <div class="controls">
														        <asp:DropDownList ID="ddlRound" runat="server" CssClass="medium m-wrap">
							                                    </asp:DropDownList>
													        </div>
												        </div>
													</div>
													
													  <div class="span4 ">
														<div class="control-group">
													        <label class="control-label"> ชื่อ</label>
													        <div class="controls">
														        <asp:TextBox ID="txtName" runat="server" CssClass="m-wrap medium" placeholder="ค้นหาจากชื่อ/เลขประจำตัว"></asp:TextBox>
													        </div>
												        </div>
													</div>						   
												</div>
												
												<div class="row-fluid form-horizontal">
											         <div class="span4 ">
												        <div class="control-group">
											                <label class="control-label"> ฝ่าย</label>
											                <div class="controls">
												                <asp:DropDownList ID="ddlSector" OnSelectedIndexChanged="ddlSector_SelectedIndexChanged" runat="server" AutoPostBack="true" CssClass="medium m-wrap">
												                </asp:DropDownList>
											                </div>
										                </div>
											        </div>	
    												
											         <div class="span3 ">
												        <div class="control-group">
											                <label class="control-label"> หน่วยงาน</label>
											                <div class="controls">
												                <asp:DropDownList ID="ddlDept" runat="server" CssClass="medium m-wrap">
												                </asp:DropDownList>		
											                </div>
										                </div>
											        </div>		
											     </div>
											     
											     
									     
								            
							<div class="portlet-body no-more-tables">
								<asp:Label ID="lblCountPSN" runat="server" Font-Size="14px" Font-Bold="true"></asp:Label>
								<table class="table table-full-width table-bordered dataTable table-hover">
									<thead>
										<tr>
											<th style="text-align:center;"> หน่วยงาน</th>
											<th style="text-align:center;"> ระดับคะแนน</th>
											<th style="text-align:center;"> ช่วง</th>
											<th style="text-align:center;"> คะแนนที่ได้</th>
											<th style="text-align:center;"> เลขประจำตัว</th>
											<th style="text-align:center;"> ชื่อ</th>
											<th style="text-align:center;"> ตำแหน่ง</th>
											<th style="text-align:center; display:none;"> ระดับ</th>											
											<th style="text-align:center;"> พิมพ์</th>											
										</tr>
									</thead>
									<tbody>
										<asp:Repeater ID="rptKPI" OnItemDataBound="rptKPI_ItemDataBound" runat="server">
									        <ItemTemplate>
									            <tr>    
									                <td data-title="หน่วยงาน" id="td1" runat="server"><asp:Label ID="lblPSNOrganize" runat="server"></asp:Label></td>
									                <td data-title="ระดับคะแนน" id="td2" runat="server"><asp:Label ID="lblRankName" runat="server"></asp:Label></td>
									                <td data-title="ช่วง" id="td3" runat="server"><asp:Label ID="lblRankNo" runat="server"></asp:Label></td>
											        <td data-title="คะแนนที่ได้" id="td4" runat="server" style="text-align:center;"><asp:Label ID="lblScore" runat="server"></asp:Label></td>
											        <td data-title="เลขประจำตัว" id="td5" runat="server" style="text-align:center;"><asp:Label ID="lblPSNNo" runat="server"></asp:Label></td>
											        <td data-title="ชื่อ" id="td6" runat="server"><asp:Label ID="lblPSNName" runat="server"></asp:Label></td>
											        <td data-title="ตำแหน่ง" id="td7" runat="server"><asp:Label ID="lblPSNPos" runat="server"></asp:Label></td>
											        <td data-title="ระดับ" id="td8" runat="server" style="text-align:center; display:none;"><asp:Label ID="lblPSNClass" runat="server"></asp:Label></td>
											        <td data-title="พิมพ์ " id="td9" runat="server" style="text-align:center; ">   
                                                       <a class="btn mini blue" id="btnPrint" runat="server" title="พิมพ์" style="cursor:pointer;" target="_blank"><i class="icon-print"></i></a> 
                                                    </td>
										        </tr>	
									        </ItemTemplate>
										</asp:Repeater>
																							
									</tbody>
								</table>
								<asp:PageNavigation ID="Pager" OnPageChanging="Pager_PageChanging" MaximunPageCount="10" PageSize="20" runat="server" />
							</div>
					</div>
                </div>  
                </asp:Panel>  
</div>

</ContentTemplate>           
</asp:UpdatePanel>	
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ScriptPlaceHolder" Runat="Server">
</asp:Content>


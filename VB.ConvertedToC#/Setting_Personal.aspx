﻿<%@ Page  Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPage.Master" CodeBehind="Setting_Personal.aspx.cs" Inherits="VB.Setting_Personal" %>

<%@ Register src="WUC_DateReporter.ascx" tagname="WUC_DateReporter" tagprefix="uc1" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<%@ Register src="PageNavigation.ascx" tagname="PageNavigation" tagprefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server"></asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<asp:ToolkitScriptManager ID="toolkit1" runat="server"></asp:ToolkitScriptManager>
<asp:UpdatePanel ID="UpdatePanel1" runat="server">				           
<ContentTemplate>


<div class="container-fluid">
				<!-- BEGIN PAGE HEADER-->
				<div class="row-fluid" id="fixedPageHeader">
					<div class="span12"> 
						<!-- BEGIN PAGE TITLE & BREADCRUMB-->
						<h3 class="page-user-md">บุคลากร (ข้อมูลจาก Oracle) <font color="blue">(ScreenID : S-BAS-04)</font></h3>					
									
						<ul class="breadcrumb">
                            <li>
                            	<i class="icon-cogs"></i><a href="javascript:;">จัดการระบบ/ข้อมูลพื้นฐาน</a><i class="icon-angle-right"></i>
                            </li>
                        	<li><i class="icon-sitemap"></i> <a href="javascript:;">บุคลากร</a></li>
                        	
                            <uc1:WUC_DateReporter ID="WUC_DateReporter1" runat="server" />
                        </ul>
						<!-- END PAGE TITLE & BREADCRUMB-->
									        
				     </div>

				
			    </div>
		        <!-- END PAGE HEADER-->
				        <asp:Panel ID="pnlList" runat="server" CssClass="row-fluid form-horizontal"  DefaultButton="btnSearch">
				                    <div class="span4 ">
									    <div class="control-group">
								            <label class="control-label"><i class="icon-sitemap"></i> ฝ่าย</label>
								            <div class="controls">
								                <asp:DropDownList ID="ddlSector" OnSelectedIndexChanged="Search_Changed" runat="server" AutoPostBack="true" CssClass="medium m-wrap">
								                </asp:DropDownList>
								            </div>
							            </div>
								    </div>
								    <div class="span6 ">
									    <div class="control-group">
								            <label class="control-label"><i class="icon-sitemap"></i> ค้นหา</label>
								            <div class="controls">
									            <asp:TextBox  ID="txtFilter" OnTextChanged="Search_Changed" runat="server" AutoPostBack="true" CssClass="m-wrap large" placeholder="ชื่อ/เลขประจำตัว/ตำแหน่ง/หน่วยงาน"></asp:TextBox> 														       
								            </div>
							            </div>
								    </div>	
								    <asp:Button ID="btnSearch" OnClick="Search_Changed" runat="server" Text="" style="display:none;" />
	                    </asp:Panel>
				        <div class="portlet-body no-more-tables">
							    <asp:Label ID="lblCountPSN" runat="server" Font-Size="14px" Font-Bold="true"></asp:Label>
								<table class="table table-full-width table-advance dataTable no-more-tables table-hover">
									<thead>
										<tr>
											<th style="text-align:center;"><i class="icon-sitemap"></i> ฝ่าย</th>
											<th style="text-align:center;"><i class="icon-sitemap"></i> หน่วยงาน</th>
											<th style="text-align:center;"><i class="icon-user"></i> ชื่อ</th>
											<th style="text-align:center;"><i class="icon-bookmark"></i> ระดับ</th>
											<th style="text-align:center;"><i class="icon-briefcase"></i> ตำแหน่ง</th>
											<th style="text-align:center;"><i class="icon-list"></i> ประเภท</th>
											<th style="text-align:center;"><i class="icon-download-alt"></i>  ข้อมูลล่าสุด ณ วันที่</th>
										</tr>
									</thead>
									<tbody>
									    <asp:Repeater ID="rptPSN" OnItemDataBound="rptPSN_ItemDataBound" runat="server">
									        <ItemTemplate>
    									        <tr>                                        
											        <td data-title="ฝ่าย"><asp:Label ID="lblSector" runat="server"></asp:Label></td>
											        <td data-title="หน่วยงาน"><asp:Label ID="lblDept" runat="server"></asp:Label></td>
											        <td data-title="ชื่อ"><asp:Label ID="lblName" runat="server"></asp:Label></td>
											        <td data-title="ระดับ"><asp:Label ID="lblClass" runat="server"></asp:Label></td>
											        <td data-title="ตำแหน่ง"><asp:Label ID="lblPOS" runat="server"></asp:Label></td>
											        <td data-title="ประเภท"><asp:Label ID="lblType" runat="server"></asp:Label></td>											        
											        <td data-title="ข้อมูลล่าสุด ณ วันที่"><asp:Label ID="lblUpdate" runat="server"></asp:Label></td>
										        </tr>	
									        </ItemTemplate>
									    </asp:Repeater>
									</tbody>
								</table>
								
								<asp:PageNavigation ID="Pager" OnPageChanging="Pager_PageChanging" MaximunPageCount="10" PageSize="20" runat="server" />
								
							</div>
               
							 
</ContentTemplate>				                
</asp:UpdatePanel>		 
</asp:Content>


using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Xml.Linq;
using System.Diagnostics;
using System.Collections.Specialized;
using System.Configuration;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Caching;
using System.Web.SessionState;
using System.Web.Security;
using System.Web.Profile;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
namespace VB
{

	public partial class RPT_A_PSNAssStatus : System.Web.UI.Page
	{


		HRBL BL = new HRBL();
		GenericLib GL = new GenericLib();

		Converter C = new Converter();
		public int R_Year {
			get {
				try {
					return GL.CINT( GL.SplitString(ddlRound.Items[ddlRound.SelectedIndex].Value, "-")[0]);
				} catch (Exception ex) {
					return 0;
				}
			}
		}

		public int R_Round {
			get {
				try {
					return GL.CINT(GL.SplitString(ddlRound.Items[ddlRound.SelectedIndex].Value, "-")[1]);
				} catch (Exception ex) {
					return 0;
				}
			}
		}

		protected void Page_Load(object sender, System.EventArgs e)
		{
			if ((Session["USER_PSNL_NO"] == null)) {
				ScriptManager.RegisterStartupScript(this.Page, typeof(string), "Alert", "showAlert('กรุณาลงชื่อเข้าใช้ระบบ');", true);
				Response.Redirect("Login.aspx");
				return;
			}

			if (!IsPostBack) {
				//----------------- ทำสองที่ Page Load กับ Update Status ---------------
				BL.BindDDlYearRound(ddlRound);
				ddlRound.Items.RemoveAt(0);
				BL.BindDDlSector(ddlSector);
				BindPersonalList();
				//Else
				//    ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "InitApp", "App.initUniform();", True)
			}

			SetCheckboxStyle();
		}

		private void SetCheckboxStyle()
		{
			if (chkASSYes.Checked) {
				chkASSYes.CssClass = "checked";
			} else {
				chkASSYes.CssClass = "";
			}
			if (chkASSNo.Checked) {
				chkASSNo.CssClass = "checked";
			} else {
				chkASSNo.CssClass = "";
			}

			for (int i = 1; i <= 13; i++) {
                CheckBox chk = (CheckBox)pnlList.FindControl("chkClass" + i);
				if (chk.Checked) {
					chk.CssClass = "checked";
				} else {
					chk.CssClass = "";
				}
			}
		}

        protected void btnSearch_Click(object sender, System.EventArgs e)
		{
			BindPersonalList();
		}

		private void BindPersonalList()
		{
			string SQL = "";
			SQL += " SELECT KPI.R_Year,KPI.R_Round,KPI.SECTOR_CODE,KPI.SECTOR_NAME,KPI.DEPT_CODE,KPI.DEPT_NAME,KPI.PSNL_NO,KPI.PSNL_Fullname,KPI.POS_Name,KPI.PNPS_CLASS," + "\n";
            SQL += " ISNULL(KPI.Ass_Status,0) KPI_Status,ISNULL(COMP.Ass_Status,0) COMP_Status,\n";
            SQL += " ISNULL(KPI.RESULT,0) KPI_Result,ISNULL(COMP.RESULT,0) COMP_Result,R.Weight_KPI,R.Weight_COMP,\n";
			SQL += " ((ISNULL(KPI.RESULT,0)*R.Weight_KPI)+(ISNULL(COMP.RESULT,0)*R.Weight_COMP))/(R.Weight_KPI+R.Weight_COMP) Total_Result\n";
            SQL += " FROM vw_RPT_KPI_Result KPI \n";
			SQL += " LEFT JOIN vw_RPT_COMP_Result COMP ON KPI.R_Year=COMP.R_Year AND KPI.R_Round=COMP.R_Round AND KPI.PSNL_NO=COMP.PSNL_NO\n";
			SQL += " LEFT JOIN tb_HR_Round R ON KPI.R_Year=R.R_Year AND KPI.R_Round=R.R_Round" + "\n";
            SQL += " WHERE KPI.R_Year=" + R_Year + "\n";
			SQL += " AND KPI.R_Round=" + R_Round + "\n";
			string Title = ddlRound.Items[ddlRound.SelectedIndex].Text.Replace("รอบ", "ครั้งที่") + " ";


			if (ddlSector.SelectedIndex > 0) {
				SQL += " AND ( KPI.SECTOR_CODE='" + ddlSector.Items[ddlSector.SelectedIndex].Value + "' " + "\n";
				if (ddlSector.Items[ddlSector.SelectedIndex].Value == "3200") {
					SQL += " AND  KPI.DEPT_CODE NOT IN ('32000300'))    " + "\n";
				//-----------ฝ่ายโรงงานผลิตยาสูบ 3
				} else if (ddlSector.Items[ddlSector.SelectedIndex].Value == "3203") {
					SQL += " OR  KPI.DEPT_CODE IN ('32000300','32030000','32030100','32030200','32030300','32030500'))  " + "\n";
				//-----------ฝ่ายโรงงานผลิตยาสูบ 4
				} else if (ddlSector.Items[ddlSector.SelectedIndex].Value == "3204") {
					SQL += " OR  KPI.DEPT_CODE IN ('32040000','32040100','32040200','32040300','32040500','32040300'))  " + "\n";
				//-----------ฝ่ายโรงงานผลิตยาสูบ 5
				} else if (ddlSector.Items[ddlSector.SelectedIndex].Value == "3205") {
					SQL += " OR  KPI.DEPT_CODE IN ('32050000','32050100','32050200','32050300','32050500'))\t  " + "\n";
				} else {
					SQL += ")   " + "\n";
				}

				Title += GL.SplitString(ddlSector.Items[ddlSector.SelectedIndex].Text, ":")[1].Trim() + " ";
			}
			if (!string.IsNullOrEmpty(txtDeptName.Text)) {
				SQL += " AND (KPI.DEPT_Name LIKE '%" + txtDeptName.Text.Replace("'", "''") + "%' OR KPI.Sector_Name LIKE '%" + txtDeptName.Text.Replace("'", "''") + "%') " + "\n";
				//Title &= " หน่วยงาน '" & txtDeptName.Text & "' "
			}
			if (!string.IsNullOrEmpty(txtName.Text)) {
				SQL += " AND (KPI.PSNL_Fullname LIKE '%" + txtName.Text.Replace("'", "''") + "%' OR KPI.PSNL_NO LIKE '%" + txtName.Text.Replace("'", "''") + "%') " + "\n";
				//Title &= " พนักงาน '" & txtName.Text & "' "
			}

			if ((chkClass1.Checked & chkClass2.Checked & chkClass3.Checked & chkClass4.Checked & chkClass5.Checked & chkClass6.Checked & chkClass7.Checked & chkClass8.Checked & chkClass9.Checked & chkClass10.Checked & chkClass11.Checked & chkClass12.Checked & chkClass13.Checked) | (!chkClass1.Checked & !chkClass2.Checked & !chkClass3.Checked & !chkClass4.Checked & !chkClass5.Checked & !chkClass6.Checked & !chkClass7.Checked & !chkClass8.Checked & !chkClass9.Checked & !chkClass10.Checked & !chkClass11.Checked & !chkClass12.Checked & !chkClass13.Checked)) {
				//----------- Do nothing--------
			} else {
				string _classFilter = "";
				string _classTitle = "";
				for (int i = 1; i <= 13; i++) {
                    CheckBox chk = (CheckBox)pnlList.FindControl("chkClass" + i);
					if (chk.Checked) {
						_classFilter += "'" + i.ToString().PadLeft(2, GL.chr0) + "',";
						_classTitle += i + ",";
					}
				}
				Title += " ระดับ " + _classTitle.Substring(0, _classTitle.Length - 1);
				SQL += " AND KPI.PNPS_CLASS IN (" + _classFilter.Substring(0, _classFilter.Length - 1) + ") " + "\n";
			}

			switch (ddlStatus.Items[ddlStatus.SelectedIndex].Value) {
				case "-1":
					SQL += "";
					break;
				case "0":
                    SQL += " AND (ISNULL(KPI.Ass_Status,0) IN (0,-1)  OR ISNULL(COMP.ASS_Status,0) IN (0,-1))" + "\n";
					Title += " รายที่ยังไม่ส่งแบบประเมิน";
					break;
				case "1":
				case "2":
				case "3":
				case "4":
				case "5":
                    int _status =GL.CINT(ddlStatus.Items[ddlStatus.SelectedIndex].Value);
                    SQL += " AND (KPI.Ass_Status =" + ddlStatus.Items[ddlStatus.SelectedIndex].Value + " OR COMP.ASS_Status=" + ddlStatus.Items[ddlStatus.SelectedIndex].Value + ")" + "\n";
                    Title += " สถานะ" + BL.GetAssessmentStatusName(_status);
					break;
			}

			if (chkASSYes.Checked & !chkASSNo.Checked) {
				Title += " การประเมินเสร็จสมบูรณ์";
                SQL += " AND KPI.Ass_Status=5 AND COMP.ASS_Status=5 ";
			} else if (!chkASSYes.Checked & chkASSNo.Checked) {
				Title += " การยังประเมินไม่เสร็จ";
                SQL += " AND (ISNULL(KPI.Ass_Status,0)<>5 OR ISNULL(COMP.ASS_Status,0)<>5) ";
			}

            SQL += "   AND ( KPI.PSN_STAT_NAME_ON_ROUND ='ปัจจุบัน' OR  COMP.PSN_STAT_NAME_ON_ROUND ='ปัจจุบัน' ) ";
			SQL += " ORDER BY KPI.R_Year,KPI.R_Round,KPI.SECTOR_CODE,KPI.DEPT_CODE,KPI.PNPS_CLASS DESC,KPI.POS_No" + "\n";

			SqlDataAdapter DA = new SqlDataAdapter(SQL, BL.ConnectionString());
			DA.SelectCommand.CommandTimeout = 90;
			DataTable DT = new DataTable();
			DA.Fill(DT);


			Session["RPT_A_PSNAssStatus"] = DT;
			//--------- Title --------------
			Session["RPT_A_PSNAssStatus_Title"] = Title;
			//--------- Sub Title ----------
			Session["RPT_A_PSNAssStatus_SubTitle"] = "รายงาน ณ วันที่ " + DateTime.Now.Day + " " + C.ToMonthNameTH(DateTime.Now.Month) + " พ.ศ." + (DateTime.Now.Year + 543);
			Session["RPT_A_PSNAssStatus_SubTitle"] += "  พบ   " + GL.StringFormatNumber(DT.Rows.Count, 0) + " รายการ ";

			Pager.SesssionSourceName = "RPT_A_PSNAssStatus";
			Pager.RenderLayout();
			lblTitle.Text = Title;
			if (DT.Rows.Count == 0) {
				lblCountPSN.Text = "ไม่พบรายการดังกล่าว";
			} else {
				lblCountPSN.Text = "พบ " + GL.StringFormatNumber(DT.Rows.Count, 0) + " รายการ";
			}

		}

		protected void Pager_PageChanging(PageNavigation Sender)
		{
			Pager.TheRepeater = rptASS;
		}

		//------------ For Grouping -----------
		string LastSector = "";
		string LastDept = "";
		protected void rptASS_ItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
		{
			if (e.Item.ItemType != ListItemType.AlternatingItem & e.Item.ItemType != ListItemType.Item)
				return;

            Label lblSector = (Label)e.Item.FindControl("lblSector");
            Label lblDept = (Label)e.Item.FindControl("lblDept");
            Label lblPSNNo = (Label)e.Item.FindControl("lblPSNNo");
            Label lblPSNName = (Label)e.Item.FindControl("lblPSNName");
            Label lblPSNPos = (Label)e.Item.FindControl("lblPSNPos");
            Label lblPSNClass = (Label)e.Item.FindControl("lblPSNClass");
            HtmlAnchor lbl_KPI_Status = (HtmlAnchor)e.Item.FindControl("lbl_KPI_Status");
            HtmlAnchor lbl_KPI_Result = (HtmlAnchor)e.Item.FindControl("lbl_KPI_Result");
            HtmlAnchor lbl_COMP_Status = (HtmlAnchor)e.Item.FindControl("lbl_COMP_Status");
            HtmlAnchor lbl_COMP_Result = (HtmlAnchor)e.Item.FindControl("lbl_COMP_Result");
            Label lbl_Total_Result = (Label)e.Item.FindControl("lbl_Total_Result");

            DataRowView drv = (DataRowView)e.Item.DataItem;

			if (drv["SECTOR_NAME"].ToString() != LastSector) {
				lblSector.Text = drv["SECTOR_NAME"].ToString();
				LastSector = drv["SECTOR_NAME"].ToString();
				LastDept = "";
				for (int i = 1; i <= 11; i++) {
                    HtmlTableCell td = (HtmlTableCell)e.Item.FindControl("td" + i);
					td.Style["border-top-width"] = "2px";
					td.Style["border-top-color"] = "#999999";
				}
			} else {
                HtmlTableCell td1 = (HtmlTableCell)e.Item.FindControl("td1");
				td1.Style["border-top"] = "none";
			}

			if (drv["DEPT_NAME"].ToString() != LastDept) {
				if (drv["DEPT_NAME"].ToString() != drv["SECTOR_NAME"].ToString()) {
					lblDept.Text = drv["DEPT_NAME"].ToString().Replace(drv["SECTOR_NAME"].ToString(), "").Trim();
				} else {
					lblDept.Text = drv["DEPT_NAME"].ToString();
				}
				LastDept = drv["DEPT_NAME"].ToString();
			} else {
                HtmlTableCell td2 = (HtmlTableCell)e.Item.FindControl("td2");
				td2.Style["border-top"] = "none";
			}
			lblPSNNo.Text = drv["PSNL_NO"].ToString();
			lblPSNName.Text = drv["PSNL_Fullname"].ToString();
			lblPSNPos.Text = drv["POS_Name"].ToString();

			if (!GL.IsEqualNull(drv["PNPS_CLASS"])) {
				lblPSNClass.Text = GL.CINT (drv["PNPS_CLASS"]).ToString();
			}

			//--------------------- KPI Result -------------------
			if (!GL.IsEqualNull(drv["KPI_Status"]) && GL.CINT(drv["KPI_Status"]) == 5) {
				lbl_KPI_Status.InnerHtml = "<img src='images/check.png' title='ประเมินเสร็จสมบูรณ์'>";
				if (!GL.IsEqualNull(drv["KPI_Result"])) {
					lbl_KPI_Result.InnerHtml = GL.StringFormatNumber(drv["KPI_Result"]);
				} else {
					lbl_KPI_Result.InnerHtml = "-";
				}
			//check
			} else {
				lbl_KPI_Status.InnerHtml = BL.GetAssessmentStatusName(GL.CINT(drv["KPI_Status"]));
				//------ปรับสีสถานะ-----------
				//----<5
                if (GL.IsEqualNull(drv["KPI_Status"]) || GL.CINT(drv["KPI_Status"]) < HRBL.AssessmentStatus.AssessmentCompleted)
                {
					lbl_KPI_Status.Style["color"] = "red";
                    HtmlTableCell td7 = (HtmlTableCell)e.Item.FindControl("td7");
                    HtmlTableCell td8 = (HtmlTableCell)e.Item.FindControl("td8");
					td7.ColSpan = 2;
					td8.Visible = false;
				}
				//------ปรับสีสถานะ-----------
				lbl_KPI_Result.InnerHtml = "-";
			}

			if (!GL.IsEqualNull(drv["COMP_Status"]) && GL.CINT(drv["COMP_Status"]) == 5) {
				lbl_COMP_Status.InnerHtml = "<img src='images/check.png' title='ประเมินเสร็จสมบูรณ์'>";
				if (!GL.IsEqualNull(drv["COMP_Result"])) {
					lbl_COMP_Result.InnerHtml = GL.StringFormatNumber(drv["COMP_Result"]);
				} else {
					lbl_COMP_Result.InnerHtml = "-";
				}
			} else {
				lbl_COMP_Status.InnerHtml = BL.GetAssessmentStatusName(GL.CINT(drv["COMP_Status"]));

				//------ปรับสีสถานะ-----------
				//----<5
                if (GL.IsEqualNull(drv["COMP_Result"]) || GL.CINT(drv["COMP_Status"]) < HRBL.AssessmentStatus.AssessmentCompleted)
                {
					lbl_COMP_Status.Style["color"] = "red";
                    HtmlTableCell td9 = (HtmlTableCell)e.Item.FindControl("td9");
                    HtmlTableCell td10 = (HtmlTableCell)e.Item.FindControl("td10");
					td9.ColSpan = 2;
					td10.Visible = false;
				}
				//------ปรับสีสถานะ-----------

				lbl_COMP_Result.InnerHtml = "-";
			}

            if (!GL.IsEqualNull(drv["Total_Result"]) && GL.CDBL(drv["Total_Result"]) != 0 && Information.IsNumeric(lbl_KPI_Result.InnerHtml) && Information.IsNumeric(lbl_COMP_Result.InnerHtml))
            {
				lbl_Total_Result.Text = GL.StringFormatNumber(drv["Total_Result"]);
			} else {
				lbl_Total_Result.Text = "-";
			}

            lbl_KPI_Status.HRef = "Print/RPT_K_PNSAss.aspx?Mode=PDF&R_Year=" + drv["R_Year"].ToString() + "&R_Round=" + drv["R_Round"].ToString() + "&PSNL_NO=" + drv["PSNL_NO"].ToString() + "&Status=" + drv["KPI_Status"].ToString();
            lbl_KPI_Result.HRef = "Print/RPT_K_PNSAss.aspx?Mode=PDF&R_Year=" + drv["R_Year"].ToString() + "&R_Round=" + drv["R_Round"].ToString() + "&PSNL_NO=" + drv["PSNL_NO"].ToString() + "&Status=" + drv["KPI_Status"].ToString();
            lbl_COMP_Status.HRef = "Print/RPT_C_PNSAss.aspx?Mode=PDF&R_Year=" + drv["R_Year"].ToString() + "&R_Round=" + drv["R_Round"].ToString() + "&PSNL_NO=" + drv["PSNL_NO"].ToString() + "&Status=" + drv["COMP_Status"].ToString();
            lbl_COMP_Result.HRef = "Print/RPT_C_PNSAss.aspx?Mode=PDF&R_Year=" + drv["R_Year"].ToString() + "&R_Round=" + drv["R_Round"].ToString() + "&PSNL_NO=" + drv["PSNL_NO"].ToString() + "&Status=" + drv["COMP_Status"].ToString();
		}
		public RPT_A_PSNAssStatus()
		{
			Load += Page_Load;
		}
	}
}

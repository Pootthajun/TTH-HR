using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Xml.Linq;
using System.Diagnostics;
using System.Collections.Specialized;
using System.Configuration;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Caching;
using System.Web.SessionState;
using System.Web.Security;
using System.Web.Profile;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
namespace VB
{

	public partial class AssessmentSetting_Assessor : System.Web.UI.Page
	{

		HRBL BL = new HRBL();
		GenericLib GL = new GenericLib();

		textControlLib TC = new textControlLib();
		public string PREVIOS_POS {
			get { return lblASSName.Attributes["PREVIOS_POS"]; }
			set { lblASSName.Attributes["PREVIOS_POS"] = value; }
		}


		public string ASSESSOR_POS {
			get { return lblASSName.Attributes["ASSESSOR_POS"]; }
			set { lblASSName.Attributes["ASSESSOR_POS"] = value; }
		}

		public string[] IgnoreList {
			get {

                string[] tmp;
                try {
                    tmp = (string[])ViewState["IgnoreList"];
                    return tmp;
				} catch (Exception ex) {	
				    tmp=new string[0];
					return tmp;
				}
			}
			set { ViewState["IgnoreList"] = value; }
		}

		public string ChooseMode {
			get { return lblChooseMode.Text; }
			set { lblChooseMode.Text = value.ToString (); }
		}



		protected void Page_Load(object sender, System.EventArgs e)
		{
			if ((Session["USER_PSNL_NO"] == null)) {
				ScriptManager.RegisterStartupScript(this.Page, typeof(string), "Alert", "showAlert('กรุณาลงชื่อเข้าใช้ระบบ');", true);
				Response.Redirect("Login.aspx");
				return;
			}

			if (!IsPostBack) {

                // Keep Start Time
                DateTime StartTime = DateTime.Now;

				BindList();
				BL.BindDDlPSNType(ddlPSNType);
				pnlList.Visible = true;
				pnlEdit.Visible = false;
				pnlPOSDialog.Visible = false;

                //Report Time To Entry This Page
                DateTime EndTime = DateTime.Now;
                lblReportTime.Text = "เริ่มเข้าสู่หน้านี้ " + StartTime.ToString("HH:mm:ss.ff") + " ถึง " + EndTime.ToString("HH:mm:ss.ff") + " ใช้เวลา " + Math.Round(GL.DiffTimeDecimalSeconds(StartTime, EndTime), 2) + " วินาที";
			}
			SetCheckboxStyle();
		}

		private void SetCheckboxStyle()
		{
			if (chkBlank.Checked) {
				chkBlank.CssClass = "checked";
			} else {
				chkBlank.CssClass = "";
			}

			for (int i = 1; i <= 13; i++) {
				CheckBox chk =(CheckBox) pnlList.FindControl("chkClass" + i);
				if (chk.Checked) {
					chk.CssClass = "checked";
				} else {
					chk.CssClass = "";
				}
			}
		}

		protected void btnSearch_Click(object sender, System.EventArgs e)
		{
			BindList();
		}

		private void BindList()
		{
			string SQL = "SELECT DISTINCT  vw_ASSESSOR_MAPPING.* ,vw_PN_Position.PNPO_TYPE, vw_PN_Position.PNPO_TYPE_NAME\n";
			SQL += "   FROM vw_ASSESSOR_MAPPING \n";
			//----------ดึงประเภท อิงตามตำแหน่ง------------
			SQL += "  LEFT JOIN vw_PN_Position ON vw_ASSESSOR_MAPPING.PSN_POS_NO= vw_PN_Position.POS_NO\n";

			string Title = "";
			string Filter = "";
			if (!string.IsNullOrEmpty(txtSearch_ASS_POS.Text)) {
				Filter += " (vw_ASSESSOR_MAPPING.MGR_POS LIKE '%" + txtSearch_ASS_POS.Text.Replace("'", "''").Replace(" ", "%") + "%' OR vw_ASSESSOR_MAPPING.MGR_POS_NAME LIKE '%" + txtSearch_ASS_POS.Text.Replace("'", "''").Replace(" ", "%") + "%') AND ";
			}

			if (!string.IsNullOrEmpty(txtSearch_PSN_POS.Text)) {
				Filter += " (vw_ASSESSOR_MAPPING.PSN_POS_NO LIKE '%" + txtSearch_PSN_POS.Text.Replace("'", "''").Replace(" ", "%") + "%' OR vw_ASSESSOR_MAPPING.PSN_POS_NAME LIKE '%" + txtSearch_PSN_POS.Text.Replace("'", "''").Replace(" ", "%") + "%') AND ";
			}

			if (!string.IsNullOrEmpty(txtSearch_ASS_DEPT.Text)) {
				Filter += " (vw_ASSESSOR_MAPPING.MGR_DEPT_CODE LIKE '%" + txtSearch_ASS_DEPT.Text.Replace("'", "''").Replace(" ", "%") + "%' OR vw_ASSESSOR_MAPPING.MGR_DEPT_NAME LIKE '%" + txtSearch_ASS_DEPT.Text.Replace("'", "''").Replace(" ", "%") + "%') AND ";
			}

			if (!string.IsNullOrEmpty(txtSearch_PSN_DEPT.Text)) {
				Filter += " (vw_ASSESSOR_MAPPING.PSN_DEPT_CODE LIKE '%" + txtSearch_PSN_DEPT.Text.Replace("'", "''").Replace(" ", "%") + "%' OR vw_ASSESSOR_MAPPING.PSN_DEPT_NAME LIKE '%" + txtSearch_PSN_DEPT.Text.Replace("'", "''").Replace(" ", "%") + "%') AND ";
			}

			if (!string.IsNullOrEmpty(txtSearch_MGR_CODE.Text)) {
				Filter += " (vw_ASSESSOR_MAPPING.MGR_CODE LIKE '%" + txtSearch_MGR_CODE.Text.Replace("'", "''").Replace(" ", "%") + "%' OR vw_ASSESSOR_MAPPING.MGR_NAME LIKE '%" + txtSearch_MGR_CODE.Text.Replace("'", "''").Replace(" ", "%") + "%') AND ";
			}

			if (!string.IsNullOrEmpty(txtSearch_PSN_CODE.Text)) {
				Filter += " (vw_ASSESSOR_MAPPING.PSN_CODE LIKE '%" + txtSearch_PSN_CODE.Text.Replace("'", "''").Replace(" ", "%") + "%' OR vw_ASSESSOR_MAPPING.PSN_NAME LIKE '%" + txtSearch_PSN_CODE.Text.Replace("'", "''").Replace(" ", "%") + "%') AND ";
			}

			if (ddlPSNType.SelectedIndex > 0) {
                Filter += " (vw_PN_Position.PNPO_TYPE IN ( " + ddlPSNType.SelectedValue + " ))  AND ";

			}

			if ((chkClass1.Checked & chkClass2.Checked & chkClass3.Checked & chkClass4.Checked & chkClass5.Checked & chkClass6.Checked & chkClass7.Checked & chkClass8.Checked & chkClass9.Checked & chkClass10.Checked & chkClass11.Checked & chkClass12.Checked & chkClass13.Checked) | (!chkClass1.Checked & !chkClass2.Checked & !chkClass3.Checked & !chkClass4.Checked & !chkClass5.Checked & !chkClass6.Checked & !chkClass7.Checked & !chkClass8.Checked & !chkClass9.Checked & !chkClass10.Checked & !chkClass11.Checked & !chkClass12.Checked & !chkClass13.Checked)) {
				//----------- Do nothing--------
			} else {
				string _classFilter = "";
				string _classTitle = "";
				for (int i = 1; i <= 13; i++) {
					CheckBox chk =(CheckBox) pnlList.FindControl("chkClass" + i);
					if (chk.Checked) {
						_classFilter += "'" + i.ToString().PadLeft(2, GL.chr0) + "',";
						_classTitle += i + ",";
					}
				}
				//Title &= " ระดับ " & _classTitle.Substring(0, _classTitle.Length - 1)
				Filter += " vw_ASSESSOR_MAPPING.PSN_CLASS IN (" + _classFilter.Substring(0, _classFilter.Length - 1) + ") AND\n";
			}

			if (chkBlank.Checked) {
				Filter += " vw_ASSESSOR_MAPPING.MGR_POS IS NULL AND ";
				Title += " รายงานตำแหน่งที่ยังไม่กำหนดผู้ประเมินหลัก ";
			} else {
				Title += " รายงานผู้มีสิทธิ์ประเมินผล ";
			}

			if (!string.IsNullOrEmpty(Filter)) {
				SQL += " WHERE " + Filter.Substring(0, Filter.Length - 4) + "\n";
			}

			SQL += " ORDER BY vw_ASSESSOR_MAPPING.MGR_CLASS DESC,vw_ASSESSOR_MAPPING.MGR_DEPT_CODE,vw_ASSESSOR_MAPPING.MGR_POS,vw_ASSESSOR_MAPPING.PSN_DEPT_CODE,vw_ASSESSOR_MAPPING.PSN_CLASS DESC,vw_ASSESSOR_MAPPING.PSN_POS_NO";

			SqlDataAdapter DA = new SqlDataAdapter(SQL, BL.ConnectionString());
			DataTable DT = new DataTable();
			DA.Fill(DT);

			string Count_List = "";

			Session["AssessmentSetting_Assessor_List"] = DT;
			Pager.SesssionSourceName = "AssessmentSetting_Assessor_List";
			Pager.RenderLayout();

			if (DT.Rows.Count == 0) {
				lblCountList.Text = "ไม่พบรายการดังกล่าว";
				Count_List += lblCountList.Text;
			} else {
				lblCountList.Text = "พบ " + GL.StringFormatNumber(DT.Rows.Count, 0) + " รายการ";
				Count_List += "  พบ " + GL.StringFormatNumber(DT.Rows.Count, 0) + " รายการ";
			}
			Session["AssessmentSetting_Assessor_Title"] = Title;
			Session["AssessmentSetting_Assessor_Count_List"] = Count_List;
		}

		protected void Pager_PageChanging(PageNavigation Sender)
		{
			Pager.TheRepeater = rptList;
		}

		//---------- Datasource = Department ---------
		string LastPos = "";

		string LastDept = "";
		protected void rptList_ItemCommand(object source, System.Web.UI.WebControls.RepeaterCommandEventArgs e)
		{
			if (e.Item.ItemType != ListItemType.Item & e.Item.ItemType != ListItemType.AlternatingItem)
				return;

			LinkButton btnEdit =(LinkButton) e.Item.FindControl("btnEdit");
			string MGR_POS = btnEdit.Attributes["MGR_POS"];

			//------------- Edit Group -----------
			if (!string.IsNullOrEmpty(MGR_POS)) {

				string SQL = "";
				SQL += " SELECT POS.POS_NO,ISNULL(POS.MGR_NAME,POS.FN_Name) POS_NAME,PNPO_CLASS,POS.DEPT_CODE,POS.DEPT_NAME\n";
				SQL += " ,PSN.PSNL_NO,PSN.PSNL_Fullname\n";
				SQL += " FROM vw_PN_Position POS\n";
				SQL += " LEFT JOIN vw_PN_PSNL PSN ON POS.POS_NO=PSN.POS_NO\n";
				SQL += " WHERE POS.POS_NO='" + MGR_POS + "'\n";

				SqlDataAdapter DA = new SqlDataAdapter(SQL, BL.ConnectionString());
				DataTable DT = new DataTable();
				DA.Fill(DT);

				if (DT.Rows.Count == 0) {
					//-------
					ScriptManager.RegisterStartupScript(this.Page, typeof(string), "Alert", "showAlert('ไม่พบข้อมูลดังกล่าว');", true);
					return;
				}

				ASSESSOR_POS = MGR_POS;
				PREVIOS_POS = MGR_POS;

				lblASSName.Text = DT.Rows[0]["POS_NO"] + " : " + DT.Rows[0]["POS_NAME"];
				lblASSDept.Text = DT.Rows[0]["DEPT_NAME"].ToString();
				if (!GL.IsEqualNull(DT.Rows[0]["PNPO_CLASS"])) {
					lblASSClass.Text = GL.CINT (DT.Rows[0]["PNPO_CLASS"]).ToString ();
				} else {
					lblASSClass.Text = "";
				}
				if (!GL.IsEqualNull(DT.Rows[0]["PSNL_NO"])) {
					lblASSNow.Text = DT.Rows[0]["PSNL_NO"] + " : " + DT.Rows[0]["PSNL_Fullname"];
					lblASSNow.ForeColor = System.Drawing.Color.Green;
				} else {
					lblASSNow.Text = "ไม่มีผู้ครองตำแหน่ง";
					lblASSNow.ForeColor = System.Drawing.Color.Red;
				}

				SQL = "SELECT POS.POS_NO,ISNULL(POS.MGR_NAME,POS.FN_Name) POS_NAME,PNPO_CLASS,POS.DEPT_CODE,POS.DEPT_NAME\n";
				SQL += " ,PSN.PSNL_NO,PSN.PSNL_Fullname\n";
				SQL += " FROM vw_PN_Position POS\n";
				SQL += " LEFT JOIN vw_PN_PSNL PSN ON POS.POS_NO=PSN.POS_NO\n";
				SQL += " LEFT JOIN tb_HR_Assessor_Pos ASS_POS ON POS.POS_NO=ASS_POS.POS_NO\n";
				SQL += " WHERE ASS_POS.ASSESSOR_POS='" + ASSESSOR_POS + "'\n";

                ////  filter ตามหน้า List แสดงรายการตามหน้า List
                string Filter = "";
                if (!string.IsNullOrEmpty(txtSearch_PSN_POS.Text))
                {
                    Filter += " (POS.POS_NO LIKE '%" + txtSearch_PSN_POS.Text.Replace("'", "''").Replace(" ", "%") + "%' OR ISNULL(POS.MGR_NAME,POS.FN_Name) LIKE '%" + txtSearch_PSN_POS.Text.Replace("'", "''").Replace(" ", "%") + "%') AND ";
                }
                if (!string.IsNullOrEmpty(txtSearch_PSN_DEPT.Text))
                {
                    Filter += " (POS.DEPT_CODE LIKE '%" + txtSearch_PSN_DEPT.Text.Replace("'", "''").Replace(" ", "%") + "%' OR POS.DEPT_NAME LIKE '%" + txtSearch_PSN_DEPT.Text.Replace("'", "''").Replace(" ", "%") + "%') AND ";
                }
                if (!string.IsNullOrEmpty(txtSearch_PSN_CODE.Text))
                {
                    Filter += " (PSN.PSNL_NO LIKE '%" + txtSearch_PSN_CODE.Text.Replace("'", "''").Replace(" ", "%") + "%' OR PSN.PSNL_Fullname LIKE '%" + txtSearch_PSN_CODE.Text.Replace("'", "''").Replace(" ", "%") + "%') AND ";
                }
                if (ddlPSNType.SelectedIndex > 0)
                {
                    Filter += " (POS.PNPO_TYPE IN ( " + ddlPSNType.SelectedValue + " ))  AND ";
                }
                if ((chkClass1.Checked & chkClass2.Checked & chkClass3.Checked & chkClass4.Checked & chkClass5.Checked & chkClass6.Checked & chkClass7.Checked & chkClass8.Checked & chkClass9.Checked & chkClass10.Checked & chkClass11.Checked & chkClass12.Checked & chkClass13.Checked) | (!chkClass1.Checked & !chkClass2.Checked & !chkClass3.Checked & !chkClass4.Checked & !chkClass5.Checked & !chkClass6.Checked & !chkClass7.Checked & !chkClass8.Checked & !chkClass9.Checked & !chkClass10.Checked & !chkClass11.Checked & !chkClass12.Checked & !chkClass13.Checked))
                {
                    //----------- Do nothing--------
                }
                else
                {
                    string _classFilter = "";
                    string _classTitle = "";
                    for (int i = 1; i <= 13; i++)
                    {
                        CheckBox chk = (CheckBox)pnlList.FindControl("chkClass" + i);
                        if (chk.Checked)
                        {
                            _classFilter += "'" + i.ToString().PadLeft(2, GL.chr0) + "',";
                            _classTitle += i + ",";
                        }
                    }
                    Filter += " PNPO_CLASS IN (" + _classFilter.Substring(0, _classFilter.Length - 1) + ") AND\n";
                }
                if (!string.IsNullOrEmpty(Filter))
                {
                    SQL += " AND " + Filter.Substring(0, Filter.Length - 4) + "\n";
                }



				DA = new SqlDataAdapter(SQL, BL.ConnectionString());
				DT = new DataTable();
				DA.Fill(DT);

				rptPSNPOS.DataSource = DT;
				rptPSNPOS.DataBind();

				pnlList.Visible = false;
				pnlEdit.Visible = true;
				pnlPOSDialog.Visible = false;
			} else {
				//'------------- Choose ASSESSOR -----------
				//Dim lbl_PSN_POS As Label = e.Item.FindControl("lbl_PSN_POS")
				//Dim lbl_PSN_DEPT As Label = e.Item.FindControl("lbl_PSN_DEPT")
				//ChooseMode = "เลือกผู้ประเมินหลักสำหรับตำแหน่ง " & lbl_PSN_POS.Text & " " & lbl_PSN_DEPT.Text
				//Dim tmpList As String() = {btnEdit.Attributes("PSN_POS_NO")}
				//IgnoreList = tmpList

				//BindChooseList()
				//pnlPOSDialog.Visible = True



				ChooseMode = "เพิ่มตำแหน่งผู้ประเมิน";

				string[] tmpList = {
					
				};
				string SQL = "SELECT DISTINCT ASSESSOR_POS FROM tb_HR_Assessor_Pos WHERE ASSESSOR_POS IS NOT NULL";
				SqlDataAdapter DA = new SqlDataAdapter(SQL, BL.ConnectionString());
				DataTable DT = new DataTable();
				DA.Fill(DT);

				for (int i = 0; i <= DT.Rows.Count - 1; i++) {
					GL.PushArray_String(tmpList, DT.Rows[i]["ASSESSOR_POS"].ToString ());
				}
				IgnoreList = tmpList;

				BindChooseList();

				pnlPOSDialog.Visible = true;
			}

		}

		protected void rptList_ItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
		{
			if (e.Item.ItemType != ListItemType.Item & e.Item.ItemType != ListItemType.AlternatingItem)
				return;

			Label lbl_ASS_POS =(Label) e.Item.FindControl("lbl_ASS_POS");
			Label lbl_ASS_CLASS = (Label)e.Item.FindControl("lbl_ASS_CLASS");
			Label lbl_ASS_DEPT = (Label)e.Item.FindControl("lbl_ASS_DEPT");
			Label lbl_ASS_NAME = (Label)e.Item.FindControl("lbl_ASS_NAME");
			Label lbl_PSN_POS = (Label)e.Item.FindControl("lbl_PSN_POS");
			Label lbl_PSN_CLASS = (Label)e.Item.FindControl("lbl_PSN_CLASS");
			Label lbl_PSN_DEPT = (Label)e.Item.FindControl("lbl_PSN_DEPT");
			 Label lbl_PSN_NAME = (Label)e.Item.FindControl("lbl_PSN_NAME");
			Label lbl_PSNType = (Label)e.Item.FindControl("lbl_PSNType");
            
			LinkButton btnEdit =(LinkButton) e.Item.FindControl("btnEdit");
			AjaxControlToolkit.ConfirmButtonExtender cfm_btnEdit =(AjaxControlToolkit.ConfirmButtonExtender) e.Item.FindControl("cfm_btnEdit");
			HtmlTableCell tdEdit =(HtmlTableCell) e.Item.FindControl("tdEdit");

            DataRowView drv = (DataRowView)e.Item.DataItem;
			if (!GL.IsEqualNull(drv["MGR_POS"])) {
				if (LastPos != drv["MGR_POS"].ToString ()) { 
					lbl_ASS_POS.Text = drv["MGR_POS"].ToString() + " : " + drv["MGR_POS_NAME"].ToString();
					LastPos = drv["MGR_POS"].ToString();
					LastDept = "";

					if (!GL.IsEqualNull(drv["MGR_CLASS"])) {
						lbl_ASS_CLASS.Text = GL.CINT (drv["MGR_CLASS"]).ToString ();
					}

					if (!GL.IsEqualNull(drv["MGR_DEPT_NAME"])) {
						lbl_ASS_DEPT.Text = drv["MGR_DEPT_NAME"].ToString ();
					}

					if (!GL.IsEqualNull(drv["MGR_NAME"])) {
						lbl_ASS_NAME.Text = drv["MGR_CODE"].ToString() + " : " + drv["MGR_NAME"].ToString();
					} else {
						lbl_ASS_NAME.Text = "ไม่มีผู้ครองตำแหน่ง";
						lbl_ASS_NAME.ForeColor = System.Drawing.Color.Red;
					}

					btnEdit.Attributes["MGR_POS"] = drv["MGR_POS"].ToString ();
					//----------- Key -------------
					cfm_btnEdit.Enabled = false;
					//-------------
					btnEdit.Visible = true;

				} else {
					for (int i = 1; i <= 4; i++) {
						HtmlTableCell td =(HtmlTableCell) e.Item.FindControl("td" + i);
						td.Style["border-top"] = "none";
					}
					tdEdit.Style["border-top"] = "none";
					cfm_btnEdit.ConfirmText = "คุณต้องการเลือกผู้ประเมินหลักสำหรับตำแหน่ง " + drv["PSN_POS_NAME"].ToString() + " " + drv["PSN_DEPT_NAME"].ToString() + " ?";
					//-------------
					btnEdit.Visible = false;
				}

				btnEdit.Attributes["MGR_POS"] = drv["MGR_POS"].ToString ();
			} else {
				if (LastPos == "ไม่มีผู้ประเมินหลัก") {
					for (int i = 1; i <= 4; i++) {
						HtmlTableCell td =(HtmlTableCell) e.Item.FindControl("td" + i);
						td.Style["border-top"] = "none";
					}
					tdEdit.Style["border-top"] = "none";
					//-------------
					btnEdit.Visible = false;
				} else {
					LastPos = "ไม่มีผู้ประเมินหลัก";
					lbl_ASS_POS.Text = "ยังไม่กำหนดผู้ประเมินหลัก";
					lbl_ASS_POS.ForeColor = System.Drawing.Color.Red;

                    HtmlTableCell td1 = (HtmlTableCell)e.Item.FindControl("td1");
					td1.ColSpan = 4;
					//td1.Style.Item("border-right") = "1px solid #cccccc;"
					for (int i = 2; i <= 4; i++) {
                        HtmlTableCell td = (HtmlTableCell)e.Item.FindControl("td" + i.ToString());
						td.Visible = false;
					}

					//-------------btnAdd
					btnEdit.Visible = true;
				}
				btnEdit.Attributes["MGR_POS"] = "";
				cfm_btnEdit.ConfirmText = "คุณต้องการเพิ่มตำแหน่งผู้ประเมินหลัก " + " ?";



			}

			btnEdit.Attributes["PSN_POS_NO"] = drv["PSN_POS_NO"].ToString ();

			if (LastDept != drv["PSN_DEPT_CODE"].ToString()) {
				LastDept = drv["PSN_DEPT_CODE"].ToString();
			} else {
				for (int i = 5; i <= 9; i++) {
                    HtmlTableCell td = (HtmlTableCell)e.Item.FindControl("td" + i);
					td.Style["border-top"] = "none";
				}
			}

			if (!GL.IsEqualNull(drv["PSN_POS_NO"])) {
				lbl_PSN_POS.Text = drv["PSN_POS_NO"].ToString() + " : " + drv["PSN_POS_NAME"].ToString();
			}

			if (!GL.IsEqualNull(drv["PSN_CLASS"])) {
				lbl_PSN_CLASS.Text = GL.CINT (drv["PSN_CLASS"]).ToString ();
			}

			if (!GL.IsEqualNull(drv["PSN_DEPT_NAME"])) {
				lbl_PSN_DEPT.Text = drv["PSN_DEPT_NAME"].ToString ();
			}

			if (!GL.IsEqualNull(drv["PNPO_TYPE_NAME"])) {
				lbl_PSNType.Text = drv["PNPO_TYPE_NAME"].ToString ();
				lbl_PSNType.ForeColor = System.Drawing.Color.Gray;
			}

			if (!GL.IsEqualNull(drv["PSN_NAME"])) {
				lbl_PSN_NAME.Text = drv["PSN_CODE"].ToString() + " : " + drv["PSN_NAME"].ToString();
				lbl_PSN_NAME.ForeColor = System.Drawing.Color.Gray;
			} else {
				lbl_PSN_NAME.Text = "ไม่มีผู้ครองตำแหน่ง";
				lbl_PSN_NAME.ForeColor = System.Drawing.Color.Red;
			}

		}


		protected void btnAdd_Click(object sender, System.EventArgs e)
		{
			ChooseMode = "เพิ่มตำแหน่งผู้ประเมิน";

			string[] tmpList = {
				
			};
			string SQL = "SELECT DISTINCT ASSESSOR_POS FROM tb_HR_Assessor_Pos WHERE ASSESSOR_POS IS NOT NULL";
			SqlDataAdapter DA = new SqlDataAdapter(SQL, BL.ConnectionString());
			DataTable DT = new DataTable();
			DA.Fill(DT);

			for (int i = 0; i <= DT.Rows.Count - 1; i++) {
				GL.PushArray_String(tmpList, DT.Rows[i]["ASSESSOR_POS"].ToString());
			}
			IgnoreList = tmpList;

			BindChooseList();

			pnlPOSDialog.Visible = true;

		}


		protected void btn_Search_POS_Click(object sender, System.EventArgs e)
		{
			BindChooseList();
		}


		private void BindChooseList()
		{
			string SQL = "";
			SQL += " SELECT POS.POS_NO,ISNULL(POS.MGR_NAME,POS.FN_Name) POS_NAME,PNPO_CLASS,POS.DEPT_CODE,POS.DEPT_NAME\n";
			SQL += " ,PSN.PSNL_NO,PSN.PSNL_Fullname\n";
			SQL += " ,ASS.POS_NO ASS_BY_NO,CASE ASS.POS_NO WHEN '00001' THEN  ASS.DEPT_NAME ELSE ISNULL(ASS.MGR_NAME,ASS.FN_NAME)+ ' ' + ASS.DEPT_NAME END ASS_BY_NAME\n";
			SQL += " FROM vw_PN_Position POS\n";
			SQL += " LEFT JOIN vw_PN_PSNL PSN ON POS.POS_NO=PSN.POS_NO  AND  POS.PSNL_NO=PSN.PSNL_NO      \n";
			SQL += " LEFT JOIN tb_HR_Assessor_Pos ASS_POS ON POS.POS_NO=ASS_POS.POS_NO\n";
			SQL += " LEFT JOIN vw_PN_PSNL ASS ON ASS_POS.ASSESSOR_POS=ASS.POS_NO\n";

			string Filter = "";
			if (IgnoreList.Length > 0) {
				for (int i = 0; i <= IgnoreList.Length - 1; i++) {
					Filter += "'" + IgnoreList[i] + "',";
				}
				Filter = "POS.POS_NO NOT IN (" + Filter.Substring(0, Filter.Length - 1) + ") AND ";
			}

			if (!string.IsNullOrEmpty(txt_Search_Organize.Text)) {
				Filter += " (POS.DEPT_CODE LIKE '%" + txt_Search_Organize.Text.Replace("'", "''").Replace(" ", "%") + "%' OR POS.DEPT_NAME LIKE '%" + txt_Search_Organize.Text.Replace("'", "''").Replace(" ", "%") + "%' ) AND ";
			}
			if (!string.IsNullOrEmpty(txt_Search_Class.Text)) {
				Filter += " PNPO_CLASS='" + txt_Search_Class.Text.PadLeft(2, GL.chr0) + "' AND ";
			}
			if (!string.IsNullOrEmpty(txt_Search_Name.Text)) {
				Filter += " (POS.POS_NO LIKE '%" + txt_Search_Name.Text.Replace("'", "''").Replace(" ", "%") + "%' OR ISNULL(POS.MGR_NAME,POS.FN_Name) LIKE '%" + txt_Search_Name.Text.Replace("'", "''").Replace(" ", "%") + "%' OR PSN.PSNL_NO LIKE '%" + txt_Search_Name.Text.Replace("'", "''").Replace(" ", "%") + "%' OR PSN.PSNL_Fullname LIKE '%" + txt_Search_Name.Text.Replace("'", "''").Replace(" ", "%") + "%') AND ";
			}

			switch (ChooseMode) {
				case "เพิ่มตำแหน่งพนักงาน/ผู้ถูกประเมิน":
					Filter += " POS.POS_NO<>'00001' AND ";
					break;
				default:
                    if (ChooseMode.IndexOf("เลือกผู้ประเมินหลักสำหรับตำแหน่ง")>-1)
                    {
						Filter += " POS.POS_NO IN (SELECT DISTINCT ASSESSOR_POS\n";
						Filter += " FROM tb_HR_Assessor_Pos) AND ";
					}
					break;
			}

            if (!string.IsNullOrEmpty(Filter))
            {
                SQL += "\n" + " WHERE " + Filter.Substring(0, Filter.Length - 4) + "\n";
            }



			SQL += " ORDER BY PNPO_CLASS DESC,DEPT_CODE\n";

			SqlDataAdapter DA = new SqlDataAdapter(SQL, BL.ConnectionString());
			DataTable DT = new DataTable();
			DA.Fill(DT);

			Session["Assessment_Setting_Assessor_Search_POS_LIST"] = DT;
			PagerPOS.SesssionSourceName = "Assessment_Setting_Assessor_Search_POS_LIST";
			PagerPOS.RenderLayout();

			if (DT.Rows.Count == 0) {
				lblCountPSN.Text = "ไม่พบรายการดังกล่าว";
			} else {
				lblCountPSN.Text = "พบ " + GL.StringFormatNumber(DT.Rows.Count, 0) + " รายการ";
			}
		}

		protected void PagerPOS_PageChanging(PageNavigation Sender)
		{
			PagerPOS.TheRepeater = rptPOSDialog;
		}

		protected void rptPOSDialog_ItemCommand(object source, System.Web.UI.WebControls.RepeaterCommandEventArgs e)
		{
            string SQL;
            DataTable DT;
            SqlCommandBuilder cmd;
            SqlDataAdapter DA;
            DataRow DR;
            string SELECTED_POS_NO;

			switch (e.CommandName) {
				case "Select":
					Button btnSelect =(Button) e.Item.FindControl("btnSelect");
					 SELECTED_POS_NO = btnSelect.CommandArgument;

					 SQL = "";
					SQL += " SELECT POS.POS_NO,ISNULL(POS.MGR_NAME,POS.FN_Name) POS_NAME,PNPO_CLASS,POS.DEPT_CODE,POS.DEPT_NAME\n";
					SQL += " ,PSN.PSNL_NO,PSN.PSNL_Fullname\n";
					SQL += " FROM vw_PN_Position POS\n";
					SQL += " LEFT JOIN vw_PN_PSNL PSN ON POS.POS_NO=PSN.POS_NO\n";
					SQL += " WHERE POS.POS_NO='" + SELECTED_POS_NO + "'\n";

					 DA = new SqlDataAdapter(SQL, BL.ConnectionString());
					 DT = new DataTable();
					DA.Fill(DT);

					if (DT.Rows.Count == 0) {
						pnlEdit.Visible = false;
						return;
					}


					switch (ChooseMode) {
						case "เพิ่มตำแหน่งผู้ประเมิน":
						case "เปลี่ยนตำแหน่งผู้ประเมินหลัก":

							ASSESSOR_POS = SELECTED_POS_NO;
							lblASSName.Text = DT.Rows[0]["POS_NO"] + " : " + DT.Rows[0]["POS_NAME"];
							lblASSDept.Text = DT.Rows[0]["DEPT_NAME"].ToString();
							if (!GL.IsEqualNull(DT.Rows[0]["PNPO_CLASS"])) {
								lblASSClass.Text = GL.CINT (DT.Rows[0]["PNPO_CLASS"]).ToString ();
							} else {
								lblASSClass.Text = "";
							}
							if (!GL.IsEqualNull(DT.Rows[0]["PSNL_NO"])) {
								lblASSNow.Text = DT.Rows[0]["PSNL_NO"] + " : " + DT.Rows[0]["PSNL_Fullname"];
								lblASSNow.ForeColor = System.Drawing.Color.Green;
							} else {
								lblASSNow.Text = "ไม่มีผู้ครองตำแหน่ง";
								lblASSNow.ForeColor = System.Drawing.Color.Red;
							}

							break;
						case "เพิ่มตำแหน่งพนักงาน/ผู้ถูกประเมิน":
							DataTable ST = SELECTED_PSN_POS();

							 DR = ST.NewRow();
							DR["POS_NO"] = DT.Rows[0]["POS_NO"];
							DR["POS_NAME"] = DT.Rows[0]["POS_NAME"];
							if (!GL.IsEqualNull(DT.Rows[0]["PNPO_CLASS"])) {
								DR["PNPO_CLASS"] = DT.Rows[0]["PNPO_CLASS"];
							}
							DR["DEPT_CODE"] = DT.Rows[0]["DEPT_CODE"];
							DR["DEPT_NAME"] = DT.Rows[0]["DEPT_NAME"];
							if (!GL.IsEqualNull(DT.Rows[0]["PSNL_NO"])) {
								DR["PSNL_NO"] = DT.Rows[0]["PSNL_NO"];
							}
							if (!GL.IsEqualNull(DT.Rows[0]["PSNL_Fullname"])) {
								DR["PSNL_Fullname"] = DT.Rows[0]["PSNL_Fullname"];
							}
							ST.Rows.Add(DR);
							rptPSNPOS.DataSource = ST;
							rptPSNPOS.DataBind();

							break;
						default:
                            if (ChooseMode.IndexOf("เลือกผู้ประเมินหลักสำหรับตำแหน่ง")>-1)
                            {
								SqlConnection Conn = new SqlConnection(BL.ConnectionString());
								Conn.Open();
								SqlCommand COMM = new SqlCommand();
								COMM.Connection = Conn;
								COMM.CommandType = CommandType.Text;

								//------------- DELETE PSN First -------------

								COMM.CommandText = "DELETE FROM tb_HR_Assessor_Pos WHERE ASSESSOR_POS ='" + ASSESSOR_POS + "' AND POS_NO='" + PREVIOS_POS + "' ";
								COMM.ExecuteNonQuery();

								DT = new DataTable();
								SQL = "SELECT * FROM tb_HR_Assessor_Pos WHERE 1=0";
								DA = new SqlDataAdapter(SQL, Conn);
								DA.Fill(DT);

								 DR = DT.NewRow();
								try {
									DR["POS_NO"] = IgnoreList[0];
								} catch {
								}
								DR["ASSESSOR_POS"] = SELECTED_POS_NO;
								DR["Update_By"] = Session["USER_PSNL_NO"];
								DR["Update_Time"] = DateAndTime.Now;
								DT.Rows.Add(DR);
								 cmd = new SqlCommandBuilder(DA);
								try {
									DA.Update(DT);
								} catch (Exception ex) {
								}
								DT.AcceptChanges();

								Conn.Close();

								ScriptManager.RegisterStartupScript(this.Page, typeof(string), "Alert", "showAlert('บันทึกสำเร็จ');", true);

								pnlPOSDialog.Visible = false;
								BindList();
								return;
							}
							break;
					}

					pnlList.Visible = false;
					pnlEdit.Visible = true;
					pnlPOSDialog.Visible = false;

					break;
			}
		}

		protected void rptPOSDialog_ItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
		{
			if (e.Item.ItemType != ListItemType.Item & e.Item.ItemType != ListItemType.AlternatingItem)
				return;

			Label lblListPOS =(Label) e.Item.FindControl("lblListPOS");
			Label lblListClass =(Label) e.Item.FindControl("lblListClass");
			Label lblListDEPT =(Label) e.Item.FindControl("lblListDEPT");
			Label lblListNow =(Label) e.Item.FindControl("lblListNow");

			HtmlInputButton btnPreSelect =(HtmlInputButton) e.Item.FindControl("btnPreSelect");
			Button btnSelect =(Button) e.Item.FindControl("btnSelect");

            DataRowView drv = (DataRowView)e.Item.DataItem;
			lblListPOS.Text = drv["POS_NO"].ToString() + " : " + drv["POS_Name"].ToString();
			if (!GL.IsEqualNull(drv["POS_NO"])) {
				lblListClass.Text = GL.CINT (drv["PNPO_CLASS"]).ToString ();
			}
			if (!GL.IsEqualNull(drv["DEPT_NAME"])) {
				lblListDEPT.Text = drv["DEPT_NAME"].ToString ();
			}
			if (!GL.IsEqualNull(drv["PSNL_NO"])) {
				lblListNow.Text = drv["PSNL_NO"] + " : " + drv["PSNL_Fullname"];
				lblListNow.ForeColor = System.Drawing.Color.Green;
			} else {
				lblListNow.Text = "ไม่มีผู้ครองตำแหน่ง";
				lblListNow.ForeColor = System.Drawing.Color.Red;

			}

			switch (ChooseMode) {
				case "เพิ่มตำแหน่งผู้ประเมิน":
				case "เปลี่ยนตำแหน่งผู้ประเมินหลัก":
					if (!GL.IsEqualNull(drv["PSNL_NO"])) {
						btnSelect.Style.Remove("display");
						btnPreSelect.Style["display"] = "none";
					} else {
						btnPreSelect.Attributes["onclick"] = "if(confirm('ตำแหน่งนี้ไม่มีผู้ครองตำแหน่ง ยืนยันดำเนินการต่อ?'))document.getElementById('" + btnSelect.ClientID + "').click();";
					}
					break;
				case "เพิ่มตำแหน่งพนักงาน/ผู้ถูกประเมิน":
                    if (!GL.IsEqualNull(drv["ASS_BY_NO"]) && !GL.IsEqualNull(drv["ASS_BY_NAME"]) && (drv["ASS_BY_NO"].ToString () != ASSESSOR_POS )   )
                    {
						btnPreSelect.Attributes["onclick"] = "if(confirm('ตำแหน่งมีผู้ประเมินอยู่แล้วคือ \\'" + drv["ASS_BY_NAME"] + "\\'\\n\\n ยืนยันดำเนินการต่อ?'))document.getElementById('" + btnSelect.ClientID + "').click();";
					} else {
						btnPreSelect.Style["display"] = "none";
						btnSelect.Style.Remove("display");
					}
					break;
				default:
                    if (ChooseMode.IndexOf("เลือกผู้ประเมินหลักสำหรับตำแหน่ง")>-1)
                    {
						btnPreSelect.Attributes["onclick"] = "if(confirm('ยืนยันเลือกตำแหน่ง " + lblListPOS.Text + " เป็นผู้ประเมินหลัก ?'))document.getElementById('" + btnSelect.ClientID + "').click();";
					}
					break;
			}


			btnSelect.CommandArgument = drv["POS_NO"].ToString();

		}

		protected void btnCloseDialog_Click(object sender, System.EventArgs e)
		{
			pnlPOSDialog.Visible = false;
		}

		protected void btnBack_Click(object sender, System.EventArgs e)
		{
			pnlList.Visible = true;
			pnlEdit.Visible = false;
			BindList();
		}


		protected void btnChoosePSN_Click(object sender, System.EventArgs e)
		{
			ChooseMode = "เพิ่มตำแหน่งพนักงาน/ผู้ถูกประเมิน";

			DataTable DT = SELECTED_PSN_POS();

			string[] tmpList = { ASSESSOR_POS };
			for (int i = 0; i <= DT.Rows.Count - 1; i++) {
				GL.PushArray_String(tmpList, DT.Rows[i]["POS_NO"].ToString ());
			}
			GL.PushArray_String(tmpList, ASSESSOR_POS);

			IgnoreList = tmpList;
			BindChooseList();

			pnlPOSDialog.Visible = true;

		}

		private DataTable SELECTED_PSN_POS()
		{

			DataTable DT = new DataTable();
			DT.Columns.Add("POS_NO");
			DT.Columns.Add("POS_NAME");
			DT.Columns.Add("PNPO_CLASS");
			DT.Columns.Add("DEPT_CODE");
			DT.Columns.Add("DEPT_NAME");
			DT.Columns.Add("PSNL_NO");
			DT.Columns.Add("PSNL_Fullname");
			foreach (RepeaterItem Item in rptPSNPOS.Items) {
				if (Item.ItemType != ListItemType.Item & Item.ItemType != ListItemType.AlternatingItem)
					continue;

				Label lblNo =(Label) Item.FindControl("lblNo");
				Label lblPSNPos =(Label) Item.FindControl("lblPSNPos");
				Label lblPSNClass =(Label) Item.FindControl("lblPSNClass");
				Label lblPSNDept =(Label ) Item.FindControl("lblPSNDept");
				Label lblPSNNow =(Label) Item.FindControl("lblPSNNow");

				Button btnPSNDelete =(Button) Item.FindControl("btnPSNDelete");

				DataRow DR = DT.NewRow();
				DR["POS_NO"] = lblNo.Attributes["POS_NO"];
				DR["POS_NAME"] = lblNo.Attributes["POS_NAME"];
				DR["PNPO_CLASS"] = lblPSNClass.Text;
				DR["DEPT_CODE"] = lblPSNDept.Attributes["DEPT_CODE"];
				DR["DEPT_NAME"] = lblPSNDept.Text;
				if (!string.IsNullOrEmpty(lblPSNNow.Attributes["PSNL_NO"])) {
					DR["PSNL_NO"] = lblPSNNow.Attributes["PSNL_NO"];
				}
				if (!string.IsNullOrEmpty(lblPSNNow.Attributes["PSNL_Fullname"])) {
					DR["PSNL_Fullname"] = lblPSNNow.Attributes["PSNL_Fullname"];
				}
				DT.Rows.Add(DR);
			}
			return DT;
		}

		protected void rptPSNPOS_ItemCommand(object source, System.Web.UI.WebControls.RepeaterCommandEventArgs e)
		{
			if (e.Item.ItemType != ListItemType.Item & e.Item.ItemType != ListItemType.AlternatingItem)
				return;

			DataTable DT = SELECTED_PSN_POS();
			try {
				DT.Rows.RemoveAt(e.Item.ItemIndex);
				rptPSNPOS.DataSource = DT;
				rptPSNPOS.DataBind();
			} catch {
			}
		}

		protected void rptPSNPOS_ItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
		{
			if (e.Item.ItemType != ListItemType.Item & e.Item.ItemType != ListItemType.AlternatingItem)
				return;

			Label lblNo =(Label) e.Item.FindControl("lblNo");
			 Label lblPSNPos = (Label)e.Item.FindControl("lblPSNPos");
			 Label lblPSNClass = (Label)e.Item.FindControl("lblPSNClass");
			Label lblPSNDept =(Label) e.Item.FindControl("lblPSNDept");
			Label lblPSNNow =(Label) e.Item.FindControl("lblPSNNow");

			  Button btnPSNDelete =(Button) e.Item.FindControl("btnPSNDelete");
			AjaxControlToolkit.ConfirmButtonExtender cfm_PSN_Delete =(AjaxControlToolkit.ConfirmButtonExtender) e.Item.FindControl("cfm_PSN_Delete");

            DataRowView drv = (DataRowView)e.Item.DataItem;
			lblNo.Text = GL.CINT (e.Item.ItemIndex + 1).ToString ();
			lblPSNPos.Text = drv["POS_NO"] + " : " + drv["POS_Name"].ToString();
			lblNo.Attributes["POS_NO"] = drv["POS_NO"].ToString ();
			lblNo.Attributes["POS_NAME"] = drv["POS_Name"].ToString();

			cfm_PSN_Delete.ConfirmText = "ยืนยันยกเลิกตำแหน่ง  " + lblPSNPos.Text;

			if (!GL.IsEqualNull(drv["PNPO_CLASS"])) {
				lblPSNClass.Text = GL.CINT (drv["PNPO_CLASS"]).ToString ();
			}
			lblPSNDept.Attributes["DEPT_CODE"] = drv["DEPT_CODE"].ToString ();
			lblPSNDept.Text = drv["DEPT_NAME"].ToString ();
			if (!GL.IsEqualNull(drv["PSNL_NO"])) {
				lblPSNNow.Attributes["PSNL_NO"] = drv["PSNL_NO"].ToString ();
				lblPSNNow.Text = drv["PSNL_NO"] + " : ";
			}
			if (!GL.IsEqualNull(drv["PSNL_Fullname"])) {
				lblPSNNow.Attributes["PSNL_Fullname"] = drv["PSNL_Fullname"].ToString ();
				lblPSNNow.Text += drv["PSNL_Fullname"];
			} else {
				lblPSNNow.Text = "ว่าง";
			}

		}

		protected void btnChooseAss_Click(object sender, System.EventArgs e)
		{
			ChooseMode = "เปลี่ยนตำแหน่งผู้ประเมินหลัก";

			string[] tmpList = { ASSESSOR_POS };
			string SQL = "SELECT DISTINCT ASSESSOR_POS FROM tb_HR_Assessor_Pos \n";
			SQL += " WHERE ASSESSOR_POS IS NOT NULL OR ASSESSOR_POS ='" + PREVIOS_POS + "'\n";
			SqlDataAdapter DA = new SqlDataAdapter(SQL, BL.ConnectionString());
			DataTable DT = new DataTable();
			DA.Fill(DT);
			for (int i = 0; i <= DT.Rows.Count - 1; i++) {
				GL.PushArray_String(tmpList, DT.Rows[i]["ASSESSOR_POS"].ToString ());
			}

			IgnoreList = tmpList;
			BindChooseList();

			pnlPOSDialog.Visible = true;
		}

		protected void btnSave_Click(object sender, System.EventArgs e)
		{
			if (string.IsNullOrEmpty(ASSESSOR_POS)) {
				ScriptManager.RegisterStartupScript(this.Page, typeof(string), "Alert", "showAlert('เลือกผู้ประเมินหลัก');", true);
				return;
			}

			DataTable ST = SELECTED_PSN_POS();
			//If ST.Rows.Count = 0 Then
			//    ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "showAlert('เลือกตำแหน่งพนักงาน/ผู้ถูกประเมิน');", True)
			//    Exit Sub
			//End If

			SqlConnection Conn = new SqlConnection(BL.ConnectionString());
			Conn.Open();
			SqlCommand COMM = new SqlCommand();
			COMM.Connection = Conn;
			COMM.CommandType = CommandType.Text;

			//------------- DELETE PSN First -------------
            if (ST.Rows.Count > 0)
            {
                string PSNList = "";
                for (int i = 0; i <= ST.Rows.Count - 1; i++)
                {
                    PSNList += "'" + ST.Rows[i]["POS_NO"] + "',";
                }
                PSNList = " (" + PSNList.Substring(0, PSNList.Length - 1) + ") ";
                COMM.CommandText = "DELETE FROM tb_HR_Assessor_Pos WHERE POS_NO IN " + PSNList;
                COMM.ExecuteNonQuery();
            }
            else
            {

                //COMM.CommandText = "DELETE FROM tb_HR_Assessor_Pos WHERE ASSESSOR_POS ='" + ASSESSOR_POS + "' ";
                //COMM.ExecuteNonQuery();
            }




			DataTable DT = new DataTable();
			string SQL = "SELECT * FROM tb_HR_Assessor_Pos WHERE 1=0";
			SqlDataAdapter DA = new SqlDataAdapter(SQL, Conn);
			DA.Fill(DT);

            string listPSNL_NO = "";
			for (int i = 0; i <= ST.Rows.Count - 1; i++) {
				DataRow DR = DT.NewRow();
				DR["POS_NO"] = ST.Rows[i]["POS_NO"];
				DR["ASSESSOR_POS"] = ASSESSOR_POS;
				DR["Update_By"] = Session["USER_PSNL_NO"];
				DR["Update_Time"] = DateAndTime.Now;
				DT.Rows.Add(DR);
				SqlCommandBuilder cmd = new SqlCommandBuilder(DA);
				try {
					DA.Update(DT);

				} catch {
				}
				DT.AcceptChanges();
                if (ST.Rows[i]["PSNL_NO"].ToString() != "") {                 
                    listPSNL_NO += "'" + ST.Rows[i]["PSNL_NO"].ToString() + "'" + " ,"  ;
                } 
			}
            if (listPSNL_NO != ""){
                listPSNL_NO = "  " + listPSNL_NO.Substring(0, listPSNL_NO.Length - 1) ;
            }
            //Conn.Close();      


            //====START=====update ผู้ประเมินในตาราง  tb_HR_Actual_Assessor========================
            HRBL.HR_Round_Structure StructureInfo = BL.LastRound ();
          
            //---------ลบรายการ PSNL_NO ที่อยู่ใน LIST ตำแหน่งผู้ประเมินหลักที่เลือก-------------
            COMM.CommandText = "DELETE FROM tb_HR_Actual_Assessor WHERE PSNL_NO IN  (" + listPSNL_NO + ") AND  R_Year=" + StructureInfo.Year + " AND R_Round=" + StructureInfo.Round;
            COMM.ExecuteNonQuery();
            Conn.Close();

            //============ป้องกันการ update ตำแหน่ง ณ สิ้นรอบประเมินและปิดรอบประเมิน======
            Boolean IsInPeriod = BL.IsTimeInPeriod(DateTime.Now, StructureInfo.Year, StructureInfo.Round );
            Boolean IsRoundCompleted = BL.Is_Round_Completed(StructureInfo.Year, StructureInfo.Round );
            Boolean ckUpdate =  IsInPeriod & !IsRoundCompleted;
            if (ckUpdate)  //  ถ้ายังไม่สิ้นรอบ และไม่ปิดรอบ  เมื่อแก้ไขผู้ประเมิน จะอัพเดทให้ ณ ตาราง  tb_HR_Actual_Assessor
            {
                //--------ข้อมูลผู้ประเมิน---------
                string SQL_Info = "";               
                SQL_Info += " SELECT * FROM vw_PN_PSNL \n";
                SQL_Info += " WHERE POS_NO='" + ASSESSOR_POS + "'\n";
                SqlDataAdapter DA_Info = new SqlDataAdapter(SQL_Info, BL.ConnectionString());
                DataTable DT_Info = new DataTable();
                DA_Info.Fill(DT_Info);

                //  update ผู้ประเมินให้ตาม หน้ากำหนด tb_HR_Assessment_PSN
                DataTable DT_Assessment_PSN = new DataTable();
                string SQL_Assessment_PSN = "UPDATE tb_HR_Assessment_PSN SET PSN_ASSESSOR_POS='" + ASSESSOR_POS + "' WHERE PSNL_NO IN  (" + listPSNL_NO + ") AND  R_Year=" + StructureInfo.Year + " AND R_Round=" + StructureInfo.Round;
                SqlDataAdapter DA_Assessment_PSN = new SqlDataAdapter(SQL_Assessment_PSN, Conn);
                DA_Assessment_PSN.Fill(DT_Assessment_PSN);


                DataTable DT_Actual = new DataTable();
                string SQL_Actual = "SELECT * FROM tb_HR_Actual_Assessor WHERE 0=1";
                SqlDataAdapter DA_Actual = new SqlDataAdapter(SQL_Actual, Conn);
                DA_Actual.Fill(DT_Actual);

                //GetAssessmentPersonalInfo
                SqlCommandBuilder cmd = new SqlCommandBuilder(DA);
                DataRow  DR = null;
                DataTable TMP = DT_Info.Copy();

                if (TMP.DefaultView.Count > 0)
                {
                    for (int j = 0; j <= ST.Rows.Count - 1; j++) {
                        if (ST.Rows[j]["PSNL_NO"].ToString () != "") { 
                            DR = DT_Actual.NewRow();
                            DR["PSNL_NO"] = ST.Rows[j]["PSNL_NO"];
                            DR["R_Year"] = StructureInfo.Year;
                            DR["R_Round"] = StructureInfo.Round;
                            DR["MGR_ASSESSOR_POS"] = ASSESSOR_POS;
                            DR["MGR_PSNL_NO"] = TMP.DefaultView[0]["PSNL_NO"];
                            DR["MGR_PSNL_Fullname"] = TMP.DefaultView[0]["PSNL_Fullname"];
                            DR["MGR_PNPS_CLASS"] = TMP.DefaultView[0]["PNPS_CLASS"];
                            DR["MGR_PSNL_TYPE"] = TMP.DefaultView[0]["PSNL_TYPE"];
                            DR["MGR_POS_NO"] = TMP.DefaultView[0]["POS_NO"];
                            DR["MGR_WAGE_TYPE"] = TMP.DefaultView[0]["WAGE_TYPE"];
                            DR["MGR_WAGE_NAME"] = TMP.DefaultView[0]["WAGE_NAME"];
                            DR["MGR_DEPT_CODE"] = TMP.DefaultView[0]["DEPT_CODE"];
                            //DR("MGR_MINOR_CODE") = TMP.DefaultView(0).Item("MINOR_CODE")
                            DR["MGR_SECTOR_CODE"] = Strings.Left(TMP.DefaultView[0]["SECTOR_CODE"].ToString(), 2);
                            DR["MGR_SECTOR_NAME"] = TMP.DefaultView[0]["SECTOR_NAME"];
                            DR["MGR_DEPT_NAME"] = TMP.DefaultView[0]["DEPT_NAME"];
                            //DR("MGR_FN_ID") = TMP.DefaultView(0).Item("FN_ID")
                            //DR("MGR_FLD_Name") = TMP.DefaultView(0).Item("FLD_Name")
                            DR["MGR_FN_CODE"] = TMP.DefaultView[0]["FN_CODE"];
                            //DR("MGR_FN_TYPE") = TMP.DefaultView(0).Item("FN_TYPE")
                            DR["MGR_FN_NAME"] = TMP.DefaultView[0]["FN_NAME"];
                            if (!GL.IsEqualNull(TMP.DefaultView[0]["MGR_NAME"]))
                            {
                                DR["MGR_MGR_CODE"] = TMP.DefaultView[0]["MGR_CODE"];
                            }
                            if (!GL.IsEqualNull(TMP.DefaultView[0]["MGR_NAME"]))
                            {
                                DR["MGR_MGR_NAME"] = TMP.DefaultView[0]["MGR_NAME"];
                            }
                            try
                            {
                                DR["MGR_PNPS_RETIRE_DATE"] = (BL.GetPSNRetireDate(TMP.DefaultView[0]["PSNL_NO"].ToString()));
                            }
                            catch
                            {
                            }
                            DR["Update_By"] = Session["USER_PSNL_NO"];
                            DR["Update_Time"] = DateAndTime.Now;
                            DT_Actual.Rows.Add(DR);
                            cmd = new SqlCommandBuilder(DA_Actual);
                            DA_Actual.Update(DT_Actual);
                            }

           
                    }
                }                            
            }

            //====START=====update ผู้ประเมินในตาราง  tb_HR_Actual_Assessor========================













			ScriptManager.RegisterStartupScript(this.Page, typeof(string), "Alert", "showAlert('บันทึกสำเร็จ');", true);

			pnlList.Visible = true;
			pnlEdit.Visible = false;
			BindList();

		}
		public AssessmentSetting_Assessor()
		{
			Load += Page_Load;
		}


	}
}

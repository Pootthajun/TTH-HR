using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Xml.Linq;
using System.Diagnostics;
using System.Collections.Specialized;
using System.Configuration;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Caching;
using System.Web.SessionState;
using System.Web.Security;
using System.Web.Profile;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
namespace VB
{

	public partial class AssessmentSetting_GAP_MGR : System.Web.UI.Page
	{


		HRBL BL = new HRBL();

		GenericLib GL = new GenericLib();
		public int R_Year {
			get {
				try {
					return GL.CINT( GL.SplitString(ddlRound.Items[ddlRound.SelectedIndex].Value, "-")[0]);
				} catch (Exception ex) {
					return 0;
				}
			}
		}

		public int R_Round {
			get {
				try {
					return GL.CINT(GL.SplitString(ddlRound.Items[ddlRound.SelectedIndex].Value, "-")[1]);
				} catch (Exception ex) {
					return 0;
				}
			}
		}

		public int PSNL_CLASS_GROUP {
			get {
				try {
					return Convert.ToInt32(Conversion.Val(lblClassName.Attributes["CLSGP_ID"]));
				} catch (Exception ex) {
					return 0;
				}
			}
            set { lblClassName.Attributes["CLSGP_ID"] = value.ToString(); }
		}

		public int PSNL_TYPE {
			get {
				try {
					return Convert.ToInt32(Conversion.Val(lblPSNType.Attributes["PSNL_Type_Code"]));
				} catch (Exception ex) {
					return -1;
				}
			}
            set { lblPSNType.Attributes["PSNL_Type_Code"] = value.ToString(); }
		}

		public int MGR_No {
			get {
				try {
					return Convert.ToInt32(Conversion.Val(lblCOMPNo.Attributes["MGR_No"]));
				} catch (Exception ex) {
					return 0;
				}
			}
			set {
                lblCOMPNo.Attributes["MGR_No"] = value.ToString();
				lblCOMPNo.Text = value.ToString();
				//.PadLeft(2, GL.chr0)
			}
		}

		public string EXIST_GAP {
			get { return lblCOMPNo.Attributes["EXIST_GAP"]; }
			set { lblCOMPNo.Attributes["EXIST_GAP"] = value; }
		}


		protected void Page_Load(object sender, System.EventArgs e)
		{
			if ((Session["USER_PSNL_NO"] == null)) {
				ScriptManager.RegisterStartupScript(this.Page, typeof(string), "Alert", "showAlert('กรุณาลงชื่อเข้าใช้ระบบ');", true);
				Response.Redirect("Login.aspx");
				return;
			}

			if (!IsPostBack) {
				BL.BindDDlYearRound(ddlRound);
				ddlRound.Items.RemoveAt(0);
				BL.BindDDlCOMPClassGroup(ddlClassGroup, R_Year, R_Round);
				BL.BindDDlPSNType(ddlPSNType);
				BindCOMPList();
				pnlList.Visible = true;
				pnlEdit.Visible = false;
				ModalGAP.Visible = false;
			}
		}

		private void BindCOMPList()
		{
			string SQL = "";
			SQL += " SELECT DISTINCT CLSGP_ID\n";
			SQL += " ,PNPO_CLASS_Start,PNPO_CLASS_End\n";
			SQL += " ,PSNL_Type_Code,PSNL_Type_Name\n";
			SQL += " ,TOTAL_COMP,COMP_GAP\n";
			SQL += " FROM vw_HR_COMP_Master_MGR_Header \n";
			SQL += " WHERE R_Year=" + R_Year + " AND R_Round=" + R_Round + "\n";

			if (ddlClassGroup.SelectedIndex > 0) {
				SQL += " AND CLSGP_ID=" + ddlClassGroup.Items[ddlClassGroup.SelectedIndex].Value + "\n";
			}
			if (ddlPSNType.SelectedIndex > 0) {
				SQL += " AND PSNL_Type_Code=" + ddlPSNType.Items[ddlPSNType.SelectedIndex].Value + "\n";
			}
			if (chkBlank.Checked) {
				SQL += " AND (TOTAL_COMP=0 OR COMP_GAP<>TOTAL_COMP) \n";
			}

			SQL += " ORDER BY PNPO_CLASS_Start,PSNL_Type_Code DESC";

			SqlDataAdapter DA = new SqlDataAdapter(SQL, BL.ConnectionString());
			DataTable DT = new DataTable();
			DA.Fill(DT);

			Session["AssessmentSetting_GAP_MGR"] = DT;
			Pager.SesssionSourceName = "AssessmentSetting_GAP_MGR";
			Pager.RenderLayout();

			if (DT.Rows.Count == 0) {
				lblCountList.Text = "ไม่พบรายการดังกล่าว";
			} else {
				lblCountList.Text = "พบ " + GL.StringFormatNumber(DT.Rows.Count, 0) + " รายการ";
			}

		}

		protected void Pager_PageChanging(PageNavigation Sender)
		{
			Pager.TheRepeater = rptList;
		}

		protected void Search(object sender, System.EventArgs e)
		{
			BindCOMPList();
		}

		protected void ddlRound_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			BL.BindDDlCOMPClassGroup(ddlClassGroup, R_Year, R_Round);
			BindCOMPList();
		}

		protected void rptList_ItemCommand(object source, System.Web.UI.WebControls.RepeaterCommandEventArgs e)
		{
            Button btnEdit = (Button)e.Item.FindControl("btnEdit");
			switch (e.CommandName) {
				case "Edit":
					

					PSNL_CLASS_GROUP = GL.CINT ( btnEdit.Attributes["CLSGP_ID"]);
					PSNL_TYPE = GL.CINT (btnEdit.Attributes["PSNL_Type_Code"]);
					//----------------Bind COMP Header-----------------
					DataTable DT = BL.GetCOMPMGRHeader(R_Year, R_Round, PSNL_CLASS_GROUP, PSNL_TYPE);
					if (DT.Rows.Count == 0) {
						ScriptManager.RegisterStartupScript(this.Page, typeof(string), "Alert", "showAlert('ไม่พบหัวข้อการประเมิน');", true);
						BindCOMPList();
						return;
					}

					lblPSNType.Text = DT.Rows[0]["PSNL_Type_Name"].ToString ();
					int mn = Convert.ToInt32(DT.Rows[0]["PNPO_CLASS_Start"]);
					int mx = Convert.ToInt32(DT.Rows[0]["PNPO_CLASS_End"]);
					if (mn == mx) {
						lblClassName.Text = GL.CINT (mn).ToString ();
					} else {
						lblClassName.Text = mn + " - " + mx;
					}
					lblYearRound.Text = "ปี " + R_Year + " รอบ " + R_Round;

					BindCOMPDetail();
					//------------ Toggle show----------
					pnlList.Visible = false;
					pnlEdit.Visible = true;
					divView.Visible = true;
					ModalGAP.Visible = false;

					break;
				case "Delete":
					

					SqlConnection Conn = new SqlConnection(BL.ConnectionString());
					Conn.Open();
					SqlCommand Comm = new SqlCommand();
					var _with1 = Comm;
					_with1.CommandType = CommandType.Text;

					_with1.CommandText = "DELETE FROM tb_HR_COMP_Master_MGR_GAP WHERE \n";
					_with1.CommandText += "R_Year=" + R_Year + " AND \n";
					_with1.CommandText += "R_Round=" + R_Round + " AND \n";
					_with1.CommandText += "CLSGP_ID=" + btnEdit.Attributes["CLSGP_ID"] + " AND \n";
					_with1.CommandText += "PNPO_TYPE=" + btnEdit.Attributes["PSNL_Type_Code"] + " \n";
					_with1.Connection = Conn;
					_with1.ExecuteNonQuery();
					_with1.Dispose();

					Conn.Close();
					BindCOMPList();

					break;
			}
		}

		private void BindCOMPDetail()
		{
			//----------------Bind COMP Detail-----------------
			HRBL.DataManager DM = BL.GetCOMPMasterMGRDetail(R_Year, R_Round, PSNL_CLASS_GROUP, PSNL_TYPE);
			DataTable DT = DM.Table;
			//-------- Keep GAP-----------
			GAP = BL.GetCOMPMasterMGRGAP(R_Year, R_Round, PSNL_CLASS_GROUP, PSNL_TYPE).Table;
			rptCOMP.DataSource = DT;
			rptCOMP.DataBind();
		}

		//------------ For Grouping -----------
		string LastClass = "";
		protected void rptList_ItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
		{
			if (e.Item.ItemType != ListItemType.AlternatingItem & e.Item.ItemType != ListItemType.Item)
				return;

			Label lblClassGroup =(Label) e.Item.FindControl("lblClassGroup");
			Label lblPSNType =(Label) e.Item.FindControl("lblPSNType");
			Label lblTotalCOMP =(Label) e.Item.FindControl("lblTotalCOMP");
			HtmlGenericControl iconGAP = (HtmlGenericControl) e.Item.FindControl("iconGAP");
			Button btnEdit =(Button) e.Item.FindControl("btnEdit");
			Button btnDelete =(Button) e.Item.FindControl("btnDelete");
			AjaxControlToolkit.ConfirmButtonExtender cfm_Delete =(AjaxControlToolkit.ConfirmButtonExtender) e.Item.FindControl("cfm_Delete");

            DataRowView drv = (DataRowView)e.Item.DataItem;
			int mn = GL.CINT (drv["PNPO_CLASS_Start"]);
			int mx = Convert.ToInt32(drv["PNPO_CLASS_End"]);
			string ClassName = "";
			if (mn == mx) {
				ClassName =GL.CINT ( mn).ToString ();
			} else {
				ClassName = mn + " - " + mx;
			}

			if (ClassName != LastClass) {
				lblClassGroup.Text = ClassName;
				LastClass = ClassName;
			}

			lblPSNType.Text = drv["PSNL_Type_Name"].ToString ();
			lblTotalCOMP.Text = drv["TOTAL_COMP"].ToString ();

			if (GL.CINT (drv["TOTAL_COMP"]) == 0) {
				iconGAP.Attributes["class"] = "icon-minus";
				iconGAP.Style["color"] = "silver";
				iconGAP.Attributes["Title"] = "ไม่พบหัวข้อการประเมิน";
			} else if (!GL.IsEqualNull(drv["COMP_GAP"]) && GL.CINT (drv["COMP_GAP"]) == 0) {
				iconGAP.Attributes["class"] = "icon-minus";
				iconGAP.Style["color"] = "silver";
				iconGAP.Attributes["Title"] = "ยังไม่ได้ระบหลักสูตร";
			} else if (!GL.IsEqualNull(drv["COMP_GAP"]) && drv["COMP_GAP"] == drv["TOTAL_COMP"]) {
				iconGAP.Attributes["class"] = "icon-ok-sign";
				iconGAP.Style["color"] = "green";
				iconGAP.Attributes["Title"] = "กำหนดหลักสูตรแล้ว";
			} else {
				iconGAP.Attributes["class"] = "icon-pencil";
				iconGAP.Style["color"] = "red";
				iconGAP.Attributes["Title"] = "กำหนดหลักสูตรยังไม่ครบ";
			}

			cfm_Delete.ConfirmText = "ยืนยันยกเลิกหลักสูตรทั้งหมดของ พนักงาน" + lblPSNType.Text + " กลุ่มระดับ " + ClassName;

			btnDelete.Visible = GL.CINT (drv["COMP_GAP"]) > 0;

			btnEdit.Attributes["CLSGP_ID"] = drv["CLSGP_ID"].ToString ();
			btnEdit.Attributes["PSNL_Type_Code"] = drv["PSNL_Type_Code"].ToString ();

		}

		protected void rptCOMP_ItemCommand(object source, System.Web.UI.WebControls.RepeaterCommandEventArgs e)
		{

            HtmlTableCell ass_no = (HtmlTableCell)e.Item.FindControl("ass_no");
            switch (e.CommandName) {
				case "Add":

                    Repeater rptGAP = (Repeater)e.Item.FindControl("rptGAP");

					//------------- Get Existing GAP List ---------------
					string _gaplist = "";
					foreach (RepeaterItem item in rptGAP.Items) {
						if (item.ItemType != ListItemType.AlternatingItem & item.ItemType != ListItemType.Item)
							continue;
                        HtmlAnchor lblDelete = (HtmlAnchor)item.FindControl("lblDelete");
						_gaplist += lblDelete.Attributes["GAP_ID"] + ",";
					}


					//----------- Bind Needed Information ---------------
					MGR_No = GL.CINT(ass_no.InnerHtml);
					if (!string.IsNullOrEmpty(_gaplist)) {
						EXIST_GAP = _gaplist.Substring(0, _gaplist.Length - 1);
					} else {
						EXIST_GAP = "";
					}

					BindGAPList();
					ModalGAP.Visible = true;
					ScriptManager.RegisterStartupScript(this.Page, typeof(string), "Focus", "document.getElementById('" + txtSearchGAP.ClientID + "').focus();", true);
					break;
				case "Delete":
                    TextBox txtDeleteID = (TextBox)e.Item.FindControl("txtDeleteID");
                    String GAP_ID = e.CommandArgument.ToString();				

					SqlConnection Conn = new SqlConnection(BL.ConnectionString());
					Conn.Open();
					SqlCommand Comm = new SqlCommand();
					var _with2 = Comm;
					_with2.CommandType = CommandType.Text;

					_with2.CommandText = "DELETE FROM tb_HR_COMP_Master_MGR_GAP WHERE \n";
					_with2.CommandText += "R_Year=" + R_Year + " AND \n";
					_with2.CommandText += "R_Round=" + R_Round + " AND \n";
					_with2.CommandText += "CLSGP_ID=" + PSNL_CLASS_GROUP + " AND \n";
					_with2.CommandText += "PNPO_TYPE=" + PSNL_TYPE + " AND \n";
					_with2.CommandText += "MGR_No=" + ass_no.InnerHtml + " AND \n";
					_with2.CommandText += "GAP_ID=" + GAP_ID + "\n";
					_with2.Connection = Conn;
					_with2.ExecuteNonQuery();
					_with2.Dispose();

					Conn.Close();
				    OrderGAPNo(PSNL_CLASS_GROUP, PSNL_TYPE, GL.CINT(ass_no.InnerHtml));
					ModalGAP.Visible = false;
					BindCOMPDetail();
					break;
			}
		}

		protected void btnCancel_Click(object sender, System.EventArgs e)
		{
			BindCOMPList();
			pnlList.Visible = true;
			pnlEdit.Visible = false;
			ModalGAP.Visible = false;
		}

		DataTable GAP = null;
		protected void rptCOMP_ItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
		{
			if (e.Item.ItemType != ListItemType.AlternatingItem & e.Item.ItemType != ListItemType.Item)
				return;

            HtmlTableCell ass_no = (HtmlTableCell)e.Item.FindControl("ass_no");
            HtmlTableCell COMP_Name = (HtmlTableCell)e.Item.FindControl("COMP_Name");
            HtmlTableCell target = (HtmlTableCell)e.Item.FindControl("target");
            HtmlTableCell GAPList = (HtmlTableCell)e.Item.FindControl("GAPList");

            DataRowView drv = (DataRowView)e.Item.DataItem;
			ass_no.InnerHtml = drv["MGR_No"].ToString ();
			COMP_Name.InnerHtml = drv["MGR_Comp"].ToString().Replace("\n", "<br>");
			target.InnerHtml = drv["MGR_Std"].ToString().Replace("\n", "<br>");

			
			//-------------- Bind GAP ----------------
			DataTable tmp = new DataTable();
			tmp = GAP.Copy();
			tmp.DefaultView.Sort = "GAP_No";
			tmp.DefaultView.RowFilter = "CLSGP_ID=" + PSNL_CLASS_GROUP + " AND PNPO_TYPE=" + PSNL_TYPE + " AND MGR_No=" + drv["MGR_No"];
			tmp = tmp.DefaultView.ToTable();

			Repeater rptGAP =(Repeater) e.Item.FindControl("rptGAP");
			rptGAP.ItemDataBound += rptGAP_ItemDataBound;
			rptGAP.DataSource = tmp;
			rptGAP.DataBind();
		}

		protected void rptGAP_ItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
		{
			if (e.Item.ItemType != ListItemType.AlternatingItem & e.Item.ItemType != ListItemType.Item)
				return;
            Label lblName = (Label)e.Item.FindControl("lblName");
            HtmlAnchor lblDelete = (HtmlAnchor)e.Item.FindControl("lblDelete");
            TextBox txtDeleteID = (TextBox)e.Item.FindControl("txtDeleteID");
            Button btnDelete = (Button)e.Item.FindControl("btnDelete");

            DataRowView drv = (DataRowView)e.Item.DataItem;
			lblName.Text = drv["GAP_No"].ToString() + ". " + drv["GAP_Name"].ToString();
			lblName.Attributes["MGR_No"] = drv["MGR_No"].ToString ();
			lblDelete.Attributes["GAP_ID"] = drv["GAP_ID"].ToString ();
            //lblDelete.Attributes["onClick"] = "document.getElementById('" + txtDeleteID.ClientID.ToString () + "').value=" + drv["GAP_ID"] + "; document.getElementById('" + btnDelete.ClientID + "').click();";
            btnDelete.CommandArgument = drv["GAP_ID"].ToString();
		}

        protected void rptGAP_ItemCommand(object source, System.Web.UI.WebControls.RepeaterCommandEventArgs e)
        {
            switch (e.CommandName)
            {

                case "Delete":
                    Label lblName = (Label)e.Item.FindControl("lblName");
                    TextBox txtDeleteID = (TextBox)e.Item.FindControl("txtDeleteID");
                    Button btnDelete = (Button)e.Item.FindControl("btnDelete");
                    String GAP_ID = e.CommandArgument.ToString();
                    SqlConnection Conn = new SqlConnection(BL.ConnectionString());
                    Conn.Open();
                    SqlCommand Comm = new SqlCommand();
                    var _with2 = Comm;
                    _with2.CommandType = CommandType.Text;

                    _with2.CommandText = "DELETE FROM tb_HR_COMP_Master_MGR_GAP WHERE \n";
					_with2.CommandText += "R_Year=" + R_Year + " AND \n";
					_with2.CommandText += "R_Round=" + R_Round + " AND \n";
					_with2.CommandText += "CLSGP_ID=" + PSNL_CLASS_GROUP + " AND \n";
					_with2.CommandText += "PNPO_TYPE=" + PSNL_TYPE + " AND \n";
                    _with2.CommandText += "MGR_No=" + lblName.Attributes["MGR_No"] + " AND \n";
					_with2.CommandText += "GAP_ID=" + GAP_ID + "\n";
					_with2.Connection = Conn;
					_with2.ExecuteNonQuery();
					_with2.Dispose();

					Conn.Close();
                    OrderGAPNo(PSNL_CLASS_GROUP, PSNL_TYPE, GL.CINT(lblName.Attributes["MGR_No"]));
					ModalGAP.Visible = false;
					BindCOMPDetail();
					break;

            }
        }

		protected void rptGAPDialog_ItemCommand(object source, System.Web.UI.WebControls.RepeaterCommandEventArgs e)
		{
			switch (e.CommandName) {
				case "Select":
					Button btnGAPSelect =(Button) e.Item.FindControl("btnGAPSelect");
					string GAP_ID = btnGAPSelect.CommandArgument;

					string SQL = "SElECT * FROM tb_HR_COMP_Master_MGR_GAP \n";
					SQL += " WHERE R_Year=" + R_Year + " AND R_Round=" + R_Round + "\n";
					SQL += " AND CLSGP_ID=" + PSNL_CLASS_GROUP + "\n";
					SQL += " AND PNPO_TYPE=" + PSNL_TYPE + "\n";
					SQL += " AND MGR_No=" + MGR_No + "\n";
					SQL += " AND GAP_ID=" + GAP_ID + "\n";

					DataTable DT = new DataTable();
					SqlDataAdapter DA = new SqlDataAdapter(SQL, BL.ConnectionString());
					DA.Fill(DT);

					DataRow DR = null;
					if (DT.Rows.Count == 0) {
						DR = DT.NewRow();
						DR["GAP_ID"] = GAP_ID;
						DR["R_Year"] = R_Year;
						DR["R_Round"] = R_Round;
						DR["CLSGP_ID"] = PSNL_CLASS_GROUP;
						DR["PNPO_TYPE"] = PSNL_TYPE;
						DR["MGR_No"] = MGR_No;
					} else {
						DR = DT.Rows[0];
					}
					DR["Update_By"] = Session["USER_PSNL_NO"];
					DR["Update_Time"] = DateAndTime.Now;
					if (DT.Rows.Count == 0)
						DT.Rows.Add(DR);

					SqlCommandBuilder cmd = new SqlCommandBuilder();
					cmd = new SqlCommandBuilder(DA);
					DA.Update(DT);
					//--------- Order GAP_No-------
					OrderGAPNo(PSNL_CLASS_GROUP, PSNL_TYPE, MGR_No);
					BindCOMPDetail();
					ModalGAP.Visible = false;
					break;
			}
		}

		private void OrderGAPNo(int CLSGP_ID, int PNPO_TYPE, int MGR_No)
		{
			string SQL = "SElECT * FROM tb_HR_COMP_Master_MGR_GAP \n";
			SQL += " WHERE R_Year=" + R_Year + " AND R_Round=" + R_Round + "\n";
			SQL += " AND CLSGP_ID=" + CLSGP_ID + "\n";
			SQL += " AND PNPO_TYPE=" + PNPO_TYPE + "\n";
			SQL += " AND MGR_No=" + MGR_No + "\n";
			SQL += " ORDER BY ISNULL(GAP_NO,99999999) ASC";
			DataTable DT = new DataTable();
			SqlDataAdapter DA = new SqlDataAdapter(SQL, BL.ConnectionString());
			DA.Fill(DT);
			for (int i = 0; i <= DT.Rows.Count - 1; i++) {
				DT.Rows[i]["GAP_NO"] = i + 1;
			}
			SqlCommandBuilder cmd = new SqlCommandBuilder();
			cmd = new SqlCommandBuilder(DA);
			try {
				DA.Update(DT);
			} catch {
			}
		}

		protected void rptGAPDialog_ItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
		{
			if (e.Item.ItemType != ListItemType.AlternatingItem & e.Item.ItemType != ListItemType.Item)
				return;
			Label lblNo =(Label) e.Item.FindControl("lblNo");
			Label lblName =(Label) e.Item.FindControl("lblName");
			Button btnGAPSelect =(Button) e.Item.FindControl("btnGAPSelect");

            DataRowView drv = (DataRowView)e.Item.DataItem;
			lblNo.Text = drv["GAP_ID"].ToString().PadLeft(3, GL.chr0);
			lblName.Text = drv["GAP_Name"].ToString ();
			btnGAPSelect.CommandArgument = drv["GAP_ID"].ToString ();
		}

		#region "GAPDialog"

		protected void SearchGAP(object sender, System.EventArgs e)
		{
			BindGAPList();
		}

		private void BindGAPList()
		{
			string SQL = "";
			SQL += " SELECT GAP.GAP_ID,GAP_Name,GAP_Desc,Is_Active,\n";
			SQL += " SUM(CASE COMP_Type_ID WHEN 1 THEN 1 ELSE 0 END) Core,\n";
			SQL += " SUM(CASE COMP_Type_ID WHEN 2 THEN 1 ELSE 0 END) FN,\n";
			SQL += " SUM(CASE COMP_Type_ID WHEN 3 THEN 1 ELSE 0 END) MGR\n";
			SQL += " FROM vw_HR_GAP GAP\n";
			SQL += " LEFT JOIN\n";
			SQL += " \t\t(\n";
			SQL += " \t\t\tSELECT GAP_ID,1 COMP_Type_ID FROM tb_HR_COMP_Master_Core_GAP\n";
			SQL += " \t\t\tUNION ALL \n";
			SQL += " \t\t\tSELECT GAP_ID,2 COMP_Type_ID FROM tb_HR_COMP_Master_FN_GAP\n";
			SQL += " \t\t\tUNION ALL\n";
			SQL += " \t\t\tSELECT GAP_ID,3 COMP_Type_ID FROM tb_HR_COMP_Master_MGR_GAP\n";
			SQL += " \t\t) COMP ON GAP.GAP_ID=COMP.GAP_ID\n";

			string Filter = "";
			if (!string.IsNullOrEmpty(txtSearchGAP.Text)) {
				Filter += " (GAP_Name LIKE '%" + txtSearchGAP.Text.Replace("'", "''") + "%' OR \n";
				Filter += " GAP.GAP_ID LIKE '%" + txtSearchGAP.Text.Replace("'", "''") + "%' OR ";
				Filter += " GAP_Desc LIKE '%" + txtSearchGAP.Text.Replace("'", "''") + "%') AND ";
			}

			if (!string.IsNullOrEmpty(EXIST_GAP)) {
				Filter += " GAP.GAP_ID NOT IN (" + EXIST_GAP + ") AND ";
			}
			Filter += " Is_Active=1 AND ";

			if (!string.IsNullOrEmpty(Filter)) {
				SQL += " WHERE " + Filter.Substring(0, Filter.Length - 4) + "\n";
			}
			SQL += " \n";
			SQL += " GROUP BY GAP.GAP_ID,GAP_Name,GAP_Desc,Is_Active\n";
			SQL += " ORDER BY GAP.GAP_ID\n";

			SqlDataAdapter DA = new SqlDataAdapter(SQL, BL.ConnectionString());
			DataTable DT = new DataTable();
			DA.Fill(DT);

			DT.Columns.Add("GAP_Order");
			for (int i = 0; i <= DT.Rows.Count - 1; i++) {
				DT.Rows[i]["GAP_Order"] = i + 1;
			}

			Session["AssessmentSetting_GAP_MGR_GAPList"] = DT;
			PagerGAP.SesssionSourceName = "AssessmentSetting_GAP_MGR_GAPList";
			PagerGAP.RenderLayout();

			if (DT.Rows.Count == 0) {
				lblCountGAP.Text = "ไม่พบรายการดังกล่าว";
			} else {
				lblCountGAP.Text = "พบ " + GL.StringFormatNumber(DT.Rows.Count, 0) + " รายการ";
			}

		}

		protected void PagerGAP_PageChanging(PageNavigation Sender)
		{
			PagerGAP.TheRepeater = rptGAPDialog;
		}

		protected void btnClose_Click(object sender, System.EventArgs e)
		{
			ModalGAP.Visible = false;
		}
		public AssessmentSetting_GAP_MGR()
		{
			Load += Page_Load;
		}

		#endregion

	}
}

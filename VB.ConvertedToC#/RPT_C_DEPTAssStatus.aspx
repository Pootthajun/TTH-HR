﻿<%@ Page  Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPage.Master" CodeBehind="RPT_C_DEPTAssStatus.aspx.cs" Inherits="VB.RPT_C_DEPTAssStatus" %>

<%@ Register src="WUC_DateReporter.ascx" tagname="WUC_DateReporter" tagprefix="uc1" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<%@ Register src="PageNavigation.ascx" tagname="PageNavigation" tagprefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">      
<ContentTemplate>
<asp:ToolkitScriptManager ID="toolkit1" runat="server"></asp:ToolkitScriptManager>

<div class="container-fluid">
				<!-- BEGIN PAGE HEADER-->
				<div class="row-fluid" id="fixedPageHeader">
					<div class="span12"> 
						<!-- BEGIN PAGE TITLE & BREADCRUMB-->
						<h3>สถานะการประเมินของหน่วยงาน<font color="blue">(ScreenID : R-COMP-05)</font></h3>						
						<ul class="breadcrumb">
                            <li>
                            	<i class="icon-copy"></i> <a href="javascript:;">รายงาน</a><i class="icon-angle-right"></i>
                            </li>
                            <li>
                            	<i class="icon-copy"></i> <a href="javascript:;">การประเมินผลสมรรถนะ</a><i class="icon-angle-right"></i>
                            </li>
                            <li>
                            	<i class="icon-copy"></i> <a href="javascript:;">รายงานประจำรอบ</a><i class="icon-angle-right"></i>
                            </li>
                        	<li>
                        	    <i class="icon-check"></i> <a href="javascript:;">สถานะการประเมินของหน่วยงาน</a>
                        	</li>                      	
                        	
                            <uc1:WUC_DateReporter ID="WUC_DateReporter1" runat="server" />
                        </ul>
						<!-- END PAGE TITLE & BREADCRUMB-->
									        
				     </div>

				
			    </div>
		        <!-- END PAGE HEADER-->
				
                <asp:Panel ID="pnlList" runat="server" Visible="True">
				<div class="row-fluid">
					
					<div class="span12">
						<!-- BEGIN SAMPLE TABLE PORTLET-->
                                         <div class="row-fluid form-horizontal">
                			                  <div class="btn-group pull-left">
                                                    <asp:Button ID="btnSearch" OnClick="btnSearch_Click"  runat="server" CssClass="btn green" Text="ค้นหา" />
                                              </div> 
												<div class="btn-group pull-right">                                    
										            <button data-toggle="dropdown" class="btn dropdown-toggle" id="Button1">พิมพ์ <i class="icon-angle-down"></i></button>										
										            <ul class="dropdown-menu pull-right">
                                                       
                                                        <li><asp:LinkButton ID="btnPDF" runat="server"  >รูปแบบ PDF</asp:LinkButton></li>
                                                        <li><asp:LinkButton ID="btnExcel" runat="server"  >รูปแบบ Excel</asp:LinkButton></li>											
										            </ul>
									            </div>
                                         </div>
						                 <div class="row-fluid form-horizontal">
						                             <div class="span4 ">
														<div class="control-group">
													        <label class="control-label"> รอบการประเมิน</label>
													        <div class="controls">
														        <asp:DropDownList ID="ddlRound" OnSelectedIndexChanged="btnSearch_Click" runat="server" AutoPostBack="false" CssClass="medium m-wrap">
							                                    </asp:DropDownList>
													        </div>
												        </div>
													</div>
												     <div class="span4 ">
														<div class="control-group">
													        <label class="control-label"> ฝ่าย</label>
													        <div class="controls">
														        <asp:DropDownList ID="ddlSector" OnSelectedIndexChanged="ddlSector_SelectedIndexChanged" runat="server" AutoPostBack="true" CssClass="medium m-wrap">
														        </asp:DropDownList>
													        </div>
												        </div>
													</div>	
													
													 <div class="span3 ">
														<div class="control-group">
													        <label class="control-label"> หน่วยงาน</label>
													        <div class="controls">
														        <asp:DropDownList ID="ddlDept" OnSelectedIndexChanged="btnSearch_Click" runat="server" AutoPostBack="false" CssClass="medium m-wrap">
														        </asp:DropDownList>	 	
													        </div>
												        </div>
													</div>											   
												</div>
												
								            
							<div class="portlet-body no-more-tables">
								<asp:Label ID="lblCountPSN" runat="server" Font-Size="14px" Font-Bold="true"></asp:Label>
								&nbsp;
								<table class="table table-striped table-full-width table-bordered dataTable table-hover">
									<thead>
										<tr>
											<th> ฝ่าย</th>
											<th> หน่วยงาน</th>
											<th> ยังไม่กำหนดแบบประเมิน</th>
											<th> รออนุมัติแบบประเมิน</th>
											<th> อนุมัติแบบประเมินแล้ว</th>
											<th> พนักงานประเมินตนเอง</th>
											<th> รออนุมัติผลการประเมิน</th>
											<th> ประเมินเสร็จสมบูรณ์</th>
										</tr>
									</thead>
									<tbody>
										<asp:Repeater ID="rptKPI" OnItemDataBound="rptKPI_ItemDataBound" runat="server">
									        <ItemTemplate>
									            <tr>    
									                <td data-title="ฝ่าย"><asp:Label ID="lblSector" runat="server"></asp:Label></td>                                    
											        <td data-title="หน่วยงาน"><asp:Label ID="lblPSNOrganize" runat="server"></asp:Label></td>
											        <td data-title="ยังไม่กำหนดแบบประเมิน" style="text-align: center"><asp:Label ID="lblNoneAss" runat="server"></asp:Label></td>
											        <td data-title="รออนุมัติแบบประเมิน" style="text-align: center"><asp:Label ID="lblWaitAss" runat="server"></asp:Label></td>
											        <td data-title="อนุมัติแบบประเมินแล้ว" style="text-align: center"><asp:Label ID="lblApproveAss" runat="server"></asp:Label></td>
											        <td data-title="พนักงานประเมินตนเอง" style="text-align: center"><asp:Label ID="lblPSNLAss" runat="server"></asp:Label></td>
											        <td data-title="รออนุมัติผลการประเมิน" style="text-align: center"><asp:Label ID="lblWaitResults" runat="server"></asp:Label></td>
											        <td data-title="ประเมินเสร็จสมบูรณ์" style="text-align: center"><asp:Label ID="lblComplete" runat="server"></asp:Label></td>
										        </tr>	
									        </ItemTemplate>
										</asp:Repeater>
																							
									</tbody>
								</table>
								<asp:PageNavigation ID="Pager" OnPageChanging="Pager_PageChanging" MaximunPageCount="10" PageSize="20" runat="server" />
							</div>
					</div>
                </div>  
                </asp:Panel>  


</div>

</ContentTemplate>           
</asp:UpdatePanel>					 
</asp:Content>

using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Xml.Linq;
using System.Diagnostics;
using System.Collections.Specialized;
using System.Configuration;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Caching;
using System.Web.SessionState;
using System.Web.Security;
using System.Web.Profile;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
namespace VB
{

	public partial class RPT_IDP_PSN : System.Web.UI.Page
	{


		HRBL BL = new HRBL();

		GenericLib GL = new GenericLib();
		public int R_Year {
			get {
				try {
					return GL.CINT( GL.SplitString(ddlRound.Items[ddlRound.SelectedIndex].Value, "-")[0]);
				} catch (Exception ex) {
					return 0;
				}
			}
		}

		public int R_Round {
			get {
				try {
					return GL.CINT(GL.SplitString(ddlRound.Items[ddlRound.SelectedIndex].Value, "-")[1]);
				} catch (Exception ex) {
					return 0;
				}
			}
		}

		protected void Page_Load(object sender, System.EventArgs e)
		{
			if ((Session["USER_PSNL_NO"] == null)) {
				ScriptManager.RegisterStartupScript(this.Page, typeof(string), "Alert", "showAlert('กรุณาลงชื่อเข้าใช้ระบบ');", true);
				Response.Redirect("Login.aspx");
				return;
			}


			if (!IsPostBack) {
				//'------------ Do it From Team COMP ---------
				//BL.BindDDlSector(ddlSector)
				//'------------ Remove อยส -----------
				//For i As Integer = 0 To ddlSector.Items.Count - 1
				//    If ddlSector.Items(i).Value = "10" Then
				//        ddlSector.Items.RemoveAt(i)
				//        Exit For
				//    End If
				//Next
				//ddl_Search_Class
				BL.BindDDlYearRound(ddlRound);
				//: ddlRound.Items.RemoveAt(0)
				BL.BindDDlCOMPClassGroup(ddl_Search_Class, R_Year, R_Round);

				BindPersonalList();
			}
		}

        protected void btnSearch_Click(object sender, System.EventArgs e)
		{
          
			BindPersonalList();

			//If pnlEdit.Visible Then
			//    BindPersonal()
			//    BindAssessor()
			//    BindMasterCOMP()
			//End If
		}

		protected void ddlRound_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			BL.BindDDlCOMPClassGroup(ddl_Search_Class, R_Year, R_Round);
			BindPersonalList();
		}

		private void BindPersonalList()
		{
            string LastRound = "";
            string LastSectorName = "";
            string LastOrgranizeName = "";
			string SQL = " SELECT DISTINCT YearRound.R_Year,YearRound.R_Round,vw.SECTOR_CODE,vw.SECTOR_NAME,vw.DEPT_CODE,vw.DEPT_NAME,vw.PSNL_NO,vw.PSNL_Fullname,ISNULL(Header.PNPS_CLASS,vw.PNPS_CLASS) PNPS_CLASS," + "\n";
			SQL += "  CLS.CLSGP_ID,vw.WAGE_NAME,ISNULL(vw.MGR_NAME,vw.FN_NAME) POS_Name,vw.POS_NO,vw.PNPS_CLASS" + "\n";
			SQL += "  ,Header.COMP_Status,dbo.udf_GetAssessmentStatusName(Header.COMP_Status) COMP_Status_Name  " + "\n";
			SQL += "  FROM tb_HR_Round YearRound CROSS JOIN vw_PN_PSNL vw " + "\n";
			SQL += "  LEFT JOIN tb_HR_COMP_Header Header ON vw.PSNL_NO=Header.PSNL_NO" + "\n";
			SQL += " \t\t\t\t\t\t\t\t\tAND  Header.R_Year=YearRound.R_Year AND Header.R_Round=YearRound.R_Round" + "\n";
			SQL += "  LEFT JOIN tb_HR_PNPO_CLASS_GROUP CLS " + "\n";
			SQL += " \t\t\tON ISNULL(Header.PNPS_CLASS,vw.PNPS_CLASS) BETWEEN CLS.PNPO_CLASS_Start AND CLS.PNPO_CLASS_End" + "\n";
			SQL += " \t\t\tAND CLS.R_Year=YearRound.R_Year AND CLS.R_Round=YearRound.R_Round" + "\n";
			SQL += " \t\t\t" + "\n";

			if (chkIsSet.Checked) {
				SQL += " INNER JOIN (SELECT * FROM vw_COMP_MGR_GAP" + "\n";
				SQL += " UNION ALL " + "\n";
				SQL += " SELECT * FROM vw_COMP_CORE_GAP" + "\n";
				SQL += " UNION ALL " + "\n";
				SQL += " SELECT * FROM vw_COMP_FN_GAP) GAP ON YearRound.R_Year=GAP.R_Year AND YearRound.R_Round=GAP.R_Round" + "\n";
                SQL += "  AND vw.PSNL_NO=GAP.PSNL_NO AND GAP.Selected=1  --AND GAP.Final_Score < 3 " + "\n";
			}
            string Filter =" ";
			SQL += "  WHERE vw.SECTOR_CODE<>'10'" + "\n";
           

			if (ddlRound.SelectedIndex > 0) {
                //SQL += " AND YearRound.R_Year=" + R_Year + " AND YearRound.R_Round=" + R_Round + "\n";
                Filter += "   R_Year=" + R_Year + " AND R_Round=" + R_Round + "\n";
			}
			//If ddlSector.SelectedIndex > 0 Then
			//    SQL &= " AND vw.SECTOR_CODE='" & ddlSector.Items(ddlSector.SelectedIndex).Value & "' " & vbLf
			//End If

			if (!string.IsNullOrEmpty(txt_Search_Name.Text)) {
				SQL += " AND (vw.PSNL_NO LIKE '%" + txt_Search_Name.Text.Replace("'", "''") + "%'" + "\n";
				SQL += " OR vw.PSNL_Fullname LIKE '%" + txt_Search_Name.Text.Replace("'", "''") + "%'" + "\n";
				SQL += " OR ISNULL(vw.MGR_NAME,vw.FN_NAME) LIKE '%" + txt_Search_Name.Text.Replace("'", "''") + "%')" + "\n";


			}
			if (!string.IsNullOrEmpty(txt_Search_Organize.Text)) {
				SQL += " AND (vw.SECTOR_NAME LIKE '%" + txt_Search_Organize.Text.Replace("'", "''") + "%' OR " + "\n";
				SQL += " vw.DEPT_NAME LIKE '%" + txt_Search_Organize.Text.Replace("'", "''") + "%')" + "\n";
			}
			if (ddl_Search_Class.SelectedIndex > 0) {
				SQL += " AND CLS.CLSGP_ID=" + ddl_Search_Class.Items[ddl_Search_Class.SelectedIndex].Value + "\n";
			}

			switch (ddlStatus.Items[ddlStatus.SelectedIndex].Value) {
				case "-1":
					SQL += "";
					break;
				case "4":
					SQL += " AND ISNULL(Header.COMP_Status,0) IN (0,-1,1,2,3,4) " + "\n";
					break;
				case "5":
					SQL += " AND Header.COMP_Status =" + ddlStatus.Items[ddlStatus.SelectedIndex].Value + "\n";
					break;
			}

			SQL += "ORDER BY vw.SECTOR_CODE,vw.DEPT_CODE,vw.PNPS_CLASS DESC,vw.POS_NO" + "\n";

			SqlDataAdapter DA = new SqlDataAdapter(SQL, BL.ConnectionString());
			DA.SelectCommand.CommandTimeout = 120;
			DataTable DT = new DataTable();
			DA.Fill(DT);

            DT.DefaultView.RowFilter = Filter;

            Session["Setting_Personal"] = DT.DefaultView.ToTable();
			Pager.SesssionSourceName = "Setting_Personal";
			Pager.RenderLayout();

            if (DT.DefaultView.Count == 0)
            {
				lblCountPSN.Text = "ไม่พบรายการดังกล่าว";
			} else {
				lblCountPSN.Text = "พบ " + GL.StringFormatNumber(DT.DefaultView.Count, 0) + " รายการ";
			}

		}

		protected void Pager_PageChanging(PageNavigation Sender)
		{
			Pager.TheRepeater = rptCOMP;
		}

		//------------ For Grouping -----------
		string LastRound = "";
		string LastSectorName = "";
		string LastOrgranizeName = "";
		protected void rptCOMP_ItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
		{
			if (e.Item.ItemType != ListItemType.AlternatingItem & e.Item.ItemType != ListItemType.Item)
				return;

            Label lblRound = (Label)e.Item.FindControl("lblRound");
            Label lblSector = (Label)e.Item.FindControl("lblSector");
            Label lblPSNOrganize = (Label)e.Item.FindControl("lblPSNOrganize");
            Label lblPSNNo = (Label)e.Item.FindControl("lblPSNNo");
            Label lblPSNName = (Label)e.Item.FindControl("lblPSNName");
            Label lblPSNPos = (Label)e.Item.FindControl("lblPSNPos");
            Label lblPSNClass = (Label)e.Item.FindControl("lblPSNClass");
            Label lblStatus = (Label)e.Item.FindControl("lblStatus");
            HtmlAnchor btnPrint = (HtmlAnchor)e.Item.FindControl("btnPrint");

            DataRowView drv = (DataRowView)e.Item.DataItem;

			if (LastRound != "ปี " + drv["R_Year"] + " รอบ " + drv["R_Round"]) {
				LastRound = "ปี " + drv["R_Year"] + " รอบ " + drv["R_Round"];
				lblRound.Text = LastRound;
			}

			if (drv["SECTOR_NAME"].ToString() != LastSectorName) {
				lblSector.Text = drv["SECTOR_NAME"].ToString();
				LastSectorName = drv["SECTOR_NAME"].ToString();
			}
			if (drv["DEPT_NAME"].ToString() != LastOrgranizeName) {
				if (drv["DEPT_NAME"].ToString() != drv["SECTOR_NAME"].ToString()) {
					lblPSNOrganize.Text = drv["DEPT_NAME"].ToString().Replace(drv["SECTOR_NAME"].ToString(), "").Trim();
				} else {
					lblPSNOrganize.Text = drv["DEPT_NAME"].ToString();
				}
				LastOrgranizeName = drv["DEPT_NAME"].ToString();
			}

			lblPSNNo.Text = drv["PSNL_NO"].ToString();
			lblPSNName.Text = drv["PSNL_Fullname"].ToString();
			lblPSNPos.Text = drv["POS_Name"].ToString();
			if (!GL.IsEqualNull(drv["PNPS_CLASS"])) {
				lblPSNClass.Text = GL.CINT (drv["PNPS_CLASS"]).ToString();
			} else {
				lblPSNClass.Text = "";
			}

			lblStatus.Text = drv["COMP_Status_Name"].ToString();

            btnPrint.HRef = "Print/RPT_IDP_PSN.aspx?Mode=PDF&R_Year=" + drv["R_Year"].ToString() + "&R_Round=" + drv["R_Round"].ToString() + "&PSNL_NO=" + drv["PSNL_NO"].ToString() + "&Status=" + GL.CINT(drv["COMP_Status"]); 
			btnPrint.Target = "_blank";
		}
		public RPT_IDP_PSN()
		{
			Load += Page_Load;
		}

	}
}

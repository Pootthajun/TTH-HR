using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Xml.Linq;
using System.Diagnostics;
using System.Collections.Specialized;
using System.Configuration;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Caching;
using System.Web.SessionState;
using System.Web.Security;
using System.Web.Profile;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
namespace VB
{

	public partial class AssessmentSetting_Assessor_Assigned : System.Web.UI.Page
	{


		HRBL BL = new HRBL();
		GenericLib GL = new GenericLib();

		textControlLib TC = new textControlLib();
		public int R_Year {
			get {
				try {
					return GL.CINT( GL.SplitString(ddlRound.Items[ddlRound.SelectedIndex].Value, "-")[0]);
				} catch (Exception ex) {
					return 0;
				}
			}
		}

		public int R_Round {
			get {
				try {
					return GL.CINT(GL.SplitString(ddlRound.Items[ddlRound.SelectedIndex].Value, "-")[1]);
				} catch (Exception ex) {
					return 0;
				}
			}
		}

		public string ASSESSOR_POS {
			get {
				try {
					return lblEditDept.Attributes["ASSESSOR_POS"];
				} catch (Exception ex) {
					return "";
				}
			}
			set { lblEditDept.Attributes["ASSESSOR_POS"] = value; }
		}

		public DataTable ASSIGNED {
            get { return (DataTable)Session["AssessmentSetting_Assessor_Assigned_Assigned"]; }
			set { Session["AssessmentSetting_Assessor_Assigned_Assigned"] = value; }
		}

		public string DEPT_CODE {
			get {
				try {
					return lblEditDept.Attributes["DEPT_CODE"];
				} catch (Exception ex) {
					return "";
				}
			}
			set { lblEditDept.Attributes["DEPT_CODE"] = value; }
		}

		protected void Page_Load(object sender, System.EventArgs e)
		{
			if ((Session["USER_PSNL_NO"] == null)) {
				ScriptManager.RegisterStartupScript(this.Page, typeof(string), "Alert", "showAlert('กรุณาลงชื่อเข้าใช้ระบบ');", true);
				Response.Redirect("Login.aspx");
				return;
			}

			if (!IsPostBack) {
				BindSearchForm();
				BindList();
                SetPrintButton();
				pnlList.Visible = true;
				pnlEdit.Visible = false;
				pnlAdd.Visible = false;
			} 
		}

        private void SetPrintButton()
        {
            btnFormPDF.HRef = "Print/RPT_A_Assessor_Assigned.aspx?MODE=PDF&R_Year=" + R_Year ;
            btnFormExcel.HRef = "Print/RPT_A_Assessor_Assigned.aspx?MODE=EXCEL&R_Year=" + R_Year ;

        }

		private void BindSearchForm()
		{
			BL.BindDDlYearRound(ddlRound);
			txtFilter.Text = "";
		}

		protected void btnSearch_Click(object sender, System.EventArgs e)
		{
			BindList();
		}


		private void BindList()
		{
            try
            {

                if (R_Year == 0 | R_Round == 0)
                {
                    rptList.DataSource = null;
                    rptList.DataBind();
                    lblCountList.Text = "เลือกรอบการประเมิน";
                    return;
                }

                if (BL.Is_Round_Completed(R_Year, R_Round))
                {
                    rptList.DataSource = null;
                    rptList.DataBind();
                    lblCountList.Text = "รอบการประเมินนี้ปิดไปแล้วไม่สามารถกำหนดข้อมูลอื่นๆได้";
                    return;
                }

                string SQL = "";

                SQL += " SELECT \n";
                SQL += "  ASS_MASTER.ASSESSOR_POS,ASS_MASTER.ASSESSOR_POS MGR_POS_NO,MGR_DETAIL.PNPO_CLASS\n";
                SQL += "  ,ISNULL(MGR_DETAIL.MGR_NAME,MGR_DETAIL.FN_Name) MGR_POS_NAME,MGR_DETAIL.DEPT_CODE MGR_DEPT_CODE,MGR_DETAIL.DEPT_NAME MGR_DEPT_NAME\n";
                SQL += "  ,MGR_PSN.MGR_PSNL_NO,MGR_PSN.MGR_PSNL_Fullname,COUNT(PSN.PSNL_NO) TOTAL_PSN\n";
                SQL += " \n";
                SQL += "  FROM tb_HR_Assessor_Pos ASS_MASTER\n";
                SQL += "  -------------ข้อมูลประจำตำแหน่ง ------------\n";
                SQL += "  INNER JOIN vw_PN_Position MGR_DETAIL ON ASS_MASTER.ASSESSOR_POS=MGR_DETAIL.POS_NO\n";
                SQL += "   -------------คนที่ดำรงตำแหน่ง ------------\n";
                SQL += "  LEFT JOIN vw_HR_Manager_Default MGR_PSN ON ASS_MASTER.ASSESSOR_POS=MGR_PSN.ASSESSOR_POS\n";
                SQL += " \t\t\t\t\t\t\t\t\t\tAND R_Year=" + R_Year + " And R_Round=" + R_Round + "\n";
                SQL += "  ------------- คนที่ต้องประเมิน ------------\n";
                SQL += "  LEFT JOIN vw_PN_PSNL PSN ON ASS_MASTER.POS_NO=PSN.POS_NO\n";
                SQL += " \n";
                SQL += "  -------------- กรองประเภทและระดับพนักงานรอบที่กำหนด ---------------\n";
                SQL += "  LEFT JOIN vw_PN_PSNL PSN_DETAIL ON PSN.POS_NO=PSN_DETAIL.POS_NO\n";
                SQL += "  LEFT JOIN tb_HR_Round_Class R_Class ON MGR_PSN.R_Year=R_Class.R_Year AND MGR_PSN.R_Round=R_Class.R_Round\n";
                SQL += "\t\t\t\t\t\t\t\t\tAND PSN_DETAIL.PNPS_CLASS=\n";
                SQL += "\t\t\t\t\t\t\t\t\t\tCASE WHEN LEN(R_Class.PNPS_CLASS)<2 THEN\n";
                SQL += "\t\t\t\t\t\t\t\t\t\t\tREPLICATE('0',2-LEN(R_Class.PNPS_CLASS))+R_Class.PNPS_CLASS\n";
                SQL += "\t\t\t\t\t\t\t\t\t\tELSE\n";
                SQL += "\t\t\t\t\t\t\t\t\t\t\tR_Class.PNPS_CLASS\n";
                SQL += "\t\t\t\t\t\t\t\t\t\tEND\n";
                SQL += "  LEFT JOIN tb_HR_Round_Type R_Type ON MGR_PSN.R_Year=R_Type.R_Year AND MGR_PSN.R_Round=R_Type.R_Round\n";
                SQL += "\t\t\t\t\t\t\t\t\tAND PSN_DETAIL.PSNL_TYPE=R_Type.PNPS_PSNL_TYPE\n";


                //--------------กรอง เงื่อนไข ------------
                string Filter = "";
                //"MGR_PSN.R_Year IS NOT NULL AND "
                if (!string.IsNullOrEmpty(txtFilter.Text))
                {
                    Filter += " (MGR_DETAIL.DEPT_CODE LIKE '%" + txtFilter.Text.Replace("'", "''").Replace(" ", "%") + "%' OR \n";
                    Filter += " MGR_DETAIL.DEPT_NAME LIKE '%" + txtFilter.Text.Replace("'", "''").Replace(" ", "%") + "%' OR \n";
                    Filter += " ASS_MASTER.ASSESSOR_POS LIKE '%" + txtFilter.Text.Replace("'", "''").Replace(" ", "%") + "%' OR \n";
                    Filter += " MGR_PSN.MGR_PSNL_NO LIKE '%" + txtFilter.Text.Replace("'", "''").Replace(" ", "%") + "%' OR \n";
                    Filter += " ISNULL(MGR_DETAIL.MGR_NAME,MGR_DETAIL.FN_Name) LIKE '%" + txtFilter.Text.Replace("'", "''").Replace(" ", "%") + "%' OR \n";
                    Filter += " MGR_PSN.MGR_PSNL_Fullname LIKE '%" + txtFilter.Text.Replace("'", "''").Replace(" ", "%") + "%' ) AND ";
                }
                if (chkBlank.Checked)
                {
                    Filter += " MGR_PSN.MGR_PSNL_Fullname IS NULL AND ";
                }
                if (!string.IsNullOrEmpty(Filter))
                {
                    SQL += "\n" + " WHERE " + Filter.Substring(0, Filter.Length - 4) + "\n\n";
                }

                SQL += " GROUP BY  ASS_MASTER.ASSESSOR_POS,ASS_MASTER.ASSESSOR_POS,MGR_DETAIL.PNPO_CLASS\n";
                SQL += "  ,MGR_DETAIL.MGR_NAME,MGR_DETAIL.FN_Name,MGR_DETAIL.DEPT_CODE,MGR_DETAIL.DEPT_NAME \n";
                SQL += "  ,MGR_PSN.MGR_PSNL_NO,MGR_PSN.MGR_PSNL_Fullname\n";
                SQL += "  \n";
                SQL += " ORDER BY MGR_DETAIL.PNPO_CLASS DESC,MGR_DETAIL.DEPT_CODE,ASS_MASTER.ASSESSOR_POS\n";
                SqlDataAdapter DA = new SqlDataAdapter(SQL, BL.ConnectionString());
                DataTable DT = new DataTable();
                DA.Fill(DT);
                Session["AssessmentSetting_MGR"] = DT;

                //-------------- หา List คนที่ถูก Assigned ในรอบนั้น ------------
                SQL = "SELECT * FROM vw_HR_Manager_Assigned WHERE R_Year=" + R_Year + " AND R_Round=" + R_Round + "\n";
                SQL += " ORDER BY MGR_POS_NO";
                DA = new SqlDataAdapter(SQL, BL.ConnectionString());
                DataTable AT = new DataTable();
                DA.Fill(AT);
                ASSIGNED = AT;

                Pager.SesssionSourceName = "AssessmentSetting_MGR";
                Pager.RenderLayout();
                string Count_List = "";
                if (DT.Rows.Count == 0)
                {
                    lblCountList.Text = "แสดงความสัมพันธ์ระหว่างผู้ประเมินและผู้ถูกประเมินตามตำแหน่ง : ไม่พบรายการดังกล่าว";
                    Count_List += " สำหรับรอบการประเมิน ปี " + R_Year + " รอบ " + R_Round + "ไม่พบรายการดังกล่าว";
                }
                else
                {
                    lblCountList.Text = "แสดงความสัมพันธ์ระหว่างผู้ประเมินและผู้ถูกประเมินตามตำแหน่ง : พบ " + GL.StringFormatNumber(DT.Rows.Count, 0) + " รายการ";
                    Count_List += " สำหรับรอบการประเมิน ปี " + R_Year + " รอบ " + R_Round + " พบ " + GL.StringFormatNumber(DT.Rows.Count, 0) + " รายการ";
                }
                Session["AssessmentSetting_MGR_Count_List"] = Count_List;

            }
            catch (Exception ex) { 
            
            }
		}

		protected void Pager_PageChanging(PageNavigation Sender)
		{
			Pager.TheRepeater = rptList;
		}


		protected void rptList_ItemCommand(object source, System.Web.UI.WebControls.RepeaterCommandEventArgs e)
		{
			LinkButton btnEdit =(LinkButton) e.Item.FindControl("btnEdit");
			string _POS = btnEdit.CommandArgument;

			string SQL = "";
			SQL += " SELECT POS.POS_NO,ISNULL(POS.MGR_NAME,POS.FN_Name) POS_NAME,PNPO_CLASS,POS.DEPT_CODE,POS.DEPT_NAME\n";
			SQL += " ,PSN.PSNL_NO,PSN.PSNL_Fullname\n";
			SQL += " FROM vw_PN_Position POS\n";
			SQL += " LEFT JOIN vw_PN_PSNL PSN ON POS.POS_NO=PSN.POS_NO\n";
			SQL += " WHERE POS.POS_NO='" + _POS + "'\n";

			SqlDataAdapter DA = new SqlDataAdapter(SQL, BL.ConnectionString());
			DataTable DT = new DataTable();
			DA.Fill(DT);

			if (DT.Rows.Count == 0) {
				ScriptManager.RegisterStartupScript(this.Page, typeof(string), "Alert", "showAlert('ไม่พบข้อมูลดังกล่าว');", true);
				return;
			}
			ClearEditForm();

			ASSESSOR_POS = _POS;
			lblEditPosNo.Text = DT.Rows[0]["POS_NO"].ToString ();
			lblEditPosName.Text = DT.Rows[0]["POS_NAME"].ToString ();
			lblEditDept.Text = DT.Rows[0]["DEPT_NAME"] + " สำหรับการประเมินปี " + R_Year + " รอบ " + R_Round;
			DEPT_CODE = DT.Rows[0]["DEPT_CODE"].ToString ();

			if (!GL.IsEqualNull(DT.Rows[0]["PSNL_NO"])) {
				lblMainName.Text = DT.Rows[0]["PSNL_NO"] + " : " + DT.Rows[0]["PSNL_Fullname"];
				lblMainName.ForeColor = System.Drawing.Color.Green;
			}

			lblMainPos.Text = DT.Rows[0]["POS_NO"] + " : " + DT.Rows[0]["POS_NAME"];
			lblMainClass.Text = GL.CINT (DT.Rows[0]["PNPO_CLASS"]).ToString ();
			lblMainDept.Text = DT.Rows[0]["DEPT_NAME"].ToString ();
			lblMainPos.ForeColor = System.Drawing.Color.Black;
			lblMainClass.ForeColor = System.Drawing.Color.Black;

			//-------------- หา List คนที่ถูก Assigned ในรอบนั้น ------------
			BindAssigned();

			pnlList.Visible = false;
			pnlEdit.Visible = true;
			pnlAdd.Visible = false;

		}

		private void BindAssigned()
		{
			//-------------- หา List คนที่ถูก Assigned ในรอบนั้น ------------
			string SQL = null;
			SQL = "SELECT *,ISNULL(MGR_MGR_NAME,MGR_FN_NAME)  MGR_POS_NAME \n";
			SQL += " FROM vw_HR_Manager_Assigned \n";
			SQL += " WHERE R_Year = " + R_Year + " And R_Round = " + R_Round + "\n";
			SQL += " AND ASSESSOR_POS='" + ASSESSOR_POS.Replace("'", "''") + "'\n";
			SQL += " ORDER BY MGR_CLASS DESC,MGR_DEPT_CODE,MGR_POS_NO\n";
			SqlDataAdapter DA = new SqlDataAdapter(SQL, BL.ConnectionString());
			DataTable DT = new DataTable();
			DA.Fill(DT);

			if (DT.Rows.Count == 0) {
				lblHeaderAssigned.Text = "ไม่มีผู้รักษาการเพิ่มเติม";
				pnlAssignedList.Visible = false;
			} else {
				lblHeaderAssigned.Text = "ผู้รักษาการเพิ่มเติม " + DT.Rows.Count + " คน";
				pnlAssignedList.Visible = true;
				//------------- Bind Assigned List ---------------
				rptAssigned.DataSource = DT;
				rptAssigned.DataBind();
			}

			rptAssigned.DataSource = DT;
			rptAssigned.DataBind();
		}

		protected void rptList_ItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
		{
			if (e.Item.ItemType != ListItemType.Item & e.Item.ItemType != ListItemType.AlternatingItem)
				return;

			Label lblPos =(Label) e.Item.FindControl("lblPos");
			Label lblDefault =(Label) e.Item.FindControl("lblDefault");
			Label lblClass = (Label) e.Item.FindControl("lblClass");
			Label lblDept =(Label) e.Item.FindControl("lblDept");
			Label lblPSN = (Label) e.Item.FindControl("lblPSN");
			Label lblAssigned =(Label) e.Item.FindControl("lblAssigned");
			LinkButton btnEdit =(LinkButton) e.Item.FindControl("btnEdit");

            DataRowView drv = (DataRowView)e.Item.DataItem;
			lblPos.Text = drv["ASSESSOR_POS"].ToString() + " : " + drv["MGR_POS_NAME"].ToString();
			if (!GL.IsEqualNull(drv["MGR_PSNL_NO"])) {
				lblDefault.Text = drv["MGR_PSNL_NO"].ToString() + " : " + drv["MGR_PSNL_Fullname"].ToString();
				lblDefault.ForeColor = System.Drawing.Color.SteelBlue;
			} else {
				lblDefault.Text = "ไม่มีผู้ครองตำแหน่ง";
				lblDefault.ForeColor = System.Drawing.Color.Red;
			}
			if (!GL.IsEqualNull(drv["PNPO_CLASS"])) {
				lblClass.Text = GL.CINT (drv["PNPO_CLASS"]).ToString ();
			}
			lblDept.Text = drv["MGR_DEPT_NAME"].ToString();
			if (GL.CINT (drv["Total_PSN"]) > 0) {
				lblPSN.Text = GL.StringFormatNumber(drv["Total_PSN"], 0);
			} else {
				lblPSN.Text = "-";
			}

			//----------------- Get Assigned -----------
			DataTable DT = ASSIGNED.Copy();
			DT.DefaultView.RowFilter = "ASSESSOR_POS='" + drv["ASSESSOR_POS"] + "'";
			for (int i = 0; i <= DT.DefaultView.Count - 1; i++) {
				lblAssigned.Text += "<i class='icon-user-md'></i> " + DT.DefaultView[i]["MGR_PSNL_NO"] + ":" + DT.DefaultView[i]["MGR_PSNL_Fullname"] + "<br>";
			}
			if (DT.DefaultView.Count > 0) {
				lblAssigned.ForeColor = System.Drawing.Color.Green;
			}

			btnEdit.CommandArgument = drv["ASSESSOR_POS"].ToString ();
		}

		private void ClearEditForm()
		{
			lblEditDept.Text = "";
			lblEditRound.Text = "";

			lblMainName.Text = "ไมมีผู้ครองตำแหน่ง";
			lblMainPos.Text = "-";
			lblMainClass.Text = "-";

			lblMainName.ForeColor = System.Drawing.Color.Red;
			lblMainPos.ForeColor = System.Drawing.Color.Black;
			lblMainClass.ForeColor = System.Drawing.Color.Black;


			rptAssigned.DataSource = null;
			rptAssigned.DataBind();
		}

		protected void rptAssigned_ItemCommand(object source, System.Web.UI.WebControls.RepeaterCommandEventArgs e)
		{
			switch (e.CommandName) {
				case "Delete":
					  Button btnPSNDelete =(Button) e.Item.FindControl("btnPSNDelete");
					SqlConnection Conn = new SqlConnection(BL.ConnectionString());
					Conn.Open();
					SqlCommand Comm = new SqlCommand();
					var _with1 = Comm;
					_with1.Connection = Conn;
					_with1.CommandType = CommandType.Text;
					_with1.CommandText = "DELETE FROM tb_HR_Assessor_Assigned WHERE FOR_POS='" + ASSESSOR_POS.Replace("'", "''") + "' AND ASSIGNED_POS_NO='" + btnPSNDelete.CommandArgument.Replace("'", "''") + "'";
					_with1.ExecuteNonQuery();
					Conn.Close();

					BindAssigned();
					break;
			}
		}

		protected void rptAssigned_ItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
		{
			if (e.Item.ItemType != ListItemType.Item & e.Item.ItemType != ListItemType.AlternatingItem)
				return;

			Label lblNo =(Label) e.Item.FindControl("lblNo");
			 Label lblPSNPos = (Label)e.Item.FindControl("lblPSNPos");
			 Label lblPSNClass = (Label)e.Item.FindControl("lblPSNClass");
			Label lblPSNDept =(Label) e.Item.FindControl("lblPSNDept");
			Label lblPSNNow =(Label) e.Item.FindControl("lblPSNNow");

			  Button btnPSNDelete =(Button) e.Item.FindControl("btnPSNDelete");
			AjaxControlToolkit.ConfirmButtonExtender cfm_PSN_Delete =(AjaxControlToolkit.ConfirmButtonExtender) e.Item.FindControl("cfm_PSN_Delete");
            DataRowView drv = (DataRowView)e.Item.DataItem;
			lblNo.Text = GL.CINT ( e.Item.ItemIndex+ 1).ToString () ;
			lblPSNPos.Text = drv["MGR_POS_NO"].ToString () + " : " + drv["MGR_POS_NAME"].ToString ();

			cfm_PSN_Delete.ConfirmText = "ยืนยันยกเลิกตำแหน่ง  " + lblPSNPos.Text;

			if (!GL.IsEqualNull(drv["MGR_CLASS"])) {
				lblPSNClass.Text = GL.CINT (drv["MGR_CLASS"]).ToString ();
			}
			lblPSNDept.Text = drv["MGR_DEPT_NAME"].ToString ();
			if (!GL.IsEqualNull(drv["MGR_PSNL_NO"])) {
				lblPSNNow.Text = drv["MGR_PSNL_NO"].ToString () + " : ";
			}
			if (!GL.IsEqualNull(drv["MGR_PSNL_Fullname"])) {
				lblPSNNow.Text += drv["MGR_PSNL_Fullname"].ToString ();
			} else {
				lblPSNNow.Text = "ไม่มีผู้ครองตำแหน่ง";
				lblPSNNow.ForeColor = System.Drawing.Color.Red;
			}

			btnPSNDelete.CommandArgument = drv["MGR_POS_NO"].ToString ();

		}

		protected void btnCancel_Click(object sender, System.EventArgs e)
		{
			BindList();
			pnlEdit.Visible = false;
			pnlList.Visible = true;
		}

		protected void btnAdd_Click(object sender, System.EventArgs e)
		{
			BL.BindDDlSector(ddl_Search_Sector, DEPT_CODE.Substring(0, 2));
			txt_Search_Name.Text = "";
			txt_Search_Organize.Text = "";
			BindAddPanel();
			pnlAdd.Visible = true;
		}

		private void BindAddPanel()
		{
			string SQL = " SELECT PSN.PSNL_NO,PSN.PSNL_Fullname,DEPT_NAME,POS_NO,ISNULL(MGR_NAME,FN_NAME) POS_NAME,PNPS_CLASS,DEPT_CODE,MINOR_CODE\n";
			SQL += " FROM vw_PN_PSNL PSN \n";
			SQL += " WHERE PSNL_STAT<>'03' \n";

			if (ddl_Search_Sector.SelectedIndex > 0) {
				SQL += " AND PSN.SECTOR_CODE = '" + ddl_Search_Sector.Items[ddl_Search_Sector.SelectedIndex].Value + "' \n";

				if (GL.CINT (ddl_Search_Sector.Items[ddl_Search_Sector.SelectedIndex].Value) == 3200) {
					SQL += " AND DEPT_CODE NOT IN ('32000300')    \n";
				} else if (GL.CINT (ddl_Search_Sector.Items[ddl_Search_Sector.SelectedIndex].Value) == 3203) {
					SQL += " OR DEPT_CODE IN ('32000300')  \n";
				} else {
					SQL += "   \n";
				}
			}
			if (!string.IsNullOrEmpty(txt_Search_Organize.Text)) {
				SQL += " AND (DEPT_CODE LIKE '%" + txt_Search_Organize.Text.Replace("'", "''") + "%' OR SECTOR_NAME LIKE '%" + txt_Search_Organize.Text.Replace("'", "''") + "%' OR DEPT_NAME LIKE '%" + txt_Search_Organize.Text.Replace("'", "''") + "%' OR FN_NAME LIKE '%" + txt_Search_Organize.Text.Replace("'", "''") + "%' OR MGR_NAME LIKE '%" + txt_Search_Organize.Text.Replace("'", "''") + "%' ) \n";
			}
			if (!string.IsNullOrEmpty(txt_Search_Name.Text)) {
				SQL += " AND (PSN.PSNL_Fullname LIKE '%" + txt_Search_Name.Text.Replace("'", "''") + "%' OR PSN.PSNL_NO LIKE '%" + txt_Search_Name.Text.Replace("'", "''") + "%' OR  ISNULL(MGR_NAME,FN_NAME) LIKE '%" + txt_Search_Name.Text.Replace("'", "''") + "%' OR POS_NO LIKE '%" + txt_Search_Name.Text.Replace("'", "''") + "%') \n";
			}

			string[] MGR = CurrentManager();
			string Filter = "";
			if (MGR.Length > 0) {
				for (int i = 0; i <= MGR.Length - 1; i++) {
					Filter += "'" + MGR[i] + "',";
				}
				SQL += " AND PSN.POS_NO NOT IN (" + Filter.Substring(0, Filter.Length - 1) + ")\n";
			}

			SQL += " ORDER BY SECTOR_CODE,DEPT_CODE,PNPS_CLASS DESC,PSN.POS_NO\n";
			SqlDataAdapter DA = new SqlDataAdapter(SQL, BL.ConnectionString());
			DataTable DT = new DataTable();
			DA.Fill(DT);

			Session["AssessmentSetting_Asessor_Assigned_MGR"] = DT;
			PagerPSN.SesssionSourceName = "AssessmentSetting_Asessor_Assigned_MGR";
			PagerPSN.RenderLayout();
			lblCountPSN.Text = "พบ " + GL.StringFormatNumber(DT.Rows.Count, 0) + " รายการ";

		}

		private string[] CurrentManager()
		{
			string[] Result = { ASSESSOR_POS };
			foreach (RepeaterItem Item in rptAssigned.Items) {
				if (Item.ItemType != ListItemType.Item & Item.ItemType != ListItemType.AlternatingItem)
					continue;
				Button btnPSNDelete =(Button) Item.FindControl("btnPSNDelete");
				GL.PushArray_String(Result, btnPSNDelete.CommandArgument);
			}
			return Result;
		}

		protected void Search_Changed(object sender, System.EventArgs e)
		{
			BindAddPanel();
		}

		protected void PagerPSN_PageChanging(PageNavigation Sender)
		{
			PagerPSN.TheRepeater = rptPSN;
		}

		protected void rptPSN_ItemCommand(object source, System.Web.UI.WebControls.RepeaterCommandEventArgs e)
		{
			switch (e.CommandName) {
				case "Select":

					Button btnSelect =(Button) e.Item.FindControl("btnSelect");
					string _POS = btnSelect.CommandArgument;

					string SQL = "SELECT * FROM vw_PN_PSNL WHERE POS_NO='" + _POS.Replace("'", "''") + "'";
					SqlDataAdapter DA = new SqlDataAdapter(SQL, BL.ConnectionString());
					DataTable PSN = new DataTable();
					DA.Fill(PSN);

					if (PSN.Rows.Count == 0) {
						ScriptManager.RegisterStartupScript(this.Page, typeof(string), "Alert", "alert('ไม่พบข้อมูลตำแหน่งดังกล่าว');", true);
						return;
					}
					DataRow PN = PSN.Rows[0];

					SQL = "SELECT * FROM tb_HR_Assessor_Assigned \n";
					SQL += " WHERE R_Year =" + R_Year + " AND R_Round=" + R_Round + " AND FOR_POS='" + ASSESSOR_POS.Replace("'", "''") + "' AND ASSIGNED_POS_NO='" + _POS.Replace("'", "''") + "'\n";

					DA = new SqlDataAdapter(SQL, BL.ConnectionString());
					DataTable DT = new DataTable();
					DA.Fill(DT);

					DataRow DR = null;

					if (DT.Rows.Count == 0) {
						DR = DT.NewRow();
						DT.Rows.Add(DR);
						DR["R_Year"] = R_Year;
						DR["R_Round"] = R_Round;
						DR["FOR_POS"] = ASSESSOR_POS;
						DR["ASSIGNED_PSNL_NO"] = PN["PSNL_NO"];
						DR["ASSIGNED_PSNL_Fullname"] = PN["PSNL_Fullname"];
						DR["ASSIGNED_PNPS_CLASS"] = PN["PNPS_CLASS"];
						DR["ASSIGNED_PSNL_TYPE"] = PN["PSNL_TYPE"];
						DR["ASSIGNED_POS_NO"] = _POS;
						DR["ASSIGNED_WAGE_TYPE"] = PN["WAGE_TYPE"];
						DR["ASSIGNED_WAGE_NAME"] = PN["WAGE_NAME"];
						DR["ASSIGNED_DEPT_CODE"] = PN["DEPT_CODE"];
						DR["ASSIGNED_MINOR_CODE"] = PN["MINOR_CODE"];
						DR["ASSIGNED_SECTOR_CODE"] = Strings.Left(PN["SECTOR_CODE"].ToString(), 2);
						DR["ASSIGNED_SECTOR_NAME"] = PN["SECTOR_NAME"];
						DR["ASSIGNED_DEPT_NAME"] = PN["DEPT_NAME"];
						DR["ASSIGNED_FN_ID"] = PN["FN_ID"];
						DR["ASSIGNED_FLD_Name"] = PN["FLD_Name"];
						DR["ASSIGNED_FN_CODE"] = PN["FN_CODE"];
						DR["ASSIGNED_FN_TYPE"] = PN["FN_TYPE"];
						DR["ASSIGNED_FN_NAME"] = PN["FN_NAME"];
						DR["ASSIGNED_MGR_CODE"] = PN["MGR_CODE"];
						DR["ASSIGNED_MGR_NAME"] = PN["MGR_NAME"];
						DR["ASSIGNED_PNPS_RETIRE_DATE"] = PN["PNPS_RETIRE_DATE"];
					} else {
						DR = DT.Rows[0];
					}

					DR["Update_By"] = Session["USER_PSNL_NO"];
					DR["Update_Time"] = DateAndTime.Now;

					SqlCommandBuilder cmd = new SqlCommandBuilder(DA);
					DA.Update(DT);

					//-------------- หา List คนที่ถูก Assigned ในรอบนั้น ------------
					BindAssigned();

					pnlAdd.Visible = false;

					break;
			}
		}

		string LastOrgranizeName = "";
		protected void rptPSN_ItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
		{
			if (e.Item.ItemType != ListItemType.Item & e.Item.ItemType != ListItemType.AlternatingItem)
				return;

			 Label lblPSNPos = (Label)e.Item.FindControl("lblPSNPos");
			 Label lblPSNClass = (Label)e.Item.FindControl("lblPSNClass");
			Label lblPSNDept =(Label) e.Item.FindControl("lblPSNDept");
			Label lblPSNName = (Label)e.Item.FindControl("lblPSNName");
			AjaxControlToolkit.ConfirmButtonExtender cfmbtnSelect =(AjaxControlToolkit.ConfirmButtonExtender) e.Item.FindControl("cfmbtnSelect");
			Button btnSelect =(Button) e.Item.FindControl("btnSelect");

            DataRowView drv = (DataRowView)e.Item.DataItem;
			if (Convert.ToString(drv["DEPT_NAME"]) != LastOrgranizeName) {
				lblPSNDept.Text = drv["DEPT_NAME"].ToString ();
				LastOrgranizeName = drv["DEPT_NAME"].ToString ();
			}

			lblPSNName.Text = drv["PSNL_NO"] + " : " + drv["PSNL_Fullname"];
			lblPSNPos.Text = drv["POS_Name"].ToString ();
			lblPSNClass.Text = GL.CINT (drv["PNPS_CLASS"]).ToString ();
			btnSelect.CommandArgument = drv["POS_NO"].ToString ();

			cfmbtnSelect.ConfirmText = "ยืนยันแต่งตั้ง " + drv["POS_Name"] + " ระดับ " + Convert.ToInt32(drv["PNPS_CLASS"]) + " " + drv["DEPT_NAME"];

		}

		protected void btnClosePNLAdd_Click(object sender, System.EventArgs e)
		{
			pnlAdd.Visible = false;
		}
		public AssessmentSetting_Assessor_Assigned()
		{
			Load += Page_Load;
		}
	}
}

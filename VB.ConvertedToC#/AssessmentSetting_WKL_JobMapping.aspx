﻿<%@ Page  Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPage.Master" CodeBehind="AssessmentSetting_WKL_JobMapping.aspx.cs" Inherits="VB.AssessmentSetting_WKL_JobMapping" %>

<%@ Register src="WUC_DateReporter.ascx" tagname="WUC_DateReporter" tagprefix="uc1" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<asp:UpdatePanel ID="UpdatePanel1" runat="server">				           
<ContentTemplate>
<asp:ToolkitScriptManager ID="toolkit1" runat="server"></asp:ToolkitScriptManager>

<div class="container-fluid">
				<!-- BEGIN PAGE HEADER-->
				<div class="row-fluid" id="fixedPageHeader">
					<div class="span12"> 
						<!-- BEGIN PAGE TITLE & BREADCRUMB-->
						<h3 class="page-title">แผนที่งาน (Job Mapping) <font color="blue">(ScreenID : S-HDR-10)</font></h3>
						
									
						<ul class="breadcrumb">
                          
                            <li>
                            	<i class="icon-dashboard"></i><a href="javascript:;">การบริหารอัตรากำลัง (Workload)</a><i class="icon-angle-right"></i>
                            </li>
                        	<li>
                        	    <i class="icon-table"></i> <a href="javascript:;">แผนที่งาน (Job Mapping)</a>
                        	</li>
                        	
                        	
                            <uc1:WUC_DateReporter ID="WUC_DateReporter1" runat="server" />
                        </ul>
						<!-- END PAGE TITLE & BREADCRUMB-->
									        
				     </div>

				
			    </div>
		        <!-- END PAGE HEADER-->
				
				<asp:Panel ID="pnlList" runat="server" Visible="True">

               

                <div class="row-fluid">
					
					<div class="span12">
						<!-- BEGIN SAMPLE TABLE PORTLET-->
								                           
							    
							    <h4 class="control-label"> <i class="icon-sitemap"></i> ฝ่าย
							    
								    <asp:DropDownList ID="DropDownList1" runat="server" CssClass="medium m-wrap">
						            <asp:ListItem>ทั้งหมด</asp:ListItem>
						            <asp:ListItem>ฝ่ายจัดหาและรักษาพัสดุ</asp:ListItem>
						            <asp:ListItem>ฝ่ายวิจัยและพัฒนา</asp:ListItem>
						            <asp:ListItem>ฝ่ายผลิต</asp:ListItem>
						            
									</asp:DropDownList>
						            &nbsp; &nbsp;
						            ชื่อหน่วยงาน
						            <input type="text" placeholder="ค้นหาจากชื่อหน่วยงาน/กอง" style=" background-color:White; margin-top:5px;margin-left:10px;" class="m-wrap large">
						            
							   </h4>
							   
							  
							    
						    <div class="portlet-body no-more-tables">
								<table class="table table-full-width table-advance dataTable no-more-tables table-hover">
									<thead>
										<tr>
											<th><i class="icon-sitemap"></i> ฝ่าย</th>
											<th><i class="icon-sitemap"></i> ส่วน</th>
											<th><i class="icon-sitemap"></i> กอง</th>
											<th><i class="icon-time"></i> ปรับปรุงล่าสุด</th>
											<th><i class="icon-bolt"></i> ดำเนินการ</th>
											<th></th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td data-title="หน่วยงาน">ฝ่ายพัฒนาธุรกิจ</td>
											<td data-title="ส่วน">ศูนย์เทคโนโลยีสารสนเทศ(เดิม)</td>
											<td data-title="ส่วน">งานธุรการ</td>
											<td data-title="ปรับปรุงล่าสุด">24/01/2557</td>
											<td data-title="ดำเนินการ"><asp:LinkButton ID="LinkButton1" OnClick="LinkButton_Click" runat="server" CssClass="btn mini blue"><i class="icon-search"></i> แสดง</asp:LinkButton></td>
										</tr>
										<tr>
											<td data-title="หน่วยงาน">ฝ่ายพัฒนาธุรกิจ</td>
											<td data-title="ส่วน">ศูนย์เทคโนโลยีสารสนเทศ(เดิม)</td>
											<td data-title="ส่วน">กองเทคนิคและเครือข่าย</td>
											<td data-title="ปรับปรุงล่าสุด">02/02/2558</td>
											<td data-title="ดำเนินการ"><asp:LinkButton ID="LinkButton2" OnClick="LinkButton_Click" runat="server" CssClass="btn mini blue"><i class="icon-search"></i> แสดง</asp:LinkButton></td>
										</tr>
										<tr>
											<td data-title="หน่วยงาน">ฝ่ายพัฒนาธุรกิจ</td>
											<td data-title="ส่วน">ศูนย์เทคโนโลยีสารสนเทศ(เดิม)</td>
											<td data-title="ส่วน">กองสารสนเทศ</td>
											<td data-title="ปรับปรุงล่าสุด">14/08/2557</td>
											<td data-title="ดำเนินการ"><asp:LinkButton ID="LinkButton3" OnClick="LinkButton_Click" runat="server" CssClass="btn mini blue"><i class="icon-search"></i> แสดง</asp:LinkButton></td>
										</tr>
										<tr>
											<td data-title="หน่วยงาน">ฝ่ายพัฒนาธุรกิจ</td>
											<td data-title="ส่วน">ศูนย์เทคโนโลยีสารสนเทศ(เดิม)</td>
											<td data-title="ส่วน">กองพัฒนาโปรแกรมสำเร็จรูป</td>
											<td data-title="ปรับปรุงล่าสุด">05/07/2557</td>
											<td data-title="ดำเนินการ"><asp:LinkButton ID="LinkButton4" OnClick="LinkButton_Click" runat="server" CssClass="btn mini blue"><i class="icon-search"></i> แสดง</asp:LinkButton></td>
										</tr>									
									</tbody>
								</table>
							</div>
						
						<!-- END SAMPLE TABLE PORTLET-->
						
					</div>
                </div>
               </asp:Panel>
               
              <asp:Panel ID="pnlEdit" runat="server" Visible="False">               
				
					<div class="portlet-body form">
											
					<!-- BEGIN HEADER-->
					     <h3 class="form-section"><i class="icon-table"></i> การกำหนดแผนที่งาน (Job Mapping) สำหรับ ฝ่ายพัฒนาธุรกิจ ศูนย์เทคโนโลยีสารสนเทศ(เดิม) งานธุรการ</h3>
					<!-- END HEADER-->  
				
				    <!-- BEGIN FORM-->
				              
						    <div class="portlet-body no-more-tables">
								    <table class="table table-bordered" style="background-color:White; width:auto;">
									            <thead>
										            <tr>
											            <th style="text-align:center; font-size:12px; background-color:#CCCCCC;"> หมวด</th>
											            <th style="text-align:center; font-size:12px; background-color:#CCCCCC;"> งาน</th>
											            <th style="text-align:center; font-size:12px; background-color:#CCCCCC;"> ขั้นตอนการปฏิบัติ</th>
											            <th style="text-align:center; font-size:12px; background-color:#CCCCCC;">หัวหน้ากอง</th>
									                    <th style="text-align:center; font-size:12px; background-color:#CCCCCC;">ผู้ช่วยหัวหน้ากอง</th>
									                    <th style="text-align:center; font-size:12px; background-color:#CCCCCC;">พนักงานระดับ 6</th>
									                    <th style="text-align:center; font-size:12px; background-color:#CCCCCC;">พนักงานระดับ 4-5</th>
									                    <th style="text-align:center; font-size:12px; background-color:#CCCCCC;">พนักงานระดับ 1-3</th>
										                <th style="text-align:center; font-size:12px; background-color:#CCCCCC;"> ดำเนินการ</th>
									                </tr>                                               
									            </thead>
									            <tbody>
									                <tr>
										              <td colspan="8" style="font-weight:bold; padding-left:5px; padding-right:0px; cursor:pointer;" onClick="showDialog(this);">1. งานด้านบริหาร</td>
										              <td style="text-align:left; width:80px;">
										                  <div class="btn-group">
										                    <button class="btn dropdown-toggle mini" data-toggle="dropdown">ดำเนินการ <i class="icon-angle-down"></i></button>
										                    <ul class="dropdown-menu pull-right">
										                      <li><a href="javascript:;"><i class="icon-indent-right"></i> แทรกหมวด</a></li>
										                      <li><a href="javascript:;"><i class="icon-circle-arrow-down"></i> เพิ่มงาน</a></li>
										                      <li><a href="javascript:;"><i class="icon-trash"></i>ลบหมวดนี้</a></li>
									                        </ul>
									                      </div>
										              </td>
									                </tr>
    									            
										            <tr>
										              <td>&nbsp;</td>
										              <td colspan="7" style="font-weight:bold; padding-left:5px; padding-right:0px; cursor:pointer;" onClick="showDialog(this);">1.1 ด้านวางแผน</td>
										              <td style="font-weight:bold;" >
										                  <div class="btn-group">
										                    <button class="btn dropdown-toggle mini" data-toggle="dropdown">ดำเนินการ <i class="icon-angle-down"></i></button>
										                    <ul class="dropdown-menu pull-right">
										                      <li><a href="javascript:;"><i class="icon-indent-right"></i> แทรกงาน</a></li>
										                      <li><a href="javascript:;"><i class="icon-circle-arrow-down"></i> เพิ่มการปฏิบัติ</a></li>
										                      <li><a href="javascript:;"><i class="icon-trash"></i>ลบงานนี้</a></li>
									                        </ul>
									                      </div>
										              </td>
										            </tr>
    										        
										            <tr>
										              <td>&nbsp;</td>
										              <td>&nbsp;</td>
										              <td onClick="showDialog(this);" style="padding-left:5px; padding-right:0px; cursor:pointer;" >การจัดทำแผน ฯ</td>
                                                      <td onclick="showDialog(this);" style="padding:5px; cursor:pointer;">วางแผนควบคุมและตรวจสอบการจัดทำแผน ฯ</td>
                                                      <td onclick="showDialog(this);" style="padding:5px; cursor:pointer;">&nbsp;</td>
                                                      <td onclick="showDialog(this);" style="padding:5px; cursor:pointer;">&nbsp;</td>
                                                      <td onclick="showDialog(this);" style="padding:5px; cursor:pointer;">&nbsp;</td>
                                                      <td onclick="showDialog(this);" style="padding:5px; cursor:pointer;">&nbsp;</td>
										              <td onclick="showDialog(this);" style="text-align:left;">
										                  <div class="btn-group">
										                    <button class="btn dropdown-toggle mini" data-toggle="dropdown">ดำเนินการ <i class="icon-angle-down"></i></button>
										                    <ul class="dropdown-menu pull-right">
										                      <li><a href="javascript:;"><i class="icon-indent-right"></i> แทรกรายการก่อนหน้า</a></li>
										                      <li><a href="javascript:;"><i class="icon-indent-right"></i> เพิ่มรายการถัดไป</a></li>
										                      <li><a href="javascript:;"><i class="icon-chevron-down"></i> เลื่อนลง</a></li>
										                      <li><a href="javascript:;"><i class="icon-trash"></i>ลบรายการนี้</a></li>
									                        </ul>
									                      </div>
										              </td>
									                </tr>
										            <tr>
										              <td>&nbsp;</td>
										              <td>&nbsp;</td>
										              <td onClick="showDialog(this);" style="padding-left:5px; padding-right:0px; cursor:pointer;" >งานเรื่องการขอจ้าง</td>
                                                      <td onclick="showDialog(this);" style="padding:5px; cursor:pointer;">ตรวจสอบการขอจ้างให้เป็นไปตามระเบียบ</td>
                                                      <td onclick="showDialog(this);" style="padding:5px; cursor:pointer;">&nbsp;</td>
                                                      <td onclick="showDialog(this);" style="padding:5px; cursor:pointer;">&nbsp;</td>
                                                      <td onclick="showDialog(this);" style="padding:5px; cursor:pointer;">&nbsp;</td>
                                                      <td onclick="showDialog(this);" style="padding:5px; cursor:pointer;">&nbsp;</td>
										              <td>
										                  <div class="btn-group">
										                    <button class="btn dropdown-toggle mini" data-toggle="dropdown">ดำเนินการ <i class="icon-angle-down"></i></button>
										                    <ul class="dropdown-menu pull-right">
										                      <li><a href="javascript:;"><i class="icon-indent-right"></i> แทรกรายการก่อนหน้า</a></li>
										                      <li><a href="javascript:;"><i class="icon-indent-right"></i> เพิ่มรายการถัดไป</a></li>
										                      <li><a href="javascript:;"><i class="icon-chevron-up"></i> เลื่อนขึ้น</a></li>
										                      <li><a href="javascript:;"><i class="icon-chevron-down"></i> เลื่อนลง</a></li>
										                      <li><a href="javascript:;"><i class="icon-trash"></i>ลบรายการนี้</a></li>
									                        </ul>
									                      </div>
										              </td>
									                </tr>
									                <tr>
										              <td>&nbsp;</td>
										              <td>&nbsp;</td>
										              <td onClick="showDialog(this);" style="padding-left:5px; padding-right:0px; cursor:pointer;" >การประชุมคณะทำงาน</td>
                                                      <td onclick="showDialog(this);" style="padding:5px; cursor:pointer;">เป็นคณะกรรมการเข้าประชุมคณะทำงาน ฯ</td>
                                                      <td onclick="showDialog(this);" style="padding:5px; cursor:pointer;">เป็นคณะกรรมการเข้าประชุมคณะทำงาน ฯ</td>
                                                      <td onclick="showDialog(this);" style="padding:5px; cursor:pointer;">&nbsp;</td>
                                                      <td onclick="showDialog(this);" style="padding:5px; cursor:pointer;">จัดเตรียมเอกสารสำหรับเข้าประชุม</td>
                                                      <td onclick="showDialog(this);" style="padding:5px; cursor:pointer;">จัดเตรียมเอกสารสำหรับเข้าประชุม</td>
										              <td>
										                  <div class="btn-group">
										                    <button class="btn dropdown-toggle mini" data-toggle="dropdown">ดำเนินการ <i class="icon-angle-down"></i></button>
										                    <ul class="dropdown-menu pull-right">
										                      <li><a href="javascript:;"><i class="icon-indent-right"></i> แทรกรายการก่อนหน้า</a></li>
										                      <li><a href="javascript:;"><i class="icon-indent-right"></i> เพิ่มรายการถัดไป</a></li>
										                      <li><a href="javascript:;"><i class="icon-chevron-up"></i> เลื่อนขึ้น</a></li>
										                      <li><a href="javascript:;"><i class="icon-trash"></i>ลบรายการนี้</a></li>
									                        </ul>
									                      </div>
										              </td>
									                </tr>
    									           
    									            
    									            
									                <tr>
										              <td>&nbsp;</td>
										              <td onClick="showDialog(this);" colspan="7" style="font-weight:bold; padding-left:5px; padding-right:0px; cursor:pointer;" >1.2 ระบบประเมินตัวชี้วัด</td>
										              <td style="font-weight:bold;" >
										                  <div class="btn-group">
										                    <button class="btn dropdown-toggle mini" data-toggle="dropdown">ดำเนินการ <i class="icon-angle-down"></i></button>
										                    <ul class="dropdown-menu pull-right">
										                      <li><a href="javascript:;"><i class="icon-indent-right"></i> แทรกงาน</a></li>
										                      <li><a href="javascript:;"><i class="icon-circle-arrow-down"></i> เพิ่มการปฏิบัติ</a></li>
										                      <li><a href="javascript:;"><i class="icon-trash"></i>ลบงานนี้</a></li>
									                        </ul>
									                      </div>
										              </td>
										            </tr>
    										        
										            <tr>
										              <td>&nbsp;</td>
										              <td>&nbsp;</td>
										              <td onClick="showDialog(this);" style="padding-left:5px; padding-right:0px; cursor:pointer;" >งานพัฒนาความสามารถพนักงาน เพื่อสนับสนุนยุทธศาสตร์องค์กร</td>
                                                      <td onclick="showDialog(this);" style="padding:5px; cursor:pointer;">ตรวจสอบรายงานแผนพัฒนาความสามารถพนักงาน เพื่อสนับสนุนยุทธศาสตร์องค์กร</td>
                                                      <td onclick="showDialog(this);" style="padding:5px; cursor:pointer;">&nbsp;</td>
                                                      <td onclick="showDialog(this);" style="padding:5px; cursor:pointer;">&nbsp;</td>
                                                      <td onclick="showDialog(this);" style="padding:5px; cursor:pointer;">รายงานแผนพัฒนาความสามารถพนักงาน เพื่อสนับสนุนยุทธศาสตร์องค์กร</td>
                                                      <td onclick="showDialog(this);" style="padding:5px; cursor:pointer;">รายงานแผนพัฒนาความสามารถพนักงาน เพื่อสนับสนุนยุทธศาสตร์องค์กร</td>
										              <td>
										                  <div class="btn-group">
										                    <button class="btn dropdown-toggle mini" data-toggle="dropdown">ดำเนินการ <i class="icon-angle-down"></i></button>
										                    <ul class="dropdown-menu pull-right">
										                      <li><a href="javascript:;"><i class="icon-indent-right"></i> แทรกรายการก่อนหน้า</a></li>
										                      <li><a href="javascript:;"><i class="icon-indent-right"></i> เพิ่มรายการถัดไป</a></li>
										                      <li><a href="javascript:;"><i class="icon-chevron-down"></i> เลื่อนลง</a></li>
										                      <li><a href="javascript:;"><i class="icon-trash"></i>ลบรายการนี้</a></li>
									                        </ul>
									                      </div>
										              </td>
									                </tr>
										            <tr>
										              <td>&nbsp;</td>
										              <td>&nbsp;</td>
										              <td onClick="showDialog(this);" style="padding-left:5px; padding-right:0px; cursor:pointer;">งานปรับปรุงอัตรากำลัง กรณีย้ายโรงงาน</td>
                                                      <td onclick="showDialog(this);" style="padding:5px; cursor:pointer;">ตรวจสอบก่อนลงนามในรายงานแผนงานปรับปรุงอัตรากำลัง กรณีย้ายโรงงาน</td>
                                                      <td onclick="showDialog(this);" style="padding:5px; cursor:pointer;">ตรวจสอบรายงานแผนงานปรับปรุงอัตรากำลัง กรณีย้ายโรงงาน</td>
                                                      <td onclick="showDialog(this);" style="padding:5px; cursor:pointer;">&nbsp;</td>
                                                      <td onclick="showDialog(this);" style="padding:5px; cursor:pointer;">รายงานแผนงานปรับปรุงอัตรากำลัง กรณีย้ายโรงงาน</td>
                                                      <td onclick="showDialog(this);" style="padding:5px; cursor:pointer;">รายงานแผนงานปรับปรุงอัตรากำลัง กรณีย้ายโรงงาน</td>
										              <td>
										                  <div class="btn-group">
										                    <button class="btn dropdown-toggle mini" data-toggle="dropdown">ดำเนินการ <i class="icon-angle-down"></i></button>
										                    <ul class="dropdown-menu pull-right">
										                      <li><a href="javascript:;"><i class="icon-indent-right"></i> แทรกรายการก่อนหน้า</a></li>
										                      <li><a href="javascript:;"><i class="icon-indent-right"></i> เพิ่มรายการถัดไป</a></li>
										                      <li><a href="javascript:;"><i class="icon-chevron-up"></i> เลื่อนขึ้น</a></li>
										                      <li><a href="javascript:;"><i class="icon-trash"></i>ลบรายการนี้</a></li>
									                        </ul>
									                      </div>
										              </td>
									                </tr>
    									            
									                <tr>
										              <td colspan="8" style="font-weight:bold; padding-left:5px; padding-right:0px; cursor:pointer;" onClick="showDialog(this);">2. งานสรรหาคัดเลือก</td>
										              <td style="text-align:left; width:80px;">
										                  <div class="btn-group">
										                    <button class="btn dropdown-toggle mini" data-toggle="dropdown">ดำเนินการ <i class="icon-angle-down"></i></button>
										                    <ul class="dropdown-menu pull-right">
										                      <li><a href="javascript:;"><i class="icon-indent-right"></i> แทรกหมวด</a></li>
										                      <li><a href="javascript:;"><i class="icon-circle-arrow-down"></i> เพิ่มงาน</a></li>
										                      <li><a href="javascript:;"><i class="icon-trash"></i>ลบหมวดนี้</a></li>
									                        </ul>
									                      </div>
										              </td>
									                </tr>
									                <tr>
										              <td>&nbsp;</td>
										              <td onClick="showDialog(this);" colspan="2" style="padding-left:5px; padding-right:0px; cursor:pointer;" >งานเรื่องการขอจ้าง</td>										          
                                                      <td onclick="showDialog(this);" style="padding:5px; cursor:pointer;">-ตรวจทานความถูกต้อง ก่อนลงนามในสรุปเรื่องขอจ้าง</td>
                                                      <td onclick="showDialog(this);" style="padding:5px; cursor:pointer;">-ตรวจทานความถูกต้อง ก่อนลงนามในสรุปเรื่องขอจ้าง</td>
                                                      <td onclick="showDialog(this);" style="padding:5px; cursor:pointer;">-ตรวจสอบอัตราว่าง<br>-สรุปเรื่องขอจ้างเสนอเข้าที่ประชุม คณะกรรมการบริหารงานบุคคล<br></td>
                                                      <td onclick="showDialog(this);" style="padding:5px; cursor:pointer;">-ตรวจสอบอัตราว่าง<br>-สรุปเรื่องขอจ้างเสนอเข้าที่ประชุม คณะกรรมการบริหารงานบุคคล<br></td>
                                                      <td onclick="showDialog(this);" style="padding:5px; cursor:pointer;">&nbsp;</td>
										              <td>
										                  <div class="btn-group">
										                    <button class="btn dropdown-toggle mini" data-toggle="dropdown">ดำเนินการ <i class="icon-angle-down"></i></button>
										                    <ul class="dropdown-menu pull-right">
										                      <li><a href="javascript:;"><i class="icon-indent-right"></i> แทรกรายการก่อนหน้า</a></li>
										                      <li><a href="javascript:;"><i class="icon-indent-right"></i> เพิ่มรายการถัดไป</a></li>
										                      <li><a href="javascript:;"><i class="icon-chevron-down"></i> เลื่อนลง</a></li>
										                      <li><a href="javascript:;"><i class="icon-trash"></i>ลบรายการนี้</a></li>
									                        </ul>
									                      </div>
										              </td>
									                </tr>
									                <tr>
										              <td>&nbsp;</td>
										              <td onClick="showDialog(this);" colspan="2" style="padding-left:5px; padding-right:0px; cursor:pointer;" >การสอบคัดเลือก</td>
										              <td onclick="showDialog(this);" style="padding:5px; cursor:pointer;">&nbsp;</td>
                                                      <td onclick="showDialog(this);" style="padding:5px; cursor:pointer;">&nbsp;</td>
                                                      <td onclick="showDialog(this);" style="padding:5px; cursor:pointer;">-เสนอแต่งตั้งคุณะกรรมการในการสอบคัดเลือก<br>-ประชุมคณะกรรมการเพื่อกำหนดหลักเกณฑ์ในการสอบ<br></td>
                                                      <td onclick="showDialog(this);" style="padding:5px; cursor:pointer;">-เสนอแต่งตั้งคุณะกรรมการในการสอบคัดเลือก<br>-ประชุมคณะกรรมการเพื่อกำหนดหลักเกณฑ์ในการสอบ<br></td>
                                                      <td onclick="showDialog(this);" style="padding:5px; cursor:pointer;">&nbsp;</td>
										              <td>
										                  <div class="btn-group">
										                    <button class="btn dropdown-toggle mini" data-toggle="dropdown">ดำเนินการ <i class="icon-angle-down"></i></button>
										                    <ul class="dropdown-menu pull-right">
										                      <li><a href="javascript:;"><i class="icon-indent-right"></i> แทรกรายการก่อนหน้า</a></li>
										                      <li><a href="javascript:;"><i class="icon-indent-right"></i> เพิ่มรายการถัดไป</a></li>
										                      <li><a href="javascript:;"><i class="icon-chevron-up"></i> เลื่อนขึ้น</a></li>
										                      <li><a href="javascript:;"><i class="icon-trash"></i>ลบรายการนี้</a></li>
									                        </ul>
									                      </div>
										              </td>
									                </tr>

    									   	       							
									            </tbody>
								            </table>
							    </div>
				        <!-- END FORM-->  
    				    
    				    
				                                    <div class="form-actions">
												        <asp:Button ID="Button1" runat="server" CssClass="btn blue" Text="เพิ่มหมวด" />
												        <asp:Button ID="btnClear" runat="server" CssClass="btn red" Text="ลบแผนที่งานทั้งหมด" />
												        <asp:Button ID="btnBack" OnClick="HideDetail_Click" runat="server" CssClass="btn" Text="ย้อนกลับ" />
												    </div>
    				    
				    </div>
                
                      <div class="modal" id="divTest" runat="server" style="width:400px; top:20%; visibility:hidden;" >
			            <div class="modal-header">										
				            <h3>รายละเอียด</h3>
			            </div>
			            <div class="modal-body" style="padding:0px; overflow:hidden;">
            						        
						                   <textarea rows="3" id="contentDialog" class="large m-wrap" style="width:100% !important; height:100%; margin:0px;"></textarea>
            				
			            </div>
			            <div class="modal-footer">
			                
			                <input type="button" Class="btn blue" value="บันทึก" onclick="document.getElementById('ctl00_ContentPlaceHolder1_divTest').style.visibility='hidden';" />
			                <input type="button" Class="btn" value="ยกเลิก" onclick="document.getElementById('ctl00_ContentPlaceHolder1_divTest').style.visibility='hidden';" />
			            </div>
		            </div> 
				</asp:Panel>

							 
</ContentTemplate>				                
</asp:UpdatePanel>	


<script language="javascript" type="text/javascript">


    function showDialog(td) {
        var txt = td.innerHTML.toString().replace('&nbsp;', ' ').replace('<br>', '\n');
        while (txt.indexOf('<br>') > -1)
        { txt = txt.replace('<br>', '\n'); }
        while (txt.indexOf('&nbsp;') > -1)
        { txt = txt.replace('&nbsp;', ' '); }

        document.getElementById('contentDialog').value = txt;
        document.getElementById('ctl00_ContentPlaceHolder1_divTest').style.visibility = 'visible';

    }
        
   </script>
   	 
</asp:Content>



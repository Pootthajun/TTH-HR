﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPage.Master" CodeBehind="Assessment_self_KPI_Create.aspx.cs" Inherits="VB.Assessment_self_KPI_Create" %>

<%@ Register src="WUC_DateReporter.ascx" tagname="WUC_DateReporter" tagprefix="uc1" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<asp:UpdatePanel ID="UpdatePanel1" runat="server">      
<ContentTemplate>
<asp:ToolkitScriptManager ID="toolkit1" runat="server"></asp:ToolkitScriptManager>

<div class="container-fluid">
				<!-- BEGIN PAGE HEADER-->
				<div class="row-fluid" id="fixedPageHeader">
					<div class="span12"> 
						<!-- BEGIN PAGE TITLE & BREADCRUMB-->
						<h3 class="page-user-md">กำหนดแบบประเมินตัวชี้วัดตนเอง (KPI)  <font color="blue">(ScreenID : S-EMP-01)</font>
                        <asp:Label ID="lblReportTime" Font-Size="14px" ForeColor="Green" runat="Server"></asp:Label>
                        </h3>					
									
						<ul class="breadcrumb">
                            <li>
                            	<i class="icon-user"></i> <a href="javascript:;">การประเมินตนเอง</a><i class="icon-angle-right"></i>
                            </li>
                        	<li>
                        	    <i class="icon-th-list"></i> <a href="javascript:;">กำหนดแบบประเมินตัวชี้วัด (KPI)</a>
                        	</li>                        	
                        	
                            <uc1:WUC_DateReporter ID="WUC_DateReporter1" runat="server" />
                        </ul>
						<!-- END PAGE TITLE & BREADCRUMB-->									        
				     </div>

				
			    </div>
		        <!-- END PAGE HEADER-->
				                                
                                
				<div class="portlet-body form" id="divView" runat="server">
				
                    <div class="btn-group pull-right">
			            <div class="btn-group">
                            <a data-toggle="dropdown" class="btn  red dropdown-toggle"  data-trigger="hover" data-container="body" data-placement="top" data-content="Popover body goes here! Popover body goes here!" data-original-title="Popover in top">แก้ไข <i class="icon-angle-down"></i></a>										
			                <ul class="dropdown-menu pull-right" >                                                       
                                <%--<li><a id="btnCopyAll" runat="server" target="_blank">คัดลอก KPI</a></li>
                                <li><a id="btnPasteAll" runat="server" target="_blank">วาง</a></li>--%>
                                <li><a ><asp:LinkButton id="lnkCopyAll" OnClick="lnkCopyAll_Click" runat ="server"  ><i class="icon-copy"></i> คัดลอกทั้งหมด</asp:LinkButton></a></li>	
                                <li><a ><asp:LinkButton id="lnkPasteAll" OnClick="lnkPasteAll_Click" runat ="server" ><i class="icon-paste"></i> วางแทนที่ทั้งหมด</asp:LinkButton></a></li>											
			                </ul>
                        </div>

                        <div class="btn-group">
                            <a data-toggle="dropdown" class="btn dropdown-toggle">พิมพ์ <i class="icon-angle-down"></i></a>										
			                <ul class="dropdown-menu pull-right">                                                       
                                <li><a id="btnPDF" runat="server" target="_blank">รูปแบบ PDF</a></li>
                                <li><a id="btnExcel" runat="server" target="_blank">รูปแบบ Excel</a></li>											
			                </ul>
                        </div>                                    
		            </div>
                     <div id="boxAlertEdit" runat ="server"  class="alert" style =" margin-top :-10px;">
							<li>
								สำหรับ <b style ="">การคัดลอกและวาง</b> แบบประเมินตัวชี้วัด (KPI)  ทั้งตาราง
							</li>
							<li>
								โดยสามารถ เปิดหน้าแบบประเมินที่ต้องการหรือเลือกจากรอบประเมินที่ต้องการคัดลอก คลิกปุ่ม <b>แก้ไข > คัดลอกทั้งหมด</b>
							</li>
							<li>
								<u>วิธีการวาง</u> เปิดหน้า กำหนดแบบประเมินตัวชี้วัด (KPI) ที่ต้องการ วางแบบประเมิน คลิกปุ่ม <b>แก้ไข > วางแทนที่ทั้งหมด</b>
							</li>
					</div>
								            
			        <h3 class="form-section"><i class="icon-th-list"></i>
			            <asp:Label ID="lblName" runat="server"></asp:Label>
			            สำหรับรอบการประเมิน
			            <asp:DropDownList ID="ddlRound" OnSelectedIndexChanged="ddlRound_SelectedIndexChanged" runat="server" AutoPostBack="true" CssClass="medium m-wrap header" style="font-size:20px;">
					    </asp:DropDownList>
					    (<asp:Label ID="lblKPIStatus" runat="server" KPI_Status="0"></asp:Label>)                        
			        </h3>
			        
			                                    
			        
    			    <div class="form-horizontal form-view">
    			        
    			        
					     <div class="row-fluid" style="border-bottom:1px solid #EEEEEE;">
					        <div class="span4 ">
					            <div class="control-group">
								    <label class="control-label" style="width:100px;">ผู้รับการประเมิน :</label>
								    <div class="controls" style="margin-left: 120px;">
									    <asp:Label ID="lblPSNName" runat="server" CssClass="text bold"></asp:Label>
								    </div>
							    </div>
					        </div>
					        <div class="span3 ">
					            <div class="control-group">
								    <label class="control-label" style="width:100px;">ตำแหน่ง :</label>
								    <div class="controls" style="margin-left: 120px;">
									    <asp:Label ID="lblPSNPos" runat="server" CssClass="text bold"></asp:Label>
								    </div>
							    </div>
					        </div>
					        <div class="span5 ">
					            <div class="control-group">
								    <label class="control-label" style="width:100px;">สังกัด :</label>
								    <div class="controls" style="margin-left: 120px;">
									    <asp:Label ID="lblPSNDept" runat="server" CssClass="text bold"></asp:Label>
								    </div>
							    </div>
					        </div>
					     </div>
					     
					      <div class="row-fluid" style="margin-top:10px;">
					        <div class="span4 ">
					            <div class="control-group">
								    <label class="control-label" style="width:100px;">ผู้ประเมิน :</label>
								    <div class="controls" style="margin-left: 120px;">
									    <asp:DropDownList ID="ddlASSName" OnSelectedIndexChanged="ddlASSName_SelectedIndexChanged" runat="Server" AutoPostBack="True" style="font-weight:bold; color:Black; border:none;">									        
									    </asp:DropDownList>
									</div>
							    </div>
					        </div>
					        <div class="span3 ">
					            <div class="control-group">
								    <label class="control-label" style="width:100px;">ตำแหน่ง :</label>
								    <div class="controls" style="margin-left: 120px;">
									    <asp:Label ID="lblASSPos" runat="server" CssClass="text bold"></asp:Label>
								    </div>
							    </div>
					        </div>
					        <div class="span5 ">
					            <div class="control-group">
								    <label class="control-label" style="width:100px;">สังกัด :</label>
								    <div class="controls" style="margin-left: 120px;">
									    <asp:Label ID="lblASSDept" runat="server" CssClass="text bold"></asp:Label>
								    </div>
							    </div>
					        </div>
					     </div>
    			    </div>
    			  								
				    <!-- BEGIN FORM-->
				    <div class="portlet-body no-more-tables">
				              
				                        <table class="table table-bordered no-more-tables" style="border-bottom:none; background-color:White;">
									        <thead>
										        <tr>
											        <th rowspan="2" style="text-align:center; background-color:Silver;"> ลำดับ</th>
											        <th rowspan="2" style="text-align:center; background-color:Silver;"> งาน</th>
											        <th rowspan="2" style="text-align:center; background-color:Silver;"> ตัวชี้วัดผลงาน</th>
											        <th colspan="5" style="text-align:center; background-color:Silver;"> คะแนนตามระดับค่าเป้าหมาย</th>
											        <th rowspan="2" style="text-align:center; background-color:Silver;"> น้ำหนัก(%)</th>											        
										        </tr>
                                                <tr>
										          <th style="text-align:center; font-weight:bold; vertical-align:middle; background-color:Silver;">1</th>
										          <th style="text-align:center; font-weight:bold; vertical-align:middle; background-color:Silver;">2</th>
										          <th style="text-align:center; font-weight:bold; vertical-align:middle; background-color:Silver;">3</th>
										          <th style="text-align:center; font-weight:bold; vertical-align:middle; background-color:Silver;">4</th>
										          <th style="text-align:center; font-weight:bold; vertical-align:middle; background-color:Silver;">5</th>
								              </tr>
									        </thead>
									        <tbody>
									            <asp:Panel ID="pnlKPI" runat="server">							            
									                <asp:Repeater ID="rptAss" OnItemCommand="rptAss_ItemCommand" OnItemDataBound="rptAss_ItemDataBound" runat="server">
									                <ItemTemplate>
									                <tr>
										              <td data-title="ลำดับ" id="ass_no" runat="server" title="Click เพื่อแก้ไข" style="text-align:center; font-weight:bold; vertical-align:top; cursor:pointer;">001</td>
										              <td data-title="งาน" id="job" rowspan="2" title="Click เพื่อแก้ไข" runat="server" style="background-color:#f8f8f8; cursor:pointer;">xxxxxxxxxxxx xxxxxxxxxxxx</td>
										              <td data-title="ตัวชี้วัดผลงาน" rowspan="2" id="target" runat="server" title="Click เพื่อแก้ไข" class="TextLevel0" style="background-color:#f8f8f8; cursor:pointer; ">xxxxxxxxxxxx xxxxxxxxxxxx</td>
										              <td data-title="ระดับ 1 คะแนน" rowspan="2" title="Click เพื่อตั้งเป้าหมาย" id="choice1" class="TextLevel0" runat="server" style="cursor:pointer;">xxxxxxxxxxxx xxxxxxxxxxxx xxxxxxxxxxxx xxxxxxxxxxxx xxxxxxxxxxxx xxxxxxxxxxxx</td>
                                                      <td data-title="ระดับ 2 คะแนน" rowspan="2" title="Click เพื่อตั้งเป้าหมาย" id="choice2" class="TextLevel0" runat="server" style="cursor:pointer;">xxxxxxxxxxxx xxxxxxxxxxxx xxxxxxxxxxxx xxxxxxxxxxxx xxxxxxxxxxxx xxxxxxxxxxxx</td>
                                                      <td data-title="ระดับ 3 คะแนน" rowspan="2" title="Click เพื่อตั้งเป้าหมาย" id="choice3" class="TextLevel0" runat="server" style="cursor:pointer;">xxxxxxxxxxxx xxxxxxxxxxxx xxxxxxxxxxxx xxxxxxxxxxxx xxxxxxxxxxxx xxxxxxxxxxxx</td>
                                                      <td data-title="ระดับ 4 คะแนน" rowspan="2" title="Click เพื่อตั้งเป้าหมาย" id="choice4" class="TextLevel0" runat="server" style="cursor:pointer;">xxxxxxxxxxxx xxxxxxxxxxxx xxxxxxxxxxxx xxxxxxxxxxxx xxxxxxxxxxxx xxxxxxxxxxxx</td>
                                                      <td data-title="ระดับ 5 คะแนน" rowspan="2" title="Click เพื่อตั้งเป้าหมาย" id="choice5" class="TextLevel0" runat="server" style="cursor:pointer;">xxxxxxxxxxxx xxxxxxxxxxxx xxxxxxxxxxxx xxxxxxxxxxxx xxxxxxxxxxxx xxxxxxxxxxxx</td>
										              <td data-title="น้ำหนัก" id="weight" runat="server" title="Click เพื่อแก้ไข" style="text-align:center; cursor:pointer; font-size:16px; font-weight:bold; vertical-align:middle;">10%</td>
    										          
									                </tr>
									                <tr>
									                      <td data-title="ลบ" id="cel_delete" runat="server" style="cursor:pointer; font-weight:bold; text-align:center; padding:0px !important; height:10px !important; background-color:silver;">
									                      <asp:Button ID="btn_Delete" runat="server" style="display:none; " CommandName="Delete" />ลบ
									                      <asp:ConfirmButtonExtender TargetControlID="btn_Delete" ID="cfm_Delete" runat="server"></asp:ConfirmButtonExtender>
									                      </td>
									                      
                                                          <td data-title="แก้ไข" id="cel_edit" runat="server" style="cursor:pointer; font-weight:bold; text-align:center; padding:0px !important; height:10px !important; background-color:Silver;"><asp:Button ID="btn_Edit" runat="server" style="display:none; " CommandName="Edit"  />แก้ไข</td>
									                </tr>
									                </ItemTemplate>
									                <FooterTemplate>									                
									                 <tfoot>
									                        <tr>
									                            <td colspan="8" style="text-align:center; background-color:#EEEEEE; font-size:14px; font-weight:bold;">
									                            ผลรวมน้ำหนัก
									                            </td>
									                            <td style="text-align:center; background-color:#EEEEEE; font-size:16px; font-weight:bold;" id="cell_sum_weight" runat="server">
									                            
									                            </td>
									                        </tr>
									                    </tfoot>									                
									                </FooterTemplate>
									            </asp:Repeater>
									            </asp:Panel>	
									        </tbody>									        
								        </table>
								        
						                <asp:Panel CssClass="form-actions" id="pnlActivity" runat="server">
										    <asp:Button ID="btnAdd" OnClick="btnAdd_Click" runat="server" CssClass="btn purple" Text="เพิ่มตัวชี้วัด" />
											<asp:Button ID="btnPreSend" OnClick="btnPreSend_Click" runat="server" CssClass="btn green" Text="ส่งแบบประเมินให้หัวหน้า/ผู้ประเมิน  (หลังจากส่งแบบประเมินแล้วคุณไม่สามารถแก้ไขได้)" />
											<asp:Button ID="btnSend" OnClick="btnSend_Click" runat="server" Text="" style="display:none;" />
											
										</asp:Panel>
				               
							</div>
						
				    <!-- END FORM-->  
				   
			    </div>	
				
	            <div class="row-fluid" id="divEdit" runat="server" Visible="False">
                    <div class="span12">
				                    <div class="portlet-body form">
									    <h3 class="form-section"><i class="icon-th-list"></i> 
									        กำหนดตัวชี้วัดผลงาน สำหรับรอบการประเมิน <asp:Label ID="lblKPIRound" runat="server"></asp:Label><br>
									    </h3>
									    <h4 class="form-section"><i class="icon-th-list"></i> ลำดับ <asp:Label ID="lblKPINo" runat="server"></asp:Label></h4>
									    <div class="row-fluid form-horizontal">
									        <div class="span4 ">
											    <div class="control-group">
										            <label class="control-label">งาน</label>
										            <div class="controls">
										                <asp:TextBox TextMode="MultiLine" runat="server" ID="txt_KPI_Job" CssClass="large m-wrap" Height="100px"></asp:TextBox>
											        </div>
									            </div>
									            <div class="control-group">
										            <label class="control-label">ตัวชี้วัดผลงาน</label>
										            <div class="controls">
											            <asp:TextBox TextMode="MultiLine" runat="server" ID="txt_KPI_Target" CssClass="large m-wrap" Height="100px"></asp:TextBox>
										            </div>
									            </div>
									            <div class="control-group">
										            <label class="control-label">1 คะแนน</label>
										            <div class="controls">
											            <asp:TextBox TextMode="MultiLine" runat="server" ID="txt_KPI_Choice1" CssClass="large m-wrap" Height="100px"></asp:TextBox>
										            </div>
									            </div>
									            <div class="control-group">
										            <label class="control-label">2 คะแนน</label>
										            <div class="controls">
											            <asp:TextBox TextMode="MultiLine" runat="server" ID="txt_KPI_Choice2" CssClass="large m-wrap" Height="100px"></asp:TextBox>
										            </div>
									            </div>
									            <div class="control-group">
										            <label class="control-label">3 คะแนน</label>
										            <div class="controls">
											            <asp:TextBox TextMode="MultiLine" runat="server" ID="txt_KPI_Choice3" CssClass="large m-wrap" Height="100px"></asp:TextBox>
										            </div>
									            </div>
									            <div class="control-group">
										            <label class="control-label">4 คะแนน</label>
										            <div class="controls">
											            <asp:TextBox TextMode="MultiLine" runat="server" ID="txt_KPI_Choice4" CssClass="large m-wrap" Height="100px"></asp:TextBox>
										            </div>
									            </div>
									            <div class="control-group">
										            <label class="control-label">5 คะแนน</label>
										            <div class="controls">
											            <asp:TextBox TextMode="MultiLine" runat="server" ID="txt_KPI_Choice5" CssClass="large m-wrap" Height="100px"></asp:TextBox>
										            </div>
									            </div>
									            <div class="control-group">
										            <label class="control-label">น้ำหนัก</label>
										            <div class="controls">
											            <asp:TextBox runat="server" ID="txt_KPI_Weight" CssClass="m-wrap small" ></asp:TextBox>
											            <span class="help-inline">%</span>
										            </div>
									            </div>
										    </div>													
									    </div>
								    <div class="form-actions">
								        <asp:Button ID="btnOK" OnClick="btnOK_Click" runat="server" CssClass="btn blue" Text="บันทึก" />
								        <asp:Button ID="btnClose" OnClick="btnClose_Click" runat="server" CssClass="btn" Text="ยกเลิก" />							            
						            </div>												
							    </div>
						</div>
                </div>                                  		 
</div>	

</ContentTemplate>           
</asp:UpdatePanel>					 
</asp:Content>


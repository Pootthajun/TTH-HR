﻿<%@ Page  Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPage.Master" CodeBehind="AssessmentSetting_WKL_Workload.aspx.cs" Inherits="VB.AssessmentSetting_WKL_Workload" %>

<%@ Register src="WUC_DateReporter.ascx" tagname="WUC_DateReporter" tagprefix="uc1" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">


<asp:UpdatePanel ID="UpdatePanel1" runat="server">
<ContentTemplate>
<asp:ToolkitScriptManager ID="toolkit1" runat="server"></asp:ToolkitScriptManager>

<div class="container-fluid">
				<!-- BEGIN PAGE HEADER-->
				<div class="row-fluid" id="fixedPageHeader">
					<div class="span12"> 
						<!-- BEGIN PAGE TITLE & BREADCRUMB-->
						<h3 class="page-title">การกำหนดตารางภารกิจงาน <font color="blue">(ScreenID : S-HDR-11)</font></h3>
						
									
						<ul class="breadcrumb">
                            <li>
                            	<i class="icon-th-list"></i><a href="javascript:;">การกำหนดข้อมูลการประเมิน</a><i class="icon-angle-right"></i>
                            </li>
                            <li>
                            	<i class="icon-dashboard"></i><a href="javascript:;">การบริหารอัตรากำลัง</a><i class="icon-angle-right"></i>
                            </li>
                        	<li>
                        	    <i class="icon-dashboard"></i> <a href="javascript:;">การกำหนดตารางภารกิจงาน</a>
                        	</li>
                        	
                        	
                            <uc1:WUC_DateReporter ID="WUC_DateReporter1" runat="server" />
                        </ul>
						<!-- END PAGE TITLE & BREADCRUMB-->
									        
				     </div>

				
			    </div>
		        <!-- END PAGE HEADER-->
				
				<asp:Panel ID="pnlList" runat="server" Visible="True">

               

                <div class="row-fluid">
					
					<div class="span12">
						<!-- BEGIN SAMPLE TABLE PORTLET-->
								                           
							    
							    <h4 class="control-label"> <i class="icon-sitemap"></i> ฝ่าย
							    
								    <asp:DropDownList ID="DropDownList1" runat="server" CssClass="medium m-wrap">
						            <asp:ListItem>ทั้งหมด</asp:ListItem>
						            <asp:ListItem>ฝ่ายจัดหาและรักษาพัสดุ</asp:ListItem>
						            <asp:ListItem>ฝ่ายวิจัยและพัฒนา</asp:ListItem>
						            <asp:ListItem>ฝ่ายผลิต</asp:ListItem>
						            
									</asp:DropDownList>
						            &nbsp; &nbsp;
						            ชื่อหน่วยงาน
						            <input type="text" placeholder="ค้นหาจากชื่อหน่วยงาน/กอง" style=" background-color:White; margin-top:5px;margin-left:10px;" class="m-wrap large">
						            
							   </h4>
							   
							  
							    
						    <div class="portlet-body no-more-tables">
								<table class="table table-full-width table-advance dataTable no-more-tables table-hover">
									<thead>
										<tr>
											<th><i class="icon-sitemap"></i> ฝ่าย</th>
											<th><i class="icon-sitemap"></i> ส่วน</th>
											<th><i class="icon-sitemap"></i> กอง</th>
											<th><i class="icon-group"></i> จำนวนบุคลากร</th>
											<th><i class="icon-dashboard"></i> FTE</th>
											<th><i class="icon-time"></i> ปรับปรุงล่าสุด</th>
											<th><i class="icon-bolt"></i> ดำเนินการ</th>
											<th></th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td data-title="หน่วยงาน">ฝ่ายพัฒนาธุรกิจ</td>
											<td data-title="ส่วน">ศูนย์เทคโนโลยีสารสนเทศ(เดิม)</td>
											<td data-title="ส่วน">งานธุรการ</td>
											<td data-title="จำนวนบุคลากร" style="color:red">7</td>
											<td data-title="FTE" style="color:red">8.04</td>
											<td data-title="ปรับปรุงล่าสุด">24/01/2557</td>
											<td data-title="ดำเนินการ"><asp:LinkButton ID="LinkButton1" OnClick="LinkButton_Click" runat="server" CssClass="btn mini blue"><i class="icon-search"></i> แสดง</asp:LinkButton></td>
										</tr>
										<tr>
											<td data-title="หน่วยงาน">ฝ่ายพัฒนาธุรกิจ</td>
											<td data-title="ส่วน">ศูนย์เทคโนโลยีสารสนเทศ(เดิม)</td>
											<td data-title="ส่วน">กองเทคนิคและเครือข่าย</td>
											<td data-title="จำนวนบุคลากร" style="color:red">8</td>
											<td data-title="FTE" style="color:red">9.75</td>
											<td data-title="ปรับปรุงล่าสุด">02/02/2558</td>
											<td data-title="ดำเนินการ"><asp:LinkButton ID="LinkButton2" OnClick="LinkButton_Click" runat="server" CssClass="btn mini blue"><i class="icon-search"></i> แสดง</asp:LinkButton></td>
										</tr>
										<tr>
											<td data-title="หน่วยงาน">ฝ่ายพัฒนาธุรกิจ</td>
											<td data-title="ส่วน">ศูนย์เทคโนโลยีสารสนเทศ(เดิม)</td>
											<td data-title="ส่วน">กองสารสนเทศ</td>
											<td data-title="จำนวนบุคลากร" style="color:green">12</td>
											<td data-title="FTE" style="color:green">10.55</td>
											<td data-title="ปรับปรุงล่าสุด">14/08/2557</td>
											<td data-title="ดำเนินการ"><asp:LinkButton ID="LinkButton3" OnClick="LinkButton_Click" runat="server" CssClass="btn mini blue"><i class="icon-search"></i> แสดง</asp:LinkButton></td>
										</tr>
										<tr>
											<td data-title="หน่วยงาน">ฝ่ายพัฒนาธุรกิจ</td>
											<td data-title="ส่วน">ศูนย์เทคโนโลยีสารสนเทศ(เดิม)</td>
											<td data-title="ส่วน">กองพัฒนาโปรแกรมสำเร็จรูป</td>
											<td data-title="จำนวนบุคลากร">4</td>
											<td data-title="FTE">3.50</td>
											<td data-title="ปรับปรุงล่าสุด">05/07/2557</td>
											<td data-title="ดำเนินการ"><asp:LinkButton ID="LinkButton4" OnClick="LinkButton_Click" runat="server" CssClass="btn mini blue"><i class="icon-search"></i> แสดง</asp:LinkButton></td>
										</tr>									
									</tbody>
								</table>
							</div>
						
						<!-- END SAMPLE TABLE PORTLET-->
						
					</div>
                </div>
               </asp:Panel>
			
                <asp:Panel ID="pnlEdit" runat="server" Visible="False">
                <div class="portlet-body form">
											
					<!-- BEGIN HEADER-->
					<div class="form-horizontal form-view">        
						<h3 class="form-section"><i class="icon-dashboard"></i> การทำตารางสรุปภารกิจงาน (Workload) สำหรับ ฝ่ายพัฒนาธุรกิจ ศูนย์เทคโนโลยีสารสนเทศ(เดิม) งานธุรการ</h3>
					</div>
					
					<!-- END HEADER-->  
					
					 <!-- BEGIN FORM-->
				                <div class="portlet-body no-more-tables">
				              
				                                    <table class="table table-bordered" style="background-color:White;">
									                    <thead>
										                    <tr>
											                    <th rowspan="2" style="text-align:center; background-color:#CCCCCC; padding:0px !important; width:30px; font-size:13px;"> ลำดับ</th>
											                    <th rowspan="2" style="text-align:center; background-color:#CCCCCC; padding:0px !important; font-size:13px;"> งาน</th>
											                    <th rowspan="2"style="text-align:center; background-color:#CCCCCC; padding:0px !important; font-size:13px; width:auto; min-width:100px;" >จำนวน<br>ผู้ปฏิบัติ</th>
											                    <th rowspan="2"style="text-align:center; background-color:#CCCCCC; padding:0px !important; font-size:13px; width:auto; min-width:100px;">จำนวน<br>นาทีที่ใช้<br>ในการ<br>ปฏิบัติ<br>ต่อครั้ง</th>
											                    <th rowspan="2"style="text-align:center; background-color:#CCCCCC; padding:0px !important; font-size:13px; width:auto; min-width:60px;">รวม</th>
											                    <th rowspan="2"style="text-align:center; background-color:#CCCCCC; padding:0px !important; font-size:13px; width:auto; min-width:80px;">จำนวนครั้ง<br>(ความถี่)<br>ที่ใช้ใน<br>การปฏิบัติ<br>ใน 1 ปี</th>
											                    <th rowspan="2"style="text-align:center; background-color:#CCCCCC; padding:0px !important; font-size:13px; width:auto; min-width:80px;">จำนวน<br>นาทีที่ใช้<br>ใน 1 ปี</th>
											                    <th style="text-align:center; background-color:white; padding:0px !important; font-size:13px; min-width:150px;">
											                        <div class="btn-group" style="text-align:left;">
										                            <button class="dropdown-toggle" style="width:100% !important; background-color:white; font-weight:bold; border:0px;" data-toggle="dropdown">หัวหน้ากอง <i class="icon-angle-down"></i></button>
										                            <ul class="dropdown-menu pull-left">
										                              <li><a href="javascript:;"><i class="icon-user"></i> เพิ่มผู้ปฏิบัติ</a></li>
										                              <li><a href="javascript:;"><i class="icon-download"></i> ปรับจำนวนผู้ปฏิบัติตามฐานข้อมูลจริง</a></li>										                  
									                                </ul>
									                              </div>
											                    </th>
									                            <th style="text-align:center; background-color:white; padding:0px !important; font-size:13px; min-width:150px;">
									                              <div class="btn-group" style="text-align:left;">
										                            <button class="dropdown-toggle" style="width:100% !important; background-color:white; font-weight:bold; border:0px;" data-toggle="dropdown">ผู้ช่วยหัวหน้ากอง <i class="icon-angle-down"></i></button>
										                            <ul class="dropdown-menu pull-right">
										                              <li><a href="javascript:;"><i class="icon-user"></i> เพิ่มผู้ปฏิบัติ</a></li>
										                              <li><a href="javascript:;"><i class="icon-download"></i> ปรับจำนวนผู้ปฏิบัติตามฐานข้อมูลจริง</a></li>										                  
									                                </ul>
									                              </div>
									                            </th>
									                            <th style="text-align:center; background-color:white; padding:0px !important; font-size:13px; min-width:150px;">
									                              <div class="btn-group" style="text-align:left;">
										                            <button class="dropdown-toggle" style="width:100% !important; background-color:white; font-weight:bold; border:0px;" data-toggle="dropdown">พนักงานระดับ 6 <i class="icon-angle-down"></i></button>
										                            <ul class="dropdown-menu pull-right">
										                              <li><a href="javascript:;"><i class="icon-user"></i> เพิ่มผู้ปฏิบัติ</a></li>
										                              <li><a href="javascript:;"><i class="icon-download"></i> ปรับจำนวนผู้ปฏิบัติตามฐานข้อมูลจริง</a></li>										                  
									                                </ul>
									                              </div>
									                            </th>
									                            <th style="text-align:center; background-color:white; padding:0px !important; font-size:13px; min-width:150px;" colspan="3">
									                              <div class="btn-group" style="text-align:left;">
										                            <button class="dropdown-toggle" style="width:100% !important; background-color:white; font-weight:bold; border:0px;" data-toggle="dropdown">พนักงานระดับ 5 <i class="icon-angle-down"></i></button>
										                            <ul class="dropdown-menu pull-right">
										                              <li><a href="javascript:;"><i class="icon-user"></i> เพิ่มผู้ปฏิบัติ</a></li>
										                              <li><a href="javascript:;"><i class="icon-download"></i> ปรับจำนวนผู้ปฏิบัติตามฐานข้อมูลจริง</a></li>										                  
									                                </ul>
									                              </div>
									                            </th>
									                            <th style="text-align:center; background-color:white; padding:0px !important; font-size:13px; min-width:150px;" colspan="2">
									                              <div class="btn-group" style="text-align:left;">
										                            <button class="dropdown-toggle" style="width:100% !important; background-color:white; font-weight:bold; border:0px;" data-toggle="dropdown">พนักงานระดับ 4 <i class="icon-angle-down"></i></button>
										                            <ul class="dropdown-menu pull-right">
										                              <li><a href="javascript:;"><i class="icon-user"></i> เพิ่มผู้ปฏิบัติ</a></li>
										                              <li><a href="javascript:;"><i class="icon-download"></i> ปรับจำนวนผู้ปฏิบัติตามฐานข้อมูลจริง</a></li>										                  
									                                </ul>
									                              </div>
									                            </th>
									                            <th style="text-align:center; background-color:white; padding:0px !important; font-size:13px; min-width:150px;">
									                               <div class="btn-group" style="text-align:left;">
										                            <button class="dropdown-toggle" style="width:100% !important; background-color:white; font-weight:bold; border:0px;" data-toggle="dropdown">พนักงานระดับ 3 <i class="icon-angle-down"></i></button>
										                            <ul class="dropdown-menu pull-right">
										                              <li><a href="javascript:;"><i class="icon-user"></i> เพิ่มผู้ปฏิบัติ</a></li>
										                              <li><a href="javascript:;"><i class="icon-download"></i> ปรับจำนวนผู้ปฏิบัติตามฐานข้อมูลจริง</a></li>										                  
									                                </ul>
									                              </div>
									                            </th>
									                            <th style="text-align:center; background-color:white; padding:0px !important; font-size:13px; min-width:150px;" colspan="2">
									                               <div class="btn-group" style="text-align:left;">
										                            <button class="dropdown-toggle" style="width:100% !important; background-color:white; font-weight:bold; border:0px;" data-toggle="dropdown">พนักงานระดับ 2 <i class="icon-angle-down"></i></button>
										                            <ul class="dropdown-menu pull-right">
										                              <li><a href="javascript:;"><i class="icon-user"></i> เพิ่มผู้ปฏิบัติ</a></li>
										                              <li><a href="javascript:;"><i class="icon-download"></i> ปรับจำนวนผู้ปฏิบัติตามฐานข้อมูลจริง</a></li>										                  
									                                </ul>
									                              </div>
									                            </th>
									                            <th style="text-align:center; background-color:white; padding:0px !important; font-size:13px; min-width:150px;" colspan="2">
									                             <div class="btn-group" style="text-align:left;">
										                            <button class="dropdown-toggle" style="width:100% !important; background-color:white; font-weight:bold; border:0px;" data-toggle="dropdown">พนักงานระดับ 1 <i class="icon-angle-down"></i></button>
										                            <ul class="dropdown-menu pull-right">
										                              <li><a href="javascript:;"><i class="icon-user"></i> เพิ่มผู้ปฏิบัติ</a></li>
										                              <li><a href="javascript:;"><i class="icon-download"></i> ปรับจำนวนผู้ปฏิบัติตามฐานข้อมูลจริง</a></li>										                  
									                                </ul>
									                              </div>
									                            </th>
									                        </tr>      
            									            
									                        <tr>
											                    <th style="text-align:center; background-color:#e3ebf0; padding:0px !important; min-width:100px;">
											                     <div class="btn-group" style="text-align:left;">
										                            <button class="dropdown-toggle" style="width:100% !important; background-color:#e3ebf0; font-size:12px; font-weight:normal;  border:0px;" data-toggle="dropdown">นางชมพิศ	กิ่งภากรณ์<i class="icon-angle-down"></i></button>
										                            <ul class="dropdown-menu pull-right">
										                              <li><a href="javascript:;showDialog('นางชมพิศ	กิ่งภากรณ์');"><i class="icon-edit"></i> เปลี่ยนชื่อ</a></li>
										                              <li><a href="javascript:;"><i class="icon-trash"></i> ลบ</a></li>										                  
									                                </ul>
									                              </div>
											                    </th>
									                            <th style="text-align:center; background-color:#e3ebf0; padding:0px !important; font-size:12px; font-weight:normal; min-width:120px;">
									                              <div class="btn-group" style="text-align:left;">
										                            <button class="dropdown-toggle" style="width:100% !important; background-color:#e3ebf0; font-size:12px; font-weight:normal;  border:0px;" data-toggle="dropdown">นายเธียร	สัณสุตนานนท์<i class="icon-angle-down"></i></button>
										                            <ul class="dropdown-menu pull-right">
										                              <li><a href="javascript:;showDialog('นายเธียร	สัณสุตนานนท์');"><i class="icon-edit"></i> เปลี่ยนชื่อ</a></li>
										                              <li><a href="javascript:;"><i class="icon-trash"></i> ลบ</a></li>										                  
									                                </ul>
									                              </div>
									                            </th>
									                            <th style="text-align:center; background-color:#e3ebf0; padding:0px !important; font-size:12px; font-weight:normal; min-width:120px;">
									                              <div class="btn-group" style="text-align:left;">
										                            <button class="dropdown-toggle" style="width:100% !important; background-color:#e3ebf0; font-size:12px; font-weight:normal;  border:0px;" data-toggle="dropdown">นายสันติ	ภูอมรกุล<i class="icon-angle-down"></i></button>
										                            <ul class="dropdown-menu pull-right">
										                              <li><a href="javascript:;showDialog('นายสันติ	ภูอมรกุล');"><i class="icon-edit"></i> เปลี่ยนชื่อ</a></li>
										                              <li><a href="javascript:;"><i class="icon-trash"></i> ลบ</a></li>										                  
									                                </ul>
									                              </div>
									                            </th>
									                            <th style="text-align:center; background-color:#e3ebf0; padding:0px !important; font-size:12px; font-weight:normal; min-width:120px;">
									                              <div class="btn-group" style="text-align:left;">
										                            <button class="dropdown-toggle" style="width:100% !important; background-color:#e3ebf0; font-size:12px; font-weight:normal;  border:0px;" data-toggle="dropdown">นางขวัญใจ  วรวุฒิ<i class="icon-angle-down"></i></button>
										                            <ul class="dropdown-menu pull-right">
										                              <li><a href="javascript:;showDialog('นางขวัญใจ  วรวุฒิ');"><i class="icon-edit"></i> เปลี่ยนชื่อ</a></li>
										                              <li><a href="javascript:;"><i class="icon-trash"></i> ลบ</a></li>										                  
									                                </ul>
									                              </div>
									                            </th>
									                            <th style="text-align:center; background-color:#e3ebf0; padding:0px !important; font-size:12px; font-weight:normal; min-width:120px;">
									                            <div class="btn-group" style="text-align:left;">
										                            <button class="dropdown-toggle" style="width:100% !important; background-color:#e3ebf0; font-size:12px; font-weight:normal;  border:0px;" data-toggle="dropdown">นายดนัย	สุทธิพงศ์<i class="icon-angle-down"></i></button>
										                            <ul class="dropdown-menu pull-right">
										                              <li><a href="javascript:;showDialog('นายดนัย	สุทธิพงศ์');"><i class="icon-edit"></i> เปลี่ยนชื่อ</a></li>
										                              <li><a href="javascript:;"><i class="icon-trash"></i> ลบ</a></li>										                  
									                                </ul>
									                              </div>
									                            </th>
									                            <th style="text-align:center; background-color:#e3ebf0; padding:0px !important; font-size:12px; font-weight:normal; min-width:120px;">
									                             <div class="btn-group" style="text-align:left;">
										                            <button class="dropdown-toggle" style="width:100% !important; background-color:#e3ebf0; font-size:12px; font-weight:normal;  border:0px;" data-toggle="dropdown">เจ้าหน้าที่ใหม่<i class="icon-angle-down"></i></button>
										                            <ul class="dropdown-menu pull-right">
										                              <li><a href="javascript:;showDialog('เจ้าหน้าที่ใหม่');"><i class="icon-edit"></i> เปลี่ยนชื่อ</a></li>
										                              <li><a href="javascript:;"><i class="icon-trash"></i> ลบ</a></li>										                  
									                                </ul>
									                              </div>									                
									                            </th>
									                            <th style="text-align:center; background-color:#e3ebf0; padding:0px !important; font-size:12px; font-weight:normal; min-width:120px;">
									                              <div class="btn-group" style="text-align:left;">
										                            <button class="dropdown-toggle" style="width:100% !important; background-color:#e3ebf0; font-size:12px; font-weight:normal;  border:0px;" data-toggle="dropdown">นางมลวิภา ดุลยาภินันท์<i class="icon-angle-down"></i></button>
										                            <ul class="dropdown-menu pull-right">
										                              <li><a href="javascript:;showDialog('นางมลวิภา ดุลยาภินันท์');"><i class="icon-edit"></i> เปลี่ยนชื่อ</a></li>
										                              <li><a href="javascript:;"><i class="icon-trash"></i> ลบ</a></li>										                  
									                                </ul>
									                              </div>
									                            </th>
									                            <th style="text-align:center; background-color:#e3ebf0; padding:0px !important; font-size:12px; font-weight:normal; min-width:120px;">
									                              <div class="btn-group" style="text-align:left;">
										                            <button class="dropdown-toggle" style="width:100% !important; background-color:#e3ebf0; font-size:12px; font-weight:normal;  border:0px;" data-toggle="dropdown">นายบุญสืบ เฟื่องฟู<i class="icon-angle-down"></i></button>
										                            <ul class="dropdown-menu pull-right">
										                              <li><a href="javascript:;showDialog('นายบุญสืบ เฟื่องฟู');"><i class="icon-edit"></i> เปลี่ยนชื่อ</a></li>
										                              <li><a href="javascript:;"><i class="icon-trash"></i> ลบ</a></li>										                  
									                                </ul>
									                              </div>
									                            </th>
									                            <th style="text-align:center; background-color:#e3ebf0; padding:0px !important; font-size:12px; font-weight:normal; min-width:120px;">
									                                <div class="btn-group" style="text-align:left;">
										                            <button class="dropdown-toggle" style="width:100% !important; background-color:#e3ebf0; font-size:12px; font-weight:normal;  border:0px;" data-toggle="dropdown">นายไมตรี	พุกะทรัพย์<i class="icon-angle-down"></i></button>
										                            <ul class="dropdown-menu pull-right">
										                              <li><a href="javascript:;showDialog('นายไมตรี  พุกะทรัพย์');"><i class="icon-edit"></i> เปลี่ยนชื่อ</a></li>
										                              <li><a href="javascript:;"><i class="icon-trash"></i> ลบ</a></li>										                  
									                                </ul>
									                              </div>
									                            </th>
									                            <th style="text-align:center; background-color:#e3ebf0; padding:0px !important; font-size:12px; font-weight:normal; min-width:120px;">
									                             <div class="btn-group" style="text-align:left;">
										                            <button class="dropdown-toggle" style="width:100% !important; background-color:#e3ebf0; font-size:12px; font-weight:normal;  border:0px;" data-toggle="dropdown">นายนิพนธ์	 จิรรัตนพันธ์<i class="icon-angle-down"></i></button>
										                            <ul class="dropdown-menu pull-right">
										                              <li><a href="javascript:;showDialog('นายนิพนธ์  จิรรัตนพันธ์');"><i class="icon-edit"></i> เปลี่ยนชื่อ</a></li>
										                              <li><a href="javascript:;"><i class="icon-trash"></i> ลบ</a></li>										                  
									                                </ul>
									                              </div>
									                            </th>
									                            <th style="text-align:center; background-color:#e3ebf0; padding:0px !important; font-size:12px; font-weight:normal; min-width:120px;">
									                             <div class="btn-group" style="text-align:left;">
										                            <button class="dropdown-toggle" style="width:100% !important; background-color:#e3ebf0; font-size:12px; font-weight:normal;  border:0px;" data-toggle="dropdown">นายชุมพล เตมียเวส<i class="icon-angle-down"></i></button>
										                            <ul class="dropdown-menu pull-right">
										                              <li><a href="javascript:;showDialog('นายชุมพล เตมียเวส');"><i class="icon-edit"></i> เปลี่ยนชื่อ</a></li>
										                              <li><a href="javascript:;"><i class="icon-trash"></i> ลบ</a></li>										                  
									                                </ul>
									                              </div>
									                            </th>
									                            <th style="text-align:center; background-color:#e3ebf0; padding:0px !important; font-size:12px; font-weight:normal; min-width:120px;">
									                             <div class="btn-group" style="text-align:left;">
										                            <button class="dropdown-toggle" style="width:100% !important; background-color:#e3ebf0; font-size:12px; font-weight:normal;  border:0px;" data-toggle="dropdown">นายจำเนียร แจ่มจำรัส<i class="icon-angle-down"></i></button>
										                            <ul class="dropdown-menu pull-right">
										                              <li><a href="javascript:;showDialog('นายจำเนียร แจ่มจำรัส');"><i class="icon-edit"></i> เปลี่ยนชื่อ</a></li>
										                              <li><a href="javascript:;"><i class="icon-trash"></i> ลบ</a></li>										                  
									                                </ul>
									                              </div>
									                            </th>
									                            <th style="text-align:center; background-color:#e3ebf0; padding:0px !important; font-size:12px; font-weight:normal; min-width:120px;">
									                             <div class="btn-group" style="text-align:left;">
										                            <button class="dropdown-toggle" style="width:100% !important; background-color:#e3ebf0; font-size:12px; font-weight:normal;  border:0px;" data-toggle="dropdown">เจ้าหน้าที่ใหม่<i class="icon-angle-down"></i></button>
										                            <ul class="dropdown-menu pull-right">
										                              <li><a href="javascript:;showDialog('เจ้าหน้าที่ใหม่');"><i class="icon-edit"></i> เปลี่ยนชื่อ</a></li>
										                              <li><a href="javascript:;"><i class="icon-trash"></i> ลบ</a></li>										                  
									                                </ul>
									                              </div>	
									                            </th>
									                        </tr>                                         
									                    </thead>
									                    <tbody>
									                        <asp:Repeater ID="rpt" OnItemDataBound="rpt_ItemDataBound" runat="server">
									                            <ItemTemplate>
									                                <tr>
									                                  <td style="width:40px; text-align:center; padding-left:0px; padding-right:0px; "><asp:Label ID="lblNo" runat="server" Font-Bold="true" ></asp:Label></td>
									                                  <td style="font-weight:bold;"><asp:Label ID="lblTask" runat="server" Width="150px" ></asp:Label></td>
									                                  <td class="WorkLoadTableCell" style="padding:0px !important; "><asp:TextBox id="txtNumPerson" runat="server" CssClass="WorkLoadTableTextbox" MaxLength="6"></asp:TextBox></td>
                                                                      <td class="WorkLoadTableCell" style="padding:0px !important; "><asp:TextBox id="txtMinPerTime" runat="server" CssClass="WorkLoadTableTextbox" MaxLength="6"></asp:TextBox></td>
                                                                      <td class="WorkLoadTableCell" style="background-color:#F8F8F8; padding:0px !important; " id="cellSumMin" runat="server"><asp:TextBox id="txtPersonMin" ReadOnly="True" style="background-color:#F8F8F8 !important;" Font-Bold="true" runat="server" CssClass="WorkLoadTableTextbox"></asp:TextBox></td>
                                                                      <td class="WorkLoadTableCell" style="padding:0px !important; "><asp:TextBox id="txtTimePerYear" runat="server" CssClass="WorkLoadTableTextbox" MaxLength="6"></asp:TextBox></td>
                                                                      <td class="WorkLoadTableCell" style="background-color:#F8F8F8; padding:0px !important;" id="cellTotal" runat="server"><asp:TextBox id="txtTotalMin" ReadOnly="True" style="background-color:#F8F8F8 !important;" Font-Bold="true"  runat="server" CssClass="WorkLoadTableTextbox"></asp:TextBox></td>
                                                                      <td class="WorkLoadTableCell" style="padding:0px !important; " id="cell13" runat="server"><asp:TextBox id="txtLevel13" runat="server" CssClass="WorkLoadTableTextbox" MaxLength="6"></asp:TextBox></td>
                                                                      <td class="WorkLoadTableCell" style="padding:0px !important; " id="cell12" runat="server"><asp:TextBox id="txtLevel12" runat="server" CssClass="WorkLoadTableTextbox" MaxLength="6"></asp:TextBox></td>
                                                                      <td class="WorkLoadTableCell" style="padding:0px !important; " id="cell11" runat="server"><asp:TextBox id="txtLevel11" runat="server" CssClass="WorkLoadTableTextbox" MaxLength="6"></asp:TextBox></td>
                                                                      <td class="WorkLoadTableCell" style="padding:0px !important; " id="cell10" runat="server"><asp:TextBox id="txtLevel10" runat="server" CssClass="WorkLoadTableTextbox" MaxLength="6"></asp:TextBox></td>
                                                                      <td class="WorkLoadTableCell" style="padding:0px !important; " id="cell9" runat="server"><asp:TextBox id="txtLevel9" runat="server" CssClass="WorkLoadTableTextbox" MaxLength="6"></asp:TextBox></td>
                                                                      <td class="WorkLoadTableCell" style="padding:0px !important; " id="cell8" runat="server"><asp:TextBox id="txtLevel8" runat="server" CssClass="WorkLoadTableTextbox" MaxLength="6"></asp:TextBox></td>
                                                                      <td class="WorkLoadTableCell" style="padding:0px !important; " id="cell7" runat="server"><asp:TextBox id="txtLevel7" runat="server" CssClass="WorkLoadTableTextbox" MaxLength="6"></asp:TextBox></td>
                                                                      <td class="WorkLoadTableCell" style="padding:0px !important; " id="cell6" runat="server"><asp:TextBox id="txtLevel6" runat="server" CssClass="WorkLoadTableTextbox" MaxLength="6"></asp:TextBox></td>
                                                                      <td class="WorkLoadTableCell" style="padding:0px !important; " id="cell5" runat="server"><asp:TextBox id="txtLevel5" runat="server" CssClass="WorkLoadTableTextbox" MaxLength="6"></asp:TextBox></td>
                                                                      <td class="WorkLoadTableCell" style="padding:0px !important; " id="cell4" runat="server"><asp:TextBox id="txtLevel4" runat="server" CssClass="WorkLoadTableTextbox" MaxLength="6"></asp:TextBox></td>
                                                                      <td class="WorkLoadTableCell" style="padding:0px !important; " id="cell3" runat="server"><asp:TextBox id="txtLevel3" runat="server" CssClass="WorkLoadTableTextbox" MaxLength="6"></asp:TextBox></td>
                                                                      <td class="WorkLoadTableCell" style="padding:0px !important; " id="cell2" runat="server"><asp:TextBox id="txtLevel2" runat="server" CssClass="WorkLoadTableTextbox" MaxLength="6"></asp:TextBox></td>
                                                                      <td class="WorkLoadTableCell" style="padding:0px !important; " id="cell1" runat="server"><asp:TextBox id="txtLevel1" runat="server" CssClass="WorkLoadTableTextbox" MaxLength="6"></asp:TextBox></td>
									                                </tr>
									                            </ItemTemplate>
									                            <FooterTemplate>
									                             <tr>
									                                  <td>&nbsp;</td>
									                                  <td style="text-align:right; font-weight:bold; background-color:#F8F8F8 !important;" colspan="5">จำนวนนาทีที่ใช้ในการปฏิบัติ</td>
									                                  <td class="WorkLoadSummaryCell"><asp:Label ID="lblTotalMinPerYear" runat="server"></asp:Label></td>
									                                  <td class="WorkLoadSummaryCell"><asp:Label ID="lblTotalMinLevel13" runat="server"></asp:Label></td>
									                                  <td class="WorkLoadSummaryCell"><asp:Label ID="lblTotalMinLevel12" runat="server"></asp:Label></td>
									                                  <td class="WorkLoadSummaryCell"><asp:Label ID="lblTotalMinLevel11" runat="server"></asp:Label></td>
									                                  <td class="WorkLoadSummaryCell"><asp:Label ID="lblTotalMinLevel10" runat="server"></asp:Label></td>
									                                  <td class="WorkLoadSummaryCell"><asp:Label ID="lblTotalMinLevel9" runat="server"></asp:Label></td>
									                                  <td class="WorkLoadSummaryCell"><asp:Label ID="lblTotalMinLevel8" runat="server"></asp:Label></td>
									                                  <td class="WorkLoadSummaryCell"><asp:Label ID="lblTotalMinLevel7" runat="server"></asp:Label></td>
									                                  <td class="WorkLoadSummaryCell"><asp:Label ID="lblTotalMinLevel6" runat="server"></asp:Label></td>
									                                  <td class="WorkLoadSummaryCell"><asp:Label ID="lblTotalMinLevel5" runat="server"></asp:Label></td>
									                                  <td class="WorkLoadSummaryCell"><asp:Label ID="lblTotalMinLevel4" runat="server"></asp:Label></td>
									                                  <td class="WorkLoadSummaryCell"><asp:Label ID="lblTotalMinLevel3" runat="server"></asp:Label></td>
									                                  <td class="WorkLoadSummaryCell"><asp:Label ID="lblTotalMinLevel2" runat="server"></asp:Label></td>
									                                  <td class="WorkLoadSummaryCell"><asp:Label ID="lblTotalMinLevel1" runat="server"></asp:Label></td>
									                                </tr>
            									                    
									                                <tr>
									                                  <td>&nbsp;</td>
									                                  <td style="text-align:right; font-weight:bold; background-color:#F8F8F8 !important;" colspan="5">คิดเป็นจำนวนชั่วโมง</td>
									                                  <td class="WorkLoadSummaryCell"><asp:Label ID="lblTotalHourPerYear" runat="server"></asp:Label></td>
									                                  <td class="WorkLoadSummaryCell"><asp:Label ID="lblTotalHourLevel13" runat="server"></asp:Label></td>
									                                  <td class="WorkLoadSummaryCell"><asp:Label ID="lblTotalHourLevel12" runat="server"></asp:Label></td>
									                                  <td class="WorkLoadSummaryCell"><asp:Label ID="lblTotalHourLevel11" runat="server"></asp:Label></td>
									                                  <td class="WorkLoadSummaryCell"><asp:Label ID="lblTotalHourLevel10" runat="server"></asp:Label></td>
									                                  <td class="WorkLoadSummaryCell"><asp:Label ID="lblTotalHourLevel9" runat="server"></asp:Label></td>
									                                  <td class="WorkLoadSummaryCell"><asp:Label ID="lblTotalHourLevel8" runat="server"></asp:Label></td>
									                                  <td class="WorkLoadSummaryCell"><asp:Label ID="lblTotalHourLevel7" runat="server"></asp:Label></td>
									                                  <td class="WorkLoadSummaryCell"><asp:Label ID="lblTotalHourLevel6" runat="server"></asp:Label></td>
									                                  <td class="WorkLoadSummaryCell"><asp:Label ID="lblTotalHourLevel5" runat="server"></asp:Label></td>
									                                  <td class="WorkLoadSummaryCell"><asp:Label ID="lblTotalHourLevel4" runat="server"></asp:Label></td>
									                                  <td class="WorkLoadSummaryCell"><asp:Label ID="lblTotalHourLevel3" runat="server"></asp:Label></td>
									                                  <td class="WorkLoadSummaryCell"><asp:Label ID="lblTotalHourLevel2" runat="server"></asp:Label></td>
									                                  <td class="WorkLoadSummaryCell"><asp:Label ID="lblTotalHourLevel1" runat="server"></asp:Label></td>
									                                </tr>
            									                    
									                                <tr>
									                                  <td>&nbsp;</td>
									                                  <td style="text-align:right; font-weight:bold; background-color:#F8F8F8 !important;" colspan="5">FTE</td>
									                                  <td class="WorkLoadSummaryCell"><asp:Label ID="lblFTEYear" runat="server"></asp:Label></td>
									                                  <td class="WorkLoadSummaryCell"><asp:Label ID="lblFTELevel13" runat="server"></asp:Label></td>
									                                  <td class="WorkLoadSummaryCell"><asp:Label ID="lblFTELevel12" runat="server"></asp:Label></td>
									                                  <td class="WorkLoadSummaryCell"><asp:Label ID="lblFTELevel11" runat="server"></asp:Label></td>
									                                  <td class="WorkLoadSummaryCell"><asp:Label ID="lblFTELevel10" runat="server"></asp:Label></td>
									                                  <td class="WorkLoadSummaryCell"><asp:Label ID="lblFTELevel9" runat="server"></asp:Label></td>
									                                  <td class="WorkLoadSummaryCell"><asp:Label ID="lblFTELevel8" runat="server"></asp:Label></td>
									                                  <td class="WorkLoadSummaryCell"><asp:Label ID="lblFTELevel7" runat="server"></asp:Label></td>
									                                  <td class="WorkLoadSummaryCell"><asp:Label ID="lblFTELevel6" runat="server"></asp:Label></td>
									                                  <td class="WorkLoadSummaryCell"><asp:Label ID="lblFTELevel5" runat="server"></asp:Label></td>
									                                  <td class="WorkLoadSummaryCell"><asp:Label ID="lblFTELevel4" runat="server"></asp:Label></td>
									                                  <td class="WorkLoadSummaryCell"><asp:Label ID="lblFTELevel3" runat="server"></asp:Label></td>
									                                  <td class="WorkLoadSummaryCell"><asp:Label ID="lblFTELevel2" runat="server"></asp:Label></td>
									                                  <td class="WorkLoadSummaryCell"><asp:Label ID="lblFTELevel1" runat="server"></asp:Label></td>
									                                </tr>
									                            </FooterTemplate>
									                        </asp:Repeater>
            									            
            									                   
            									                    
									                        <asp:Button ID="btnCalculate" OnClick="btnCalculate_Click" runat="server" style="display:none" />									   	       							
									                    </tbody>
								                    </table>
            								        
						                            <div class="form-actions">
            										    
										                <asp:Button CssClass="btn red" runat="server" ID="btnSave" Text="ลบข้อมูลเวลาการปฏิบัติงานทั้งหมด" />
            										    <asp:Button CssClass="btn" runat="server" ID="btnBack" OnClick="HideDetail_Click" Text="ย้อนกลับ" />
										            </div>
            				               
							            </div>
            						
				                <!-- END FORM-->  
				            </div>
            
                             <div class="modal" id="divTest" runat="server" style="width:350px; top:20%; visibility:hidden;" >
			                    <div class="modal-header">										
				                    <h3>เปลี่ยนชื่อ</h3>
			                    </div>
			                    <div class="modal-body" style="padding:0px; overflow:hidden;">
                    						        
						                           <input type="text" id="contentDialog" class="large m-wrap" style="width:100% !important; margin:0px;" />
                    				
			                    </div>
			                    <div class="modal-footer">
			                        <input type="button" Class="btn blue" value="บันทึก" onclick="document.getElementById('ctl00_ContentPlaceHolder1_divTest').style.visibility='hidden';" />
			                        <input type="button" Class="btn" value="ยกเลิก" onclick="document.getElementById('ctl00_ContentPlaceHolder1_divTest').style.visibility='hidden';" />
			                    </div>
		                    </div>
                              
                </asp:Panel>
          
         
</div>

    
										
</ContentTemplate>           
</asp:UpdatePanel>

<!-------------For Demo--------------->
<script language="javascript">
    function showDialog(thename) {
        document.getElementById('contentDialog').value = thename;
        document.getElementById('ctl00_ContentPlaceHolder1_divTest').style.visibility = 'visible';

    }
</script>
<!-------------For Demo--------------->
										
										
</asp:Content>



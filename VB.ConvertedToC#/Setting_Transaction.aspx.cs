using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Xml.Linq;
using System.Diagnostics;
using System.Collections.Specialized;
using System.Configuration;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Caching;
using System.Web.SessionState;
using System.Web.Security;
using System.Web.Profile;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;

namespace VB
{

	public partial class Setting_Transaction : System.Web.UI.Page
	{


		HRBL BL = new HRBL();
		GenericLib GL = new GenericLib();

		Converter C = new Converter();
		protected void Page_Load(object sender, System.EventArgs e)
		{
			if ((Session["USER_PSNL_NO"] == null)) {
				ScriptManager.RegisterStartupScript(this.Page, typeof(string), "Alert", "showAlert('กรุณาลงชื่อเข้าใช้ระบบ');", true);
				Response.Redirect("Login.aspx");
				return;
			}


			if (!IsPostBack) {
				//---------- Set Date Scope--------
                TimeSpan lastM = new TimeSpan(30, 0, 0, 0);
                DateTime theTime = DateTime.Now - lastM;
                txtStart.Text = C.DateToString(theTime, "dd-MMM-yyyy");
				txtEnd.Text = C.DateToString(DateTime.Now, "dd-MMM-yyyy");
				BindTransaction();
			}
		}

		protected void Search_Changed(object sender, System.EventArgs e)
		{
			BindTransaction();
		}


		private void BindTransaction()
		{
			string SQL = "EXEC dbo.sp_Transaction_Info ";
			if (!string.IsNullOrEmpty(txtFilter.Text)) {
				SQL += "'" + txtFilter.Text.Replace("'", "''") + "',";
			} else {
				SQL += " NULL,";
			}
			if (GL.IsProgrammingDate(txtStart.Text, "dd-MMM-yyyy")) {
				string TheDate = C.DateToString(C.StringToDate(txtStart.Text, "dd-MMM-yyyy"), "yyyy-MM-dd");
				SQL += "'" + TheDate + "',";
			} else {
				SQL += " NULL,";
			}
			if (GL.IsProgrammingDate(txtEnd.Text, "dd-MMM-yyyy")) {
				string TheDate = C.DateToString(C.StringToDate(txtEnd.Text, "dd-MMM-yyyy"), "yyyy-MM-dd");
				SQL += "'" + TheDate + "'";
			} else {
				SQL += " NULL";
			}

			SqlDataAdapter DA = new SqlDataAdapter(SQL, BL.ConnectionString());
			DataTable DT = new DataTable();
			DA.Fill(DT);

			Session["Setting_Transaction"] = DT;
			Pager.SesssionSourceName = "Setting_Transaction";
			Pager.RenderLayout();

			if (DT.Rows.Count == 0) {
				lblCountList.Text = "ไม่พบรายการดังกล่าว";
			} else {
				lblCountList.Text = "พบ " + GL.StringFormatNumber(DT.Rows.Count.ToString(), 0) + " รายการ";
			}

		}

		protected void Pager_PageChanging(PageNavigation Sender)
		{
			Pager.TheRepeater = rptList;
		}

		protected void rptList_ItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
		{
			if (e.Item.ItemType != ListItemType.Item & e.Item.ItemType != ListItemType.AlternatingItem)
				return;

            Label lblPSN = (Label)e.Item.FindControl("lblPSN");
            Label lblPos = (Label)e.Item.FindControl("lblPos");
            Label lblAction = (Label)e.Item.FindControl("lblAction");
            Label lblTime = (Label)e.Item.FindControl("lblTime");

            DataRowView drv = (DataRowView)e.Item.DataItem;

			lblPSN.Text = drv["PSNL_NO"].ToString() + " : " + drv["Fullname"].ToString();
			lblPos.Text = drv["POS_Name"].ToString();
			lblAction.Text = drv["Detail"].ToString();
			if (!GL.IsEqualNull(drv["Trans_Time"])) {
                //lblTime.Text = C.DateToString((DateTime)drv["Trans_Time"], "dd/MM/") + (((DateTime)drv["Trans_Time"]).Year+543) + " &nbsp; &nbsp; " + C.DateToString((DateTime)drv["Trans_Time"], "HH:mm");
                //lblTime.Text = BL.DateTimeToThaiDateTime((DateTime)drv["Trans_Time"]);

                //lblTime.Text = BL.DateTimeToThaiDate((DateTime)drv["Trans_Time"]) + " &nbsp; &nbsp; " + C.DateToString((DateTime)drv["Trans_Time"], "HH:mm");
                lblTime.Text = BL.DateTimeToThaiDateTime((DateTime)drv["Trans_Time"]);
            }
		}
		public Setting_Transaction()
		{
			Load += Page_Load;
		}
	}
}

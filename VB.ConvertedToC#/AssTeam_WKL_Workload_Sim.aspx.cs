using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Xml.Linq;
using System.Diagnostics;
using System.Collections.Specialized;
using System.Configuration;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Caching;
using System.Web.SessionState;
using System.Web.Security;
using System.Web.Profile;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
namespace VB
{

	public partial class AssTeam_WKL_Workload_Sim : System.Web.UI.Page
	{


		HRBL BL = new HRBL();
		GenericLib GL = new GenericLib();
		Converter C = new Converter();

		textControlLib TC = new textControlLib();
		public string PSNL_NO {
			get {
				try {
					return Session["USER_PSNL_NO"].ToString ();
				} catch (Exception ex) {
					return "";
				}
			}
		}

		public int SIM_ID {
			get { return Convert.ToInt32(Conversion.Val(divModal.Attributes["SIM_ID"])); }
			set { divModal.Attributes["SIM_ID"] = value.ToString(); }
		}

        public string USER_DEPT_CODE
        {
            get{
                if(Session["USER_DEPT_CODE"]==null) return"";
                else
                    return Session["USER_DEPT_CODE"].ToString();
            }       
        }


		public string NEW_DEPT_CODE {
			get {
				try {
					return GL.SplitString(ddlNewDept.Items[ddlNewDept.SelectedIndex].Value, "-")[0];
				} catch (Exception ex) {
					return "";
				}
			}
		}

		public string NEW_MINOR_CODE {
			get {
				try {
					return GL.SplitString(ddlNewDept.Items[ddlNewDept.SelectedIndex].Value, "-")[1];
				} catch (Exception ex) {
					return "00";
				}
			}
		}


		public string DEPT_CODE {
			get { return pnlSim.Attributes["DEPT_CODE"]; }
			set { pnlSim.Attributes["DEPT_CODE"] = value; }
		}

		public string MINOR_CODE {
			get { return pnlSim.Attributes["MINOR_CODE"]; }
			set { pnlSim.Attributes["MINOR_CODE"] = value; }
		}

		public string SIM_Name {
			get { return txtNewName.Text; }
			set {
				txtNewName.Text = value;
				lblSimName.Text = value;
			}
		}


		protected void Page_Load(object sender, System.EventArgs e)
		{
			if ((Session["USER_PSNL_NO"] == null)) {
				ScriptManager.RegisterStartupScript(this.Page, typeof(string), "Alert", "showAlert('กรุณาลงชื่อเข้าใช้ระบบ');", true);
				Response.Redirect("Login.aspx");
				return;
			}

            if (!Convert.ToBoolean(Session["USER_Is_Workload_Admin"]) & !Convert.ToBoolean(Session["USER_Is_Workload_User"]) & !Convert.ToBoolean(Session["USER_Is_Manager"]))
            {
                ScriptManager.RegisterStartupScript(this.Page, typeof(string), "Alert", "showAlert('คุณไม่มีสิทธิ์เข้าถึงข้อมูลส่วนนี้');", true);
                Response.Redirect("Overview.aspx");
                return;
            }

			if (!IsPostBack) {

                BindSIMList();
				pnlList.Visible = true;
				pnlSim.Visible = false;
				pnlDialogNew.Visible = false;
				pnlDialogDEPT.Style["visibility"] = "hidden";

				btnOKDialogNew.Attributes["onMouseOut"] = "document.getElementById('" + lblErrorDialogNew.ClientID + "').innerHTML='';";
			}

			if (IsPostBack) {
				ScriptManager.RegisterStartupScript(this.Page, typeof(string), "Alert", "activeLastFocusControl();", true);
			}

			lblErrorDialogNew.Text = "";
		}

		private void BindSIMList()
		{
			string SQL = "SELECT SIM.SIM_ID,SIM.SIM_NAME,SIM.Update_Time,DEPT,ISNULL(COUNT(PSN.SIM_ID),0) PSN\n";
			SQL += " FROM \n";
			SQL += " (SELECT tb_SIM.SIM_ID,tb_SIM.SIM_NAME,tb_SIM.Update_Time,ISNULL(COUNT(DEPT.SIM_ID),0) DEPT\n";
			SQL += " FROM tb_SIM LEFT JOIN tb_SIM_DEPT DEPT ON tb_SIM.SIM_ID=DEPT.SIM_ID\n";
			SQL += " WHERE PSNL_NO='" + PSNL_NO + "'\n";
			SQL += " GROUP BY tb_SIM.SIM_ID,tb_SIM.SIM_NAME,tb_SIM.Update_Time\n";
			SQL += " ) SIM LEFT JOIN tb_SIM_Workload_PSN PSN ON SIM.SIM_ID=PSN.SIM_ID\n";
			SQL += " GROUP BY SIM.SIM_ID,SIM.SIM_NAME,SIM.Update_Time,DEPT\n";
			SQL += " ORDER BY SIM.SIM_ID\n";

			DataTable DT = new DataTable();
			SqlDataAdapter DA = new SqlDataAdapter(SQL, BL.ConnectionString());
			DA.Fill(DT);

			rptList.DataSource = DT;
			rptList.DataBind();

		}

		protected void rptList_ItemCommand(object source, System.Web.UI.WebControls.RepeaterCommandEventArgs e)
		{
			switch (e.CommandName) {
				case "Delete":
					BL.DeleteWorkloadSIM(GL.CINT(e.CommandArgument));
					BindSIMList();

					break;
				case "Select":

					string SQL = "";
					SQL += " SELECT TOP 1 SIM_NAME,DEPT.DEPT_CODE,DEPT.MINOR_CODE\n";
					SQL += " FROM tb_SIM SIM LEFT JOIN tb_SIM_DEPT DEPT ON SIM.SIM_ID=DEPT.SIM_ID\n";
					SQL += " WHERE SIM.SIM_ID=" + e.CommandArgument + "\n";
					SQL += " ORDER BY DEPT_No\n";

					DataTable DT = new DataTable();
					SqlDataAdapter DA = new SqlDataAdapter(SQL, BL.ConnectionString());
					DA.Fill(DT);
					if (DT.Rows.Count == 0) {
						ScriptManager.RegisterStartupScript(this.Page, typeof(string), "Alert", "showAlert('ไม่พบรายการที่เลือก กรุณาตรวจสอบอีกครั้ง');", true);
						BindSIMList();
						break; // TODO: might not be correct. Was : Exit Select
					}

					SIM_ID =GL.CINT(e.CommandArgument);
                    SIM_Name = DT.Rows[0]["SIM_Name"].ToString();
					//------------- Update Label---------
					DEPT_CODE = DT.Rows[0]["DEPT_CODE"].ToString ();
					//--------- Set Selection Dept --------
                    MINOR_CODE = DT.Rows[0]["MINOR_CODE"].ToString();
					//--------- Set Selection Dept --------

					BindTab();
					BindWholeTable();

					pnlList.Visible = false;
					pnlSim.Visible = true;
					pnlDialogNew.Visible = false;

					//-------- Bind Dialog First---------
					BL.BindDDlSector(ddlSector, DEPT_CODE.Substring(0, 2));
					txtSearchDept.Text = "";
					break;
			}
		}

		protected void rptList_ItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
		{
			if (e.Item.ItemType != ListItemType.Item & e.Item.ItemType != ListItemType.AlternatingItem)
				return;

			Label lblName =(Label) e.Item.FindControl("lblName");
			Label lblDept =(Label) e.Item.FindControl("lblDept");
			Label lblPSN = (Label) e.Item.FindControl("lblPSN");
            Label lblUpdate = (Label)e.Item.FindControl("lblUpdate");
            LinkButton lnkDelete = (LinkButton)e.Item.FindControl("lnkDelete");
            LinkButton lnkSelect = (LinkButton)e.Item.FindControl("lnkSelect");
            AjaxControlToolkit.ConfirmButtonExtender cfmbtnDelete = (AjaxControlToolkit.ConfirmButtonExtender)e.Item.FindControl("cfmbtnDelete");

            DataRowView drv = (DataRowView)e.Item.DataItem;
            lblName.Text = drv["SIM_NAME"].ToString();
			lblDept.Text = GL.StringFormatNumber(drv["DEPT"], 0);
			lblPSN.Text = GL.StringFormatNumber(drv["PSN"], 0);
            DateTime U = (DateTime)drv["Update_Time"];
			lblUpdate.Text = U.Day + " " + C.ToMonthShortTH(U.Month) + " " + (U.Year + 543);
            lnkDelete.CommandArgument = drv["SIM_ID"].ToString();
            lnkSelect.CommandArgument = drv["SIM_ID"].ToString();

			cfmbtnDelete.ConfirmText = "ยืนยันลบ '" + drv["SIM_NAME"] + "'";
		}


		#region "DialogNew"

		protected void btnNew_Click(object sender, System.EventArgs e)
		{
			SIM_ID = 0;
			SIM_Name = "แบบจำลองสร้างวันที่ " + DateAndTime.Now.Day + " " + C.ToMonthNameTH(DateAndTime.Now.Month) + " " + (DateAndTime.Now.Year + 543);

			//------------- หาหน่วยงานเริ่มต้น ---------------
            //DataTable DT = BL.GetAllAssessmentRound();
            //DT.DefaultView.Sort = " R_Year DESC , R_Round DESC";
            //if (DT.Rows.Count > 0) {
            //    BL.BindDDlWorkloadDepartment(ddlNewDept, "", (string)Session["USER_DEPT_CODE"]);
            //    //BL.BindDDlTeamDepartment(ddlNewDept, GL.CINT(DT.DefaultView[0]["R_Year"]), GL.CINT(DT.DefaultView[0]["R_Round"]), PSNL_NO.ToString());
            //} else {
            //    BL.BindDDlTeamDepartment(ddlNewDept, 0, 0, PSNL_NO);

            //}

            BL.BindDDlWorkloadDepartment(ddlNewDept, "", USER_DEPT_CODE);
            if (Convert.ToBoolean(Session["USER_Is_Workload_Admin"]))
            {

            }
            else if (Convert.ToBoolean(Session["USER_Is_Workload_User"]) & Convert.ToBoolean(Session["USER_Is_Manager"]))
            {
                ddlNewDept.Enabled = false;
            }



			if (ddlNewDept.Items.Count == 1) {
				ScriptManager.RegisterStartupScript(this.Page, typeof(string), "Alert", "showAlert('คุณไม่มีสิทธิ์เข้าถึงข้อมูลส่วนนี้');", true);
				return;
			} else {
				ddlNewDept.Items.RemoveAt(0);
				ddlNewDept.SelectedIndex = 0;
			}

			pnlDialogNew.Visible = true;
		}

		protected void btnCloseDialogNew_Click(object sender, System.EventArgs e)
		{
			pnlDialogNew.Visible = false;
		}

		protected void btnOKDialogNew_Click(object sender, System.EventArgs e)
		{
			if (string.IsNullOrEmpty(txtNewName.Text)) {
				ScriptManager.RegisterStartupScript(this.Page, typeof(string), "Alert", "showAlert('กรอกชื่อแบบจำลอง');", true);
				return;
			}

			if (ddlNewDept.SelectedIndex < 0) {
				ScriptManager.RegisterStartupScript(this.Page, typeof(string), "Alert", "showAlert('เลือกหน่วยงานเริ่มต้น');", true);
				return;
			}

			//------------ Check SIM NAME is duplicated ?-----------
			string SQL = "SELECT SIM_NAME FROM tb_SIM \n";
			SQL += " WHERE SIM_NAME='" + txtNewName.Text.Replace("'", "''") + "' AND PSNL_NO='" + PSNL_NO.Replace("'", "''") + "'\n";
			DataTable DT = new DataTable();
			SqlDataAdapter DA = new SqlDataAdapter(SQL, BL.ConnectionString());
			DA.Fill(DT);
			if (DT.Rows.Count > 0) {
				lblErrorDialogNew.Text = "ชื่อนี้มีอยู่แล้ว";
				return;
			}

			//------------ Save tb_SIM------------

			SQL = "SELECT * FROM tb_SIM WHERE 1=0";
			DT = new DataTable();
			DA = new SqlDataAdapter(SQL, BL.ConnectionString());
			DA.Fill(DT);
			DataRow DR = DT.NewRow();
			DR["SIM_ID"] = BL.GetNewWorkLoadSimID();
			DR["PSNL_NO"] = PSNL_NO;
			DR["SIM_NAME"] = SIM_Name;
			DR["Update_By"] = PSNL_NO;
			DR["Update_Time"] = DateAndTime.Now;
			DT.Rows.Add(DR);
			SqlCommandBuilder cmd = new SqlCommandBuilder(DA);
			DA.Update(DT);
			//------------ Save tb_SIM_DEPT-------------------------
			//------------ Save tb_SIM_Job_Mapping_Class -----------
			//------------ Save tb_SIM_Job_Mapping -----------------
			//------------ Save tb_SIM_Job_Mapping_Detail ----------
			//------------ Save tb_SIM_Workload_PSN ----------------
			//------------ Save tb_SIM_Workload_Slot ---------------
            BL.CloneWorkloadJobForSimulate(GL.CINT(DR["SIM_ID"]), NEW_DEPT_CODE, NEW_MINOR_CODE, PSNL_NO);

			SIM_ID = GL.CINT(DR["SIM_ID"]);
			SIM_Name = SIM_Name;
			//------------- Update Label---------
			DEPT_CODE = NEW_DEPT_CODE;
			//--------- Set Selection Dept --------
			MINOR_CODE = NEW_MINOR_CODE;
			//--------- Set Selection Dept --------

			BindTab();
			BindWholeTable();

			pnlList.Visible = false;
			pnlSim.Visible = true;
			pnlDialogNew.Visible = false;
		}

		#endregion

		#region "WorkloadTable AND Tab"


        protected void btnExcel_click(object sender, System.EventArgs e)
        {
            //
            ScriptManager.RegisterStartupScript(this.Page, typeof(string), "Excel", "window.location.href='print/RPT_WS_Excel.aspx?S=" + SIM_ID + "&D=" + DEPT_CODE + "&M=" + MINOR_CODE + "';", true);
        }

		private DataTable GetDeptData()
		{
			string SQL = "SELECT SIM_ID,DEPT_CODE,MINOR_CODE,DEPT_NAME,SECTOR_NAME \n";
			SQL += " FROM tb_SIM_DEPT \n";
			SQL += " WHERE SIM_ID=" + SIM_ID + "\n";
			SQL += " ORDER BY DEPT_No\n";
			DataTable DT = new DataTable();
			SqlDataAdapter DA = new SqlDataAdapter(SQL, BL.ConnectionString());
			DA.Fill(DT);

			return DT;
		}

		private void BindTab()
		{
			//----------------- Bind Organize ------------
			rptTabDEPT.DataSource = GetDeptData().Copy();
			rptTabDEPT.DataBind();
		}

		protected void rptTabDEPT_ItemCommand(object source, System.Web.UI.WebControls.RepeaterCommandEventArgs e)
		{
            Button btnDEPT = (Button)e.Item.FindControl("btnDEPT");

			switch (e.CommandName) {
				case "Select":
                    
					if (btnDEPT.Attributes["DEPT_CODE"] == DEPT_CODE & btnDEPT.Attributes["MINOR_CODE"] == MINOR_CODE)
						return;

					//----- Select same Dept ----------

					DEPT_CODE = btnDEPT.Attributes["DEPT_CODE"];
					//--------- Set Selection Dept --------
					MINOR_CODE = btnDEPT.Attributes["MINOR_CODE"];
					//--------- Set Selection Dept --------

					BindTab();
					BindWholeTable();

					break;
				case "Delete":

                    

					string SQL = "";
					SQL += " DECLARE @SIM_ID As INT=" + SIM_ID + "\n";
					SQL += " DECLARE @DEPT_CODE As nvarchar(50)='" + btnDEPT.Attributes["DEPT_CODE"] + "'\n";
					SQL += " DECLARE @MINOR_CODE As nvarchar(50)='" + btnDEPT.Attributes["MINOR_CODE"] + "'\n";
					SQL += " \n";
					SQL += " DELETE FROM tb_SIM_Workload_Slot WHERE SIM_ID=@SIM_ID AND Job_ID IN \n";
					SQL += " \t(SELECT Job_ID FROM tb_SIM_Job_Mapping \n";
					SQL += " \tWHERE SIM_ID=@SIM_ID AND PNDP_DEPT_CODE=@DEPT_CODE \n";
					SQL += " \tAND PNDP_MINOR_CODE=@MINOR_CODE)\n";
					SQL += " \t\n";
					SQL += " DELETE FROM tb_SIM_Job_Mapping_Detail WHERE SIM_ID=@SIM_ID AND Job_ID IN \n";
					SQL += " \t(SELECT Job_ID FROM tb_SIM_Job_Mapping \n";
					SQL += " \tWHERE SIM_ID=@SIM_ID AND PNDP_DEPT_CODE=@DEPT_CODE \n";
					SQL += " \tAND PNDP_MINOR_CODE=@MINOR_CODE)\n";
					SQL += " \n";
					SQL += " DELETE FROM tb_SIM_Job_Mapping WHERE SIM_ID=@SIM_ID AND Job_ID IN \n";
					SQL += " \t(SELECT Job_ID FROM tb_SIM_Job_Mapping \n";
					SQL += " \tWHERE SIM_ID=@SIM_ID AND PNDP_DEPT_CODE=@DEPT_CODE \n";
					SQL += " \tAND PNDP_MINOR_CODE=@MINOR_CODE)\n";
					SQL += " \n";
					SQL += " DELETE FROM tb_SIM_Job_Mapping \n";
					SQL += " \tWHERE SIM_ID=@SIM_ID AND PNDP_DEPT_CODE=@DEPT_CODE \n";
					SQL += " \tAND PNDP_MINOR_CODE=@MINOR_CODE\n";
					SQL += " \t\n";
					SQL += " DELETE FROM tb_SIM_Workload_PSN\n";
					SQL += " \tWHERE SIM_ID=@SIM_ID AND DEPT_CODE=@DEPT_CODE AND MINOR_CODE=@MINOR_CODE\n";
					SQL += " \n";
					SQL += " DELETE FROM tb_SIM_Job_Mapping_Class\n";
					SQL += " \tWHERE SIM_ID=@SIM_ID AND DEPT_CODE=@DEPT_CODE AND MINOR_CODE=@MINOR_CODE\n";
					SQL += " \t\n";
					SQL += " DELETE FROM tb_SIM_DEPT\n";
					SQL += " \tWHERE SIM_ID=@SIM_ID AND DEPT_CODE=@DEPT_CODE AND MINOR_CODE=@MINOR_CODE\n";

					SqlConnection Conn = new SqlConnection(BL.ConnectionString());
					Conn.Open();
					SqlCommand COMM = new SqlCommand();
					var _with1 = COMM;
					_with1.CommandType = CommandType.Text;
					_with1.Connection = Conn;
					_with1.CommandText = SQL;
					_with1.ExecuteNonQuery();
					_with1.Dispose();
					Conn.Close();

					SQL = "SELECT TOP 1 DEPT_CODE,MINOR_CODE FROM tb_SIM_DEPT WHERE SIM_ID=" + SIM_ID;
					SqlDataAdapter DA = new SqlDataAdapter(SQL, BL.ConnectionString());
					DataTable DT = new DataTable();
					DA.Fill(DT);
					if (DT.Rows.Count > 0) {
						DEPT_CODE = DT.Rows[0]["DEPT_CODE"].ToString ();
                        MINOR_CODE = DT.Rows[0]["MINOR_CODE"].ToString();
					} else {
						DEPT_CODE = "";
						MINOR_CODE = "";
					}
					BindTab();
					BindWholeTable();
					break;
			}
		}

		protected void rptTabDEPT_ItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
		{
			if (e.Item.ItemType != ListItemType.Item & e.Item.ItemType != ListItemType.AlternatingItem)
				return;

            Button btnDEPT = (Button)e.Item.FindControl("btnDEPT");
            HtmlGenericControl liCaption = (HtmlGenericControl)e.Item.FindControl("liCaption");
            LinkButton lnkDelete = (LinkButton)e.Item.FindControl("lnkDelete");
            HtmlGenericControl badgeDelete = (HtmlGenericControl)e.Item.FindControl("badgeDelete");
            DataRowView drv = (DataRowView)e.Item.DataItem;

            btnDEPT.Text = drv["DEPT_NAME"].ToString();
            btnDEPT.Attributes["DEPT_CODE"] = drv["DEPT_CODE"].ToString();
            btnDEPT.Attributes["MINOR_CODE"] = drv["MINOR_CODE"].ToString();
			if (((DataTable)rptTabDEPT.DataSource).Rows.Count > 1) {
                badgeDelete.Attributes["onClick"] = "if(confirm('ยืนยันลบ " + drv["DEPT_NAME"].ToString().Replace("\n", "").Replace("'", "\'") + " ออกจากแบบจำลอง?')) document.getElementById('" + lnkDelete.ClientID + "').click();";
				//badgeDelete.Attributes["onClick"] = "document.getElementById('" + lnkDelete.ClientID + "').click();";
			} else {
				lnkDelete.Visible = false;
                badgeDelete.Visible = false;
			}
			//badgeDelete.Visible = false;


			if (Convert.ToString(drv["DEPT_CODE"]) == DEPT_CODE & Convert.ToString(drv["MINOR_CODE"]) == MINOR_CODE) {
				liCaption.Attributes["class"] = "active";
			} else {
				liCaption.Attributes["class"] = "";
				btnDEPT.Style["color"] = "#999999";
			}

		}

		#region "Code From Workload"
		private void BindWholeTable()
		{
			BindClass();
			BindPSN();
			BindHeader();
			//----------- Add Class Header + Personal Detail--------
			BindJob();
		}

			//--------- ข้อมูล แผนก -------------
		DataTable DeptClass;

		DataTable PSNData;
		private void BindClass()
		{
			string SQL = "SELECT Class_ID,PNPO_CLASS_Start,PNPO_CLASS_End\n";
			SQL += " FROM tb_SIM_Job_Mapping_Class\n";
			SQL += " WHERE SIM_ID=" + SIM_ID + " AND DEPT_CODE='" + DEPT_CODE.Replace("'", "''") + "' AND MINOR_CODE='" + MINOR_CODE.Replace("'", "''") + "' \n";
			SQL += " ORDER BY Class_ID DESC\n";
			SqlDataAdapter DA = new SqlDataAdapter(SQL, BL.ConnectionString());
			DeptClass = new DataTable();
			DA.Fill(DeptClass);

			//----------- ใส่ Default ให้ กรณีที่ ยังไม่ได้ตั้งค่าระดับ --------------
			if (DeptClass.Rows.Count == 0) {
				BL.SetSIMAutoClass(SIM_ID, DEPT_CODE, MINOR_CODE);
				DA = new SqlDataAdapter(SQL, BL.ConnectionString());
				DeptClass = new DataTable();
				DA.Fill(DeptClass);
			}
		}

		private void BindPSN()
		{
			string SQL = " SELECT \n";
			SQL += " CLS.Class_ID,PSN_ID,REF_PSNL_NO PSNL_NO,Slot_No,Slot_Name\n";
			SQL += " FROM tb_SIM_Job_Mapping_Class CLS\n";
			SQL += " LEFT JOIN tb_SIM_Workload_PSN PSN ON CLS.SIM_ID=PSN.SIM_ID ";
			SQL += " \t\t\t\t\t\t\tAND PSN.Class_ID=CLS.Class_ID\n";
			SQL += " \t\t\t\t\t\t\tAND PSN.DEPT_CODE=CLS.DEPT_CODE \n";
			SQL += " \t\t\t\t\t\t\tAND PSN.MINOR_CODE=CLS.MINOR_CODE\n";
			SQL += " WHERE CLS.SIM_ID=" + SIM_ID + " AND CLS.DEPT_CODE='" + DEPT_CODE.Replace("'", "''") + "' AND CLS.MINOR_CODE='" + MINOR_CODE.Replace("'", "''") + "'\n";
			SQL += " ORDER BY CLS.Class_ID DESC,Slot_No ASC\n";

			SqlDataAdapter DA = new SqlDataAdapter(SQL, BL.ConnectionString());
			PSNData = new DataTable();
			DA.Fill(PSNData);

			//----------- ใส่ Default ให้ กรณีที่ ยังไม่ได้ตั้งค่าบุคคล --------------
			if (PSNData.Rows.Count == 0) {
				BL.SetWorkloadSIMDefaultPSN(SIM_ID, DEPT_CODE, MINOR_CODE);
				DA = new SqlDataAdapter(SQL, BL.ConnectionString());
				PSNData = new DataTable();
				DA.Fill(PSNData);
			}
		}

		private void BindHeader()
		{
			//----------- Add Class Header--------
			rptClass.DataSource = DeptClass;
			rptClass.DataBind();
			//----------- Add Personal Detail--------
			rptPSN.DataSource = PSNData;
			rptPSN.DataBind();
		}

		protected void rptClass_ItemCommand(object source, System.Web.UI.WebControls.RepeaterCommandEventArgs e)
		{
			switch (e.CommandName) {
				case "Default":
                    HtmlAnchor lnkClassName = (HtmlAnchor)e.Item.FindControl("lnkClassName");
					BL.SetWorkloadSIMDefaultPSN(SIM_ID, DEPT_CODE.ToString (), MINOR_CODE, GL.CINT(lnkClassName.Attributes["Class_ID"]));
					BindWholeTable();
					break;
			}
		}


		protected void rptClass_ItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
		{
			if (e.Item.ItemType != ListItemType.Item & e.Item.ItemType != ListItemType.AlternatingItem)
				return;

            HtmlTableCell thClass = (HtmlTableCell)e.Item.FindControl("thClass");
            HtmlAnchor lnkClassName = (HtmlAnchor)e.Item.FindControl("lnkClassName");
            HtmlAnchor btnAddPSN = (HtmlAnchor)e.Item.FindControl("btnAddPSN");
            LinkButton btnDefault = (LinkButton)e.Item.FindControl("btnDefault");
            DataRowView drv = (DataRowView)e.Item.DataItem;
			int _start = GL.CINT(drv["PNPO_CLASS_Start"]);
            int _end = GL.CINT(drv["PNPO_CLASS_End"]);
			if (_start == _end) {
				lnkClassName.InnerHtml = "พนักงานระดับ " + _start + " <i class='icon-angle-down'></i>";
			} else {
				lnkClassName.InnerHtml = "พนักงานระดับ " + _start + "-" + _end + " <i class='icon-angle-down'></i>";
			}

			//---------------- Set Binding Data ----------
            lnkClassName.Attributes["Class_ID"] = drv["Class_ID"].ToString();
            lnkClassName.Attributes["PNPO_CLASS_Start"] = drv["PNPO_CLASS_Start"].ToString();
            lnkClassName.Attributes["PNPO_CLASS_End"] = drv["PNPO_CLASS_End"].ToString();

			//---------------- Set ColSpan ---------------
			PSNData.DefaultView.RowFilter = "Class_ID=" + drv["Class_ID"];
			if (PSNData.DefaultView.Count > 1) {
				thClass.ColSpan = PSNData.DefaultView.Count;
			}
			PSNData.DefaultView.RowFilter = "";
			//---------------- Set Button ----------------
			btnAddPSN.Attributes["onClick"] = "showDialog(" + drv["Class_ID"] + ",'พนักงานใหม่','เพิ่มผู้ปฏิบัติ','Add')";
		}

		protected void rptPSN_ItemCommand(object source, System.Web.UI.WebControls.RepeaterCommandEventArgs e)
		{
			switch (e.CommandName) {
				case "Delete":
					string SQL = "DELETE FROM tb_SIM_Workload_Slot WHERE SIM_ID=" + SIM_ID + " AND PSN_ID=" + e.CommandArgument + "\n";
					SQL += "DELETE FROM tb_SIM_Workload_PSN WHERE SIM_ID=" + SIM_ID + " AND PSN_ID=" + e.CommandArgument + "\n";

					SqlConnection Conn = new SqlConnection(BL.ConnectionString());
					Conn.Open();
					SqlCommand COMM = new SqlCommand();
					var _with2 = COMM;
					_with2.CommandType = CommandType.Text;
					_with2.Connection = Conn;
					_with2.CommandText = SQL;
					_with2.ExecuteNonQuery();
					_with2.Dispose();

					//-------- ReOrder Slot No---------
                    LinkButton btnDelete = (LinkButton)e.CommandSource;
					SQL = " SELECT * FROM tb_SIM_Workload_PSN \n";
					SQL += " WHERE DEPT_CODE='" + DEPT_CODE.Replace("'", "''") + "' AND MINOR_CODE='" + MINOR_CODE.Replace("'", "''") + "'\n";
					SQL += " AND SIM_ID=" + SIM_ID + " AND Class_ID=" + btnDelete.Attributes["Class_ID"] + "\n";
					SQL += " ORDER BY Slot_No DESC  ";
					DataTable DT = new DataTable();
					SqlDataAdapter DA = new SqlDataAdapter(SQL, BL.ConnectionString());
					DA.Fill(DT);
					for (int i = 0; i <= DT.Rows.Count - 1; i++) {
						DT.Rows[i]["Slot_No"] = i + 1;
					}

					try {
						SqlCommandBuilder cmd = new SqlCommandBuilder(DA);
						DA.Update(DT);
					} catch {
					}

					Conn.Close();
					Conn.Dispose();

					BindWholeTable();
					break;
			}
		}

		protected void rptPSN_ItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
		{
			if (e.Item.ItemType != ListItemType.Item & e.Item.ItemType != ListItemType.AlternatingItem)
				return;

            HtmlAnchor lnkPSNName = (HtmlAnchor)e.Item.FindControl("lnkPSNName");
            HtmlAnchor btnRename = (HtmlAnchor)e.Item.FindControl("btnRename");
            LinkButton btnDelete = (LinkButton)e.Item.FindControl("btnDelete");
            HtmlGenericControl btnGroup = (HtmlGenericControl)e.Item.FindControl("btnGroup");
			AjaxControlToolkit.ConfirmButtonExtender cfmBtnDelete = (AjaxControlToolkit.ConfirmButtonExtender)e.Item.FindControl("cfmBtnDelete");
            DataRowView drv = (DataRowView)e.Item.DataItem;

			if (!GL.IsEqualNull(drv["Slot_Name"]) || !GL.IsEqualNull(drv["PSN_ID"])) {
				lnkPSNName.InnerHtml = drv["Slot_Name"] + " <i class='icon-angle-down'></i>";
				lnkPSNName.Attributes["Slot_Name"] = drv["Slot_Name"].ToString();

				//---------------- Set Button ----------------
				btnRename.Attributes["onClick"] = "showDialog(" + drv["PSN_ID"] + ",'" + drv["Slot_Name"].ToString().Replace("'", "\\'").Replace("\n", "\\n") + "','เปลี่ยนชื่อ','Rename');";
				cfmBtnDelete.ConfirmText = "ยีนยันลบ '" + drv["Slot_Name"] + "' ?";
                btnDelete.CommandArgument = drv["PSN_ID"].ToString();
                btnDelete.Attributes["Class_ID"] = drv["Class_ID"].ToString();
			} else {
				lnkPSNName.InnerHtml = "";
				btnGroup.Visible = false;
			}
			//---------------- Set Binding Data ----------
            lnkPSNName.Attributes["Class_ID"] = drv["Class_ID"].ToString();

		}

			//---------- ข้อมูลแผนที่งาน -----------
		DataTable JobData;
			//----------- ข้อมูลเวลา -------------
		DataTable SlotData;
		DataTable MapClass;

		DataTable MoveToList;

		private void BindJob()
		{
			string SQL = null;
			SQL = "  SELECT Job_LEVEL,Job_No,\n";
			SQL += "   Header.Job_ID,Job_Parent,Header.Job_Name,NumPerson,MinPerTime,TimePerYear,\n";
			SQL += "   CAST(COUNT(Detail.Class_ID) AS BIT) Editable\n";
			SQL += "   FROM tb_SIM_Job_Mapping Header  \n";
			SQL += "   LEFT JOIN tb_SIM_Job_Mapping_Detail Detail ON Header.SIM_ID=Detail.SIM_ID AND Header.Job_ID=Detail.Job_ID\n";
			SQL += "   WHERE Header.SIM_ID=" + SIM_ID + " AND PNDP_DEPT_CODE='" + DEPT_CODE.Replace("'", "''") + "' AND PNDP_MINOR_CODE='" + MINOR_CODE.Replace("'", "''") + "' \n";
			SQL += "   GROUP BY PNDP_DEPT_CODE,PNDP_MINOR_CODE,Job_LEVEL,Job_No,Header.Job_ID,Job_Parent,Header.Job_Name,\n";
			SQL += "   NumPerson,MinPerTime,TimePerYear\n";
			SQL += "   ORDER BY Header.Job_LEVEL,Header.Job_No\n";

			SqlDataAdapter DA = new SqlDataAdapter(SQL, BL.ConnectionString());
			JobData = new DataTable();
			DA.Fill(JobData);

			//----------- Sort For Binding Structure----------
			DataTable DT = JobData.Copy();
			DT.Rows.Clear();
			DT.AcceptChanges();
			JobData.DefaultView.RowFilter = "Job_LEVEL=1";
			DataTable L1 = JobData.DefaultView.ToTable().Copy();
			JobData.DefaultView.RowFilter = "Job_LEVEL=2";
			DataTable L2 = JobData.DefaultView.ToTable().Copy();
			JobData.DefaultView.RowFilter = "Job_LEVEL=3";
			DataTable L3 = JobData.DefaultView.ToTable().Copy();
			for (int i = 0; i <= L1.Rows.Count - 1; i++) {
				DataRow R1 = DT.NewRow();
				R1.ItemArray = L1.Rows[i].ItemArray;
				DT.Rows.Add(R1);
				L2.DefaultView.RowFilter = "Job_Parent=" + R1["Job_ID"];
				for (int j = 0; j <= L2.DefaultView.Count - 1; j++) {
					DataRow R2 = DT.NewRow();
					R2.ItemArray = L2.DefaultView[j].Row.ItemArray;
					DT.Rows.Add(R2);
					L3.DefaultView.RowFilter = "Job_Parent=" + R2["Job_ID"];
					for (int k = 0; k <= L3.DefaultView.Count - 1; k++) {
						DataRow R3 = DT.NewRow();
						R3.ItemArray = L3.DefaultView[k].Row.ItemArray;
						DT.Rows.Add(R3);
					}
				}
			}
			JobData = DT.Copy();
			JobData.Columns.Add("PersonMin", typeof(long), "NumPerson*MinPerTime");
			JobData.Columns.Add("TotalMin", typeof(long), "PersonMin*TimePerYear");

			//----------- Job Data -------------
			string[] Col = {
				"Job_LEVEL",
				"Job_No",
				"Job_ID",
				"Job_Parent",
				"Job_Name",
				"Editable",
				"NumPerson",
				"MinPerTime",
				"TimePerYear"
			};
			DT.DefaultView.RowFilter = "";
			DT = DT.DefaultView.ToTable(true, Col);
			DataTable _job = DT.Copy();
			//----------- Get Slot Data ---------
			DT.DefaultView.RowFilter = "Editable=1";
			string _jid = "0,";
			for (int i = 0; i <= DT.DefaultView.Count - 1; i++) {
				_jid += DT.DefaultView[i]["Job_ID"] + ",";
			}

			if (!string.IsNullOrEmpty(_jid))
				_jid = _jid.Substring(0, _jid.Length - 1);
			SQL = " SELECT Header.Job_ID,Slot_ID,Slot.PSN_ID,MinPerYear\n";
			SQL += "   FROM tb_SIM_Job_Mapping Header  \n";
			SQL += "   INNER JOIN tb_SIM_Workload_Slot Slot ON Header.SIM_ID=Slot.SIM_ID AND Header.Job_ID=Slot.Job_ID\n";
			SQL += "   INNER JOIN tb_SIM_Workload_PSN PSN ON Slot.SIM_ID=PSN.SIM_ID AND Slot.PSN_ID=PSN.PSN_ID\n";
			SQL += "   INNER JOIN tb_SIM_Job_Mapping_Class CLS ON PSN.SIM_ID=CLS.SIM_ID AND PSN.Class_ID=CLS.Class_ID\n";
			SQL += "                            AND PSN.DEPT_CODE=CLS.DEPT_CODE\n";
			SQL += "                            AND PSN.MINOR_CODE=CLS.MINOR_CODE\n";
			SQL += "    WHERE Header.Job_ID IN (" + _jid + ")\n";
			SlotData = new DataTable();
			DA = new SqlDataAdapter(SQL, BL.ConnectionString());
			DA.Fill(SlotData);
			//----------- SlotData -------------

			//----------- Get Mapping Class------
			SQL = " SELECT Header.Job_ID,Detail.Class_ID\n";
			SQL += " FROM tb_SIM_Job_Mapping Header  \n";
			SQL += " INNER JOIN tb_SIM_Job_Mapping_Detail Detail ON Header.SIM_ID=Detail.SIM_ID AND Header.Job_ID=Detail.Job_ID\n";
			MapClass = new DataTable();
			DA = new SqlDataAdapter(SQL, BL.ConnectionString());
			DA.Fill(MapClass);
			//----------- for check requirement----------------

			//----------- for Move Job to another dept---------
			MoveToList = GetDeptData();
			MoveToList.DefaultView.RowFilter = "(DEPT_CODE+MINOR_CODE)<>'" + (DEPT_CODE + MINOR_CODE).Replace("'", "''") + "'";
			MoveToList = MoveToList.DefaultView.ToTable();

			rptJob.DataSource = JobData;
			rptJob.DataBind();

		}

		protected void rptJob_ItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
		{

            DataRowView drv = (DataRowView)e.Item.DataItem;
            object _sumMin;

            DataTable DT;
            //SqlCommandBuilder cmd;
            //SqlDataAdapter DA;
            //string SQL = "";


			switch (e.Item.ItemType) {
				case ListItemType.Item:
				case ListItemType.AlternatingItem:

					Label lblNo =(Label) e.Item.FindControl("lblNo");
					Label lblTask =(Label) e.Item.FindControl("lblTask");
					HtmlTableCell tdTask =(HtmlTableCell) e.Item.FindControl("tdTask");
					TextBox txtNumPerson =(TextBox) e.Item.FindControl("txtNumPerson");
					TextBox txtMinPerTime =(TextBox) e.Item.FindControl("txtMinPerTime");
					Label lblPersonMin =(Label) e.Item.FindControl("lblPersonMin");
					TextBox txtTimePerYear =(TextBox) e.Item.FindControl("txtTimePerYear");
					Label lblTotalMin =(Label) e.Item.FindControl("lblTotalMin");

					HtmlTableCell cellNumPerson =(HtmlTableCell) e.Item.FindControl("cellNumPerson");
					HtmlTableCell cellMinPerTime =(HtmlTableCell) e.Item.FindControl("cellMinPerTime");
					HtmlTableCell cellTimePerYear =(HtmlTableCell) e.Item.FindControl("cellTimePerYear");
					HtmlTableCell cellTotalMin =(HtmlTableCell) e.Item.FindControl("cellTotalMin");
					HtmlTableCell cellSumMin =(HtmlTableCell) e.Item.FindControl("cellSumMin");
					Label lblSumMin =(Label) e.Item.FindControl("lblSumMin");
					//---------- Job Header -----------
					lblNo.Attributes["Job_ID"] = drv["Job_ID"].ToString ();
					if (GL.CINT (drv["Job_LEVEL"]) == 1)
						lblNo.Text = drv["Job_No"].ToString ();
					if (!GL.IsEqualNull(drv["PersonMin"]))
						lblPersonMin.Text = GL.StringFormatNumber(drv["PersonMin"], 0);

					//----------- Calculate Sum Min -------------
					_sumMin = SlotData.Compute("SUM(MinPerYear)", "Job_ID=" + drv["Job_ID"]);
					if (!GL.IsEqualNull(_sumMin)) {
						lblSumMin.Text = GL.StringFormatNumber(_sumMin, 0);
					}

					if (!GL.IsEqualNull(drv["TotalMin"])) {
						lblTotalMin.Text = GL.StringFormatNumber(drv["TotalMin"], 0);
						//-------------------- Calculate Matching ------------------
						if (GL.IsEqualNull(_sumMin) || _sumMin != drv["TotalMin"]) {
							cellTotalMin.Attributes["class"] += " WorkLoadBGNotMatch";
							cellTotalMin.Style.Remove("background-color");
							cellTotalMin.Style.Remove("color");
							cellSumMin.Attributes["class"] += " WorkLoadBGNotMatch";
							cellSumMin.Style.Remove("background-color");
							cellSumMin.Style.Remove("color");

							if (GL.IsEqualNull(_sumMin)) {
								cellTotalMin.Attributes["title"] = "เวลาประเมินและผลรวมเวลาที่ปฏิบัติไม่เท่ากัน";
							} else if (GL.CDBL (drv["TotalMin"]) > GL.CDBL (_sumMin)) {
								cellTotalMin.Attributes["title"] = "ผลรวมเวลาที่ปฏิบัติน้อยกว่าอยู่ " + GL.StringFormatNumber(GL.CDBL(drv["TotalMin"]) - GL.CDBL(_sumMin), 0) + " นาที";
							} else if (GL.CDBL (drv["TotalMin"]) < GL.CDBL (_sumMin)) {
								cellTotalMin.Attributes["title"] = "เวลาที่ปฏิบัติมากกว่ากว่าอยู่ " + GL.StringFormatNumber(GL.CDBL(_sumMin) - GL.CDBL(drv["TotalMin"]), 0) + " นาที";
							}

						} else {
							cellTotalMin.Attributes["class"] += " WorkLoadBGMatch";
							cellTotalMin.Style.Remove("background-color");
							cellTotalMin.Style.Remove("color");
							cellSumMin.Attributes["class"] += " WorkLoadBGMatch";
							cellSumMin.Style.Remove("background-color");
							cellSumMin.Style.Remove("color");
						}
					}

					switch (GL.CINT (drv["Job_LEVEL"])) {
						case 1:
							lblTask.Text = drv["Job_Name"].ToString().Replace("\n", "<br>");
							lblTask.Font.Bold = true;
							break;
						case 2:
							lblTask.Text = drv["Job_Name"].ToString().Replace("\n", "<br>");
							//------------ Check Has Sub ----------------
							DT = JobData.Copy();
							int _parent_no =(int ) DT.Compute("MAX(Job_No)", "Job_ID=" + drv["Job_Parent"]);
							DT.DefaultView.RowFilter = "Job_LEVEL=2 AND Job_Parent=" + drv["Job_Parent"];
							//--------- หารายการที่อยู่ใน Sub เดียวกัน ------------ 
							DataTable tmp = DT.DefaultView.ToTable().Copy();
							for (int i = 0; i <= tmp.Rows.Count - 1; i++) {
								DT.DefaultView.RowFilter = "Job_Parent=" + tmp.Rows[i]["Job_ID"];
								if (DT.DefaultView.Count > 0) {
									lblTask.Text = _parent_no + "." + drv["Job_No"] + " " + drv["Job_Name"].ToString ().Replace("\n", "<br>");
									lblTask.Font.Bold = true;
									break; // TODO: might not be correct. Was : Exit For
								}
							}

							break;
						case 3:
							lblTask.Text = drv["Job_Name"].ToString().Replace("\n", "<br>");
							tdTask.Style["padding-left"] = "30px";
							break;
					}

                    bool Editable = GL.CBOOL(drv["Editable"]);

					txtNumPerson.Visible = Editable;
					txtMinPerTime.Visible = Editable;
					lblPersonMin.Visible = Editable;
					txtTimePerYear.Visible = Editable;
					lblTotalMin.Visible = Editable;
					if (Editable) {
						TC.ImplementJavaIntegerText(txtNumPerson);
						TC.ImplementJavaIntegerText(txtMinPerTime);
						TC.ImplementJavaIntegerText(txtTimePerYear);
						//--------------- Auto Save--------------
						txtNumPerson.Attributes["onChange"] = "updateHeader(" + drv["Job_ID"] + ",this.value,0,'NumPerson');";
						txtMinPerTime.Attributes["onChange"] = "updateHeader(" + drv["Job_ID"] + ",this.value,0,'MinPerTime');";
						txtTimePerYear.Attributes["onChange"] = "updateHeader(" + drv["Job_ID"] + ",this.value,0,'TimePerYear');";

						txtNumPerson.Attributes["onFocus"] = "textboxFocused(" + drv["Job_ID"] + ",this.value,0,'NumPerson'); setLastFocusControl(this);";
						txtMinPerTime.Attributes["onFocus"] = "textboxFocused(" + drv["Job_ID"] + ",this.value,0,'MinPerTime'); setLastFocusControl(this);";
						txtTimePerYear.Attributes["onFocus"] = "textboxFocused(" + drv["Job_ID"] + ",this.value,0,'TimePerYear'); setLastFocusControl(this);";

						txtNumPerson.Attributes["onBlur"] = "clearLastFocusControl();";
						txtMinPerTime.Attributes["onBlur"] = "clearLastFocusControl();";
						txtTimePerYear.Attributes["onBlur"] = "clearLastFocusControl();";

						txtNumPerson.Attributes["onKeyPress"] = "document.getElementById('ctl00_ContentPlaceHolder1_txt_Value').value=this.value;";
						txtMinPerTime.Attributes["onKeyPress"] = "document.getElementById('ctl00_ContentPlaceHolder1_txt_Value').value=this.value;";
						txtTimePerYear.Attributes["onKeyPress"] = "document.getElementById('ctl00_ContentPlaceHolder1_txt_Value').value=this.value;";

						if (!GL.IsEqualNull(drv["NumPerson"])) {
							txtNumPerson.Text = GL.StringFormatNumber(drv["NumPerson"], 0);
						}
						cellNumPerson.Attributes["class"] += " WorkLoadBGNotFill";

						if (!GL.IsEqualNull(drv["MinPerTime"])) {
							txtMinPerTime.Text = GL.StringFormatNumber(drv["MinPerTime"], 0);
						}
						cellMinPerTime.Attributes["class"] += " WorkLoadBGNotFill";

						if (!GL.IsEqualNull(drv["TimePerYear"])) {
							txtTimePerYear.Text = GL.StringFormatNumber(drv["TimePerYear"], 0);
						}
						cellTimePerYear.Attributes["class"] += " WorkLoadBGNotFill";

						cellNumPerson.Attributes["onClick"] = "document.getElementById('" + txtNumPerson.ClientID + "').focus();";
						cellMinPerTime.Attributes["onClick"] = "document.getElementById('" + txtMinPerTime.ClientID + "').focus();";
						cellTimePerYear.Attributes["onClick"] = "document.getElementById('" + txtTimePerYear.ClientID + "').focus();";
					}
					//---------- Bind Dept To Move-----
                    HtmlTableCell cellMove = (HtmlTableCell)e.Item.FindControl("cellMove");

					if (Editable) {
						cellMove.InnerHtml = "";
						//---------------- ถ้าไม่ใช่หัวย้ายไม่ได้ -----------------
					} else {
                        HtmlAnchor btnDropdown = (HtmlAnchor)e.Item.FindControl("btnDropdown");
                        HtmlAnchor btnSearch = (HtmlAnchor)e.Item.FindControl("btnSearch");
                        Repeater rptDeptList = (Repeater)e.Item.FindControl("rptDeptList");

						if (MoveToList.Rows.Count == 0) {
							btnDropdown.Attributes.Remove("data-toggle");
							btnDropdown.Attributes["onClick"] = "ShowDialogDEPT(" + drv["Job_ID"] + ",'OUT');";
						} else {
							rptDeptList.ItemDataBound += rptDeptList_ItemDataBound;
							rptDeptList.DataSource = MoveToList;
							rptDeptList.DataBind();
							btnSearch.Attributes["onClick"] = "ShowDialogDEPT(" + drv["Job_ID"] + ",'OUT');";
						}
					}

					//---------- Bind Slot -----------------------
                    Repeater rptSlot = (Repeater)e.Item.FindControl("rptSlot");
					rptSlot.ItemDataBound += rptSlot_ItemDataBound;
					rptSlot.DataSource = PSNData;
					rptSlot.DataBind();

					break;
				case ListItemType.Footer:
					//------------- Calculate Summary -------------
					DT = JobData.Copy();

                    double _result;
					object Tmp = DT.Compute("SUM(TotalMin)", "");
					if (!GL.IsEqualNull(Tmp)) {
                        Label lblTotalMinPerYear = (Label)e.Item.FindControl("lblTotalMinPerYear");
                        Label lblTotalHourPerYear = (Label)e.Item.FindControl("lblTotalHourPerYear");
                        Label lblFTEYear = (Label)e.Item.FindControl("lblFTEYear");

                        _result =GL.CDBL(Tmp);
                        lblTotalMinPerYear.Text = GL.StringFormatNumber(_result, 0);
                        _result /= 60;
                        lblTotalHourPerYear.Text = GL.StringFormatNumber(_result, 2);
                        _result /= BL.MasterFTE();
                        lblFTEYear.Text = GL.StringFormatNumber(_result, 2);
					}

					_sumMin = SlotData.Compute("SUM(MinPerYear)", "");
					if (!GL.IsEqualNull(_sumMin)) {
                        Label lblSumMinPerYear = (Label)e.Item.FindControl("lblSumMinPerYear");
                        Label lblSumHourPerYear = (Label)e.Item.FindControl("lblSumHourPerYear");
                        Label lblFTESum = (Label)e.Item.FindControl("lblFTESum");

                        _result = GL.CDBL(_sumMin);
                        lblSumMinPerYear.Text = GL.StringFormatNumber(_result, 0);
                        _result /= 60;
                        lblSumHourPerYear.Text = GL.StringFormatNumber(_result, 2);
                        _result /= BL.MasterFTE();
                        lblFTESum.Text = GL.StringFormatNumber(_result, 2);
					}

					//------------- Display Total Personal Minute Per Year ---------
                    Repeater rptMin = (Repeater)e.Item.FindControl("rptMin");
					rptMin.ItemDataBound += rptMin_ItemDataBound;
					rptMin.DataSource = PSNData;
					rptMin.DataBind();

					//------------- Display Total Personal Hour Per Year ---------
                    Repeater rptHour = (Repeater)e.Item.FindControl("rptHour");
					rptHour.ItemDataBound += rptHour_ItemDataBound;
					rptHour.DataSource = PSNData;
					rptHour.DataBind();

					//------------- Display Total Personal FTE Per Year ---------
                    Repeater rptFTE = (Repeater)e.Item.FindControl("rptFTE");
					rptFTE.ItemDataBound += rptFTE_ItemDataBound;
					rptFTE.DataSource = PSNData;
					rptFTE.DataBind();
					break;
			}
		}

		protected void rptDeptList_ItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
		{
			switch (e.Item.ItemType) {
				case ListItemType.Item:
				case ListItemType.AlternatingItem:
					HtmlAnchor btnDept =(HtmlAnchor) e.Item.FindControl("btnDept");
					RepeaterItem ParentItem =(RepeaterItem) e.Item.Parent.Parent.Parent;
					Label lblNo =(Label) ParentItem.FindControl("lblNo");

                    DataRowView drv = (DataRowView)e.Item.DataItem;
					btnDept.InnerHtml = "ย้ายไปยัง <i class='icon-arrow-right'></i> " + drv ["DEPT_NAME"].ToString ();
					btnDept.Attributes["onClick"] = "document.getElementById('ctl00_ContentPlaceHolder1_txt_Mode').value='OUT'; ";
					btnDept.Attributes["onClick"] += "moveJob(" + lblNo.Attributes["Job_ID"] + ",'" + drv ["DEPT_CODE"].ToString ().Replace("'", "\\'") + "','" + drv["MINOR_CODE"].ToString ().Replace("'", "\\'") + "')";

					break;
			}
		}

		protected void rptSlot_ItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
		{
			if (e.Item.ItemType != ListItemType.Item & e.Item.ItemType != ListItemType.AlternatingItem)
				return;

            TextBox txtSlot = (TextBox)e.Item.FindControl("txtSlot");
			HtmlTableCell cellSlot =(HtmlTableCell) e.Item.FindControl("cellSlot");
			DataTable Slot =(DataTable) SlotData.Copy();
			DataTable Job = (DataTable)JobData.Copy();

			RepeaterItem ParentItem =(RepeaterItem) e.Item.Parent.Parent;
			Label lblNo =(Label) ParentItem.FindControl("lblNo");

            DataRowView drv = (DataRowView)e.Item.DataItem;

			//-------------- ไม่มีคนอยู่ใน Slot ------------
			if (GL.IsEqualNull(drv["PSN_ID"])) {
				txtSlot.Visible = false;
				return;
			}

			Job.DefaultView.RowFilter = "Job_ID=" + lblNo.Attributes["Job_ID"];

			if (Job.DefaultView.Count > 0 && GL.CBOOL(Job.DefaultView[0]["Editable"])) {
				txtSlot.Visible = true;
				Slot.DefaultView.RowFilter = "Job_ID=" + lblNo.Attributes["Job_ID"] + " AND PSN_ID=" + drv["PSN_ID"];
				if (Slot.DefaultView.Count > 0 && !GL.IsEqualNull(Slot.DefaultView[0]["MinPerYear"])) {
					txtSlot.Text = GL.StringFormatNumber(Slot.DefaultView[0]["MinPerYear"], 0);
				}
				//------------ Check Requirement---------
				MapClass.DefaultView.RowFilter = "Job_ID=" + lblNo.Attributes["Job_ID"] + " AND Class_ID=" + drv["Class_ID"];
				if (MapClass.DefaultView.Count > 0) {
					cellSlot.Attributes["class"] += " WorkLoadBGNotFill";
				}
				TC.ImplementJavaIntegerText(txtSlot);
				cellSlot.Attributes["onClick"] = "document.getElementById('" + txtSlot.ClientID + "').focus();";
			} else {
				txtSlot.Visible = false;
			}

			txtSlot.Attributes["onChange"] = "updateHeader(" + lblNo.Attributes["Job_ID"] + ",this.value," + drv["PSN_ID"] + ",'MinPerYear');";
			txtSlot.Attributes["onFocus"] = "textboxFocused(" + lblNo.Attributes["Job_ID"] + ",this.value," + drv["PSN_ID"] + ",'MinPerYear'); setLastFocusControl(this);";
			txtSlot.Attributes["onBlur"] = "clearLastFocusControl();";
			txtSlot.Attributes["onKeyPress"] = "document.getElementById('ctl00_ContentPlaceHolder1_txt_Value').value=this.value;";
		}

		protected void rptMin_ItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
		{
			if (e.Item.ItemType != ListItemType.Item & e.Item.ItemType != ListItemType.AlternatingItem)
				return;
 DataRowView drv = (DataRowView)e.Item.DataItem;
			if (GL.IsEqualNull(drv["PSN_ID"]))
				return;
           
			DataTable DT = SlotData.Copy();
			object Tmp = DT.Compute("SUM(MinPerYear)", "PSN_ID=" + drv["PSN_ID"]);
			if (!GL.IsEqualNull(Tmp)) {
                Label lblMin = (Label)e.Item.FindControl("lblMin");
				lblMin.Text = GL.StringFormatNumber(Tmp, 0);
			}
		}

		protected void rptHour_ItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
		{
			if (e.Item.ItemType != ListItemType.Item & e.Item.ItemType != ListItemType.AlternatingItem)
				return;

            DataRowView drv = (DataRowView)e.Item.DataItem;
			
			if (GL.IsEqualNull(drv["PSN_ID"]))
				return;
			DataTable DT = SlotData.Copy();
			object Tmp = DT.Compute("SUM(MinPerYear)", "PSN_ID=" + drv["PSN_ID"]);
			if (!GL.IsEqualNull(Tmp)) {
                Label lblHour = (Label)e.Item.FindControl("lblHour");
                double _result = GL.CDBL(Tmp);
                _result /= 60;
                lblHour.Text = GL.StringFormatNumber(_result, 2);
			}
		}

		protected void rptFTE_ItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
		{
			if (e.Item.ItemType != ListItemType.Item & e.Item.ItemType != ListItemType.AlternatingItem)
				return;

            DataRowView drv = (DataRowView)e.Item.DataItem;

			if (GL.IsEqualNull(drv["PSN_ID"]))
				return;
			DataTable DT = SlotData.Copy();
			object Tmp = DT.Compute("SUM(MinPerYear)", "PSN_ID=" + drv["PSN_ID"]);
            
			if (!GL.IsEqualNull(Tmp)) {
                Label lblFTE = (Label)e.Item.FindControl("lblFTE");
                double _result = GL.CDBL(Tmp);
                _result /= 60;
                _result /= BL.MasterFTE();
                lblFTE.Text = GL.StringFormatNumber(_result, 2);
			}
		}

		#endregion


		#endregion

		#region "DialogDEPT"

		protected void ddlSector_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			BindDept();
		}

		protected void btn_DialogDEPT_Click(object sender, System.EventArgs e)
		{
			pnlDialogDEPT.Style["visibility"] = "visible";
			BindDept();
		}

		protected void btnCloseDialogDEPT_Click(object sender, System.EventArgs e)
		{
			pnlDialogDEPT.Style["visibility"] = "hidden";
		}


		private void BindDept()
		{
			string LastSector = "";
			if (ddlSector.SelectedIndex > 0) {
				LastSector = ddlSector.Items[ddlSector.SelectedIndex].Value;
			}
			BL.BindDDlSector(ddlSector, LastSector);

			string SQL = "";
			SQL += " SELECT DEPT.SECTOR_CODE,DEPT.SECTOR_NAME,DEPT.DEPT_NAME,DEPT.DEPT_CODE,DEPT.MINOR_CODE,COUNT(PSN.PSNL_NO) PSN\n";
			SQL += "  FROM vw_HR_Department DEPT\n";
			SQL += "  LEFT JOIN vw_PN_PSNL PSN ON DEPT.DEPT_CODE=PSN.DEPT_CODE AND DEPT.MINOR_CODE=PSN.MINOR_CODE\n";
			string Filter = "DEPT.SECTOR_CODE<>'10' AND ";

			if (ddlSector.SelectedIndex > 0) {
				Filter += " ( LEFT(DEPT.DEPT_CODE,2)='" + ddlSector.Items[ddlSector.SelectedIndex].Value + "' \n";
				Filter += " OR LEFT(DEPT.DEPT_CODE,4)='" + ddlSector.Items[ddlSector.SelectedIndex].Value + "'   \n";

				if (ddlSector.Items[ddlSector.SelectedIndex].Value == "3200") {
					Filter += " AND DEPT.DEPT_CODE NOT IN ('32000300'))    AND\n";
				} else if (ddlSector.Items[ddlSector.SelectedIndex].Value == "3203") {
					Filter += " OR DEPT.DEPT_CODE IN ('32000300','32030000','32030100','32030200','32030300','32030500'))  AND\n";
				} else if (ddlSector.Items[ddlSector.SelectedIndex].Value == "3204") {
					Filter += " OR DEPT.DEPT_CODE IN ('32040000','32040100','32040200','32040300','32040500','32040300'))  AND\n";
				} else if (ddlSector.Items[ddlSector.SelectedIndex].Value == "3205") {
					Filter += " OR DEPT.DEPT_CODE IN ('32050000','32050100','32050200','32050300','32050500'))  AND\n";
				} else {
					Filter += ")   AND\n";
				}







			}
			if (!string.IsNullOrEmpty(Strings.Trim(txtSearchDept.Text))) {
				Filter += " (DEPT.DEPT_NAME LIKE '%" + Strings.Trim(txtSearchDept.Text).Replace("'", "''") + "%' OR DEPT.SECTOR_NAME LIKE '%" + Strings.Trim(txtSearchDept.Text).Replace("'", "''") + "%') AND ";
			}
			//If Filter <> "" Then
			SQL += " WHERE " + Filter.Substring(0, Filter.Length - 4) + "\n";
			//End If
			SQL += "  GROUP BY DEPT.SECTOR_CODE,DEPT.SECTOR_NAME,DEPT.DEPT_NAME,DEPT.DEPT_CODE,DEPT.MINOR_CODE\n";
			SQL += " ORDER BY DEPT.SECTOR_CODE,DEPT.SECTOR_NAME,DEPT.DEPT_CODE\n";

			SqlDataAdapter DA = new SqlDataAdapter(SQL, BL.ConnectionString());
			DataTable DT = new DataTable();
			DA.Fill(DT);

			//----------- Remove Exists Department ---------
			DT.Columns.Add("Checker", typeof(string), "DEPT_CODE+'-'+MINOR_CODE");
			MoveToList = GetDeptData();
			string ExceptList = "";
			for (int i = 0; i <= MoveToList.Rows.Count - 1; i++) {
				ExceptList += "'" + (MoveToList.Rows[i]["DEPT_CODE"] + "-" + MoveToList.Rows[i]["MINOR_CODE"]).Replace("'", "''") + "',";
			}
			DT.DefaultView.RowFilter = "Checker NOT IN (" + ExceptList.Substring(0, ExceptList.Length - 1) + ")";
			DT = DT.DefaultView.ToTable().Copy();
			DT.DefaultView.RowFilter = "";

			Session["WKL_SIM_Search_DEPT"] = DT;
			Pager.SesssionSourceName = "WKL_SIM_Search_DEPT";
			Pager.RenderLayout();

			if (DT.Rows.Count == 0) {
				lblCountDEPT.Text = "ไม่พบรายการดังกล่าว";
			} else {
				lblCountDEPT.Text = "พบ " + GL.StringFormatNumber(DT.Rows.Count, 0) + " รายการ";
			}

		}

		protected void Pager_PageChanging(PageNavigation Sender)
		{
			Pager.TheRepeater = rptSearchDept;
		}

		//---------- Datasource = Department ---------

		string LastSector = "";
		protected void rptSearchDept_ItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
		{
			if (e.Item.ItemType != ListItemType.Item & e.Item.ItemType != ListItemType.AlternatingItem)
				return;

			Label lblSector =(Label) e.Item.FindControl("lblSector");
			Label lblDept =(Label) e.Item.FindControl("lblDept");
			Label lblPSN = (Label) e.Item.FindControl("lblPSN");
            LinkButton btnSelect = (LinkButton)e.Item.FindControl("btnSelect");

            DataRowView drv = (DataRowView)e.Item.DataItem;

			if (LastSector != drv["SECTOR_NAME"].ToString()) {
				lblSector.Text = drv["SECTOR_NAME"].ToString();
				LastSector = drv["SECTOR_NAME"].ToString();
			}
			lblDept.Text = drv["DEPT_NAME"].ToString();
			btnSelect.Attributes["DEPT_CODE"] = drv["DEPT_CODE"].ToString();
            btnSelect.Attributes["MINOR_CODE"] = drv["MINOR_CODE"].ToString();
			lblPSN.Text = GL.StringFormatNumber(drv["PSN"].ToString(), 0);
			btnSelect.Attributes["onClick"] = "moveJob(" + Job_ID + ",'" + drv["DEPT_CODE"].ToString().Replace("'", "\\'") + "','" + drv["MINOR_CODE"].ToString().Replace("'", "\\'") + "');";
		}

		protected void btnMove_Click(object sender, System.EventArgs e)
		{
			switch (EditMode) {
				case "IN":

					BL.CloneWorkloadJobForSimulate(SIM_ID, Target_DEPT_CODE, Target_MINOR_CODE, PSNL_NO);
					DEPT_CODE = Target_DEPT_CODE;
					//--------- Set Selection Dept --------
					MINOR_CODE = Target_MINOR_CODE;
					//--------- Set Selection Dept --------
					BindTab();
					BindWholeTable();

					break;
				case "OUT":

					DataTable DT = GetDeptData();
					DT.DefaultView.RowFilter = "DEPT_CODE='" + Target_DEPT_CODE.Replace("'", "''") + "' AND MINOR_CODE='" + Target_MINOR_CODE.Replace("'", "''") + "'";
					if (DT.DefaultView.Count == 0) {
						//----------- Add Organize if not exists--------------
						BL.CloneWorkloadJobForSimulate(SIM_ID, Target_DEPT_CODE, Target_MINOR_CODE, PSNL_NO);
					}


					SqlConnection Conn = new SqlConnection(BL.ConnectionString());
					Conn.Open();
					//------------ Get All Affiliate Job Table--------------------
					string SQL = "SELECT SIM_ID,Job_ID,PNDP_DEPT_CODE,PNDP_MINOR_CODE,Job_LEVEL,Job_Parent,\n";
					SQL += " Job_No, Job_Name, NumPerson, MinPerTime, TimePerYear, Update_By, Update_Time\n";
					SQL += " FROM tb_SIM_Job_Mapping \n";
					SQL += " WHERE SIM_ID=" + SIM_ID + "\n";

					SqlDataAdapter DA = new SqlDataAdapter(SQL, Conn);
					DT = new DataTable();
					DA.Fill(DT);

					//----------- Get All Affiliate Job ID To Move -------------
					DT.DefaultView.RowFilter = "Job_ID=" + Job_ID;
					if (DT.DefaultView.Count == 0) {
						BindTab();
						BindWholeTable();
						return;
					}
                    int JOBLEVEL =GL.CINT(DT.DefaultView[0]["Job_LEVEL"]);
					int[] target_job_id = { Job_ID };
					switch (JOBLEVEL) {
						//------------ มีได้แค่ 2 Level ที่จะย้าย -----------
						case 1:
							DT.DefaultView.RowFilter = "Job_Parent=" + Job_ID;
							//-------- ตอนนี้อยู่ที่ LEVEL 2 ----------
							for (int i = 0; i <= DT.DefaultView.Count - 1; i++) {
                                GL.PushArray_Integer(target_job_id, GL.CINT(DT.DefaultView[i]["Job_ID"]));
							}

							int child = target_job_id.Length;
							for (int i = 0; i <= child - 1; i++) {
								DT.DefaultView.RowFilter = "Job_Parent=" + target_job_id[i];
								//-------- ตอนนี้อยู่ที่ LEVEL 3 ----------
								for (int j = 0; j <= DT.DefaultView.Count - 1; j++) {
                                    GL.PushArray_Integer(target_job_id, GL.CINT(DT.DefaultView[j]["Job_ID"]));
								}
							}

							break;
						case 2:
							DT.DefaultView.RowFilter = "Job_Parent=" + Job_ID;
							for (int i = 0; i <= DT.DefaultView.Count - 1; i++) {
                                GL.PushArray_Integer(target_job_id, GL.CINT(DT.DefaultView[i]["Job_ID"]));
							}

							break;
					}
					//----------- Delete Slot -----------
					DT.DefaultView.RowFilter = "";
					string Filter = "(";
					for (int i = 0; i <= target_job_id.Length - 1; i++) {
						Filter += target_job_id[i] + ",";
					}

					Filter = Filter.Substring(0, Filter.Length - 1) + ")";

					SqlCommand COMM = new SqlCommand();
					var _with3 = COMM;
					_with3.CommandType = CommandType.Text;
					_with3.Connection = Conn;
					_with3.CommandText = "DELETE FROM tb_SIM_Workload_Slot WHERE SIM_ID=" + SIM_ID + " AND JOB_ID IN " + Filter;
					_with3.ExecuteNonQuery();

					//----------- Get Data For Compare Class_ID -----------
					DataTable Source_Class = new DataTable();
					SQL = " SELECT Class_ID,CAST(PNPO_CLASS_Start AS INT) CLASS_Start,CAST(PNPO_CLASS_End AS INT) CLASS_End\n";
					SQL += " FROM tb_SIM_Job_Mapping_Class \n";
					SQL += " WHERE SIM_ID=" + SIM_ID + " AND DEPT_CODE='" + DEPT_CODE.Replace("'", "''") + "' AND MINOR_CODE='" + MINOR_CODE.Replace("'", "''") + "'\n";
					var CA = new SqlDataAdapter(SQL, Conn);
					CA.Fill(Source_Class);

					DataTable Target_Class = new DataTable();
					SQL = " SELECT Class_ID,CAST(PNPO_CLASS_Start AS INT) CLASS_Start,CAST(PNPO_CLASS_End AS INT) CLASS_End\n";
					SQL += " FROM tb_SIM_Job_Mapping_Class \n";
					SQL += " WHERE SIM_ID=" + SIM_ID + " AND DEPT_CODE='" + Target_DEPT_CODE.Replace("'", "''") + "' AND MINOR_CODE='" + Target_MINOR_CODE.Replace("'", "''") + "'\n";
					CA = new SqlDataAdapter(SQL, Conn);
					CA.Fill(Target_Class);

					//------------ Run by Job -----------
					for (int i = 0; i <= target_job_id.Length - 1; i++) {
						DT.DefaultView.RowFilter = "JOB_ID=" + target_job_id[i];
						//----------- Change Department -----
						//----------- Change Level ----------
						if (DT.DefaultView.Count > 0) {
							DT.DefaultView[0].Row["PNDP_DEPT_CODE"] = Target_DEPT_CODE;
							DT.DefaultView[0].Row["PNDP_MINOR_CODE"] = Target_MINOR_CODE;
							if (JOBLEVEL == 2) {
                                int jl = GL.CINT(DT.DefaultView[0].Row["Job_LEVEL"])-1;
                                DT.DefaultView[0].Row["Job_LEVEL"] = jl;
							}
						}

						//--------Compare ClassID---------
						SQL = "SELECT * FROM tb_SIM_Job_Mapping_Detail WHERE SIM_ID=" + SIM_ID + " AND JOB_ID=" + target_job_id[i];
						CA = new SqlDataAdapter(SQL, Conn);
						DataTable Detail = new DataTable();
						CA.Fill(Source_Class);
						for (int r = 0; r <= Detail.Rows.Count - 1; r++) {
							Source_Class.DefaultView.RowFilter = "Class_ID=" + Detail.Rows[r]["Class_ID"];
							int _Start = GL.CINT(Source_Class.DefaultView[0]["PNPO_CLASS_Start"]);
                            int _End = GL.CINT(Source_Class.DefaultView[0]["PNPO_CLASS_End"]);
							float thePoint = (_Start + _End) / 2;
							Target_Class.DefaultView.RowFilter = thePoint + " BETWEEN CLASS_Start AND CLASS_End";
							Detail.Rows[r]["Class_ID"] = Target_Class.DefaultView[0]["Class_ID"];
						}
						SqlCommandBuilder _cmd = new SqlCommandBuilder(CA);
						CA.Update(Source_Class);
					}

					//------------ Move To New LEVEL 1 Node ---------
					DT.DefaultView.RowFilter = "Job_ID=" + Job_ID;
                    int Old_Job_No = GL.CINT(DT.DefaultView[0].Row["JOB_No"]);
					object Old_Job_Parent = DT.DefaultView[0]["Job_Parent"];

					object Max_JOB_No = DT.Compute("MAX(JOB_No)", "Job_ID<>" + Job_ID + " AND Job_LEVEL=1 AND PNDP_DEPT_CODE='" + Target_DEPT_CODE.Replace("'", "''") + "' AND PNDP_MINOR_CODE='" + Target_MINOR_CODE.Replace("'", "''") + "'");

					if (GL.IsEqualNull(Max_JOB_No)) {
						DT.DefaultView[0].Row["JOB_No"] = 1;
					} else {
						DT.DefaultView[0].Row["JOB_No"] = GL.CINT(Max_JOB_No) + 1;
					}
					DT.DefaultView[0]["JOB_Parent"] = DBNull.Value;

					//------------Reorder Job_No on source Department---------------------
					DT.DefaultView.RowFilter = "PNDP_DEPT_CODE='" + DEPT_CODE.Replace("'", "''") + "' AND PNDP_MINOR_CODE='" + MINOR_CODE.Replace("'", "''") + "'";
					DT.DefaultView.RowFilter += " AND Job_LEVEL=" + JOBLEVEL + " AND JOB_No>" + Old_Job_No;
					if (JOBLEVEL == 2) {
						DT.DefaultView.RowFilter += " AND JOB_Parent=" + Old_Job_Parent;
					}
					for (int i = DT.DefaultView.Count - 1; i >= 0; i += -1) {
                        int tmp = GL.CINT(DT.DefaultView[i].Row["JOB_No"])-1;
                        DT.DefaultView[i].Row["JOB_No"] = tmp;
					}

					SqlCommandBuilder cmd = new SqlCommandBuilder(DA);
					DA.Update(DT);

					Conn.Close();
					Conn.Dispose();

					BindTab();
					BindWholeTable();
					break;
			}
			pnlDialogDEPT.Style["visibility"] = "hidden";
		}

		#endregion

		#region "Autosave Property"

		public string EditMode {
			get { return txt_Mode.Text; }
			set { txt_Mode.Text = value.ToString (); }
		}

		public int Job_ID {
			get { return GL.CINT(txt_Job_ID.Text); }
			set { txt_Job_ID.Text = value.ToString (); }
		}

		public int PSN_ID {
			get { return GL.CINT(txt_PSN_ID.Text); }
			set { txt_PSN_ID.Text = value.ToString (); }
		}

		public string EditValue {
			get { return txt_Value.Text; }
			set { txt_Value.Text = value.ToString (); }
		}

		public string DialogText {
			get { return txtDialog.Text; }
			set { txtDialog.Text = value.ToString (); }
		}

		public string Target_DEPT_CODE {
			get { return txt_DEPT_CODE.Text; }
			set { txt_DEPT_CODE.Text = value.ToString (); }
		}

		public string Target_MINOR_CODE {
			get { return txt_MINOR_CODE.Text; }
			set { txt_MINOR_CODE.Text = value.ToString (); }
		}

		#endregion


		protected void btnBack_Click(object sender, System.EventArgs e)
		{
			BindSIMList();
			pnlList.Visible = true;
			pnlSim.Visible = false;
			pnlDialogNew.Visible = false;
			pnlDialogDEPT.Style["visibility"] = "hidden";
		}

		protected void btnOK_Click(object sender, System.EventArgs e)
		{
			if (string.IsNullOrEmpty(Strings.Trim(DialogText))) {
				ScriptManager.RegisterStartupScript(this.Page, typeof(string), "Alert", "showAlert('กรอกชื่อ');", true);
				return;
			}
			switch (EditMode) {
				case "Add":
					int Class_ID = PSN_ID;
					//-------- ขอยืมช่องใส่ --------
					string Sql = " SELECT * FROM tb_SIM_Workload_PSN \n";
					Sql += " WHERE DEPT_CODE='" + DEPT_CODE.Replace("'", "''") + "' AND MINOR_CODE='" + MINOR_CODE.Replace("'", "''") + "'\n";
					Sql += " AND SIM_ID=" + SIM_ID + " AND Class_ID=" + Class_ID + "\n";
					Sql += " ORDER BY Slot_No";

					SqlDataAdapter DA = new SqlDataAdapter(Sql, BL.ConnectionString());
					DataTable DT = new DataTable();
					DA.Fill(DT);

					DataRow DR = DT.NewRow();
					DR["SIM_ID"] = SIM_ID;
					DR["PSN_ID"] = BL.GetNewWorkLoadSimPSNID(SIM_ID);
					DR["DEPT_CODE"] = DEPT_CODE;
					DR["MINOR_CODE"] = MINOR_CODE;
					DR["Class_ID"] = Class_ID;
					if (DT.Rows.Count > 0) {
						DR["Slot_No"] =GL.CINT(DT.Rows[DT.Rows.Count - 1]["Slot_No"]) + 1;
					} else {
						DR["Slot_No"] = 1;
					}
					DR["Slot_Name"] = DialogText;
					DR["REF_PSNL_NO"] = DBNull.Value;
					//----------- Manual Add ------------
					DR["Update_By"] = PSNL_NO;
					DR["Update_Time"] = DateAndTime.Now;
					DT.Rows.Add(DR);

					SqlCommandBuilder cmd = new SqlCommandBuilder(DA);
					DA.Update(DT);
					BindWholeTable();

					break;
				case "Rename":

					string SQL = "UPDATE tb_SIM_Workload_PSN SET Slot_Name='" + DialogText.Replace("'", "''") + "'\n";
					SQL += " WHERE PSN_ID=" + PSN_ID + " AND SIM_ID=" + SIM_ID;

					SqlConnection Conn = new SqlConnection(BL.ConnectionString());
					Conn.Open();
					SqlCommand COMM = new SqlCommand();
					var _with4 = COMM;
					_with4.CommandType = CommandType.Text;
					_with4.Connection = Conn;
					_with4.CommandText = SQL;
					_with4.ExecuteNonQuery();
					_with4.Dispose();
					Conn.Close();
					Conn.Dispose();

					BindClass();
					BindPSN();
					BindHeader();

					break;
			}
		}

		protected void btnCalculate_Click(object sender, System.EventArgs e)
		{

            string SQL;

            switch (EditMode) {
				case "NumPerson":
				case "MinPerTime":
				case "TimePerYear":
					SQL = "UPDATE tb_SIM_Job_Mapping SET " + EditMode + "=";
					if (!Information.IsNumeric(EditValue)) {
						SQL += " NULL \n";
					} else {
						SQL += EditValue + "\n";
					}
					SQL += " WHERE Job_ID=" + Job_ID + " AND SIM_ID=" + SIM_ID;
					SqlConnection Conn = new SqlConnection(BL.ConnectionString());
					Conn.Open();
					SqlCommand COMM = new SqlCommand();
					var _with5 = COMM;
					_with5.CommandType = CommandType.Text;
					_with5.Connection = Conn;
					_with5.CommandText = SQL;
					_with5.ExecuteNonQuery();
					_with5.Dispose();
					Conn.Close();
					Conn.Dispose();

					BindWholeTable();

					break;
				case "MinPerYear":

					SQL = "DELETE FROM tb_SIM_Workload_Slot WHERE SIM_ID=" + SIM_ID + " AND PSN_ID=" + PSN_ID + " AND Job_ID=" + Job_ID + "\n";
					SQL += " SELECT * FROM tb_SIM_Workload_Slot WHERE SIM_ID=" + SIM_ID + " AND PSN_ID=" + PSN_ID + " AND Job_ID=" + Job_ID + "\n";
					SqlDataAdapter DA = new SqlDataAdapter(SQL, BL.ConnectionString());
					DataTable DT = new DataTable();
					DA.Fill(DT);

					if (!Information.IsNumeric(EditValue)) {
						BindWholeTable();
						return;
					}

					DataRow DR = DT.NewRow();
					DR["SIM_ID"] = SIM_ID;
					DR["Slot_ID"] = BL.GetNewWorkLoadSimSlotID(SIM_ID);
					DR["PSN_ID"] = PSN_ID;
					DR["Job_ID"] = Job_ID;
					DR["MinPerYear"] = Convert.ToInt32(EditValue);
					DR["Update_By"] = PSNL_NO;
					DR["Update_Time"] = DateAndTime.Now;
					DT.Rows.Add(DR);
					SqlCommandBuilder cmd = new SqlCommandBuilder(DA);
					DA.Update(DT);

					BindWholeTable();

					break;
			}
		}

		protected void btnDefault_Click(object sender, System.EventArgs e)
		{
			BL.SetWorkloadSIMDefaultPSN(SIM_ID, DEPT_CODE, MINOR_CODE);
			BindWholeTable();
		}

		protected void btnClear_Click(object sender, System.EventArgs e)
		{
			string SQL = "";

			SQL += " DELETE FROM tb_SIM_Workload_Slot WHERE SIM_ID=" + SIM_ID + " AND JOB_ID IN\n";
			SQL += " (SELECT DISTINCT JOB_ID FROM tb_SIM_Job_Mapping \n";
			SQL += " WHERE SIM_ID=" + SIM_ID + " AND PNDP_DEPT_CODE='" + DEPT_CODE.Replace("'", "''") + "' AND PNDP_MINOR_CODE='" + MINOR_CODE.Replace("'", "''") + "')\n\n";

			SQL += " UPDATE tb_SIM_Job_Mapping SET NumPerson=Null, MinPerTime=NUll, TimePerYear=Null\n";
			SQL += " WHERE SIM_ID=" + SIM_ID + " AND PNDP_DEPT_CODE='" + DEPT_CODE.Replace("'", "''") + "' AND PNDP_MINOR_CODE='" + MINOR_CODE.Replace("'", "''") + "' \n\n";

			SqlConnection Conn = new SqlConnection(BL.ConnectionString());
			Conn.Open();
			SqlCommand COMM = new SqlCommand();
			var _with6 = COMM;
			_with6.CommandType = CommandType.Text;
			_with6.Connection = Conn;
			_with6.CommandText = SQL;
			_with6.ExecuteNonQuery();
			_with6.Dispose();
			Conn.Close();
			Conn.Dispose();

			BindWholeTable();
		}
		public AssTeam_WKL_Workload_Sim()
		{
			Load += Page_Load;
		}







	}
}

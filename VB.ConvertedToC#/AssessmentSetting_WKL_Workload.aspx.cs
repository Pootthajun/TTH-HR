using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Xml.Linq;
using System.Diagnostics;
using System.Collections.Specialized;
using System.Configuration;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Caching;
using System.Web.SessionState;
using System.Web.Security;
using System.Web.Profile;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
namespace VB
{

	public partial class AssessmentSetting_WKL_Workload : System.Web.UI.Page
	{


		double MasterFTE = 1535;
		GenericLib GL = new GenericLib();
		//-------------------All For Demo ------------

		protected void Page_Load(object sender, System.EventArgs e)
		{
			if (!IsPostBack) {
				DataTable DT = BlankWorkloadTable();
				//----------- Simulate Data ------------
				DataRow DR_1_0 = DT.NewRow();
				DR_1_0["TaskNo"] = 1;
				DR_1_0["TaskName"] = "งานด้านบริหาร";
				DT.Rows.Add(DR_1_0);
				DataRow DR_1_1 = DT.NewRow();
				DR_1_1["TaskNo"] = 1;
				DR_1_1["SubNo1"] = 1;
				DR_1_1["TaskName"] = "ด้านวางแผน";
				DT.Rows.Add(DR_1_1);
				DataRow DR_1_1_1 = DT.NewRow();
				DR_1_1_1["TaskNo"] = 1;
				DR_1_1_1["SubNo1"] = 1;
				DR_1_1_1["SubNo2"] = 1;
				DR_1_1_1["TaskName"] = "การจัดทำแผน ฯ";
				DT.Rows.Add(DR_1_1_1);
				DataRow DR_1_1_2 = DT.NewRow();
				DR_1_1_2["TaskNo"] = 1;
				DR_1_1_2["SubNo1"] = 1;
				DR_1_1_2["SubNo2"] = 2;
				DR_1_1_2["TaskName"] = "งานเรื่องการขอจ้าง";
				DT.Rows.Add(DR_1_1_2);
				DataRow DR_1_1_3 = DT.NewRow();
				DR_1_1_3["TaskNo"] = 1;
				DR_1_1_3["SubNo1"] = 1;
				DR_1_1_3["SubNo2"] = 3;
				DR_1_1_3["TaskName"] = "การประชุมคณะทำงาน";
				DT.Rows.Add(DR_1_1_3);



				DataRow DR_1_2 = DT.NewRow();
				DR_1_2["TaskNo"] = 1;
				DR_1_2["SubNo1"] = 2;
				DR_1_2["TaskName"] = "ระบบประเมินตัวชี้วัด";
				DT.Rows.Add(DR_1_2);
				DataRow DR_1_2_1 = DT.NewRow();
				DR_1_2_1["TaskNo"] = 1;
				DR_1_2_1["SubNo1"] = 2;
				DR_1_2_1["SubNo2"] = 1;
				DR_1_2_1["TaskName"] = "งานพัฒนาความสามารถพนักงาน เพื่อสนับสนุนยุทธศาสตร์องค์กร";
				DT.Rows.Add(DR_1_2_1);
				DataRow DR_1_2_2 = DT.NewRow();
				DR_1_2_2["TaskNo"] = 1;
				DR_1_2_2["SubNo1"] = 2;
				DR_1_2_2["SubNo2"] = 2;
				DR_1_2_2["TaskName"] = "งานปรับปรุงอัตรากำลัง กรณีย้ายโรงงาน";
				DT.Rows.Add(DR_1_2_2);


				DataRow DR_2_0 = DT.NewRow();
				DR_2_0["TaskNo"] = 2;
				DR_2_0["TaskName"] = "งานสรรหาคัดเลือก";
				DT.Rows.Add(DR_2_0);

				DataRow DR_2_1 = DT.NewRow();
				DR_2_1["TaskNo"] = 2;
				DR_2_1["SubNo2"] = 1;
				DR_2_1["TaskName"] = "งานเรื่องการขอจ้าง";
				DT.Rows.Add(DR_2_1);
				DataRow DR_2_2 = DT.NewRow();
				DR_2_2["TaskNo"] = 2;
				DR_2_2["SubNo2"] = 2;
				DR_2_2["TaskName"] = "งานเรื่องการสอบคัดเลือก";
				DT.Rows.Add(DR_2_2);


				rpt.DataSource = DT;
				rpt.DataBind();
			}
		}

		private DataTable BlankWorkloadTable()
		{
			DataTable DT = new DataTable();

			DT.Columns.Add("TaskNo", typeof(string));
			DT.Columns.Add("SubNo1", typeof(string));
			DT.Columns.Add("SubNo2", typeof(string));
			DT.Columns.Add("TaskName", typeof(string));
			DT.Columns.Add("NumPerson", typeof(Int32));
			DT.Columns.Add("MinPerTime", typeof(Int32));
			DT.Columns.Add("PersonMin", typeof(Int32), "NumPerson*MinPerTime");
			DT.Columns.Add("TimePerYear", typeof(Int32));
			DT.Columns.Add("TotalMin", typeof(Int32), "PersonMin*TimePerYear");
			DT.Columns.Add("Level13", typeof(Int32));
			DT.Columns.Add("Level12", typeof(Int32));
			DT.Columns.Add("Level11", typeof(Int32));
			DT.Columns.Add("Level10", typeof(Int32));
			DT.Columns.Add("Level9", typeof(Int32));
			DT.Columns.Add("Level8", typeof(Int32));
			DT.Columns.Add("Level7", typeof(Int32));
			DT.Columns.Add("Level6", typeof(Int32));
			DT.Columns.Add("Level5", typeof(Int32));
			DT.Columns.Add("Level4", typeof(Int32));
			DT.Columns.Add("Level3", typeof(Int32));
			DT.Columns.Add("Level2", typeof(Int32));
			DT.Columns.Add("Level1", typeof(Int32));
			DT.Columns.Add("TotalPSB", typeof(Int32), "Level1+Level2+Level3+Level4+Level5+Level6+Level7+Level8");
			return DT;

		}


		protected void rpt_ItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
		{

            DataTable DT;
            switch (e.Item.ItemType) {
				case ListItemType.Item:
				case ListItemType.AlternatingItem:
                    Label lblNo = (Label)e.Item.FindControl("lblNo");
                    Label lblTask = (Label)e.Item.FindControl("lblTask");
                    TextBox txtNumPerson = (TextBox)e.Item.FindControl("txtNumPerson");
                    TextBox txtMinPerTime = (TextBox)e.Item.FindControl("txtMinPerTime");
                    TextBox txtPersonMin = (TextBox)e.Item.FindControl("txtPersonMin");
                    TextBox txtTimePerYear = (TextBox)e.Item.FindControl("txtTimePerYear");
                    TextBox txtTotalMin = (TextBox)e.Item.FindControl("txtTotalMin");

                    DataRowView drv = (DataRowView)e.Item.DataItem;

					//------------- Add Auto Calculation -----------
					for (int i = 1; i <= 13; i++) {
                        TextBox txtLevel = (TextBox)e.Item.FindControl("txtLevel" + i);
						if (!GL.IsEqualNull(drv["Level" + i.ToString()])) {
                            txtLevel.Text = GL.StringFormatNumber(drv["Level" + i.ToString()], 0);
						}
						txtLevel.Attributes["onChange"] = "document.getElementById('" + btnCalculate.ClientID + "').click();";
					}

					txtNumPerson.Attributes["onChange"] = "document.getElementById('" + btnCalculate.ClientID + "').click();";
					txtMinPerTime.Attributes["onChange"] = "document.getElementById('" + btnCalculate.ClientID + "').click();";
					txtTimePerYear.Attributes["onChange"] = "document.getElementById('" + btnCalculate.ClientID + "').click();";

					//---------- Binding Data ---------
                    DT = (DataTable)rpt.DataSource;
					txtTotalMin.Attributes["TaskNo"] = drv["TaskNo"].ToString();
					if (!GL.IsEqualNull(drv["SubNo1"])) {
                        txtTotalMin.Attributes["SubNo1"] = drv["SubNo1"].ToString();
					} else {
						txtTotalMin.Attributes["SubNo1"] = "";
					}
					if (!GL.IsEqualNull(drv["SubNo2"])) {
                        txtTotalMin.Attributes["SubNo2"] = drv["SubNo2"].ToString();
					} else {
						txtTotalMin.Attributes["SubNo2"] = "";
					}
                    txtTotalMin.Attributes["TaskName"] = drv["TaskName"].ToString();

					if (!GL.IsEqualNull(drv["TaskName"]))
						lblTask.Text += drv["TaskName"].ToString();


					//---------- หัว หมวด --------
					if (GL.IsEqualNull(drv["SubNo1"]) & GL.IsEqualNull(drv["SubNo2"])) {
                        lblNo.Text = drv["TaskNo"].ToString();
                        lblTask.Text = drv["TaskName"].ToString();
						lblTask.Font.Bold = true;
						txtNumPerson.Visible = false;
						txtMinPerTime.Visible = false;
						txtPersonMin.Visible = false;
						txtTimePerYear.Visible = false;
						txtTotalMin.Visible = false;
						for (int i = 1; i <= 13; i++) {
                            TextBox txtLevel = (TextBox)e.Item.FindControl("txtLevel" + i);
							txtLevel.Visible = false;
						}
						//lblMove.Text = "ย้ายหมวดไปยังหน่วยงานอื่น"
					//---------- หัว Sub Level 1 ที่มี Sub ย่อย --------
					} else if (!GL.IsEqualNull(drv["SubNo1"]) & GL.IsEqualNull(drv["SubNo2"])) {
						lblTask.Text = drv["TaskNo"] + "." + drv["SubNo1"] + " " + drv["TaskName"];
						lblTask.Font.Bold = true;
						txtNumPerson.Visible = false;
						txtMinPerTime.Visible = false;
						txtPersonMin.Visible = false;
						txtTimePerYear.Visible = false;
						txtTotalMin.Visible = false;
						for (int i = 1; i <= 13; i++) {
                            TextBox txtLevel = (TextBox)e.Item.FindControl("txtLevel" + i);
							txtLevel.Visible = false;
						}
						//lblMove.Text = "ย้ายกลุ่มนี้ไปหน่วยงานอื่น"
					//----------Sub ย่อย --------
					} else if (!GL.IsEqualNull(drv["SubNo2"])) {
						lblTask.Text = drv["TaskName"].ToString();
						lblTask.Font.Bold = false;
						lblTask.Style["Margin-Left"] = "20px";
						txtNumPerson.Visible = true;
						txtMinPerTime.Visible = true;
						txtPersonMin.Visible = true;
						txtTimePerYear.Visible = true;
						txtTotalMin.Visible = true;
						for (int i = 1; i <= 13; i++) {
                            TextBox txtLevel = (TextBox)e.Item.FindControl("txtLevel" + i);
							txtLevel.Visible = true;
						}
						//lblMove.Text = "ย้ายงานนี้ไปหน่วยงานอื่น"
					}


					if (!GL.IsEqualNull(drv["NumPerson"]))
						txtNumPerson.Text = GL.StringFormatNumber(drv["NumPerson"], 0);
					if (!GL.IsEqualNull(drv["MinPerTime"]))
						txtMinPerTime.Text = GL.StringFormatNumber(drv["MinPerTime"], 0);
					if (!GL.IsEqualNull(drv["PersonMin"]))
						txtPersonMin.Text = GL.StringFormatNumber(drv["PersonMin"], 0);
					if (!GL.IsEqualNull(drv["TimePerYear"]))
						txtTimePerYear.Text = GL.StringFormatNumber(drv["TimePerYear"], 0);
					if (!GL.IsEqualNull(drv["TotalMin"]))
						txtTotalMin.Text = GL.StringFormatNumber(drv["TotalMin"], 0);

					break;
				case ListItemType.Footer:
					//------------- Calculate Summary -------------
                    DT = (DataTable)rpt.DataSource;



					object Tmp = DT.Compute("SUM(TotalMin)", "");
                    double result;
					if (!GL.IsEqualNull(Tmp)) {
                        Label lblTotalMinPerYear = (Label)e.Item.FindControl("lblTotalMinPerYear");
                        Label lblTotalHourPerYear = (Label)e.Item.FindControl("lblTotalHourPerYear");
                        Label lblFTEYear = (Label)e.Item.FindControl("lblFTEYear");

                        result =GL.CDBL(Tmp.ToString());
                        lblTotalMinPerYear.Text = GL.StringFormatNumber(result, 0);
                        result /= 60;
                        lblTotalHourPerYear.Text = GL.StringFormatNumber(result, 2);
                        result /= MasterFTE;
                        lblFTEYear.Text = GL.StringFormatNumber(result, 2);
					}

					for (int i = 1; i <= 13; i++) {
                        Label lblTotalMinLevel = (Label)e.Item.FindControl("lblTotalMinLevel" + i);
                        Label lblTotalHourLevel = (Label)e.Item.FindControl("lblTotalHourLevel" + i);
                        Label lblFTELevel = (Label)e.Item.FindControl("lblFTELevel" + i);
                        Tmp = DT.Compute("SUM(Level" + i + ")", "");
						if (!GL.IsEqualNull(Tmp)) {
                            result = GL.CDBL(Tmp.ToString());
                            lblTotalMinLevel.Text = GL.StringFormatNumber(result, 0);
                            result /= 60;
                            lblTotalHourLevel.Text = GL.StringFormatNumber(result, 2);
                            result /= MasterFTE;
                            lblFTELevel.Text = GL.StringFormatNumber(result, 2);
						}
					}


					break;
			}


		}

		private DataTable CurrentWorkLoadTable()
		{
			DataTable DT = BlankWorkloadTable();

			foreach (RepeaterItem Item in rpt.Items) {
				if (Item.ItemType != ListItemType.Item & Item.ItemType != ListItemType.AlternatingItem)
					continue;

                Label lblNo = (Label)Item.FindControl("lblNo");
                Label lblTask = (Label)Item.FindControl("lblTask");
                TextBox txtNumPerson = (TextBox)Item.FindControl("txtNumPerson");
                TextBox txtMinPerTime = (TextBox)Item.FindControl("txtMinPerTime");
                TextBox txtPersonMin = (TextBox)Item.FindControl("txtPersonMin");
                TextBox txtTimePerYear = (TextBox)Item.FindControl("txtTimePerYear");
                TextBox txtTotalMin = (TextBox)Item.FindControl("txtTotalMin");

				DataRow DR = DT.NewRow();
				DR["TaskNo"] = txtTotalMin.Attributes["TaskNo"];
				if (!string.IsNullOrEmpty(txtTotalMin.Attributes["SubNo1"])) {
					DR["SubNo1"] = txtTotalMin.Attributes["SubNo1"];
				}
				if (!string.IsNullOrEmpty(txtTotalMin.Attributes["SubNo2"])) {
					DR["SubNo2"] = txtTotalMin.Attributes["SubNo2"];
				}
				DR["TaskName"] = txtTotalMin.Attributes["TaskName"];

				if (Information.IsNumeric(txtNumPerson.Text)) {
					DR["NumPerson"] = Convert.ToInt32(txtNumPerson.Text.Replace(",", ""));
				}

				if (Information.IsNumeric(txtMinPerTime.Text)) {
					DR["MinPerTime"] = Convert.ToInt32(txtMinPerTime.Text.Replace(",", ""));
				}
				if (Information.IsNumeric(txtTimePerYear.Text)) {
					DR["TimePerYear"] = Convert.ToInt32(txtTimePerYear.Text.Replace(",", ""));
				}

				for (int i = 1; i <= 13; i++) {
                    TextBox txtLevel = (TextBox)Item.FindControl("txtLevel" + i);
					if (Information.IsNumeric(txtLevel.Text)) {
						DR["Level" + i] = Convert.ToInt32(txtLevel.Text.Replace(",", ""));
					}
				}
				DT.Rows.Add(DR);
			}

			return DT;
		}

		protected void btnCalculate_Click(object sender, System.EventArgs e)
		{
			DataTable DT = CurrentWorkLoadTable();
			rpt.DataSource = DT;
			rpt.DataBind();

		}


		protected void LinkButton_Click(object sender, System.EventArgs e)
		{
			pnlEdit.Visible = true;
			pnlList.Visible = false;
		}

		protected void HideDetail_Click(object sender, System.EventArgs e)
		{
			pnlEdit.Visible = false;
			pnlList.Visible = true;
		}
		public AssessmentSetting_WKL_Workload()
		{
			Load += Page_Load;
		}

	}
}

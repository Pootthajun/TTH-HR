using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Xml.Linq;
using System.Diagnostics;
using System.Collections.Specialized;
using System.Configuration;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Caching;
using System.Web.SessionState;
using System.Web.Security;
using System.Web.Profile;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
namespace VB
{
	public partial class WUC_DateReporter : System.Web.UI.UserControl
	{

        GenericLib GL = new GenericLib();

		protected void Page_Load(object sender, System.EventArgs e)
		{
			lblDate.Text = DateTime.Now.Day + " " + ReportMonthThai(DateTime.Now.Month) + " " + (DateTime.Now.Year + 543) + "    " + DateTime.Now.Hour.ToString().PadLeft(2, GL.chr0) + ":" + DateTime.Now.Minute.ToString().PadLeft(2, GL.chr0);

		}

		public string ReportMonthEnglish(int MonthID)
		{
			switch (MonthID) {
				case 1:
					return "January";
				case 2:
					return "February";
				case 3:
					return "March";
				case 4:
					return "April";
				case 5:
					return "May";
				case 6:
					return "June";
				case 7:
					return "July";
				case 8:
					return "August";
				case 9:
					return "September";
				case 10:
					return "October";
				case 11:
					return "November";
				case 12:
					return "December";
				default:
					return "";
			}
		}

		public string ReportMonthThai(int MonthID)
		{
			switch (MonthID) {
				case 1:
					return "มกราคม";
				case 2:
					return "กุมภาพันธ์";
				case 3:
					return "มีนาคม";
				case 4:
					return "เมษายน";
				case 5:
					return "พฤษภาคม";
				case 6:
					return "มิถุนายน";
				case 7:
					return "กรกฎาคม";
				case 8:
					return "สิงหาคม";
				case 9:
					return "กันยายน";
				case 10:
					return "ตุลาคม";
				case 11:
					return "พฤศจิกายน";
				case 12:
					return "ธันวาคม";
				default:
					return "";
			}
		}
		public WUC_DateReporter()
		{
			Load += Page_Load;
		}

	}
}

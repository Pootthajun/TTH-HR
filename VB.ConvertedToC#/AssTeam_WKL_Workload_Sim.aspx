﻿<%@ Page  Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPage.Master" CodeBehind="AssTeam_WKL_Workload_Sim.aspx.cs" Inherits="VB.AssTeam_WKL_Workload_Sim" %>

<%@ Register src="WUC_DateReporter.ascx" tagname="WUC_DateReporter" tagprefix="uc1" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<%@ Register src="PageNavigation.ascx" tagname="PageNavigation" tagprefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

	<link type="text/css" rel="stylesheet" href="assets/plugins/font-awesome/css/font-awesome.css" />	
    <link type="text/css" rel="stylesheet" href="assets/css/pages/search.css" />
	<link type="text/css" rel="stylesheet" href="assets/css/pages/profile.css" />
	<link type="text/css" rel="stylesheet" href="assets/plugins/chosen-bootstrap/chosen/chosen.css" />
	<style>
	    .portfolio-btn a:hover {
            background: none repeat scroll 0 0 red !important;
	    }
	</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<asp:ToolkitScriptManager ID="toolkit1" runat="server"></asp:ToolkitScriptManager>
                                     

<asp:UpdatePanel ID="UpdatePanel1" runat="server">
<ContentTemplate>
<div class="container-fluid">
				<!-- BEGIN PAGE HEADER-->
				<div class="row-fluid" id="fixedPageHeader">
					<div class="span12"> 
						<!-- BEGIN PAGE TITLE & BREADCRUMB-->
						<h3 class="page-title">จำลองปรับภารกิจงาน (Simulation) <font color="blue">(ScreenID : S-MGR-09)</font></h3>
						
									
						<ul class="breadcrumb">
                            <li>
                            	<i class="icon-dashboard"></i><a href="javascript:;">การบริหารอัตรากำลัง (Workload)</a><i class="icon-angle-right"></i>
                            </li>
                        	<li>
                        	    <i class="icon-dashboard"></i> <a href="javascript:;">จำลองปรับภารกิจงาน (Simulation)</a>
                        	</li>
                        	
                        	
                            <uc1:WUC_DateReporter ID="WUC_DateReporter1" runat="server" />
                        </ul>
						<!-- END PAGE TITLE & BREADCRUMB-->
									        
				     </div>

				
			    </div>
		        <!-- END PAGE HEADER-->
		    <!-- SAVED WORKSPACE-->
					
		    <asp:Panel ID="pnlList" runat="server"  CssClass="portlet-body form">
		                        
		                        
									<asp:Repeater ID="rptList" OnItemCommand="rptList_ItemCommand" OnItemDataBound="rptList_ItemDataBound" runat="server">
									    <ItemTemplate>
									        <div class="row-fluid portfolio-block">
								                <div class="span4 portfolio-text">
										            <asp:LinkButton ID="lnkSelect" runat="server" CssClass="fancybox-button" CommandName="Select"><img alt="" src="assets/img/simIcon.png"></asp:LinkButton>
										            <div class="portfolio-text-info">
											            <h4><asp:Label ID="lblName" runat="server"></asp:Label></h4>
											            <p><< คลิกเพื่อดำเนินการต่อ</p>
										            </div>
									            </div>
									            <div class="span7">
										            <div class="portfolio-info">
											            หน่วยงานที่เกี่ยวข้อง
											            <span><asp:Label ID="lblDept" runat="server"></asp:Label></span>
										            </div>
										            <div class="portfolio-info">
											            บุคคลากรที่เกี่ยวข้อง
											            <span><asp:Label ID="lblPSN" runat="server"></asp:Label></span>
										            </div>
										            <div class="portfolio-info">
											            แก้ไขล่าสุดเมื่อวันที่
											            <span><asp:Label ID="lblUpdate" runat="server"></asp:Label></span>
										            </div>
									            </div>
									            <div class="span1 portfolio-btn">
										            <asp:LinkButton ID="lnkDelete" CssClass="btn bigicn-only" runat="server" CommandName="Delete"><span>ลบ</span></asp:LinkButton>
										            <asp:ConfirmButtonExtender TargetControlID="lnkDelete" ID="cfmbtnDelete" ConfirmText="ยืนยันปรับโครงสร้าง ?" runat="server"></asp:ConfirmButtonExtender>										                     
									            </div>
									        </div>
									    </ItemTemplate>
									</asp:Repeater>
									
								
		        
		        <asp:LinkButton ID="btnNew" OnClick="btnNew_Click" runat="server" CssClass="btn icn-only green"><i class="m-icon-swapright m-icon-white"></i> สร้างใหม่</asp:LinkButton>
		        
		    </asp:Panel>
		    
			<asp:Panel ID="pnlSim" runat="server" CssClass="portlet-body form">
				    
				    <h3><i class="icon-dashboard"></i> 
				        <asp:Label ID="lblSimName" runat="server"></asp:Label>	 
				        <asp:LinkButton CssClass="btn blue" runat="server" ID="btnBack1" OnClick="btnBack_Click"><i class="icon-arrow-left"></i> กลับไปยังแบบจำลองทั้งหมด</asp:LinkButton>	
    				</h3>

                    <div class="btn-group pull-right">                                    
			                <asp:LinkButton ID="btnExcel" runat="server" OnClick="btnExcel_click" CssClass="btn green">ส่งออกเป็น Excel <i class="icon-upload-alt"></i></asp:LinkButton>
		            </div>

				    <br><asp:Button ID="btnCalculate" OnClick="btnCalculate_Click" runat="server" style="display:none" />
	                <div class="tabbable tabbable-custom">
				            <ul class="nav nav-tabs">
					            <asp:Repeater ID="rptTabDEPT" OnItemCommand="rptTabDEPT_ItemCommand" OnItemDataBound="rptTabDEPT_ItemDataBound" runat="server">
					                <ItemTemplate>
					                    <li class="active" id="liCaption" runat="server" style="position:static;">
					                        
					                        <a href="javascript:;">
					                        <asp:Button ID="btnDEPT" runat="server" CommandName="Select" style="cursor:pointer; background-color:White; border:none; margin:0px;"></asp:Button>
					                        <span class='badge badge-important' id="badgeDelete" runat="server" style="position:static; left:5px; cursor:pointer;">x</span>
					                        <asp:LinkButton ID="lnkDelete" runat="Server" CommandName="Delete" style="display:none;"></asp:LinkButton>	
					                        </a>
					                    </li>
					                </ItemTemplate>
					            </asp:Repeater>	
					            <li style="position:static;"><a href="javascript:ShowDialogDEPT(0,'IN');" class="btn blue"><i class="icon-download-alt"></i> เลือกหน่วยงาน</a></li>				            
				            </ul>
				   
						            <asp:Panel CssClass="portlet-body no-more-tables" ID="pnlJob" runat="server" DefaultButton="btnCalculate">
				              
				                        <table class="table table-bordered" style="background-color:White;">
									        <thead>
										        <tr>
											        <th rowspan="2" style="text-align:center; background-color:#CCCCCC; padding:0px !important; width:30px; font-size:13px;"> ลำดับ</th>
											        <th rowspan="2" style="text-align:center; background-color:#CCCCCC; padding:0px !important; font-size:13px;"> งาน</th>
											        <th rowspan="2" style="text-align:center; background-color:#CCCCCC; padding:0px !important; width:30px;">ย้าย</th>
											        <th rowspan="2"style="text-align:center; background-color:#CCCCCC; padding:0px !important; font-size:13px; width:auto; min-width:100px;" >จำนวน<br>ผู้ปฏิบัติ</th>
											        <th rowspan="2"style="text-align:center; background-color:#CCCCCC; padding:0px !important; font-size:13px; width:auto; min-width:100px;">จำนวน<br>นาทีที่ใช้<br>ในการ<br>ปฏิบัติ<br>ต่อครั้ง</th>
											        <th rowspan="2"style="text-align:center; background-color:#CCCCCC; padding:0px !important; font-size:13px; width:auto; min-width:60px;">รวม</th>
											        <th rowspan="2"style="text-align:center; background-color:#CCCCCC; padding:0px !important; font-size:13px; width:auto; min-width:80px;">จำนวนครั้ง<br>(ความถี่)<br>ที่ใช้ใน<br>การปฏิบัติ<br>ใน 1 ปี</th>
											        <th rowspan="2"style="text-align:center; background-color:#CCCCCC; padding:0px !important; font-size:13px; width:auto; min-width:80px;">จำนวน<br>นาทีที่ใช้<br>ใน 1 ปี
											            <br>
											            <img src="images/arrow_blue_left.png" /><img src="images/sum.png" />
											        </th>
											        <th rowspan="2"style="text-align:center; background-color:#CCCCCC; padding:0px !important; font-size:13px; width:auto; min-width:80px;">ผลรวมเวลา<br>ที่ใช้ในการปฏิบัติ
											            <br>
											            <img src="images/sum.png" /><img src="images/arrow_blue_right.png" />
											        </th>
											        <asp:Repeater ID="rptClass" OnItemCommand="rptClass_ItemCommand" OnItemDataBound="rptClass_ItemDataBound" runat="server">
											            <ItemTemplate>
											                <th id="thClass" runat="server" style="text-align:center; background-color:white; padding:0px !important; font-size:13px; min-width:150px; padding:5px;">
											                   <div class="btn-group" style="text-align:left; cursor:pointer;" >
											                    <a id="lnkClassName" href="javascript:;" runat="Server" class="dropdown-toggle" style="width:100% !important; font-size:13px; font-weight:bold; border:0px; text-decoration:none; color:Black;" data-toggle="dropdown">xxxxxxxxx <i class='icon-angle-down'></i></a>
										                        <ul class="dropdown-menu pull-left">
										                          <li><a id="btnAddPSN" href="javascript:;" runat="server" style="cursor:pointer; text-decoration:none;"><i class="icon-user"></i> เพิ่มผู้ปฏิบัติ </a></li>
										                          <li><asp:LinkButton id="btnDefault" CommandName="Default" runat="server" style="cursor:pointer; text-decoration:none;"><i class="icon-download"></i> ปรับจำนวนผู้ปฏิบัติตามฐานข้อมูลจริง &nbsp;</asp:LinkButton></li>										                  
									                            </ul>
									                            <asp:ConfirmButtonExtender TargetControlID="btnDefault" ID="cfmbtnDefault" ConfirmText="ยืนยันปรับโครงสร้าง ?" runat="server"></asp:ConfirmButtonExtender>
									                          </div>
											                </th>
											            </ItemTemplate>
											        </asp:Repeater>
											        
									            </tr>      
									            
									            <tr>
											        <asp:Repeater ID="rptPSN" OnItemCommand="rptPSN_ItemCommand" OnItemDataBound="rptPSN_ItemDataBound" runat="server">
											            <ItemTemplate>
											                <th style="text-align:center; background-color:#e3ebf0; padding:0px !important; font-size:12px; font-weight:normal;">
									                          <div id="btnGroup" runat="server" class="btn-group" style="text-align:left; cursor:pointer; margin:0px 10px 0px 10px ; ">
										                        <a id="lnkPSNName" runat="server" href="javascript:;" class="dropdown-toggle" style="width:100% !important; background-color:#e3ebf0; font-size:12px; font-weight:normal; text-decoration:none; border:0px;" data-toggle="dropdown">xxxxxxxxx <i class="icon-angle-down"></i></a>
										                        <ul class="dropdown-menu pull-right">
										                          <li><a href="javascript:;" id="btnRename" runat="server"><i class="icon-edit"></i> เปลี่ยนชื่อ</a></li>
										                          <li><asp:LinkButton id="btnDelete" runat="server" CommandName="Delete"><i class="icon-trash"></i> ลบ</asp:LinkButton>
										                            <asp:ConfirmButtonExtender TargetControlID="btnDelete" ID="cfmBtnDelete" ConfirmText="ยืนยันลบเจ้าหน้าที่รายการนี้" runat="server"></asp:ConfirmButtonExtender>
										                          </li>										                  
									                            </ul>
									                          </div>
									                        </th>
									                    </ItemTemplate>
											        </asp:Repeater>									                
									            </tr>                                         
									        </thead>
									        <tbody>
									            <asp:Repeater ID="rptJob" OnItemDataBound="rptJob_ItemDataBound" runat="server">
									                <ItemTemplate>
									                    <tr>
									                      <td style="width:40px; text-align:center; padding-left:0px; padding-right:0px; "><asp:Label ID="lblNo" runat="server" Font-Bold="true" ></asp:Label></td>
									                      <td style="min-width:150px; border-right: 1px solid #DDD;" id="tdTask" runat="server"><asp:Label ID="lblTask" runat="server" style="min-width:150px;" ></asp:Label></td>
									                      <td style="width:30px; border-left: none;" id="cellMove" runat="server">
									                                    <div class="btn-group" >
									                                        <a id="btnDropdown" runat="server" class="btn " data-toggle="dropdown"  style="width:40px; cursor:pointer; position:static; padding:3px 10px 3px 10px; cursor:pointer; background-color:White" href="javascript:;" title="ย้ายงานนี้"><i class="icon-external-link"></i></a>
									                                        									                                        
													                        <ul class="dropdown-menu">
													                            <asp:Repeater id="rptDeptList" runat="Server">
													                                <ItemTemplate>
										                                               <li style="padding-right:10px; cursor:pointer;"><a id="btnDept" runat="server" href="javascript:;"></a></li>									                                        
													                                </ItemTemplate>
													                                <FooterTemplate>
													                                    <li class="divider" id="divider" runat="server"></li>
													                                </FooterTemplate>
													                            </asp:Repeater>														              
														                        <li><a id="btnSearch" runat="server" href="javascript:;"><i class="icon-search"></i> เลือกหน่วยงานอื่น</a></li>
													                        </ul>
													                    </div>							                 
									                      </td>
									                      <td class="WorkLoadTableCell" id="cellNumPerson" runat="server"><asp:TextBox id="txtNumPerson" runat="server" CssClass="WorkLoadTableTextbox" style="min-width:20px;" MaxLength="3"></asp:TextBox></td>
                                                          <td class="WorkLoadTableCell" id="cellMinPerTime" runat="server"><asp:TextBox id="txtMinPerTime" runat="server" CssClass="WorkLoadTableTextbox" style="min-width:30px;" MaxLength="5"></asp:TextBox></td>
                                                          <td class="WorkLoadTableCell" style="background-color:#eeeeee; color:#555555; text-align:right; padding-top:4px !important;"><asp:Label id="lblPersonMin" Font-Size="14px" runat="server" style="margin:6px 8px 6px 8px; font-weight:600;" ></asp:Label></td>
                                                          <td class="WorkLoadTableCell" id="cellTimePerYear" runat="server" style="" ><asp:TextBox id="txtTimePerYear" runat="server" CssClass="WorkLoadTableTextbox" style="min-width:20px;" MaxLength="3"></asp:TextBox></td>
                                                          <td class="WorkLoadTableCell" style="background-color:#eeeeee; color:#555555; text-align:right; padding-top:4px !important;" id="cellTotalMin" runat="server" ><asp:Label id="lblTotalMin" Font-Size="14px" style="margin:6px 8px 6px 8px; font-weight:600;" runat="server" ></asp:Label></td>
                                                          <td class="WorkLoadTableCell" style="background-color:#eeeeee; color:#555555; text-align:right; padding-top:4px !important;" id="cellSumMin" runat="server"><asp:Label id="lblSumMin" Font-Size="14px" style="margin:6px 8px 6px 8px; font-weight:600;" runat="server" ></asp:Label></td>
                                                          <asp:Repeater ID="rptSlot" runat="server">
                                                              <ItemTemplate>
                                                                    <td class="WorkLoadTableCell" id="cellSlot" runat="server"><asp:TextBox id="txtSlot" runat="server" CssClass="WorkLoadTableTextbox" MaxLength="6"></asp:TextBox></td>
                                                              </ItemTemplate>
                                                          </asp:Repeater>
									                    </tr>
									                </ItemTemplate>
									                <FooterTemplate>
									                 <tr>
									                      <td>&nbsp;</td>
									                      <td style="text-align:right; font-weight:bold; background-color:#F8F8F8 !important;" colspan="6">จำนวนนาทีที่ใช้ในการปฏิบัติ</td>
									                      <td class="WorkLoadSummaryCell"><asp:Label ID="lblTotalMinPerYear" runat="server"></asp:Label></td>
									                      <td class="WorkLoadSummaryCell"><asp:Label ID="lblSumMinPerYear" runat="server"></asp:Label></td>
									                      <asp:Repeater ID="rptMin" runat="server">
									                        <ItemTemplate>
									                            <td class="WorkLoadSummaryCell"><asp:Label ID="lblMin" runat="server"></asp:Label></td>
									                        </ItemTemplate>
									                      </asp:Repeater>
									                    </tr>
									                    
									                    <tr>
									                      <td>&nbsp;</td>
									                      <td style="text-align:right; font-weight:bold; background-color:#F8F8F8 !important;" colspan="6">คิดเป็นจำนวนชั่วโมง</td>
									                      <td class="WorkLoadSummaryCell"><asp:Label ID="lblTotalHourPerYear" runat="server"></asp:Label></td>
									                      <td class="WorkLoadSummaryCell"><asp:Label ID="lblSumHourPerYear" runat="server"></asp:Label></td>
									                      <asp:Repeater ID="rptHour" runat="server">
									                        <ItemTemplate>
									                            <td class="WorkLoadSummaryCell"><asp:Label ID="lblHour" runat="server"></asp:Label></td>
									                        </ItemTemplate>
									                      </asp:Repeater>
									                    </tr>									                    
									                    <tr>
									                      <td>&nbsp;</td>
									                      <td style="text-align:right; font-weight:bold; background-color:#F8F8F8 !important;" colspan="6">FTE</td>
									                      <td class="WorkLoadSummaryCell"><asp:Label ID="lblFTEYear" runat="server"></asp:Label></td>
									                      <td class="WorkLoadSummaryCell"><asp:Label ID="lblFTESum" runat="server"></asp:Label></td>
									                      <asp:Repeater ID="rptFTE" runat="server">
									                        <ItemTemplate>
									                             <td class="WorkLoadSummaryCell"><asp:Label ID="lblFTE" runat="server"></asp:Label></td>
									                        </ItemTemplate>
									                      </asp:Repeater>
									                    </tr>
									                </FooterTemplate>
									            </asp:Repeater>
									            									   	       							
									        </tbody>
								        </table>
								        
						                <div class="form-actions">										    
										    <asp:LinkButton CssClass="btn" runat="server" ID="btnBack2" OnClick="btnBack_Click"><i class="icon-arrow-left"></i> กลับไปยังแบบจำลองทั้งหมด</asp:LinkButton>
										    <asp:LinkButton CssClass="btn red" runat="server" ID="btnClear" OnClick="btnClear_Click" Text="ลบข้อมูลเวลาทั้งหมด" />
										    <asp:LinkButton CssClass="btn blue" runat="server" ID="btnDefault" OnClick="btnDefault_Click" Text="ปรับโครงสร้างผู้ปฏิบัติให้เป็นปัจจุบัน" />
										    <asp:ConfirmButtonExtender ID="dfmClear" runat="server" ConfirmText="ยืนยันลบตารางทั้งหมด ?" TargetControlID="btnClear"></asp:ConfirmButtonExtender>
										    <asp:ConfirmButtonExtender ID="cfmDefault" runat="server" ConfirmText="ยืนยันปรับโครงสร้าง ?" TargetControlID="btnDefault"></asp:ConfirmButtonExtender>
										</div>
				               
							        </asp:Panel>
					
				    </div>
						
			    
		   </asp:Panel>
            
         
                               
</div>
            
                <asp:Panel ID="pnlDialogNew" runat="server">
			        <div style="z-index: 10049;" class="modal-backdrop fade in"></div>
				    <div style="z-index: 10050;" class="modal-scrollable">
				        <div style="display: block; " class="modal hide fade in" tabindex="-1">
				            <div class="modal-header">
	                            <h3>สร้างแบบจำลองใหม่</h3>
                            </div>
                            <div class="modal-body">
	                                        <div class="control-group">
						                        <label class="control-label">ตั้งชื่อ</label>
						                        <div class="controls">
						                            <asp:TextBox runat="server" ID="txtNewName" CssClass="large m-wrap"></asp:TextBox>
							                    </div>
					                        </div>
					                        <div class="control-group">
						                        <label class="control-label">เริ่มที่หน่วยงาน</label>
						                        <div class="controls">
							                        <asp:DropDownList ID="ddlNewDept" runat="server" AutoPostBack="true" CssClass="m-wrap" style="font-size:20px; width:auto !important; max-width:80%; padding:0px;">
		                                            </asp:DropDownList>
						                        </div>
					                        </div>
                            </div>
                            <div class="modal-footer">
                                <asp:Label ID="lblErrorDialogNew" runat="server" ForeColor="red"></asp:Label> &nbsp; &nbsp; &nbsp;
	                            <asp:LinkButton ID="btnOKDialogNew" OnClick="btnOKDialogNew_Click" runat="server" CssClass="btn green" Text="เริ่มสร้าง" />
                            </div>
                            <asp:LinkButton ID="btnCloseDialogNew" OnClick="btnCloseDialogNew_Click" runat="server" CssClass="fancybox-item fancybox-close" ToolTip="Close"></asp:LinkButton>
                        </div>
                    </div>
			    </asp:Panel>
            
               <asp:Panel ID="pnlDialogDEPT" DefaultButton="btn_DialogDEPT" runat="server">
                    <div style="z-index: 10049;" class="modal-backdrop fade in"></div>
                    <div style="z-index: 10050; max-width:80%; min-width:800px; left:35%; top:5%;" class="modal">
                        <%--<div style="display: block; left:30%; right:20%; position:fixed;" class="modal hide fade in" tabindex="-1">--%>
				            <div class="modal-header">
	                            <h3><i class="icon-search"></i> เลือกหน่วยงาน</h3>
                            </div>
                            <div class="modal-body" style="max-height:90%;">
	                                  	<div >
											<div class="row-fluid">
												<div class="form-horizontal">												        
												        <div class="span12">
													        ฝ่าย &nbsp;
													        <asp:DropDownList ID="ddlSector" OnSelectedIndexChanged="ddlSector_SelectedIndexChanged" runat="server" CssClass="medium m-wrap" AutoPostBack="true"></asp:DropDownList>
													        &nbsp; &nbsp; &nbsp; หน่วยงาน &nbsp; 
														    <asp:TextBox ID="txtSearchDept" OnTextChanged="ddlSector_SelectedIndexChanged" runat="server" CssClass="medium m-wrap" placeholder="ค้นหาจากชื่อกอง/สำนัก" AutoPostBack="true"></asp:TextBox>	
														    &nbsp; &nbsp;
														     <asp:Label ID="lblCountDEPT" runat="server" Font-Size="14px" Font-Bold="true"></asp:Label>													   
												        </div>
												       	<br>								        
												       	<br>	
												        <div class="portlet-body no-more-tables">
								                           
								                            <table class="table table-full-width table-advance dataTable no-more-tables table-hover">
									                            <thead>
										                            <tr>
											                            <th><i class="icon-sitemap"></i> ฝ่าย</th>
											                            <th><i class="icon-sitemap"></i> หน่วยงาน</th>
											                            <th style="width:80px;"><i class="icon-user"></i> พนักงาน</th>
											                            <th style="width:40px;"> เลือก</th>
										                            </tr>
									                            </thead>
									                            <tbody>
										                            <asp:Repeater ID="rptSearchDept" OnItemDataBound="rptSearchDept_ItemDataBound" runat="server">
										                                <ItemTemplate>
									                                        <tr>
										                                        <td class="highlight" data-title="ฝ่าย"><asp:Label ID="lblSector" runat="server"></asp:Label></td>
										                                        <td class="highlight" data-title="หน่วยงาน"><asp:Label ID="lblDept" runat="server"></asp:Label></td>
										                                        <td class="highlight" data-title="พนักงาน" style="width:80px;"><asp:Label ID="lblPSN" runat="server"></asp:Label></td>
										                                        <td data-title="เลือก" style="width:40px;"><asp:LinkButton ID="btnSelect" runat="server" CssClass="btn mini blue" href="javascript:;" style="cursor:pointer"><i class="icon-circle-arrow-left"></i></asp:LinkButton></td>
									                                        </tr>
										                                </ItemTemplate>
										                            </asp:Repeater>										                            										                            								
									                            </tbody>
								                            </table>
								                            <asp:PageNavigation ID="Pager" OnPageChanging="Pager_PageChanging" MaximunPageCount="10" PageSize="7" runat="server" />
							                            </div>
											   </div>
											</div>
										</div>
                            </div>                            
                            <a href="javascript:;" onclick="document.getElementById('ctl00_ContentPlaceHolder1_pnlDialogDEPT').style.visibility='hidden'; document.getElementById('ctl00_ContentPlaceHolder1_btnCloseDialogDEPT').click();" class="fancybox-item fancybox-close" title="Close"></a>
                            <asp:Button ID="btnCloseDialogDEPT" OnClick="btnCloseDialogDEPT_Click" runat="server" style="display:none;" />
                        <%--</div>--%>
                    </div>
               </asp:Panel>
			
			    <asp:Panel CssClass="modal" DefaultButton="btnOK" id="divModal" SIM_ID="0" runat="server" style="width:400px; top:20%; visibility:hidden;" >
			        <div class="modal-header">
				        <h3><asp:Label ID="lblHeader" runat="server"></asp:Label></h3>
			        </div>
			        <div class="modal-body" style="padding:0px; overflow:hidden;">
			        <asp:TextBox ID="txtDialog" MaxLength="255" runat="server" Rows="4" CssClass="large m-wrap" style="width:100% !important; height:100%; margin:0px;"></asp:TextBox>			        
        			
			        </div>
			        <div class="modal-footer">
			            <asp:Button ID="btnOK" OnClick="btnOK_Click" runat="server" CssClass="btn blue" Text="บันทึก" />
			            <a class="btn" onClick="document.getElementById('ctl00_ContentPlaceHolder1_divModal').style.visibility='hidden';">ยกเลิก</a>			    
			        </div>			
			        <asp:TextBox ID="txt_Job_ID" runat="server" style="width:0px; height:0px; display:none;" Text="0"></asp:TextBox>
                    <asp:TextBox ID="txt_Value" runat="server" style="width:0px; height:0px; display:none;" Text=""></asp:TextBox>
                    <asp:TextBox ID="txt_PSN_ID" runat="server" style="width:0px; height:0px; display:none;" Text="0"></asp:TextBox>
                    <asp:TextBox ID="txt_Mode" runat="server" style="width:0px; height:0px; display:none;" Text=""></asp:TextBox>
                    <asp:TextBox ID="txt_DEPT_CODE" runat="server" style="width:0px; height:0px; display:none;" Text="0"></asp:TextBox>
                    <asp:TextBox ID="txt_MINOR_CODE" runat="server" style="width:0px; height:0px; display:none;" Text="0"></asp:TextBox>
                    <asp:Button ID="btn_DialogDEPT"  OnClick="btn_DialogDEPT_Click" runat="server" style="display:none" />
                    <asp:Button ID="btnMove" OnClick="btnMove_Click" runat="server" style="display:none" />
		        </asp:Panel>
				    
</ContentTemplate>           
</asp:UpdatePanel>

<script language="javascript">
    function showDialog(PSN_ID, PSN_Name, HeaderText, Mode) {
        document.getElementById("ctl00_ContentPlaceHolder1_txt_PSN_ID").value = PSN_ID;
        document.getElementById("ctl00_ContentPlaceHolder1_txtDialog").value = PSN_Name;
        document.getElementById("ctl00_ContentPlaceHolder1_txt_Mode").value = Mode;
        document.getElementById("ctl00_ContentPlaceHolder1_lblHeader").innerHTML = HeaderText;
        document.getElementById('ctl00_ContentPlaceHolder1_divModal').style.visibility = 'visible';
        document.getElementById("ctl00_ContentPlaceHolder1_txtDialog").focus();
    }

    function updateHeader(Job_ID, _Value, PSN_ID, Mode) {
        document.getElementById("ctl00_ContentPlaceHolder1_txt_Job_ID").value = Job_ID;
        document.getElementById("ctl00_ContentPlaceHolder1_txt_Value").value = _Value.toString().replace(',', '');
        document.getElementById("ctl00_ContentPlaceHolder1_txt_PSN_ID").value = PSN_ID;
        document.getElementById("ctl00_ContentPlaceHolder1_txt_Mode").value = Mode;
        document.getElementById("ctl00_ContentPlaceHolder1_btnCalculate").click();
    }

    function textboxFocused(Job_ID, _Value, PSN_ID, Mode) {
        document.getElementById("ctl00_ContentPlaceHolder1_txt_Job_ID").value = Job_ID;
        document.getElementById("ctl00_ContentPlaceHolder1_txt_Value").value = _Value;
        document.getElementById("ctl00_ContentPlaceHolder1_txt_PSN_ID").value = PSN_ID;
        document.getElementById("ctl00_ContentPlaceHolder1_txt_Mode").value = Mode;
    }

    function ShowDialogDEPT(Job_ID, Mode) {
        document.getElementById("ctl00_ContentPlaceHolder1_txt_Job_ID").value = Job_ID;
        document.getElementById("ctl00_ContentPlaceHolder1_txt_Mode").value = Mode;
        document.getElementById("ctl00_ContentPlaceHolder1_pnlDialogDEPT").style.visibility = 'visible';
        document.getElementById("ctl00_ContentPlaceHolder1_btn_DialogDEPT").click();
    }

    function moveJob(Job_ID, DEPT_CODE, MINOR_CODE) {
        document.getElementById("ctl00_ContentPlaceHolder1_txt_Job_ID").value = Job_ID;
        document.getElementById("ctl00_ContentPlaceHolder1_txt_DEPT_CODE").value = DEPT_CODE;
        document.getElementById("ctl00_ContentPlaceHolder1_txt_MINOR_CODE").value = MINOR_CODE;
        document.getElementById("ctl00_ContentPlaceHolder1_pnlDialogDEPT").style.visibility = 'hidden';
        document.getElementById("ctl00_ContentPlaceHolder1_btnMove").click();

    }
</script>
	
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ScriptPlaceHolder" Runat="Server">

</asp:Content>




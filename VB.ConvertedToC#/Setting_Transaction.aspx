﻿<%@ Page  Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPage.Master" CodeBehind="Setting_Transaction.aspx.cs" Inherits="VB.Setting_Transaction" %>

<%@ Register src="WUC_DateReporter.ascx" tagname="WUC_DateReporter" tagprefix="uc1" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<%@ Register src="PageNavigation.ascx" tagname="PageNavigation" tagprefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<asp:ToolkitScriptManager ID="toolkit1" runat="server"></asp:ToolkitScriptManager>

<asp:UpdatePanel ID="UpdatePanel1" runat="server">				           
<ContentTemplate>


<div class="container-fluid">
				<!-- BEGIN PAGE HEADER-->
				<div class="row-fluid" id="fixedPageHeader">
					<div class="span12"> 
						<!-- BEGIN PAGE TITLE & BREADCRUMB-->
						<h3 class="page-user-md">ตรวจสอบ Log การใช้งาน<font color="blue">(ScreenID : S-BAS-05)</font></h3>					
									
						<ul class="breadcrumb">
                            <li>
                            	<i class="icon-cogs"></i><a href="javascript:;">จัดการระบบ/ข้อมูลพื้นฐาน</a><i class="icon-angle-right"></i>
                            </li>
                        	<li><i class="icon-time"></i> <a href="javascript:;">ตรวจสอบ Log การใช้งาน</a></li>
                        	
                            <uc1:WUC_DateReporter ID="WUC_DateReporter1" runat="server" />
                        </ul>
						<!-- END PAGE TITLE & BREADCRUMB-->
									        
				     </div>

				
			    </div>
		        <!-- END PAGE HEADER-->
				
				<asp:Panel ID="pnlList" runat="server" DefaultButton="btnSearch">

               

                <div class="row-fluid">
					
					<div class="span12">
						<!-- BEGIN SAMPLE TABLE PORTLET-->								                           
							  <div class="row-fluid form-horizontal">
	                               <div class="span4 ">
									    <div class="control-group">
								            <label class="control-label"><i class="icon-user"></i> ผู้ใช้</label>
								            <div class="controls">
									            <asp:TextBox  ID="txtFilter" OnTextChanged="Search_Changed" runat="server" AutoPostBack="true" CssClass="m-wrap large" placeholder="ชื่อ/เลขประจำตัว"></asp:TextBox> 														       
								            </div>
							            </div>
								    </div>
								    <div class="span8 ">
									    <div class="control-group">
								            <label class="control-label"><i class="icon-time"></i> ช่วงเวลา</label>
								            <div class="controls">
									            <asp:TextBox  ID="txtStart" OnTextChanged="Search_Changed" runat="server" AutoPostBack="true" CssClass="m-wrap small" placeholder="dd-MMM-yyyy" style="text-align:center;"></asp:TextBox> 														       
									            <asp:CalendarExtender ID="txtStart_cal" runat="server" PopupPosition="BottomRight" TargetControlID="txtStart" BehaviorID="txtStart" Format="dd-MMM-yyyy" />
									            <span style="font-size:14px; position:relative; top:10px; margin-left:20px; margin-right:20px;">ถึง</span>
									            <asp:TextBox  ID="txtEnd" OnTextChanged="Search_Changed" runat="server" AutoPostBack="true" CssClass="m-wrap small" placeholder="dd-MMM-yyyy" style="text-align:center;"></asp:TextBox> 
									            <asp:CalendarExtender ID="txtEnd_cal" runat="server" PopupPosition="BottomRight" TargetControlID="txtEnd" BehaviorID="txtEnd" Format="dd-MMM-yyyy" />
								            </div>
							            </div>
								    </div>	
								    <asp:Button ID="btnSearch" OnClick="Search_Changed" runat="server" Text="" style="display:none;" />
	                            </div>    
	                            
						    <div class="portlet-body no-more-tables" style="width:auto  ">
						        <asp:Label ID="lblCountList" runat="server" Font-Size="14px" Font-Bold="true"></asp:Label>
								<table class="table table-bordered  table-full-width table-advance dataTable no-more-tables table-hover">
									<thead style="text-align:center;">                                        
										<tr>											
											<th><i class="icon-user"></i> ผู้ใช้งาน</th>
											<th style="text-align:center;"><i class="icon-bookmark"></i> ตำแหน่ง</th>
											<th style="text-align:center;"><i class="icon-eye-open"></i> กิจกรรม</th>
                                            <th style="text-align:center;"><i class="icon-time"></i> เวลา</th>
										</tr>
									</thead>
									<tbody style="text-align:center; background-color:White ;">
										<asp:Repeater ID="rptList" OnItemDataBound="rptList_ItemDataBound" runat="server">
										    <ItemTemplate>
    										        <tr>
    										            <td data-title="ผู้ใช้งาน"><asp:Label ID="lblPSN" runat="server"></asp:Label></td>
    										            <td data-title="ตำแหน่ง" style="text-align:center;"><asp:Label ID="lblPos" runat="server"></asp:Label></td>
                                                        <td data-title="กิจกรรม" style="text-align:center;"><asp:Label ID="lblAction" runat="server"></asp:Label></td>
                                                        <td data-title="เวลา" style="text-align:center;"><asp:Label ID="lblTime" runat="server"></asp:Label></td>
                                                    </tr>
										    </ItemTemplate>
										</asp:Repeater>
										
										

                                    </tbody>
                                </table>
                                <asp:PageNavigation ID="Pager" OnPageChanging="Pager_PageChanging" MaximunPageCount="5" PageSize="20" runat="server" />
                                						

							</div>
						
						<!-- END SAMPLE TABLE PORTLET-->
						
					</div>
                </div>
               </asp:Panel>
               
							 
</ContentTemplate>				                
</asp:UpdatePanel>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ScriptPlaceHolder" Runat="Server">
</asp:Content>


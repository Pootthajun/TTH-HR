﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPage.Master" CodeBehind="CP_View_Personal_Self.aspx.cs" Inherits="VB.CP_View_Personal_Self" %>

<%@ Register src="WUC_DateReporter.ascx" tagname="WUC_DateReporter" tagprefix="uc1" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<%@ Register src="PageNavigation.ascx" tagname="PageNavigation" tagprefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <asp:ToolkitScriptManager ID="toolkit1" runat="server"></asp:ToolkitScriptManager>
<asp:UpdatePanel ID="UpdatePanel1" runat="server">
<ContentTemplate>

<div class="container-fluid">
				<!-- BEGIN PAGE HEADER-->
				<div class="row-fluid" id="fixedPageHeader">
					<div class="span12"> 
						<!-- BEGIN PAGE TITLE & BREADCRUMB-->
						<h3 class="page-user-md">ประวัติส่วนตัว </h3>
						<ul class="breadcrumb">
                            
                        	<li>
                        	    <i class="icon-signal"></i> <a href="javascript:;">เส้นทางก้าวหน้าในสายอาชีพ</a><i class="icon-angle-right"></i>
                        	</li>
                            <li>
                        	    <i class="icon-user"></i> <a href="javascript:;">ข้อมูลของคุณ</a><i class="icon-angle-right"></i>
                        	</li>
                        	<li>
                        	    <i class="icon-file-text"></i> <a href="javascript:;">ประวัติส่วนตัว</a>
                        	</li>
                        	<uc1:WUC_DateReporter ID="WUC_DateReporter1" runat="server" />
                        </ul>
						<!-- END PAGE TITLE & BREADCRUMB-->
									        
				     </div>

				
			    </div>
		        <!-- END PAGE HEADER-->
				
				<asp:Panel ID="PnlShow" runat="server" Visible="True" DefaultButton ="btnSearch_Show">
                <%--ข้อมูล--%>
                    <div class="row-fluid">
                        <div class="span10">
                                    
                                    <div class="row-fluid form-horizontal">					            
                                     <div class="row-fluid" style=" margin-bottom :10px;">
					                        <div class="span6 ">
					                            <div class="control-group">
								                    <label class="control-label">พนักงาน :</label>
								                    <div class="controls" >									                    
                                                        <asp:Label ID="lblName" runat="server" CssClass="text bold" Height="20px" Text ="XXX"></asp:Label>
								                    </div>
							                    </div>
					                        </div>
					                        <div class="span6 ">
					                            <div class="control-group">
								                    <label class="control-label">หน่วยงาน :</label>
								                    <div class="controls">
									                    <asp:Label ID="lblDept" runat="server" CssClass="text bold" Height="20px" Text ="XXX"></asp:Label>
								                    </div>
							                    </div>
					                        </div>
					                       
					                 </div>
    			                     <div class="row-fluid" style="border-bottom:1px solid #EEEEEE; margin-bottom :10px;">
					                        <div class="span6 ">
					                            <div class="control-group">
								                    <label class="control-label" >ปัจจุบันตำแหน่ง :</label>
								                    <div class="controls">									                    
                                                        <asp:Label ID="lblPos" runat="server" CssClass="text bold" Height="20px" Text ="XXX"></asp:Label>
								                    </div>
							                    </div>
					                        </div>
					                        <div class="span4 ">
					                            <div class="control-group">
								                    <label class="control-label">ระดับ :</label>
								                    <div class="controls">
									                    <asp:Label ID="lblClass" runat="server" CssClass="text bold" Height="20px" Text ="XXX"></asp:Label>
								                    </div>
							                    </div>
					                        </div>
					                       
					                 </div>
							
							        </div>  
                      
                        </div>
                        <div class="span2">
                            <div class="row-fluid" style=" margin-bottom :3px;">
                                
                        		    <div class="control-group"></div>        
								    <div class="control-group">
									    <label class="control-label"></label>
									    <div class="controls">
                                            
									    </div>
								    </div>
                                
                            </div>
                            <div class="row-fluid" style=" margin-bottom :3px;">
                                
                        		    <div class="control-group"></div>        
								    <div class="control-group">
									    <label class="control-label"></label>
									    <div class="controls">
                                            <h4 id="Box_PUNISH_Yes" runat ="server" visible ="false"   class="page-user-md" style=" color:white ; background-color:#FF6347; text-align:center; padding:15px;">
					                        &nbsp;<asp:Label ID="lblBoxPUNISH_Yes" runat="server" CssClass="text bold" Font-Size ="16px" Height="30px" Text ="XXX"></asp:Label>  
					                        </h4>                                                            
                                            <h4 id="Box_PUNISH_No" runat ="server" visible ="false"  class="page-user-md" style=" color:white ; background-color:#32CD32; text-align:center; padding:15px;">
					                        &nbsp;<asp:Label ID="lblBoxPUNISH_No" runat="server" CssClass="text bold" Font-Size ="16px" Height="30px" Text ="XXX" ></asp:Label>  
					                        </h4> 
									    </div>
								    </div>
                                
                            </div> 
                        </div>
                    </div>
                    <div class="row-fluid">
					<div class="span6">
						<!-- BEGIN SAMPLE TABLE PORTLET-->
						<div class="portlet box blue">
							<div class="portlet-title">
								<div class="caption"> ผลการประเมิน KPI/COMP ย้อนหลัง 3 รอบ</div>
								
							</div>
							<div class="portlet-body">
                            <asp:Label ID="lblHeadAss" runat="server" Font-Size="14px" Font-Bold="true"></asp:Label>      
                            <div  id="table_Ass" runat ="server" >   
                                <table class="table table-full-width table-advance dataTable no-more-tables table-hover">
									<thead>
										<tr>
											<th ><i class="icon-retweet"></i> รอบการประเมิน</th>
											<th  style =" text-align :center;"><i class="icon-check"></i> KPI </th>
											<th  style =" text-align :center;"><i class="icon-check"></i> Competency </th>
                                            <th  style =" text-align :center;"><i class="icon-trophy"></i> รวม </th>
										</tr>
									</thead>
									<tbody>
                                        <asp:Repeater ID="rptAss"  OnItemDataBound="rptAss_ItemDataBound" runat="server">
									         <ItemTemplate>
                                                <tr>                                        
											        <td ><span><asp:Label ID="lblYear" runat="server" Text="XX"></asp:Label></span></td>
											        <td  style =" text-align :center;"><span><asp:Label ID="lblKPI_Result" runat="server" Text="XX"></asp:Label></span></td>
											        <td  style =" text-align :center;"><span><asp:Label ID="lblCOMP_Result" runat="server" Text="XX"></asp:Label></span></td>
										            <td  style =" text-align :center;"><span><asp:Label ID="lblSUM_Result" runat="server" Text="XX"></asp:Label></span></td>
                                                </tr>
                                             </ItemTemplate>
                                        </asp:Repeater> 									
									</tbody>
                                    <tfoot >
                                        <tr>                                        
											<td ></td>
											<td  style =" text-align :center;"><span><b>ผลรวมเฉลี่ย</b></span></td>
											<td  style =" text-align :center;"></td>
										    <td  style =" text-align :center;"><span><asp:Label ID="lblSUM_AVG" runat="server" Text="XX"></asp:Label></span></td>
                                        </tr>	
                                    </tfoot>
								</table>
                            </div>
							</div>
						</div>
						<!-- END SAMPLE TABLE PORTLET-->
						<!-- BEGIN SAMPLE TABLE PORTLET-->
						<div class="portlet box green">
							<div class="portlet-title">
								<div class="caption"> หลักสูตรการฝึกอบรมที่เคยเข้าร่วม</div>
								
							</div>
							<div class="portlet-body">
                                            <div class="row-fluid">
												<div class="form-horizontal">	
												   <div class="row-fluid form-horizontal" id="Search_Course" runat ="server" visible ="false"  >
                                                        <div class="span5 ">
														    <div class="control-group">
													            <label class="control-label"><i class="icon-book"></i> ชื่อหลักสูตร</label>
													            <div class="controls">
														            <asp:TextBox ID="txt_Search_Course"   OnTextChanged="btnSearch_Show_Click" runat="server" AutoPostBack="true" CssClass="m-wrap medium" placeholder="ค้นหาจากชื่อหลักสูตร"></asp:TextBox>
													            </div>
												            </div>
													    </div>
                                                        <div class="span6 ">
														    
													    </div>
                                                    </div>											

                                                    <div class="row-fluid">

                                                    <asp:Label ID="lblCountCourse" runat="server" Font-Size="14px" Font-Bold="true"></asp:Label>
                                                    <div  id="table_Course" runat ="server" >                                                                
                                					<table class="table table-full-width table-advance dataTable no-more-tables table-hover">
									                    <thead>
										                    <tr>
											                    <th><i class="icon-book"></i> หลักสูตร</th>
											                    <th style="text-align:center; width :90px;"><i class="icon-time"></i> จำนวน(วัน)</th>
										                    </tr>
                                                         </thead>
									                    <tbody>
                                                        <asp:Repeater ID="rptCourse"  OnItemDataBound="rptCourse_ItemDataBound" runat="server">
									                        <ItemTemplate>
                                                            <tr>                                        
											                    <td data-title="หลักสูตร"><asp:Label ID="lblCourse" runat="server" Text ="XX"></asp:Label></td>											                    
											                    <td style="text-align:center;">
											                        <asp:Label ID="lblDateFrom" runat="server" Text="XX"></asp:Label>
                                                                    
                                                                    <asp:Label ID="lblDateTo" runat="server" Text="XX"></asp:Label>
										                        </td>
										                    </tr>
									                       </ItemTemplate> 
                                                        </asp:Repeater> 

									                    </tbody>
								                    </table>
                                                    </div> 
                                                    <asp:PageNavigation ID="Page_Course"  OnPageChanging="Page_Course_PageChanging"  MaximunPageCount="5" PageSize="10" runat="server" />

													        </div>
												       		    											        												        
											   </div>
											</div>

							</div>
						</div>
						<!-- END SAMPLE TABLE PORTLET-->
                        <!-- BEGIN SAMPLE TABLE PORTLET-->
						<div class="portlet box yellow ">
							<div class="portlet-title">
								<div class="caption"> ตำแหน่งที่เคยครอง</div>
								
							</div>
							<div class="portlet-body">
                                            <div class="row-fluid">
												<div class="form-horizontal">	
                                                 <div class="row-fluid form-horizontal"  id="Search_HPosition" runat ="server" visible ="false">
                                                        <div class="span5 ">
														    <div class="control-group">
													            <label class="control-label"><i class="icon-briefcase"></i> ชื่อตำแหน่ง</label>
													            <div class="controls">
														            <asp:TextBox ID="txtSearch_HPostion"  OnTextChanged="btnSearch_Show_Click" runat="server" AutoPostBack="true" CssClass="m-wrap medium" placeholder="ค้นหาจากชื่อวิชา"></asp:TextBox>
													            </div>
												            </div>
													    </div>
                                                        <div class="span6 ">
														    
													    </div>
                                                    </div>	
                                                    <div class="row-fluid">
                                                            <asp:Label ID="lblCount_HPosition" runat="server" Font-Size="14px" Font-Bold="true"></asp:Label> 
                                                            <div  id="table_HPosition" runat ="server" >                                                            
								                            <table class="table table-full-width table-advance dataTable no-more-tables table-hover">
									                            <thead>
										                            <tr>
											                            <th  style="text-align:left;"><i class="icon-briefcase"></i> ตำแหน่ง</th>
											                            <th style="text-align:center;" width="180px"><i class="icon-time"></i> ช่วงเวลาที่ครองตำแหน่ง</th>
										                            </tr>
									                            </thead>
									                            <tbody>
										                            <asp:Repeater ID="rptHPosition" OnItemDataBound="rptHPosition_ItemDataBound"  runat ="server" >
                                                                        <ItemTemplate >
    									                                    <tr>                                        
											                                    <td><asp:Label ID="lblPosition" runat="server"></asp:Label></td>											            
											                                    <td style="text-align:center;">
                                                                                    &nbsp;<asp:Label ID="lblDateTo" runat="server"  ></asp:Label>  <asp:Label ID="lblDateFrom" runat="server" ></asp:Label>
                                                                                </td>
										                                    </tr> 
                                                                         </ItemTemplate>
                                                                    </asp:Repeater> 
									                            </tbody>
								                            </table>
                                                            </div> 
                                                            <asp:PageNavigation ID="Page_HPosition" OnPageChanging="Page_HPosition_PageChanging"   MaximunPageCount="5" PageSize="10" runat="server" />
													        </div>
												        </div>
                                                    </div>	
							</div>
						</div>
						<!-- END SAMPLE TABLE PORTLET-->

					</div>
					<div class="span6">
						<!-- BEGIN BORDERED TABLE PORTLET-->
						<div class="portlet box yellow">
							<div class="portlet-title">
								<div class="caption"> จำนวนวันลา(กิจ/ป่วย) 3 ปีงบประมาณย้อนหลัง</div>
								
							</div>
							<div class="portlet-body">
								<table class="table table-full-width table-advance dataTable no-more-tables table-hover">
									<thead>
										<tr>
											<th></th>
											<th><i class="icon-reorder"></i> ประเภท</th>
											<th  style =" text-align :center;"><i class="icon-time"></i> จำนวน  ( วัน )</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td  style =" text-align :center;"><i class="icon-medkit"></i></td>
											<td>ลา(กิจ/ป่วย)</td>
                                            <td  style =" text-align :center;"><span><asp:Label ID="lblL_HEALTH" runat="server" Text="XX"></asp:Label></span></td>
										</tr>
										<%--<tr>
											<td  style =" text-align :center;"><i class="icon-envelope"></i></td>
											<td>ลากิจ</td>
                                            <td  style =" text-align :center;"><span><asp:Label ID="lblL_ACTIVITY" runat="server" Text="XX"></asp:Label></span></td>
										</tr>
										<tr>
											<td  style =" text-align :center;"><i class="icon-time"></i></td>
											<td>มาสาย</td>
                                            <td  style =" text-align :center;"><span><asp:Label ID="lblL_LATE" runat="server" Text="XX"></asp:Label></span></td>
										</tr>
										<tr>
											<td  style =" text-align :center;"><i class="icon-ambulance"></i></td>
											<td>ลาคลอด</td>
                                            <td  style =" text-align :center;"><span><asp:Label ID="lblL_BIRTH" runat="server" Text="XX"></asp:Label></span></td>
										</tr>
                                        <tr>
											<td></td>
											<td  style =" text-align :center;"><b>รวม</b></td>
                                            <td  style =" text-align :center;"><span><asp:Label ID="lblL_SUM" runat="server" Text="XX"></asp:Label></span></td>
										</tr>--%>
									</tbody>
								</table>
							</div>
						</div>
						<!-- END BORDERED TABLE PORTLET-->
						<!-- BEGIN BORDERED TABLE PORTLET-->
						<div class="portlet box purple">
							<div class="portlet-title">
								<div class="caption"> ผลการสอบวัดผลที่ผ่านมา</div>
								
							</div>
							<div class="portlet-body">
                                                    <div class="row-fluid">
												        <div class="form-horizontal">
                                                    <div class="row-fluid form-horizontal"  id="Search_Test" runat ="server" visible ="false">
                                                        <div class="span5 ">
														    <div class="control-group">
													            <label class="control-label"><i class="icon-book "></i> ชื่อวิชา</label>
													            <div class="controls">
														            <asp:TextBox ID="txtSearch_test"  OnTextChanged="btnSearch_Show_Click" runat="server" AutoPostBack="true" CssClass="m-wrap medium" placeholder="ค้นหาจากชื่อวิชา"></asp:TextBox>
													            </div>
												            </div>
													    </div>
                                                        <div class="span6 ">
														    
													    </div>
                                                    </div>	
                                                    <div class="row-fluid">
                                                        <asp:Label ID="lblCount_Test" runat="server" Font-Size="14px" Font-Bold="true"></asp:Label>      
                                                        <div  id="table_Test" runat ="server" >                                                    
								                        <table class="table table-full-width table-advance dataTable no-more-tables table-hover">
									                        <thead>
										                        <tr>
											                        <th  style="text-align:left;"><i class="icon-book"></i> ชื่อวิชา</th>
                                                                    <th  style="text-align:center;"><i class="icon-time"></i> วันที่</th>
                                                                    <th  style="text-align:center;"><i class="icon-trophy"></i> คะแนนเต็ม</th>
											                        <th  style="text-align:center;"><i class="icon-bolt"></i> เกณฑ์คะแนน</th>
                                                                    <th  style="text-align:center;"><i class=" icon-bar-chart "></i> ได้คะแนน</th>
										                        </tr>
									                        </thead>
									                        <tbody>
                                                                <asp:Repeater ID ="rptTest" OnItemDataBound="rptTest_ItemDataBound" runat ="server" >
                                                                    <ItemTemplate >			                                
    									                            <tr>                                        
											                            <td><asp:Label ID="lblSubject" runat="server" ></asp:Label></td>
                                                                        <td style="text-align:center;"><asp:Label ID="lblDate"  runat="server" ></asp:Label></td>
                                                                        <td style="text-align:center;"><asp:Label ID="lblPOINT" runat="server" ></asp:Label></td>
                                                                        <td style="text-align:center;"><asp:Label ID="lblPOINT_RULE" runat="server" ></asp:Label></td>
											                            <td style="text-align:center;"><asp:Label ID="lblScore" runat="server" ></asp:Label></td>
										                            </tr>
                                                                    </ItemTemplate>
                                                                </asp:Repeater>	 
									                        </tbody>
								                        </table>
                                                        </div> 
                                                        <asp:PageNavigation ID="Page_Test" OnPageChanging="Page_Test_PageChanging"  MaximunPageCount="5" PageSize="10" runat="server" />
													
                                                    </div>		    											        												        
										        </div>
									        </div>
							</div>
						</div>
						<!-- END BORDERED TABLE PORTLET-->
                        <!-- BEGIN SAMPLE TABLE PORTLET-->
						<div class="portlet box red">
							<div class="portlet-title">
								<div class="caption"> ตำแหน่งที่สามารถเข้ารับได้</div>
								
							</div>
							<div class="portlet-body">
                                            <div class="row-fluid">
												<div class="form-horizontal">	
                                                 <div class="row-fluid form-horizontal"  id="Div4" runat ="server" visible ="false">
                                                        <div class="span5 ">
														    <div class="control-group">
													            <label class="control-label"><i class="icon-briefcase"></i> ชื่อตำแหน่ง</label>
													            <div class="controls">
														            <asp:TextBox ID="txtSearch_Pos_Route"  OnTextChanged="btnSearch_Show_Click" runat="server" AutoPostBack="true" CssClass="m-wrap medium" placeholder="ค้นหาจากชื่อวิชา"></asp:TextBox>
													            </div>
												            </div>
													    </div>
                                                        <div class="span6 ">
														    
													    </div>
                                                    </div>	
                                                    <div class="row-fluid">
                                                            <asp:Label ID="lblCountPos_Route" runat="server" Font-Size="14px" Font-Bold="true"></asp:Label> 
                                                            <div  id="table_Pos_Route" runat ="server" >                                                           
								                            <table class="table table-full-width table-advance dataTable no-more-tables table-hover">
									                            <thead>
										                            <tr>
											                            <th  style="text-align:left;"><i class="icon-sitemap"></i> หน่วยงาน</th>
											                            <th style="text-align:left;"><i class="icon-briefcase"></i> ตำแหน่ง</th>
											                            <th style="text-align:center;"><i class="icon-bookmark"></i> ระดับ</th>
										                            </tr>
									                            </thead>
									                            <tbody>
										                            <asp:Repeater ID="rptPos_Route" OnItemDataBound="rptPos_Route_ItemDataBound" runat ="server" >
                                                                        <ItemTemplate >
    									                                    <tr>                                        
											                                    <td><asp:Label ID="lblDept" runat="server"></asp:Label></td>											            
											                                    <td >
                                                                                    <b><asp:Label ID="lblPosName" runat="server" ></asp:Label></b>
                                                                                    
                                                                                </td>
                                                                                <td style="text-align:center;">
                                                                                    <asp:Label ID="lblClass" runat="server" ></asp:Label>
                                                                                </td>
										                                    </tr> 
                                                                         </ItemTemplate>
                                                                    </asp:Repeater> 
									                            </tbody>
								                            </table>
                                                            </div> 
													        </div>
												        </div>
                                                    </div>	
							</div>
						</div>
						<!-- END SAMPLE TABLE PORTLET-->
<asp:Button ID="btnSearch_Show"  OnTextChanged="btnSearch_Show_Click" runat="server" Text="" style="display:none;" />
					</div>
				</div>

              </asp:Panel>
			    
			    
		               
				       
</div>
</ContentTemplate>           
</asp:UpdatePanel>					 
</asp:Content>


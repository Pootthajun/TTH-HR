﻿<%@ Page  Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPage.Master" CodeBehind="CP_Setting_CareerPath_Self.aspx.cs" Inherits="VB.CP_Setting_CareerPath_Self" %>

<%@ Register src="WUC_DateReporter.ascx" tagname="WUC_DateReporter" tagprefix="uc1" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<%@ Register src="PageNavigation.ascx" tagname="PageNavigation" tagprefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <link href="css/flow_container.css" type="text/css" rel="stylesheet" />
    <link href="assets/css/pages/pricing-tables.css" rel="stylesheet" type="text/css">
    <link href="assets/plugins/glyphicons/css/glyphicons.css" rel="stylesheet">
    <link href="assets/plugins/fancybox/source/jquery.fancybox.css" rel="stylesheet">
    <script src="assets/plugins/fancybox/source/jquery.fancybox.pack.js"></script>   
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:ToolkitScriptManager ID="toolkit1" runat="server"></asp:ToolkitScriptManager>
<asp:UpdatePanel ID="UpdatePanel1" runat="server">
<ContentTemplate>
<div class="container-fluid">
                <!-- BEGIN PAGE HEADER-->
                <div class="row-fluid" id="fixedPageHeader">
					<div class="span12"> 
						<!-- BEGIN PAGE TITLE & BREADCRUMB-->
						<h3>กำหนดเส้นทางความก้าวหน้า</h3>					
						
						<ul class="breadcrumb">
                            <li>
                            	<i class="icon-user"></i> <a href="javascript:;">เส้นทางก้าวหน้าในสายอาชีพ</a><i class="icon-angle-right"></i>
                            </li>
                            <li>
                        	    <i class="icon-user"></i> <a href="javascript:;">ข้อมูลของคุณ</a><i class="icon-angle-right"></i>
                        	</li>
                        	<li>
                        	    <i class="icon-check"></i> <a href="javascript:;">กำหนดเส้นทางความก้าวหน้า</a>
                        	</li>                        	
                        	
                            <uc1:WUC_DateReporter ID="WUC_DateReporter1" runat="server" />
                        </ul>
						<!-- END PAGE TITLE & BREADCRUMB-->
									        
				     </div>			
			    </div>
		        <!-- END PAGE HEADER-->
		        

                <asp:Panel ID="pnlEdit" runat="server" Visible="True">  
                         
				                <!-- BEGIN FORM-->
                			    <div class="row-fluid" id="div1" runat="server" Visible="True">
                                    <div class="span12"></div>
                                    <div class="span12">
                                    <div class="portlet">
				                    <div class="portlet-body ">
                                    <div class="row-fluid" style="font-size:16px; font-weight:bold; text-align:center; text-decoration:underline;">
											    กำหนดเส้นทางความก้าวหน้าของตำแหน่ง
                                                <asp:Label ID="lblTMP" runat="server" style=" display :none ;"></asp:Label>
									</div>
											    <div class="row-fluid" style=" font-size:14px; font-weight:bold; padding-bottom:20px; padding-top:2px; border-bottom:0px solid #eeeeee;">
										           
                                                        <table border="0" cellspacing="0" cellpadding="0"  >
                                                              <tr>
                                                                  <td align="center" valign="top" style =" margin-top :50px;">
                                                                      
                                                                                      ตำแหน่งปัจจุบัน &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src="images/Next.png" alt="" style="height:50px;" />
                                                                                      <br /><br />         
                                                                      <table border="0" cellpadding="0" cellspacing="0" class="flow_item_disable" width="200"  >
                                                                          <tr>
                                                                              <td align="center" width="30">
                                                                                  <img src="images/pos.png" alt="" width="16" height="16" />
                                                                              </td>
                                                                              <td>
                                                                                  <asp:Label ID="lblCurrentPos" runat="server"></asp:Label>
                                                                              </td>
                                                                             
                                                                          </tr>

                                                                      </table>
                                                                      
                                                                  </td>
                                                                  
                                                                  <asp:Repeater  ID="rptAddPos" runat="server">
                                                                     <ItemTemplate>
                                                                          <td id="Td1"  align="center" runat ="server"  style =" width:500px; vertical-align :top ; "     >
                                                                                    <asp:Panel ID="pnl_Goal" runat ="server"     >
                                                                                                
                                                                                      เป้าหมายขั้นที่ <asp:Label ID="lblNO" runat="server"></asp:Label>&nbsp;&nbsp;
                                                                                                <asp:LinkButton ID="lnkEdit_POS" runat ="server" style =" top :-9px;"   class="  glyphicons no-js edit  " CommandName ="lnk_add_POS_NO"><i></i></asp:LinkButton>
                                                                                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src="images/Next.png" alt="" style="height:50px;" />
                                                                                      <br /><br />         
                                                                                      <table border="0"   cellspacing="0" style =" width :150px;"   >
                                                                                  
                                                                                          <tr style=" margin:0px;">

                                                                                              <td style=" margin-left :10px;">
                                                                                                  <div style="padding: 0px; width:auto; height: 5px; margin-top :0px; margin-bottom :0px; top: 0px; left: 10px;" class="fancybox-skin">
                                                                                                  <div class="fancybox-outer">
                                                                                                    <%--<div class="row-fluid"></div>--%>
					                                                                                    <div class="span12">
						                                                                                    <!-- BEGIN SAMPLE TABLE PORTLET DodgerBlue-->
						                                                                                    <div class="portlet box blue">
							                                                                                    <div class="portlet-title" ">
								                                                                                    <div class="caption">
                                                                                                                    <%--<asp:LinkButton ID="L" runat ="server"  CommandName="lnkCurrentGoal" > --%>                                                                                         
                                                                                                                      <table border="0"  cellpadding="0" cellspacing="0" style=" margin:0px;" class="flow_item"  >
                                                                                                                          <tr>
                                                                                                                              <td align="center" width="20">
                                                                                                                                  <img src="images/pos.png" width="16" height="16" />
                                                                                                                              </td>
                                                                                                                              <td width="150">
                                                                                                                                  <asp:Label ID="lblPosGoal" runat="server"></asp:Label>
                                                                                                                              </td>
                                                                                                                          </tr>
                                                                                                                      </table> 
                                                                                                                    <%--</asp:LinkButton>--%>

                                                                                                                    </div>
								                                                                                    <div class="tools" style ="margin-top:1px;" >
									                                                                                    <a href="javascript:;" title ="เพิ่มเติม" style =" vertical-align : top ;" class="expand"></a>
								                                                                                    </div>
                                                                                                                     
							                                                                                    </div>
                                                                                                                <div style="display: none;" class="portlet-body hide"  >
								                                                                                    <table class="table table-hover" >
									                                                                            
									                                                                                    <tbody>
										                                                                                    <tr>
											                                                                                    <ul class="pricing-content unstyled" style =" width :300px;">
												                                                                                    <li>
                                                                                                                                        <asp:Panel ID="icon_Ass_green" runat ="server" CssClass =" inline "><i id="icon_Ass_KPI" runat ="server"  class="icon-check" style =" color :Green ;"></i></asp:Panel>
                                                                                                                                        <asp:Panel ID="icon_Ass_Red" runat ="server" CssClass =" inline " ><i id="i1" runat ="server"  class="icon-check" style =" color :Red ;"></i></asp:Panel> 
                                                                                                                                        ผลการประเมิน KPI/COMP ย้อนหลัง 3 รอบ  <b><asp:Label ID="lblAss_MIN_Score_Property" ForeColor ="Black"  runat="server" Text ="XXX"></asp:Label><asp:Label ID="lblAss_Min_Score" ForeColor ="Black"  runat="server" Text ="XXX"></asp:Label></b>
                                                                                                                                    
                                                                                                                                    </li>
												                                                                                    <li>
                                                                                                                                        <asp:Panel ID="icon_Leave_green" runat ="server" CssClass =" inline "><i id="i2" runat ="server"  class="icon-ambulance" style =" color :Green ;"></i></asp:Panel>
                                                                                                                                        <asp:Panel ID="icon_Leave_Red" runat ="server" CssClass =" inline " ><i id="i3" runat ="server"  class="icon-ambulance" style =" color :Red ;"></i></asp:Panel> 
                                                                                                                                        จำนวนวันลา(กิจ/ป่วย) 3 ปีงบประมาณย้อนหลัง <br /> <b><asp:Label ID="lblLeave_Property" ForeColor ="Black"  runat="server" Text ="XXX"></asp:Label><asp:Label ID="lblLeave" runat="server" Text ="XXX"></asp:Label></b> 
                                                                                                                                    </li>
												                                                                                    <li>
                                                                                                                                        <%--<i class="icon-book"></i>--%>
                                                                                                                                        <asp:Panel ID="icon_Course_green" runat ="server" CssClass =" inline "><i id="i4" runat ="server"  class="icon-book" style =" color :Green ;"></i></asp:Panel>
                                                                                                                                        <asp:Panel ID="icon_Course_Red" runat ="server" CssClass =" inline " ><i id="i5" runat ="server"  class="icon-book" style =" color :Red ;"></i></asp:Panel>  
												                                                                                        ผ่านการฝึกอบรม <b><asp:Label ID="lblCount_Course" ForeColor ="Black"  runat="server" Text ="XXX"></asp:Label></b> หลักสูตร
												                                                                                        <ul>
                                                                                                                                            <asp:Repeater ID="rptCourse" runat="server">
								                                                                                                                <ItemTemplate>	
												                                                                                                    <li>
                                                                                                                                                    <asp:Label ID="lblCourse" runat="server" ForeColor ="Black"  Text ="XXX"></asp:Label><asp:Label ID="lblCourse_Status" runat="server" Text ="XXX"></asp:Label>
												                                                                                                    </li>
                                                                                                                                                </ItemTemplate> 
                                                                                                                                            </asp:Repeater> 
												                                                                                        </ul>    
												                                                                                    </li>
												                                                                                    <li>
                                                                                                                                     <asp:Panel ID="icon_Test_green" runat ="server" CssClass =" inline "><i id="i6" runat ="server"  class="icon-thumbs-up" style =" color :Green ;"></i></asp:Panel>
                                                                                                                                     <asp:Panel ID="icon_Test_Red" runat ="server" CssClass =" inline " ><i id="i7" runat ="server"  class="icon-thumbs-up" style =" color :Red ;"></i></asp:Panel>  
												                                                                                        
                                                                                                                                    เกณฑ์การสอบวัดผล <b><asp:Label ID="lblCount_Test" ForeColor ="Black"  runat="server" Text ="XXX"></asp:Label></b> วิชา
												                                                                                        <ul>
                                                                                                                                            <asp:Repeater ID="rptTest" runat="server" >
								                                                                                                                <ItemTemplate>
												                                                                                                    <li><asp:Label ID="lblTest" ForeColor ="Black"  runat="server" Text ="XXX"></asp:Label> <b><asp:Label ID="lblMin_Score"   runat="server" Text ="XXX"></asp:Label></b> </li>
												                                                                                                </ItemTemplate> 
                                                                                                                                            </asp:Repeater> 
												                                                                                        </ul>  
												                                                                                    </li>
												                                                                                    <li>
                                                                                                                                        <asp:Panel ID="icon_Punis_green" runat ="server" CssClass =" inline "><i id="i8" runat ="server"  class="icon-legal" style =" color :Green ;"></i></asp:Panel>
                                                                                                                                        <asp:Panel ID="icon_Punis_Red" runat ="server" CssClass =" inline " ><i id="i9" runat ="server"  class="icon-legal" style =" color :Red ;"></i></asp:Panel>  
												                                                                                        <asp:Label ID="lblPunishment" runat="server" Text ="XXX"></asp:Label>											
												                                                                                    </li>												
											                                                                                    </ul>
										                                                                                    <%--</div>--%>
                                                                                                                    
                                                                                                                            </tr>
										                                                                            
									                                                                                    </tbody>
								                                                                                    </table>
                                                                                                                    <div class="scroller-footer"  style=" margin-top :-25px;">
										                                                                                <div class="pull-right">
                                                                                                                            <a  target ="_blank" id="lnkL" runat ="server" style =" display :none ;"  > ดูรายละเอียดทั้งหมด &nbsp;&nbsp;<i class="m-icon-swapright m-icon-gray"></i> &nbsp;</a>
                                                                                                                            <asp:Button ID="btnPosCompare" runat="server" style =" display :none ;" CssClass="btn mini blue" CommandName="PosCompare" Text="ดูเกณฑ์ของตนเอง" />
                                                                                                                         </div>
									                                                                                </div>
							                                                                                    </div>
						                                                                                    </div>
						                                                                                    <!-- END SAMPLE TABLE PORTLET-->
					                                                                                    </div>
				                                                                                    
                                                                                                        </div>
                                                                                                    <a title="ลบ" id="btnDeleteLeft_Close" runat ="server"  class="fancybox-item fancybox-close" ></a>
                                                                                                  </div>
                                                                                       
                                                                                              </td>
                                                                                              <td  id="td" runat ="server"   style=" margin:0px; display :none ;" >
                                                                                                  <asp:Button ID="btnDelete" runat="server" CommandName="Delete" 
                                                                                                  CssClass="btn mini  white "     Text="ลบ"  />
                                                                                                  <asp:ConfirmButtonExtender ID="btnDeleteLeft_Confirm" runat="server"  ConfirmText="ลบตำแหน่งที่เลือก ?" TargetControlID="btnDelete"></asp:ConfirmButtonExtender>
                                                                                              </td>
                                                                                          </tr>
                                                                                  
                                                                                      </table>
                                                                                </asp:Panel>
                                                                                <asp:Panel ID="pnl_Add" runat ="server"  >
                                                               	                   <%-- เพิ่มเป้าหมายที่ <asp:Label ID="lblCareerPath_NO" runat="server"></asp:Label>
                                                                                    <asp:LinkButton ID="btnAdd_POS_NO" runat ="server" style =" top :-9px;"   class="  glyphicons no-js cogwheel " CommandName ="lnk_add_POS_NO"><i></i></asp:LinkButton>--%>
                                                                                    <table width="200" border="0" cellspacing="0" cellpadding="0" >
                                                                                      <tr>
                                                                                        <td width="30" align="center"><img src="images/pos.png" alt="" width="16" height="16" /></td>
                                                                                            <td>
                                                                                                 เพิ่มเป้าหมายที่ <asp:Label ID="lblCareerPath_NO" runat="server"></asp:Label>
                                                                                                 <asp:LinkButton ID="btnAdd_POS_NO" runat ="server" style =" top :-9px;"   class="  glyphicons no-js circle_plus " CommandName ="lnk_add_POS_NO"><i></i></asp:LinkButton>
                                                                                            </td>
                                                                                      </tr>
                                                                                    </table>
                                                                                </asp:Panel> 
                                                                          </td>

                                                                          
                                                                    </ItemTemplate>
                                                                  </asp:Repeater>
                                                                  <br /><br />
                                                              </tr>
                                                            
                                                        </table>

                                                
                                               											    
                                                </div>
									        
										    </div>
                                            													
									    </div>
                        <%-- เลือกตำแหน่งที่สามารถเข้าตำแหน่งนี้ได้   LEFT --%>
                    
		                <asp:Panel CssClass="modal" style="top:10%; position:fixed; width:auto;" id="ModalPost" runat="server" DefaultButton="btnSearchPos"  >
                        <div style="padding: 0px; width: auto;  margin-top :0px; margin-bottom :0px; top: 0px; left: 0px;" class="fancybox-skin">
                        <div class="fancybox-outer">
				            <div class="modal-header">										
					            <h3>ตำแหน่งที่สามารถเข้ารับได้ </h3>
				            </div>
				            <div class="modal-body">					            
					            <div class="row-fluid form-horizontal">
                                 <asp:Button ID="btnSearchPos" runat="server" Text="" style="display:none;" />   
					                 <div class="span12 ">
                                        <div class="span6 ">
											<div class="control-group">
												<label class="control-label"><i class="icon-sitemap"></i> หน่วยงาน/ตำแหน่ง</label>
												<div class="controls">
													<asp:TextBox ID="txtSearchPos" runat="server" AutoPostBack="true" CssClass="m-wrap medium" placeholder="ค้นหาจากชื่อหน่วยงาน/ตำแหน่ง"></asp:TextBox>
												</div>
											</div>
										</div>
								    </div>								       
					            </div>
					            <div class="portlet-body no-more-tables">
    								
								    <asp:Label ID="lblCountPos" runat="server" Font-Size="14px" Font-Bold="true"></asp:Label>
								    <table class="table table-full-width  no-more-tables table-hover">
									    <thead>
										    <tr>

                                                <th style="text-align:center;"><i class="icon-sitemap"></i> ฝ่าย</th>
											    <th style="text-align:center;"><i class="icon-sitemap"></i> หน่วยงาน</th>
											    <th style="text-align:center;"><i class="icon-bookmark"></i> ระดับ</th>
											    <th style="text-align:center;"><i class="icon-briefcase"></i> ตำแหน่ง</th>
											    <th style="text-align:center;"><i class="icon-bookmark"></i> ตำแหน่งการบริหาร</th>											
											    <%--<th style="text-align:center;"><i class="icon-user"></i> ผู้ครองตำแหน่ง</th>
											    <th style="text-align:center;"><i class="icon-caret-right"></i> รหัส</th>
											    <th style="text-align:center;"><i class="icon-book"></i> ชื่อวิชา</th>	--%>										    
											    <th style="text-align:center;"><i class="icon-check"></i> เลือก</th>
										    </tr>										    
									    </thead>
									    <tbody>
										    <asp:Repeater ID="rptPosDialog" runat="server">
									            <ItemTemplate>
    									            <tr>                                        
											            <td style="text-align:center;"><asp:Label ID="lblSector" runat="server"></asp:Label></td>
											            <td ><asp:Label ID="lblDept" runat="server"></asp:Label><asp:Label ID="lblTMPDept" runat="server"  style="display:none;"></asp:Label></td>		
                                                        <td style="text-align:center;"><asp:Label ID="lblClass" runat="server"></asp:Label></td>	
                                                        <td ><asp:Label ID="lblPosNo" runat="server"></asp:Label></td>	
                                                        <td ><asp:Label ID="lblMGR" runat="server"></asp:Label>
                                                            <asp:Label ID="lblPSN" runat="server" style="display:none;"></asp:Label>
                                                        </td>	
                                                        <%--<td ></td>--%>										           
											            <td data-title="ดำเนินการ" style="text-align:center;">
											                <asp:Button ID="btnPosSelect" runat="server" CssClass="btn mini blue" CommandName="Select" Text="เลือก" /> 
										                </td>
										            </tr>	
									            </ItemTemplate>
									        </asp:Repeater>					
									    </tbody>
								    </table>
    								
								    <asp:PageNavigation ID="Pager_Pos" MaximunPageCount="10" PageSize="10" runat="server" />
							    </div>				            
				            </div>
				            <%--<div class="modal-footer">
				                <asp:Button ID="btnClose_Pos" runat="server" CssClass="btn" Text="ปิด" />								
				            </div>--%>

</div>
                        <a title="ลบ" id="btnDeleteModel_Close" runat ="server"  class="fancybox-item fancybox-close" ></a>
                    </div>
			            </asp:Panel>
                        
								            
                        <%--BOX DETAIL compare Property   ข้อมูลสำหรับเข้ารับตำแหน่ง  --%>  
                        
		                <asp:Panel CssClass="modal" style="top:10%; position:fixed; width:400px; height :500px;" id="ModelCompareDetail" runat="server" DefaultButton="btnSearchPos"  >
                        <div style="padding: 0px; width: auto;  margin-top :0px; margin-bottom :0px; top: 0px; left: 0px;" class="fancybox-skin">
                        <div class="fancybox-outer">
				            <div class="modal-header">										
					           <b><asp:Label ID="lblHeaderPos" runat="server" style =" font-size:16px;" Text =""></asp:Label><br /><asp:Label ID="lblHeaderPos_Detail" style =" font-size:16px;" runat="server" Text =""></asp:Label></b>
				            </div>
				            <div class="modal-body" style =" margin-top :-30px;">	
							<div class="row-fluid" class="flow_container" style=" font-size:16px; font-weight:bold; padding-bottom:20px; padding-top:20px;  ">
                                <%--<table style =" width :auto ; height :400px ; "  border="3"   class="table table-bordered datatable ">
                                        
										<tr align ="center" >
                                            
											<td id="cel_KPI" runat ="server" align ="center" style ="  width: 300px; vertical-align :middle ; "  >
                                                <asp:Panel ID="pnl_Ass_MIN_Score" runat ="server"  >
                                                    <ul   class="pricing-content unstyled" style=" border-bottom :0px; color  :Black ;">
                                                        <li><i class="icon-check" style =" color :black;"></i> ผลการประเมิน KPI+Competency ย้อนหลัง 3 รอบ <b><asp:Label ID="lblAss_MIN_Score_Property" runat="server" Text ="XXX"></asp:Label><asp:Label ID="lblAss_MIN_Score" runat="server" Text ="XXX"></asp:Label></b></li>
											        </ul>
                                                    </asp:Panel>
                                                
                                            </td>
										</tr>
                                        <tr>
											<td id="cel_Leave" runat ="server"  style =" width: 300px; vertical-align :middle ;">
												<asp:Panel ID="pnl_Leave" runat ="server"  >
                                                    <ul class="pricing-content unstyled" style=" border-bottom :0px;">
                                                        <li><i class="icon-ambulance" style =" color :black;"></i> จำนวนวัน ป่วย สาย ลา ขาด ย้อนหลัง 3 ปี  <b><asp:Label ID="lblLeave_Property" runat="server" Text ="XXX"></asp:Label><asp:Label ID="lblLeave" runat="server" Text ="XXX"></asp:Label></b></li>
											        </ul>   
                                                </asp:Panel>
                                                
                                            </td>
										</tr>
										<tr>
											<td id="cel_Course" runat ="server"  style =" width: 300px">
                                                <asp:Panel ID="pnl_Course" runat ="server"  >
                                                    <ul class="pricing-content unstyled">
                                                    <li><i class="icon-book" style =" color :black;"></i> 
												        เกณฑ์ผ่านการฝึกอบรม <b><asp:Label ID="lblCount_Course" runat="server" Text ="XXX"></asp:Label></b> หลักสูตร
												        <ul>
                                                            <asp:Repeater ID="rptDetail_Course" runat="server">
								                                <ItemTemplate>	
												                    <li><asp:Label ID="lblCourse" runat="server" Text ="XXX"></asp:Label><asp:Label ID="lblCourse_Status" runat="server" Text ="XXX"></asp:Label></li>
												                </ItemTemplate> 
                                                            </asp:Repeater> 
												        </ul>    
												    </li>
                                                    </ul>
                                                </asp:Panel>
                                                
											</td>
                                                
										</tr>
										<tr>
											<td  id="cel_Test" runat ="server"  style =" width: 300px">
												<asp:Panel ID="pnl_Test" runat ="server"  >
                                                    <ul class="pricing-content unstyled">
                                                    <li><i class="icon-thumbs-up" style =" color :black;"></i> 
												        เกณฑ์การสอบวัดผล <b><asp:Label ID="lblCount_Test" runat="server" Text ="XXX"></asp:Label></b> วิชา
												        <ul>
                                                            <asp:Repeater ID="rptDetail_Test" runat="server">
								                                <ItemTemplate>	
												                    <li><asp:Label ID="lblTest" runat="server" Text ="XXX"></asp:Label><i class =" icon ipod"></i><asp:Label ID="lblMin_Score" runat="server" Text ="XXX"></asp:Label> </li>
												                </ItemTemplate> 
                                                            </asp:Repeater> 
												        </ul>    
												    </li>
                                                    </ul> 
                                                </asp:Panel>
                                                
											</td>
										</tr>
										<tr>
											<td id="cel_Punishment" runat ="server" align ="left"   style =" width: 300px">
												<asp:Panel ID="pnl_Punishment" runat ="server"  >
                                                    <ul class="pricing-content unstyled">
                                                        <li>
                                                            <i class="icon-legal" style =" color :black;"></i> <asp:Label ID="lblPunishment" runat="server" Text ="โทษทางวินัย"></asp:Label>											
												        </li>												
											        </ul>
                                                </asp:Panel>
                                                
											</td>
										</tr>
										<tr  >
                                            
											<td id="cel_btn"  runat ="server"  style =" width: 300px; cursor:pointer; padding:0px !important; height:10px !important; border-right:0px none;"  >
												<asp:Panel ID="pnl_btn" runat ="server"  >
                                                <b><h3 style =" font-size:16px; text-align :center "> คลิกเพื่อกำหนดเป้าหมาย </h3> <asp:Button ID="btnSetPath" runat="server" style="display:none;" CommandName="SelectPath" /></b>
                                                   <asp:Label ID="lblTMP_POS_SetPath" runat="server" Text ="" style="display:none;"></asp:Label> 
                                                </asp:Panel>
											</td>
										</tr>





									
								</table>--%>

                                

                                <%-----------------------------------------------------------------------%>
                                <br />
                                                <asp:Panel ID="Panel1" runat ="server"  >
                                                    <ul   class="pricing-content unstyled"  color  :Black ;">
                                                        <li><i id ="icon_Ass_KPI" runat ="server"  class="icon-check" ></i> ผลการประเมิน KPI/COMP ย้อนหลัง 3 รอบ <b><asp:Label ID="lblAss_MIN_Score_Property"  ForeColor ="Black"  runat="server" Text ="XXX"></asp:Label><asp:Label ID="lblAss_MIN_Score" runat="server" Text ="XXX"></asp:Label></b></li>
											        </ul>
                                                    </asp:Panel>
                                                <asp:Panel ID="Panel2" runat ="server"  >
                                                    <ul class="pricing-content unstyled" style=" border-bottom :0px;">
                                                        <li><i id ="icon_Leave" runat ="server" class="icon-ambulance" style =" color :black;"></i> จำนวนวันลา(กิจ/ป่วย) 3 ปีงบประมาณย้อนหลัง  <b><asp:Label ID="lblLeave_Property"  ForeColor ="Black"  runat="server" Text ="XXX"></asp:Label><asp:Label ID="lblLeave" runat="server" Text ="XXX"></asp:Label></b></li>
											        </ul>   
                                                </asp:Panel>
                                                <asp:Panel ID="Panel3" runat ="server"  >
                                                    <ul class="pricing-content unstyled">
                                                    <li><i id ="icon_Course" runat ="server" class="icon-book" style =" color :black;"></i> 
												        เกณฑ์ผ่านการฝึกอบรม <b><asp:Label ID="lblCount_Course"  ForeColor ="Black"  runat="server" Text ="XXX"></asp:Label></b> หลักสูตร
												        <ul>
                                                            <asp:Repeater ID="rptDetail_Course" runat="server">
								                                <ItemTemplate>	
												                    <li><asp:Label ID="lblCourse" ForeColor ="Black"  runat="server" Text ="XXX"></asp:Label><asp:Label ID="lblCourse_Status" runat="server" Text ="XXX"></asp:Label></li>
												                </ItemTemplate> 
                                                            </asp:Repeater> 
												        </ul>    
												    </li>
                                                    </ul>
                                                </asp:Panel>
												<asp:Panel ID="Panel4" runat ="server"  >
                                                    <ul class="pricing-content unstyled">
                                                    <li><i id ="icon_Test" runat ="server" class="icon-thumbs-up" style =" color :black;"></i> 
												        เกณฑ์การสอบวัดผล <b><asp:Label ID="lblCount_Test"  ForeColor ="Black"  runat="server" Text ="XXX"></asp:Label></b> วิชา
												        <ul>
                                                            <asp:Repeater ID="rptDetail_Test" runat="server">
								                                <ItemTemplate>	
												                    <li><asp:Label ID="lblTest" ForeColor ="Black"  runat="server" Text ="XXX"></asp:Label><i class =" icon ipod"></i><asp:Label ID="lblMin_Score" runat="server" Text ="XXX"></asp:Label> </li>
												                </ItemTemplate> 
                                                            </asp:Repeater> 
												        </ul>    
												    </li>
                                                    </ul> 
                                                </asp:Panel>
                                                <asp:Panel ID="Panel5" runat ="server"  >
                                                    <ul class="pricing-content unstyled">
                                                        <li>
                                                            <i id ="icon_Pushnish" runat ="server" class="icon-legal" style =" color :black;"></i> <asp:Label ID="lblPunishment" runat="server" Text ="โทษทางวินัย"></asp:Label>											
												        </li>												
											        </ul>
                                                </asp:Panel>
                                                <br />
                                                <asp:Panel ID="Panel6" runat ="server" style=" text-align : right  ;"  >
                                                    <asp:Button ID="btnSetPath" runat="server" CssClass =" btn purple " Text ="คลิกเพื่อกำหนดเป้าหมาย"  CommandName="SelectPath" />
                                                    <asp:Label ID="lblTMP_POS_SetPath" runat="server" Text ="" style="display:none;"></asp:Label> 
                                                </asp:Panel>
                                                
												


							</div>
				            </div>
                        </div>
                        <a title="ลบ" id="btnDeleteModelCompare_Close" runat ="server"  class="fancybox-item fancybox-close" ></a>
                    </div>
			            </asp:Panel>
                        
                             												
							    </div>
						</div>
                        
			   </asp:Panel>



     			 
</div>
</ContentTemplate>
</asp:UpdatePanel>
<script>
    jQuery(document).ready(function () {
        // initiate layout and plugins
        App.init();
        Gallery.init();
        $('.fancybox-video').fancybox({ type: 'iframe' });
    });
	</script>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ScriptPlaceHolder" Runat="Server">
</asp:Content>


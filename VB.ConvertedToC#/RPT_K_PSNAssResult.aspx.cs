﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data;
using System.Data.SqlClient;

namespace VB
{
    public partial class RPT_K_PSNAssResult : System.Web.UI.Page
    {

        HRBL BL = new HRBL();
        GenericLib GL = new GenericLib();

        Converter C = new Converter();
        public int R_Year
        {
            get
            {
                try
                {
                    return GL.CINT(GL.SplitString(ddlRound.Items[ddlRound.SelectedIndex].Value, "-")[0]);
                }
                catch (Exception ex)
                {
                    return 0;
                }
            }
        }

        public int R_Round
        {
            get
            {
                try
                {
                    return GL.CINT(GL.SplitString(ddlRound.Items[ddlRound.SelectedIndex].Value, "-")[1]);
                }
                catch (Exception ex)
                {
                    return 0;
                }
            }
        }


        protected int Selected_Level()
        {
          
                if (ddlDept.SelectedIndex > 0) return 1;
                if (ddlSector.SelectedIndex > 0) return 2;
                return 3;          
        }

        protected void Page_Load(object sender, System.EventArgs e)
        {
            if ((Session["USER_PSNL_NO"] == null))
            {
                ScriptManager.RegisterStartupScript(this.Page, typeof(string), "Alert", "showAlert('กรุณาลงชื่อเข้าใช้ระบบ');", true);
                Response.Redirect("Login.aspx");
                return;
            }

            if (!IsPostBack)
            {
                //----------------- ทำสองที่ Page Load กับ Update Status ---------------
                BL.BindDDlYearRound(ddlRound);
                ddlRound.Items.RemoveAt(0);
                BL.BindDDlSector(ddlSector);
                BL.BindDDlDepartment(ddlDept, ddlSector.SelectedValue);
                BindPersonalList();
            }
        }

        protected void btnSearch_Click(object sender, System.EventArgs e)
        {
            BindPersonalList();
        }

        protected void ddlSector_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            BL.BindDDlDepartment(ddlDept, ddlSector.SelectedValue);
        }

        private void BindPersonalList()
        {
            string SQL = "";

            SQL += " SELECT Header.R_Year,Header.R_Round,Header.PSNL_NO,Header.PSNL_Fullname,Header.PNPS_CLASS,SECTOR_CODE+SECTOR_NAME SECTOR," + "\n";
            SQL += " DEPT_CODE,DEPT_NAME,SECTOR_CODE,SECTOR_NAME,COUNT(KPI_No) AS NumKPI" + "\n";
            SQL += " ,SUM(CASE ISNULL(ISNULL(Detail.KPI_Answer_MGR,Detail.KPI_Answer_PSN),0) WHEN 1 THEN 1 ELSE 0 END) KPI1" + "\n";
            SQL += " ,SUM(CASE ISNULL(ISNULL(Detail.KPI_Answer_MGR,Detail.KPI_Answer_PSN),0) WHEN 2 THEN 1 ELSE 0 END) KPI2" + "\n";
            SQL += " ,SUM(CASE ISNULL(ISNULL(Detail.KPI_Answer_MGR,Detail.KPI_Answer_PSN),0) WHEN 3 THEN 1 ELSE 0 END) KPI3" + "\n";
            SQL += " ,SUM(CASE ISNULL(ISNULL(Detail.KPI_Answer_MGR,Detail.KPI_Answer_PSN),0) WHEN 4 THEN 1 ELSE 0 END) KPI4" + "\n";
            SQL += " ,SUM(CASE ISNULL(ISNULL(Detail.KPI_Answer_MGR,Detail.KPI_Answer_PSN),0) WHEN 5 THEN 1 ELSE 0 END) KPI5" + "\n";
            SQL += " --,SUM(Detail.KPI_Weight*ISNULL(ISNULL(Detail.KPI_Answer_MGR,Detail.KPI_Answer_PSN),0)) RESULT" + "\n";
            SQL += ",KPI_Status Ass_Status" + "\n";
            SQL += " FROM vw_RPT_KPI_Status Header" + "\n";
            SQL += " LEFT JOIN tb_HR_KPI_Detail Detail  ON Header.R_Year=Detail.R_Year " + "\n";
            SQL += " 									AND Header.R_Round=Detail.R_Round" + "\n";
            SQL += " 									AND Header.PSNL_NO=Detail.PSNL_NO" + "\n";
            SQL += " WHERE Header.R_Year=" + R_Year + "\n";
            SQL += " AND Header.R_Round=" + R_Round + "\n";

            string Title = "";

            if (ddlSector.SelectedIndex > 0)
            {
                SQL += " AND ( SECTOR_CODE='" + ddlSector.Items[ddlSector.SelectedIndex].Value + "' " + "\n";
                if (ddlSector.Items[ddlSector.SelectedIndex].Value == "3200")
                {
                    SQL += " AND  DEPT_CODE NOT IN ('32000300'))    " + "\n";
                    //-----------ฝ่ายโรงงานผลิตยาสูบ 3
                }
                else if (ddlSector.Items[ddlSector.SelectedIndex].Value == "3203")
                {
                    SQL += " OR  DEPT_CODE IN ('32000300','32030000','32030100','32030200','32030300','32030500'))  " + "\n";
                    //-----------ฝ่ายโรงงานผลิตยาสูบ 4
                }
                else if (ddlSector.Items[ddlSector.SelectedIndex].Value == "3204")
                {
                    SQL += " OR  DEPT_CODE IN ('32040000','32040100','32040200','32040300','32040500','32040300'))  " + "\n";
                    //-----------ฝ่ายโรงงานผลิตยาสูบ 5
                }
                else if (ddlSector.Items[ddlSector.SelectedIndex].Value == "3205")
                {
                    SQL += " OR  DEPT_CODE IN ('32050000','32050100','32050200','32050300','32050500'))\t  " + "\n";
                }
                else
                {
                    SQL += ")   " + "\n";
                }

                if (ddlDept.SelectedIndex < 1)
                {
                    Title += GL.SplitString(ddlSector.Items[ddlSector.SelectedIndex].Text, ":")[1].Trim() + " ";
                    //----- เลือกแสดงชื่อหน่วยงานเดียว --------
                }
            }
            if (ddlDept.SelectedIndex > 0)
            {
                SQL += " AND DEPT_CODE = '" + GL.SplitString(ddlDept.SelectedValue, "-")[0] + "' " + "\n";

                Title += ddlDept.Items[ddlDept.SelectedIndex].Text + " ";
                //----- เลือกแสดงชื่อหน่วยงานเดียว --------
            }
            if (Title == "") 
                Title += "";
            else
                Title = "ส่วนงาน/" + Title;
            string r = ddlRound.Items[ddlRound.SelectedIndex].Text;
            string[] splt={" "};
            Title +=  r.Split(splt,StringSplitOptions.RemoveEmptyEntries)[2] + " " + r.Split(splt,StringSplitOptions.RemoveEmptyEntries)[3] + " " + r.Split(splt,StringSplitOptions.RemoveEmptyEntries)[0] + " " + r.Split(splt,StringSplitOptions.RemoveEmptyEntries)[1];

            SQL += " GROUP BY Header.R_Year,Header.R_Round,Header.PSNL_NO,Header.PSNL_Fullname,Header.PNPS_CLASS," + "\n";
            SQL += " DEPT_CODE,DEPT_NAME,SECTOR_CODE,SECTOR_NAME,KPI_Status" + "\n";
            SQL += " ORDER BY SECTOR_CODE,DEPT_CODE,PNPS_CLASS DESC";

            SqlDataAdapter DA = new SqlDataAdapter(SQL, BL.ConnectionString());
            DA.SelectCommand.CommandTimeout = 90;
            DataTable DT = new DataTable();
            DA.Fill(DT);

            //------------ Addd Summary Record ----------
            DT.Columns.Add("New_Dept", typeof(Int32));
            DT.Columns.Add("Is_LastInDept", typeof(Int32));
            DT.Columns.Add("Is_LastInSector", typeof(Int32));
            string lastdept = "";
            string lastsector = "";
            for (int i = 0; i < DT.Rows.Count; i++)
            {
                if (DT.Rows[i]["DEPT_Code"].ToString() != lastdept)
                {
                    DT.Rows[i]["New_Dept"] = 1;
                    lastdept=DT.Rows[i]["DEPT_Code"].ToString();
                }
                else { DT.Rows[i]["New_Dept"] = 0; }

                if (DT.Rows[i]["SECTOR"].ToString() != lastsector)
                {
                    lastsector = DT.Rows[i]["SECTOR"].ToString();
                }

                if (i == DT.Rows.Count - 1 || DT.Rows[i + 1]["DEPT_Code"].ToString() != lastdept)
                {
                    DT.Rows[i]["Is_LastInDept"] = 1;
                }
                else {
                    DT.Rows[i]["Is_LastInDept"] = 0;
                }

                if (i == DT.Rows.Count - 1 || DT.Rows[i + 1]["SECTOR"].ToString() != lastsector)
                {
                    DT.Rows[i]["Is_LastInSector"] = 1;
                }
                else
                {
                    DT.Rows[i]["Is_LastInSector"] = 0;
                }
            }


            Session["RPT_K_PSNAssResult"] = DT;
            //--------- Title --------------
            Session["RPT_K_PSNAssResult_Title"] = Title;
            //--------- Sub Title ----------
            //Session["RPT_K_PSNAssResult_SubTitle"] = "รายงาน ณ วันที่ " + DateTime.Now.Day + " " + C.ToMonthNameTH(DateTime.Now.Month) + " พ.ศ." + (DateTime.Now.Year + 543);
            Session["RPT_K_PSNAssResult_SubTitle"] = "พบ   " + GL.StringFormatNumber(DT.Compute("COUNT(PSNL_No)", ""), 0) + "  รายการ";

            // Selected Level
            Session["RPT_K_PSNAssResult_Selected_Level"] = Selected_Level();

            Pager.SesssionSourceName = "RPT_K_PSNAssResult";
            Pager.RenderLayout();

            if (DT.Rows.Count == 0)
            {
                lblCountPSN.Text = Title + "ไม่พบรายการดังกล่าว";
            }
            else
            {
                lblCountPSN.Text = Title + "พบ " + GL.StringFormatNumber(DT.Compute("COUNT(PSNL_No)", ""), 0) + " รายการ";
            }
        }

        protected void Pager_PageChanging(PageNavigation Sender)
        {
            Pager.TheRepeater = rptKPI;
        }

        //------------ For Grouping -----------
		//string LastOrgranizeName = "";
		protected void rptKPI_ItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
		{
			if (e.Item.ItemType != ListItemType.AlternatingItem & e.Item.ItemType != ListItemType.Item)
				return;

            Label lblOrganize = (Label)e.Item.FindControl("lblOrganize");
            Label lblPSNName = (Label)e.Item.FindControl("lblPSNName");
            Label lblKPI1 = (Label)e.Item.FindControl("lblKPI1");
            Label lblKPI2 = (Label)e.Item.FindControl("lblKPI2");
            Label lblKPI3 = (Label)e.Item.FindControl("lblKPI3");
            Label lblKPI4 = (Label)e.Item.FindControl("lblKPI4");
            Label lblKPI5 = (Label)e.Item.FindControl("lblKPI5");
            Label lblNumKPI = (Label)e.Item.FindControl("lblNumKPI");
            HtmlAnchor btnPrint = (HtmlAnchor)e.Item.FindControl("btnPrint");

            DataRowView drv = (DataRowView)e.Item.DataItem;

            if (drv["New_Dept"].ToString() == "1")
            {
                lblOrganize.Text = drv["DEPT_NAME"].ToString();
                for (int i = 1; i <= 9; i++)
                {
                    HtmlTableCell td = (HtmlTableCell)e.Item.FindControl("td" + i);
                    td.Style["border-top-width"] = "2px";
                    td.Style["border-top-color"] = "#999999";
                }
            }
            else if(e.Item.ItemIndex>0)
            {
                HtmlTableCell td1 = (HtmlTableCell)e.Item.FindControl("td1");
                td1.Style["border-top"] = "none";
            }

            if( drv["KPI1"].ToString() != "0")
            {
                lblKPI1.Text = drv["KPI1"].ToString();
            }
            if (drv["KPI2"].ToString() != "0")
            {
                lblKPI2.Text = drv["KPI2"].ToString();
            }
            if (drv["KPI3"].ToString() != "0")
            {
                lblKPI3.Text = drv["KPI3"].ToString();
            }
            if (drv["KPI4"].ToString() != "0")
            {
                lblKPI4.Text = drv["KPI4"].ToString();
            }
            if (drv["KPI5"].ToString() != "0")
            {
                lblKPI5.Text = drv["KPI5"].ToString();
            }
            lblNumKPI.Text = drv["NumKPI"].ToString();
            
            lblPSNName.Text = drv["PSNL_Fullname"].ToString();

            if (GL.IsEqualNull(drv["ASS_Status"]))
            {
                //btnPrint.HRef = "Print/RPT_C_PNSAss.aspx?Mode=PDF&R_Year=" & drv["R_Year"] & "&R_Round=" & drv["R_Round"] & "&PSNL_NO=" & drv["PSNL_NO"].ToString
                btnPrint.Visible = false;
            }
            else
            {
                btnPrint.HRef = "Print/RPT_K_PNSAss.aspx?Mode=PDF&R_Year=" + drv["R_Year"].ToString() + "&R_Round=" + drv["R_Round"].ToString() + "&PSNL_NO=" + drv["PSNL_NO"].ToString() + "&Status=" + drv["ASS_Status"].ToString();
            }

            //--------------- Add Summary Record-----------------
            if (drv["Is_LastInDept"].ToString() == "1")
            {
                //------------ Sum----------------
                HtmlTableRow trSumDept = (HtmlTableRow)e.Item.FindControl("trSumDept");
                Label lblSUMDeptName = (Label)e.Item.FindControl("lblSUMDeptName");
                Label lblCountKPI1 = (Label)e.Item.FindControl("lblCountKPI1");
                Label lblCountKPI2 = (Label)e.Item.FindControl("lblCountKPI2");
                Label lblCountKPI3 = (Label)e.Item.FindControl("lblCountKPI3");
                Label lblCountKPI4 = (Label)e.Item.FindControl("lblCountKPI4");
                Label lblCountKPI5 = (Label)e.Item.FindControl("lblCountKPI5");
                Label lblCountKPI = (Label)e.Item.FindControl("lblCountKPI");
                
                trSumDept.Visible=true;
                lblSUMDeptName.Text = drv["DEPT_NAME"].ToString();
                DataTable DT = ((DataTable)rptKPI.DataSource).Copy();
               
                int KPI1= Convert.ToInt32( DT.Compute("SUM(KPI1)", "DEPT_Code='" + drv["DEPT_Code"].ToString() + "'"));
                int KPI2 = Convert.ToInt32(DT.Compute("SUM(KPI2)", "DEPT_Code='" + drv["DEPT_Code"].ToString() + "'"));
                int KPI3 = Convert.ToInt32(DT.Compute("SUM(KPI3)", "DEPT_Code='" + drv["DEPT_Code"].ToString() + "'"));
                int KPI4 = Convert.ToInt32(DT.Compute("SUM(KPI4)", "DEPT_Code='" + drv["DEPT_Code"].ToString() + "'"));
                int KPI5 = Convert.ToInt32(DT.Compute("SUM(KPI5)", "DEPT_Code='" + drv["DEPT_Code"].ToString() + "'"));
                int Total = Convert.ToInt32(DT.Compute("SUM(NumKPI)", "DEPT_Code='" + drv["DEPT_Code"].ToString() + "'"));

                if (KPI1 > 0) lblCountKPI1.Text = KPI1.ToString();
                if (KPI2 > 0) lblCountKPI2.Text = KPI2.ToString();
                if (KPI3 > 0) lblCountKPI3.Text = KPI3.ToString();
                if (KPI4 > 0) lblCountKPI4.Text = KPI4.ToString();
                if (KPI5 > 0) lblCountKPI5.Text = KPI5.ToString();
                if (Total > 0) lblCountKPI.Text = Total.ToString();

                //------------ Avg----------------
                HtmlTableRow trAvgDept= (HtmlTableRow)e.Item.FindControl("trAvgDept");
                Label lblAVGKPI1 = (Label)e.Item.FindControl("lblAVGKPI1");
                Label lblAVGKPI2 = (Label)e.Item.FindControl("lblAVGKPI2");
                Label lblAVGKPI3 = (Label)e.Item.FindControl("lblAVGKPI3");
                Label lblAVGKPI4 = (Label)e.Item.FindControl("lblAVGKPI4");
                Label lblAVGKPI5 = (Label)e.Item.FindControl("lblAVGKPI5");

                trAvgDept.Visible = true;
                if (Total != 0)
                {
                    Single AVG1 = ((Single)KPI1 * 100 / Total);
                    Single AVG2 = ((Single)KPI2 * 100 / Total);
                    Single AVG3 = ((Single)KPI3 * 100 / Total);
                    Single AVG4 = ((Single)KPI4 * 100 / Total);
                    Single AVG5 = ((Single)KPI5 * 100 / Total);

                    if (AVG1 > 0) lblAVGKPI1.Text = AVG1.ToString("N");
                    if (AVG2 > 0) lblAVGKPI2.Text = AVG2.ToString("N");
                    if (AVG3 > 0) lblAVGKPI3.Text = AVG3.ToString("N");
                    if (AVG4 > 0) lblAVGKPI4.Text = AVG4.ToString("N");
                    if (AVG5 > 0) lblAVGKPI5.Text = AVG5.ToString("N");
                }                
            }

            //--------------- Add Summary Record-----------------
            if (drv["Is_LastInSector"].ToString() == "1" &GL.CINT( Session["RPT_K_PSNAssResult_Selected_Level"])>1)
            {
                HtmlTableRow trSumSector = (HtmlTableRow)e.Item.FindControl("trSumSector");
                Label lblSUMSectorName = (Label)e.Item.FindControl("lblSUMSectorName");
                Label lblCountSector1 = (Label)e.Item.FindControl("lblCountSector1");
                Label lblCountSector2 = (Label)e.Item.FindControl("lblCountSector2");
                Label lblCountSector3 = (Label)e.Item.FindControl("lblCountSector3");
                Label lblCountSector4 = (Label)e.Item.FindControl("lblCountSector4");
                Label lblCountSector5 = (Label)e.Item.FindControl("lblCountSector5");
                Label lblCountSector = (Label)e.Item.FindControl("lblCountSector");

                trSumSector.Visible = true;
                lblSUMSectorName.Text = drv["SECTOR_NAME"].ToString();
                DataTable DT = ((DataTable)rptKPI.DataSource).Copy();

                int KPI1 = Convert.ToInt32(DT.Compute("SUM(KPI1)", "SECTOR_CODE='" + drv["SECTOR_CODE"].ToString() + "' AND SECTOR_NAME='" + drv["SECTOR_NAME"].ToString() + "'"));
                int KPI2 = Convert.ToInt32(DT.Compute("SUM(KPI2)", "SECTOR_CODE='" + drv["SECTOR_CODE"].ToString() + "' AND SECTOR_NAME='" + drv["SECTOR_NAME"].ToString() + "'"));
                int KPI3 = Convert.ToInt32(DT.Compute("SUM(KPI3)", "SECTOR_CODE='" + drv["SECTOR_CODE"].ToString() + "' AND SECTOR_NAME='" + drv["SECTOR_NAME"].ToString() + "'"));
                int KPI4 = Convert.ToInt32(DT.Compute("SUM(KPI4)", "SECTOR_CODE='" + drv["SECTOR_CODE"].ToString() + "' AND SECTOR_NAME='" + drv["SECTOR_NAME"].ToString() + "'"));
                int KPI5 = Convert.ToInt32(DT.Compute("SUM(KPI5)", "SECTOR_CODE='" + drv["SECTOR_CODE"].ToString() + "' AND SECTOR_NAME='" + drv["SECTOR_NAME"].ToString() + "'"));
                int Total = Convert.ToInt32(DT.Compute("SUM(NumKPI)", "SECTOR_CODE='" + drv["SECTOR_CODE"].ToString() + "' AND SECTOR_NAME='" + drv["SECTOR_NAME"].ToString() + "'"));

                if (KPI1 > 0) lblCountSector1.Text = KPI1.ToString();
                if (KPI2 > 0) lblCountSector1.Text = KPI2.ToString();
                if (KPI3 > 0) lblCountSector3.Text = KPI3.ToString();
                if (KPI4 > 0) lblCountSector4.Text = KPI4.ToString();
                if (KPI5 > 0) lblCountSector5.Text = KPI5.ToString();
                if (Total > 0) lblCountSector.Text = Total.ToString();

                //------------ Avg----------------
                HtmlTableRow trAvgSector = (HtmlTableRow)e.Item.FindControl("trAvgSector");
                Label lblAVGSector1 = (Label)e.Item.FindControl("lblAVGSector1");
                Label lblAVGSector2 = (Label)e.Item.FindControl("lblAVGSector2");
                Label lblAVGSector3 = (Label)e.Item.FindControl("lblAVGSector3");
                Label lblAVGSector4 = (Label)e.Item.FindControl("lblAVGSector4");
                Label lblAVGSector5 = (Label)e.Item.FindControl("lblAVGSector5");

                trAvgSector.Visible = true;
                if (Total != 0)
                {
                    Single AVG1 = ((Single)KPI1 * 100 / Total);
                    Single AVG2 = ((Single)KPI2 * 100 / Total);
                    Single AVG3 = ((Single)KPI3 * 100 / Total);
                    Single AVG4 = ((Single)KPI4 * 100 / Total);
                    Single AVG5 = ((Single)KPI5 * 100 / Total);

                    if (AVG1 > 0) lblAVGSector1.Text = AVG1.ToString("N");
                    if (AVG2 > 0) lblAVGSector2.Text = AVG2.ToString("N");
                    if (AVG3 > 0) lblAVGSector3.Text = AVG3.ToString("N");
                    if (AVG4 > 0) lblAVGSector4.Text = AVG4.ToString("N");
                    if (AVG5 > 0) lblAVGSector5.Text = AVG5.ToString("N");
                }           
            }
		}

        public RPT_K_PSNAssResult()
		{
			Load += Page_Load;
		}

    }
}
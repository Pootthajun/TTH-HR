using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Xml.Linq;
using System.Diagnostics;
using System.Collections.Specialized;
using System.Configuration;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Caching;
using System.Web.SessionState;
using System.Web.Security;
using System.Web.Profile;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.IO;
using System.Drawing;
using AjaxControlToolkit;
namespace VB
{

    public partial class CP_Setting_Property : System.Web.UI.Page
    {


        HRBL BL = new HRBL();
        GenericLib GL = new GenericLib();

        textControlLib CL = new textControlLib();

        protected void Page_Load(object sender, System.EventArgs e)
        {
            if ((Session["USER_PSNL_NO"] == null))
            {
                ScriptManager.RegisterStartupScript(this.Page, typeof(string), "Alert", "showAlert('กรุณาลงชื่อเข้าใช้ระบบ');", true);
                Response.Redirect("Login.aspx");
                return;
            }
            if (!IsPostBack)
            {
                if (!string.IsNullOrEmpty(Request.QueryString["POS_NO"]))
                {
                    PnlList.Visible = false;
                    pnlEdit.Visible = true;

                    ModalTraining.Visible = false;
                    ModalTest.Visible = false;
                    ModalPost.Visible = false;
                    POS_NO = Request.QueryString["POS_NO"].ToString();
                    BL.BindGroup_Course(BindGroup_Course);
                    BL.BindDDlSector(ddlSector);
                    BL.BindDDlSector(ddl_Search_Sector);
                    BL.BindDDlSector(ddl_Search_Sector_CopyPOS);
                    CurrentPos();
                    return;
                }

                BindPosition();
                ClearForm();
                BL.BindGroup_Course(BindGroup_Course);
                BL.BindDDlSector(ddlSector);
                BL.BindDDlSector(ddl_Search_Sector);
            }

        }

        public string POS_NO
        {
            get { return lblPos_Head.Attributes["POS_NO"]; }
            set { lblPos_Head.Attributes["POS_NO"] = value; }
        }

        public string POS_NO_Detail
        {
            get { return lblPos_Head.Attributes["POS_NO_Detail"]; }
            set { lblPos_Head.Attributes["POS_NO_Detail"] = value; }
        }
        public string SECTOR_CODE
        {
            get { return lblPSN_Head.Attributes["SECTOR_CODE"]; }
            set { lblPSN_Head.Attributes["SECTOR_CODE"] = value; }
        }
        public string DEPT_CODE
        {
            get { return lblPSN_Head.Attributes["DEPT_CODE"]; }
            set { lblPSN_Head.Attributes["DEPT_CODE"] = value; }
        }
        public string EXIST_Training
        {
            get { return lblTrainingNo.Attributes["EXIST_Training"]; }
            set { lblTrainingNo.Attributes["EXIST_Training"] = value; }
        }

        public string EXIST_Test
        {
            get { return lblTestNo.Attributes["EXIST_Test"]; }
            set { lblTestNo.Attributes["EXIST_Test"] = value; }
        }

        public string EXIST_Pos
        {
            get { return lblPosCurrent.Attributes["EXIST_Pos"]; }
            set { lblPosCurrent.Attributes["EXIST_Pos"] = value; }
        }

        public string Mode_Pos
        {
            get { return lblModePos.Attributes["Mode_Pos"]; }
            set { lblModePos.Attributes["Mode_Pos"] = value; }
        }
        public string Mode_Select
        {
            get { return lblModePos.Attributes["Mode_Select"]; }
            set { lblModePos.Attributes["Mode_Select"] = value.ToString(); }
        }

        private void ClearForm()
        {
            CL.ImplementJavaMoneyText(txtAss_Min_Score);
            //CL.ImplementJavaFloatText(txtLeave_Score)
            CL.ImplementJavaMoneyText(txtLeave_Score);
            PnlList.Visible = true;
            pnlEdit.Visible = false;
            ModalTraining.Visible = false;
            ModalTest.Visible = false;
            ModalPost.Visible = false;
            txtAss_Min_Score.Text = "";
            rptTraining = null;
            txtLeave_Score.Text = "";
            txtArrive_Late.Text = "";
            rptTest = null;
            chkPunishment.Checked = false;
            EXIST_Pos = "";
            lblModePos.Text = "";
            lblPSN_Head.Text = "";

        }


        private void BindPosition()
        {
            string SQL = "";
            SQL += " SELECT DEPT.SECTOR_CODE,DEPT.SECTOR_NAME,DEPT.DEPT_CODE,DEPT.DEPT_NAME,POS.POS_NO,ISNULL(POS.MGR_NAME,'-') MGR_NAME,ISNULL(POS.FLD_NAME,'-') FLD_NAME,\n";
            SQL += "  POS.PNPO_CLASS,PSN.PSNL_NO,PSN.PSNL_Fullname,POS.Update_Time \n";
            SQL += "  ,Setting_Status.Status_Path\n";
            SQL += "  FROM vw_PN_Position POS \n";
            SQL += "  INNER JOIN vw_HR_Department DEPT ON POS.DEPT_CODE=DEPT.DEPT_CODE AND POS.MINOR_CODE=DEPT.MINOR_CODE\n";
            SQL += "  LEFT JOIN vw_PN_PSNL PSN ON POS.POS_NO=PSN.POS_NO\n";
            SQL += "  INNER JOIN vw_Path_Setting_Status Setting_Status ON Setting_Status.POS_No = POS.POS_NO\n";
            SQL += "  WHERE POS.PNPO_CLASS IS NOT NULL \n";

            if (ddlSector.SelectedIndex > 0)
            {
                string[] Sector = Strings.Split(ddlSector.Items[ddlSector.SelectedIndex].Text, ":");
                string Sector_Name = Sector[1];

                SQL += " AND  ( LEFT(DEPT.DEPT_CODE,2)='" + ddlSector.Items[ddlSector.SelectedIndex].Value + "' \n";
                SQL += " OR LEFT(DEPT.DEPT_CODE,4)='" + ddlSector.Items[ddlSector.SelectedIndex].Value + "'   \n";

                if (ddlSector.Items[ddlSector.SelectedIndex].Value == "3200")
                {
                    SQL += " AND  DEPT.DEPT_CODE NOT IN ('32000300'))    \n";
                    //-----------ฝ่ายโรงงานผลิตยาสูบ 3
                }
                else if (ddlSector.Items[ddlSector.SelectedIndex].Value == "3203")
                {
                    SQL += " OR  DEPT.DEPT_CODE IN ('32000300','32030000','32030100','32030200','32030300','32030500'))  \n";
                    //-----------ฝ่ายโรงงานผลิตยาสูบ 4
                }
                else if (ddlSector.Items[ddlSector.SelectedIndex].Value == "3204")
                {
                    SQL += " OR  DEPT.DEPT_CODE IN ('32040000','32040100','32040200','32040300','32040500','32040300'))  \n";
                    //-----------ฝ่ายโรงงานผลิตยาสูบ 5
                }
                else if (ddlSector.Items[ddlSector.SelectedIndex].Value == "3205")
                {
                    SQL += " OR  DEPT.DEPT_CODE IN ('32050000','32050100','32050200','32050300','32050500'))\t  \n";
                }
                else
                {
                    SQL += ")   \n";
                }


            }
            if (!string.IsNullOrEmpty(txt_Search_Organize.Text))
            {
                SQL += " AND (DEPT.DEPT_NAME LIKE '%" + txt_Search_Organize.Text.Replace("'", "''") + "%' ) \n";
            }
            if (!string.IsNullOrEmpty(lblPOS.Text))
            {
                SQL += " AND (POS.POS_NO LIKE '%" + lblPOS.Text.Replace("'", "''") + "%' OR \n";
                SQL += " POS.MGR_NAME LIKE '%" + lblPOS.Text.Replace("'", "''") + "%' OR \n";
                SQL += " POS.FLD_NAME LIKE '%" + lblPOS.Text.Replace("'", "''") + "%' )\n";
            }

            switch (GL.CINT(ddlSetting.SelectedValue))
            {
                case 1:
                    SQL += " AND (Setting_Status.Status_Path IN (1))\n";
                    break;
                case 2:
                    SQL += " AND (Setting_Status.Status_Path IN (0))\n";
                    break;
                default:
                    break;
            }
            switch (GL.CINT(ddlChkPosEmpty.SelectedValue))
            {
                case 1:
                    SQL += " AND (PSN.PSNL_Fullname IS NULL)\n";
                    break;
                case 2:
                    SQL += " AND (PSN.PSNL_Fullname IS NOT NULL)\n";
                    break;
                default:
                    break;
            }


            SQL += "  ";
            SQL += "  GROUP BY DEPT.SECTOR_NAME,DEPT.DEPT_NAME,POS.POS_NO,POS.FLD_NAME,POS.MGR_NAME,\n";
            SQL += "  POS.PNPO_CLASS, PSN.PSNL_NO, PSN.PSNL_Fullname, POS.Update_Time, DEPT.SECTOR_CODE, DEPT.DEPT_CODE, POS.PNPO_CLASS, POS.POS_NO\n";
            SQL += "  , Setting_Status.Status_Path\n";
            SQL += "  ORDER BY DEPT.SECTOR_CODE,DEPT.DEPT_CODE,POS.PNPO_CLASS DESC,POS.POS_NO\n";


            SqlDataAdapter DA = new SqlDataAdapter(SQL, BL.ConnectionString());
            DataTable DT = new DataTable();
            DA.Fill(DT);

            Session["Setting_Position"] = DT;
            Pager.SesssionSourceName = "Setting_Position";
            Pager.RenderLayout();

            if (DT.Rows.Count == 0)
            {
                lblCountList.Text = "ไม่พบรายการดังกล่าว";
            }
            else
            {
                lblCountList.Text = "พบ " + GL.StringFormatNumber(DT.Rows.Count, 0) + " รายการ";
            }

        }

        protected void btnSearch_Click(object sender, System.EventArgs e)
        {
            BindPosition();
        }

        protected void Pager_PageChanging(PageNavigation Sender)
        {
            Pager.TheRepeater = rptList;
        }

        string LastSector = "";

        string LastDept = "";
        protected void rptList_ItemCommand(object source, System.Web.UI.WebControls.RepeaterCommandEventArgs e)
        {
            switch (e.CommandName)
            {
                case "Setting":
                    PnlList.Visible = false;
                    pnlEdit.Visible = true;
                    Label lblClass = (Label)e.Item.FindControl("lblClass");
                    Label lblPosNo = (Label)e.Item.FindControl("lblPosNo");
                    Label lblMGR = (Label)e.Item.FindControl("lblMGR");
                    Label lblDept_Temp = (Label)e.Item.FindControl("lblDept_Temp");
                    Label lblPSN = (Label)e.Item.FindControl("lblPSN");
                    Label lblSector_No = (Label)e.Item.FindControl("lblSector_No");
                    Label lblDept_No = (Label)e.Item.FindControl("lblDept_No");
                    Button btnSetting = (Button)e.Item.FindControl("btnSetting");
                    //-------------- ดึงข้อมูลตำแหน่ง------------
                    POS_NO = btnSetting.CommandArgument;
                    lblPos_Head.Text = lblPosNo.Text + " : " + lblMGR.Text;
                    lblDept_Head.Text = lblDept_Temp.Text;
                    lblClass_Head.Text = lblClass.Text;
                    if (lblPSN.Text != "-")
                    {
                        lblPSN_Head.Text = lblPSN.Text;
                        lblPSN_Head.ForeColor = System.Drawing.Color.Black;
                    }
                    else
                    {
                        lblPSN_Head.Text = "ไม่มีผู้ครองตำแหน่ง";
                        lblPSN_Head.ForeColor = System.Drawing.Color.Red;
                    }
                    //------------ค้นหาเกณฑ์ของตำแหน่ง--------------------

                    //-----ผลการประเมิน--,วันลา สาย ขาด ,โทษทางวินัย------
                    Path_Setting();
                    //-----การฝึกอบรม----------
                    BindTrainingDetail();
                    //-----การทดสอบ----------
                    BindTestDetail();
                    //-----กำหนดเส้นทาง---------
                    BindPath_Position();
                    //------LEFT/RIGHT----------
                    if (Convert.ToInt32(lblClass_Head.Text) == 14)
                    {
                        //------LEFT----------
                        tdPosLeft_Header.Visible = true;
                        tdPosLeft_Detail.Visible = true;
                        tdPosLeft_rpt.Visible = true;
                        tdimgPosLeft_Header.Visible = true;
                        tdimgPosLeft_Detail.Visible = true;
                        tdimgPosLeft_rpt.Visible = true;
                        //------RIGHT----------
                        tdPosRight_Header.Visible = false;
                        tdPosRight_Detail.Visible = false;
                        tdPosRight_rpt.Visible = false;
                        tdimgPosRight_Header.Visible = false;
                        tdimgPosRight_Detail.Visible = false;
                        tdimgPosRight_rpt.Visible = false;
                        BindPosLeft();
                    }
                    else if (Convert.ToInt32(lblClass_Head.Text) == 1)
                    {
                        //------LEFT----------
                        tdPosLeft_Header.Visible = false;
                        tdPosLeft_Detail.Visible = false;
                        tdPosLeft_rpt.Visible = false;
                        tdimgPosLeft_Header.Visible = false;
                        tdimgPosLeft_Detail.Visible = false;
                        tdimgPosLeft_rpt.Visible = false;
                        //------RIGHT----------
                        tdPosRight_Header.Visible = true;
                        tdPosRight_Detail.Visible = true;
                        tdPosRight_rpt.Visible = true;
                        tdimgPosRight_Header.Visible = true;
                        tdimgPosRight_Detail.Visible = true;
                        tdimgPosRight_rpt.Visible = true;
                        BindPosRight();
                    }
                    else
                    {
                        //------LEFT----------
                        tdPosLeft_Header.Visible = true;
                        tdPosLeft_Detail.Visible = true;
                        tdPosLeft_rpt.Visible = true;
                        tdimgPosLeft_Header.Visible = true;
                        tdimgPosLeft_Detail.Visible = true;
                        tdimgPosLeft_rpt.Visible = true;
                        //------RIGHT----------
                        tdPosRight_Header.Visible = true;
                        tdPosRight_Detail.Visible = true;
                        tdPosRight_rpt.Visible = true;
                        tdimgPosRight_Header.Visible = true;
                        tdimgPosRight_Detail.Visible = true;
                        tdimgPosRight_rpt.Visible = true;
                        //'------comment ปรับโครงสร้าง
                        BindPosLeft();
                        BindPosRight();

                    }
                    //-----กำหนดเส้นทางในฝ่ายเดียวกัน---------
                    SECTOR_CODE = lblSector_No.Text;
                    DEPT_CODE = lblDept_No.Text;

                    break;

                case "ckSelect":
                    //CurrentData_SelectPOS();
                    String POS = CurrentData_SelectPOS();
                    if (POS != "")
                    {
                        POS = POS.Replace("'", "");
                    }
                    lblCurrent_SelectPOS.Text = "ตำแหน่งที่เลือก : " + POS;
                    break;
            }
        }

        //---------------รายการตำแหน่ง----------------
        protected void rptList_ItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType != ListItemType.Item & e.Item.ItemType != ListItemType.AlternatingItem)
                return;

            Label lblClass = (Label)e.Item.FindControl("lblClass");
            CheckBox chkSelect_Paste = (CheckBox)e.Item.FindControl("chkSelect_Paste");

            Label lblPosNo = (Label)e.Item.FindControl("lblPosNo");
            Label lblFLD = (Label)e.Item.FindControl("lblFLD");
            Label lblMGR = (Label)e.Item.FindControl("lblMGR");
            Label lblSector = (Label)e.Item.FindControl("lblSector");
            Label lblDept = (Label)e.Item.FindControl("lblDept");
            Label lblDept_Temp = (Label)e.Item.FindControl("lblDept_Temp");
            Label lblPSN = (Label)e.Item.FindControl("lblPSN");
            Label lblSector_No = (Label)e.Item.FindControl("lblSector_No");
            Label lblDept_No = (Label)e.Item.FindControl("lblDept_No");
            Button btnSetting = (Button)e.Item.FindControl("btnSetting");
            HtmlAnchor img_Status = (HtmlAnchor)e.Item.FindControl("img_Status");

            DataRowView drv = (DataRowView)e.Item.DataItem;

            if (LastSector != drv["SECTOR_NAME"].ToString())
            {
                lblSector.Text = drv["SECTOR_NAME"].ToString();
                LastSector = drv["SECTOR_NAME"].ToString();
                lblDept.Text = drv["DEPT_NAME"].ToString();
                LastDept = drv["DEPT_NAME"].ToString();
            }
            else if (LastDept != drv["DEPT_NAME"].ToString())
            {
                lblDept.Text = drv["DEPT_NAME"].ToString();
                LastDept = drv["DEPT_NAME"].ToString();
            }

            lblSector_No.Text = drv["SECTOR_CODE"].ToString();
            lblDept_No.Text = drv["DEPT_CODE"].ToString();
            lblClass.Text = GL.CINT(drv["PNPO_CLASS"]).ToString();
            lblDept_Temp.Text = drv["DEPT_NAME"].ToString();
            lblPosNo.Text = drv["POS_NO"].ToString();
            lblPosNo.Attributes["POS_NO"] = drv["POS_NO"].ToString();
            lblFLD.Text = "<B>" + drv["FLD_NAME"].ToString() + "</B>";
            lblMGR.Text = "<B>" + drv["MGR_NAME"].ToString() + "</B>";
            lblPSN.ForeColor = System.Drawing.Color.Gray;
            if (!GL.IsEqualNull(drv["PSNL_Fullname"]))
            {
                lblPSN.Text = drv["PSNL_NO"] + " : " + drv["PSNL_Fullname"].ToString();
            }
            else
            {
                lblPSN.Text = "-";
            }
            if (GL.CINT(drv["Status_Path"]) == 1)
            {
                img_Status.InnerHtml = "<img src='images/check.png' />";
            }
            else
            {
                img_Status.InnerHtml = "<img src='images/noneCP.png' />";
            }

            btnSetting.CommandArgument = drv["POS_NO"].ToString();
        }


        private void Path_Setting()
        {
            string SQL = "";
            SQL += " SELECT * FROM tb_Path_Setting \n";
            SQL += " WHERE POS_No='" + POS_NO + "'\n";
            SqlDataAdapter DA = new SqlDataAdapter(SQL, BL.ConnectionString());
            DataTable DT = new DataTable();
            DA.Fill(DT);

            if (DT.Rows.Count == 1)
            {
                if ((DT.Rows[0]["Ass_Min_Score"].ToString() != ""))
                {
                    txtAss_Min_Score.Text = GL.StringFormatNumber(GL.CDBL(DT.Rows[0]["Ass_Min_Score"]));
                }

                if ((DT.Rows[0]["Leave_Score"].ToString() != ""))
                {
                    txtLeave_Score.Text = GL.StringFormatNumber(GL.CDBL(DT.Rows[0]["Leave_Score"]));
                }
                if ((DT.Rows[0]["Arrive_Late"].ToString() != ""))
                {
                    txtArrive_Late.Text = GL.StringFormatNumber(GL.CDBL(DT.Rows[0]["Arrive_Late"]));
                }

                if (GL.CBOOL(DT.Rows[0]["Punishment"]))
                {
                    btnCheck_Punishment.ImageUrl = "images/checkCP.png";
                    chkPunishment.Checked = true;
                }
                else
                {
                    btnCheck_Punishment.ImageUrl = "images/noneCP.png";
                    chkPunishment.Checked = false;
                }
                btnCheck_Punishment.Attributes["Punishment"] = "True";
            }
            else
            {
                txtAss_Min_Score.Text = "";
                rptTraining.DataSource = null;
                txtLeave_Score.Text = "";
                txtArrive_Late.Text = "";
                rptTest.DataSource = null;
                chkPunishment.Checked = false;
                btnCheck_Punishment.ImageUrl = "images/noneCP.png";
            }
        }


        //-----------------------ตารางหลักสูตร--------------------
        private void BindTrainingDetail()
        {
            string SQL = "";

            SQL += "   SELECT Training.POS_No,COURSE.COURSE_ID,COURSE.COURSE_DESC \n";
            SQL += "   FROM tb_Path_Setting_Training Training\n";
            SQL += "   LEFT JOIN tb_PK_COURSE COURSE ON Training.COURSE_ID=COURSE.COURSE_ID\n";
            SQL += "   WHERE Training.POS_NO ='" + POS_NO + "'\n";
            SQL += "   ORDER BY Training.POS_No,COURSE.COURSE_ID,COURSE.COURSE_DESC\n";

            SqlDataAdapter DA = new SqlDataAdapter(SQL, BL.ConnectionString());
            DataTable DT = new DataTable();
            DA.Fill(DT);
            if (DT.Rows.Count == 0)
            {
                pnltable_Course.Visible = false;
            }
            else
            {
                pnltable_Course.Visible = true;
            }
            rptTraining.DataSource = DT;
            rptTraining.DataBind();
        }

        protected void btnAddCourse_Click(object sender, System.EventArgs e)
        {
            //------------- Get Existing Training List ---------------
            string _Traininglist = "";
            foreach (RepeaterItem item in rptTraining.Items)
            {
                if (item.ItemType != ListItemType.AlternatingItem & item.ItemType != ListItemType.Item)
                    continue;
                Button btnDelete = (Button)item.FindControl("btnDelete");
                _Traininglist += "'" + btnDelete.CommandArgument + "'" + ",";
            }
            if (!string.IsNullOrEmpty(_Traininglist))
            {
                EXIST_Training = _Traininglist.Substring(0, _Traininglist.Length - 1);
            }
            else
            {
                EXIST_Training = "";
            }

            BindTrainingList();
            ModalTraining.Visible = true;
            ScriptManager.RegisterStartupScript(this.Page, typeof(string), "Focus", "document.getElementById('" + txtSearchTraining.ClientID + "').focus();", true);
        }

        protected void rptTraining_ItemCommand(object source, System.Web.UI.WebControls.RepeaterCommandEventArgs e)
        {
            switch (e.CommandName)
            {
                case "Delete":
                    Button btnDelete = (Button)e.Item.FindControl("btnDelete");
                    int COURSE_ID = GL.CINT(btnDelete.CommandArgument);

                    string SQL = "";
                    SQL += " DELETE FROM tb_Path_Setting_Training\n";
                    SQL += " WHERE COURSE_ID=" + COURSE_ID + "\n";
                    SQL += " AND POS_NO='" + POS_NO + "'\n";
                    SqlDataAdapter DA = new SqlDataAdapter(SQL, BL.ConnectionString());
                    DataTable DT = new DataTable();
                    DA.Fill(DT);
                    rptTraining.DataSource = DT;
                    rptTraining.DataBind();
                    BindTrainingDetail();
                    break;
            }


        }

        protected void rptTraining_ItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType != ListItemType.AlternatingItem & e.Item.ItemType != ListItemType.Item)
                return;
            Label lblName = (Label)e.Item.FindControl("lblName");
            Button btnDelete = (Button)e.Item.FindControl("btnDelete");
            DataRowView drv = (DataRowView)e.Item.DataItem;

            lblName.Text = drv["COURSE_DESC"].ToString();
            btnDelete.CommandArgument = drv["COURSE_ID"].ToString();
        }

        //-----------------------ตารางการทดสอบ--------------------
        private void BindTestDetail()
        {
            string SQL = "";

            SQL += " SELECT Setting_Test.POS_No,Test.CPSM_SUBJECT_CODE,Test.CPSM_SUBJECT_NAME,Setting_Test.Min_Score \n";
            SQL += " FROM tb_Path_Setting_Test Setting_Test\n";
            SQL += " LEFT JOIN tb_CP_SUBJECT_MASTER Test ON Setting_Test.SUBJECT_CODE=Test.CPSM_SUBJECT_CODE\n";
            SQL += " WHERE POS_NO ='" + POS_NO + "'\n";
            SQL += " ORDER BY Setting_Test.POS_No,Test.CPSM_SUBJECT_CODE,Test.CPSM_SUBJECT_NAME\n";

            SqlDataAdapter DA = new SqlDataAdapter(SQL, BL.ConnectionString());
            DataTable DT = new DataTable();
            DA.Fill(DT);
            if (DT.Rows.Count == 0)
            {
                pnltable_Test.Visible = false;
            }
            else
            {
                pnltable_Test.Visible = true;
            }
            rptTest.DataSource = DT;
            rptTest.DataBind();
        }


        protected void BtnAddTest_Click(object sender, System.EventArgs e)
        {
            //------------- Get Existing Training List ---------------
            string _Testlist = "";
            foreach (RepeaterItem item in rptTest.Items)
            {
                if (item.ItemType != ListItemType.AlternatingItem & item.ItemType != ListItemType.Item)
                    continue;
                Button btnDelete = (Button)item.FindControl("btnDelete");
                _Testlist += "'" + btnDelete.CommandArgument + "'" + ",";
            }
            if (!string.IsNullOrEmpty(_Testlist))
            {
                EXIST_Test = _Testlist.Substring(0, _Testlist.Length - 1);
            }
            else
            {
                EXIST_Test = "";
            }

            BindTestList();
            ModalTest.Visible = true;
            ScriptManager.RegisterStartupScript(this.Page, typeof(string), "Focus", "document.getElementById('" + txtSearchTest.ClientID + "').focus();", true);
        }

        protected void rptTest_ItemCommand(object source, System.Web.UI.WebControls.RepeaterCommandEventArgs e)
        {
            string SQL;
            DataTable DT;
            SqlCommandBuilder cmd;
            SqlDataAdapter DA;
            DataRow DR;
            string SUBJECT_CODE;

            switch (e.CommandName)
            {
                case "Delete":
                    Button btnDelete = (Button)e.Item.FindControl("btnDelete");
                    SUBJECT_CODE = btnDelete.CommandArgument;

                    SQL = "";
                    SQL += " DELETE FROM tb_Path_Setting_Test\n";
                    SQL += " WHERE SUBJECT_CODE=" + SUBJECT_CODE + "\n";
                    SQL += " AND POS_NO='" + POS_NO + "'\n";
                    DA = new SqlDataAdapter(SQL, BL.ConnectionString());
                    DT = new DataTable();
                    DA.Fill(DT);
                    rptTest.DataSource = DT;
                    rptTest.DataBind();
                    BindTestDetail();
                    break;
                case "Update":
                    TextBox txtScore = (TextBox)e.Item.FindControl("txtScore");
                    Button btnupdate = (Button)e.Item.FindControl("btnDelete");
                    SUBJECT_CODE = btnupdate.CommandArgument;

                    SQL = "SElECT * FROM tb_Path_Setting_Test \n";
                    SQL += " WHERE POS_No='" + POS_NO + "'\n";
                    SQL += " AND SUBJECT_CODE=" + SUBJECT_CODE + "\n";

                    DT = new DataTable();
                    DA = new SqlDataAdapter(SQL, BL.ConnectionString());
                    DA.Fill(DT);

                    DR = null;
                    if (DT.Rows.Count == 0)
                    {
                        DR = DT.NewRow();
                    }
                    else
                    {
                        DR = DT.Rows[0];
                    }
                    if (string.IsNullOrEmpty(txtScore.Text))
                    {
                        DR["Min_Score"] = GL.StringFormatNumber(0);
                    }
                    else
                    {
                        DR["Min_Score"] = Conversion.Val(txtScore.Text);
                    }
                    DR["Update_By"] = Session["USER_PSNL_NO"].ToString();
                    DR["Update_Time"] = DateAndTime.Now;
                    if (DT.Rows.Count == 0)
                        DT.Rows.Add(DR);

                    cmd = new SqlCommandBuilder();
                    cmd = new SqlCommandBuilder(DA);
                    DA.Update(DT);
                    BindTestDetail();

                    break;
            }
        }

        protected void rptTest_ItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType != ListItemType.AlternatingItem & e.Item.ItemType != ListItemType.Item)
                return;
            Label lblName = (Label)e.Item.FindControl("lblName");
            TextBox txtScore = (TextBox)e.Item.FindControl("txtScore");
            Button btnDelete = (Button)e.Item.FindControl("btnDelete");
            Button btnUpdate = (Button)e.Item.FindControl("btnUpdate");
            HtmlTableCell cel_Score = (HtmlTableCell)e.Item.FindControl("cel_Score");
            DataRowView drv = (DataRowView)e.Item.DataItem;

            lblName.Text = drv["CPSM_SUBJECT_NAME"].ToString();

            cel_Score.Attributes["onclick"] = "document.getElementById('" + txtScore.ClientID + "').focus();";
            if (Information.IsNumeric(drv["Min_Score"]))
            {
                txtScore.Text = GL.StringFormatNumber(GL.CDBL(drv["Min_Score"]));
                if (GL.CINT(drv["Min_Score"]) > 100 | GL.CINT(drv["Min_Score"]) <= 0)
                {
                    txtScore.Style["color"] = "red";
                }
                else
                {
                    txtScore.Style["color"] = "black";
                }
            }
            CL.ImplementJavaFloatText(txtScore);
            txtScore.Attributes["onchange"] = "document.getElementById('" + btnUpdate.ClientID + "').click();";

            btnDelete.CommandArgument = drv["CPSM_SUBJECT_CODE"].ToString();
            btnUpdate.CommandArgument = drv["CPSM_SUBJECT_CODE"].ToString();
        }


        #region "TrainingDialog"

        protected void rptTrainingDialog_ItemCommand(object source, System.Web.UI.WebControls.RepeaterCommandEventArgs e)
        {
            switch (e.CommandName)
            {
                case "Select":
                    Button btnTrainingSelect = (Button)e.Item.FindControl("btnTrainingSelect");
                    string COURSE_ID = btnTrainingSelect.CommandArgument;

                    string SQL_Setting = "";
                    SQL_Setting += " SELECT * \n";
                    SQL_Setting += " FROM tb_Path_Setting\n";
                    SQL_Setting += " WHERE POS_No='" + POS_NO + "'\n";
                    SqlDataAdapter DA_Setting = new SqlDataAdapter(SQL_Setting, BL.ConnectionString());
                    DataTable DT_Setting = new DataTable();
                    DA_Setting.Fill(DT_Setting);
                    if (DT_Setting.Rows.Count == 0)
                    {
                        AutoSave();
                    }

                    string SQL = "SElECT * FROM tb_Path_Setting_Training \n";
                    SQL += " WHERE POS_No='" + POS_NO + "'\n";
                    SQL += " AND COURSE_ID='" + COURSE_ID + "'\n";

                    DataTable DT = new DataTable();
                    SqlDataAdapter DA = new SqlDataAdapter(SQL, BL.ConnectionString());
                    DA.Fill(DT);

                    DataRow DR = null;
                    if (DT.Rows.Count == 0)
                    {
                        DR = DT.NewRow();
                        DR["POS_No"] = POS_NO;
                        DR["COURSE_ID"] = COURSE_ID;
                    }
                    else
                    {
                        DR = DT.Rows[0];
                    }
                    //DR("Update_By") = Session("")
                    DR["Update_Time"] = DateAndTime.Now;
                    if (DT.Rows.Count == 0)
                        DT.Rows.Add(DR);

                    SqlCommandBuilder cmd = new SqlCommandBuilder();
                    cmd = new SqlCommandBuilder(DA);
                    DA.Update(DT);
                    //-----bind ตารางกำหนดหลักสูตร------
                    BindTrainingDetail();
                    ModalTraining.Visible = false;
                    break;
            }
        }

        string LastGroup_Course = "";
        protected void rptTrainingDialog_ItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType != ListItemType.AlternatingItem & e.Item.ItemType != ListItemType.Item)
                return;
            Label lblGroup_Course = (Label)e.Item.FindControl("lblGroup_Course");
            Label lblNo = (Label)e.Item.FindControl("lblNo");
            Label lblName = (Label)e.Item.FindControl("lblName");
            Button btnTrainingSelect = (Button)e.Item.FindControl("btnTrainingSelect");

            DataRowView drv = (DataRowView)e.Item.DataItem;

            if (LastGroup_Course != drv["SUBJ_G_DESC"].ToString())
            {
                lblGroup_Course.Text = drv["SUBJ_G_DESC"].ToString();
                LastGroup_Course = drv["SUBJ_G_DESC"].ToString();
            }

            lblNo.Text = drv["COURSE_ID"].ToString().PadLeft(3, GL.chr0);
            lblName.Text = drv["COURSE_DESC"].ToString().ToString();
            btnTrainingSelect.CommandArgument = drv["COURSE_ID"].ToString();
        }

        protected void SearchTraining(object sender, System.EventArgs e)
        {
            BindTrainingList();
        }

        private void BindTrainingList()
        {
            string SQL = "";

            SQL += " SELECT tb_PK_SUBJ_G.SUBJ_G_DESC ,  tb_PK_COURSE.* FROM tb_PK_COURSE\n";
            SQL += " LEFT JOIN tb_PK_SUBJ_G ON tb_PK_COURSE .SUBJ_G_ID =tb_PK_SUBJ_G .SUBJ_G_ID\n";
            SQL += " WHERE tb_PK_COURSE.SUBJ_G_ID Is Not NULL And COURSE_DESC Is Not NULL \n";


            string Filter = "";
            if (!string.IsNullOrEmpty(txtSearchTraining.Text))
            {
                Filter += " (COURSE_ID LIKE '%" + txtSearchTraining.Text.Replace("'", "''") + "%' OR \n";
                Filter += " COURSE_DESC LIKE '%" + txtSearchTraining.Text.Replace("'", "''") + "%') AND ";
            }

            if (BindGroup_Course.SelectedIndex > 0)
            {
                Filter += " tb_PK_SUBJ_G.SUBJ_G_ID='" + BindGroup_Course.Items[BindGroup_Course.SelectedIndex].Value + "'  AND\n";
            }

            if (!string.IsNullOrEmpty(EXIST_Training))
            {
                Filter += " COURSE_ID NOT IN (" + EXIST_Training + ") AND ";
            }

            if (!string.IsNullOrEmpty(Filter))
            {
                SQL += "  AND " + Filter.Substring(0, Filter.Length - 4) + "\n";
            }
            SQL += " \n";

            SQL += " ORDER BY tb_PK_SUBJ_G.SUBJ_G_DESC ,  COURSE_ID,COURSE_DESC\n";

            SqlDataAdapter DA = new SqlDataAdapter(SQL, BL.ConnectionString());
            DataTable DT = new DataTable();
            DA.Fill(DT);

            Session["CP_Setting_PropertyTraining"] = DT;
            Pager_Training.SesssionSourceName = "CP_Setting_PropertyTraining";
            Pager_Training.RenderLayout();

            if (DT.Rows.Count == 0)
            {
                lblCountTraining.Text = "ไม่พบรายการดังกล่าว";
            }
            else
            {
                lblCountTraining.Text = "พบ " + GL.StringFormatNumber(DT.Rows.Count.ToString(), 0) + " รายการ";
            }

        }

        protected void Pager_Training_PageChanging(PageNavigation Sender)
        {
            Pager_Training.TheRepeater = rptTrainingDialog;
        }



        #endregion



        #region "TestDialog"

        protected void rptTestDialog_ItemCommand(object source, System.Web.UI.WebControls.RepeaterCommandEventArgs e)
        {
            switch (e.CommandName)
            {
                case "Select":
                    Button btnTestSelect = (Button)e.Item.FindControl("btnTestSelect");
                    string SUBJECT_CODE = btnTestSelect.CommandArgument;

                    string SQL_Setting = "";
                    SQL_Setting += " SELECT * \n";
                    SQL_Setting += " FROM tb_Path_Setting\n";
                    SQL_Setting += " WHERE POS_No='" + POS_NO + "'\n";
                    SqlDataAdapter DA_Setting = new SqlDataAdapter(SQL_Setting, BL.ConnectionString());
                    DataTable DT_Setting = new DataTable();
                    DA_Setting.Fill(DT_Setting);
                    if (DT_Setting.Rows.Count == 0)
                    {
                        AutoSave();
                    }


                    string SQL = "SElECT * FROM tb_Path_Setting_Test \n";
                    SQL += " WHERE POS_No='" + POS_NO + "'\n";
                    SQL += " AND SUBJECT_CODE='" + SUBJECT_CODE + "'\n";

                    DataTable DT = new DataTable();
                    SqlDataAdapter DA = new SqlDataAdapter(SQL, BL.ConnectionString());
                    DA.Fill(DT);

                    DataRow DR = null;
                    if (DT.Rows.Count == 0)
                    {
                        DR = DT.NewRow();
                        DR["POS_No"] = POS_NO;
                        DR["SUBJECT_CODE"] = SUBJECT_CODE;
                    }
                    else
                    {
                        DR = DT.Rows[0];
                    }
                    DR["Min_Score"] = GL.StringFormatNumber(0);
                    DR["Update_By"] = Session["USER_PSNL_NO"].ToString();
                    DR["Update_Time"] = DateAndTime.Now;
                    if (DT.Rows.Count == 0)
                        DT.Rows.Add(DR);

                    SqlCommandBuilder cmd = new SqlCommandBuilder();
                    cmd = new SqlCommandBuilder(DA);
                    DA.Update(DT);
                    //-----bind ตารางกำหนดหลักสูตร------
                    BindTestDetail();
                    ModalTest.Visible = false;
                    break;
            }

        }

        protected void rptTestDialog_ItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType != ListItemType.AlternatingItem & e.Item.ItemType != ListItemType.Item)
                return;
            Label lblNo = (Label)e.Item.FindControl("lblNo");
            Label lblName = (Label)e.Item.FindControl("lblName");
            Button btnTestSelect = (Button)e.Item.FindControl("btnTestSelect");

            DataRowView drv = (DataRowView)e.Item.DataItem;
            lblNo.Text = drv["CPSM_SUBJECT_CODE"].ToString().PadLeft(3, GL.chr0);
            lblName.Text = drv["CPSM_SUBJECT_NAME"].ToString();
            btnTestSelect.CommandArgument = drv["CPSM_SUBJECT_CODE"].ToString();
        }


        protected void btnSearchTest_Click(object sender, System.EventArgs e)
        {
            BindTestList();
        }

        private void BindTestList()
        {
            string SQL = "";
            SQL += " SELECT * FROM tb_CP_SUBJECT_MASTER\n";
            string Filter = "";
            if (!string.IsNullOrEmpty(txtSearchTest.Text))
            {
                Filter += " (CPSM_SUBJECT_CODE LIKE '%" + txtSearchTest.Text.Replace("'", "''") + "%' OR \n";
                Filter += " CPSM_SUBJECT_NAME LIKE '%" + txtSearchTest.Text.Replace("'", "''") + "%') AND ";
            }

            if (!string.IsNullOrEmpty(EXIST_Test))
            {
                Filter += " CPSM_SUBJECT_CODE NOT IN (" + EXIST_Test + ") AND ";
            }

            if (!string.IsNullOrEmpty(Filter))
            {
                SQL += " WHERE " + Filter.Substring(0, Filter.Length - 4) + "\n";
            }
            SQL += " \n";

            SQL += "  ORDER BY CPSM_SUBJECT_CODE,CPSM_SUBJECT_NAME\n";

            SqlDataAdapter DA = new SqlDataAdapter(SQL, BL.ConnectionString());
            DataTable DT = new DataTable();
            DA.Fill(DT);

            Session["CP_Setting_PropertyTest"] = DT;
            Pager_Test.SesssionSourceName = "CP_Setting_PropertyTest";
            Pager_Test.RenderLayout();

            if (DT.Rows.Count == 0)
            {
                lblCountTest.Text = "ไม่พบรายการดังกล่าว";
            }
            else
            {
                lblCountTest.Text = "พบ " + GL.StringFormatNumber(DT.Rows.Count, 0) + " รายการ";
            }

        }



        protected void Pager_Test_PageChanging(PageNavigation Sender)
        {
            Pager_Test.TheRepeater = rptTestDialog;
        }


        #endregion

        protected void txtAss_Min_Score_TextChanged(object sender, System.EventArgs e)
        {
            if (!string.IsNullOrEmpty(txtAss_Min_Score.Text))
            {
                if (Convert.ToDouble(txtAss_Min_Score.Text) > 500 | Convert.ToDouble(txtAss_Min_Score.Text) < 0)
                {
                    ScriptManager.RegisterStartupScript(this.Page, typeof(string), "Alert", "showAlert('กรอกคะแนน  KPI + Competency  ระหว่าง 1.00 - 500.00 คะแนน');", true);
                    txtAss_Min_Score.Text = "";
                    return;
                }
            }
            if (!string.IsNullOrEmpty(txtLeave_Score.Text))
            {
                if (Convert.ToDouble(txtLeave_Score.Text) < 0)
                {
                    ScriptManager.RegisterStartupScript(this.Page, typeof(string), "Alert", "showAlert('ไม่สามาถรกรอก จำนวนติดลบได้');", true);
                    txtLeave_Score.Text = "";
                    return;
                }
            }



            AutoSave();
        }

        protected void btnOK_Click(object sender, System.EventArgs e)
        {
            AutoSave();

            ScriptManager.RegisterStartupScript(this.Page, typeof(string), "Alert", "showAlert('บันทึกเกณฑ์การเลื่อนตำแหน่งเรียบร้อยแล้ว');", true);
            BindPosition();
        }


        private void AutoSave()
        {
            string SQL = "";
            SQL += " SELECT * \n";
            SQL += " FROM tb_Path_Setting\n";
            SQL += " WHERE POS_No='" + POS_NO + "'\n";
            SqlDataAdapter DA = new SqlDataAdapter(SQL, BL.ConnectionString());
            DataTable DT = new DataTable();
            DA.Fill(DT);

            DataRow DR = null;
            if (DT.Rows.Count == 0)
            {
                DR = DT.NewRow();
                DR["POS_No"] = POS_NO;

            }
            else
            {
                DR = DT.Rows[0];
            }
            if (!string.IsNullOrEmpty(txtAss_Min_Score.Text))
            {
                DR["Ass_Min_Score"] = txtAss_Min_Score.Text.Replace(",", "");
            }
            else
            {
                DR["Ass_Min_Score"] = DBNull.Value;
            }
            if (!string.IsNullOrEmpty(txtLeave_Score.Text))
            {
                DR["Leave_Score"] = txtLeave_Score.Text.Replace(",", "");
            }
            else
            {
                DR["Leave_Score"] = DBNull.Value;
            }
            if (!string.IsNullOrEmpty(txtArrive_Late.Text))
            {
                DR["Arrive_Late"] = txtArrive_Late.Text.Replace(",", "");
            }
            else
            {
                DR["Arrive_Late"] = DBNull.Value;
            }

            if (chkPunishment.Checked)
            {
                DR["Punishment"] = true;
            }
            else
            {
                DR["Punishment"] = false;
            }

            //If DT.Rows(0).Item("Punishment") Then
            //    btnCheck_Punishment.ImageUrl = "images/checkCP.png"
            //    chkPunishment.Checked = True
            //Else
            //    btnCheck_Punishment.ImageUrl = "images/noneCP.png"
            //    chkPunishment.Checked = False
            //End If

            switch (btnCheck_Punishment.ImageUrl)
            {
                case "images/checkCP.png":
                    DR["Punishment"] = true;
                    break;
                default:
                    DR["Punishment"] = false;
                    break;
            }



            DR["Update_By"] = Session["USER_PSNL_NO"].ToString();
            DR["Update_Time"] = DateAndTime.Now;
            if (DT.Rows.Count == 0)
                DT.Rows.Add(DR);
            SqlCommandBuilder cmd = new SqlCommandBuilder();
            cmd = new SqlCommandBuilder(DA);
            DA.Update(DT);

        }

        protected void btnBack_Click(object sender, System.EventArgs e)
        {
            PnlList.Visible = true;
            pnlEdit.Visible = false;
            if (!string.IsNullOrEmpty(Request.QueryString["POS_NO"]))
            {
                BindPosition();
            }
            //ClearForm();
            BindPosition();
        }

        private void BindPath_Position()
        {
            lblCurrentPos.Text = lblPos_Head.Text + " ระดับ " + lblClass_Head.Text + " หน่วยงาน" + lblDept_Head.Text;
        }


        #region "PosDialog"

        private void BindPostList()
        {


            string filter = "";
            string SQL = "";
            SQL += "  SELECT DEPT.SECTOR_CODE , DEPT.SECTOR_NAME,DEPT.DEPT_NAME,DEPT.DEPT_CODE ,POS.POS_NO,ISNULL(POS.MGR_NAME,'-') MGR_NAME,ISNULL(POS.FLD_NAME,'-') FLD_NAME,POS.PNPO_CLASS\n";
            SQL += "  FROM vw_PN_Position POS \n";
            SQL += "  INNER JOIN vw_HR_Department DEPT ON POS.DEPT_CODE=DEPT.DEPT_CODE AND POS.MINOR_CODE=DEPT.MINOR_CODE\n";
            SQL += "  LEFT JOIN vw_PN_PSNL PSN ON POS.POS_NO=PSN.POS_NO\n";
            SQL += "  INNER JOIN vw_Path_Setting_Status Setting_Status ON Setting_Status.POS_No = POS.POS_NO\n";
            SQL += "  WHERE POS.PNPO_CLASS IS NOT NULL \n";

            if (ddl_Search_Sector.SelectedIndex > 0)
            {
                filter += " AND (DEPT.SECTOR_CODE='" + ddl_Search_Sector.Items[ddl_Search_Sector.SelectedIndex].Value + "' \n";
                if (ddl_Search_Sector.Items[ddl_Search_Sector.SelectedIndex].Value == "3200")
                {
                    filter += " AND DEPT.DEPT_CODE NOT IN ('32000300'))\n ";
                    //-----------ฝ่ายโรงงานผลิตยาสูบ 3
                }
                else if (ddl_Search_Sector.Items[ddl_Search_Sector.SelectedIndex].Value == "3203")
                {
                    filter += " OR DEPT.DEPT_CODE IN ('32000300','32030000','32030100','32030200','32030300','32030500'))\n";
                    //-----------ฝ่ายโรงงานผลิตยาสูบ 4
                }
                else if (ddl_Search_Sector.Items[ddl_Search_Sector.SelectedIndex].Value == "3204")
                {
                    filter += " OR DEPT.DEPT_CODE IN ('32040000','32040100','32040200','32040300','32040500','32040300'))\n";
                    //-----------ฝ่ายโรงงานผลิตยาสูบ 5
                }
                else if (ddl_Search_Sector.Items[ddl_Search_Sector.SelectedIndex].Value == "3205")
                {
                    filter += " OR DEPT.DEPT_CODE IN ('32050000','32050100','32050200','32050300','32050500'))\t";
                }
                else
                {
                    filter += ")\n";
                }
            }


            if (!string.IsNullOrEmpty(txt_Search_Name.Text))
            {
                SQL += " AND (DEPT.SECTOR_NAME LIKE '%" + txt_Search_Name.Text.Replace("'", "''") + "%' OR \n";
                SQL += " DEPT.DEPT_NAME LIKE '%" + txt_Search_Name.Text.Replace("'", "''") + "%' ) \n";
            }

            if (!string.IsNullOrEmpty(txtSearchPos.Text))
            {
                SQL += " AND (POS.MGR_NAME LIKE '%" + txtSearchPos.Text.Replace("'", "''") + "%' OR \n";
                SQL += " POS.POS_NO LIKE '%" + txtSearchPos.Text.Replace("'", "''") + "%' OR \n";
                SQL += " POS.FLD_NAME LIKE '%" + txtSearchPos.Text.Replace("'", "''") + "%' ) \n";
            }


            filter += " AND POS.POS_NO NOT IN (" + POS_NO + ")  \n";

            switch (Mode_Pos)
            {
                case "Left":
                    filter += " AND  POS.PNPO_CLASS IN (" + Convert.ToInt32(lblClass_Head.Text) + "," + (Convert.ToInt32(lblClass_Head.Text) - 1) + "," + (Convert.ToInt32(lblClass_Head.Text) - 2) + ")\n";
                    break;
                case "Right":
                    filter += " AND  POS.PNPO_CLASS IN (" + Convert.ToInt32(lblClass_Head.Text) + "," + (Convert.ToInt32(lblClass_Head.Text) + 1) + "," + (Convert.ToInt32(lblClass_Head.Text) + 2) + ")\n";
                    break;
                default:
                    break;
            }

            SQL += "  " + filter;

            SQL += "  GROUP BY DEPT.SECTOR_CODE,DEPT.SECTOR_NAME,DEPT.DEPT_NAME,DEPT.DEPT_CODE ,POS.POS_NO,POS.FLD_NAME,POS.MGR_NAME,\n";
            SQL += "  POS.PNPO_CLASS, DEPT.SECTOR_CODE, DEPT.DEPT_CODE, POS.PNPO_CLASS, POS.POS_NO\n";
            SQL += "  ORDER BY DEPT.SECTOR_CODE,DEPT.DEPT_CODE,POS.PNPO_CLASS DESC,POS.POS_NO\n";

            SqlDataAdapter DA = new SqlDataAdapter(SQL, BL.ConnectionString());
            DataTable DT = new DataTable();
            DA.Fill(DT);

            DataTable DTRight = new DataTable();
            DTRight = (DataTable)Session["CP_Setting_PropertyPos_Right"];

            DT.Columns.Add("SelectPos", typeof(Boolean));

            for (int i = 0; i <= DT.Rows.Count - 1; i++)
            {

                DTRight.DefaultView.RowFilter = "POS_NO='" + DT.Rows[i]["POS_NO"] + "'";
                if (DTRight.DefaultView.Count > 0)
                {
                    DT.Rows[i]["SelectPos"] = 1;
                }
                else
                {
                    DT.Rows[i]["SelectPos"] = 0;
                }
            }


            //EXIST_Pos = "";
            Session["CP_Setting_PropertyPos_Dialog"] = DT;
            Pager_Pos.SesssionSourceName = "CP_Setting_PropertyPos_Dialog";
            Pager_Pos.RenderLayout();

            if (btnSelectAll.ImageUrl == "images/none.png")
            {
                Mode_Select = " ยืนยันเลือกตำแหน่งทั้งหมด ";
            }
            else
            {
                Mode_Select = " ยืนยันลบตำแหน่งทั้งหมด ";
            }
            btnDeleteLeft_Confirm.ConfirmText = Mode_Select + " " + DT.Rows.Count + " ตำแหน่ง";

            if (DT.Rows.Count == 0)
            {
                lblCountPosNo.Text = "ไม่พบรายการดังกล่าว";
            }
            else
            {
                lblCountPosNo.Text = "พบ " + GL.StringFormatNumber(DT.Rows.Count, 0) + " รายการ";
            }

        }

        protected void btnAddLeft_ServerClick(object sender, System.EventArgs e)
        {
            //------------- Get Existing  List ---------------
            lblHeader_ModalPost.Text = "เลือกตำแหน่งที่สามารถเข้ารับได้";
            lblModePos.Text = "Left";
            Mode_Pos = lblModePos.Text;
            string _Poslist = "";
            foreach (RepeaterItem item in rptPosLeft.Items)
            {
                if (item.ItemType != ListItemType.AlternatingItem & item.ItemType != ListItemType.Item)
                    continue;
                Button btnDeleteLeft = (Button)item.FindControl("btnDeleteLeft");
                _Poslist += "'" + btnDeleteLeft.CommandArgument + "'" + ",";
            }
            if (!string.IsNullOrEmpty(_Poslist))
            {
                EXIST_Pos += _Poslist.Substring(0, _Poslist.Length - 1);
            }
            else
            {
                EXIST_Pos += "";
            }

            BindPostList();
            ModalPost.Visible = true;
            ScriptManager.RegisterStartupScript(this.Page, typeof(string), "Focus", "document.getElementById('" + txtSearchPos.ClientID + "').focus();", true);
        }

        protected void Pager_Pos_PageChanging(PageNavigation Sender)
        {
            Pager_Pos.TheRepeater = rptPosDialog;

        }

        string LastSectorPos = "";
        string LastDeptPos = "";
        protected void rptPosDialog_ItemCommand(object source, System.Web.UI.WebControls.RepeaterCommandEventArgs e)
        {
            Button btnPosSelect = (Button)e.Item.FindControl("btnPosSelect");
            string To_POS_No = btnPosSelect.CommandArgument;
            SqlDataAdapter DA;
            DataTable DT;
            SqlCommandBuilder cmd;

            string SQL = "";
            SQL += "  SELECT * \n";
            SQL += "  FROM  tb_Path_Setting_Route \n";
            SQL += "  WHERE From_POS_No ='" + POS_NO + "'\n";
            SQL += "  AND To_POS_No='" + To_POS_No + "'\n";
            DA = new SqlDataAdapter(SQL, BL.ConnectionString());
            DT = new DataTable();
            DA.Fill(DT);
            DataRow DR = null;

            switch (e.CommandName)
            {
                case "Select":

                    if (DT.Rows.Count == 0)
                    {
                        DR = DT.NewRow();
                        DR["From_POS_No"] = POS_NO;
                    }
                    else
                    {
                        DR = DT.Rows[0];
                    }
                    DR["To_POS_No"] = To_POS_No;
                    DR["Update_By"] = (Session["USER_PSNL_NO"]);
                    DR["Update_Time"] = DateAndTime.Now;
                    if (DT.Rows.Count == 0)
                    {
                        DT.Rows.Add(DR);
                    }
                    cmd = new SqlCommandBuilder(DA);
                    DA.Update(DT);
                    //-----bind ตารางตำแหน่ง------
                    BindPosLeft();
                    BindPosRight();
                    ModalPost.Visible = false;
                    break;


                case "SelectPos":
                    AutoSave();
                    ImageButton btnSelectPos = (ImageButton)e.Item.FindControl("btnSelectPos");
                    if (btnSelectPos.ImageUrl == "images/check.png")
                    {
                        btnSelectPos.ImageUrl = "images/none.png";
                        SQL = "";
                        SQL += "  DELETE ";
                        SQL += "  FROM  tb_Path_Setting_Route \n";
                        SQL += "  WHERE From_POS_No ='" + POS_NO + "'\n";
                        SQL += "  AND To_POS_No='" + To_POS_No + "'\n";
                        DA = new SqlDataAdapter(SQL, BL.ConnectionString());
                        DT = new DataTable();
                        DA.Fill(DT);
                        BindPosRight();
                    }
                    else
                    {
                        btnSelectPos.ImageUrl = "images/check.png";

                        if (DT.Rows.Count == 0)
                        {
                            DR = DT.NewRow();
                            DR["From_POS_No"] = POS_NO;
                        }
                        else
                        {
                            DR = DT.Rows[0];
                        }
                        DR["To_POS_No"] = To_POS_No;
                        DR["Update_By"] = (Session["USER_PSNL_NO"]);
                        DR["Update_Time"] = DateAndTime.Now;
                        if (DT.Rows.Count == 0)
                        {
                            DT.Rows.Add(DR);
                        }
                        cmd = new SqlCommandBuilder(DA);
                        DA.Update(DT);
                        //-----bind ตารางตำแหน่ง------
                        BindPosRight();


                    }

                    break;

            }



        }
        protected void rptPosDialog_ItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType != ListItemType.AlternatingItem & e.Item.ItemType != ListItemType.Item)
                return;
            Label lblSector = (Label)e.Item.FindControl("lblSector");
            Label lblDept = (Label)e.Item.FindControl("lblDept");
            Label lblClass = (Label)e.Item.FindControl("lblClass");
            Label lblPosNo = (Label)e.Item.FindControl("lblPosNo");
            Label lblMGR = (Label)e.Item.FindControl("lblMGR");
            Label lblPSN = (Label)e.Item.FindControl("lblPSN");
            Button btnPosSelect = (Button)e.Item.FindControl("btnPosSelect");
            ImageButton btnSelectPos = (ImageButton)e.Item.FindControl("btnSelectPos");
            DataRowView drv = (DataRowView)e.Item.DataItem;
            if (LastSectorPos != drv["SECTOR_NAME"].ToString())
            {
                lblSector.Text = drv["SECTOR_NAME"].ToString();
                LastSectorPos = drv["SECTOR_NAME"].ToString();
                lblDept.Text = drv["DEPT_NAME"].ToString();
                LastDeptPos = drv["DEPT_NAME"].ToString();
            }
            else if (LastDeptPos != drv["DEPT_NAME"].ToString())
            {
                lblDept.Text = drv["DEPT_NAME"].ToString();
                LastDeptPos = drv["DEPT_NAME"].ToString();
            }
            lblClass.Text = GL.CINT(drv["PNPO_CLASS"]).ToString();
            if (GL.IsEqualNull(drv["POS_NO"]))
            {
                lblPosNo.Text = "-";
            }
            else
            {
                lblPosNo.Text = drv["POS_NO"].ToString() + ":" + drv["FLD_NAME"].ToString();
            }
            lblMGR.Text = drv["MGR_NAME"].ToString();


            if (GL.CBOOL(drv["SelectPos"]))
            {
                btnSelectPos.ImageUrl = "images/check.png";
            }
            else
            {
                btnSelectPos.ImageUrl = "images/none.png";
            }

            btnPosSelect.CommandArgument = drv["POS_NO"].ToString();

        }

        protected void btnSearchPos_Click(object sender, System.EventArgs e)
        {
            BindPostList();
        }


        private void BindPosLeft()
        {
            BindCourse_Detail();
            BindTest_Detail();

            string SQL = "";
            SQL += "  SELECT To_POS_No  From_POS_No \n";
            SQL += "  ,  From_POS_No To_POS_No \n";
            SQL += "  ,vw_PN_Position.POS_NO\n";
            SQL += "  ,vw_PN_Position.DEPT_CODE ,vw_PN_Position.DEPT_NAME\n";
            SQL += "  ,vw_PN_Position.PNPO_CLASS\n";
            SQL += "  ,vw_PN_Position.MGR_NAME\n";
            SQL += "  FROM  tb_Path_Setting_Route Setting_Route\n";
            SQL += "  LEFT JOIN tb_Path_Setting Path_Setting  ON Path_Setting.POS_No=Setting_Route.To_POS_No\n";
            SQL += "  INNER JOIN vw_PN_Position ON vw_PN_Position.POS_NO=Setting_Route.From_POS_No\n";
            SQL += "  WHERE To_POS_No ='" + POS_NO + "'\n";
            SQL += "  ORDER BY vw_PN_Position.PNPO_CLASS DESC,vw_PN_Position.DEPT_CODE,vw_PN_Position.POS_NO\n";

            SqlDataAdapter DA = new SqlDataAdapter(SQL, BL.ConnectionString());
            DataTable DT = new DataTable();
            DA.Fill(DT);
            rptPosLeft.DataSource = DT;
            rptPosLeft.DataBind();
        }

        private void BindPosRight()
        {
            BindCourse_Detail();
            BindTest_Detail();

            string SQL = "";
            SQL += "  SELECT From_POS_No , To_POS_No \n";
            SQL += "  ,vw_PN_Position.POS_NO\n";
            SQL += "  ,vw_PN_Position.DEPT_CODE ,vw_PN_Position.DEPT_NAME\n";
            SQL += "  ,vw_PN_Position.PNPO_CLASS\n";
            SQL += "  ,vw_PN_Position.MGR_NAME\n";
            SQL += "  FROM  tb_Path_Setting_Route Setting_Route\n";
            SQL += "  LEFT JOIN tb_Path_Setting Path_Setting  ON Path_Setting.POS_No=Setting_Route.From_POS_No\n";
            SQL += "  INNER JOIN vw_PN_Position ON vw_PN_Position.POS_NO=Setting_Route.To_POS_No\n";
            SQL += "  WHERE From_POS_No ='" + POS_NO + "'\n";
            SQL += "  ORDER BY vw_PN_Position.PNPO_CLASS DESC,vw_PN_Position.DEPT_CODE ,  vw_PN_Position.POS_NO\n";
            SqlDataAdapter DA = new SqlDataAdapter(SQL, BL.ConnectionString());
            DataTable DT = new DataTable();
            DA.Fill(DT);

            Session["CP_Setting_PropertyPos_Right"] = DT;
            rptPosRight.DataSource = DT;
            rptPosRight.DataBind();
        }


        private void CurrentPos()
        {
            string SQL = "";
            SQL += " SELECT DEPT.SECTOR_CODE,DEPT.SECTOR_NAME,DEPT.DEPT_CODE,DEPT.DEPT_NAME,POS.POS_NO,ISNULL(POS.MGR_NAME,'-') MGR_NAME,ISNULL(POS.FLD_NAME,'-') FLD_NAME,\n";
            SQL += "  POS.PNPO_CLASS,PSN.PSNL_NO,PSN.PSNL_Fullname,POS.Update_Time \n";
            SQL += "  ,Setting_Status.Status_Path\n";
            SQL += "  FROM vw_PN_Position POS \n";
            SQL += "  INNER JOIN vw_HR_Department DEPT ON POS.DEPT_CODE=DEPT.DEPT_CODE AND POS.MINOR_CODE=DEPT.MINOR_CODE\n";
            SQL += "  LEFT JOIN vw_PN_PSNL PSN ON POS.POS_NO=PSN.POS_NO\n";
            SQL += "  INNER JOIN vw_Path_Setting_Status Setting_Status ON Setting_Status.POS_No = POS.POS_NO\n";
            SQL += "  WHERE POS.PNPO_CLASS IS NOT NULL   \n";
            SQL += "  AND POS.POS_NO='" + POS_NO + "'\n";
            SQL += "  ";
            SQL += "  GROUP BY DEPT.SECTOR_NAME,DEPT.DEPT_NAME,POS.POS_NO,POS.FLD_NAME,POS.MGR_NAME,\n";
            SQL += "  POS.PNPO_CLASS, PSN.PSNL_NO, PSN.PSNL_Fullname, POS.Update_Time, DEPT.SECTOR_CODE, DEPT.DEPT_CODE, POS.PNPO_CLASS, POS.POS_NO\n";
            SQL += "  , Setting_Status.Status_Path\n";
            SQL += "  ORDER BY DEPT.SECTOR_CODE,DEPT.DEPT_CODE,POS.PNPO_CLASS DESC,POS.POS_NO\n";
            SqlDataAdapter DA = new SqlDataAdapter(SQL, BL.ConnectionString());
            DataTable DT = new DataTable();
            DA.Fill(DT);
            if (DT.Rows.Count == 1)
            {
                //-----------------แสดงรายละเอียดตำแหน่ง--------------
                lblPos_Head.Text = DT.Rows[0]["POS_NO"].ToString() + " : " + DT.Rows[0]["MGR_NAME"].ToString();
                lblDept_Head.Text = DT.Rows[0]["DEPT_NAME"].ToString();
                if (!GL.IsEqualNull(DT.Rows[0]["PNPO_CLASS"]))
                {
                    lblClass_Head.Text = Convert.ToInt32(DT.Rows[0]["PNPO_CLASS"]).ToString();
                }
                else
                {
                    lblClass_Head.Text = "-";
                }

                if (!GL.IsEqualNull(DT.Rows[0]["PSNL_NO"]))
                {
                    lblPSN_Head.Text = DT.Rows[0]["PSNL_NO"].ToString() + " : " + DT.Rows[0]["PSNL_Fullname"].ToString();
                }
                else
                {
                    lblPSN_Head.Text = "ไม่มีผู้ครองตำแหน่ง";
                }

                //-----ผลการประเมิน--,วันลา สาย ขาด ,โทษทางวินัย------
                Path_Setting();
                //-----การฝึกอบรม----------
                BindTrainingDetail();
                //-----การทดสอบ----------
                BindTestDetail();
                //-----กำหนดเส้นทาง---------
                BindPath_Position();
                //------LEFT/RIGHT----------
                if (Convert.ToInt32(lblClass_Head.Text) == 14)
                {
                    //------LEFT----------
                    tdPosLeft_Header.Visible = true;
                    tdPosLeft_Detail.Visible = true;
                    tdPosLeft_rpt.Visible = true;
                    tdimgPosLeft_Header.Visible = true;
                    tdimgPosLeft_Detail.Visible = true;
                    tdimgPosLeft_rpt.Visible = true;
                    //------RIGHT----------
                    tdPosRight_Header.Visible = false;
                    tdPosRight_Detail.Visible = false;
                    tdPosRight_rpt.Visible = false;
                    tdimgPosRight_Header.Visible = false;
                    tdimgPosRight_Detail.Visible = false;
                    tdimgPosRight_rpt.Visible = false;
                    BindPosLeft();
                }
                else if (Convert.ToInt32(lblClass_Head.Text) == 1)
                {
                    //------LEFT----------
                    tdPosLeft_Header.Visible = false;
                    tdPosLeft_Detail.Visible = false;
                    tdPosLeft_rpt.Visible = false;
                    tdimgPosLeft_Header.Visible = false;
                    tdimgPosLeft_Detail.Visible = false;
                    tdimgPosLeft_rpt.Visible = false;
                    //------RIGHT----------
                    tdPosRight_Header.Visible = true;
                    tdPosRight_Detail.Visible = true;
                    tdPosRight_rpt.Visible = true;
                    tdimgPosRight_Header.Visible = true;
                    tdimgPosRight_Detail.Visible = true;
                    tdimgPosRight_rpt.Visible = true;

                    BindPosRight();
                }
                else
                {
                    //------LEFT----------
                    tdPosLeft_Header.Visible = true;
                    tdPosLeft_Detail.Visible = true;
                    tdPosLeft_rpt.Visible = true;
                    tdimgPosLeft_Header.Visible = true;
                    tdimgPosLeft_Detail.Visible = true;
                    tdimgPosLeft_rpt.Visible = true;
                    //------RIGHT----------
                    tdPosRight_Header.Visible = true;
                    tdPosRight_Detail.Visible = true;
                    tdPosRight_rpt.Visible = true;
                    tdimgPosRight_Header.Visible = true;
                    tdimgPosRight_Detail.Visible = true;
                    tdimgPosRight_rpt.Visible = true;

                    BindPosLeft();
                    BindPosRight();

                }

                //-----กำหนดเส้นทางในฝ่ายเดียวกัน---------
                SECTOR_CODE = DT.Rows[0]["SECTOR_CODE"].ToString();
                DEPT_CODE = DT.Rows[0]["DEPT_CODE"].ToString();
                btnBack.Focus();
            }

        }

        //Protected Sub rptPosLeft_ItemCommand(source As Object, e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles rptPosLeft.ItemCommand
        //    Select Case e.CommandName
        //        Case "Delete"
        //            Dim btnDeleteLeft As Button = e.Item.FindControl("btnDeleteLeft")
        //            Dim POS_Left As String = btnDeleteLeft.CommandArgument
        //            Dim SQL As String = ""
        //            SQL &= "  SELECT * " & vbLf
        //            SQL &= "  FROM  tb_Path_Setting_Route  WHERE From_POS_No ='" & POS_NO & "'" & vbLf
        //            SQL &= "  AND To_POS_No='" & POS_Left & "'" & vbLf
        //            SQL &= "  AND Route='Left' "
        //            Dim DA As New SqlDataAdapter(SQL, BL.ConnectionString())
        //            Dim DT As New DataTable
        //            DA.Fill(DT)
        //            If DT.Rows.Count = 1 Then
        //                SQL = "  "
        //                SQL &= "  Delete From tb_Path_Setting_Route WHERE From_POS_No ='" & POS_NO & "'" & vbLf
        //                SQL &= "  AND To_POS_No='" & POS_Left & "'" & vbLf
        //                SQL &= "  AND Route='Left' "
        //                DA = New SqlDataAdapter(SQL, BL.ConnectionString())
        //                DT = New DataTable
        //                DA.Fill(DT)
        //            Else
        //                SQL = ""
        //                SQL &= "  SELECT *  FROM  tb_Path_Setting_Route WHERE  To_POS_No='" & POS_NO & "'" & vbLf
        //                SQL &= "  AND From_POS_No='" & POS_Left & "'" & vbLf
        //                SQL &= "  AND Route='Right' "
        //                DA = New SqlDataAdapter(SQL, BL.ConnectionString())
        //                DT = New DataTable
        //                DA.Fill(DT)

        //                If DT.Rows.Count > 0 Then
        //                    SQL = ""
        //                    SQL &= "  DELETE  FROM  tb_Path_Setting_Route WHERE  To_POS_No='" & POS_NO & "'" & vbLf
        //                    SQL &= "  AND From_POS_No='" & POS_Left & "'" & vbLf
        //                    SQL &= "  AND Route='Right' "
        //                    DA = New SqlDataAdapter(SQL, BL.ConnectionString())
        //                    DT = New DataTable
        //                    DA.Fill(DT)
        //                End If

        //                'ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "showAlert('ไม่สามารถลบตำแหน่งที่ ตำแหน่ง " & POS_NO & "  ไม่ได้กำหนดเอง');", True)
        //                'Exit Sub
        //            End If
        //            BindPosLeft()
        //        Case "lnkCurrentFromLeft"
        //            Dim btnDeleteLeft As Button = e.Item.FindControl("btnDeleteLeft")
        //            Dim POS_Left As String = btnDeleteLeft.CommandArgument
        //            POS_NO = POS_Left
        //            CurrentPos()
        //    End Select

        //End Sub

        DataTable DT_Course_Detail = null;

        DataTable DT_Test_Detail = null;
        protected void rptPosLeft_ItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType != ListItemType.AlternatingItem & e.Item.ItemType != ListItemType.Item)
                return;
            Label lblPosLeft = (Label)e.Item.FindControl("lblPosLeft");
            Button btnDeleteLeft = (Button)e.Item.FindControl("btnDeleteLeft");
            HtmlAnchor tableLeft = (HtmlAnchor)e.Item.FindControl("tableLeft");

            HtmlAnchor btnDeleteLeft_Close = (HtmlAnchor)e.Item.FindControl("btnDeleteLeft_Close");
            HtmlAnchor lnkL = (HtmlAnchor)e.Item.FindControl("lnkL");

            DataRowView drv = (DataRowView)e.Item.DataItem;

            lblPosLeft.Text = drv["POS_NO"] + " : " + drv["MGR_NAME"].ToString() + " ระดับ " + Convert.ToInt32(drv["PNPO_CLASS"]) + " หน่วยงาน " + drv["DEPT_NAME"].ToString();
            btnDeleteLeft.CommandArgument = drv["POS_NO"].ToString();
            btnDeleteLeft_Close.Attributes["onclick"] = "document.getElementById('" + btnDeleteLeft.ClientID + "').click();";
            btnDeleteLeft_Close.Attributes["btnDeleteLeft_Close"] = drv["POS_NO"].ToString();

            //----------------Show Detail-----------------
            POS_NO_Detail = drv["POS_NO"].ToString();
            //'-----ผลการประเมิน--,วันลา สาย ขาด ,โทษทางวินัย------
            Label lblAss_Min_Score = (Label)e.Item.FindControl("lblAss_Min_Score");
            Label lblLeave_Score = (Label)e.Item.FindControl("lblLeave_Score");
            Label lblPunishment = (Label)e.Item.FindControl("lblPunishment");

            DataTable DT_Path_Setting_Detail = Path_Setting_Detail(POS_NO_Detail);
            if (DT_Path_Setting_Detail.Rows.Count == 1)
            {
                if (Information.IsNumeric(DT_Path_Setting_Detail.Rows[0]["Ass_Min_Score"]))
                {
                    lblAss_Min_Score.Text = "ไม่น้อยกว่า " + GL.StringFormatNumber(GL.CDBL(DT_Path_Setting_Detail.Rows[0]["Ass_Min_Score"])) + " คะแนน";
                }
                else
                {
                    lblAss_Min_Score.Text = " ไม่ระบุ";
                }
                ///**********คะแนนวันลา*****Arrive_Late******/
                if (Information.IsNumeric(DT_Path_Setting_Detail.Rows[0]["Leave_Score"]))
                {
                    lblLeave_Score.Text = " ไม่เกิน " + GL.StringFormatNumber(GL.CDBL(DT_Path_Setting_Detail.Rows[0]["Leave_Score"])) + " วัน";
                }
                else
                {
                    lblLeave_Score.Text = " ไม่ระบุ";
                }

                if (GL.CBOOL(DT_Path_Setting_Detail.Rows[0]["Punishment"]) == true)
                {
                    lblPunishment.Text = "ต้องไม่มีประวัติมีโทษทางวินัยใน 1 ปี ย้อนหลัง";
                    lblPunishment.ForeColor = System.Drawing.Color.Red;
                }
                else
                {
                    lblPunishment.Text = "ไม่ระบุโทษทางวินัย";
                    lblPunishment.ForeColor = System.Drawing.Color.Gray;
                }
            }
            else
            {
                lblAss_Min_Score.Text = "ยังไม่ระบุ";
                lblLeave_Score.Text = "ยังไม่ระบุ";
                lblPunishment.Text = "ไม่ระบุโทษทางวินัย";
            }

            //'-----การฝึกอบรม----------
            Repeater rptCourse = (Repeater)e.Item.FindControl("rptCourse");
            Label lblCount_Course = (Label)e.Item.FindControl("lblCount_Course");
            rptCourse.ItemDataBound += rptCourse_ItemDataBound;
            DT_Course_Detail.DefaultView.RowFilter = "POS_NO='" + drv["POS_NO"] + "'";
            lblCount_Course.Text = DT_Course_Detail.DefaultView.Count.ToString();
            rptCourse.DataSource = DT_Course_Detail.DefaultView.ToTable().Copy();
            rptCourse.DataBind();

            //'-----การทดสอบ----------
            Repeater rptTest = (Repeater)e.Item.FindControl("rptTest");
            Label lblCount_Test = (Label)e.Item.FindControl("lblCount_Test");
            rptTest.ItemDataBound += rptTest_Detail_ItemDataBound;
            DT_Test_Detail.DefaultView.RowFilter = "POS_NO='" + drv["POS_NO"] + "'";
            lblCount_Test.Text = DT_Test_Detail.DefaultView.Count.ToString();
            rptTest.DataSource = DT_Test_Detail.DefaultView.ToTable().Copy();
            rptTest.DataBind();

            lnkL.HRef = "CP_Setting_Property.aspx?POS_NO=" + drv["POS_NO"];
        }


        public DataTable Path_Setting_Detail(string POS_NO_Detail)
        {
            string SQL = "";
            SQL += " SELECT * FROM tb_Path_Setting \n";
            SQL += " WHERE POS_No='" + POS_NO_Detail + "'\n";
            SqlDataAdapter DA = new SqlDataAdapter(SQL, BL.ConnectionString());
            DataTable DT = new DataTable();
            DA.Fill(DT);
            return DT;
        }


        private void BindCourse_Detail()
        {
            string SQL = "";
            SQL += "   SELECT Training.POS_No,COURSE.COURSE_ID,COURSE.COURSE_DESC \n";
            SQL += "   FROM tb_Path_Setting_Training Training\n";
            SQL += "   LEFT JOIN tb_PK_COURSE COURSE ON Training.COURSE_ID=COURSE.COURSE_ID\n";
            SQL += "   ORDER BY Training.POS_No,COURSE.COURSE_ID,COURSE.COURSE_DESC\n";
            SqlDataAdapter DA = new SqlDataAdapter(SQL, BL.ConnectionString());
            DataTable DT = new DataTable();
            DA.Fill(DT);
            DT_Course_Detail = DT;
        }

        protected void rptCourse_ItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType != ListItemType.AlternatingItem & e.Item.ItemType != ListItemType.Item)
                return;
            Label lblCourse = (Label)e.Item.FindControl("lblCourse");

            DataRowView drv = (DataRowView)e.Item.DataItem;

            lblCourse.Text = drv["COURSE_DESC"].ToString();

        }

        private void BindTest_Detail()
        {
            string SQL = "";
            SQL += " SELECT Setting_Test.POS_No,Test.CPSM_SUBJECT_CODE,Test.CPSM_SUBJECT_NAME,Setting_Test.Min_Score \n";
            SQL += " FROM tb_Path_Setting_Test Setting_Test\n";
            SQL += " LEFT JOIN tb_CP_SUBJECT_MASTER Test ON Setting_Test.SUBJECT_CODE=Test.CPSM_SUBJECT_CODE\n";
            SQL += " ORDER BY Setting_Test.POS_No,Test.CPSM_SUBJECT_CODE,Test.CPSM_SUBJECT_NAME\n";
            SqlDataAdapter DA = new SqlDataAdapter(SQL, BL.ConnectionString());
            DataTable DT = new DataTable();
            DA.Fill(DT);
            DT_Test_Detail = DT;
        }

        protected void rptTest_Detail_ItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType != ListItemType.AlternatingItem & e.Item.ItemType != ListItemType.Item)
                return;
            Label lblTest = (Label)e.Item.FindControl("lblTest");
            Label lblMin_Score = (Label)e.Item.FindControl("lblMin_Score");
            DataRowView drv = (DataRowView)e.Item.DataItem;
            lblTest.Text = drv["CPSM_SUBJECT_NAME"].ToString();
            if (Information.IsNumeric(drv["Min_Score"]))
            {
                lblMin_Score.Text = GL.StringFormatNumber(GL.CDBL(drv["Min_Score"]));
            }
            else
            {
                lblMin_Score.Text = "0.00";
            }

        }


        protected void rptPosRight_ItemCommand(object source, System.Web.UI.WebControls.RepeaterCommandEventArgs e)
        {
            Button btnDeleteRight = (Button)e.Item.FindControl("btnDeleteRight");
            string POS_Right;

            switch (e.CommandName)
            {
                case "Delete":

                    POS_Right = btnDeleteRight.CommandArgument;
                    string SQL = "";
                    SQL += "  SELECT *  FROM  tb_Path_Setting_Route \n";
                    SQL += "  WHERE From_POS_No ='" + POS_NO + "'\n";
                    SQL += "  AND To_POS_No='" + POS_Right + "'\n";
                    SqlDataAdapter DA = new SqlDataAdapter(SQL, BL.ConnectionString());
                    DataTable DT = new DataTable();
                    DA.Fill(DT);
                    if (DT.Rows.Count == 1)
                    {
                        SQL = "  ";
                        SQL += "  Delete From tb_Path_Setting_Route";
                        SQL += "  WHERE From_POS_No ='" + POS_NO + "'\n";
                        SQL += "  AND To_POS_No='" + POS_Right + "'\n";
                        DA = new SqlDataAdapter(SQL, BL.ConnectionString());
                        DT = new DataTable();
                        DA.Fill(DT);
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this.Page, typeof(string), "Alert", "showAlert('ไม่สามารถลบตำแหน่งที่ " + POS_NO + "  ได้');", true);
                        return;
                    }
                    BindPosRight();

                    break;
                case "lnkCurrentFromRight":


                    POS_Right = btnDeleteRight.CommandArgument;
                    POS_NO = POS_Right;
                    CurrentPos();
                    break;
            }
        }

        int LastClass = 0;

        //string LastDept = "";
        protected void rptPosRight_ItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType != ListItemType.AlternatingItem & e.Item.ItemType != ListItemType.Item)
                return;
            Label lblPosRight = (Label)e.Item.FindControl("lblPosRight");
            Button btnDeleteRight = (Button)e.Item.FindControl("btnDeleteRight");
            HtmlAnchor lnkR = (HtmlAnchor)e.Item.FindControl("lnkR");
            HtmlAnchor btnDeleteRight_Close = (HtmlAnchor)e.Item.FindControl("btnDeleteRight_Close");
            HtmlControl portletBox = (HtmlControl)e.Item.FindControl("portletBox");

            DataRowView drv = (DataRowView)e.Item.DataItem;

            //portletBox.Attributes["class"] = "portlet box box_4";
            lblPosRight.Text = drv["POS_NO"] + " : " + drv["MGR_NAME"].ToString() + " ระดับ" + Convert.ToInt32(drv["PNPO_CLASS"]) + " หน่วยงาน " + drv["DEPT_NAME"].ToString();
            btnDeleteRight.CommandArgument = drv["POS_NO"].ToString();
            btnDeleteRight_Close.Attributes["onclick"] = "document.getElementById('" + btnDeleteRight.ClientID + "').click();";
            btnDeleteRight_Close.Attributes["btnDeleteRight_Close"] = drv["POS_NO"].ToString();


            //if (LastClass != (Convert.ToInt32(drv["PNPO_CLASS"])))
            //{
            //    portletBox.Attributes["class"] = "portlet box box_" + (Convert.ToInt32(drv["PNPO_CLASS"])) ;
            //}

            if ((Convert.ToInt32(drv["PNPO_CLASS"])) == Convert.ToInt32(lblClass_Head.Text))
            {
                portletBox.Attributes["class"] = "portlet box green";

            }
            else if ((Convert.ToInt32(drv["PNPO_CLASS"])) == (Convert.ToInt32(lblClass_Head.Text) + 1))
            {
                portletBox.Attributes["class"] = "portlet box purple";
            }
            else
            {
                portletBox.Attributes["class"] = "portlet box red";
            }



            //----------------Show Detail-----------------
            POS_NO_Detail = drv["POS_NO"].ToString();
            //'-----ผลการประเมิน--,วันลา สาย ขาด ,โทษทางวินัย------
            Label lblAss_Min_Score = (Label)e.Item.FindControl("lblAss_Min_Score");
            Label lblLeave_Score = (Label)e.Item.FindControl("lblLeave_Score");
            Label lblPunishment = (Label)e.Item.FindControl("lblPunishment");

            DataTable DT_Path_Setting_Detail = Path_Setting_Detail(POS_NO_Detail);
            if (DT_Path_Setting_Detail.Rows.Count == 1)
            {
                if (Information.IsNumeric(DT_Path_Setting_Detail.Rows[0]["Ass_Min_Score"]))
                {
                    lblAss_Min_Score.Text = "ไม่น้อยกว่า " + GL.StringFormatNumber(GL.CDBL(DT_Path_Setting_Detail.Rows[0]["Ass_Min_Score"])) + " คะแนน";
                }
                else
                {
                    lblAss_Min_Score.Text = " ไม่ระบุ";
                }
                ///**********คะแนนวันลา*****Arrive_Late******/
                if (Information.IsNumeric(DT_Path_Setting_Detail.Rows[0]["Leave_Score"]))
                {
                    lblLeave_Score.Text = " ไม่เกิน " + GL.StringFormatNumber(GL.CDBL(DT_Path_Setting_Detail.Rows[0]["Leave_Score"])) + " วัน";
                }
                else
                {
                    lblLeave_Score.Text = " ไม่ระบุ";
                }

                if (GL.CBOOL(DT_Path_Setting_Detail.Rows[0]["Punishment"]) == true)
                {
                    lblPunishment.Text = "ต้องไม่มีประวัติมีโทษทางวินัยใน 1 ปี ย้อนหลัง";
                    lblPunishment.ForeColor = System.Drawing.Color.Red;
                }
                else
                {
                    lblPunishment.Text = "ไม่ระบุโทษทางวินัย";
                    lblPunishment.ForeColor = System.Drawing.Color.Gray;
                }

            }
            else
            {
                lblAss_Min_Score.Text = "ยังไม่ระบุ";
                lblLeave_Score.Text = "ยังไม่ระบุ";
                lblPunishment.Text = "ไม่ระบุโทษทางวินัย";
            }

            //'-----การฝึกอบรม----------
            Repeater rptCourse = (Repeater)e.Item.FindControl("rptCourse");
            Label lblCount_Course = (Label)e.Item.FindControl("lblCount_Course");
            rptCourse.ItemDataBound += rptCourse_ItemDataBound;
            DT_Course_Detail.DefaultView.RowFilter = "POS_NO='" + drv["POS_NO"] + "'";
            lblCount_Course.Text = DT_Course_Detail.DefaultView.Count.ToString();
            rptCourse.DataSource = DT_Course_Detail.DefaultView.ToTable().Copy();
            rptCourse.DataBind();

            //'-----การทดสอบ----------
            Repeater rptTest = (Repeater)e.Item.FindControl("rptTest");
            Label lblCount_Test = (Label)e.Item.FindControl("lblCount_Test");
            rptTest.ItemDataBound += rptTest_Detail_ItemDataBound;
            DT_Test_Detail.DefaultView.RowFilter = "POS_NO='" + drv["POS_NO"] + "'";
            lblCount_Test.Text = DT_Test_Detail.DefaultView.Count.ToString();
            rptTest.DataSource = DT_Test_Detail.DefaultView.ToTable().Copy();
            rptTest.DataBind();

            lnkR.HRef = "CP_Setting_Property.aspx?POS_NO=" + drv["POS_NO"] + "";

        }



        #endregion

        private void BindPosLevel()
        {
            string SQL = "";
            SQL += " SELECT DEPT.SECTOR_CODE,DEPT.SECTOR_NAME,DEPT.DEPT_CODE,DEPT.DEPT_NAME,POS.POS_NO,ISNULL(POS.MGR_NAME,'-') MGR_NAME,ISNULL(POS.FLD_NAME,'-') FLD_NAME,POS.PNPO_CLASS\n";
            SQL += "  FROM vw_PN_Position POS\n";
            SQL += " INNER JOIN vw_HR_Department DEPT ON POS.DEPT_CODE=DEPT.DEPT_CODE AND POS.MINOR_CODE=DEPT.MINOR_CODE\n";
            SQL += " LEFT JOIN vw_PN_PSNL PSN ON POS.POS_NO=PSN.POS_NO\n";
            SQL += " INNER JOIN vw_Path_Setting_Status Setting_Status ON Setting_Status.POS_No = POS.POS_NO\n";
            SQL += " WHERE POS.PNPO_CLASS Is Not NULL \n";
            SQL += " AND POS.POS_NO Not IN (" + POS_NO + ")\n";
            SQL += " AND POS.PNPO_CLASS>=" + Conversion.Val(lblClass_Head.Text) + "\n";
            SQL += " AND DEPT.SECTOR_CODE=" + SECTOR_CODE + "\n";
            SQL += " AND DEPT.DEPT_CODE=" + DEPT_CODE + "\n";
            SQL += " GROUP BY DEPT.SECTOR_NAME,DEPT.DEPT_NAME,POS.POS_NO,POS.FLD_NAME,POS.MGR_NAME,\n";
            SQL += " POS.PNPO_CLASS, DEPT.SECTOR_CODE, DEPT.DEPT_CODE, POS.PNPO_CLASS, POS.POS_NO\n";
            SQL += " ORDER BY DEPT.SECTOR_CODE,DEPT.DEPT_CODE,POS.PNPO_CLASS DESC,POS.POS_NO\n";

            SqlDataAdapter DA = new SqlDataAdapter(SQL, BL.ConnectionString());
            DataTable DT = new DataTable();
            DA.Fill(DT);
            rptPosLevel.DataSource = DT;
            rptPosLevel.DataBind();
        }

        protected void btnAddRight_ServerClick(object sender, System.EventArgs e)
        {
            //------------- Get Existing  List ---------------
            lblHeader_ModalPost.Text = "เลือกตำแหน่งต่อไปที่สามารถไปต่อได้ ";

            lblModePos.Text = "Right";
            Mode_Pos = lblModePos.Text;
            string _Poslist = "";
            foreach (RepeaterItem item in rptPosRight.Items)
            {
                if (item.ItemType != ListItemType.AlternatingItem & item.ItemType != ListItemType.Item)
                    continue;

                Button btnDeleteRight = (Button)item.FindControl("btnDeleteRight");
                _Poslist += "'" + btnDeleteRight.CommandArgument + "'" + ",";
            }
            if (!string.IsNullOrEmpty(_Poslist))
            {
                EXIST_Pos += _Poslist.Substring(0, _Poslist.Length - 1);
            }
            else
            {
                EXIST_Pos += "";
            }

            BindPostList();
            ModalPost.Visible = true;
            ScriptManager.RegisterStartupScript(this.Page, typeof(string), "Focus", "document.getElementById('" + txtSearchPos.ClientID + "').focus();", true);
        }

        protected void rptPosLevel_ItemCommand(object source, System.Web.UI.WebControls.RepeaterCommandEventArgs e)
        {
            switch (e.CommandName)
            {
                case "lnkCurrentFromTop":
                    Button btnDeleteTop = (Button)e.Item.FindControl("btnDeleteTop");
                    string POS_Top = btnDeleteTop.CommandArgument;
                    POS_NO = POS_Top;
                    CurrentPos();
                    break;
            }

        }


        protected void rptPosLevel_ItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType != ListItemType.AlternatingItem & e.Item.ItemType != ListItemType.Item)
                return;
            Label lblPosLevel = (Label)e.Item.FindControl("lblPosLevel");
            Button btnDeleteTop = (Button)e.Item.FindControl("btnDeleteTop");

            DataRowView drv = (DataRowView)e.Item.DataItem;

            lblPosLevel.Text = drv["POS_NO"] + " : " + drv["MGR_NAME"].ToString() + " ระดับ" + Convert.ToInt32(drv["PNPO_CLASS"]) + " หน่วยงาน " + drv["DEPT_NAME"].ToString();
            btnDeleteTop.CommandArgument = Conversion.Val(drv["POS_NO"]).ToString();
        }

        protected void btnModalPost_Close_ServerClick(object sender, System.EventArgs e)
        {
            ModalPost.Visible = false;
            Mode_Pos = "";
        }

        protected void btnModalTest_Close_ServerClick(object sender, System.EventArgs e)
        {
            ModalTest.Visible = false;
        }

        protected void btnModalTraining_Close_ServerClick(object sender, System.EventArgs e)
        {
            ModalTraining.Visible = false;
        }

        protected void btnCheck_Punishment_Click(object sender, System.Web.UI.ImageClickEventArgs e)
        {
            ImageButton btn = (ImageButton)sender;
            switch (btn.ImageUrl)
            {
                case "images/checkCP.png":
                    btn.ImageUrl = "images/noneCP.png";
                    break;
                default:
                    btn.ImageUrl = "images/checkCP.png";
                    break;
            }
            AutoSave();
        }
        public CP_Setting_Property()
        {
            Load += Page_Load;
        }



        protected void btnOKAll_Click(object sender, ImageClickEventArgs e)
        {


            SqlDataAdapter DA;
            DataTable DT;
            DataTable DTFilter;
            SqlCommandBuilder cmd;
            string SQL = "";
            DTFilter = (DataTable)Session["CP_Setting_PropertyPos_Dialog"];


            if (btnSelectAll.ImageUrl == "images/none.png")
            {
                btnSelectAll.ImageUrl = "images/check.png";
                for (int i = 0; i <= DTFilter.Rows.Count - 1; i++)
                {

                    SQL = "";
                    SQL += "  SELECT * \n";
                    SQL += "  FROM  tb_Path_Setting_Route \n";
                    SQL += "  WHERE From_POS_No ='" + POS_NO + "'\n";
                    SQL += "  AND To_POS_No='" + DTFilter.Rows[i]["POS_NO"] + "'\n";
                    DA = new SqlDataAdapter(SQL, BL.ConnectionString());
                    DT = new DataTable();
                    DA.Fill(DT);
                    DataRow DR = null;

                    if (DT.Rows.Count == 0)
                    {
                        DR = DT.NewRow();
                        DR["From_POS_No"] = POS_NO;
                    }
                    else
                    {
                        DR = DT.Rows[0];
                    }
                    DR["To_POS_No"] = DTFilter.Rows[i]["POS_NO"];
                    DR["Update_By"] = (Session["USER_PSNL_NO"]);
                    DR["Update_Time"] = DateAndTime.Now;
                    if (DT.Rows.Count == 0)
                    {
                        DT.Rows.Add(DR);
                    }
                    cmd = new SqlCommandBuilder(DA);
                    DA.Update(DT);
                    //-----bind ตารางตำแหน่ง------

                    BindPosRight();
                }

            }
            else
            {
                btnSelectAll.ImageUrl = "images/none.png";
                for (int i = 0; i <= DTFilter.Rows.Count - 1; i++)
                {

                    SQL = "";
                    SQL += "  DELETE ";
                    SQL += "  FROM  tb_Path_Setting_Route \n";
                    SQL += "  WHERE From_POS_No ='" + POS_NO + "'\n";
                    SQL += "  AND To_POS_No='" + DTFilter.Rows[i]["POS_NO"] + "'\n";
                    DA = new SqlDataAdapter(SQL, BL.ConnectionString());
                    DT = new DataTable();
                    DA.Fill(DT);
                    BindPosRight();
                }


            }
            Mode_Pos = "Right";
            BindPostList();
        }



        //---------------คัดลอก คำแหน่ง-----------------

        protected void btnCopySource_POS_Click(object sender, EventArgs e)
        {
            BindSourcePOS_Copy();
            Modal_CopyPOS.Visible = true;
        }

        protected void btnSearch_CopyPOS_Click(object sender, EventArgs e)
        {
            BindSourcePOS_Copy();
        }

        private void BindSourcePOS_Copy()
        {
            string filter = "";
            string SQL = "";
            SQL += "  SELECT DEPT.SECTOR_CODE , DEPT.SECTOR_NAME,DEPT.DEPT_NAME,DEPT.DEPT_CODE ,POS.POS_NO,ISNULL(POS.MGR_NAME,'-') MGR_NAME,ISNULL(POS.FLD_NAME,'-') FLD_NAME,POS.PNPO_CLASS\n";
            SQL += "  FROM vw_PN_Position POS \n";
            SQL += "  INNER JOIN vw_HR_Department DEPT ON POS.DEPT_CODE=DEPT.DEPT_CODE AND POS.MINOR_CODE=DEPT.MINOR_CODE\n";
            SQL += "  LEFT JOIN vw_PN_PSNL PSN ON POS.POS_NO=PSN.POS_NO\n";
            SQL += "  INNER JOIN vw_Path_Setting_Status Setting_Status ON Setting_Status.POS_No = POS.POS_NO\n";
            SQL += "  WHERE POS.PNPO_CLASS IS NOT NULL \n";

            if (ddl_Search_Sector_CopyPOS.SelectedIndex > 0)
            {
                filter += " AND (DEPT.SECTOR_CODE='" + ddl_Search_Sector_CopyPOS.Items[ddl_Search_Sector_CopyPOS.SelectedIndex].Value + "' \n";
                if (ddl_Search_Sector_CopyPOS.Items[ddl_Search_Sector_CopyPOS.SelectedIndex].Value == "3200")
                {
                    filter += " AND DEPT.DEPT_CODE NOT IN ('32000300'))\n ";
                    //-----------ฝ่ายโรงงานผลิตยาสูบ 3
                }
                else if (ddl_Search_Sector_CopyPOS.Items[ddl_Search_Sector_CopyPOS.SelectedIndex].Value == "3203")
                {
                    filter += " OR DEPT.DEPT_CODE IN ('32000300','32030000','32030100','32030200','32030300','32030500'))\n";
                    //-----------ฝ่ายโรงงานผลิตยาสูบ 4
                }
                else if (ddl_Search_Sector_CopyPOS.Items[ddl_Search_Sector_CopyPOS.SelectedIndex].Value == "3204")
                {
                    filter += " OR DEPT.DEPT_CODE IN ('32040000','32040100','32040200','32040300','32040500','32040300'))\n";
                    //-----------ฝ่ายโรงงานผลิตยาสูบ 5
                }
                else if (ddl_Search_Sector_CopyPOS.Items[ddl_Search_Sector_CopyPOS.SelectedIndex].Value == "3205")
                {
                    filter += " OR DEPT.DEPT_CODE IN ('32050000','32050100','32050200','32050300','32050500'))\t";
                }
                else
                {
                    filter += ")\n";
                }
            }


            if (!string.IsNullOrEmpty(txt_Search_Name_CopyPOS.Text))
            {
                SQL += " AND (DEPT.SECTOR_NAME LIKE '%" + txt_Search_Name_CopyPOS.Text.Replace("'", "''") + "%' OR \n";
                SQL += " DEPT.DEPT_NAME LIKE '%" + txt_Search_Name_CopyPOS.Text.Replace("'", "''") + "%' ) \n";
            }

            if (!string.IsNullOrEmpty(txtSearch_CopyPOS.Text))
            {
                SQL += " AND (POS.MGR_NAME LIKE '%" + txtSearch_CopyPOS.Text.Replace("'", "''") + "%' OR \n";
                SQL += " POS.POS_NO LIKE '%" + txtSearch_CopyPOS.Text.Replace("'", "''") + "%' OR \n";
                SQL += " POS.FLD_NAME LIKE '%" + txtSearch_CopyPOS.Text.Replace("'", "''") + "%' ) \n";
            }


            //filter += " AND POS.POS_NO NOT IN (" + POS_NO + ")  \n";

            //switch (Mode_Pos)
            //{
            //    case "Left":
            //        filter += " AND  POS.PNPO_CLASS IN (" + Convert.ToInt32(lblClass_Head.Text) + "," + (Convert.ToInt32(lblClass_Head.Text) - 1) + "," + (Convert.ToInt32(lblClass_Head.Text) - 2) + ")\n";
            //        break;
            //    case "Right":
            //        filter += " AND  POS.PNPO_CLASS IN (" + Convert.ToInt32(lblClass_Head.Text) + "," + (Convert.ToInt32(lblClass_Head.Text) + 1) + "," + (Convert.ToInt32(lblClass_Head.Text) + 2) + ")\n";
            //        break;
            //    default:
            //        break;
            //}

            SQL += "  " + filter;

            SQL += "  GROUP BY DEPT.SECTOR_CODE,DEPT.SECTOR_NAME,DEPT.DEPT_NAME,DEPT.DEPT_CODE ,POS.POS_NO,POS.FLD_NAME,POS.MGR_NAME,\n";
            SQL += "  POS.PNPO_CLASS, DEPT.SECTOR_CODE, DEPT.DEPT_CODE, POS.PNPO_CLASS, POS.POS_NO\n";
            SQL += "  ORDER BY DEPT.SECTOR_CODE,DEPT.DEPT_CODE,POS.PNPO_CLASS DESC,POS.POS_NO\n";

            SqlDataAdapter DA = new SqlDataAdapter(SQL, BL.ConnectionString());
            DataTable DT = new DataTable();
            DA.Fill(DT);

            DataTable DTRight = new DataTable();
            DTRight = (DataTable)Session["CP_Setting_PropertyPos_Copy"];

            DT.Columns.Add("SelectPos", typeof(Boolean));



            //EXIST_Pos = "";
            Session["CP_Setting_PropertyPos_CopyDialog"] = DT;
            Pager_CopyPos.SesssionSourceName = "CP_Setting_PropertyPos_CopyDialog";
            Pager_CopyPos.RenderLayout();


            if (DT.Rows.Count == 0)
            {
                lblCountPosNo.Text = "ไม่พบรายการดังกล่าว";
            }
            else
            {
                lblCountPosNo.Text = "พบ " + GL.StringFormatNumber(DT.Rows.Count, 0) + " รายการ";
            }

        }
        protected void Pager_CopyPos_PageChanging(PageNavigation Sender)
        {
            Pager_CopyPos.TheRepeater = rptCopyPOSDialog;

        }

        protected void rptCopyPOSDialog_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            switch (e.CommandName)
            {
                case "Select":
                    Label lblSector = (Label)e.Item.FindControl("lblSector");
                    Label lblDept = (Label)e.Item.FindControl("lblDept");
                    Label lblClass = (Label)e.Item.FindControl("lblClass");
                    Label lblPosNo = (Label)e.Item.FindControl("lblPosNo");
                    Label lblMGR = (Label)e.Item.FindControl("lblMGR");
                    Label lblPSN = (Label)e.Item.FindControl("lblPSN");
                    Button btnPosSelect = (Button)e.Item.FindControl("btnPosSelect");

                    string Source_POS = btnPosSelect.CommandArgument;

                    lblCOPY_POS_NO.Text = Source_POS;
                    lblSource_POS.Text = " ฝ่าย : " + lblSector.Attributes["SECTOR_NAME"].ToString() + "  " + "หน่วยงาน : " + lblSector.Attributes["DEPT_NAME"].ToString();
                    lblSource_POS_Sub.Text = " ระดับ : " + " ตำแหน่ง : " + lblPosNo.Text;
                    Modal_CopyPOS.Visible = false;
                    if (lblCOPY_POS_NO.Text == "")
                    {
                        btnPasteDestination_POS.Visible = false;
                    }
                    else
                    {
                        btnPasteDestination_POS.Visible = true;
                    }
                    break;
            }
        }

        protected void rptCopyPOSDialog_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType != ListItemType.AlternatingItem & e.Item.ItemType != ListItemType.Item)
                return;
            Label lblSector = (Label)e.Item.FindControl("lblSector");
            Label lblDept = (Label)e.Item.FindControl("lblDept");
            Label lblClass = (Label)e.Item.FindControl("lblClass");
            Label lblPosNo = (Label)e.Item.FindControl("lblPosNo");
            Label lblMGR = (Label)e.Item.FindControl("lblMGR");
            Label lblPSN = (Label)e.Item.FindControl("lblPSN");
            Button btnPosSelect = (Button)e.Item.FindControl("btnPosSelect");
            DataRowView drv = (DataRowView)e.Item.DataItem;

            lblSector.Attributes["SECTOR_NAME"] = drv["SECTOR_NAME"].ToString();
            lblSector.Attributes["DEPT_NAME"] = drv["DEPT_NAME"].ToString();

            if (LastSectorPos != drv["SECTOR_NAME"].ToString())
            {
                lblSector.Text = drv["SECTOR_NAME"].ToString();
                LastSectorPos = drv["SECTOR_NAME"].ToString();
                lblDept.Text = drv["DEPT_NAME"].ToString();
                LastDeptPos = drv["DEPT_NAME"].ToString();
            }
            else if (LastDeptPos != drv["DEPT_NAME"].ToString())
            {
                lblDept.Text = drv["DEPT_NAME"].ToString();
                LastDeptPos = drv["DEPT_NAME"].ToString();
            }
            lblClass.Text = GL.CINT(drv["PNPO_CLASS"]).ToString();
            if (GL.IsEqualNull(drv["POS_NO"]))
            {
                lblPosNo.Text = "-";
            }
            else
            {
                lblPosNo.Text = drv["POS_NO"].ToString() + ":" + drv["FLD_NAME"].ToString();
            }
            lblMGR.Text = drv["MGR_NAME"].ToString();

            btnPosSelect.CommandArgument = drv["POS_NO"].ToString();
        }

        protected void btnModalCopyPost_Close_ServerClick(object sender, System.EventArgs e)
        {
            Modal_CopyPOS.Visible = false;
            if (lblCOPY_POS_NO.Text == "")
            {
                btnPasteDestination_POS.Visible = false;
            }
            else
            {
                btnPasteDestination_POS.Visible = true;
            }
        }






        //--Dialog ตำแหน่งเพื่อเลือกตำแหน่งตั้งต้น---------------------------------------




        //--อ่านตำแหน่งที่เลือก---------------------------------------
        private string CurrentData_SelectPOS()
        {
            String Current_SelectPOS = "";
            //DataTable DT;
            //DT.Columns.Add("POS_NO", typeof(string));
            //DT.Columns.Add("SelectPos", typeof(Boolean));
            foreach (RepeaterItem Item in rptList.Items)
            {
                CheckBox chkSelect_Paste = (CheckBox)Item.FindControl("chkSelect_Paste");
                Label lblPosNo = (Label)Item.FindControl("lblPosNo");
                if (chkSelect_Paste.Checked)
                {
                    //Current_SelectPOS += "'" + DataBinder.Eval(Item.DataItem, lblPosNo.Text ) + "' , ";
                    Current_SelectPOS += "'" + lblPosNo.Attributes["POS_NO"] + "' , ";
                }
            }


            if (Current_SelectPOS != "")
            {
                Current_SelectPOS = Current_SelectPOS.Substring(0, Current_SelectPOS.Length - 2);
            }
            return Current_SelectPOS.ToString();
        }


        private DataTable CurrentDT_SelectPOS()
        {
            String Current_SelectPOS = "";
            DataTable DT = new DataTable();
            DataRow DR = null;

            DT.Columns.Add("POS_NO", typeof(string));
            DT.Columns.Add("SelectPos", typeof(Boolean));
            foreach (RepeaterItem Item in rptList.Items)
            {
                CheckBox chkSelect_Paste = (CheckBox)Item.FindControl("chkSelect_Paste");
                Label lblPosNo = (Label)Item.FindControl("lblPosNo");
                if (chkSelect_Paste.Checked)
                {
                    DR = DT.NewRow();
                    DR["POS_NO"] = lblPosNo.Attributes["POS_NO"].ToString();
                    Current_SelectPOS += "'" + lblPosNo.Attributes["POS_NO"] + "' , ";
                    DT.Rows.Add(DR);
                }
            }

            if (Current_SelectPOS != "")
            {
                Current_SelectPOS = Current_SelectPOS.Substring(0, Current_SelectPOS.Length - 2);
                lblCurrent_SelectPOS.Text = Current_SelectPOS;
            }
            return DT;
        }



        protected void btnPasteDestination_POS_Click(object sender, EventArgs e)
        {

            //----เชคต้นทาง----
            DataTable DT_CurrentSelect;
            DT_CurrentSelect = CurrentDT_SelectPOS();
            if (lblCOPY_POS_NO.Text == "")
            {
                ScriptManager.RegisterStartupScript(this.Page, typeof(string), "Alert", "showAlert('กรุณาเลือกตำแหน่งตั้งต้น ก่อนวางข้อมูล');", true);
                return;
            }
            if (DT_CurrentSelect.Rows.Count == 0)
            {
                ScriptManager.RegisterStartupScript(this.Page, typeof(string), "Alert", "showAlert('กรุณาเลือกตำแหน่งที่ต้องการวางข้อมูล');", true);
                return;
            }


            DataTable DT_Form;
            SqlDataAdapter DA_Form;
            SqlDataAdapter DA;
            DataTable DT;
            SqlCommandBuilder cmd;

            //-------form ต้นทาง---
            string SQL_Form = "";
            SQL_Form += "  SELECT To_POS_No \n";
            SQL_Form += "  FROM  tb_Path_Setting_Route \n";
            SQL_Form += "  WHERE From_POS_No ='" + lblCOPY_POS_NO.Text + "'\n";
            DA_Form = new SqlDataAdapter(SQL_Form, BL.ConnectionString());
            DT_Form = new DataTable();
            DA_Form.Fill(DT_Form);

            string SQL = "";
            SQL += "  DELETE FROM tb_Path_Setting_Route WHERE  From_POS_No IN (" + lblCurrent_SelectPOS.Text + ") \n";
            SQL += "  SELECT * \n";
            SQL += "  FROM  tb_Path_Setting_Route \n";
            SQL += "  WHERE 0=1";
            DA = new SqlDataAdapter(SQL, BL.ConnectionString());
            DT = new DataTable();
            DA.Fill(DT);

            DataRow DR = null;
            if (DT_CurrentSelect.Rows.Count > 0)
            {
                //--------loop เขียนแต่บะตำแหน่งที่เลือก Paste----------
                for (int i = 0; i <= DT_CurrentSelect.Rows.Count - 1; i++)
                {
                    //------อัดข้อมูลต้นทางเข้าทีละตำแหน่งที่เลือก-----
                    for (int j = 0; j <= DT_Form.Rows.Count - 1; j++)
                    {
                        DR = DT.NewRow();
                        DR["From_POS_No"] = DT_CurrentSelect.Rows[i]["POS_NO"];
                        DR["To_POS_No"] = DT_Form.Rows[j]["To_POS_No"];
                        DR["Update_By"] = (Session["USER_PSNL_NO"]);
                        DR["Update_Time"] = DateAndTime.Now;
                        DT.Rows.Add(DR);
                    }
                    cmd = new SqlCommandBuilder(DA);
                    DA.Update(DT);

                }

                btnPasteDestination_POS.Visible = false;
                lblSource_POS.Text = "-";
                lblSource_POS_Sub.Text = "";
                lblCOPY_POS_NO.Text = "";
                lblCurrent_SelectPOS.Text = "";
                BindPostList();

                ScriptManager.RegisterStartupScript(this.Page, typeof(string), "Alert", "showAlert('วางเรียบร้อยแล้ว');", true);
                return;


            }


        }
    }
}


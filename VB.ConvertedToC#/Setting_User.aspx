﻿<%@ Page  Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPage.Master" CodeBehind="Setting_User.aspx.cs" Inherits="VB.Setting_User" %>

<%@ Register src="WUC_DateReporter.ascx" tagname="WUC_DateReporter" tagprefix="uc1" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<%@ Register src="PageNavigation.ascx" tagname="PageNavigation" tagprefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

<!-- BEGIN PAGE LEVEL STYLES -->

	<!-- END PAGE LEVEL STYLES -->
	
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<asp:ToolkitScriptManager ID="toolkit1" runat="server"></asp:ToolkitScriptManager>

<asp:UpdatePanel ID="UpdatePanel1" runat="server">			           
<ContentTemplate>
<div class="container-fluid">
				<!-- BEGIN PAGE HEADER-->
				<div class="row-fluid" id="fixedPageHeader">
					<div class="span12"> 
						<!-- BEGIN PAGE TITLE & BREADCRUMB-->
						<h3 class="page-user-md">สิทธิ์การเข้าใช้ระบบ  <font color="blue">(ScreenID : S-BAS-01)</font></h3>					
									
						<ul class="breadcrumb">
                            <li>
                            	<i class="icon-cogs"></i><a href="javascript:;">จัดการระบบ/ข้อมูลพื้นฐาน</a><i class="icon-angle-right"></i>
                            </li>
                        	<li><i class="icon-group"></i> <a href="javascript:;">สิทธิการเข้าใช้ระบบ</a></li>
                        	
                            <uc1:WUC_DateReporter ID="WUC_DateReporter1" runat="server" />
                        </ul>
						<!-- END PAGE TITLE & BREADCRUMB-->
									        
				     </div>

				
			    </div>
		        <!-- END PAGE HEADER-->
				
				<asp:Panel ID="pnlList" runat="server" Visible="True">

               

                <div class="row-fluid">
					
					<div class="span12">
						<!-- BEGIN SAMPLE TABLE PORTLET-->
								     <asp:Panel ID="pnlSearch" runat="server" DefaultButton="btnSearch">                      
							                    <div class="row-fluid form-horizontal">
												     <div class="span6 ">
														<div class="control-group">
													        <label class="control-label" style="width:200px;"><i class="icon-sitemap"></i> ชื่อหน่วยงาน</label>
													        <div class="controls" style="margin-left:220px;">
														        <asp:TextBox id="txtSearchDept" OnTextChanged="Search_Changed" runat="server" CssClass="m-wrap medium" placeholder="ค้นหาจากชื่อหน่วยงาน/ตำแหน่ง" AutoPostBack="true"></asp:TextBox>
													        </div>
												        </div>
													</div>	
													 
													<div class="span4 ">
														<div class="control-group">
													        <label class="control-label"><i class="icon-user"></i> ชื่อ</label>
													        <div class="controls">
														        <asp:TextBox ID="txtSearchName" OnTextChanged="Search_Changed" runat="server" CssClass="m-wrap medium" placeholder="ค้นหาจากชื่อ/เลขประจำตัว" AutoPostBack="true"></asp:TextBox>
													        </div>
												        </div>
													</div>	
												</div>
												<div class="row-fluid form-horizontal">
														
													<div class="span6 ">
														<div class="control-group">
													        <label class="control-label" style="width:200px;"><i class="icon-lock"></i> กำหนดข้อมูลการประเมิน</label>
													        <div class="controls" style="margin-left:220px;">
													                     <asp:DropDownList ID="ddlSearchAdmin" OnSelectedIndexChanged="Search_Changed" runat="server" CssClass="medium m-wrap" AutoPostBack="true">
													                        <asp:ListItem Text="แสดงทั้งหมด"></asp:ListItem>
													                        <asp:ListItem Text="ได้"></asp:ListItem>
													                        <asp:ListItem Text="ไม่ได้"></asp:ListItem>
													                    </asp:DropDownList>
													               
													         </div>														           												            
														 </div>	
                                                         <div class="control-group">
												            <label class="control-label" style="width:200px;"><i class="icon-lock"></i> กำหนดหลักสูตรการพัฒนา </label>
												            <div class="controls" style="margin-left:220px;">
									                            <asp:DropDownList ID="ddlSearchGAP" OnSelectedIndexChanged="Search_Changed" runat="server" CssClass="medium m-wrap" AutoPostBack="true">
										                            <asp:ListItem Text="แสดงทั้งหมด"></asp:ListItem>
										                            <asp:ListItem Text="ได้"></asp:ListItem>
										                            <asp:ListItem Text="ไม่ได้"></asp:ListItem>
										                        </asp:DropDownList>
												            </div>
                                                        </div>                                                      
                                                        										       
													</div>	
													<div class="span6">
                                                        <div class="control-group">
													        <label class="control-label"><i class="icon-lock"></i> กำหนดเกณฑ์ตำแหน่ง</label>
													        <div class="controls">
														        <asp:DropDownList CssClass="medium m-wrap" ID="ddlSearchPropertyPos" OnSelectedIndexChanged="Search_Changed" runat="server" AutoPostBack="true">
														            <asp:ListItem Text="ทั้งหมด" Value="0" Selected="True"></asp:ListItem>
														            <asp:ListItem Text="ได้" Value="1" ></asp:ListItem>
														            <asp:ListItem Text="ไม่ได้" Value="2"></asp:ListItem>
														        </asp:DropDownList>
														        
														    </div>
												        </div>
                                                        <div class="control-group">
													        <label class="control-label"><i class="icon-lock"></i> ดูข้อมูลพนักงาน</label>
													        <div class="controls">
														        <asp:DropDownList CssClass="medium m-wrap" ID="ddlSearchViewPSN" OnSelectedIndexChanged="Search_Changed" runat="server" AutoPostBack="true">
														            <asp:ListItem Text="ทั้งหมด" Value="0" Selected="True"></asp:ListItem>
														            <asp:ListItem Text="ได้" Value="1" ></asp:ListItem>
														            <asp:ListItem Text="ไม่ได้" Value="2"></asp:ListItem>
														        </asp:DropDownList>
														        
														    </div>
												        </div>
														<div class="control-group">
													        <label class="control-label"><i class="icon-lock"></i> อนุญาติเข้าสู่ระบบ</label>
													        <div class="controls">
														        <asp:DropDownList CssClass="medium m-wrap" ID="ddlSearchAllow" OnSelectedIndexChanged="Search_Changed" runat="server" AutoPostBack="true">
														            <asp:ListItem Text="ทั้งหมด" Value="0" Selected="True"></asp:ListItem>
														            <asp:ListItem Text="ใช้งานได้ตามปกติ" Value="1" ></asp:ListItem>
														            <asp:ListItem Text="ถูกยกเลิกสิทธ์" Value="2"></asp:ListItem>
														        </asp:DropDownList>
														        
														    </div>
												        </div>
													</div>					   
												</div>
												
												
												 <div class="row-fluid form-horizontal">
												    <div class="span6">
												        
												    </div>												    
											      </div>
												<asp:Button ID="btnSearch" OnClick="Search_Changed" runat="server" style="display:none;" />
									</asp:Panel>
												
						    <div class="portlet-body no-more-tables" >
								 
								  <asp:Label ID="lblCountUser" runat="server" Font-Size="14px" Font-Bold="true"></asp:Label>
								  <table class="table table-full-width table-advance dataTable no-more-tables table-hover">
									<thead>
										<tr>											
											<th>&nbsp;</th>
											<th>&nbsp;</th>
											<th>&nbsp;</th>
											<th>&nbsp;</th>
											<th colspan="2" style=" text-align:center ;"><i class="icon-user"></i>การประเมิน</th>
                                            <th colspan="2" style=" text-align:center ;"><i class="icon-signal"></i>เส้นทางก้าวหน้าในสายอาชีพ</th>
                                            <th colspan="2" style=" text-align:center ;"><i class="icon-dashboard"></i>ตารางภารกิจงาน</th>
								            <th>
                                                &nbsp;</th>
											<th>&nbsp;</th>										
										</tr>
									    
                                    <tr>
                                            <th>
                                                <i class="icon-sitemap"></i>หน่วยงาน</th>
                                            <th>
                                                <i class="icon-user"></i>เลขประจำตัว</th>
                                            <th>
                                                <i class="icon-user"></i>ชื่อ</th>
                                            <th>
                                                <i class="icon-bookmark"></i>ตำแหน่ง</th>
                                            <th>
                                                <i class="icon-user"></i>กำหนดข้อมูลการประเมิน</th>
                                            <th>
                                                <i class="icon-user"></i>กำหนดหลักสูตรการพัฒนาฯ</th>
                                            <th>
                                                <i class="icon-signal"></i>กำหนดเกณฑ์ตำแหน่ง</th>
                                            <th>
                                                <i class="icon-signal"></i>ดูข้อมูลพนักงาน</th>
                                            <th>
                                                <i class="icon-dashboard"></i>ผู้ดูแลระบบ</th>
                                            <th>
                                                <i class="icon-dashboard"></i>ผู้ทำข้อมูล</th>
                                            <th>
                                                <i class="icon-lock"></i>อนุญาตเข้าใช้</th>
                                            <th>
                                                <i class="icon-bolt"></i>ดำเนินการ</th>
                                        </tr>
									</thead>
									<tbody>
										<asp:Repeater ID="rptUser" OnItemCommand="rptUser_ItemCommand" OnItemDataBound="rptUser_ItemDataBound" runat="server">
										    <ItemTemplate>
										                <tr>
											                <td class="highlight" data-title="หน่วยงาน"><asp:Label ID="lblDept" runat="server"></asp:Label></td>
											                <td data-title="รหัสสำหรับเข้าสู่ระบบ"><asp:Label ID="lblCode" runat="server"></asp:Label></td>
											                <td data-title="ชื่อ"><asp:Label ID="lblName" runat="server"></asp:Label></td>
											                <td data-title="ตำแหน่ง"><asp:Label ID="lblPos" runat="server"></asp:Label></td>
											                <td data-title="กำหนดข้อมูลการประเมิน" style="text-align:center">
								                                <asp:ImageButton ID="btnAdmin" runat="server" ImageUrl="images/check.png" Height="24px" CommandName="Admin" ToolTip="Click เพื่อเปลี่ยน" />
											                </td>
											                <td data-title="กำหนดหลักสูตรการพัฒนาฯ" style="text-align:center">											       
								                                <asp:ImageButton ID="btnGAP" runat="server" ImageUrl="images/check.png" Height="24px" CommandName="GAP" ToolTip="Click เพื่อเปลี่ยน" />					             
											                </td>
											                <td data-title="กำหนดเกณฑ์ตำแหน่ง" style="text-align:center">											       
								                                <asp:ImageButton ID="btnProperty" runat="server" ImageUrl="images/check.png" Height="24px" CommandName="SettingProperty" ToolTip="Click เพื่อเปลี่ยน" />					             
											                </td>
                                                            <td data-title="ดูข้อมูลพนักงาน" style="text-align:center">											       
								                                <asp:ImageButton ID="btnViewPSN" runat="server" ImageUrl="images/check.png" Height="24px" CommandName="ViewPSN" ToolTip="Click เพื่อเปลี่ยน" />					             
											                </td>
                                                            <td data-title="Admin" style="text-align:center">											       
								                                <asp:ImageButton ID="btnWorkloadAdmin" runat="server" ImageUrl="images/check.png" Height="24px" CommandName="WorkloadAdmin" ToolTip="Click เพื่อเปลี่ยน" />					             
											                </td>
                                                            <td data-title="User" style="text-align:center">											       
								                                <asp:ImageButton ID="btnWorkloadUser" runat="server" ImageUrl="images/check.png" Height="24px" CommandName="WorkloadUser" ToolTip="Click เพื่อเปลี่ยน" />					             
											                </td>
											                <td data-title="อนุญาตเข้าใช้ระบบ" style="text-align:center">
								                                <asp:ImageButton ID="btnAllow" runat="server" ImageUrl="images/check.png" Height="24px" CommandName="Allow" ToolTip="Click เพื่อเปลี่ยน" />
											                </td>
                											<td data-title="ดำเนินการ"><asp:Button ID="btnView" runat="server" CssClass="btn mini blue" CommandName="View" Text="ดำเนินการ" /></asp:Button>
                											<asp:Button ID="btnEdit" runat="server" style="display:none;" CommandName="Edit" />
                											</td>
										                </tr>
										    </ItemTemplate>
										</asp:Repeater>
										
													
									</tbody>
								</table>
								<asp:PageNavigation ID="Pager" OnPageChanging="Pager_PageChanging" MaximunPageCount="10" PageSize="20" runat="server" />
								
                                <%--<div class="form-horizontal form-view">
                                    <div class="form-actions">
									    <asp:Button ID="btn" runat="server" CssClass="btn green" Text="ดึงข้อมูลล่าสุดจากระบบทรัพยากรบุคคล(Oracle)" />
								    </div>
                                </div>	--%>							

							</div>
						
						<!-- END SAMPLE TABLE PORTLET-->
						
					</div>
                </div>
               </asp:Panel>
                 
                                <div class="modal" id="divModal" runat="server" style="top:10%; position:fixed; width:auto;" visible="False" >
									<div class="modal-header" >										
										<h3 style=" margin-right:30px;">กำหนดสิทธิ์การเข้าใช้ระบบของ </h3> <br>
										 
										 <h4 style=" margin-right:30px;">
										    <asp:Label ID="lblUserName" runat="server"></asp:Label> (<asp:Label ID="lblUserCode" runat="server"></asp:Label>)<br>
										    <asp:Label ID="lblPOSName" runat="server"></asp:Label> <asp:Label ID="lblDEPTName" runat="server"></asp:Label>
										</h4>
									</div>
									<div class="modal-body">
										
											<div class="row-fluid" style="display:none;">
												<div class="form-horizontal">												        
												        <div class="control-group">
													        <label class="control-label">รหัสผ่าน</label>
													        <div class="controls">
														        <asp:TextBox CssClass="m-wrap small" ID="txtPassword" MaxLength="20" runat="server" style="text-align:center" ></asp:TextBox>
													        </div>
												        </div>											        												        
											   </div>
											</div>
											<div class="row-fluid">
												<div class="form-horizontal">												        
												        <div class="control-group">
												            <label class="control-label">กำหนดข้อมูลการประเมิน</label>													        
													        <div class="controls" >
								                                <asp:ImageButton ID="btnEditAdmin" OnClick="btnEdit_Click" runat="server" ImageUrl="images/check.png" Height="24px" ToolTip="Click เพื่อเปลี่ยน" />
							                                </div>													        
												        </div>											        												        
											   </div>
											</div>
											<div class="row-fluid">
												<div class="form-horizontal">												        
												        <div class="control-group">
												            <label class="control-label">กำหนดหลักสูตรการพัฒนาฯ</label>													        
													        <div class="controls">
								                                <asp:ImageButton ID="btnEditGAP" OnClick="btnEdit_Click" runat="server" ImageUrl="images/check.png" Height="24px" ToolTip="Click เพื่อเปลี่ยน" />
							                                </div>													        
												        </div>										        												        
											   </div>
											</div>
                                            <div class="row-fluid">
												<div class="form-horizontal">												        
												        <div class="control-group">
												            <label class="control-label">กำหนดเกณฑ์ตำแหน่ง</label>													        
													        <div class="controls">
								                                <asp:ImageButton ID="btnEditPropertyPos" OnClick="btnEdit_Click" runat="server" ImageUrl="images/check.png" Height="24px" ToolTip="Click เพื่อเปลี่ยน" />
							                                </div>													        
												        </div>										        												        
											   </div>
											</div>
                                            <div class="row-fluid">
												<div class="form-horizontal">												        
												        <div class="control-group">
												            <label class="control-label">ดูข้อมูลพนักงาน</label>													        
													        <div class="controls">
								                                <asp:ImageButton ID="btnEditViewPSN" OnClick="btnEdit_Click" runat="server" ImageUrl="images/check.png" Height="24px" ToolTip="Click เพื่อเปลี่ยน" />
							                                </div>													        
												        </div>										        												        
											   </div>
											</div>
                                            <div class="row-fluid">
												<div class="form-horizontal">												        
												        <div class="control-group">
												            <label class="control-label">ผู้ดูแลระบบ<br>(บริหารอัตรากำลัง)</label><div class="controls">
								                                <asp:ImageButton ID="btnEditWorkloadAdmin" OnClick="btnEdit_Click" runat="server" ImageUrl="images/check.png" Height="24px" ToolTip="Click เพื่อเปลี่ยน" />
							                                </div>													        
												        </div>										        												        
											   </div>
											</div>
                                            <div class="row-fluid">
												<div class="form-horizontal">												        
												        <div class="control-group">
												            <label class="control-label">ผู้ทำข้อมูล<br>(บริหารอัตรากำลัง)</label><div class="controls">
								                                <asp:ImageButton ID="btnEditWorkloadUser" OnClick="btnEdit_Click" runat="server" ImageUrl="images/check.png" Height="24px" ToolTip="Click เพื่อเปลี่ยน" />
							                                </div>													        
												        </div>										        												        
											   </div>
											</div>

											<div class="row-fluid">
												<div class="form-horizontal">												        
												        <div class="control-group">
												            <label class="control-label">อนุญาตเข้าใช้ระบบ</label>													        
													        <div class="controls">
								                                <asp:ImageButton ID="btnEditAllow" OnClick="btnEdit_Click" runat="server" ImageUrl="images/check.png" Height="24px" ToolTip="Click เพื่อเปลี่ยน" />
							                                </div>													        
												        </div>										        												        
											   </div>
											</div>
										
									</div>
									<div class="modal-footer">
									    <asp:Button ID="btnClose" OnClick="btnClose_Click" runat="server" CssClass="btn" Text="ยกเลิก" />
										<asp:Button ID="btnOK" OnClick="btnOK_Click" runat="server" CssClass="btn blue" Text="ยืนยัน" />										
									</div>
								</div>
                 
</ContentTemplate>				                
</asp:UpdatePanel>
 
</asp:Content>


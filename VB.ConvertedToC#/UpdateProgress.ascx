﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UpdateProgress.ascx.cs" Inherits="VB.UpdateProgress" %>

 <div id="divImage" runat="server" style=" display :none ; position:fixed; top:0px; left:0px; width:100%; height:100%; z-index:1000;">                    
                    <div style="width:100%; height:100%; position:absolute; background-color:white; filter:alpha(opacity=0); opacity: 0;" >
                    
                    </div>
                    <table style="width:100%; height:50%; position:absolute;">
                        <tr><td height="100%">&nbsp;</td></tr>
                        <tr>
                            <td align="center" style="  text-align:center;">
                                    <span style=" padding-left:50px; padding-right:50px; padding-top:10px; padding-bottom:10px; color:Teal; border:2px dotted teal; font-weight:bold; font-size:22px; width:auto; background-color:white;">... กำลังดำเนินการ ...</span>
                            </td>
                        </tr>
                        <tr><td height="50%">&nbsp;</td></tr>
                    </table>                     
                </div>                

    <script type="text/javascript" language="javascript">

        // Called when async postback begins
        function prm_InitializeRequest(sender, args) {
            // get the divImage and set it to visible
            var panelProg = $get('<%= divImage.ClientID %>');
            panelProg.style.display = '';
            // Disable button that caused a postback
            $get(args._postBackElement.id).disabled = true;
        }

        // Called when async postback ends
        function prm_EndRequest(sender, args) {
            // get the divImage and hide it again
            var panelProg = $get('<%= divImage.ClientID %>');
            // Enable button that caused a postback
            try
                 { $get(sender._postBackSettings.sourceElement.id).disabled = false; }
            catch (err) { }
            panelProg.style.display = 'none';
        }
        // Add Event Handler 
        try {
           
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            // Add initializeRequest and endRequest
            prm.add_initializeRequest(prm_InitializeRequest);
            prm.add_endRequest(prm_EndRequest);
        } catch (err) { }
    </script>
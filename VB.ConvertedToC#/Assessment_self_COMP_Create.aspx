﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPage.Master" CodeBehind="Assessment_self_COMP_Create.aspx.cs" Inherits="VB.Assessment_self_COMP_Create" %>

<%@ Register src="WUC_DateReporter.ascx" tagname="WUC_DateReporter" tagprefix="uc1" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<asp:UpdatePanel ID="UpdatePanel1" runat="server">      

<ContentTemplate>
<asp:ToolkitScriptManager ID="toolkit1" runat="server"></asp:ToolkitScriptManager>

<div class="container-fluid">
				<!-- BEGIN PAGE HEADER-->
				<div class="row-fluid" id="fixedPageHeader">
					<div class="span12"> 
						<!-- BEGIN PAGE TITLE & BREADCRUMB-->
						<h3 class="page-user-md">กำหนดแบบประเมินสมรรถนะตนเอง (Competency)  <font color="blue">(ScreenID : S-EMP-02)</font>
                        <asp:Label ID="lblReportTime" Font-Size="14px" ForeColor="Green" runat="Server"></asp:Label>
                        </h3>					
									
						<ul class="breadcrumb">
                            <li>
                            	<i class="icon-user"></i> <a href="javascript:;">การประเมินตนเอง</a><i class="icon-angle-right"></i>
                            </li>
                        	<li>
                        	    <i class="icon-th-list"></i> <a href="javascript:;">กำหนดแบบประเมินสมรรถนะ (Competency)</a>
                        	</li>                        	
                            <uc1:WUC_DateReporter ID="WUC_DateReporter1" runat="server" />
                        </ul>
						<!-- END PAGE TITLE & BREADCRUMB-->
									        
				     </div>
			    </div>
		        <!-- END PAGE HEADER-->
                                
				<div class="portlet-body form">										
			       
			        <div class="btn-group pull-right">                                    
			            <a data-toggle="dropdown" class="btn dropdown-toggle">พิมพ์ <i class="icon-angle-down"></i></a>										
			                <ul class="dropdown-menu pull-right">                                                       
                                <li><a id="btnCOMPFormPDF" runat="server" target="_blank">ใบประเมิน รูปแบบ PDF</a></li>
                                <li><a id="btnCOMPFormExcel" runat="server" target="_blank">ใบประเมิน รูปแบบ Excel</a></li>	
                                <li><a id="btnIDPFormPDF" runat="server" target="_blank">แผนพัฒนารายบุคคล รูปแบบ PDF</a></li>
                                <li><a id="btnIDPFormExcel" runat="server" target="_blank">แผนพัฒนารายบุคคล รูปแบบ Excel</a></li>	
                                <li><a id="btnIDPResultFormPDF" runat="server" target="_blank">ผลการพัฒนาตามแผนพัฒนารายบุคคล รูปแบบ PDF</a></li>
                                <li><a id="btnIDPResultFormExcel" runat="server" target="_blank">ผลการพัฒนาตามแผนพัฒนารายบุคคล รูปแบบ Excel</a></li>
                                <li><a id="btnIDPRemark_Plan" href="Doc/RemarkIDP/Remark_PlanIDP.pdf" runat="server" target="_blank">Download คำชี้แจง แผนการพัฒนารายบุคคล</a></li>
                                <li><a id="btnIDPRemark_Result" href="Doc/RemarkIDP/Remark_ResultIDP.pdf" runat="server" target="_blank">Download คำชี้แจง ผลการพัฒนาตามแผนพัฒนารายบุคคล</a></li>				                                </ul>
			                </ul>
		            </div>
		            
			        <h3 class="form-section"><i class="icon-th-list"></i>
			            <asp:Label ID="lblName" runat="server"></asp:Label>
			            สำหรับรอบการประเมิน
			            <asp:DropDownList ID="ddlRound" OnSelectedIndexChanged="ddlRound_SelectedIndexChanged" runat="server" AutoPostBack="true" CssClass="medium m-wrap header" style="font-size:20px;">
					    </asp:DropDownList>
					    (<asp:Label ID="lblCOMPStatus" runat="server" KPI_Status="0"></asp:Label>)
			        </h3>
			        
    			   <div class="form-horizontal form-view">
    			        <div class="row-fluid" style="border-bottom:1px solid #EEEEEE;">
					        <div class="span6 ">
					            <div class="control-group">
								    <label class="control-label" style="width:100px;">ประเภท :</label>
								    <div class="controls" style="margin-left: 120px;">
									    <asp:Label ID="lblPSNType" runat="server" CssClass="text bold"></asp:Label>
								    </div>
							    </div>
					        </div>					        
					     </div>
    			        <div class="row-fluid" style="border-bottom:1px solid #EEEEEE;">
					        <div class="span4 ">
					            <div class="control-group">
								    <label class="control-label" style="width:100px;">ผู้รับการประเมิน :</label>
								    <div class="controls" style="margin-left: 120px;">
									    <asp:Label ID="lblPSNName" runat="server" CssClass="text bold"></asp:Label>
								    </div>
							    </div>
					        </div>
					        <div class="span3 ">
					            <div class="control-group">
								    <label class="control-label" style="width:100px;">ตำแหน่ง :</label>
								    <div class="controls" style="margin-left: 120px;">
									    <asp:Label ID="lblPSNPos" runat="server" CssClass="text bold"></asp:Label>
								    </div>
							    </div>
					        </div>
					        <div class="span5 ">
					            <div class="control-group">
								    <label class="control-label" style="width:100px;">สังกัด :</label>
								    <div class="controls" style="margin-left: 120px;">
									    <asp:Label ID="lblPSNDept" runat="server" CssClass="text bold"></asp:Label>
								    </div>
							    </div>
					        </div>
					     </div>
					     
					     <div class="row-fluid" style="margin-top:10px;">
					        <div class="span4 ">
					            <div class="control-group">
								    <label class="control-label" style="width:100px;">ผู้ประเมิน :</label>
								    <div class="controls" style="margin-left: 120px;">
									    <asp:DropDownList ID="ddlASSName" OnSelectedIndexChanged="ddlASSName_SelectedIndexChanged" runat="Server" AutoPostBack="True" style="font-weight:bold; color:Black; border:none;">									        
									    </asp:DropDownList>
									</div>
							    </div>
					        </div>
					        <div class="span3 ">
					            <div class="control-group">
								    <label class="control-label" style="width:100px;">ตำแหน่ง :</label>
								    <div class="controls" style="margin-left: 120px;">
									    <asp:Label ID="lblASSPos" runat="server" CssClass="text bold"></asp:Label>
								    </div>
							    </div>
					        </div>
					        <div class="span5 ">
					            <div class="control-group">
								    <label class="control-label" style="width:100px;">สังกัด :</label>
								    <div class="controls" style="margin-left: 120px;">
									    <asp:Label ID="lblASSDept" runat="server" CssClass="text bold"></asp:Label>
								    </div>
							    </div>
					        </div>
					     </div>
					     
    			    </div>
    			 								
				    <!-- BEGIN FORM-->
				    <div class="portlet-body no-more-tables">
				
				                        <table class="table table-bordered no-more-tables" style="background-color:White;">
									        <thead>
										        <tr>
											        <th rowspan="2" style="text-align:center; background-color:Silver;"> ลำดับ</th>
											        <th rowspan="2" style="text-align:center; background-color:Silver;"> สมรรถนะ</th>
											        <th rowspan="2" style="text-align:center; background-color:Silver;"> มาตรฐานพฤติกรรม</th>
											        <th colspan="5" style="text-align:center; background-color:Silver;"> คะแนนตามระดับค่าเป้าหมาย</th>
											        <th rowspan="2" style="text-align:center; background-color:Silver;"> น้ำหนัก(%)<br><asp:Button ID="btnAVG_Weight" OnClick="btnAVG_Weight_Click" runat="server" CssClass=" btn mini  blue " Text="เฉลี่ยน้ำหนัก" />	</th>											        
										        </tr>
                                                <tr>
										          <th class="AssLevel1" style="text-align:center; font-weight:bold; vertical-align:middle;">1</th>
										          <th class="AssLevel2" style="text-align:center; font-weight:bold; vertical-align:middle;">2</th>
										          <th class="AssLevel3" style="text-align:center; font-weight:bold; vertical-align:middle;">3</th>
										          <th class="AssLevel4" style="text-align:center; font-weight:bold; vertical-align:middle;">4</th>
										          <th class="AssLevel5" style="text-align:center; font-weight:bold; vertical-align:middle;">5</th>
								              </tr>
									        </thead>
									        <tbody>
									             <asp:Panel ID="pnlCOMP" runat="server">
									             
									             
									                    <asp:Repeater ID="rptAss" OnItemCommand="rptAss_ItemCommand" OnItemDataBound="rptAss_ItemDataBound" runat="server">
									                        <ItemTemplate>
									                        <tr id="row_COMP_type" runat="server">
									                            <td colspan="9" style="font-weight:bold; font-size:18px; background-color:#AAAAAA;" id="cell_COMP_type" runat="server">สมรรถนะหลัก Core Competency, สมรรถนะตามสายงาน Functional Competency, สมรรถนะตามสายระดับ Managerial Competency</td>
									                        </tr>
        									                
									                        <tr>
										                      <td data-title="ลำดับ" id="ass_no" runat="server" style="text-align:center; font-weight:bold; vertical-align:top;">001</td>
										                      <td data-title="สมรรถนะ" id="COMP_Name" class="TextLevel3" runat="server" style="background-color:#f8f8f8; ">xxxxxxxxxxxx xxxxxxxxxxxx</td>
										                      <td data-title="มาตรฐานพฤติกรรม" id="target" runat="server" class="TextLevel3" style="background-color:#f8f8f8; ">xxxxxxxxxxxx xxxxxxxxxxxx</td>
										                      <td data-title="ระดับที่ 1" id="choice1" class="TextLevel3" runat="server" style="background-color:White;">xxxxxxxxxxxx xxxxxxxxxxxx xxxxxxxxxxxx xxxxxxxxxxxx xxxxxxxxxxxx xxxxxxxxxxxx</td>
                                                              <td data-title="ระดับที่ 2" id="choice2" class="TextLevel3" runat="server" style="background-color:White;">xxxxxxxxxxxx xxxxxxxxxxxx xxxxxxxxxxxx xxxxxxxxxxxx xxxxxxxxxxxx xxxxxxxxxxxx</td>
                                                              <td data-title="ระดับที่ 3" id="choice3" class="TextLevel3" runat="server" style="background-color:White;">xxxxxxxxxxxx xxxxxxxxxxxx xxxxxxxxxxxx xxxxxxxxxxxx xxxxxxxxxxxx xxxxxxxxxxxx</td>
                                                              <td data-title="ระดับที่ 4" id="choice4" class="TextLevel3" runat="server" style="background-color:White;">xxxxxxxxxxxx xxxxxxxxxxxx xxxxxxxxxxxx xxxxxxxxxxxx xxxxxxxxxxxx xxxxxxxxxxxx</td>
                                                              <td data-title="ระดับที่ 5" id="choice5" class="TextLevel3" runat="server" style="background-color:White;">xxxxxxxxxxxx xxxxxxxxxxxx xxxxxxxxxxxx xxxxxxxxxxxx xxxxxxxxxxxx xxxxxxxxxxxx</td>
										                      <td data-title="น้ำหนัก" title="น้ำหนัก" id="cel_weight" runat="server" style="text-align:center; font-size:14px; font-weight:bold; vertical-align:middle;">
        										              <asp:Panel ID="pnlWeight" runat="server" DefaultButton="btnUpdate">
        										                <asp:Textbox ID="txtWeight" MaxLength="5" runat="server" ReadOnly="true"  style="width:90%; padding:5px; font-size:14px; 
										                          font-weight:bold; text-align:center; float:left; 
										                           text-align:center" Text=""  placeholder="น้ำหนัก ??"></asp:Textbox>
										                       <%-- <asp:Textbox ID="txtWeight" MaxLength="5" runat="server" ReadOnly="true"  style="width:90%; font-size:16px;  margin-top:15px;
										                              font-weight:bold; text-align:center; float:left; text-align:center" Text=""  placeholder="น้ำหนัก ??"></asp:Textbox>--%>
										                        <asp:Button ID="btnUpdate" runat="server" style="display:none;" CommandName="Update" />
        										              </asp:Panel>					             
										                      
        										              
										                      </td>            										          
									                        </tr>
        									               
									                        </ItemTemplate>
									                        <FooterTemplate>        									                
									                            <tfoot>
									                                <tr>
									                                    <th colspan="8" style="text-align:center; background-color:#EEEEEE; font-size:14px; font-weight:bold;">
									                                    ผลรวมน้ำหนัก
									                                    </th>
									                                    <th id="cell_sum_weight" runat="server" style="text-align:center; color:White; font-size:16px; font-weight:bold;" colspan="2">-</th>
									                                </tr>									                        
									                            </tfoot>
        									                
									                        </FooterTemplate>
									                    </asp:Repeater>	
									              </asp:Panel>
									        </tbody>									        
								        </table>
								        
						               
						                <asp:Panel CssClass="form-actions" id="pnlActivity" runat="server">
						                    <asp:Button ID="btnPreSend" OnClick="btnPreSend_Click" runat="server" CssClass="btn green" Text="ส่งแบบประเมินให้หัวหน้า/ผู้ประเมิน  (หลังจากส่งแบบประเมินแล้วคุณไม่สามารถแก้ไขได้)" />
											<asp:Button ID="btnSend" OnClick="btnSend_Click" runat="server" Text="" style="display:none;" />
						                </asp:Panel>
										    
				      		</div>
						
				    <!-- END FORM-->  
				   
			    </div>	
								            
                  		 
</div>

</ContentTemplate>           
</asp:UpdatePanel>

</asp:Content>




using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Xml.Linq;
using System.Diagnostics;
using System.Collections.Specialized;
using System.Configuration;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Caching;
using System.Web.SessionState;
using System.Web.Security;
using System.Web.Profile;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using System.Data.SqlClient;
namespace VB
{

	public partial class CP_Setting_CareerPath_Self : System.Web.UI.Page
	{


		HRBL BL = new HRBL();

		GenericLib GL = new GenericLib();

		protected void Page_Load(object sender, System.EventArgs e)
		{
			if ((Session["USER_PSNL_NO"] == null)) {
				ScriptManager.RegisterStartupScript(this.Page, typeof(string), "Alert", "showAlert('กรุณาลงชื่อเข้าใช้ระบบ');", true);
				Response.Redirect("Login.aspx");
				return;
			}
			if (!IsPostBack) {
				BindPath_Position();
				ModalPost.Visible = false;
				ModelCompareDetail.Visible = false;
			}

		}

		public string PNPS_PSNL_NO {
			get {
				try {
					return Session["USER_PSNL_NO"].ToString ();
				} catch (Exception ex) {
					return "";
				}
			}
		}

		//---------ตำแหน่งพนักงาน-------------
		public string POS_NO {
			get { return lblTMP.Attributes["POS_NO"]; }
			set { lblTMP.Attributes["POS_NO"] = value; }
		}

		//---------ตำแหน่งพนักงาน-------------
		public string POS_NO_Right {
			get { return lblTMP.Attributes["POS_NO_Right"]; }
			set { lblTMP.Attributes["POS_NO_Right"] = value; }
		}

		//---------ตำแหน่งเป้าหมาย-------------
		public string Goal_NO {
			get { return lblTMP.Attributes["Goal_NO"]; }
			set { lblTMP.Attributes["Goal_NO"] = value; }
		}

		//---------เป้าหมายก่อนหน้า 1 เป้าหมาย-------------
		public string Pos_NO_Left {
			get { return lblTMP.Attributes["Pos_NO_Left"]; }
			set { lblTMP.Attributes["Pos_NO_Left"] = value; }
		}

		//----------เป้าหมายก่อนหน้า 1 เป้าหมาย-------------
		public int Class_Pos_NO_Left {
			get { return GL.CINT(lblTMP.Attributes["Class_Pos_NO_Left"]); }
			set { lblTMP.Attributes["Class_Pos_NO_Left"] = value.ToString (); }
		}

		//-----------ตรวจเกณฑ์การเข้าสู่ตำแหน่ง----------------------

		//---------1 คะแนนประเมิน-------------
		public string Check_Ass_MIN_Score {
			get { return lblTMP.Attributes["Check_Ass_MIN_Score"]; }
			set { lblTMP.Attributes["Check_Ass_MIN_Score"] = value; }
		}

		//---------2 คะแนนวันลา-------------
		public string Check_Leave {
			get { return lblTMP.Attributes["Check_Leave"]; }
			set { lblTMP.Attributes["Check_Leave"] = value; }
		}

		//---------3 การฝึกอบรม-------------
		public string Check_Course {
			get { return lblTMP.Attributes["Check_Course"]; }
			set { lblTMP.Attributes["Check_Course"] = value; }
		}

		//---------4 การสอบ-------------
		public string Check_Test {
			get { return lblTMP.Attributes["Check_Test"]; }
			set { lblTMP.Attributes["Check_Test"] = value; }
		}

		//---------5 โทษทางวินัย-------------
		public string Check_Punishment {
			get { return lblTMP.Attributes["Check_Punishment"]; }
			set { lblTMP.Attributes["Check_Punishment"] = value; }
		}

		//-------POS ตำแหน่งปัจจุบัน------

		private void BindSetting_header()
		{
			string SQL = "";
			SQL += " SELECT  PSNL_NO, PSNL_Fullname, PNPS_CLASS, POS_NO,\n";
			SQL += " SECTOR_CODE, SECTOR_NAME,\n";
			SQL += " DEPT_NAME,\n";
			SQL += " MGR_CODE, MGR_NAME\n";
			SQL += " FROM vw_PN_PSNL_All PSN\n";
			SQL += " WHERE PSNL_NO='" + Session["USER_PSNL_NO"].ToString().Replace("'", "''") + "'\n";
			SqlDataAdapter DA = new SqlDataAdapter(SQL, BL.ConnectionString());
			DataTable DT = new DataTable();
			DA.Fill(DT);
			if (DT.Rows.Count == 1) {
				lblCurrentPos.Text = "<b> " + DT.Rows[0]["POS_NO"].ToString() + " : " + DT.Rows[0]["MGR_NAME"].ToString() + "</b>" + " ระดับ " + Convert.ToInt32(DT.Rows[0]["PNPS_CLASS"]) + " หน่วยงาน" + DT.Rows[0]["DEPT_NAME"].ToString();
				POS_NO = DT.Rows[0]["POS_NO"].ToString();
			} else {
				lblCurrentPos.Text = "";
			}

			BindCareerPath();
		}


		//-------POS ตำแหน่งปัจจุบัน------

		private void BindPath_Position()
		{
			string SQL = "";
			SQL += " SELECT  PSNL_NO, PSNL_Fullname, PNPS_CLASS, POS_NO,\n";
			SQL += " SECTOR_CODE, SECTOR_NAME,\n";
			SQL += " DEPT_NAME,\n";
			SQL += " MGR_CODE, MGR_NAME\n";
			SQL += " FROM vw_PN_PSNL_All PSN\n";
			SQL += " WHERE PSNL_NO='" + Session["USER_PSNL_NO"].ToString().Replace("'", "''") + "'\n";
			SqlDataAdapter DA = new SqlDataAdapter(SQL, BL.ConnectionString());
			DataTable DT = new DataTable();
			DA.Fill(DT);
			if (DT.Rows.Count == 1) {
				lblCurrentPos.Text = "<b> " + DT.Rows[0]["POS_NO"].ToString() + " : " + DT.Rows[0]["MGR_NAME"].ToString() + "</b>" + " ระดับ : " + Convert.ToInt32(DT.Rows[0]["PNPS_CLASS"]) + " หน่วยงาน : " + DT.Rows[0]["DEPT_NAME"].ToString();
				POS_NO = DT.Rows[0]["POS_NO"].ToString();
			} else {
				lblCurrentPos.Text = "";
			}


			BindCareerPath();
		}


		private void BindCareerPath()
		{
			BindCourse_Detail();
			BindTest_Detail();
			string SQL = "";
			SQL += "   SELECT 'Goal' Property,PSNL_NO,Goal_NO,POS_No,MGR_CODE,MGR_NAME,PNPS_CLASS,DEPT_NAME FROM tb_Path_Setting_Self ";
			SQL += "   WHERE PSNL_NO='" + Session["USER_PSNL_NO"].ToString().Replace("'", "''") + "'\n";
			SQL += "   Order By PSNL_NO,Goal_NO";
			SqlDataAdapter DA = new SqlDataAdapter(SQL, BL.ConnectionString());
			DataTable DT = new DataTable();
			DA.Fill(DT);

			//------เพิ่มแถว สำหรับ Add เพิ่มเติม---------
			DataRow DR = null;
			DR = DT.NewRow();
			DR["Property"] = "Add";
			DR["PSNL_NO"] = "";
			DR["Goal_NO"] = (GL.CINT(DT.Rows.Count) + 1);
			DR["POS_No"] = "";
			DR["MGR_CODE"] = "";
			DR["MGR_NAME"] = "";
			DR["PNPS_CLASS"] = "";
			DR["DEPT_NAME"] = "";
			DT.Rows.Add(DR);
			rptAddPos.DataSource = DT;
			rptAddPos.DataBind();

		}

		DataTable DT_Course_Detail = null;

		DataTable DT_Test_Detail = null;
		protected void rptAddPos_ItemCommand(object source, System.Web.UI.WebControls.RepeaterCommandEventArgs e)
		{
            string SQL;
            DataTable DT;
            DataTable DT_POS;
            SqlCommandBuilder cmd;
            SqlDataAdapter DA;
            DataRow DR;

        Button btnDeleteLeft =(Button) e.Item.FindControl("btnDeleteLeft");
		Button btnDelete =(Button) e.Item.FindControl("btnDelete");
        Label lblCareerPath_NO = (Label)e.Item.FindControl("lblCareerPath_NO");
			switch (e.CommandName) {
				case "Delete":
					
					string POS = btnDelete.CommandArgument;

					//-------------ตรวจสอบตำแหน่งถัดไป สามารถเข้ารับตำแหน่งก่อนหน้าหรือปัจจุบันได้หรือไม่---------------
					 SQL = "";
					SQL += "   SELECT 'Goal' Property,PSNL_NO,Goal_NO,POS_No,MGR_CODE,MGR_NAME,PNPS_CLASS,DEPT_NAME FROM tb_Path_Setting_Self ";
					SQL += "   WHERE PSNL_NO='" + Session["USER_PSNL_NO"].ToString().Replace("'", "''") + "'\n";
					SQL += "   Order By PSNL_NO,Goal_NO";
					 DA = new SqlDataAdapter(SQL, BL.ConnectionString());
					 DT = new DataTable();
					DA.Fill(DT);


					 DT_POS = null;
					string Goal = "";
					if (GL.CINT(lblCareerPath_NO.Text) == 1) {
						//----------ใช้ตำแหน่งปัจจุันของพนักงาน-------------
						Goal = POS_NO;
					} else {
						//----------ใช้ตำแหน่งที่ ต้องการลบ -1 หาตำแหน่งต่อไปที่สามารถเข้ารับได้-------------------------
						DT.DefaultView.RowFilter = "Goal_NO='" + (GL.CINT(lblCareerPath_NO.Text) - 1) + "'";
						if (DT.DefaultView.Count > 0) {
							Goal = DT.DefaultView[0]["POS_NO"].ToString ();
						} else {
						}
					}

					DT_POS = DT_Check_POS(Goal);
					//-------ตำแหน่งตอไปที่สามารถเข้ารับได้--------
					if (DT_POS.Rows.Count > 0) {
						//----------เชตตำแหน่งสามารถเข้ารับได้หรือไม่--------------------
						DT.DefaultView.RowFilter = " Goal_NO='" + (lblCareerPath_NO.Text + 1) + "'";
						//----หาตำแหน่งถัดไป----
                        if (DT.DefaultView.Count == Convert.ToInt32(0))
                        {
							//--------------ตำหน่งที่ลบเป็นตำแหน่งสุดท้าย---------------
							DT.DefaultView.RowFilter = "POS_NO='" + POS + "'" + "  AND Goal_NO='" + lblCareerPath_NO.Text + "'";
							if (DT.DefaultView.Count > 0) {
								DT.DefaultView[0].Row.Delete();
							}
						//--------ตำแหน่งระหว่างกลาง----------
						} else {
							//--------------เชคตำแหน่งถัดไป---------------
							DT_POS.DefaultView.RowFilter = "POS_NO='" + DT.DefaultView[0]["POS_NO"] + "'";
                            if (DT_POS.DefaultView.Count == Convert.ToInt32(0))
                            {
								DT.DefaultView.RowFilter = "Goal_NO >= " + lblCareerPath_NO.Text + "";
								if (DT.DefaultView.Count > 0) {
									for (int i = 0; i <= DT.DefaultView.Count - 1; i++) {
										DT.DefaultView.RowFilter = "Goal_NO >= " + lblCareerPath_NO.Text + "";
										DT.DefaultView[0].Row.Delete();
									}
								}
							} else {
								DT.DefaultView.RowFilter = "Goal_NO='" + lblCareerPath_NO.Text + "'";
								if (DT.DefaultView.Count > 0) {
									DT.DefaultView[0].Row.Delete();
								}
							}
						}
					} else {
						DT.DefaultView.RowFilter = "POS_NO='" + POS + "'" + "  AND Goal_NO='" + lblCareerPath_NO.Text + "'";
						if (DT.DefaultView.Count > 0) {
							DT.DefaultView[0].Row.Delete();
						}
					}

					 cmd = new SqlCommandBuilder(DA);
					try {
						DA.Update(DT);
					} catch {
					}
					//--------------- ReOrder -----------
					for (int i = 0; i <= (DT.Rows.Count - 1); i++) {
						DT.Rows[i]["Goal_NO"] = (i + 1);
					}

					cmd = new SqlCommandBuilder(DA);
					try {
						DA.Update(DT);
					} catch {
					}

					BindCareerPath();

					break;
				case "lnk_add_POS_NO":
					LinkButton btnAdd_POS_NO =(LinkButton) e.Item.FindControl("btnAdd_POS_NO");
                    
					Goal_NO = lblCareerPath_NO.Text;

					//-------------ตรวจสอบตำแหน่งถัดไป สามารถเข้ารับตำแหน่งก่อนหน้าหรือปัจจุบันได้หรือไม่---------------
					 SQL = "";
					SQL += "   SELECT 'Goal' Property,PSNL_NO,Goal_NO,POS_No,MGR_CODE,MGR_NAME,PNPS_CLASS,DEPT_NAME FROM tb_Path_Setting_Self ";
					SQL += "   WHERE PSNL_NO='" + Session["USER_PSNL_NO"].ToString().Replace("'", "''") + "'\n";
					SQL += "   AND Goal_NO=" + (GL.CINT(Goal_NO) - 1) + "\n";
					SQL += "   Order By PSNL_NO,Goal_NO";
					 DA = new SqlDataAdapter(SQL, BL.ConnectionString());
					 DT = new DataTable();
					DA.Fill(DT);

                    if (DT.Rows.Count == Convert.ToInt32(0))
                    {
						Pos_NO_Left = POS_NO;
						if (string.IsNullOrEmpty(lblCareerPath_NO.Attributes["PNPS_CLASS"])) {
							Class_Pos_NO_Left = 0;
						} else {
							Class_Pos_NO_Left = GL.CINT(lblCareerPath_NO.Attributes["PNPS_CLASS"]);
						}

					} else {
						Pos_NO_Left = DT.Rows[0]["POS_No"].ToString();
						Class_Pos_NO_Left = Convert.ToInt32(DT.Rows[0]["PNPS_CLASS"]);
					}

					BindPostList();
					ModalPost.Visible = true;
					ScriptManager.RegisterStartupScript(this.Page, typeof(string), "Focus", "document.getElementById('" + txtSearchPos.ClientID + "').focus();", true);

					break;
				case "PosCompare":
                    Button btnPosCompare = (Button)e.Item.FindControl("btnPosCompare");
					string POS_No_Select = e.CommandArgument.ToString ();
					//--------Header-------------
					 DT = null;
					DT = BindDetail_POS(POS_No_Select);
					if (DT.Rows.Count > 0) {
						lblHeaderPos.Text = "<B>" + DT.Rows[0]["POS_NO"].ToString() + " : " + DT.Rows[0]["MGR_NAME"].ToString();
						lblHeaderPos_Detail.Text = "</B>" + " ระดับ : " + Convert.ToInt32(DT.Rows[0]["PNPO_CLASS"]) + " " + DT.Rows[0]["DEPT_NAME"].ToString();

						lblTMP_POS_SetPath.Attributes["MGR_NAME"] = DT.Rows[0]["MGR_NAME"].ToString();
						lblTMP_POS_SetPath.Attributes["PNPO_CLASS"] = Convert.ToInt32(DT.Rows[0]["PNPO_CLASS"]).ToString ();
						lblTMP_POS_SetPath.Attributes["DEPT_NAME"] = DT.Rows[0]["DEPT_NAME"].ToString();
					} else {
						lblHeaderPos.Text = "";
						lblHeaderPos_Detail.Text = "";
						lblTMP_POS_SetPath.Attributes["MGR_NAME"] = "";
						lblTMP_POS_SetPath.Attributes["PNPO_CLASS"] = "";
						lblTMP_POS_SetPath.Attributes["DEPT_NAME"] = "";
					}

					lblTMP_POS_SetPath.Text = POS_No_Select;

					//-----Ass Leave  Push
					BindAss_Leave_Push(POS_No_Select, PNPS_PSNL_NO);
					BindCourse_Property(POS_No_Select, PNPS_PSNL_NO);
					BindTest_Property(POS_No_Select, PNPS_PSNL_NO);

					//----------Course-------
					DataTable DT_Course_Compare = null;
					DT_Course_Compare = BindCourse_Compare(POS_No_Select, PNPS_PSNL_NO);
					rptDetail_Course.DataSource = DT_Course_Compare;
					rptDetail_Course.DataBind();
                    lblCount_Course.Text = DT_Course_Compare.Rows.Count.ToString();
					Check_Course = GL.CINT (DT_Course_Compare.Rows.Count).ToString ();
					DT_Course_Compare.DefaultView.RowFilter = " STSTUS_Course=1";
					if (GL.CINT(DT_Course_Compare.DefaultView.Count) == GL.CINT(Check_Course)) {
						icon_Course.Style["color"] = "Green";
					} else {
						icon_Course.Style["color"] = "Red";
					}

					//----------Tset------------
					DataTable DT_Test_Compare = null;
					DT_Test_Compare = BindTest_Compare(POS_No_Select, PNPS_PSNL_NO);
					rptDetail_Test.DataSource = DT_Test_Compare;
					rptDetail_Test.DataBind();
                    lblCount_Test.Text = DT_Test_Compare.Rows.Count.ToString();

                    Check_Test = DT_Test_Compare.Rows.Count.ToString();
					DT_Test_Compare.DefaultView.RowFilter = " STSTUS_Test=1";
					if (GL.CINT (DT_Test_Compare.DefaultView.Count) == GL.CINT (Check_Test)) {
						icon_Test.Style["color"] = "Green";
					} else {
						icon_Test.Style["color"] = "Red";
					}
					ModelCompareDetail.Visible = true;
					btnSetPath.Visible = false;
					break;
			}

		}

		//------------------ตรวจสอบก่อนลบตำแหน่งถัดไป-------------
		public DataTable DT_Check_POS(string POS_NO_Check)
		{
			string SQL = "";
			SQL += "  SELECT From_POS_No , To_POS_No\n";
			SQL += "  ,vw_PN_Position.POS_NO\n";
			SQL += "  ,vw_PN_Position.DEPT_NAME\n";
			SQL += "  ,vw_PN_Position.PNPO_CLASS\n";
			SQL += "  ,vw_PN_Position.MGR_NAME\n";
			SQL += "  FROM  tb_Path_Setting_Route Setting_Route\n";
			SQL += "  LEFT JOIN tb_Path_Setting Path_Setting  ON Path_Setting.POS_No=Setting_Route.From_POS_No\n";
			SQL += "  INNER JOIN vw_PN_Position ON vw_PN_Position.POS_NO=Setting_Route.To_POS_No\n";
			SQL += "  WHERE From_POS_No ='" + POS_NO_Check + "'\n";

			SqlDataAdapter DA = new SqlDataAdapter(SQL, BL.ConnectionString());
			DataTable DT = new DataTable();
			DA.Fill(DT);

			return DT;
		}

		protected void rptAddPos_ItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
		{
			if (e.Item.ItemType != ListItemType.AlternatingItem & e.Item.ItemType != ListItemType.Item)
				return;
            Label lblPosGoal = (Label)e.Item.FindControl("lblPosGoal");
            Label lblCareerPath_NO = (Label)e.Item.FindControl("lblCareerPath_NO");
			Label lblNo =(Label) e.Item.FindControl("lblNo");

			Button btnDelete =(Button) e.Item.FindControl("btnDelete");
			HtmlAnchor btnDeleteLeft_Close =(HtmlAnchor) e.Item.FindControl("btnDeleteLeft_Close");
            Button btnPosCompare = (Button)e.Item.FindControl("btnPosCompare");
			HtmlAnchor lnkL =(HtmlAnchor) e.Item.FindControl("lnkL");

            Panel pnl_Goal = (Panel)e.Item.FindControl("pnl_Goal");
            Panel pnl_Add = (Panel)e.Item.FindControl("pnl_Add");
			LinkButton btnAdd_POS_NO =(LinkButton) e.Item.FindControl("btnAdd_POS_NO");
            LinkButton lnkEdit_POS = (LinkButton)e.Item.FindControl("lnkEdit_POS");
            DataRowView drv = (DataRowView)e.Item.DataItem;
			if (drv["Property"].ToString () == "Goal") {
				lblPosGoal.Text = drv["POS_NO"] + " : " + drv["MGR_NAME"].ToString() + " ระดับ" + Convert.ToInt32(drv["PNPS_CLASS"]) + " หน่วยงาน " + drv["DEPT_NAME"].ToString();
                btnDelete.CommandArgument = drv["POS_NO"].ToString();
				btnDeleteLeft_Close.Attributes["onclick"] = "document.getElementById('" + btnDelete.ClientID + "').click();";
				btnDeleteLeft_Close.Attributes["btnDeleteLeft_Close"] = drv["POS_NO"].ToString ();



                Label lblAss_MIN_Score_Property = (Label)e.Item.FindControl("lblAss_MIN_Score_Property");
				Label lblAss_Min_Score = (Label)e.Item.FindControl("lblAss_Min_Score");
                Panel icon_Ass_green = (Panel)e.Item.FindControl("icon_Ass_green");
                Panel icon_Ass_Red = (Panel)e.Item.FindControl("icon_Ass_Red");

                Label lblLeave_Property = (Label)e.Item.FindControl("lblLeave_Property");
                Label lblLeave = (Label)e.Item.FindControl("lblLeave");
                Panel icon_Leave_green = (Panel)e.Item.FindControl("icon_Leave_green");
                Panel icon_Leave_Red = (Panel)e.Item.FindControl("icon_Leave_Red");

				Label lblPunishment =(Label) e.Item.FindControl("lblPunishment");
                Panel icon_Punis_green = (Panel)e.Item.FindControl("icon_Punis_green");
                Panel icon_Punis_Red = (Panel)e.Item.FindControl("icon_Punis_Red");


				DataTable DT_Path_Setting_Detail = Path_Setting_Detail(drv["POS_NO"].ToString());

                DataTable DT_rpt_Ass_Leave_Push = Bind_rpt_Ass_Leave_Push(drv["POS_NO"].ToString(), PNPS_PSNL_NO);
                if (DT_rpt_Ass_Leave_Push.Rows.Count > 0)
                {
                    //-------1---------------------------
                    if (GL.CINT(GL.CDBL(Check_Ass_MIN_Score)) > 0)
                    {
                        lblAss_MIN_Score_Property.Text = " | เกณฑ์ " + GL.StringFormatNumber(GL.CDBL(Check_Ass_MIN_Score)) + " คะแนน | ";
                    }
                    else
                    {
                        lblAss_MIN_Score_Property.Text = " | ยังไม่ระบุเกณฑ์ | ";
                    }

                    if (Information.IsNumeric(DT_rpt_Ass_Leave_Push.Rows[0]["Year_Score"]))
                    {
                        lblAss_Min_Score.Text = "  ผลการประเมินเท่ากับ " + GL.StringFormatNumber(GL.CDBL(DT_rpt_Ass_Leave_Push.Rows[0]["Year_Score"])) + " คะแนน ";
                    }
                    else
                    {
                        lblAss_Min_Score.Text = "  ผลการประเมินเท่ากับ 0.00 คะแนน";
                    }

                    if (GL.CDBL(Check_Ass_MIN_Score) <= GL.CDBL((DT_rpt_Ass_Leave_Push.Rows[0]["Year_Score"])))
                    {
                        icon_Ass_green.Visible = true;
                        icon_Ass_Red.Visible = false;
                        lblAss_Min_Score.Style["color"] = "Green";
                    }
                    else
                    {
                        icon_Ass_green.Visible = false;
                        icon_Ass_Red.Visible = true;
                        lblAss_Min_Score.Style["color"] = "Red";
                    }

                    //-------2---------------------------
                    if (GL.CINT(GL.CDBL(Check_Leave)) > 0)
                    {
                        lblLeave_Property.Text = " | เกณฑ์ " + GL.StringFormatNumber(GL.CDBL(Check_Leave)) + " วัน  " + " | ";
                    }
                    else
                    {
                        lblLeave_Property.Text = " | ยังไม่ระบุเกณฑ์ " + " | ";
                    }



                    if (Information.IsNumeric(DT_rpt_Ass_Leave_Push.Rows[0]["LEAVE_All"]))
                    {
                        lblLeave.Text = " จำนวนวันรวม " + GL.StringFormatNumber(GL.CDBL(DT_rpt_Ass_Leave_Push.Rows[0]["LEAVE_All"])) + " วัน";
                    }
                    else
                    {
                        lblLeave.Text = " ไม่มีการลา ";
                    }

                    if (GL.CDBL(Check_Leave) >= GL.CDBL(DT_rpt_Ass_Leave_Push.Rows[0]["LEAVE_All"]))
                    {
                        icon_Leave_green.Visible = true;
                        icon_Leave_Red.Visible = false;
                        lblLeave.Style["color"] = "Green";
                    }
                    else if (Convert.ToInt32(Check_Leave) == Convert.ToInt32(0))
                    {
                        icon_Leave_green.Visible = true;
                        icon_Leave_Red.Visible = false;
                        lblLeave.Style["color"] = "Green";
                    }
                    else
                    {
                        icon_Leave_green.Visible = false;
                        icon_Leave_Red.Visible = true;
                        lblLeave.Style["color"] = "Red";
                    }


                    //-------5---PUNISH------------------------
                    switch (DT_rpt_Ass_Leave_Push.Rows[0]["History_PUNISH"].ToString())
                    {
                        case "":
                            lblPunishment.Text = " ไม่เคยรับโทษ ";
                            lblPunishment.ForeColor = System.Drawing.Color.Green;
                            break;
                        case "ไม่เคยรับโทษ":
                            lblPunishment.Text = " ไม่เคยรับโทษ ";
                            lblPunishment.ForeColor = System.Drawing.Color.Green;
                            break;
                        case "เคยรับโทษ":
                            lblPunishment.Text = " เคยรับโทษ ";
                            lblPunishment.ForeColor = System.Drawing.Color.Red;
                            break;
                    }

                    if ((Check_Punishment) == "True")
                    {
                        if ((DT_rpt_Ass_Leave_Push.Rows[0]["History_Status_PUNISH"]).ToString() == "1")
                        {
                            icon_Punis_green.Visible = true;
                            icon_Punis_Red.Visible = false;
                        }
                        else
                        {
                            icon_Punis_green.Visible = false;
                            icon_Punis_Red.Visible = true;
                        }
                    }
                    else
                    {
                        icon_Punis_green.Visible = true;
                        icon_Punis_Red.Visible = false;
                    }
                }
                else
                {
                    //----------Ass Score------------------------
                    lblAss_MIN_Score_Property.Text = " | ยังไม่ระบุเกณฑ์ | ";
                    lblAss_Min_Score.Text = "  ไม่มีผลการประเมิน";
                    icon_Ass_green.Visible = true;
                    icon_Ass_Red.Visible = false;
                    lblAss_Min_Score.Style["color"] = "Green";

                    //----------Leave------------------------
                    lblLeave_Property.Text = " | ยังไม่ระบุเกณฑ์ " + " | ";
                    lblLeave.Text = " ไม่มีการลา ";
                    icon_Leave_green.Visible = true;
                    icon_Leave_Red.Visible = false;
                    lblLeave.Style["color"] = "Green";

                    //----------PUNISH------------------------
                    lblPunishment.Text = " ไม่เคยรับโทษ ";
                    lblPunishment.ForeColor = System.Drawing.Color.Green;
                    icon_Punis_green.Visible = true;
                    icon_Punis_Red.Visible = false;
                }

                //'-----การฝึกอบรม----------
                Panel icon_Course_green = (Panel)e.Item.FindControl("icon_Course_green");
                Panel icon_Course_Red = (Panel)e.Item.FindControl("icon_Course_Red");

                DataTable DT_Course_Compare = null;
                DT_Course_Compare = BindCourse_Compare(drv["POS_NO"].ToString(), PNPS_PSNL_NO);
                Repeater rptCourse = (Repeater)e.Item.FindControl("rptCourse");
                Label lblCount_Course = (Label)e.Item.FindControl("lblCount_Course");
                rptCourse.ItemDataBound += rptCourse_ItemDataBound;
                lblCount_Course.Text = DT_Course_Compare.Rows.Count.ToString();
                rptCourse.DataSource = DT_Course_Compare;
                rptCourse.DataBind();

                DT_Course_Compare.DefaultView.RowFilter = " STSTUS_Course=1";
                if (DT_Course_Compare.DefaultView.Count == DT_Course_Compare.Rows.Count)
                {
                    icon_Course_green.Visible = true;
                    icon_Course_Red.Visible = false;
                }
                else
                {
                    icon_Course_green.Visible = false;
                    icon_Course_Red.Visible = true;
                }

                Panel icon_Test_green = (Panel)e.Item.FindControl("icon_Test_green");
                Panel icon_Test_Red = (Panel)e.Item.FindControl("icon_Test_Red");
                Repeater rptTest = (Repeater)e.Item.FindControl("rptTest");
                Label lblCount_Test = (Label)e.Item.FindControl("lblCount_Test");

                DataTable DT_Test_Compare = null;
                DT_Test_Compare = BindTest_Compare(drv["POS_NO"].ToString(), PNPS_PSNL_NO);
                rptTest.ItemDataBound += rptTest_Detail_ItemDataBound;
                lblCount_Test.Text = (DT_Test_Compare.Rows.Count).ToString();
                rptTest.DataSource = DT_Test_Compare;
                rptTest.DataBind();

                DT_Test_Compare.DefaultView.RowFilter = " STSTUS_Test=1";
                if (DT_Test_Compare.DefaultView.Count == DT_Test_Compare.Rows.Count)
                {
                    icon_Test_green.Visible = true;
                    icon_Test_Red.Visible = false;
                }
                else
                {
                    icon_Test_green.Visible = false;
                    icon_Test_Red.Visible = true;
                }



                btnPosCompare.CommandArgument = drv["POS_NO"].ToString();
				lnkL.HRef = "CP_Setting_Property.aspx?POS_NO=" + drv["POS_NO"];
                lblNo.Text = drv["Goal_NO"].ToString ();
                lblCareerPath_NO.Text = drv["Goal_NO"].ToString();
				pnl_Goal.Visible = true;
				pnl_Add.Visible = false;

				lnkEdit_POS.ToolTip = "แก้ไขเป้าหมาย";
			} else {
				lblPosGoal.Text = "";
                lblCareerPath_NO.Text = drv["Goal_NO"].ToString();

				pnl_Goal.Visible = false;
				pnl_Add.Visible = true;

				btnAdd_POS_NO.Attributes["btnAdd_PSNL_NO"] = drv["PSNL_NO"].ToString ();
				btnAdd_POS_NO.ToolTip = "เพิ่มเป้าหมาย เส้นทางสายอาชีพ";
				btnAdd_POS_NO.Visible = true;
			}
            lblCareerPath_NO.Attributes["PNPS_CLASS"] = drv["PNPS_CLASS"].ToString();
		}

		public DataTable Path_Setting_Detail(string POS_NO_Detail)
		{
			string SQL = "";
			string Setting_POS_NO = "";
			if (string.IsNullOrEmpty(POS_NO_Detail)) {
				Setting_POS_NO = " WHERE  0=1 ";
			} else {
				Setting_POS_NO = "  WHERE POS_No ='" + POS_NO_Detail + "'";
			}
			SQL = "";
			SQL += "   SELECT ";
			SQL += "   POS_No PSNL_NO,";
			SQL += "   Ass_Min_Score Year_Score,";
			SQL += "   Leave_Score LEAVE_All,";
			SQL += "   Punishment History_Status_PUNISH,";
			SQL += "   CASE WHEN Punishment=1 THEN 'ไม่เคยรับโทษ' ELSE 'เคยรับโทษ' END History_PUNISH ";
			SQL += "   FROM tb_Path_Setting ";
			SQL += "   " + Setting_POS_NO + "\n";

			SqlDataAdapter DA = new SqlDataAdapter(SQL, BL.ConnectionString());
			DataTable DT = new DataTable();
			DA.Fill(DT);


			if (DT.Rows.Count == 1) {
				//---------Check-----------
				if (GL.IsEqualNull(DT.Rows[0]["Year_Score"])) {
					Check_Ass_MIN_Score = GL.StringFormatNumber(0);
				} else {
                    Check_Ass_MIN_Score = DT.Rows[0]["Year_Score"].ToString();
				}
				if (GL.IsEqualNull(DT.Rows[0]["LEAVE_All"])) {
					Check_Leave = GL.StringFormatNumber(0);
				} else {
                    Check_Leave = DT.Rows[0]["LEAVE_All"].ToString();
				}
                Check_Punishment = DT.Rows[0]["History_Status_PUNISH"].ToString();
			} else {
				Check_Ass_MIN_Score = GL.StringFormatNumber(0);
				Check_Leave = GL.StringFormatNumber(0);
				Check_Punishment = "False";
			}

			return DT;
		}

		private void BindCourse_Detail()
		{
			string SQL = "";
			SQL += "   SELECT Training.POS_No,COURSE.COURSE_ID,COURSE.COURSE_DESC \n";
			SQL += "   FROM tb_Path_Setting_Training Training\n";
			SQL += "   LEFT JOIN tb_PK_COURSE COURSE ON Training.COURSE_ID=COURSE.COURSE_ID\n";
			SQL += "   ORDER BY Training.POS_No,COURSE.COURSE_ID,COURSE.COURSE_DESC\n";
			SqlDataAdapter DA = new SqlDataAdapter(SQL, BL.ConnectionString());
			DataTable DT = new DataTable();
			DA.Fill(DT);
			DT_Course_Detail = DT;
		}


		protected void rptCourse_ItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
		{
			if (e.Item.ItemType != ListItemType.AlternatingItem & e.Item.ItemType != ListItemType.Item)
				return;
            Label lblCourse = (Label)e.Item.FindControl("lblCourse");
            Label lblCourse_Status = (Label)e.Item.FindControl("lblCourse_Status");
            DataRowView drv = (DataRowView)e.Item.DataItem;
			
			lblCourse.Text = drv["COURSE_DESC"].ToString();
            if (GL.CINT(drv["STSTUS_Course"]) == Convert.ToInt32(0))
            {
				lblCourse_Status.Text = " (ไม่เคยเข้าอบรม)";
				lblCourse_Status.Style["color"] = "Red";
            }
            else if (GL.CINT(drv["STSTUS_Course"]) == Convert.ToInt32(1))
            {
				lblCourse_Status.Text = " (ผ่านการฝึกอบรม)";
				lblCourse_Status.Style["color"] = "Green";
			} else {
				lblCourse_Status.Text = "";
			}
		}

		private void BindTest_Detail()
		{
			string SQL = "";
			SQL += " SELECT Setting_Test.POS_No,Test.CPSM_SUBJECT_CODE,Test.CPSM_SUBJECT_NAME,Setting_Test.Min_Score \n";
			SQL += " FROM tb_Path_Setting_Test Setting_Test\n";
			SQL += " LEFT JOIN tb_CP_SUBJECT_MASTER Test ON Setting_Test.SUBJECT_CODE=Test.CPSM_SUBJECT_CODE\n";
			SQL += " ORDER BY Setting_Test.POS_No,Test.CPSM_SUBJECT_CODE,Test.CPSM_SUBJECT_NAME\n";
			SqlDataAdapter DA = new SqlDataAdapter(SQL, BL.ConnectionString());
			DataTable DT = new DataTable();
			DA.Fill(DT);
			DT_Test_Detail = DT;
		}


		protected void rptTest_Detail_ItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
		{
			if (e.Item.ItemType != ListItemType.AlternatingItem & e.Item.ItemType != ListItemType.Item)
				return;
            Label lblTest = (Label)e.Item.FindControl("lblTest");
            DataRowView drv = (DataRowView)e.Item.DataItem;

			lblTest.Text = drv["CPSM_SUBJECT_NAME"].ToString();

            Label lblMin_Score = (Label)e.Item.FindControl("lblMin_Score");
			if (GL.CINT(drv["Pass_Score"]) ==  Convert .ToInt32 (0)) {
				lblMin_Score.Text = " (ไม่ผ่าน)";
				lblMin_Score.Style["color"] = "Red";
            }
            else if (GL.CINT(drv["Pass_Score"]) == Convert.ToInt32(1))
            {
				lblMin_Score.Text = " (ผ่าน)";
				lblMin_Score.Style["color"] = "Green";
			} else {
				lblMin_Score.Text = "";
			}
		}


		private void BindPostList()
		{
			//------------NOT IN ตำแหน่งที่เลือกไปแล้ว------------------------
			string Filter_Not_In = "";
			POS_NO_Right = "";
			string SQL = "";
			SQL += "   SELECT 'Goal' Property,PSNL_NO,Goal_NO,POS_No,MGR_CODE,MGR_NAME,PNPS_CLASS,DEPT_NAME FROM tb_Path_Setting_Self ";
			SQL += "   WHERE PSNL_NO='" + Session["USER_PSNL_NO"].ToString().Replace("'", "''") + "'\n";
			SQL += "   Order By PSNL_NO,Goal_NO";
			SqlDataAdapter DA = new SqlDataAdapter(SQL, BL.ConnectionString());
			DataTable DT_Not_In = new DataTable();
			DA.Fill(DT_Not_In);
			if (DT_Not_In.Rows.Count > 0) {
				for (int i = 0; i <= DT_Not_In.Rows.Count - 1; i++) {
					POS_NO_Right += "'" + DT_Not_In.Rows[i]["POS_NO"] + "'" + ",";
				}
				Filter_Not_In = " AND POS.POS_NO NOT IN (" + "'" + POS_NO + "'," + POS_NO_Right.Substring(0, POS_NO_Right.Length - 1) + ")";
			} else {
				Filter_Not_In = " ";
			}


			DataTable DT = new DataTable();
			DT = DT_Check_POS(Pos_NO_Left);
			DT.DefaultView.RowFilter = "PNPO_CLASS IN (" + Convert.ToInt32(Class_Pos_NO_Left) + "," + Convert.ToInt32(Class_Pos_NO_Left) + 1 + ")";

			string Filter_In = "";
			POS_NO_Right = "";

			//------------เลือกเป้าหมายที่ตำแหน่งก่อนหน้ามีสิทธิ์เข้ารับ------------------------
			//If POS_NO_Right = "" Then
			if (DT.Rows.Count > 0) {
				for (int i = 0; i <= DT.Rows.Count - 1; i++) {
					POS_NO_Right += "'" + DT.Rows[i]["POS_NO"] + "'" + ",";
				}
				Filter_In = " AND POS.POS_NO IN (" + POS_NO_Right.Substring(0, POS_NO_Right.Length - 1) + ")";
			} else {
				Filter_In = " AND 0=1";
			}
			//Else
			//Filter = " AND POS.POS_NO IN (" & POS_NO_Right.Substring(0, POS_NO_Right.Length - 1) & ")"
			//End If

			SQL = "";
			SQL += " SELECT DEPT.SECTOR_CODE,DEPT.SECTOR_NAME,DEPT.DEPT_CODE,DEPT.DEPT_NAME,POS.POS_NO,ISNULL(POS.MGR_NAME,'-') MGR_NAME,ISNULL(POS.FLD_NAME,'-') FLD_NAME,\n";
			SQL += "  POS.PNPO_CLASS,PSN.PSNL_NO,PSN.PSNL_Fullname,POS.Update_Time \n";
			SQL += "  ,Setting_Status.Status_Path\n";
			SQL += "  FROM vw_PN_Position POS \n";
			SQL += "  INNER JOIN vw_HR_Department DEPT ON POS.DEPT_CODE=DEPT.DEPT_CODE AND POS.MINOR_CODE=DEPT.MINOR_CODE\n";
			SQL += "  LEFT JOIN vw_PN_PSNL PSN ON POS.POS_NO=PSN.POS_NO\n";
			SQL += "  INNER JOIN vw_Path_Setting_Status Setting_Status ON Setting_Status.POS_No = POS.POS_NO\n";
			SQL += "  WHERE POS.PNPO_CLASS IS NOT NULL   \n";
			SQL += "  " + Filter_In + "\n";
			SQL += "  " + Filter_Not_In + "\n";

			if (!string.IsNullOrEmpty(txtSearchPos.Text)) {
				SQL += " AND (DEPT.SECTOR_NAME LIKE '%" + txtSearchPos.Text.Replace("'", "''") + "%' OR \n";
				SQL += " DEPT.DEPT_NAME LIKE '%" + txtSearchPos.Text.Replace("'", "''") + "%' OR \n";
				SQL += " POS.POS_NO LIKE '%" + txtSearchPos.Text.Replace("'", "''") + "%' OR \n";
				SQL += " POS.MGR_NAME LIKE '%" + txtSearchPos.Text.Replace("'", "''") + "%' OR \n";
				SQL += " POS.FLD_NAME LIKE '%" + txtSearchPos.Text.Replace("'", "''") + "%' )\n";
			}




			SQL += "  GROUP BY DEPT.SECTOR_NAME,DEPT.DEPT_NAME,POS.POS_NO,POS.FLD_NAME,POS.MGR_NAME,\n";
			SQL += "  POS.PNPO_CLASS, PSN.PSNL_NO, PSN.PSNL_Fullname, POS.Update_Time, DEPT.SECTOR_CODE, DEPT.DEPT_CODE, POS.PNPO_CLASS, POS.POS_NO\n";
			SQL += "  , Setting_Status.Status_Path\n";
			SQL += "  ORDER BY DEPT.SECTOR_CODE,DEPT.DEPT_CODE,POS.PNPO_CLASS DESC,POS.POS_NO\n";

			DA = new SqlDataAdapter(SQL, BL.ConnectionString());
			DT = new DataTable();
			DA.Fill(DT);

			Session["Compare_Personal_POS"] = DT;
			Pager_Pos.SesssionSourceName = "Compare_Personal_POS";
			Pager_Pos.RenderLayout();

            if (DT.Rows.Count == Convert.ToInt32(0))
            {
				lblCountPos.Text = "ไม่พบรายการดังกล่าว";
			} else {
				lblCountPos.Text = "พบ " + GL.StringFormatNumber(DT.Rows.Count, 0) + " รายการ";
			}
		}
		protected void Pager_Pos_PageChanging(PageNavigation Sender)
		{
			Pager_Pos.TheRepeater = rptPosDialog;
		}

		string LastSectorPos = "";

		string LastDeptPos = "";

		protected void rptPosDialog_ItemCommand(object source, System.Web.UI.WebControls.RepeaterCommandEventArgs e)
		{
			switch (e.CommandName) {
				case "Select":
					Button btnPosSelect = (Button) e.Item.FindControl("btnPosSelect");
					string POS_No_Select = btnPosSelect.CommandArgument;

					Label lblDept =(Label) e.Item.FindControl("lblDept");
					Label lblClass = (Label) e.Item.FindControl("lblClass");
					Label lblTMPDept =(Label) e.Item.FindControl("lblTMPDept");
					Label lblMGR =(Label) e.Item.FindControl("lblMGR");




					//--------Header-------------

					DataTable DT = null;
					DT = BindDetail_POS(POS_No_Select);
					if (DT.Rows.Count > 0) {
						lblHeaderPos.Text = "<B>" + DT.Rows[0]["POS_NO"].ToString() + " : " + DT.Rows[0]["MGR_NAME"].ToString();
						lblHeaderPos_Detail.Text = "</B>" + " ระดับ : " + Convert.ToInt32(DT.Rows[0]["PNPO_CLASS"]) + " " + DT.Rows[0]["DEPT_NAME"].ToString();

						lblTMP_POS_SetPath.Attributes["MGR_NAME"] = DT.Rows[0]["MGR_NAME"].ToString();
                        lblTMP_POS_SetPath.Attributes["PNPO_CLASS"] = Convert.ToInt32(DT.Rows[0]["PNPO_CLASS"]).ToString();
						lblTMP_POS_SetPath.Attributes["DEPT_NAME"] = DT.Rows[0]["DEPT_NAME"].ToString();
					} else {
						lblHeaderPos.Text = "";
						lblHeaderPos_Detail.Text = "";
						lblTMP_POS_SetPath.Attributes["MGR_NAME"] = "";
						lblTMP_POS_SetPath.Attributes["PNPO_CLASS"] = "";
						lblTMP_POS_SetPath.Attributes["DEPT_NAME"] = "";
					}

					lblTMP_POS_SetPath.Text = POS_No_Select;

					//-----Ass Leave  Push
					BindAss_Leave_Push(POS_No_Select, PNPS_PSNL_NO);
					BindCourse_Property(POS_No_Select, PNPS_PSNL_NO);
					BindTest_Property(POS_No_Select, PNPS_PSNL_NO);


					//----------Course-------
					DataTable DT_Course_Compare = null;
					DT_Course_Compare = BindCourse_Compare(POS_No_Select, PNPS_PSNL_NO);
					rptDetail_Course.DataSource = DT_Course_Compare;
					rptDetail_Course.DataBind();
                    lblCount_Course.Text = DT_Course_Compare.Rows.Count.ToString();
                    Check_Course = DT_Course_Compare.Rows.Count.ToString();
					DT_Course_Compare.DefaultView.RowFilter = " STSTUS_Course=1";
					if ((DT_Course_Compare.DefaultView.Count) == Convert.ToInt32((Check_Course))) {
						//cel_Course.Style("background-color") = "#E3F9E5"
						icon_Course.Style["color"] = "Green";
					} else {
						//cel_Course.Style("background-color") = "#FEE4E4"
						icon_Course.Style["color"] = "Red";
					}

					//----------Tset------------
					DataTable DT_Test_Compare = null;
					DT_Test_Compare = BindTest_Compare(POS_No_Select, PNPS_PSNL_NO);
					rptDetail_Test.DataSource = DT_Test_Compare;
					rptDetail_Test.DataBind();
                    lblCount_Test.Text = DT_Test_Compare.Rows.Count.ToString();

                    Check_Test = DT_Test_Compare.Rows.Count.ToString();
					DT_Test_Compare.DefaultView.RowFilter = " STSTUS_Test=1";
					if (GL.CINT (DT_Test_Compare.DefaultView.Count) == GL.CINT (Check_Test)) {
						icon_Test.Style["color"] = "Green";
					} else {
						icon_Test.Style["color"] = "Red";
					}

					btnSetPath.Visible = true;

					ModelCompareDetail.Visible = true;

					break;
			}
		}

		protected void rptPosDialog_ItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
		{
			if (e.Item.ItemType != ListItemType.AlternatingItem & e.Item.ItemType != ListItemType.Item)
				return;
			Label lblSector =(Label) e.Item.FindControl("lblSector");
			Label lblDept =(Label) e.Item.FindControl("lblDept");
			Label lblTMPDept =(Label) e.Item.FindControl("lblTMPDept");
			Label lblClass = (Label) e.Item.FindControl("lblClass");
			Label lblPosNo =(Label) e.Item.FindControl("lblPosNo");
			Label lblMGR =(Label) e.Item.FindControl("lblMGR");
			Label lblPSN = (Label) e.Item.FindControl("lblPSN");
			Button btnPosSelect = (Button) e.Item.FindControl("btnPosSelect");

            DataRowView drv = (DataRowView)e.Item.DataItem;

			if (LastSectorPos != (drv["SECTOR_NAME"].ToString())) {
				lblSector.Text = drv["SECTOR_NAME"].ToString();
				LastSectorPos = drv["SECTOR_NAME"].ToString();
				lblDept.Text = drv["DEPT_NAME"].ToString();
				LastDeptPos = drv["DEPT_NAME"].ToString();
			} else if (LastDeptPos != drv["DEPT_NAME"].ToString()) {
				lblDept.Text = drv["DEPT_NAME"].ToString();
				LastDeptPos = drv["DEPT_NAME"].ToString();
			}
			lblTMPDept.Text = drv["DEPT_NAME"].ToString();
			lblClass.Text = GL.CINT (drv["PNPO_CLASS"]).ToString ();
			if (GL.IsEqualNull(drv["POS_NO"])) {
				lblPosNo.Text = "-";
			} else {
				lblPosNo.Text = drv["POS_NO"].ToString() + ":" + drv["FLD_NAME"].ToString();
			}
			lblMGR.Text = drv["MGR_NAME"].ToString();
            btnPosSelect.CommandArgument = drv["POS_NO"].ToString();

		}


		protected void btnDeleteModel_Close_ServerClick(object sender, System.EventArgs e)
		{
			ModalPost.Visible = false;
		}





		//------BOX Detail COmpare------------------------

		//---------------header-----------------
		public DataTable BindDetail_POS(string POS_NO)
		{
			string filter = "";
			if (!string.IsNullOrEmpty(POS_NO)) {
				filter = " AND POS.POS_NO='" + POS_NO + "'";
			}
			string SQL = "";
			SQL += " SELECT DEPT.SECTOR_CODE,DEPT.SECTOR_NAME,DEPT.DEPT_CODE,DEPT.DEPT_NAME,POS.POS_NO,ISNULL(POS.MGR_NAME,'-') MGR_NAME,ISNULL(POS.FLD_NAME,'-') FLD_NAME,\n";
			SQL += "  POS.PNPO_CLASS,PSN.PSNL_NO,PSN.PSNL_Fullname,POS.Update_Time \n";
			SQL += "  ,Setting_Status.Status_Path\n";
			SQL += "  FROM vw_PN_Position POS \n";
			SQL += "  INNER JOIN vw_HR_Department DEPT ON POS.DEPT_CODE=DEPT.DEPT_CODE AND POS.MINOR_CODE=DEPT.MINOR_CODE\n";
			SQL += "  LEFT JOIN vw_PN_PSNL PSN ON POS.POS_NO=PSN.POS_NO\n";
			SQL += "  INNER JOIN vw_Path_Setting_Status Setting_Status ON Setting_Status.POS_No = POS.POS_NO\n";
			SQL += "  WHERE POS.PNPO_CLASS IS NOT NULL   \n";
			SQL += "" + filter + "\n";
			SQL += "  GROUP BY DEPT.SECTOR_NAME,DEPT.DEPT_NAME,POS.POS_NO,POS.FLD_NAME,POS.MGR_NAME,\n";
			SQL += "  POS.PNPO_CLASS, PSN.PSNL_NO, PSN.PSNL_Fullname, POS.Update_Time, DEPT.SECTOR_CODE, DEPT.DEPT_CODE, POS.PNPO_CLASS, POS.POS_NO\n";
			SQL += "  , Setting_Status.Status_Path\n";
			SQL += "  ORDER BY DEPT.SECTOR_CODE,DEPT.DEPT_CODE,POS.PNPO_CLASS DESC,POS.POS_NO\n";

			SqlDataAdapter DA = new SqlDataAdapter(SQL, BL.ConnectionString());
			DataTable DT = new DataTable();
			DA.Fill(DT);
			return DT;

		}


		public DataTable BindAss_Leave_Push(string POS_NO, string PNPS_PSNL_NO)
		{
			//Private Sub BindCompareDetail()
			string SQL = "";
			string Setting_POS_NO = "";
			if (string.IsNullOrEmpty(POS_NO)) {
				Setting_POS_NO = " WHERE  0=1 ";
			} else {
				Setting_POS_NO = "  WHERE POS_No ='" + POS_NO + "'";
			}

			string Setting_PSNL_NO = "";
			if (string.IsNullOrEmpty(PNPS_PSNL_NO)) {
				Setting_PSNL_NO = " WHERE  0=1 ";
			} else {
				Setting_PSNL_NO = "  WHERE  PSNL_NO IS NOT NULL AND PSNL_NO IN ('" + PNPS_PSNL_NO + "')";
			}


			SQL = "";
			SQL += "   SELECT ";
			SQL += "   POS_No PSNL_NO,";
			SQL += "   Ass_Min_Score Year_Score,";
			SQL += "   Leave_Score LEAVE_All,";
			SQL += "   Punishment History_Status_PUNISH,";
			SQL += "   CASE WHEN Punishment=1 THEN 'ไม่เคยรับโทษ' ELSE 'เคยรับโทษ' END History_PUNISH ";
			SQL += "   FROM tb_Path_Setting ";
			SQL += "   " + Setting_POS_NO + "\n";
			SqlDataAdapter DA_Check = new SqlDataAdapter(SQL, BL.ConnectionString());
			DataTable DT_Check = new DataTable();
			DA_Check.Fill(DT_Check);

            if (DT_Check.Rows.Count == 1) {
					//---------Check-----------
					if (GL.IsEqualNull(DT_Check.Rows[0]["Year_Score"])) {
						Check_Ass_MIN_Score = GL.StringFormatNumber(0);
					} else {
                        Check_Ass_MIN_Score = DT_Check.Rows[0]["Year_Score"].ToString();
					}
					if (GL.IsEqualNull(DT_Check.Rows[0]["LEAVE_All"])) {
						Check_Leave = GL.StringFormatNumber(0);
					} else {
                        Check_Leave = DT_Check.Rows[0]["LEAVE_All"].ToString();
					}
                    Check_Punishment = DT_Check.Rows[0]["History_Status_PUNISH"].ToString();

			} else {
				Check_Ass_MIN_Score = GL.StringFormatNumber(0);
				Check_Leave = GL.StringFormatNumber(0);
				Check_Punishment = "False";
			}



			SQL = "";
			SQL += "   SELECT  'Property' Property,  POS_No PSNL_NO, ' ' PSNL_Fullname ,  Ass_Min_Score Year_Score,   Leave_Score LEAVE_All,   Punishment History_Status_PUNISH, \n";
			SQL += "   CASE WHEN Punishment=1 THEN 'ไม่เคยรับโทษ' ELSE 'เคยรับโทษ' END History_PUNISH \n";
			SQL += "   FROM tb_Path_Setting \n";
			SQL += "   " + Setting_POS_NO + "\n";
			SQL += "   UNION ";
			SQL += "   SELECT 'Compare'  Property,PSNL_NO,PSNL_Fullname   ,max(Year_Score) Year_Score   ,max(LEAVE_All) LEAVE_All   ,History_Status_PUNISH   ,History_PUNISH   ";
			SQL += "   FROM vw_Path_RPT_Yearly_Result ";
			SQL += "   " + Setting_PSNL_NO + "\n";
			SQL += "   group by PSNL_NO,PSNL_Fullname,History_Status_PUNISH,History_PUNISH";
			SQL += "   ORDER BY Property";

			SqlDataAdapter DA = new SqlDataAdapter(SQL, BL.ConnectionString());
			DataTable DT = new DataTable();
            DA.SelectCommand.CommandTimeout = 1000;
			DA.Fill(DT);

			if (DT.Rows.Count > 0) {
				//-------1---------------------------
                if (GL.CDBL(Check_Ass_MIN_Score) > 0)
                {
					lblAss_MIN_Score_Property.Text = "<br>" + " | เกณฑ์ " + GL.StringFormatNumber(GL.CDBL (Check_Ass_MIN_Score))  + " คะแนน | ";
				} else {
					lblAss_MIN_Score_Property.Text = "<br>" + " | ยังไม่ระบุเกณฑ์ | ";
				}

				if (Information.IsNumeric(DT.Rows[0]["Year_Score"])) {
					lblAss_MIN_Score.Text = "  ผลการประเมินเท่ากับ " + GL.StringFormatNumber(GL.CDBL(DT.Rows[0]["Year_Score"])) + " คะแนน ";
				} else {
                    lblAss_MIN_Score.Text = "  ผลการประเมินเท่ากับ 0.00 คะแนน";
				}

                if (GL.CDBL(Check_Ass_MIN_Score) <= GL.CDBL((DT.Rows[0]["Year_Score"])))
                {
					icon_Ass_KPI.Style["color"] = "Green";
					lblAss_MIN_Score.Style["color"] = "Green";
				} else {
					icon_Ass_KPI.Style["color"] = "Red";
					lblAss_MIN_Score.Style["color"] = "Red";
				}

				//-------2---------------------------
                if (GL.CDBL(Check_Leave) > 0)
                {
					lblLeave_Property.Text = "<br>" + " | เกณฑ์ " + GL.StringFormatNumber(GL.CDBL(Check_Leave)) + " วัน  " + " | ";
				} else {
					lblLeave_Property.Text = "<br>" + " | ยังไม่ระบุเกณฑ์ " + " | ";
				}



				if (Information.IsNumeric(DT.Rows[0]["LEAVE_All"])) {
					lblLeave.Text = " จำนวนวันรวม " + GL.StringFormatNumber(GL.CDBL(DT.Rows[0]["LEAVE_All"])) + " วัน";
				} else {
					lblLeave.Text = " ไม่มีการลา ";
				}

                if (GL.CDBL(Check_Leave) >= GL.CINT(DT.Rows[0]["LEAVE_All"]))
                {
					icon_Leave.Style["color"] = "Green";
					lblLeave.Style["color"] = "Green";
                }
                else if (GL.CDBL(Check_Leave) == Convert.ToInt32(0))
                {
					icon_Leave.Style["color"] = "Green";
					lblLeave.Style["color"] = "Green";
					lblLeave.Style["color"] = "Green";
				} else {
					icon_Leave.Style["color"] = "Red";
					lblLeave.Style["color"] = "Red";
				}
				//-------5---PUNISH------------------------
				switch ((DT.Rows[0]["History_PUNISH"]).ToString ()) {
					case "":
						lblPunishment.Text = "  ";
						break;
					case "ไม่เคยรับโทษ":
						lblPunishment.Text = " ไม่เคยรับโทษ ";
						lblPunishment.ForeColor = System.Drawing.Color.Green;
						break;
					case "เคยรับโทษ":
						lblPunishment.Text = " เคยรับโทษ ";
						lblPunishment.ForeColor = System.Drawing.Color.Red;
						break;
				}

				if (Check_Punishment == "true") {
					if ((bool )DT.Rows[0]["History_Status_PUNISH"] == false) {
						icon_Pushnish.Style["color"] = "Red";
					} else {
						icon_Pushnish.Style["color"] = "Green";
					}
				} else {
					icon_Pushnish.Style["color"] = "Green";
				}

			} else {
			}

			return DT;
		}

		//-----------Course----------------
		public DataTable BindCourse_Property(string POS_NO, string PNPS_PSNL_NO)
		{
			string Setting_POS_NO = "";
			if (string.IsNullOrEmpty(POS_NO)) {
				Setting_POS_NO = " WHERE  0=1 ";
			} else {
				Setting_POS_NO = "  WHERE Training.POS_NO ='" + POS_NO + "'";
			}

			string SQL = "";
			SQL += "   SELECT Training.POS_No,COURSE.COURSE_ID,COURSE.COURSE_DESC , '2' STSTUS_Course \n";
			SQL += "   FROM tb_Path_Setting_Training Training\n";
			SQL += "   LEFT JOIN tb_PK_COURSE COURSE ON Training.COURSE_ID=COURSE.COURSE_ID\n";
			SQL += "   " + Setting_POS_NO + "\n";
			SQL += "   ORDER BY Training.POS_No,COURSE.COURSE_ID,COURSE.COURSE_DESC\n";
			SqlDataAdapter DA = new SqlDataAdapter(SQL, BL.ConnectionString());
			DataTable DT = new DataTable();
			DA.Fill(DT);

            Check_Course = DT.Rows.Count.ToString();
			return DT;

		}

		public DataTable BindCourse_Compare(string POS_NO, string PNPS_PSNL_NO)
		{
			string Setting_POS_NO = "";
			if (string.IsNullOrEmpty(POS_NO)) {
				Setting_POS_NO = " WHERE  0=1 ";
			} else {
				Setting_POS_NO = "  WHERE tb_Path_Setting_Training.POS_No='" + POS_NO + "'";
			}

			string Setting_PSNL_NO = "";
			if (string.IsNullOrEmpty(PNPS_PSNL_NO)) {
				Setting_PSNL_NO = " WHERE  0=1 ";
			} else {
				Setting_PSNL_NO = "  WHERE tb_PK_EMP_SELECT.PNPS_PSNL_NO='" + PNPS_PSNL_NO + "'";
			}

			string SQL = "";
			SQL += " SELECT \n";
			SQL += " tb_Path_Setting_Training.POS_No \n";
			SQL += " ,tb_Path_Setting_Training.Course_ID\n";
			SQL += " , Course.COURSE_DESC\n";
			SQL += " ,TB.PNPS_PSNL_NO\n";
			SQL += " ,CASE WHEN TB.PNPS_PSNL_NO IS NOT NULL  THEN 1 ELSE 0 END STSTUS_Course\n";
			SQL += " FROM tb_Path_Setting_Training LEFT JOIN \n";
			SQL += " \n";
			SQL += " (\n";
			SQL += " SELECT \n";
			SQL += " Training.POS_No \n";
			SQL += " ,Training.Course_ID \n";
			SQL += " ,tb_PK_EMP_SELECT.PNPS_PSNL_NO\n";
			SQL += " ,CASE WHEN Training.Course_ID=tb_PK_EMP_SELECT.COURSE_ID THEN 1 ELSE 0 END STSTUS_Course\n";
			SQL += " \n";
			SQL += " FROM tb_PK_EMP_SELECT \n";
			SQL += " LEFT  JOIN tb_Path_Setting_Training Training  ON Training.Course_ID=tb_PK_EMP_SELECT.COURSE_ID\n";
			SQL += " \n";
			SQL += " " + Setting_PSNL_NO + "\n";
			SQL += " Group BY \n";
			SQL += " Training.POS_No \n";
			SQL += " ,Training.Course_ID \n";
			SQL += " ,tb_PK_EMP_SELECT.PNPS_PSNL_NO\n";
			SQL += " ,tb_PK_EMP_SELECT.COURSE_ID\n";
			SQL += " ) AS TB\n";
			SQL += " ON tb.Course_ID=tb_Path_Setting_Training.Course_ID\n";
			SQL += " Left JOIN  tb_PK_COURSE Course ON tb_Path_Setting_Training.Course_ID=Course.COURSE_ID \n";
			SQL += " \n";
			SQL += " " + Setting_POS_NO + "\n";
			SQL += " GROUP BY tb_Path_Setting_Training.POS_No,tb_Path_Setting_Training.Course_ID,TB.PNPS_PSNL_NO,TB.STSTUS_Course, Course.COURSE_DESC\n";
			SQL += " ";

			SqlDataAdapter DA = new SqlDataAdapter(SQL, BL.ConnectionString());
			DataTable DT = new DataTable();
			DA.Fill(DT);
			return DT;

		}

		protected void rptDetail_Course_ItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
		{
			if (e.Item.ItemType != ListItemType.AlternatingItem & e.Item.ItemType != ListItemType.Item)
				return;

            DataRowView drv = (DataRowView)e.Item.DataItem;

            Label lblCourse = (Label)e.Item.FindControl("lblCourse");
            Label lblCourse_Status = (Label)e.Item.FindControl("lblCourse_Status");


			lblCourse.Text = drv["COURSE_DESC"].ToString();
			if (GL.CINT (drv["STSTUS_Course"]) == Convert.ToInt32 (0)) {
				lblCourse_Status.Text = "(ไม่เคยเข้าอบรม)";
				lblCourse_Status.Style["color"] = "Red";
            }
            else if (GL.CINT(drv["STSTUS_Course"]) == Convert.ToInt32(1))
            {
				lblCourse_Status.Text = "(ผ่านการฝึกอบรม)";
				lblCourse_Status.Style["color"] = "Green";
			} else {
				lblCourse_Status.Text = "";
			}
		}

		//-----------Test----------------
		//-----------Property----------------
		public DataTable BindTest_Property(string POS_NO, string PNPS_PSNL_NO)
		{
			string Setting_POS_NO = "";
			if (string.IsNullOrEmpty(POS_NO)) {
				Setting_POS_NO = " WHERE  0=1 ";
			} else {
				Setting_POS_NO = "  WHERE POS_NO ='" + POS_NO + "'";
			}

			string SQL = "";
			SQL += " SELECT Setting_Test.POS_No,Test.CPSM_SUBJECT_CODE,Test.CPSM_SUBJECT_NAME,Setting_Test.Min_Score, '2' Pass_Score\n";
			SQL += " FROM tb_Path_Setting_Test Setting_Test\n";
			SQL += " LEFT JOIN tb_CP_SUBJECT_MASTER Test ON Setting_Test.SUBJECT_CODE=Test.CPSM_SUBJECT_CODE\n";
			SQL += " " + Setting_POS_NO + "\n";
			SQL += " ORDER BY Setting_Test.POS_No,Test.CPSM_SUBJECT_CODE,Test.CPSM_SUBJECT_NAME\n";
			SqlDataAdapter DA = new SqlDataAdapter(SQL, BL.ConnectionString());
			DataTable DT = new DataTable();
			DA.Fill(DT);
            Check_Test = DT.Rows.Count.ToString();
			return DT;
		}
		//-----------Compare----------------
		public DataTable BindTest_Compare(string POS_NO, string PNPS_PSNL_NO)
		{
			string Setting_POS_NO = "";
			if (string.IsNullOrEmpty(POS_NO)) {
				Setting_POS_NO = " WHERE  0=1 ";
			} else {
				Setting_POS_NO = " WHERE  tb_Path_Setting_Test.POS_No='" + POS_NO + "'";
			}

			string Setting_PSNL_NO = "";
			if (string.IsNullOrEmpty(PNPS_PSNL_NO)) {
				Setting_PSNL_NO = " WHERE  0=1 ";
			} else {
				Setting_PSNL_NO = " WHERE tb_CP_SUBJECT_PERSONAL.CPSP_PERSONAL_NO='" + PNPS_PSNL_NO + "'";
			}

			string SQL = "";
			SQL += " SELECT  \n";
			SQL += " --*  \n";
			SQL += " tb_Path_Setting_Test.POS_No \n";
			SQL += " ,tb_Path_Setting_Test.SUBJECT_CODE \n";
			SQL += " , SUBJECT_MASTER.CPSM_SUBJECT_NAME \n";
			SQL += " ,isnull(TB.CPSP_POINT,0) CPSP_POINT \n";
			SQL += " ,CASE WHEN ISNULL(TB.CPSP_POINT,0) >= ISNULL(tb_Path_Setting_Test.Min_Score,0) THEN 1 ELSE 0 END Pass_Score \n";
			SQL += " ,TB.CPSP_PERSONAL_NO \n";
			SQL += " ,CASE WHEN TB.CPSP_PERSONAL_NO IS NOT NULL  THEN 1 ELSE 0 END STSTUS_Test \n";
			SQL += " FROM tb_Path_Setting_Test LEFT JOIN  \n";
			SQL += "  \n";
			SQL += " ( \n";
			SQL += " SELECT  \n";
			SQL += " Test.POS_No \n";
			SQL += " ,Test.SUBJECT_CODE \n";
			SQL += " ,isnull(tb_CP_SUBJECT_PERSONAL.CPSP_POINT,0) CPSP_POINT \n";
			SQL += " ,tb_CP_SUBJECT_PERSONAL.CPSP_PERSONAL_NO \n";
			SQL += " ,CASE WHEN Test.SUBJECT_CODE=tb_CP_SUBJECT_PERSONAL.CPSP_SUBJECT_CODE THEN 1 ELSE 0 END STSTUS_Course \n";
			SQL += "  \n";
			SQL += " FROM tb_CP_SUBJECT_PERSONAL \n";
			SQL += " LEFT  JOIN tb_Path_Setting_Test Test  ON Test.SUBJECT_CODE=tb_CP_SUBJECT_PERSONAL.CPSP_SUBJECT_CODE \n";
			SQL += "  \n";
			SQL += " " + Setting_PSNL_NO + "\n";
			SQL += " GROUP BY  \n";
			SQL += " Test.POS_No \n";
			SQL += " ,Test.SUBJECT_CODE  \n";
			SQL += " ,tb_CP_SUBJECT_PERSONAL.CPSP_POINT \n";
			SQL += " ,tb_CP_SUBJECT_PERSONAL.CPSP_PERSONAL_NO \n";
			SQL += " ,tb_CP_SUBJECT_PERSONAL.CPSP_SUBJECT_CODE \n";
			SQL += " ) AS TB \n";
			SQL += " ON tb.SUBJECT_CODE=tb_Path_Setting_Test.SUBJECT_CODE \n";
			SQL += " Left JOIN  tb_CP_SUBJECT_MASTER SUBJECT_MASTER ON tb_Path_Setting_Test.SUBJECT_CODE=SUBJECT_MASTER.CPSM_SUBJECT_CODE  \n";
			SQL += "  \n";
			SQL += " " + Setting_POS_NO + "\n";
			SQL += "  \n";
			SQL += " GROUP BY  \n";
			SQL += " tb_Path_Setting_Test.POS_No \n";
			SQL += " ,tb_Path_Setting_Test.SUBJECT_CODE \n";
			SQL += " , SUBJECT_MASTER.CPSM_SUBJECT_NAME \n";
			SQL += " ,TB.CPSP_PERSONAL_NO \n";
			SQL += " ,tb_Path_Setting_Test.Min_Score \n";
			SQL += " ,TB.CPSP_POINT \n";

			SqlDataAdapter DA = new SqlDataAdapter(SQL, BL.ConnectionString());
			DataTable DT = new DataTable();
			DA.Fill(DT);
			return DT;

		}

		protected void rptDetail_Test_ItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
		{
			if (e.Item.ItemType != ListItemType.AlternatingItem & e.Item.ItemType != ListItemType.Item)
				return;
            DataRowView drv = (DataRowView)e.Item.DataItem;
            Label lblTest = (Label)e.Item.FindControl("lblTest");
			lblTest.Text = drv["CPSM_SUBJECT_NAME"].ToString();


            Label lblMin_Score = (Label)e.Item.FindControl("lblMin_Score");
            if (GL.CDBL(drv["Pass_Score"]) == Convert.ToInt32(0))
            {
                lblMin_Score.Text = " (ไม่ผ่าน)";
                lblMin_Score.Style["color"] = "Red";
            }
            else if (GL.CDBL(drv["Pass_Score"]) == Convert.ToInt32(1))
            {
                lblMin_Score.Text = " (ผ่าน)";
                lblMin_Score.Style["color"] = "Green";
            }
            else
            {
                lblMin_Score.Text = "";
                //pnl_Test.Visible = False
            }



		}



		protected void btnDeleteModelCompare_Close_ServerClick(object sender, System.EventArgs e)
		{
			ModelCompareDetail.Visible = false;
		}


		protected void btnSetPath_Click(object sender, System.EventArgs e)
		{

			//-------------ตรวจสอบตำแหน่งถัดไป สามารถเข้ารับตำแหน่งก่อนหน้าหรือปัจจุบันได้หรือไม่---------------
			string SQL = "";
			SQL += "   SELECT * FROM tb_Path_Setting_Self ";
			SQL += "   WHERE PSNL_NO='" + Session["USER_PSNL_NO"].ToString().Replace("'", "''") + "'\n";
			SQL += "   AND Goal_NO=" + Goal_NO + "\n";
			SQL += "   Order By PSNL_NO,Goal_NO";
			SqlDataAdapter DA = new SqlDataAdapter(SQL, BL.ConnectionString());
			DataTable DT = new DataTable();
			DA.Fill(DT);
			if (DT.Rows.Count == 1) {
				SQL = "";
				SQL += "   DELETE FROM tb_Path_Setting_Self ";
				SQL += "   WHERE PSNL_NO='" + Session["USER_PSNL_NO"].ToString().Replace("'", "''") + "'\n";
				SQL += "   AND Goal_NO > " + Goal_NO + "\n";
				SqlDataAdapter DA_Del = new SqlDataAdapter(SQL, BL.ConnectionString());
				DataTable DT_Del = new DataTable();
				DA_Del.Fill(DT_Del);
			} else {
				SQL = "";
				SQL += "  SELECT * \n";
				SQL += "  FROM  tb_Path_Setting_Self \n";
				SQL += "  WHERE PSNL_NO ='" + Session["USER_PSNL_NO"].ToString().Replace("'", "''") + "'\n";
				SQL += "  AND POS_No='" + lblTMP_POS_SetPath.Text + "'";
				DA = new SqlDataAdapter(SQL, BL.ConnectionString());
				DT = new DataTable();
				DA.Fill(DT);
			}


			DataRow DR = null;
            if (DT.Rows.Count == Convert.ToInt32(0))
            {
				DR = DT.NewRow();
				DR["PSNL_NO"] = Session["USER_PSNL_NO"].ToString().Replace("'", "''");
				DR["Goal_NO"] = Goal_NO;
			} else {
				DR = DT.Rows[0];
			}
			DR["POS_No"] = lblTMP_POS_SetPath.Text;
			DR["MGR_NAME"] = lblTMP_POS_SetPath.Attributes["MGR_NAME"];
			DR["PNPS_CLASS"] = lblTMP_POS_SetPath.Attributes["PNPO_CLASS"];
			DR["DEPT_NAME"] = lblTMP_POS_SetPath.Attributes["DEPT_NAME"];
			//DR("Update_By") = Session("")
			DR["Update_Time"] = DateAndTime.Now;
            if (DT.Rows.Count == Convert.ToInt32(0))
				DT.Rows.Add(DR);
			SqlCommandBuilder cmd = new SqlCommandBuilder();
			cmd = new SqlCommandBuilder(DA);
			DA.Update(DT);
			BindPath_Position();
			ModalPost.Visible = false;
			ModelCompareDetail.Visible = false;
			lblTMP_POS_SetPath.Text = "";

		}



		protected void txtSearchPos_TextChanged(object sender, System.EventArgs e)
		{
			BindPostList();
		}







		//--------------------แสดงเกณฑ์ ผ่าน ไม่ผ่าน ในรายละเอียดเพิ่มเติม---------------------------
		public DataTable Bind_rpt_Ass_Leave_Push(string POS_NO, string PNPS_PSNL_NO)
		{
			string SQL = "";
			string Setting_POS_NO = "";
			if (string.IsNullOrEmpty(POS_NO)) {
				Setting_POS_NO = " WHERE  0=1 ";
			} else {
				Setting_POS_NO = "  WHERE POS_No ='" + POS_NO + "'";
			}

			string Setting_PSNL_NO = "";
			if (string.IsNullOrEmpty(PNPS_PSNL_NO)) {
				Setting_PSNL_NO = " WHERE  0=1 ";
			} else {
				Setting_PSNL_NO = "  WHERE  PSNL_NO IS NOT NULL AND PSNL_NO IN ('" + PNPS_PSNL_NO + "')";
			}

			SQL += "   SELECT  'Property' Property,  POS_No PSNL_NO, ' ' PSNL_Fullname ,  Ass_Min_Score Year_Score,   Leave_Score LEAVE_All,   Punishment History_Status_PUNISH, \n";
			SQL += "   CASE WHEN Punishment=1 THEN 'ไม่เคยรับโทษ' ELSE 'เคยรับโทษ' END History_PUNISH \n";
			SQL += "   FROM tb_Path_Setting \n";
			SQL += "   " + Setting_POS_NO + "\n";
			SQL += "   UNION ";
			SQL += "   SELECT 'Compare'  Property,PSNL_NO,PSNL_Fullname   ,max(Year_Score) Year_Score   ,max(LEAVE_All) LEAVE_All   ,History_Status_PUNISH   ,History_PUNISH   ";
			SQL += "   FROM vw_Path_RPT_Yearly_Result ";
			SQL += "   " + Setting_PSNL_NO + "\n";
			SQL += "   group by PSNL_NO,PSNL_Fullname,History_Status_PUNISH,History_PUNISH";
			SQL += "   ORDER BY Property";

			SqlDataAdapter DA = new SqlDataAdapter(SQL, BL.ConnectionString());
			DataTable DT = new DataTable();
			DA.Fill(DT);

			return DT;
		}
		public CP_Setting_CareerPath_Self()
		{
			Load += Page_Load;
		}



	}
}

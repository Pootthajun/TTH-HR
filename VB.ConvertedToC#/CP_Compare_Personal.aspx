﻿<%@ Page  Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPage.Master" CodeBehind="CP_Compare_Personal.aspx.cs" Inherits="VB.CP_Compare_Personal" %>

<%@ Register src="WUC_DateReporter.ascx" tagname="WUC_DateReporter" tagprefix="uc1" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<%@ Register src="PageNavigation.ascx" tagname="PageNavigation" tagprefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <link href="assets/css/pages/pricing-tables.css" rel="stylesheet" type="text/css">
    <link href="assets/plugins/glyphicons/css/glyphicons.css" rel="stylesheet">

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:ToolkitScriptManager ID="toolkit1" runat="server"></asp:ToolkitScriptManager>
<asp:UpdatePanel ID="UpdatePanel1" runat="server">
<ContentTemplate>
<div class="container-fluid">
                <!-- BEGIN PAGE HEADER-->
                <div class="row-fluid" id="fixedPageHeader">
					<div class="span12"> 
						<!-- BEGIN PAGE TITLE & BREADCRUMB-->
						<h3>เปรียบเทียบพนักงานเข้าสู่ตำแหน่งว่าง</h3>					
						
						<ul class="breadcrumb">
                            <li>
                            	<i class="icon-user"></i> <a href="javascript:;">เส้นทางก้าวหน้าในสายอาชีพ</a><i class="icon-angle-right"></i>
                            </li>
                            <li>
                        	    <i class="icon-group"></i> <a href="javascript:;">ข้อมูลพนักงาน</a><i class="icon-angle-right"></i>
                        	</li> 
                        	<li>
                        	    <i class="icon-columns"></i> <a href="javascript:;">เปรียบเทียบพนักงานเข้าสู่ตำแหน่งว่าง</a>
                        	</li>                        	
                        	
                            <uc1:WUC_DateReporter ID="WUC_DateReporter1" runat="server" />
                        </ul>
						<!-- END PAGE TITLE & BREADCRUMB-->
									        
				     </div>			
			    </div>
		        <!-- END PAGE HEADER-->
		        <asp:Panel ID="PnlList" runat="server" > 
                <asp:Label ID="lblPos" style=" display :none ;" runat="server" ></asp:Label>
						<!-- BEGIN SAMPLE TABLE PORTLET-->	
							<h3><asp:Label ID="lblHeader_POS" runat="server" ></asp:Label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:Label ID="lblHeader_POS_Class"  runat="server" ></asp:Label></h3>
							        
							
							<%--<div class="portlet-body">--%>
                                <div class="row-fluid" class="flow_container" style=" font-size:16px; font-weight:bold; padding-bottom:20px; padding-top:20px;  ">
                                <table style =" width :auto ; color :Black; "  border="4"    class="table table-bordered datatable ">
                                        <tr style =" height :70px; vertical-align :middle ;  border-color :Blue ; ">
                                            <asp:Repeater ID="rpttd_Header" OnItemCommand="rpttd_Header_ItemCommand" OnItemDataBound="rpttd_Header_ItemDataBound"  runat="server">
									            <ItemTemplate>
											        <td id="cel_Header" runat ="server"    style =" vertical-align :middle ; width:300px; cursor:pointer; padding:0px !important; height:10px !important; border-right:0px none;">
												        <asp:Panel id="pnl_icon_user" class="pricing-head" runat ="server"  ><i class="icon-user"></i></asp:Panel>
                                                        <asp:Label ID="lblHeader"  runat="server" ></asp:Label>&nbsp;&nbsp;
                                                        <asp:Label ID="lblHeader_Detail"  style=" font-size:12px;"  runat="server" ></asp:Label>
                                                        <asp:LinkButton ID="btnAdd_POS_NO" runat ="server" style =" top :-9px;"   class=" glyphicons  no-js suitcase " CommandName ="lnk_add_POS_NO"><i></i></asp:LinkButton>
                                                        <asp:LinkButton ID="btnAdd_PSNL_NO" runat ="server" style =" top :-9px;"   class="glyphicons no-js parents " CommandName ="lnk_add_PSNL_NO"><i></i></asp:LinkButton>
											            <asp:LinkButton ID="btnEDIT_PSNL_NO" runat ="server" style ="  top :-9px;"   class="  glyphicons no-js edit  " CommandName ="lnk_EDIT_PSNL_NO"><i></i></asp:LinkButton>
                                                    
                                                    </td>
                                                </ItemTemplate> 
                                            </asp:Repeater>

										</tr>
                                       
										<tr align ="center" >
                                            <asp:Repeater ID="rpttd_Ass_MIN_Score" OnItemDataBound="rpttd_Ass_MIN_Score_ItemDataBound" runat="server">
									            <ItemTemplate>
                                                    
											        <td id="cel_KPI" runat ="server" align ="center" style ="  width: 300px; vertical-align :middle ; "  >
                                                        <asp:Panel ID="pnl_Ass_MIN_Score" runat ="server"  >
                                                            <ul   class=" pricing-content_Property unstyled" style=" border-bottom :0px; color  :Black ; ">
                                                                  <li>
                                                                        <asp:Panel ID="icon_Ass_Black" runat ="server" CssClass =" inline "><i id="i2" runat ="server"  class="icon-check" style =" color :black ;"></i></asp:Panel>
                                                                        <asp:Panel ID="icon_Ass_green" runat ="server" CssClass =" inline "><i id="icon_Ass_KPI" runat ="server"  class="icon-check" style =" color :Green ;"></i></asp:Panel>
                                                                        <asp:Panel ID="icon_Ass_Red" runat ="server" CssClass =" inline " ><i id="i1" runat ="server"  class="icon-check" style =" color :Red ;"></i></asp:Panel> 
                                                                         ผลการประเมิน KPI/COMP ย้อนหลัง 3 รอบ <b><asp:Label ID="lblAss_Min_Score" ForeColor ="Black"  runat="server" Text ="XXX"></asp:Label></b>
                                                                        
                                                                    </li>          
                                                                            <%--<li><i class="icon-check" style =" color :Black;" ></i> ผลการประเมิน KPI+Competency ย้อนหลัง 3 รอบ <b><asp:Label ID="lblAss_MIN_Score" runat="server" Text ="XXX"></asp:Label></b></li>--%>
											                </ul>
                                                            <asp:Image id="img_Check" runat ="server"   src="images/CheckBlue.png" style=" height:25px; vertical-align :middle ;"/>
                                                         </asp:Panel>
                                                      
                                                    </td>
                                                </ItemTemplate> 
                                            </asp:Repeater>
										</tr>
                                        <tr>
                                            <asp:Repeater ID="rpttd_Leave" OnItemDataBound="rpttd_Leave_ItemDataBound" runat="server">
									            <ItemTemplate>
											        <td id="cel_Leave" runat ="server"  style =" width: 300px; vertical-align :middle ;">
												        <asp:Panel ID="pnl_Leave" runat ="server"  >
                                                            <ul class="pricing-content_Property unstyled" style=" border-bottom :0px;">
                                                                <li>
                                                                    <asp:Panel ID="icon_Leave_Black" runat ="server" CssClass =" inline "><i id="i2" runat ="server"  class="icon-ambulance" style =" color :black ;"></i></asp:Panel>
                                                                    <asp:Panel ID="icon_Leave_green" runat ="server" CssClass =" inline "><i id="icon_Ass_KPI" runat ="server"  class="icon-ambulance" style =" color :Green ;"></i></asp:Panel>
                                                                    <asp:Panel ID="icon_Leave_Red" runat ="server" CssClass =" inline " ><i id="i1" runat ="server"  class="icon-ambulance" style =" color :Red ;"></i></asp:Panel> 
                                                                    มีจำนวนวันลา(ป่วย/กิจ) 3 ปีงบประมาณย้อนหลัง รวม <b><asp:Label ID="lblLeave"  ForeColor ="Black"  runat="server" Text ="XXX"></asp:Label></b>
                                                                </li>
											                </ul> 
                                                        </asp:Panel>
                                                        <asp:Image id="img_Check" runat ="server"  src="images/CheckBlue.png" style="height:25px; vertical-align :middle ;"/>
                                                    </td>
                                                </ItemTemplate> 
                                            </asp:Repeater>
										</tr>
										<tr>
                                            <asp:Repeater ID="rpttd_Course" OnItemDataBound="rpttd_Course_ItemDataBound" runat="server">
									            <ItemTemplate>
											        <td id="cel_Course" runat ="server"  style =" width: 300px">
                                                        <asp:Panel ID="pnl_Course" runat ="server"  >
                                                            <ul class="pricing-content_Property unstyled">
                                                            <li>
                                                                    <asp:Panel ID="icon_Course_Black" runat ="server" CssClass =" inline "><i id="i2" runat ="server"  class="icon-book" style =" color :black ;"></i></asp:Panel>
                                                                    <asp:Panel ID="icon_Course_green" runat ="server" CssClass =" inline "><i id="icon_Ass_KPI" runat ="server"  class="icon-book" style =" color :Green ;"></i></asp:Panel>
                                                                    <asp:Panel ID="icon_Course_Red" runat ="server" CssClass =" inline " ><i id="i1" runat ="server"  class="icon-book" style =" color :Red ;"></i></asp:Panel> 
                                                                                                                                 
												                การฝึกอบรม <b><asp:Label ID="lblCount_Course"  ForeColor ="Black"  runat="server" Text ="XXX"></asp:Label></b> หลักสูตร
												                <ul>
                                                                    <asp:Repeater ID="rptDetail_Course" runat="server">
								                                        <ItemTemplate>	
												                            <li><asp:Label ID="lblCourse"  ForeColor ="Black"  runat="server" Text ="XXX"></asp:Label><asp:Label ID="lblCourse_Status" runat="server" Text ="XXX"></asp:Label></li>
												                        </ItemTemplate> 
                                                                    </asp:Repeater> 
												                </ul>    
												            </li>
                                                            </ul>
                                                        </asp:Panel>
                                                        <asp:Image id="img_Check" runat ="server"  src="images/CheckBlue.png" style="height:25px; vertical-align :middle ;"/>
											        </td>
                                                </ItemTemplate> 
                                            </asp:Repeater>
										</tr>
										<tr>
                                            <asp:Repeater ID="rpttd_Test" OnItemDataBound="rpttd_Test_ItemDataBound" runat="server">
									            <ItemTemplate>
											        <td  id="cel_Test" runat ="server"  style =" width: 300px">
												        <asp:Panel ID="pnl_Test" runat ="server"  >
                                                            <ul class="pricing-content_Property unstyled">
                                                            <li>
                                                                    <asp:Panel ID="icon_Test_Black" runat ="server" CssClass =" inline "><i id="i2" runat ="server"  class="icon-thumbs-up" style =" color :black ;"></i></asp:Panel>
                                                                    <asp:Panel ID="icon_Test_green" runat ="server" CssClass =" inline "><i id="icon_Ass_KPI" runat ="server"  class="icon-thumbs-up" style =" color :Green ;"></i></asp:Panel>
                                                                    <asp:Panel ID="icon_Test_Red" runat ="server" CssClass =" inline " ><i id="i1" runat ="server"  class="icon-thumbs-up" style =" color :Red ;"></i></asp:Panel> 
                                                                      
												                การสอบวัดผล <b><asp:Label ID="lblCount_Test"  ForeColor ="Black"  runat="server" Text ="XXX"></asp:Label></b> วิชา
												                <ul>
                                                                    <asp:Repeater ID="rptDetail_Test" runat="server">
								                                        <ItemTemplate>	
												                            <li><asp:Label ID="lblTest"  ForeColor ="Black"  runat="server" Text ="XXX"></asp:Label><i class =" icon ipod"></i><asp:Label ID="lblMin_Score" runat="server" Text ="XXX"></asp:Label> </li>
												                        </ItemTemplate> 
                                                                    </asp:Repeater> 
												                </ul>    
												            </li>
                                                            </ul> 
                                                        </asp:Panel>
                                                        <asp:Image id="img_Check" runat ="server"  src="images/CheckBlue.png" style="height:25px; vertical-align :middle ;"/>
											        </td>
                                                </ItemTemplate> 
                                            </asp:Repeater>
										</tr>
										<tr>
                                            <asp:Repeater ID="rpttd_Punishment" OnItemDataBound="rpttd_Punishment_ItemDataBound" runat="server">
									            <ItemTemplate>
											        <td id="cel_Punishment" runat ="server"  style =" width: 300px">
												        <asp:Panel ID="pnl_Punishment" runat ="server"  >
                                                            <ul class="pricing-content_Property unstyled">
                                                                <li>
                                                                    <asp:Panel ID="icon_Punis_Black" runat ="server" CssClass =" inline "><i id="i2" runat ="server"  class="icon-legal" style =" color :black ;"></i></asp:Panel>
                                                                    <asp:Panel ID="icon_Punis_green" runat ="server" CssClass =" inline "><i id="icon_Ass_KPI" runat ="server"  class="icon-legal" style =" color :Green ;"></i></asp:Panel>
                                                                    <asp:Panel ID="icon_Punis_Red" runat ="server" CssClass =" inline " ><i id="i1" runat ="server"  class="icon-legal" style =" color :Red ;"></i></asp:Panel> 
                                                                      
                                                                    <asp:Label ID="lblPunishment" runat="server" Text ="โทษทางวินัย"></asp:Label>											
												                </li>												
											                </ul>
                                                        </asp:Panel>
                                                        <asp:Image id="img_Check" runat ="server"  src="images/CheckBlue.png" style="height:25px; vertical-align :middle ;"/>
											        </td>
                                                </ItemTemplate> 
                                            </asp:Repeater>
										</tr>
										<tr>
                                            <asp:Repeater ID="rpttd_History" runat="server">
									            <ItemTemplate>
											        <td style =" width: 300px">
											            <div class="pricing-footer">
												            <div class="span12" style="background-color:#eeeeee; padding-top:3px; padding-bottom:3px; margin-bottom:0px;">ประวัติการดำรงตำแหน่ง
                                                            <asp:Label ID="lblHistoryCount" runat="server" Text ="XXX"></asp:Label>  ตำแหน่ง</div>
                                                            <asp:Repeater ID="rptHistory_Compare" runat="server" >
								                                <ItemTemplate>
												                    <li style="list-style:none;text-align:Left;"><i class="icon-briefcase"></i> ปี <asp:Label ID="lblHistoryYear" runat="server" Text ="XXX"></asp:Label> : <asp:Label ID="lblPos" runat="server" Text ="XXX"></asp:Label></li>
												                </ItemTemplate> 
                                                            </asp:Repeater> 
											                </div>	
                                                            
											            <div visible="False"  style="text-align:center;" >
											                <asp:Button ID="btnCancel" runat="server" CssClass="btn mini red" CommandName="CancelPosCompare" Text="ยกเลิก" />
											                <a id="btnShowAll" target ="_blank"  runat ="server"  class="btn mini blue">
												                แสดงประวัติทั้งหมด <i class="icon-exclamation-sign"></i>
											                </a>
											            </div>	



											        </td>
                                                </ItemTemplate> 
                                            </asp:Repeater>
										</tr>
										<tr >
                                            <asp:Repeater ID="rpttd_btn" OnItemCommand="rpttd_btn_ItemCommand" OnItemDataBound="rpttd_btn_ItemDataBound" runat="server">
									            <ItemTemplate> 
											        <td id="cel_btn" runat ="server"  style =" width: 300px;"  >
												        <asp:Panel ID="pnl_btn" runat ="server"  >
                                                            <div class="tools"  >

                                                                <asp:Label ID="lblTMP_POS_NO" stype=" display:none;" runat="server"  ></asp:Label>

                                                                <a target ="_blank" ID="lnk_ShowDetail" runat="server" CommandName="ShowDetail" class="btn mini purple" ><i class="icon-edit" ></i> ดูข้อมูลเพิ่มเติม</a>
                                                                <asp:LinkButton  ID="btn_Remove" runat="server" CommandName="Remove" class="btn mini red " ><i class="icon-trash" ></i> ลบ</asp:LinkButton>
                                                                <asp:ConfirmButtonExtender ID="btnDelete_Confirm" runat="server"   ConfirmText="ลบพนักงาน ?"  TargetControlID="btn_Remove"></asp:ConfirmButtonExtender>
									                            
								                            </div>
                                                        </asp:Panel>
											        </td>
                                                </ItemTemplate> 
                                            </asp:Repeater>
										</tr>





									<%--</tbody>--%>
								</table>


							</div>

						<!-- END SAMPLE TABLE PORTLET-->
						
					
               </asp:Panel>
				
			</div>

            <asp:Button ID="btnDisplay_AddPSNL_NO" OnClick="btnDisplay_AddPSNL_NO_Click" runat="server" CssClass="btn" Text="เพิ่มพนักงาน" style=" display :none;" />
            <asp:Panel CssClass="modal" style="top:10%; left :30%; position:fixed; width:1100px;" id="ModalAddPSNL_NO" runat="server" DefaultButton="btnSearch_PSNL_NO"  >
            <div style="padding: 0px; width: auto;  margin-top :0px; margin-bottom :0px; top: 0px; left: 0px;" class="fancybox-skin">
                        <div class="fancybox-outer">
				            <div class="modal-header">										
					            <h3>เลือกพนักงานเพื่อเปรียบเทียบเกณฑ์เข้ารับตำแหน่ง </h3>
					            <asp:Label ID="lblPosCurrent" runat="server" style="display:none;"></asp:Label>
                                <asp:Label ID="lblPosPath" runat="server" style="display:none;"></asp:Label>
                                <asp:Button ID="btnSearch_PSNL_NO" OnClick="btnSearch_PSNL_NO_Click" runat="server" Text="" style="display:none;" />
				            </div>
				            <div class="modal-body">					            
					            <div class="row-fluid form-horizontal">
							                   	<asp:Panel CssClass="row-fluid form-horizontal" ID="Panel6" runat="server" >
												     
												   <div class="row-fluid form-horizontal">
												        <div class="span5 ">
													        <div class="control-group">
													            <label class="control-label" ><i class="icon-sitemap"></i> ฝ่าย</label>
													            <div class="controls">
														            <%--<asp:DropDownList ID="ddlSector" runat="server" AutoPostBack="true" CssClass="medium m-wrap">
				                                                        <asp:ListItem Value="0" Selected="true">เลือกฝ่าย</asp:ListItem>
				                                                        <asp:ListItem Value="12">12 : ขึ้นตรงผู้อำนวยการยาสูบ</asp:ListItem>
				                                                        <asp:ListItem Value="13">13 : ฝ่ายอำนวยการและสื่อสารองค์กร</asp:ListItem>
				                                                       <asp:ListItem Value="16">16 : ฝ่ายบริการกลาง</asp:ListItem>
				                                                        <asp:ListItem Value="17">17 : ฝ่ายตรวจสอบภายใน</asp:ListItem>
				                                                        <asp:ListItem Value="18">18 : ฝ่ายการแพทย์</asp:ListItem>
				                                                        <asp:ListItem Value="19">19 : ขึ้นตรงผู้อำนวยการยาสูบ</asp:ListItem>
				                                                        <asp:ListItem Value="21">21 : ฝ่ายบัญชีและการเงิน</asp:ListItem>
				                                                        <asp:ListItem Value="22">22 : ฝ่ายทรัพยากรบุคคล</asp:ListItem>
				                                                        <asp:ListItem Value="23">23 : ฝ่ายจัดหาและรักษาพัสดุ</asp:ListItem>
				                                                        <asp:ListItem Value="30">30 : อำนวยการด้านปฏิบัติการ</asp:ListItem>
				                                                        <asp:ListItem Value="31">31 : ฝ่ายวิจัยและพัฒนา</asp:ListItem>
				                                                        <asp:ListItem Value="32">32 : ฝ่ายผลิต</asp:ListItem>
				                                                        <asp:ListItem Value="33">33 : ฝ่ายวิศวกรรมและพัฒนา</asp:ListItem>
				                                                        <asp:ListItem Value="41">41 : ฝ่ายตลาด</asp:ListItem>
				                                                        <asp:ListItem Value="42">42 : ฝ่ายยุทธศาสตร์องค์กร</asp:ListItem>
				                                                        <asp:ListItem Value="43">43 : ฝ่ายขาย</asp:ListItem>
				                                                        <asp:ListItem Value="51">51 : ฝ่ายใบยา</asp:ListItem>

			                                                        </asp:DropDownList>--%>
                                                                    <asp:DropDownList ID="ddlSector" OnSelectedIndexChanged="btnSearch_PSNL_NO_Click" runat="server" AutoPostBack="true" CssClass="medium m-wrap">
								                                    </asp:DropDownList>
														        </div>
												            </div>
													    </div>
													     <div class="span6 ">
														    <div class="control-group">
													            <label class="control-label"><i class="icon-sitemap"></i> หน่วยงาน</label>
													            <div class="controls">
														            <asp:TextBox ID="txt_Search_Organize_Add" OnTextChanged="btnSearch_PSNL_NO_Click" runat="server" AutoPostBack="true" CssClass="m-wrap medium" placeholder="ค้นหาจากหน่วยงาน"></asp:TextBox>
														        </div>
												            </div>
													    </div>													    
												    </div>  
												   <div class="row-fluid form-horizontal">
                                                        <div class="span5 ">
														    <div class="control-group">
													            <label class="control-label"><i class="icon-user"></i> ชื่อ</label>
													            <div class="controls">
														            <asp:TextBox ID="txt_Search_Name" OnTextChanged="btnSearch_PSNL_NO_Click" runat="server" AutoPostBack="true" CssClass="m-wrap medium" placeholder="ค้นหาจากเลขประจำตัว/ชื่อพนักงาน"></asp:TextBox>
													            </div>
												            </div>
													    </div>
                                                        <div id="Div1" class="span6 " runat ="server">
														    <div class="control-group">
													            <label class="control-label"><i class="icon-user"></i> ตำแหน่ง</label>
													            <div class="controls">
														            <asp:TextBox ID="txt_Search_POS" OnTextChanged="btnSearch_PSNL_NO_Click" runat="server" AutoPostBack="true" CssClass="m-wrap medium" placeholder="ค้นหารหัส/ชื่อตำแหน่ง"></asp:TextBox>
													            </div>
												            </div>
													    </div>
                                                        <div id="Div10"  class="span6 " runat ="server" visible ="false">
														    <div class="control-group">
													            <label class="control-label"><i class="icon-user"></i> ผลการประเมินย้อนหลัง 3 ปี ไม่น้อยกว่า</label>
													            <div class="controls">
														            <asp:TextBox ID="txtYear_Score" runat="server" AutoPostBack="true" CssClass="m-wrap medium" placeholder="ค้นหาจากคะแนนประเมิน"></asp:TextBox>
													            </div>
												            </div>
													    </div>
                                                    </div>
                                                    <%--                                                    
                                                    <div class="row-fluid form-horizontal"  visible ="false">
                                                        <div class="span5 ">
														    <div class="control-group">
													            <label class="control-label"><i class="icon-user"></i> วันลาย้อนหลังไม่เกิน</label>
													            <div class="controls">
													                <asp:TextBox ID="txtLeave_Day" runat="server" AutoPostBack="true" CssClass="m-wrap medium" placeholder="ค้นหาจากวันลาย้อนหลัง 3 ปี"></asp:TextBox>
                                                                </div>
												            </div>
													    </div>
                                                        <div class="span6 ">
														    <div class="control-group">
													            <label class="control-label"><i class="icon-user"></i> ผ่านการดำรงตำแหน่งไม่น้อยกว่า</label>
													            <div class="controls">
													                <asp:TextBox ID="txtHistory_Pos" runat="server" AutoPostBack="true" CssClass="m-wrap medium" placeholder="ค้นหาจากจำนวนการดำรงตำแหน่ง"></asp:TextBox>
                                                                </div>
												            </div>
													    </div>
                                                    </div>																							   
													<div class="row-fluid form-horizontal"  visible ="false">
                                                        <div class="span5 ">
														    <div class="control-group">
													            <label class="control-label"><i class="icon-user"></i> ผ่านการฝึกอบรมไม่น้อยกว่า</label>
													            <div class="controls">
													                <asp:TextBox ID="txtHistory_Course" runat="server" AutoPostBack="true" CssClass="m-wrap medium" placeholder="ค้นหาจากจำนวนการฝึกอบรม"></asp:TextBox>
                                                                </div>
												            </div>
													    </div>
                                                        <div class="span6 ">
														    <div class="control-group">
													            <label class="control-label"><i class="icon-user"></i> โทษทางวินัย</label>
													            <div class="controls">
														        <asp:DropDownList ID="ddlPUNISH" runat="server" AutoPostBack="true" CssClass="medium m-wrap">
				                                                        <asp:ListItem Value="0" Selected="true">ทั้งหมด</asp:ListItem>
				                                                        <asp:ListItem Value="1">ไม่เคยรับโทษ</asp:ListItem>
				                                                        <asp:ListItem Value="2">เคยรับโทษ</asp:ListItem>	
			                                                        </asp:DropDownList>

													            </div>
												            </div>
													    </div>
                                                    </div>	
--%>																						   
												</asp:Panel>
					            </div>
					            <div class="portlet-body no-more-tables">
    								
								    <asp:Label ID="lblCountPosNo" runat="server" Font-Size="14px" Font-Bold="true"></asp:Label>
                                <table class="table table-full-width table-advance dataTable no-more-tables table-hover">
									<thead>
                                        <tr>
											<th><i class="icon-sitemap"></i> หน่วยงาน</th>
											<th><i class="icon-user"></i> พนักงาน</th>
											<th><i class="icon-briefcase"></i> ตำแหน่งปัจจุบัน</th>
										    <th style="text-align:center;"><i class="icon-user"></i> ระดับ</th>
                                            <th style="text-align:center; width :400px;"><i class="icon-briefcase"></i> ประวัติโดยย่อ</th>
                                            <th style="text-align:center;"><i class="icon-bolt"></i> ดำเนินการ</th>
										</tr>
									</thead>
									<tbody>
										<asp:Repeater ID="rptPersonal" OnItemCommand="rptPersonal_ItemCommand" OnItemDataBound="rptPersonal_ItemDataBound" runat="server">
									        <ItemTemplate>	
                                                <tr>                                        
											        <td ><asp:Label ID="lblPSNDept" runat="server"></asp:Label>
                                                         <asp:Label ID="lblTMP_PSNDept"  runat="server"  style="display:none;" ></asp:Label>
                                                    </td>
											        <td ><asp:Label ID="lblPSNName" runat="server"></asp:Label></td>
											        <td ><asp:Label ID="lblPSNPos" runat="server"></asp:Label></td>
											        <td style="text-align:center;"><asp:Label ID="lblPSNClass" runat="server"></asp:Label></td>
                                                    <td  style="text-align:Left;"><asp:Label ID="lblHistory" runat="server"></asp:Label><asp:Label ID="lblHistory_PUNISH" runat="server"></asp:Label></td>  
                                                    <td data-title="ดำเนินการ">
                                                            <asp:Button ID="btnView" runat="server" CssClass="btn mini blue" CommandName="SelectPosAll" Text="เลือก" />	
                                                    </td>
										        </tr>
									        </ItemTemplate>
                                         </asp:Repeater> 								
									</tbody>
								</table>
    								
								    <asp:PageNavigation ID="Pager_PSNL_NO_List" OnPageChanging="Pager_PSNL_NO_List_PageChanging" MaximunPageCount="10" PageSize="10" runat="server" />
							    </div>				            
				            </div>
				            <div class="modal-footer">
				                <%--<asp:Button ID="btnClose_PSNL_NO" runat="server" CssClass="btn" Text="ปิด" />--%>								
				            </div>
                            </div>
                            <a title="ลบ" id="btnModalAddPSNL_NO_Close" onserverclick="btnModalAddPSNL_NO_Close_ServerClick" runat ="server"  class="fancybox-item fancybox-close" ></a>
                        </div>
			            </asp:Panel>

                      <%-- เลือกตำแหน่งที่สามารถเข้าตำแหน่งนี้ได้   LEFT --%>
		                <asp:Panel CssClass="modal" style="top:10%;left :30%; position:fixed; width:auto;" id="ModalPost" runat="server" DefaultButton="btnSearchPos"  >
                        <div style="padding: 0px; width: auto;  margin-top :0px; margin-bottom :0px; top: 0px; left: 0px;" class="fancybox-skin">
                        <div class="fancybox-outer">
				            <div class="modal-header">										
					            <h3>เลือกตำแหน่งที่ต้องการจะเปรียบเทียบพนักงานเพื่อเข้ารับตำแหน่ง </h3>
				            </div>
				            <div class="modal-body">					            
					            <div class="row-fluid form-horizontal">
					                 <div class="span12 ">
                                        		<div class="control-group">
													            <label class="control-label" ><i class="icon-sitemap"></i> ฝ่าย</label>
													            <div class="controls">
                                                                    <asp:DropDownList ID="ddlSector_Pos" OnSelectedIndexChanged="txtSearchPos_TextChanged" runat="server" AutoPostBack="true" CssClass="medium m-wrap">
								                                    </asp:DropDownList>
														        </div>
												            </div>

									    <div class="control-group">
								            <label class="control-label" style="width:120px;"><i class="icon-book"></i> ชื่อตำแหน่ง</label>
								            <div class="controls">
									            <asp:TextBox ID="txtSearchPos" OnTextChanged="txtSearchPos_TextChanged" runat="server" AutoPostBack="true" CssClass="m-wrap large" placeholder="ค้นหาจากชื่อตำแหน่ง/เลขประจำตำแหน่ง"></asp:TextBox>
									            <asp:Button ID="btnSearchPos" OnClick="txtSearchPos_TextChanged" runat="server" Text="" style="display:none;" />
								            </div>
							            </div>

								    </div>								       
					            </div>
					            <div class="portlet-body no-more-tables">
    								
								    <asp:Label ID="Label3" runat="server" Font-Size="14px" Font-Bold="true"></asp:Label>
								    <table class="table table-full-width  no-more-tables table-hover">
									    <thead>
										    <tr>

                                                <th style="text-align:center;"><i class="icon-sitemap"></i> ฝ่าย</th>
											    <th style="text-align:center;"><i class="icon-sitemap"></i> หน่วยงาน</th>
											    <th style="text-align:center;"><i class="icon-bookmark"></i> ระดับ</th>
											    <th style="text-align:center;"><i class="icon-briefcase"></i> ตำแหน่ง</th>
											    <th style="text-align:center;"><i class="icon-bookmark"></i> ตำแหน่งการบริหาร</th>											
											    <%--<th style="text-align:center;"><i class="icon-user"></i> ผู้ครองตำแหน่ง</th>
											    <th style="text-align:center;"><i class="icon-caret-right"></i> รหัส</th>
											    <th style="text-align:center;"><i class="icon-book"></i> ชื่อวิชา</th>	--%>										    
											    <th style="text-align:center;"><i class="icon-check"></i> เลือก</th>
										    </tr>										    
									    </thead>
									    <tbody>
										    <asp:Repeater ID="rptPosDialog"  OnItemCommand="rptPosDialog_ItemCommand" OnItemDataBound="rptPosDialog_ItemDataBound" runat="server">
									            <ItemTemplate>
    									            <tr>                                        
											            <td style="text-align:left;"><asp:Label ID="lblSector" runat="server"></asp:Label></td>
											            <td ><asp:Label ID="lblDept" runat="server"></asp:Label></td>		
                                                        <td style="text-align:center;"><asp:Label ID="lblClass" runat="server"></asp:Label></td>	
                                                        <td ><asp:Label ID="lblPosNo" runat="server"></asp:Label></td>	
                                                        <td ><asp:Label ID="lblMGR" runat="server"></asp:Label>
                                                            <asp:Label ID="lblPSN" runat="server" style="display:none;"></asp:Label>
                                                        </td>	
                                                        <%--<td ></td>--%>										           
											            <td data-title="ดำเนินการ" style="text-align:center;">
											                <asp:Button ID="btnPosSelect" runat="server" CssClass="btn mini blue" CommandName="Select" Text="เลือก" /> 
										                </td>
										            </tr>	
									            </ItemTemplate>
									        </asp:Repeater>					
									    </tbody>
								    </table>
    								
								    <asp:PageNavigation ID="Pager_Pos" OnPageChanging="Pager_Pos_PageChanging" MaximunPageCount="10" PageSize="10" runat="server" />
							    </div>				            
				            </div>
				            <div class="modal-footer">
				                <%--<asp:Button ID="btnClose_Pos" runat="server" CssClass="btn" Text="ปิด" />--%>								
				            </div>
                                </div>
                            <a title="ลบ" id="btnModalPost_Close"  onserverclick="btnModalPost_Close_ServerClick" runat ="server"  class="fancybox-item fancybox-close" ></a>
                        </div>
			            </asp:Panel>
					
    			           
</div>
</ContentTemplate>
</asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ScriptPlaceHolder" Runat="Server">
</asp:Content>


using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Xml.Linq;
using System.Diagnostics;
using System.Collections.Specialized;
using System.Configuration;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Caching;
using System.Web.SessionState;
using System.Web.Security;
using System.Web.Profile;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;

namespace VB
{


	public partial class Setting_Organize : System.Web.UI.Page
	{


		HRBL BL = new HRBL();
		GenericLib GL = new GenericLib();

		textControlLib TC = new textControlLib();

		protected void Page_Load(object sender, System.EventArgs e)
		{
			if ((Session["USER_PSNL_NO"] == null)) {
				ScriptManager.RegisterStartupScript(this.Page, typeof(string), "Alert", "showAlert('กรุณาลงชื่อเข้าใช้ระบบ');", true);
				Response.Redirect("Login.aspx");
				return;
			}

			if (!IsPostBack) {
				BL.BindDDlSector(ddlSector);
				BindOrganize();
			}
		}


		private void BindOrganize()
		{
			//----------------- Copy From vw_HR_Department---------------
			string SQL = "";
			SQL += " SELECT DEPT.PNDP_DEPT_CODE DEPT_CODE,DEPT.PNDP_MINOR_CODE MINOR_CODE,LEFT(DEPT.PNDP_DEPT_CODE,2) SECTOR_CODE" + "\n";
			SQL += " \t,ISNULL(DEPT.PNDP_SECTOR_NAME,DEPT.PNDP_CENTER_NAME) SECTOR_NAME";
			SQL += " \t,DEPT.PNDP_DEPARTMENT_NAME DEPT_NAME" + "\n";
			SQL += " \t,COUNT(PSN.PNPS_PSNL_NO) TOTAL_PSN" + "\n";
			SQL += " \t,MAX(POS.PNPO_CLASS) MAX_CLASS" + "\n";
			SQL += " \t,REPLICATE('0',2-LEN(MAX(POS.PNPO_CLASS)-1))+CAST(MAX(POS.PNPO_CLASS)-1 AS varchar) VICE_CLASS" + "\n";
			SQL += " \t,DEPT.Update_Time " + "\n";
			SQL += " \tFROM tb_PN_DEPARTMENT_R DEPT" + "\n";
			SQL += " \tINNER JOIN tb_PN_POSITION_T POS ON POS.PNPO_DEPT_CD=DEPT.PNDP_DEPT_CODE AND POS.PNPO_MINOR_CD=DEPT.PNDP_MINOR_CODE" + "\n";
			SQL += " \t\t\t\t\t\t\t\t\t\tAND PNPO_TYPE IN (0,4,9)" + "\n";
			SQL += " \t\t\t\t\t\t\t\t\t\t" + "\n";
			SQL += " \tLEFT JOIN tb_PN_PERSONAL_VIEW PSN ON PSN.PNPS_PSNL_NO=POS.PNPO_PSNL_NO \tAND PNPS_PSNL_STAT<>'03' ----- ยังจ้างอยู่ -----\t\t" + "\n";
			SQL += " \t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t" + "\n";
			SQL += " \tWHERE DEPT.PNDP_MINOR_CODE='00' AND (DEPT_STATUS IS NULL OR DEPT_STATUS<>'N' )" + "\n";

			if (ddlSector.SelectedIndex > 0) {
				SQL += " AND ( LEFT(DEPT.PNDP_DEPT_CODE,2)='" + ddlSector.Items[ddlSector.SelectedIndex].Value + "' " + "\n";
				SQL += " OR LEFT(DEPT.PNDP_DEPT_CODE,4)='" + ddlSector.Items[ddlSector.SelectedIndex].Value + "' " + "\n";
				if (ddlSector.Items[ddlSector.SelectedIndex].Value == "3200") {
					SQL += " AND DEPT.PNDP_DEPT_CODE NOT IN ('32000300'))    " + "\n";
				//-----------ฝ่ายโรงงานผลิตยาสูบ 3
				} else if (ddlSector.Items[ddlSector.SelectedIndex].Value == "3203") {
					SQL += " OR DEPT.PNDP_DEPT_CODE IN ('32000300','32030000','32030100','32030200','32030300','32030500'))  " + "\n";
				//-----------ฝ่ายโรงงานผลิตยาสูบ 4
				} else if (ddlSector.Items[ddlSector.SelectedIndex].Value == "3204") {
					SQL += " OR DEPT.PNDP_DEPT_CODE IN ('32040000','32040100','32040200','32040300','32040500','32040300'))  " + "\n";
				//-----------ฝ่ายโรงงานผลิตยาสูบ 5
				} else if (ddlSector.Items[ddlSector.SelectedIndex].Value == "3205") {
					SQL += " OR DEPT.PNDP_DEPT_CODE IN ('32050000','32050100','32050200','32050300','32050500'))\t  " + "\n";
				} else {
					SQL += ")   " + "\n";
				}
			}

			if (!string.IsNullOrEmpty(txtFilter.Text)) {
				SQL += " AND DEPT.PNDP_DEPARTMENT_NAME LIKE '%" + txtFilter.Text.Replace("'", "''") + "%' " + "\n";
			}

			SQL += " \tGROUP BY DEPT.PNDP_DEPT_CODE,DEPT.PNDP_MINOR_CODE,DEPT.PNDP_SECTOR_NAME," + "\n";
			SQL += " \tDEPT.PNDP_CENTER_NAME,DEPT.PNDP_DEPARTMENT_NAME,DEPT.PNDP_DIVISION_NAME,DEPT.Update_Time " + "\n";
			SQL += " \t" + "\n";
			SQL += " \tHAVING COUNT(PSN.PNPS_PSNL_NO)>0" + "\n" + "\n";
			SQL += "    ORDER BY SECTOR_CODE,DEPT_CODE" + "\n";
			SqlDataAdapter DA = new SqlDataAdapter(SQL, BL.ConnectionString());
			DataTable DT = new DataTable();
			DA.Fill(DT);

			Session["Setting_Organize_DEPT"] = DT;
			Pager.SesssionSourceName = "Setting_Organize_DEPT";
			Pager.RenderLayout();

			if (DT.Rows.Count == 0) {
				lblCountList.Text = "ไม่พบรายการดังกล่าว";
			} else {
				lblCountList.Text = "พบ " + GL.StringFormatNumber(DT.Rows.Count.ToString(), 0) + " รายการ";
			}

		}

		protected void Pager_PageChanging(PageNavigation Sender)
		{
			Pager.TheRepeater = rptList;
		}

		//---------- Datasource = Department ---------
		string LastSector = "";
		protected void rptList_ItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
		{
			if (e.Item.ItemType != ListItemType.Item & e.Item.ItemType != ListItemType.AlternatingItem)
				return;

            Label lblSector = (Label)e.Item.FindControl("lblSector");
            Label lblDept = (Label)e.Item.FindControl("lblDept");
            Label lblDeptCode = (Label)e.Item.FindControl("lblDeptCode");
            Label lblPSN = (Label)e.Item.FindControl("lblPSN");
            Label lblMGR = (Label)e.Item.FindControl("lblMGR");
            Label lblVICE = (Label)e.Item.FindControl("lblVICE");
            Label lblUpdate = (Label)e.Item.FindControl("lblUpdate");

            DataRowView drv = (DataRowView)e.Item.DataItem;

			if (LastSector != drv["SECTOR_NAME"].ToString()) {
				lblSector.Text = drv["SECTOR_NAME"].ToString();
				LastSector = drv["SECTOR_NAME"].ToString();
			}
			lblDept.Text = drv["DEPT_NAME"].ToString();
			lblDeptCode.Text = drv["DEPT_CODE"].ToString();
			lblPSN.Text = GL.StringFormatNumber(drv["Total_PSN"].ToString(), 0);
			if (!GL.IsEqualNull(drv["MAX_CLASS"])) {
				lblMGR.Text =GL.CINT ( drv["MAX_CLASS"]).ToString ();
			}
			if (!GL.IsEqualNull(drv["VICE_CLASS"])) {
				lblVICE.Text = GL.CINT (drv["VICE_CLASS"]).ToString();
			}
			if (!GL.IsEqualNull(drv["Update_Time"])) {
                lblUpdate.Text = BL.DateTimeToThaiDateTime((DateTime)drv["Update_Time"]);
			} else {
				lblUpdate.Text = "-";
			}

		}

		protected void Search_Changed(object sender, System.EventArgs e)
		{
			BindOrganize();
		}
		public Setting_Organize()
		{
			Load += Page_Load;
		}

	}
}

using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Xml.Linq;
using System.Diagnostics;
using System.Collections.Specialized;
using System.Configuration;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Caching;
using System.Web.SessionState;
using System.Web.Security;
using System.Web.Profile;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
namespace VB
{

	public partial class RPT_A_ASSESSOR_SET_RANK : System.Web.UI.Page
	{


		HRBL BL = new HRBL();
		GenericLib GL = new GenericLib();

		public int R_Year {
			get {
				try {
					return GL.CINT(GL.SplitString(ddlYear.Items[ddlYear.SelectedIndex].Value, "-")[0]);
				} catch (Exception ex) {
					return 0;
				}
			}

		}

		protected void Page_Load(object sender, System.EventArgs e)
		{
			if ((Session["USER_PSNL_NO"] == null)) {
				ScriptManager.RegisterStartupScript(this.Page, typeof(string), "Alert", "showAlert('กรุณาลงชื่อเข้าใช้ระบบ');", true);
				Response.Redirect("Login.aspx");
				return;
			}

			if (!IsPostBack) {
				BL.BindDDlYear(ddlYear);
				ddlYear.Items.RemoveAt(0);
				BindData();
			} 
			SetCheckboxStyle();

		}

		private void SetCheckboxStyle()
		{
			for (int i = 8; i <= 13; i++) {
                CheckBox chk = (CheckBox)pnlList.FindControl("chkClass" + i.ToString());
				if (chk.Checked) {
					chk.CssClass = "checked";
				} else {
					chk.CssClass = "";
				}
			}
		}


		private void BindData()
		{
			string SQL = "";

			SQL += "DECLARE @YEAR AS INT; SELECT @YEAR =" + R_Year + ";" + "\n";
            SQL += "SELECT DISTINCT ASS.ASSESSOR_POS,ISNULL(POS.MGR_NAME,POS.FN_NAME) AS POSITION," + "\n";
            //SQL += "POS.PNPO_CLASS,POS.DEPT_CODE,POS.DEPT_NAME,POS.PSNL_NO,PSN.PSNL_Fullname,ISNULL(RANK_COUNT,0) AS RANK_COUNT" + "\n";
            SQL += "POS.PNPO_CLASS,POS.DEPT_CODE,POS.DEPT_NAME,POS.PSNL_NO,ISNULL(PSN.PSN_PSNL_Fullname,PSN_.PSNL_Fullname) PSNL_Fullname,ISNULL(RANK_COUNT,0) AS RANK_COUNT" + "\n";
            SQL += "FROM" + "\n";
			SQL += "    (SELECT ASSESSOR_POS FROM tb_HR_Assessor_Pos GROUP BY ASSESSOR_POS) ASS" + "\n";
			SQL += "    INNER JOIN vw_PN_Position POS ON ASS.ASSESSOR_POS = POS.POS_NO" + "\n";
            SQL += "    LEFT JOIN tb_HR_Assessment_PSN PSN ON ASS.ASSESSOR_POS = PSN.PSN_POS_NO AND PSN.R_Year = @YEAR " + "\n";
	            //SQL += "    LEFT JOIN vw_PN_PSNL PSN ON ASS.ASSESSOR_POS = PSN.POS_NO" + "\n";

            SQL += "    LEFT JOIN vw_PN_PSNL PSN_ ON ASS.ASSESSOR_POS = PSN_.POS_NO" + "\n";
           
            SQL += "    LEFT JOIN " + "\n";
			SQL += "    (SELECT ASSESSOR_POS,COUNT(1) AS RANK_COUNT " + "\n";
			SQL += "    FROM tb_HR_Assessor_Result_RANK" + "\n";
			SQL += "    WHERE R_Year = @YEAR " + "\n";
			SQL += "    GROUP BY ASSESSOR_POS" + "\n";
			SQL += "    ) R_RANK  ON ASS.ASSESSOR_POS = R_RANK.ASSESSOR_POS" + "\n";

			//--------------กรอง เงื่อนไข ------------
			if (chk.Checked) {
				SQL += "WHERE ISNULL(RANK_COUNT,0) < 5" + "\n";
			}

			//--------------กรอง ระดับ ------------
			if ((chkClass8.Checked & chkClass9.Checked & chkClass10.Checked & chkClass11.Checked & chkClass12.Checked & chkClass13.Checked) | (!chkClass8.Checked & !chkClass9.Checked & !chkClass10.Checked & !chkClass11.Checked & !chkClass12.Checked & !chkClass13.Checked)) {
				//----------- Do nothing--------
			} else {
				string _classFilter = "";
				string _classTitle = "";
				for (int i = 8; i <= 13; i++) {
                    CheckBox _chk = (CheckBox)pnlList.FindControl("chkClass" + i.ToString());
                    if (_chk.Checked)
                    {
						_classFilter += "'" + i.ToString().PadLeft(2, GL.chr0) + "',";
						_classTitle += i + ",";
					}
				}
				if (chk.Checked) {
					SQL += " AND POS.PNPO_CLASS IN (" + _classFilter.Substring(0, _classFilter.Length - 1) + ") " + "\n";

				} else {
					SQL += " WHERE POS.PNPO_CLASS IN (" + _classFilter.Substring(0, _classFilter.Length - 1) + ") " + "\n";
				}
			}


			SQL += "  ORDER BY POS.DEPT_CODE,POS.PNPO_CLASS DESC,ASS.ASSESSOR_POS" + "\n";

			SqlDataAdapter DA = new SqlDataAdapter(SQL, BL.ConnectionString());
			DataTable DT = new DataTable();
			DA.Fill(DT);
			Session["Assessor_Set_RANK"] = DT;
			Pager.SesssionSourceName = "Assessor_Set_RANK";
			Pager.RenderLayout();

			//If DT.Rows.Count = 0 Then
			//    lblCountList.Text = "แสดงความสัมพันธ์ระหว่างผู้ประเมินและผู้ถูกประเมินตามตำแหน่ง : ไม่พบรายการดังกล่าว"
			//Else
			//    lblCountList.Text = "แสดงความสัมพันธ์ระหว่างผู้ประเมินและผู้ถูกประเมินตามตำแหน่ง : พบ " & GL.StringFormatNumber(DT.Rows.Count, 0) & " รายการ"
			//End If

		}

		protected void Pager_PageChanging(PageNavigation Sender)
		{
			Pager.TheRepeater = rptList;
		}

		protected void rptList_ItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
		{
			if (e.Item.ItemType != ListItemType.Item & e.Item.ItemType != ListItemType.AlternatingItem)
				return;

            Label lblPos = (Label)e.Item.FindControl("lblPos");
            Label lblName = (Label)e.Item.FindControl("lblName");
            Label lblClass = (Label)e.Item.FindControl("lblClass");
            Label lblDept = (Label)e.Item.FindControl("lblDept");
            Label lblRank = (Label)e.Item.FindControl("lblRank");

            DataRowView drv = (DataRowView)e.Item.DataItem;

			lblPos.Text = drv["ASSESSOR_POS"].ToString() + " : " + drv["POSITION"].ToString();

			if (!GL.IsEqualNull(drv["PSNL_NO"])) {
				lblName.Text = drv["PSNL_NO"].ToString() + " : " + drv["PSNL_Fullname"].ToString();
				lblName.ForeColor = System.Drawing.Color.SteelBlue;
			} else {
				lblName.Text = "ไม่มีผู้ครองตำแหน่ง";
				lblName.ForeColor = System.Drawing.Color.Red;
			}
			if (!GL.IsEqualNull(drv["PNPO_CLASS"])) {
				lblClass.Text = GL.CINT (drv["PNPO_CLASS"]).ToString();
			}
			lblDept.Text = drv["DEPT_NAME"].ToString();

			if ((int)drv["RANK_COUNT"] < 5) {
				lblRank.Text = "ยังไม่มีกำหนดเกณฑ์";
				lblRank.ForeColor = System.Drawing.Color.Red;
			} else {
				lblRank.Text = "กำหนดเกณฑ์แล้ว";
				lblRank.ForeColor = System.Drawing.Color.Green;
			}

			//'----------------- Get Assigned -----------
			//Dim DT As DataTable = ASSIGNED.Copy
			//DT.DefaultView.RowFilter = "ASSESSOR_POS='" & drv["ASSESSOR_POS"] & "'"
			//For i As Integer = 0 To DT.DefaultView.Count - 1
			//    lblAssigned.Text &= "<i class='icon-user-md'></i> " & DT.DefaultView(i).Item("MGR_PSNL_NO") & ":" & DT.DefaultView(i).Item("MGR_PSNL_Fullname") & "<br>"
			//Next
			//If DT.DefaultView.Count > 0 Then
			//    lblAssigned.ForeColor = Drawing.Color.Green
			//End If

			//btnEdit.CommandArgument = drv["ASSESSOR_POS"]
		}



        protected void btnSearch_Click(object sender, System.EventArgs e)
        {
            BindData();
        }

		public RPT_A_ASSESSOR_SET_RANK()
		{
			Load += Page_Load;
		}



	}
}

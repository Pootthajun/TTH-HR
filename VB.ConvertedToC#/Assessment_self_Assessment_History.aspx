﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPage.Master" CodeBehind="Assessment_self_Assessment_History.aspx.cs" Inherits="VB.Assessment_self_Assessment_History"  %>

<%@ Register src="WUC_DateReporter.ascx" tagname="WUC_DateReporter" tagprefix="uc1" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<%@ Register src="PageNavigation.ascx" tagname="PageNavigation" tagprefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<asp:ToolkitScriptManager ID="toolkit1" runat="server"></asp:ToolkitScriptManager>

<asp:UpdatePanel ID="UpdatePanel1" runat="server">      
<ContentTemplate>

<div class="container-fluid">
				<!-- BEGIN PAGE HEADER-->
				<div class="row-fluid" id="fixedPageHeader">
					<div class="span12"> 
						<!-- BEGIN PAGE TITLE & BREADCRUMB-->
						<h3 class="page-user-md">ประวัติการประเมินผล <font color="blue">(ScreenID : S-EMP-05)</font></h3>					
									
						<ul class="breadcrumb">
                            <li>
                            	<i class="icon-user"></i> <a href="javascript:;">การประเมินตนเอง</a><i class="icon-angle-right"></i>
                            </li>
                        	<li>
                        	    <i class="icon-time"></i> <a href="javascript:;">ประวัติการประเมินผล</a>
                        	</li>                        	
                        	
                            <uc1:WUC_DateReporter ID="WUC_DateReporter1" runat="server" />
                        </ul>
						<!-- END PAGE TITLE & BREADCRUMB-->
									        
				     </div>
			    </div>
		        <!-- END PAGE HEADER-->
				
				  <div class="row-fluid">
    					
    					<h3 class="form-section"><i class="icon-user"></i> สำหรับ<asp:Label ID="lblName" runat="server"></asp:Label></h3>
			        
					    <div class="span12">
						    <!-- BEGIN SAMPLE TABLE PORTLET-->
    						                <div class="row-fluid form-horizontal">
												     <div class="span4 ">
														<div class="control-group">
													        <label class="control-label"><i class="icon-sitemap"></i> รอบ</label>
													        <div class="controls">
														        <asp:DropDownList ID="ddlRound" OnSelectedIndexChanged="ddl_SelectedIndexChanged" runat="server" AutoPostBack="true" CssClass="medium m-wrap">
							                                    </asp:DropDownList>
													        </div>
												        </div>
													</div>	
													 <div class="span3 ">
														<div class="control-group">
													        <label class="control-label"><i class="icon-check"></i> แบบประเมิน</label>
													        <div class="controls">
													            <asp:DropDownList ID="ddlAssType" OnSelectedIndexChanged="ddl_SelectedIndexChanged" runat="server" AutoPostBack="true" CssClass="medium m-wrap">
							                                        <asp:ListItem selected="True">ทั้งหมด</asp:ListItem>
							                                        <asp:ListItem >ตัวชี้วัด (KPI)</asp:ListItem>		
							                                        <asp:ListItem >สมรรถนะ (Competency)</asp:ListItem>
							                                    </asp:DropDownList>														      
													        </div>
												        </div>
													</div>
																								   
												</div>
												
    								            
							    <div class="portlet-body no-more-tables">
								    <asp:Label ID="lblCountASS" runat="server" Font-Size="14px" Font-Bold="true"></asp:Label>
								    <table class="table table-full-width table-advance dataTable no-more-tables table-hover">
									    <thead>
										    <tr>
											    <th><i class="icon-sitemap"></i> รอบการประเมิน</th>
											    <th><i class="icon-check"></i> แบบประเมิน</th>										    
											    <th><i class="icon-th-list"></i> คะแนนประเมิน</th>
											    <th><i class="icon-th-list"></i> คิดเป็น %</th>
											    <th><i class="icon-info-sign"></i> สถานะ</th>
											    <th><i class="icon-bolt"></i> ดำเนินการ</th>
										    </tr>
									    </thead>
									    <tbody>
										    <asp:Repeater ID="rptAss"  OnItemDataBound="rptAss_ItemDataBound" runat="server">
										            <ItemTemplate>
										                <tr>
											            <td data-title="รอบการประเมิน"><asp:Label ID="lblRound" runat="server"></asp:Label></td>
											            <td data-title="แบบประเมิน"><asp:Label ID="lblType" runat="server"></asp:Label></td>
											            <td data-title="คะแนนประเมิน"><asp:Label ID="lblResult" runat="server"></asp:Label></td>
											            <td data-title="คิดเป็น %"><asp:Label ID="lblPercent" runat="server"></asp:Label></td>
											            <td data-title="สถานะ" style="color:green"><asp:Label ID="lblStatus" runat="server"></asp:Label></td>
											            <td data-title="ดำเนินการ">
											                <a ID="lnkView" runat="server" class="btn mini blue" target="_blank">
											                <i class="icon-bolt"></i> การประเมิน</a>
											            </td>
										                </tr>		
										            </ItemTemplate>
										    </asp:Repeater>									   								    					
									    </tbody>
								    </table>
								    <asp:PageNavigation ID="Pager" OnPageChanging="Pager_PageChanging" MaximunPageCount="10" PageSize="20" runat="server" />
							    </div>
    						
						    <!-- END SAMPLE TABLE PORTLET-->
    						
					    </div>
                    </div>  
						 
</div>	

</ContentTemplate>           
</asp:UpdatePanel>

</asp:Content>

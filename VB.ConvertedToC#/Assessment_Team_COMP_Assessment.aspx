﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPage.Master" CodeBehind="Assessment_Team_COMP_Assessment.aspx.cs" Inherits="VB.Assessment_Team_COMP_Assessment" %>

<%@ Register src="WUC_DateReporter.ascx" tagname="WUC_DateReporter" tagprefix="uc1" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<%@ Register src="PageNavigation.ascx" tagname="PageNavigation" tagprefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
 <style>
.Buttonfooter {
   position: sticky;
   left: 0;
   bottom: 0;
   width: 100%;
   background-color:  Gray  ;
   color: white;
   text-align: center;
}
</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<asp:ToolkitScriptManager ID="toolkit1" runat="server"></asp:ToolkitScriptManager>

<asp:UpdatePanel ID="UpdatePanel1" runat="server">      
<ContentTemplate>

<div class="container-fluid">
				<!-- BEGIN PAGE HEADER-->
				<div class="row-fluid" id="fixedPageHeader">
					<div class="span12"> 
						<!-- BEGIN PAGE TITLE & BREADCRUMB-->
						<h3 class="page-user-md">การประเมินผลสมรรถนะพนักงาน (Competency) <font color="blue">(ScreenID : S-MGR-05)</font>
                        <asp:Label ID="lblReportTime" Font-Size="14px" ForeColor="Green" runat="Server"></asp:Label>
                        </h3>					
						
                        <h3 id="pnlFilterYear" runat="server" visible="True">
						     สำหรับรอบการประเมิน
                              <asp:DropDownList ID="ddlRound" OnSelectedIndexChanged="Search" runat="server" AutoPostBack="true" CssClass="medium m-wrap" style="font-size:20px;">
							</asp:DropDownList>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;
                            <asp:DropDownList ID="ddlForm" OnSelectedIndexChanged="ddlForm_SelectedIndexChanged" runat="server" Visible ="false"  style="font-size:20px;" AutoPostBack="true" CssClass="medium m-wrap">
				                    <asp:ListItem Value="0" Selected="true">ประเมินผลสมรรถนะ  </asp:ListItem>
				                    <asp:ListItem Value="1" >กำหนดแบบประเมิน </asp:ListItem>
			                </asp:DropDownList>
						</h3>	
						<ul class="breadcrumb">
                            <li>
                            	<i class="icon-group"></i> <a href="javascript:;">การประเมินพนักงาน</a><i class="icon-angle-right"></i>
                            </li>
                        	<li>
                        	    <i class="icon-check"></i> <a href="javascript:;">การประเมินผลสมรรถนะ (Competency)</a>
                        	</li>                      	
                        	
                            <uc1:WUC_DateReporter ID="WUC_DateReporter1" runat="server" />
                        </ul>
						<!-- END PAGE TITLE & BREADCRUMB-->
									        
				     </div>

				
			    </div>
		        <!-- END PAGE HEADER-->
				
               <asp:Panel ID="pnlList" runat="server" Visible="True" DefaultButton="btnSearch">
				<div class="row-fluid">
					
					<div class="span12">
						<!-- BEGIN SAMPLE TABLE PORTLET-->
						            
						            <div class="row-fluid form-horizontal">
									    <div class="span4 ">
											<div class="control-group">
										        <label class="control-label"><i class="icon-user"></i> ชื่อ</label>
										        <div class="controls">
											        <asp:TextBox ID="txtName" OnTextChanged="Search" runat="server" AutoPostBack="true" CssClass="m-wrap medium" placeholder="ค้นหาจากชื่อ/เลขประจำตัว"></asp:TextBox>
										        </div>
									        </div>
										</div>
										<div class="span3 ">
											<div class="control-group">
										        <label class="control-label"><i class="icon-info-sign"></i> สถานะ</label>
										        <div class="controls">
											        <asp:DropDownList ID="ddlStatus" OnSelectedIndexChanged="Search" runat="server" AutoPostBack="true" CssClass="medium m-wrap">
											        </asp:DropDownList>
										        </div>
									        </div>
										</div>
									</div>
								    <asp:Button ID="btnSearch" OnClick="Search" runat="server" Text="" style="display:none;" />
								            
							<div class="portlet-body no-more-tables">
								<asp:Label ID="lblCountPSN" runat="server" Font-Size="14px" Font-Bold="true"></asp:Label>
								<table class="table table-full-width table-advance dataTable no-more-tables table-hover">
									<thead>
										<tr>
											<th><i class="icon-sitemap"></i> หน่วยงาน</th>
											<th><i class="icon-user"></i> เลขประจำตัว</th>
											<th><i class="icon-user"></i> ชื่อ</th>
											<th><i class="icon-bookmark"></i> ตำแหน่ง</th>
											<th><i class="icon-list-ol"></i> ระดับ</th>
											<th><i class="icon-info-sign"></i> สถานะ</th>
											<th><i class="icon-star"></i> คะแนนที่ได้</th>
											<th><i class="icon-dashboard"></i> คิดเป็น (%)</th>
											<th><i class="icon-bolt"></i> ดำเนินการ</th>
										</tr>
									</thead>
									<tbody>
									<asp:Repeater ID="rptCOMP" OnItemCommand="rptCOMP_ItemCommand" OnItemDataBound="rptCOMP_ItemDataBound" runat="server">
									        <ItemTemplate>
									            <tr>
											    <td data-title="หน่วยงาน"><asp:Label ID="lnkPSNOrganize" runat="server"></asp:Label></td>
										        <td data-title="เลขประจำตัว"><asp:Label ID="lblPSNNo" runat="server"></asp:Label></td>
										        <td data-title="ชื่อ"><asp:Label ID="lblPSNName" runat="server"></asp:Label></td>
											    <td data-title="ตำแหน่ง"><asp:Label ID="lblPSNPos" runat="server"></asp:Label></td>
											    <td data-title="ระดับ"><asp:Label ID="lblPSNClass" runat="server"></asp:Label></td>
											    <td data-title="สถานะ"><asp:Label ID="lblPSNCOMPStatus" runat="server"></asp:Label></td>											    
											    <td data-title="คะแนนที่ได้"><asp:Label ID="lblResult" runat="server"></asp:Label></td>
											    <td data-title="คิดเป็น (%)"><asp:Label ID="lblPercent" runat="server"></asp:Label></td>
										        <td data-title="ดำเนินการ">
										            <asp:Button ID="btnPSNEdit" runat="server" CssClass="btn mini blue" CommandName="Edit" Text="การประเมิน" /> 											            
									            </td>
										        </tr>
									        </ItemTemplate>
									</asp:Repeater>
									        	
									</tbody>
								</table>
								
								<asp:PageNavigation ID="Pager" OnPageChanging="Pager_PageChanging" MaximunPageCount="10" PageSize="20" runat="server" />
							</div>
						
						<!-- END SAMPLE TABLE PORTLET-->
						
					</div>
                </div>  
                </asp:Panel>  
                
                <asp:Panel ID="pnlEdit" runat="server" Visible="False">

                <div class="portlet-body form">										
			            
			        
    			                <div class="form-horizontal form-view">
    			                     <div class="btn-group pull-left">
                                                <asp:Button ID="btnBack_TOP" OnClick="btnBack_TOP_Click" runat="server" CssClass="btn" Text="ย้อนกลับ" />
                                          </div>
    			                     <div class="btn-group pull-right">                                    
			                            <a data-toggle="dropdown" class="btn dropdown-toggle">พิมพ์ <i class="icon-angle-down"></i></a>										
			                                <ul class="dropdown-menu pull-right">                                                       
                                                <li><a id="btnCOMPFormPDF" runat="server" target="_blank">ใบประเมิน รูปแบบ PDF</a></li>
                                                <li><a id="btnCOMPFormExcel" runat="server" target="_blank">ใบประเมิน รูปแบบ Excel</a></li>	
                                                <li><a id="btnIDPFormPDF" runat="server" target="_blank">แผนพัฒนารายบุคคล รูปแบบ PDF</a></li>
                                                <li><a id="btnIDPFormExcel" runat="server" target="_blank">แผนพัฒนารายบุคคล รูปแบบ Excel</a></li>	
                                                <li><a id="btnIDPResultFormPDF" runat="server" target="_blank">ผลการพัฒนาตามแผนพัฒนารายบุคคล รูปแบบ PDF</a></li>
                                                <li><a id="btnIDPResultFormExcel" runat="server" target="_blank">ผลการพัฒนาตามแผนพัฒนารายบุคคล รูปแบบ Excel</a></li>
                                                <li><a id="btnIDPRemark_Plan" href="Doc/RemarkIDP/Remark_PlanIDP.pdf" runat="server" target="_blank">Download คำชี้แจง แผนการพัฒนารายบุคคล</a></li>
                                                <li><a id="btnIDPRemark_Result" href="Doc/RemarkIDP/Remark_ResultIDP.pdf" runat="server" target="_blank">Download คำชี้แจง ผลการพัฒนาตามแผนพัฒนารายบุคคล</a></li>				                                </ul>
			                                </ul>
		                            </div>
    			                     
    			                     <h3 style="text-align:center; margin-top:0px;">
		                                    <asp:Label ID="lblCOMPStatus" runat="server" KPI_Status="0"></asp:Label> (Competency)
		                             </h3>
                                </div>	
                    <asp:Panel ID="pnlEdit_Ass" runat="server" >                 
                                 <div class="form-horizontal form-view">    	
					                <div class="row-fluid" style="border-bottom:1px solid #EEEEEE;">
					                        <div class="span6 ">
					                            <div class="control-group">
								                    <label class="control-label" style="width:100px;">ประเภท :</label>
								                    <div class="controls" style="margin-left: 120px;">
									                    <asp:Label ID="lblPSNType" runat="server" CssClass="text bold"></asp:Label>
								                    </div>
							                    </div>
					                        </div>					        
					                     </div>
    			                        <div class="row-fluid" style="border-bottom:1px solid #EEEEEE;">
					                        <div class="span4 ">
					                            <div class="control-group">
								                    <label class="control-label" style="width:100px;">ผู้รับการประเมิน :</label>
								                    <div class="controls" style="margin-left: 120px;">
									                    <asp:Label ID="lblPSNName" runat="server" CssClass="text bold"></asp:Label>
								                    </div>
							                    </div>
					                        </div>
					                        <div class="span3 ">
					                            <div class="control-group">
								                    <label class="control-label" style="width:100px;">ตำแหน่ง :</label>
								                    <div class="controls" style="margin-left: 120px;">
									                    <asp:Label ID="lblPSNPos" runat="server" CssClass="text bold"></asp:Label>
								                    </div>
							                    </div>
					                        </div>
					                        <div class="span5 ">
					                            <div class="control-group">
								                    <label class="control-label" style="width:100px;">สังกัด :</label>
								                    <div class="controls" style="margin-left: 120px;">
									                    <asp:Label ID="lblPSNDept" runat="server" CssClass="text bold"></asp:Label>
								                    </div>
							                    </div>
					                        </div>
					                     </div>
                					     
					                      <div class="row-fluid" style="margin-top:10px;">
					                        <div class="span4 ">
					                            <div class="control-group">
								                    <label class="control-label" style="width:100px;">ผู้ประเมิน :</label>
								                    <div class="controls" style="margin-left: 120px;">
									                    <asp:DropDownList ID="ddlASSName" OnSelectedIndexChanged="ddlASSName_SelectedIndexChanged" runat="Server" AutoPostBack="True" style="font-weight:bold; color:Black; border:none;">									        
									                    </asp:DropDownList>
									                </div>
							                    </div>
					                        </div>
					                        <div class="span3 ">
					                            <div class="control-group">
								                    <label class="control-label" style="width:100px;">ตำแหน่ง :</label>
								                    <div class="controls" style="margin-left: 120px;">
									                    <asp:Label ID="lblASSPos" runat="server" CssClass="text bold"></asp:Label>
								                    </div>
							                    </div>
					                        </div>
					                        <div class="span5 ">
					                            <div class="control-group">
								                    <label class="control-label" style="width:100px;">สังกัด :</label>
								                    <div class="controls" style="margin-left: 120px;">
									                    <asp:Label ID="lblASSDept" runat="server" CssClass="text bold"></asp:Label>
								                    </div>
							                    </div>
					                        </div>
					                     </div>
					                </div>
												
				    <!-- BEGIN FORM-->
				    <div class="portlet-body no-more-tables">
				                        <table class="table table-bordered no-more-tables" style="background-color:White;">
									        <thead>
										        <tr>
											        <th rowspan="2" style="text-align:center; background-color:Silver;"> ลำดับ</th>
											        <th rowspan="2" style="text-align:center; background-color:Silver;"> สมรรถนะ</th>
											        <th rowspan="2" style="text-align:center; background-color:Silver;"> มาตรฐานพฤติกรรม</th>
											        <th colspan="5" style="text-align:center; background-color:Silver;"> คะแนนตามระดับค่าเป้าหมาย</th>
											        <th rowspan="2" style="text-align:center; background-color:Silver;"> คะแนน</th>
											        <th rowspan="2" style="text-align:center; background-color:Silver;"> น้ำหนัก(%)</th>											        
										        </tr>
                                                <tr>
										          <th class="AssLevel1" style="text-align:center; font-weight:bold; vertical-align:middle;">1</th>
										          <th class="AssLevel2" style="text-align:center; font-weight:bold; vertical-align:middle;">2</th>
										          <th class="AssLevel3" style="text-align:center; font-weight:bold; vertical-align:middle;">3</th>
										          <th class="AssLevel4" style="text-align:center; font-weight:bold; vertical-align:middle;">4</th>
										          <th class="AssLevel5" style="text-align:center; font-weight:bold; vertical-align:middle;">5</th>
								              </tr>
									        </thead>
									        <tbody>
									             <asp:Panel ID="pnlCOMP" runat="server">
									             
									                    <asp:TextBox ID="txtCOMPTypeID" runat="server" style="display:none;" Text="-1"></asp:TextBox>
				                                        <asp:TextBox ID="txtMasterNo" runat="server" style="display:none;" Text="0"></asp:TextBox>
				                                        <asp:Textbox ID="txtBehaviorNo" runat="server" style="display:none;" Text="0"></asp:Textbox>
				                                        <asp:Button ID="btnBehaviorDialog" OnClick="btnBehaviorDialog_Click" runat="server" style="display:none;"/>
				                                        <asp:Button ID="btnBehaviorDelete" OnClick="btnBehaviorDelete_Click" runat="server" style="display:none;"/>
									             
									                    <asp:Repeater ID="rptAss" OnItemCommand="rptAss_ItemCommand" OnItemDataBound="rptAss_ItemDataBound" runat="server">
									                        <ItemTemplate>
									                        <tr id="row_COMP_type" runat="server">
									                            <td colspan="10" style="font-weight:bold; font-size:18px; background-color:#CCCCCC;" id="cell_COMP_type" runat="server">สมรรถนะหลัก Core Competency, สมรรถนะตามสายงาน Functional Competency, สมรรถนะตามสายระดับ Managerial Competency</td>
									                        </tr>
        									                
									                        <tr>
										                      <td data-title="ลำดับ" rowspan="6" id="ass_no" runat="server" style="text-align:center; font-weight:bold; vertical-align:top;">001</td>
										                      <td data-title="สมรรถนะ" id="COMP_Name" class="TextLevel0" runat="server" style="background-color:#f8f8f8; ">xxxxxxxxxxxx xxxxxxxxxxxx</td>
										                      <td data-title="มาตรฐานพฤติกรรม" id="target" runat="server" class="TextLevel0" style="background-color:#f8f8f8; ">xxxxxxxxxxxx xxxxxxxxxxxx</td>
										                      <td data-title="คลิกเพื่อประเมินระดับที่ 1" title="Click เพื่อเลือก" id="choice1" class="TextLevel0" runat="server" style="cursor:pointer; background-color:White;">xxxxxxxxxxxx xxxxxxxxxxxx xxxxxxxxxxxx xxxxxxxxxxxx xxxxxxxxxxxx xxxxxxxxxxxx</td>
                                                              <td data-title="คลิกเพื่อประเมินระดับที่ 2" title="Click เพื่อเลือก" id="choice2" class="TextLevel0" runat="server" style="cursor:pointer; background-color:White;">xxxxxxxxxxxx xxxxxxxxxxxx xxxxxxxxxxxx xxxxxxxxxxxx xxxxxxxxxxxx xxxxxxxxxxxx</td>
                                                              <td data-title="คลิกเพื่อประเมินระดับที่ 3" title="Click เพื่อเลือก" id="choice3" class="TextLevel0" runat="server" style="cursor:pointer; background-color:White;">xxxxxxxxxxxx xxxxxxxxxxxx xxxxxxxxxxxx xxxxxxxxxxxx xxxxxxxxxxxx xxxxxxxxxxxx</td>
                                                              <td data-title="คลิกเพื่อประเมินระดับที่ 4" title="Click เพื่อเลือก" id="choice4" class="TextLevel0" runat="server" style="cursor:pointer; background-color:White;">xxxxxxxxxxxx xxxxxxxxxxxx xxxxxxxxxxxx xxxxxxxxxxxx xxxxxxxxxxxx xxxxxxxxxxxx</td>
                                                              <td data-title="คลิกเพื่อประเมินระดับที่ 5" title="Click เพื่อเลือก" id="choice5" class="TextLevel0" runat="server" style="cursor:pointer; background-color:White;">xxxxxxxxxxxx xxxxxxxxxxxx xxxxxxxxxxxx xxxxxxxxxxxx xxxxxxxxxxxx xxxxxxxxxxxx</td>
										                      <td data-title="คะแนน" rowspan="6" title="คะแนน" id="mark" runat="server" style="text-align:center; font-size:16px; font-weight:bold; vertical-align:middle;">-</td>
										                      <td data-title="น้ำหนัก" rowspan="6" title="น้ำหนัก" id="weight" runat="server" style="text-align:center; font-size:16px; font-weight:bold; vertical-align:middle;">10%</td>
            										          
									                        </tr>
									                        <tr id="trBehavior" runat="server">
									                           <td id="tdBehavior1" runat="server" colspan="2" data-title="พฤติกรรมบ่งชี้" style=" font:inherit; background-color:#f8f8f8; text-align:center; font-size:14px;">
									                           
									                           พฤติกรรมบ่งชี้  <i class="icon-long-arrow-right"></i><br>
									                           <asp:Button ID="btnAddBehavior" runat="server" Text="(Click เพื่อเพิ่มรายการ)" style=" border:none; text-decoration:none; cursor:pointer; color:Gray; background-color:#f8f8f8;" CommandName="AddBehavior"></asp:Button>
									                           <br><br><asp:Image ID="imgAlert" runat="server" ImageUrl="~/images/alert.gif" Visible="false" />
									                           <br><asp:Label ID="lblAlert" runat="server" ForeColor="Red"></asp:Label>
									                           </td>
									                           <td id="tdBehavior2" runat="server" colspan="2"> 
									                           
									                               <asp:Repeater ID="rptBehavior" runat="server">
									                                   <ItemTemplate>
									                                            <div style="width:100%; vertical-align:top; margin-bottom:10px;">
									                                                <a tabindex="-1" class="select2-search-choice-close" onclick="return false;" href="#"></a>
									                                                <a ID="aBehavior" runat="server" style="text-decoration:none; width:80%; float:left;" href="javascript:;"></a>
									                                                <a ID="aDelete"  runat="server" class="btn mini" style="float:right;" href="javascript:;">ลบ</a>
									                                            </div><br>
									                                   </ItemTemplate>
									                               </asp:Repeater>									                               
									                           </td>
									                           <td data-title="หลักสูตรการพัฒนา" id="tdGAP1" runat="server" style=" font:inherit; background-color:#f8f8f8; text-align:center; font-size:14px;">
									                                    วิธีการพัฒนา<br>เพื่อปิด<br>ช่องว่างสมรรถนะ
									                                    <br/><asp:Button ID="btnAddGAP" runat="server" Text="(Click เพื่อเลือกวิธีการพัฒนา)" style=" border:none; text-decoration:none; cursor:pointer; color:Gray; background-color:#f8f8f8;" CommandName="AddGAP"></asp:Button>
                                                                        <br><br><asp:Image ID="imgAlertGAP" runat="server" ImageUrl="~/images/alert.gif" Visible="false" />
									                                    <br><asp:Label ID="lblAlertGAP" runat="server" ForeColor="Red"></asp:Label>
                                                               </td>
									                           <td colspan="2" id="tdGAP2" runat="server" >									                           
									                               <asp:Repeater ID="rptGAP" runat="server">
									                                   <ItemTemplate>
									                                            <div style="width:100%; vertical-align:top; margin-bottom:10px;">
									                                                <a id="aGAP" runat="server"   style="text-decoration:none;" >
									                                                    <img ID="imgSelect" runat="Server" src=""/>
									                                                    <asp:Label ID="lblGAP" runat="server"></asp:Label>
									                                                </a>
									                                            </div>
									                                   </ItemTemplate>
									                               </asp:Repeater>									                               
									                           </td>
									                        </tr>
									                        <tr>
									                           <td colspan="2" data-title="หมายเหตุ (ผู้ถูกประเมิน)" style=" font:inherit; background-color:#f8f8f8; text-align:center; font-size:14px;">หมายเหตุ <br>(ผู้ถูกประเมิน)</td>
									                           <td colspan="5" data-title="หมายเหตุ (ผู้ถูกประเมิน)" style="padding:0px;"><asp:Textbox   ID="txtPSN" runat="server" ReadOnly="true" style="width:96%; padding:5px; background-color:Transparent; height:100%; min-height:53px; border:0px none transparent;  margin:0px;" TextMode="Multiline" Text=""  Columns="50" onkeypress="return this.value.length<4000" ></asp:Textbox></td>
									                        </tr>
									                        <tr>
									                            <td colspan="2" data-title="หมายเหตุ (หัวหน้างาน/ผู้ประเมิน)" style=" font:inherit; background-color:#f8f8f8; text-align:center; font-size:14px;">หมายเหตุ <br>(ผู้ประเมิน)</td>
									                            <td colspan="5"  data-title="หมายเหตุ (ผู้ประเมิน/หัวหน้างาน)" style="padding:0px;"><asp:Textbox ID="txtMGR"  runat="server" style="width:96%; padding:5px; background-color:Transparent; height:100%; min-height:53px; border:0px none transparent;  margin:0px;" TextMode="Multiline" Text=""  Columns="50" onkeypress="return this.value.length<4000" ></asp:Textbox>
									                                <asp:Button ID="btnRemark" runat="server" style="display:none;" CommandName="Remark" />
									                            </td>
									                        </tr>
									                        <tr>
									                              <td id="cel_officer" runat="server" class="TextLevel0" colspan="2" style="text-align:center; font-weight:bold; padding:0px !important; height:10px !important;" >พนักงานประเมินตนเอง</td>
									                              <td id="selOfficialColor1" runat="server" style="padding:0px !important; height:10px !important; border-right:0px none;"><asp:Button ID="btn1" runat="server" style="display:none;" CommandName="Select" /></td>
                                                                  <td id="selOfficialColor2" runat="server" style="padding:0px !important; height:10px !important; border-right:0px none; border-left:0px none;"><asp:Button ID="btn2" runat="server" style="display:none;" CommandName="Select" /></td>
                                                                  <td id="selOfficialColor3" runat="server" style="padding:0px !important; height:10px !important; border-right:0px none; border-left:0px none;"><asp:Button ID="btn3" runat="server" style="display:none;" CommandName="Select" /></td>
                                                                  <td id="selOfficialColor4" runat="server" style="padding:0px !important; height:10px !important; border-right:0px none; border-left:0px none;"><asp:Button ID="btn4" runat="server" style="display:none;" CommandName="Select" /></td>
                                                                  <td id="selOfficialColor5" runat="server" style="padding:0px !important; height:10px !important; border-left:0px none;"><asp:Button ID="btn5" runat="server" style="display:none;" CommandName="Select" /></td>
                                                                 
									                        </tr>
									                        <tr>
									                              <td id="cel_header" runat="server" class="TextLevel0" colspan="2" style="text-align:center; font-weight:bold; padding:0px !important; height:10px !important;" >ผู้บังคับบัญชาประเมิน</td>
									                              <td data-title="คลิกเพื่อประเมินระดับที่ 1" id="selHeaderColor1" runat="server" style="cursor:pointer; padding:0px !important; height:10px !important; border-right:0px none;">&nbsp;</td>
                                                                  <td data-title="คลิกเพื่อประเมินระดับที่ 2" id="selHeaderColor2" runat="server" style="cursor:pointer; padding:0px !important; height:10px !important; border-right:0px none; border-left:0px none;">&nbsp;</td>
                                                                  <td data-title="คลิกเพื่อประเมินระดับที่ 3" id="selHeaderColor3" runat="server" style="cursor:pointer; padding:0px !important; height:10px !important; border-right:0px none; border-left:0px none;">&nbsp;</td>
                                                                  <td data-title="คลิกเพื่อประเมินระดับที่ 4" id="selHeaderColor4" runat="server" style="cursor:pointer; padding:0px !important; height:10px !important; border-right:0px none; border-left:0px none;">&nbsp;</td>
                                                                  <td data-title="คลิกเพื่อประเมินระดับที่ 5" id="selHeaderColor5" runat="server" style="cursor:pointer; padding:0px !important; height:10px !important; border-left:0px none;">&nbsp;</td>
                                                                  
									                        </tr>
									                        </ItemTemplate>
									                        <FooterTemplate>
        									                
									                             <tfoot>
									                                <tr>
									                                    <td colspan="8" style="text-align:center; background-color:#EEEEEE; font-size:14px; font-weight:bold;">
									                                    ผลรวมการประมิน
									                                    </td>
									                                    <td colspan="2" style="text-align:center; background-color:#EEEEEE; font-size:14px; font-weight:bold;">
									                                    คะแนนที่ได้
									                                    </td>
									                                </tr>
									                                <tr>
									                                    <th colspan="3" style="text-align:center; background-color:#EEEEEE; border-bottom:1px solid #DDDDDD; font-size:14px; font-weight:bold;" id="cell_total" runat="server">
									                                   รวม
									                                    </th>
									                                      <th id="cell_sum_1" runat="server" style="text-align:center; border-bottom:1px solid #DDDDDD;; font-size:14px; font-weight:bold; vertical-align:middle;">-</th>
										                                  <th id="cell_sum_2" runat="server" style="text-align:center; border-bottom:1px solid #DDDDDD;; font-size:14px; font-weight:bold; vertical-align:middle;">-</th>
										                                  <th id="cell_sum_3" runat="server" style="text-align:center; border-bottom:1px solid #DDDDDD;; font-size:14px; font-weight:bold; vertical-align:middle;">-</th>
										                                  <th id="cell_sum_4" runat="server" style="text-align:center; border-bottom:1px solid #DDDDDD;; font-size:14px; font-weight:bold; vertical-align:middle;">-</th>
										                                  <th id="cell_sum_5" runat="server" style="text-align:center; border-bottom:1px solid #DDDDDD;; font-size:14px; font-weight:bold; vertical-align:middle;">-</th>
								                                          <th id="cell_sum_raw" runat="server" style="text-align:center; border-bottom:1px solid #DDDDDD; font-size:16px; font-weight:bold;" colspan="2">-</th>
									                                </tr>
									                                <tr>
									                                    <th colspan="8" style="background-color:#FFFFFF; border:none;"></th>
									                                    <th style="text-align:center; background-color:#EEEEEE; font-size:14px; font-weight:bold;" colspan="2" >คิดเป็น</th>									                          
									                                </tr>
									                                <tr>
									                                    <th colspan="8" style="background-color:#FFFFFF; border:none;"></th>
									                                    <th id="cell_sum_mark" runat="server" style="text-align:center; font-size:24px; font-weight:bold;" colspan="2">-</th>									                          
									                                </tr>
									                            </tfoot>
        									                
									                        </FooterTemplate>
									                    </asp:Repeater>	
									                    
									              </asp:Panel>
									        </tbody>
									        
									        
									        
								        </table>
								        
					</div>
						
				    <!-- END FORM-->  
				    </asp:Panel> 
                    <div id="divFooter" runat ="server" class="Buttonfooter">
  					<asp:Panel CssClass="form-actions" id="pnlActivity" runat="server">
                                <asp:Button ID="btnSaveRemark" runat="server"  OnClick="btnSaveRemark_Click"  CssClass="btn blue" Text="บันทึกข้อมูล"   />
								<asp:Button ID="btnPreSend" OnClick="btnPreSend_Click" runat="server" CssClass="btn green" Text="อนุมัติผลการประเมิน (หลังจากอนุมัติผลการประเมินแล้วคุณไม่สามารถแก้ไขได้)" />
								<asp:Button ID="btnSend" OnClick="btnSend_Click" runat="server" Text="" style="display:none;" />
								<asp:Button ID="btnBack" OnClick="btnBack_Click" runat="server" CssClass="btn" Text="ย้อนกลับ" />								                    
					</asp:Panel>	
                  </div>
                      
			    </div>
                
                
                	
			    </asp:Panel> 
			    
		                    <asp:Panel CssClass="modal" style="top:20%; width:400px; position:fixed;" id="ModalBehavior" runat="server" Visible="False" >
									<div class="modal-header">										
										<h3>ลำดับที่ <asp:Label ID="lblBehaviorNo" runat="server" Text="0"></asp:Label></h3>
									</div>
									<div class="modal-body">
										<div style="height:120px">
											<div class="row-fluid">
											        <asp:TextBox TextMode="MultiLine" ID="txtBehavior" runat="server"
											         CssClass="m-wrap" Width="95%" Height="110px"></asp:TextBox>														       
											</div>
										</div>
									</div>
									<div class="modal-footer">
									    <asp:Button ID="btnOK" OnClick="btnOK_Click" runat="server" CssClass="btn blue" Text="บันทึก" />
									    <asp:Button ID="btnClose" OnClick="btnClose_Click" runat="server" CssClass="btn" Text="ยกเลิก" />																				
									</div>
								</asp:Panel>


                             <%-- เลือกวิธีการพัฒนา --%>
		    <asp:Panel CssClass="modal" style="top:10%;left:40%; position:fixed; width:600px; "  id="ModalGAP" runat="server"  Visible ="false"  >
            <div style="padding: 0px; width: auto;  margin-top :0px; margin-bottom :0px; top: 0px; left: 0px;" class="fancybox-skin">
            <div class="fancybox-outer">
				<div class="modal-header">										
					<h3><asp:Label ID="lblHeader_ModalGAP" runat="server">เลือกวิธีการพัฒนา</asp:Label></h3>
					<asp:Label ID="lblTMPGAP" runat="server" style="display:none;"></asp:Label>
				</div>
				<div class="modal-body"  >					            
					<div class="row-fluid form-horizontal">
                        
                       								       
					</div>
					 <div class="portlet-body no-more-tables">
    								
							<asp:Label ID="lblCountGAP" runat="server" Font-Size="14px" Font-Bold="true"></asp:Label>
							<table class="table table-full-width  no-more-tables table-hover">
								<thead>
									<tr>
										<th style="text-align:center;"><i class="icon-caret-right"></i> รหัส</th>
										<th style="text-align:center;"><i class="icon-book"></i> หลักสูตร</th>											    
										<th style="text-align:center;"><asp:ImageButton ID="btnGAPAll"   runat="server" ImageUrl="images/none.png"  OnClick="btnGAPAll_Click" Height="24px" ToolTip="Click เพื่อเปลี่ยน" /> เลือกทั้งหมด</th>
									</tr>										    
								</thead>
								<tbody>
									<asp:Repeater ID="rptGAPDialog" OnItemCommand="rptGAPDialog_ItemCommand" OnItemDataBound="rptGAPDialog_ItemDataBound" runat="server">
									    <ItemTemplate>
    									    <tr>                                        
											    <td data-title="รหัส" style="text-align:center;"><asp:Label ID="lblNo" runat="server"></asp:Label></td>
											    <td data-title="หลักสูตร"><asp:Label ID="lblName" runat="server"></asp:Label></td>											           
											    <td data-title="ดำเนินการ" style="text-align:center;">
                                                    <asp:ImageButton ID="btnEditGAP"   runat="server" ImageUrl="images/none.png"  CommandName="Select" Height="24px" ToolTip="Click เพื่อเปลี่ยน" />

										        </td>
										    </tr>	
									    </ItemTemplate>
									</asp:Repeater>					
								</tbody>
							</table>
    								
							<asp:PageNavigation ID="PagerGAP" OnPageChanging="PagerGAP_PageChanging" MaximunPageCount="10" PageSize="5" runat="server" />
						</div>				            
				</div>
				<div class="modal-footer">
					<asp:Button ID="btnCloseGAP" OnClick="btnCloseGAP_Click" runat="server" CssClass="btn" Text="ยกเลิก" />
					<asp:Button ID="btnOKGAP" OnClick="btnOKGAP_Click" runat="server" CssClass="btn blue" Text="ตกลง" />										
				</div>

                </div>
                <a title="ปิด" id="btnModalGAP_Close"  onserverclick="btnModalGAP_Close_ServerClick" runat ="server"   class="fancybox-item fancybox-close" ></a>
            </div>
			</asp:Panel>







								<%--  GAP Handler  --%>
				                <div style="display:none;">
				                    <asp:TextBox ID="txtGAPID" runat="server" Text="0"></asp:TextBox>
				                    <asp:TextBox ID="txtGAPName" runat="server" Text="0"></asp:TextBox>
				                    <asp:Button ID="btnToggleGAP" OnClick="btnToggleGAP_Click" runat="server" />
				                </div>
</div>

</ContentTemplate>           
</asp:UpdatePanel>	

<script language="javascript">
    function toggleGAP(COMP_TYPE_ID, MASTER_NO, GAP_ID, GAP_NAME) {
        document.getElementById("ctl00_ContentPlaceHolder1_txtCOMPTypeID").value = COMP_TYPE_ID;
        document.getElementById("ctl00_ContentPlaceHolder1_txtMasterNo").value = MASTER_NO;
        document.getElementById("ctl00_ContentPlaceHolder1_txtGAPID").value = GAP_ID;
        document.getElementById("ctl00_ContentPlaceHolder1_txtGAPName").value = GAP_NAME;
        document.getElementById("ctl00_ContentPlaceHolder1_btnToggleGAP").click();
    }

</script>
			 
</asp:Content>

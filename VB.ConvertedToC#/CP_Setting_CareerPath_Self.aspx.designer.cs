using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Xml.Linq;
using System.Diagnostics;
using System.Collections.Specialized;
using System.Configuration;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Caching;
using System.Web.SessionState;
using System.Web.Security;
using System.Web.Profile;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated. 
// </auto-generated>
//------------------------------------------------------------------------------

namespace VB
{


	public partial class CP_Setting_CareerPath_Self
	{

		///<summary>
		///toolkit1 control.
		///</summary>
		///<remarks>
		///Auto-generated field.
		///To modify move field declaration from designer file to code-behind file.
		///</remarks>

		protected global::AjaxControlToolkit.ToolkitScriptManager toolkit1;
		///<summary>
		///UpdatePanel1 control.
		///</summary>
		///<remarks>
		///Auto-generated field.
		///To modify move field declaration from designer file to code-behind file.
		///</remarks>

		protected global::System.Web.UI.UpdatePanel UpdatePanel1;
		///<summary>
		///WUC_DateReporter1 control.
		///</summary>
		///<remarks>
		///Auto-generated field.
		///To modify move field declaration from designer file to code-behind file.
		///</remarks>

		protected global::VB.WUC_DateReporter WUC_DateReporter1;
		///<summary>
		///pnlEdit control.
		///</summary>
		///<remarks>
		///Auto-generated field.
		///To modify move field declaration from designer file to code-behind file.
		///</remarks>

		protected global::System.Web.UI.WebControls.Panel pnlEdit;
		///<summary>
		///div1 control.
		///</summary>
		///<remarks>
		///Auto-generated field.
		///To modify move field declaration from designer file to code-behind file.
		///</remarks>

		protected global::System.Web.UI.HtmlControls.HtmlGenericControl div1;
		///<summary>
		///lblTMP control.
		///</summary>
		///<remarks>
		///Auto-generated field.
		///To modify move field declaration from designer file to code-behind file.
		///</remarks>

		protected global::System.Web.UI.WebControls.Label lblTMP;
		///<summary>
		///lblCurrentPos control.
		///</summary>
		///<remarks>
		///Auto-generated field.
		///To modify move field declaration from designer file to code-behind file.
		///</remarks>

		protected global::System.Web.UI.WebControls.Label lblCurrentPos;
		///<summary>
		///rptAddPos control.
		///</summary>
		///<remarks>
		///Auto-generated field.
		///To modify move field declaration from designer file to code-behind file.
		///</remarks>
		private global::System.Web.UI.WebControls.Repeater withEventsField_rptAddPos;
		protected global::System.Web.UI.WebControls.Repeater rptAddPos {
			get { return withEventsField_rptAddPos; }
			set {
				if (withEventsField_rptAddPos != null) {
					withEventsField_rptAddPos.ItemCommand -= rptAddPos_ItemCommand;
					withEventsField_rptAddPos.ItemDataBound -= rptAddPos_ItemDataBound;
				}
				withEventsField_rptAddPos = value;
				if (withEventsField_rptAddPos != null) {
					withEventsField_rptAddPos.ItemCommand += rptAddPos_ItemCommand;
					withEventsField_rptAddPos.ItemDataBound += rptAddPos_ItemDataBound;
				}
			}

		}
		///<summary>
		///ModalPost control.
		///</summary>
		///<remarks>
		///Auto-generated field.
		///To modify move field declaration from designer file to code-behind file.
		///</remarks>

		protected global::System.Web.UI.WebControls.Panel ModalPost;
		///<summary>
		///btnSearchPos control.
		///</summary>
		///<remarks>
		///Auto-generated field.
		///To modify move field declaration from designer file to code-behind file.
		///</remarks>
		private global::System.Web.UI.WebControls.Button withEventsField_btnSearchPos;
		protected global::System.Web.UI.WebControls.Button btnSearchPos {
			get { return withEventsField_btnSearchPos; }
			set {
				if (withEventsField_btnSearchPos != null) {
					withEventsField_btnSearchPos.Click -= txtSearchPos_TextChanged;
				}
				withEventsField_btnSearchPos = value;
				if (withEventsField_btnSearchPos != null) {
					withEventsField_btnSearchPos.Click += txtSearchPos_TextChanged;
				}
			}

		}
		///<summary>
		///txtSearchPos control.
		///</summary>
		///<remarks>
		///Auto-generated field.
		///To modify move field declaration from designer file to code-behind file.
		///</remarks>
		private global::System.Web.UI.WebControls.TextBox withEventsField_txtSearchPos;
		protected global::System.Web.UI.WebControls.TextBox txtSearchPos {
			get { return withEventsField_txtSearchPos; }
			set {
				if (withEventsField_txtSearchPos != null) {
					withEventsField_txtSearchPos.TextChanged -= txtSearchPos_TextChanged;
				}
				withEventsField_txtSearchPos = value;
				if (withEventsField_txtSearchPos != null) {
					withEventsField_txtSearchPos.TextChanged += txtSearchPos_TextChanged;
				}
			}

		}
		///<summary>
		///lblCountPos control.
		///</summary>
		///<remarks>
		///Auto-generated field.
		///To modify move field declaration from designer file to code-behind file.
		///</remarks>

		protected global::System.Web.UI.WebControls.Label lblCountPos;
		///<summary>
		///rptPosDialog control.
		///</summary>
		///<remarks>
		///Auto-generated field.
		///To modify move field declaration from designer file to code-behind file.
		///</remarks>
		private global::System.Web.UI.WebControls.Repeater withEventsField_rptPosDialog;
		protected global::System.Web.UI.WebControls.Repeater rptPosDialog {
			get { return withEventsField_rptPosDialog; }
			set {
				if (withEventsField_rptPosDialog != null) {
					withEventsField_rptPosDialog.ItemCommand -= rptPosDialog_ItemCommand;
					withEventsField_rptPosDialog.ItemDataBound -= rptPosDialog_ItemDataBound;
				}
				withEventsField_rptPosDialog = value;
				if (withEventsField_rptPosDialog != null) {
					withEventsField_rptPosDialog.ItemCommand += rptPosDialog_ItemCommand;
					withEventsField_rptPosDialog.ItemDataBound += rptPosDialog_ItemDataBound;
				}
			}

		}
		///<summary>
		///Pager_Pos control.
		///</summary>
		///<remarks>
		///Auto-generated field.
		///To modify move field declaration from designer file to code-behind file.
		///</remarks>
		private global::VB.PageNavigation withEventsField_Pager_Pos;
		protected global::VB.PageNavigation Pager_Pos {
			get { return withEventsField_Pager_Pos; }
			set {
				if (withEventsField_Pager_Pos != null) {
					withEventsField_Pager_Pos.PageChanging -= Pager_Pos_PageChanging;
				}
				withEventsField_Pager_Pos = value;
				if (withEventsField_Pager_Pos != null) {
					withEventsField_Pager_Pos.PageChanging += Pager_Pos_PageChanging;
				}
			}

		}
		///<summary>
		///btnDeleteModel_Close control.
		///</summary>
		///<remarks>
		///Auto-generated field.
		///To modify move field declaration from designer file to code-behind file.
		///</remarks>
		private global::System.Web.UI.HtmlControls.HtmlAnchor withEventsField_btnDeleteModel_Close;
		protected global::System.Web.UI.HtmlControls.HtmlAnchor btnDeleteModel_Close {
			get { return withEventsField_btnDeleteModel_Close; }
			set {
				if (withEventsField_btnDeleteModel_Close != null) {
					withEventsField_btnDeleteModel_Close.ServerClick -= btnDeleteModel_Close_ServerClick;
				}
				withEventsField_btnDeleteModel_Close = value;
				if (withEventsField_btnDeleteModel_Close != null) {
					withEventsField_btnDeleteModel_Close.ServerClick += btnDeleteModel_Close_ServerClick;
				}
			}

		}
		///<summary>
		///ModelCompareDetail control.
		///</summary>
		///<remarks>
		///Auto-generated field.
		///To modify move field declaration from designer file to code-behind file.
		///</remarks>

		protected global::System.Web.UI.WebControls.Panel ModelCompareDetail;
		///<summary>
		///lblHeaderPos control.
		///</summary>
		///<remarks>
		///Auto-generated field.
		///To modify move field declaration from designer file to code-behind file.
		///</remarks>

		protected global::System.Web.UI.WebControls.Label lblHeaderPos;
		///<summary>
		///lblHeaderPos_Detail control.
		///</summary>
		///<remarks>
		///Auto-generated field.
		///To modify move field declaration from designer file to code-behind file.
		///</remarks>

		protected global::System.Web.UI.WebControls.Label lblHeaderPos_Detail;
		///<summary>
		///Panel1 control.
		///</summary>
		///<remarks>
		///Auto-generated field.
		///To modify move field declaration from designer file to code-behind file.
		///</remarks>

		protected global::System.Web.UI.WebControls.Panel Panel1;
		///<summary>
		///icon_Ass_KPI control.
		///</summary>
		///<remarks>
		///Auto-generated field.
		///To modify move field declaration from designer file to code-behind file.
		///</remarks>

		protected global::System.Web.UI.HtmlControls.HtmlGenericControl icon_Ass_KPI;
		///<summary>
		///lblAss_MIN_Score_Property control.
		///</summary>
		///<remarks>
		///Auto-generated field.
		///To modify move field declaration from designer file to code-behind file.
		///</remarks>

		protected global::System.Web.UI.WebControls.Label lblAss_MIN_Score_Property;
		///<summary>
		///lblAss_MIN_Score control.
		///</summary>
		///<remarks>
		///Auto-generated field.
		///To modify move field declaration from designer file to code-behind file.
		///</remarks>

		protected global::System.Web.UI.WebControls.Label lblAss_MIN_Score;
		///<summary>
		///Panel2 control.
		///</summary>
		///<remarks>
		///Auto-generated field.
		///To modify move field declaration from designer file to code-behind file.
		///</remarks>

		protected global::System.Web.UI.WebControls.Panel Panel2;
		///<summary>
		///icon_Leave control.
		///</summary>
		///<remarks>
		///Auto-generated field.
		///To modify move field declaration from designer file to code-behind file.
		///</remarks>

		protected global::System.Web.UI.HtmlControls.HtmlGenericControl icon_Leave;
		///<summary>
		///lblLeave_Property control.
		///</summary>
		///<remarks>
		///Auto-generated field.
		///To modify move field declaration from designer file to code-behind file.
		///</remarks>

		protected global::System.Web.UI.WebControls.Label lblLeave_Property;
		///<summary>
		///lblLeave control.
		///</summary>
		///<remarks>
		///Auto-generated field.
		///To modify move field declaration from designer file to code-behind file.
		///</remarks>

		protected global::System.Web.UI.WebControls.Label lblLeave;
		///<summary>
		///Panel3 control.
		///</summary>
		///<remarks>
		///Auto-generated field.
		///To modify move field declaration from designer file to code-behind file.
		///</remarks>

		protected global::System.Web.UI.WebControls.Panel Panel3;
		///<summary>
		///icon_Course control.
		///</summary>
		///<remarks>
		///Auto-generated field.
		///To modify move field declaration from designer file to code-behind file.
		///</remarks>

		protected global::System.Web.UI.HtmlControls.HtmlGenericControl icon_Course;
		///<summary>
		///lblCount_Course control.
		///</summary>
		///<remarks>
		///Auto-generated field.
		///To modify move field declaration from designer file to code-behind file.
		///</remarks>

		protected global::System.Web.UI.WebControls.Label lblCount_Course;
		///<summary>
		///rptDetail_Course control.
		///</summary>
		///<remarks>
		///Auto-generated field.
		///To modify move field declaration from designer file to code-behind file.
		///</remarks>
		private global::System.Web.UI.WebControls.Repeater withEventsField_rptDetail_Course;
		protected global::System.Web.UI.WebControls.Repeater rptDetail_Course {
			get { return withEventsField_rptDetail_Course; }
			set {
				if (withEventsField_rptDetail_Course != null) {
					withEventsField_rptDetail_Course.ItemDataBound -= rptDetail_Course_ItemDataBound;
				}
				withEventsField_rptDetail_Course = value;
				if (withEventsField_rptDetail_Course != null) {
					withEventsField_rptDetail_Course.ItemDataBound += rptDetail_Course_ItemDataBound;
				}
			}

		}
		///<summary>
		///Panel4 control.
		///</summary>
		///<remarks>
		///Auto-generated field.
		///To modify move field declaration from designer file to code-behind file.
		///</remarks>

		protected global::System.Web.UI.WebControls.Panel Panel4;
		///<summary>
		///icon_Test control.
		///</summary>
		///<remarks>
		///Auto-generated field.
		///To modify move field declaration from designer file to code-behind file.
		///</remarks>

		protected global::System.Web.UI.HtmlControls.HtmlGenericControl icon_Test;
		///<summary>
		///lblCount_Test control.
		///</summary>
		///<remarks>
		///Auto-generated field.
		///To modify move field declaration from designer file to code-behind file.
		///</remarks>

		protected global::System.Web.UI.WebControls.Label lblCount_Test;
		///<summary>
		///rptDetail_Test control.
		///</summary>
		///<remarks>
		///Auto-generated field.
		///To modify move field declaration from designer file to code-behind file.
		///</remarks>
		private global::System.Web.UI.WebControls.Repeater withEventsField_rptDetail_Test;
		protected global::System.Web.UI.WebControls.Repeater rptDetail_Test {
			get { return withEventsField_rptDetail_Test; }
			set {
				if (withEventsField_rptDetail_Test != null) {
					withEventsField_rptDetail_Test.ItemDataBound -= rptDetail_Test_ItemDataBound;
				}
				withEventsField_rptDetail_Test = value;
				if (withEventsField_rptDetail_Test != null) {
					withEventsField_rptDetail_Test.ItemDataBound += rptDetail_Test_ItemDataBound;
				}
			}

		}
		///<summary>
		///Panel5 control.
		///</summary>
		///<remarks>
		///Auto-generated field.
		///To modify move field declaration from designer file to code-behind file.
		///</remarks>

		protected global::System.Web.UI.WebControls.Panel Panel5;
		///<summary>
		///icon_Pushnish control.
		///</summary>
		///<remarks>
		///Auto-generated field.
		///To modify move field declaration from designer file to code-behind file.
		///</remarks>

		protected global::System.Web.UI.HtmlControls.HtmlGenericControl icon_Pushnish;
		///<summary>
		///lblPunishment control.
		///</summary>
		///<remarks>
		///Auto-generated field.
		///To modify move field declaration from designer file to code-behind file.
		///</remarks>

		protected global::System.Web.UI.WebControls.Label lblPunishment;
		///<summary>
		///Panel6 control.
		///</summary>
		///<remarks>
		///Auto-generated field.
		///To modify move field declaration from designer file to code-behind file.
		///</remarks>

		protected global::System.Web.UI.WebControls.Panel Panel6;
		///<summary>
		///btnSetPath control.
		///</summary>
		///<remarks>
		///Auto-generated field.
		///To modify move field declaration from designer file to code-behind file.
		///</remarks>
		private global::System.Web.UI.WebControls.Button withEventsField_btnSetPath;
		protected global::System.Web.UI.WebControls.Button btnSetPath {
			get { return withEventsField_btnSetPath; }
			set {
				if (withEventsField_btnSetPath != null) {
					withEventsField_btnSetPath.Click -= btnSetPath_Click;
				}
				withEventsField_btnSetPath = value;
				if (withEventsField_btnSetPath != null) {
					withEventsField_btnSetPath.Click += btnSetPath_Click;
				}
			}

		}
		///<summary>
		///lblTMP_POS_SetPath control.
		///</summary>
		///<remarks>
		///Auto-generated field.
		///To modify move field declaration from designer file to code-behind file.
		///</remarks>

		protected global::System.Web.UI.WebControls.Label lblTMP_POS_SetPath;
		///<summary>
		///btnDeleteModelCompare_Close control.
		///</summary>
		///<remarks>
		///Auto-generated field.
		///To modify move field declaration from designer file to code-behind file.
		///</remarks>
		private global::System.Web.UI.HtmlControls.HtmlAnchor withEventsField_btnDeleteModelCompare_Close;
		protected global::System.Web.UI.HtmlControls.HtmlAnchor btnDeleteModelCompare_Close {
			get { return withEventsField_btnDeleteModelCompare_Close; }
			set {
				if (withEventsField_btnDeleteModelCompare_Close != null) {
					withEventsField_btnDeleteModelCompare_Close.ServerClick -= btnDeleteModelCompare_Close_ServerClick;
				}
				withEventsField_btnDeleteModelCompare_Close = value;
				if (withEventsField_btnDeleteModelCompare_Close != null) {
					withEventsField_btnDeleteModelCompare_Close.ServerClick += btnDeleteModelCompare_Close_ServerClick;
				}
			}
		}
	}
}

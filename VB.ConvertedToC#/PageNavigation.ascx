﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PageNavigation.ascx.cs" Inherits="VB.PageNavigation" %>

<div class="pagenavigation"> 
<asp:UpdatePanel ID="UDPMain" runat="Server">
<ContentTemplate>
<asp:LinkButton ID="btnFirst" OnClick="btnFirst_Click" SourceName="" PageSize="20" CurrentPage="0" MaximunPageCount="4" CssClass="nextpage" runat="server" Text="< หน้าแรก" ToolTip="First Page"></asp:LinkButton>
<asp:LinkButton ID="btnBack" OnClick="btnBack_Click" runat="server" Text="< ก่อนหน้า" ToolTip="Previous Page" CssClass="lastpage"></asp:LinkButton>
<asp:Repeater ID="rptPage" OnItemCommand="rptPage_ItemCommand" OnItemDataBound="rptPage_ItemDataBound" runat="server">
    <ItemTemplate>
     <asp:LinkButton ID="btnPage" runat="server" Text="1" ToolTip="Goto Page" CssClass="number"></asp:LinkButton>
    </ItemTemplate>
</asp:Repeater>
<asp:LinkButton ID="btnNext" OnClick="btnNext_Click" runat="server" Text="ถัดไป >" ToolTip="Next Page" CssClass="nextpage"></asp:LinkButton>
<asp:LinkButton ID="btnLast" OnClick="btnLast_Click" runat="server" Text="สุดท้าย >" ToolTip="Last Page" CssClass="lastpage"></asp:LinkButton>
</ContentTemplate>
</asp:UpdatePanel>  
</div>

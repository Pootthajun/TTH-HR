using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Xml.Linq;
using System.Diagnostics;
using System.Collections.Specialized;
using System.Configuration;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Caching;
using System.Web.SessionState;
using System.Web.Security;
using System.Web.Profile;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;

using System.Drawing.Imaging;
using System.Drawing.Printing;
using System.Drawing;
namespace VB
{

	public partial class CP_View_Personal_Team : System.Web.UI.Page
	{


		HRBL BL = new HRBL();

		GenericLib GL = new GenericLib();
		protected void Page_Load(object sender, System.EventArgs e)
		{
			if ((Session["USER_PSNL_NO"] == null)) {
				ScriptManager.RegisterStartupScript(this.Page, typeof(string), "Alert", "showAlert('กรุณาลงชื่อเข้าใช้ระบบ');", true);
				Response.Redirect("Login.aspx");
				return;
			}
			if (!string.IsNullOrEmpty(Request.QueryString["PSNL_NO"])) {
				pnlList.Visible = false;
				PnlShow.Visible = true;
				btnBack.Visible = false;
				PNPS_PSNL_NO = Request.QueryString["PSNL_NO"].ToString();
				BindPersonal();
				//---------------------ผลการประเมิน-----------------------
				BindAss();
				//---------------------ประวัติการอบรม-----------------------
				BindCourse();
				//---------------------จำนวนวันลา-----------------------
				BindLEAVE();
				//---------------------ผลการสอบ-----------------------
				BindTest();
				//---------------------ประวัติการครองตำแหน่ง-----------------------
				BindHPosition();
				BindPos_Route();
				BL.BindDDlSector(ddlSector);
				return;
			}

			if (!IsPostBack) {
				ClearForm();
				BL.BindDDlYearRound(ddlRound);
				//
				BL.BindDDlSector(ddlSector);
				BindPersonal();

			}

		}

		public int R_Year {
			get {
				try {
					return GL.CINT( GL.SplitString(ddlRound.Items[ddlRound.SelectedIndex].Value, "-")[0]);
				} catch (Exception ex) {
					return 0;
				}
			}
		}

		public int R_Round {
			get {
				try {
					return GL.CINT(GL.SplitString(ddlRound.Items[ddlRound.SelectedIndex].Value, "-")[1]);
				} catch (Exception ex) {
					return 0;
				}
			}
		}

		public string PNPS_PSNL_NO {
			get { return lblName.Attributes["PNPS_PSNL_NO"]; }
			set { lblName.Attributes["PNPS_PSNL_NO"] = value; }
		}

		public string POS_NO {
			get { return lblName.Attributes["POS_NO"]; }
			set { lblName.Attributes["POS_NO"] = value; }
		}
		private void ClearForm()
		{
			PNPS_PSNL_NO = "";
			POS_NO = "";
			pnlList.Visible = true;
			PnlShow.Visible = false;
		}

		//---------------แสดงรายชื่อพนักงานภายใต้----------------------
        private void BindPersonal()
        {
            string SQL = "";
            //----------เปิดดูรายละเอียดจากหน้าอื่นๆ-------------
            if (!string.IsNullOrEmpty(PNPS_PSNL_NO))
            {

                DataTable DT = new DataTable();
                DT = BL.PersonalCareerPath(PNPS_PSNL_NO);

                if (DT.Rows.Count > 0)
                {
                    lblName.Text = "<B>" + DT.Rows[0]["PSNL_NO"].ToString() + " : " + DT.Rows[0]["PSNL_Fullname"].ToString() + "</B>";
                    lblDept.Text = DT.Rows[0]["DEPT_NAME"].ToString();
                    lblPos.Text = "<B>" + DT.Rows[0]["POS_Name"].ToString() + "</B>";
                    POS_NO = DT.Rows[0]["POS_NO"].ToString();
                    lblClass.Text = "<B>" + Convert.ToInt32(DT.Rows[0]["PNPS_CLASS"]) + "</B>";
                    switch (DT.Rows[0]["History_PUNISH"].ToString())
                    {
                        case "ไม่เคยรับโทษ":
                            lblBoxPUNISH_No.Text = " ไม่เคยโดนรับโทษ ";
                            Box_PUNISH_Yes.Visible = false;
                            Box_PUNISH_No.Visible = true;
                            break;
                        case "เคยรับโทษ":
                            lblBoxPUNISH_No.Text = " เคยรับโทษ ";
                            Box_PUNISH_Yes.Visible = false;
                            Box_PUNISH_No.Visible = true;
                            break;
                    }
                }


            }
            else
            {
                SQL = "";
                SQL += " DECLARE @R_Year As Int=" + R_Year + "\n";
                SQL += " DECLARE @R_Round As Int=" + R_Round + "\n";
                SQL += " DECLARE @ASSESSOR_CODE AS nvarchar(50)='" + Session["USER_PSNL_NO"].ToString().Replace("'", "''") + "'\n";

                SQL += " Select DISTINCT\n";
                SQL += "  Header.DEPT_CODE, Header.DEPT_Name, Header.PSNL_NO, Header.PSNL_Fullname\n";
                SQL += " ,PSN.PSN_PNPS_CLASS PNPS_CLASS\n";
                SQL += " ,Header.POS_NO,Header.POS_Name,\n";
                SQL += " Header.KPI_Status, Header.KPI_Status_Name\n";
                SQL += " ,ISNULL(History_Course.Count_Course,0)  History_Course\n";
                SQL += " ,History_PUNISH.Count_PUNISH History_PUNISH_Count\n";
                SQL += " ,CASE WHEN History_PUNISH.Count_PUNISH > 0 THEN 'เคยรับโทษ' ELSE 'ไม่เคยรับโทษ' END  History_PUNISH\n";
                SQL += " ,ISNULL(Yearly_Result.Year_Score,0) Year_Score\n";
                SQL += " ,ISNULL(Yearly_Result.Leave_All ,0) Leave_All\n";

                SQL += " FROM vw_HR_ASSESSOR_PSN PSN\n";
                SQL += " INNER JOIN vw_RPT_KPI_Status Header ON PSN.PSN_PSNL_NO=Header.PSNL_NO \n";
                SQL += " \t\t\t\t\t\tAND Header.R_Year=@R_Year AND Header.R_Round=@R_Round\n";

                SQL += " LEFT JOIN vw_PN_LEAVE LEAVE ON PSN.PSN_PSNL_NO=LEAVE.PSNL_NO\n";
                SQL += "  ----------------------History Position----------------------------------------\n";
                SQL += "  LEFT JOIN vw_Path_Count_History_Position History_Pos ON PSN.PSN_PSNL_NO=History_Pos.PSNL_NO\n";
                SQL += "  -----------------------History Course---------------------------------------\n";
                SQL += " LEFT JOIN vw_Path_Count_History_Course History_Course ON PSN.PSN_PSNL_NO=History_Course.PSNL_NO\n";
                SQL += "  -----------------------History_PUNISH---------------------------------------\n";
                SQL += " LEFT JOIN vw_Path_Count_History_PUNISH History_PUNISH ON PSN.PSN_PSNL_NO=History_PUNISH.PSNL_NO\n";
                SQL += " LEFT JOIN vw_RPT_Yearly_Result Yearly_Result  ON PSN.PSN_PSNL_NO=Yearly_Result.PSNL_NO AND Yearly_Result.R_Year =2557\n";
                SQL += "  WHERE PSN.R_Year=@R_Year AND PSN.R_Round=@R_Round \n";
                string Filter = "";

                if (ddlSector.SelectedIndex > 0)
                {
                    Filter += " AND  ( LEFT(Header.DEPT_CODE,2)='" + ddlSector.Items[ddlSector.SelectedIndex].Value + "' \n";
                    Filter += " OR LEFT(Header.DEPT_CODE,4)='" + ddlSector.Items[ddlSector.SelectedIndex].Value + "'   \n";

                    if (ddlSector.Items[ddlSector.SelectedIndex].Value == "3200")
                    {
                        Filter += " AND Header.DEPT_CODE NOT IN ('32000300'))    \n";
                    }
                    else if (ddlSector.Items[ddlSector.SelectedIndex].Value == "3203")
                    {
                        Filter += " OR Header.DEPT_CODE IN ('32000300','32030000','32030100','32030200','32030300','32030500'))  \n";
                    }
                    else if (ddlSector.Items[ddlSector.SelectedIndex].Value == "3204")
                    {
                        Filter += " OR Header.DEPT_CODE IN ('32040000','32040100','32040200','32040300','32040500','32040300'))  \n";
                    }
                    else if (ddlSector.Items[ddlSector.SelectedIndex].Value == "3205")
                    {
                        Filter += " OR Header.DEPT_CODE IN ('32050000','32050100','32050200','32050300','32050500'))  \n";
                    }
                    else
                    {
                        Filter += ")   \n";
                    }


                }

                if (!string.IsNullOrEmpty(txt_Search_Organize.Text))
                {
                    Filter += " AND (Header.DEPT_CODE LIKE '%" + txt_Search_Organize.Text.Replace("'", "''") + "%' OR \n";
                    Filter += " Header.DEPT_NAME LIKE '%" + txt_Search_Organize.Text.Replace("'", "''") + "%')\n";
                }
                if (!string.IsNullOrEmpty(txt_Search_Name.Text))
                {
                    Filter += " AND (Header.PSNL_NO LIKE '%" + txt_Search_Name.Text.Replace("'", "''") + "%'\n";
                    Filter += " OR Header.PSNL_Fullname LIKE '%" + txt_Search_Name.Text.Replace("'", "''") + "%')\n";
                }
                if (!string.IsNullOrEmpty(txt_Search_POS.Text))
                {
                    Filter += " AND (Header.POS_NO LIKE '%" + txt_Search_POS.Text.Replace("'", "''") + "%'\n";
                    Filter += " OR Header.POS_Name LIKE '%" + txt_Search_POS.Text.Replace("'", "''") + "%')\n";
                }

                if (!string.IsNullOrEmpty(Filter))
                {
                    SQL += " " + Filter + "\n";
                }

                SQL += " AND PSN.MGR_PSNL_NO=@ASSESSOR_CODE\n";
                SQL += " ORDER BY Header.DEPT_CODE,PNPS_CLASS DESC,Header.POS_NO\n";

                SqlDataAdapter DA = new SqlDataAdapter(SQL, BL.ConnectionString());
                DataTable DT = new DataTable();
                DA.SelectCommand.CommandTimeout = 90;
                DA.Fill(DT);

                Session["View_Personal"] = DT;
                Pager.SesssionSourceName = "View_Personal";
                Pager.RenderLayout();

                if (DT.Rows.Count == 0)
                {
                    lblCountPersonal.Text = "ไม่พบรายการดังกล่าว";
                }
                else
                {
                    lblCountPersonal.Text = "พบ " + GL.StringFormatNumber(DT.Rows.Count, 0) + " รายการ";
                }
            }

        }

		protected void btnSearch_Click(object sender, System.EventArgs e)
		{
			BindPersonal();
		}

		protected void Pager_PageChanging(PageNavigation Sender)
		{
			Pager.TheRepeater = rptPersonal;
		}

		string LastSector = "";

		string LastDept = "";
		protected void rptPersonal_ItemCommand(object source, System.Web.UI.WebControls.RepeaterCommandEventArgs e)
		{
			switch (e.CommandName) {
				case "View":

                    Label lblPSNDept = (Label)e.Item.FindControl("lblPSNDept");
                    Label lblPSNName = (Label)e.Item.FindControl("lblPSNName");
                    Label lblPSNPos = (Label)e.Item.FindControl("lblPSNPos");
                    Label lblTMP_Pos = (Label)e.Item.FindControl("lblTMP_Pos");
                    Label lblPSNClass = (Label)e.Item.FindControl("lblPSNClass");
                    Label lblHistory = (Label)e.Item.FindControl("lblHistory");
                    Button btnView = (Button)e.Item.FindControl("btnView");
                    Label lblHistory_PUNISH = (Label)e.Item.FindControl("lblHistory_PUNISH");
                    Label lblTMP_PSNDept = (Label)e.Item.FindControl("lblTMP_PSNDept");
					PNPS_PSNL_NO = btnView.CommandArgument;
					lblName.Text = lblPSNName.Text;
					lblDept.Text = lblTMP_PSNDept.Text;
					lblPos.Text = lblPSNPos.Text;
					POS_NO = lblTMP_Pos.Text;
					lblClass.Text = lblPSNClass.Text;
					switch (lblHistory_PUNISH.Text) {
						case " ไม่เคยโดนรับโทษ ":
							lblBoxPUNISH_No.Text = " ไม่เคยโดนรับโทษ ";
							Box_PUNISH_Yes.Visible = false;
							Box_PUNISH_No.Visible = true;
							break;
						case " เคยรับโทษ ":
							lblBoxPUNISH_Yes.Text = " เคยรับโทษทางวินัย ";
							Box_PUNISH_Yes.Visible = true;
							Box_PUNISH_No.Visible = false;
							break;
					}
					pnlList.Visible = false;
					PnlShow.Visible = true;

					//---------------------ผลการประเมิน-----------------------
					BindAss();
					//---------------------ประวัติการอบรม-----------------------
					BindCourse();
					//---------------------จำนวนวันลา-----------------------
					BindLEAVE();
					//---------------------ผลการสอบ-----------------------
					BindTest();
					//---------------------ประวัติการครองตำแหน่ง-----------------------
					BindHPosition();
					//---------------------ตำแหน่งที่สามารถเข้ารับได้-----------------------
					BindPos_Route();
					break;
			}
		}
		protected void rptPersonal_ItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
		{
			if (e.Item.ItemType != ListItemType.Item & e.Item.ItemType != ListItemType.AlternatingItem)
				return;

            Label lblPSNDept = (Label)e.Item.FindControl("lblPSNDept");
            Label lblPSNName = (Label)e.Item.FindControl("lblPSNName");
            Label lblPSNPos = (Label)e.Item.FindControl("lblPSNPos");
            Label lblTMP_Pos = (Label)e.Item.FindControl("lblTMP_Pos");
            Label lblPSNClass = (Label)e.Item.FindControl("lblPSNClass");
            Label lblHistory = (Label)e.Item.FindControl("lblHistory");
            Label lblTMP_PSNDept = (Label)e.Item.FindControl("lblTMP_PSNDept");
            Button btnView = (Button)e.Item.FindControl("btnView");
            Label lblHistory_PUNISH = (Label)e.Item.FindControl("lblHistory_PUNISH");

			string lblYear_Score = "";
			string lblLeave_Day = "";
			string lblHistory_Year = "";
			string lblHistory_Pos = "";
			string lblHistory_Course = "";

            DataRowView drv = (DataRowView)e.Item.DataItem;
			
            //lblTMP_PSNDept.Text = drv["DEPT_NAME"].ToString();
            //if (LastDept != drv["DEPT_NAME"].ToString()) {
            //    lblPSNDept.Text = drv["DEPT_NAME"].ToString();

            //    LastDept = drv["DEPT_NAME"].ToString();
            //}
            //if (!GL.IsEqualNull(drv["PSNL_Fullname"])) {
            //    lblPSNName.Text = "<B>" + drv["PSNL_NO"] + " : " + drv["PSNL_Fullname"].ToString() + "</B>";
            //} else {
            //    lblPSNName.Text = "-";
            //}
            //lblPSNPos.Text = "<B>" + drv["POS_Name"].ToString() + "</B>";
            //lblTMP_Pos.Text = drv["POS_NO"].ToString();
            //lblPSNClass.Text = "<B>" + Convert.ToInt32(drv["PNPS_CLASS"]) + "</B>";

            ////--------------------------ประวัติโดยย่อ--------------------------------------
            ////---1---
            //lblHistory_Year = "";
            ////---2---

            //lblHistory_Pos = "";
            ////---3---
            //if (GL.CINT (drv["Year_Score"]) > 0) {
            //    lblYear_Score = " ผลการประเมิน KPI/COMP ย้อนหลัง 3 รอบ ได้เฉลี่ย " + GL.StringFormatNumber(drv["Year_Score"]) + " คะแนน " + " | ";
            //} else {
            //    lblYear_Score = " ไม่มีผลการประเมิน KPI/COMP ย้อนหลัง 3 รอบ " + " | ";
            //}
            ////---4---

            ////---5---
            //if (GL.CINT (drv["History_Course"]) > 0) {
            //    lblHistory_Course = " ผ่านการฝึกอบรม " + drv["History_Course"].ToString() + " หลักสูตร " + " | ";
            //} else {
            //    lblHistory_Course = " ไม่เคยเข้าร่วมการฝึกอบรม " + " | ";
            //}
            ////---6---
            //lblHistory_PUNISH.Text = "  ";
            //switch (drv["History_PUNISH"].ToString ()) {
            //    case "ไม่เคยรับโทษ":
            //        lblHistory_PUNISH.Text = " ไม่เคยโดนรับโทษ ";
            //        lblHistory_PUNISH.ForeColor = System.Drawing.Color.Green;
            //        break;
            //    case "เคยรับโทษ":
            //        lblHistory_PUNISH.Text = " เคยรับโทษ ";
            //        lblHistory_PUNISH.ForeColor = System.Drawing.Color.Red;
            //        break;
            //}
            //lblHistory.Text = lblHistory_Year + lblHistory_Pos + lblYear_Score + lblLeave_Day + lblHistory_Course;
            //btnView.CommandArgument = drv["PSNL_NO"].ToString();

            lblTMP_PSNDept.Text = drv["DEPT_NAME"].ToString();
            if (LastDept != drv["DEPT_NAME"].ToString())
            {
                lblPSNDept.Text = drv["DEPT_NAME"].ToString();

                LastDept = drv["DEPT_NAME"].ToString();
            }
            if (!GL.IsEqualNull(drv["PSNL_Fullname"]))
            {
                lblPSNName.Text = "<B>" + drv["PSNL_NO"] + " : " + drv["PSNL_Fullname"].ToString() + "</B>";
            }
            else
            {
                lblPSNName.Text = "-";
            }
            lblPSNPos.Text = "<B>" + drv["POS_Name"].ToString() + "</B>";
            lblTMP_Pos.Text = drv["POS_NO"].ToString();
            lblPSNClass.Text = "<B>" + GL.CINT(drv["PNPS_CLASS"]) + "</B>";

            //--------------------------ประวัติโดยย่อ--------------------------------------
            //---1---
            lblHistory_Year = "";
            //---2---
            lblHistory_Pos = "";
            //---3---
            if (GL.CINT(drv["Year_Score"]) > 0)
            {
                lblYear_Score = " ผลการประเมิน KPI/COMP ย้อนหลัง 3 รอบ ได้เฉลี่ย " + GL.StringFormatNumber(GL.CDBL(drv["Year_Score"])) + " คะแนน " + " | ";
            }
            else
            {
                lblYear_Score = " ไม่มีผลการประเมิน KPI/COMP ย้อนหลัง 3 รอบ " + " | ";
            }

            if (GL.CINT(drv["History_Course"]) > 0)
            {
                lblHistory_Course = " ผ่านการฝึกอบรม " + GL.CINT(drv["History_Course"]) + " หลักสูตร " + " | ";
            }
            else
            {
                lblHistory_Course = " ไม่เคยเข้าร่วมการฝึกอบรม " + " | ";
            }
            //---6---
            lblHistory_PUNISH.Text = "  ";
            switch (drv["History_PUNISH"].ToString())
            {
                case "ไม่เคยรับโทษ":
                    lblHistory_PUNISH.Text = " ไม่เคยโดนรับโทษ ";
                    lblHistory_PUNISH.ForeColor = System.Drawing.Color.Green;
                    break;
                case "เคยรับโทษ":
                    lblHistory_PUNISH.Text = " เคยรับโทษ ";
                    lblHistory_PUNISH.ForeColor = System.Drawing.Color.Red;
                    break;
            }
            lblHistory.Text = lblHistory_Year + lblHistory_Pos + lblYear_Score + lblLeave_Day + lblHistory_Course;
            btnView.CommandArgument = drv["PSNL_NO"].ToString();
		}



		#region "Show"

		protected void btnSearch_Show_Click(object sender, System.EventArgs e)
		{
			BindCourse();
			BindLEAVE();
			BindTest();
			BindHPosition();
		}

		private void BindAss()
		{
            
            string SQL = "";
            SQL += "SELECT * FROM vw_Path_3Round_Yearly_Result \n";
			SQL += " WHERE PSNL_NO='" + PNPS_PSNL_NO + "'\n";
            SQL += " ORDER BY  R_Year DESC , R_Round DESC"; 
			SqlDataAdapter DA = new SqlDataAdapter(SQL, BL.ConnectionString());
			DataTable DT = new DataTable();
			DA.Fill(DT);

            rptAss .DataSource = DT;
            rptAss.DataBind();

            if (DT.Rows.Count > 0) {
                DT.DefaultView.RowFilter = " KPI_StatusAss =5 AND COMP_StatusAss=5";
                if (DT.DefaultView.Count > 0) {
                    double SUM_AVG = (GL.CDBL(DT.Compute("SUM(Round_Result)", " KPI_StatusAss =5 AND COMP_StatusAss=5")) / (DT.DefaultView.Count));
                    lblSUM_AVG.Text = "<B>" + GL.StringFormatNumber(SUM_AVG) + "</B>";
                }  else {
                    lblSUM_AVG.Text = "<B>" + GL.StringFormatNumber(0) + "</B>";
                }

            }
            

		}

        protected void rptAss_ItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType != ListItemType.Item & e.Item.ItemType != ListItemType.AlternatingItem)
                return;
            Label lblYear = (Label)e.Item.FindControl("lblYear");
            Label lblKPI_Result = (Label)e.Item.FindControl("lblKPI_Result");
            Label lblCOMP_Result = (Label)e.Item.FindControl("lblCOMP_Result");
            Label lblSUM_Result = (Label)e.Item.FindControl("lblSUM_Result");

            DataRowView drv = (DataRowView)e.Item.DataItem;

            lblYear.Text = "<B>" + "ปี " + drv["R_Year"] + " รอบ " + drv["R_Round"] + "</B>" + "  ( KPI " + drv["Weight_KPI"] + "% : COMP " + drv["Weight_COMP"] + "% )";

            if (GL.CINT(drv["KPI_StatusAss"]) == 5 && GL.CINT(drv["COMP_StatusAss"]) == 5)
            { 
             lblKPI_Result.Text = GL.StringFormatNumber(GL.CDBL(drv["KPI_Result"]));
             lblCOMP_Result.Text = GL.StringFormatNumber(GL.CDBL(drv["COMP_Result"]));
             lblSUM_Result.Text = GL.StringFormatNumber(GL.CDBL(drv["Ass_Score"]));
            }else {
                lblKPI_Result.Text = "-";
                lblCOMP_Result.Text = "-";
                lblSUM_Result.Text = "-";
            }

        }

		protected void Page_Course_PageChanging(PageNavigation Sender)
		{
			Page_Course.TheRepeater = rptCourse;
		}
		private void BindCourse()
		{
			string SQL = "";
			SQL += " SELECT  * FROM vw_Path_Course \n";
			SQL += " WHERE PNPS_PSNL_NO='" + PNPS_PSNL_NO + "'\n";
			SQL += " AND ENTRY_DAY > 0\n";
			if (!string.IsNullOrEmpty(txt_Search_Course.Text)) {
				SQL += " AND ( COURSE_DESC LIKE '%" + txt_Search_Course.Text.Replace("'", "''") + "%')\n";
			}
			SQL += " ORDER BY COURSE_DESC";
			SqlDataAdapter DA = new SqlDataAdapter(SQL, BL.ConnectionString());
			DataTable DT = new DataTable();
			DA.SelectCommand.CommandTimeout = 90;
			DA.Fill(DT);

			if (DT.Rows.Count > 20) {
				Search_Course.Visible = true;
			}

			if (DT.Rows.Count > 0) {
				lblCountCourse.Text = " พบ " + Conversion.Val(DT.Rows.Count) + " หลักสูตร";
				table_Course.Visible = true;
			} else {
				lblCountCourse.Text = " ไม่พบประวัติการฝึกอบรมที่เข้าร่วม ";
				table_Course.Visible = false;
			}
			Session["View_Personal_Course"] = DT;
			Page_Course.SesssionSourceName = "View_Personal_Course";
			Page_Course.RenderLayout();

		}
		protected void rptCourse_ItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
		{
			if (e.Item.ItemType != ListItemType.Item & e.Item.ItemType != ListItemType.AlternatingItem)
				return;
            Label lblCourse = (Label)e.Item.FindControl("lblCourse");
            Label lblDateFrom = (Label)e.Item.FindControl("lblDateFrom");
            Label lblDateTo = (Label)e.Item.FindControl("lblDateTo");

            DataRowView drv = (DataRowView)e.Item.DataItem;
			
			lblCourse.Text = drv["COURSE_DESC"].ToString();
			if (GL.CINT (drv["ENTRY_DAY"]) > 0) {
				lblDateFrom.Text = GL.CINT(drv["ENTRY_DAY"]).ToString ();
				//& " วัน"
			} else {
				lblDateFrom.Text = " ไม่เข้าอบรม";
			}
			lblDateTo.Text = "";
		}

		private void BindLEAVE()
		{
			string SQL = " ";

			SQL += " Select DISTINCT\n";
			SQL += " KPI_1.R_Year, KPI_1.SECTOR_CODE, KPI_1.SECTOR_NAME, KPI_1.DEPT_CODE, KPI_1.DEPT_NAME\n";
			SQL += " ,KPI_1.PSNL_NO,KPI_1.PSNL_Fullname,KPI_1.POS_Name,KPI_1.PNPS_CLASS,KPI_1.WAGE_TYPE,KPI_1.WAGE_NAME,KPI_1.POS_No\n";

			SQL += " ,ISNULL(SUM(L_Day),0) Leave_Day\n";
			SQL += " ,- CASE WHEN ISNULL(SUM(L_Day),0)<=10 THEN 0\n";
			SQL += "  --หักคะแนนเพิ่มเป็นวันละ 2.5--\n";
			SQL += "  WHEN SUM(L_Day)>10 AND SUM(L_Day)<=30 THEN 2.5*(SUM(L_Day)-10)\n";
			SQL += "  ELSE 50 END\n";
			SQL += "    Leave_Score \n";
			SQL += " FROM vw_RPT_KPI_Result KPI_1  \n";
			SQL += " /**/RIGHT JOIN tb_HR_Round R1 ON R1.R_Year=KPI_1.R_Year AND R1.R_Round=1\n";
			SQL += " LEFT JOIN tb_HR_Round R2 ON R2.R_Year=R1.R_Year AND R2.R_Round=2  \n";
			SQL += " LEFT JOIN vw_PN_LEAVE LEAVE ON KPI_1.PSNL_NO=LEAVE.PSNL_NO\n";
			SQL += " \t\t\t\tAND L_YYMM BETWEEN RIGHT(Year(R1.R_Start)+543,2)+CASE WHEN MONTH(R1.R_Start)<10 THEN '0' ELSE '' END+CAST(MONTH(R1.R_Start) AS nvarchar)\n";
			SQL += " \t\t\t\t\t\t\tAND ISNULL(RIGHT(Year(R2.R_End)+543,2),'99')+ISNULL(CASE WHEN MONTH(R2.R_End)<10 THEN '0' ELSE '' END+CAST(MONTH(R2.R_End) AS nvarchar),'99')\n";
			SQL += "  WHERE KPI_1.R_Round = 1 \n";
			SQL += " AND KPI_1 .PSNL_NO ='" + PNPS_PSNL_NO + "'\n";
			SQL += " AND KPI_1 .R_Year =2557 \n";
			SQL += " GROUP BY KPI_1.R_Year,KPI_1.R_Round,KPI_1.SECTOR_CODE,KPI_1.SECTOR_NAME,KPI_1.DEPT_CODE,KPI_1.DEPT_NAME,KPI_1.PSNL_NO,KPI_1.PSNL_Fullname,KPI_1.POS_Name,KPI_1.PNPS_CLASS,KPI_1.WAGE_TYPE,KPI_1.WAGE_NAME,KPI_1.POS_No\n";
			SQL += " ,KPI_1.Ass_Status,KPI_1.RESULT\n";
			SqlDataAdapter DA = new SqlDataAdapter(SQL, BL.ConnectionString());
			DataTable DT = new DataTable();
			DA.SelectCommand.CommandTimeout = 90;
			DA.Fill(DT);
			if (DT.Rows.Count > 0) {
				lblL_HEALTH.Text = GL.StringFormatNumber(GL.CDBL(DT.Rows[0]["Leave_Day"]));
				
			} else {
                lblL_HEALTH.Text = GL.StringFormatNumber(0);
				
			}
		}

		protected void Page_Test_PageChanging(PageNavigation Sender)
		{
			Page_Test.TheRepeater = rptTest;
		}
		private void BindTest()
		{
			string SQL = " ";
			SQL += " SELECT  * FROM vw_Path_Test\n";
			SQL += " WHERE CPSP_PERSONAL_NO='" + PNPS_PSNL_NO + "'\n";
			if (!string.IsNullOrEmpty(txtSearch_test.Text)) {
				SQL += " AND ( CPSM_SUBJECT_NAME LIKE '%" + txtSearch_test.Text.Replace("'", "''") + "%')\n";
			}
			SQL += " ORDER BY  CPSP_EXAM_DATE DESC,CPSM_SUBJECT_NAME";
			SqlDataAdapter DA = new SqlDataAdapter(SQL, BL.ConnectionString());
			DataTable DT = new DataTable();
			DA.SelectCommand.CommandTimeout = 90;
			DA.Fill(DT);

			if (DT.Rows.Count > 20) {
				Search_Test.Visible = true;
			}

			if (DT.Rows.Count > 0) {
				lblCount_Test.Text = " พบ " + Conversion.Val(DT.Rows.Count) + " วิชา";
				table_Test.Visible = true;
			} else {
				lblCount_Test.Text = " ไม่พบประวัติการสอบวัดผล";
				table_Test.Visible = false;
			}
			Session["View_Personal_Test"] = DT;
			Page_Test.SesssionSourceName = "View_Personal_Test";
			Page_Test.RenderLayout();
		}
		protected void rptTest_ItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
		{
			if (e.Item.ItemType != ListItemType.Item & e.Item.ItemType != ListItemType.AlternatingItem)
				return;
            Label lblSubject = (Label)e.Item.FindControl("lblSubject");
            Label lblDate = (Label)e.Item.FindControl("lblDate");
            Label lblPOINT = (Label)e.Item.FindControl("lblPOINT");
            Label lblPOINT_RULE = (Label)e.Item.FindControl("lblPOINT_RULE");
            Label lblScore = (Label)e.Item.FindControl("lblScore");

            DataRowView drv = (DataRowView)e.Item.DataItem;
			
			lblSubject.Text = drv["CPSM_SUBJECT_NAME"].ToString();
          

            if (GL.IsEqualNull(drv["CPSP_EXAM_DATE"]))  {
                lblDate.Text = "-";
            }
            else{
                lblDate.Text = BL.DateTimeToThaiDate((DateTime)drv["CPSP_EXAM_DATE"]);
            }
            if (GL.CINT(drv["CPSP_POINT"]) > 0){
                lblScore.Text = GL.StringFormatNumber(GL.CDBL(drv["CPSP_POINT"]));
            }else{
                lblScore.Text = GL.StringFormatNumber(0);
            }
            if (GL.CINT(drv["CPSM_SUBJECT_POINT"]) > 0){
                lblPOINT.Text = GL.StringFormatNumber(GL.CDBL(drv["CPSM_SUBJECT_POINT"]));
            }else{
                lblPOINT.Text = GL.StringFormatNumber(0);
            }
            if (GL.CINT(drv["CPSM_SUBJECT_POINT_RULE"]) > 0){
                lblPOINT_RULE.Text = GL.StringFormatNumber(GL.CDBL(drv["CPSM_SUBJECT_POINT_RULE"]));
            }else{
                lblPOINT_RULE.Text = GL.StringFormatNumber(0);
            }

		}

		protected void Page_HPosition_PageChanging(PageNavigation Sender)
		{
			Page_HPosition.TheRepeater = rptHPosition;
		}
		private void BindHPosition()
		{
			string SQL = " ";
            SQL += "  SELECT PSNL_NO ,PSNL_Fullname ,PNHH_POS_NAME ,PNHH_EDU_NAME ,ISNULL(PNHH_EFFECT_STR,PNHH_DATE_END) PNHH_EFFECT_STR ,ISNULL(PNHH_DATE_END,PNHH_EFFECT_STR) PNHH_DATE_END FROM vw_Path_HPosition \n";
            SQL += " WHERE PSNL_NO='" + PNPS_PSNL_NO + "'\n";
			if (!string.IsNullOrEmpty(txtSearch_HPostion.Text)) {
				SQL += " AND ( PNHH_POS_NAME LIKE '%" + txtSearch_HPostion.Text.Replace("'", "''") + "%')\n";
			}
            SQL += "  ORDER BY PSNL_NO,PNHH_DATE_END DESC ,PNHH_EFFECT_STR  DESC, PNHH_POS_NAME \n";
			SqlDataAdapter DA = new SqlDataAdapter(SQL, BL.ConnectionString());
			DA.SelectCommand.CommandTimeout = 90;
			DataTable DT = new DataTable();
			DA.Fill(DT);

			if (DT.Rows.Count > 20) {
				Search_HPosition.Visible = true;
			}

			if (DT.Rows.Count > 0) {
				lblCount_HPosition.Text = " พบ " + Conversion.Val(DT.Rows.Count) + " ตำแหน่ง";
				table_HPosition.Visible = true;
			} else {
				lblCount_HPosition.Text = " ไม่พบประวัติการครองตำแหน่ง";
				table_HPosition.Visible = false;
			}
			Session["View_Personal_HPosition"] = DT;
			Page_HPosition.SesssionSourceName = "View_Personal_HPosition";
			Page_HPosition.RenderLayout();
		}
		protected void rptHPosition_ItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
		{
			if (e.Item.ItemType != ListItemType.Item & e.Item.ItemType != ListItemType.AlternatingItem)
				return;
            Label lblPosition = (Label)e.Item.FindControl("lblPosition");
            Label lblDateFrom = (Label)e.Item.FindControl("lblDateFrom");
            Label lblDateTo = (Label)e.Item.FindControl("lblDateTo");

            DataRowView drv = (DataRowView)e.Item.DataItem;
			
			lblPosition.Text = drv["PNHH_POS_NAME"].ToString();

            if (drv["PNHH_EFFECT_STR"].ToString() == drv["PNHH_DATE_END"].ToString())
            {
                lblDateTo.Text = BL.DateTimeToThaiDate((DateTime)drv["PNHH_EFFECT_STR"]);
                lblDateFrom.Text = "";
            }
            else
            {
                if (!GL.IsEqualNull(drv["PNHH_EFFECT_STR"]))
                {
                    lblDateTo.Text = BL.DateTimeToThaiDate((DateTime)drv["PNHH_EFFECT_STR"]);
                }
                else
                {
                    lblDateTo.Text = "";
                }
                if (!GL.IsEqualNull(drv["PNHH_DATE_END"]) && !GL.IsEqualNull(drv["PNHH_EFFECT_STR"]))
                {
                    lblDateFrom.Text = " - " + BL.DateTimeToThaiDate((DateTime)drv["PNHH_DATE_END"]);
                }
                else if (!GL.IsEqualNull(drv["PNHH_DATE_END"]) && GL.IsEqualNull(drv["PNHH_EFFECT_STR"]))
                {
                    lblDateFrom.Text = BL.DateTimeToThaiDate((DateTime)drv["PNHH_DATE_END"]);
                }
                else
                {
                    lblDateFrom.Text = "";
                }
            }


		}

		private void BindPos_Route()
		{
			string SQL = "";
			SQL += "  SELECT From_POS_No , To_POS_No\n";
			SQL += "  ,vw_PN_Position.POS_NO\n";
			SQL += "  ,vw_PN_Position.DEPT_NAME\n";
			SQL += "  ,vw_PN_Position.PNPO_CLASS\n";
			SQL += "  ,vw_PN_Position.MGR_NAME\n";
			SQL += "  FROM  tb_Path_Setting_Route Setting_Route\n";
			SQL += "  LEFT JOIN tb_Path_Setting Path_Setting  ON Path_Setting.POS_No=Setting_Route.From_POS_No\n";
			SQL += "  INNER JOIN vw_PN_Position ON vw_PN_Position.POS_NO=Setting_Route.To_POS_No\n";
			SQL += "  WHERE From_POS_No ='" + POS_NO + "'\n";
			SqlDataAdapter DA = new SqlDataAdapter(SQL, BL.ConnectionString());
			DataTable DT = new DataTable();
			DA.SelectCommand.CommandTimeout = 90;
			DA.Fill(DT);
			if (DT.Rows.Count > 0) {
				lblCountPos_Route.Text = "พบ " + DT.Rows.Count + " รายการ";
				table_Pos_Route.Visible = true;
			} else {
				lblCountPos_Route.Text = " ไม่พบตำแหน่งที่สามารถเข้ารับได้ ณ ขณะนี้";
				table_Pos_Route.Visible = false;
			}
			rptPos_Route.DataSource = DT;
			rptPos_Route.DataBind();
		}


		string LastDept_Route = "";
		protected void rptPos_Route_ItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
		{
			if (e.Item.ItemType != ListItemType.Item & e.Item.ItemType != ListItemType.AlternatingItem)
				return;
			Label lblDept =(Label) e.Item.FindControl("lblDept");
            Label lblPosName = (Label)e.Item.FindControl("lblPosName");
			Label lblClass = (Label) e.Item.FindControl("lblClass");

            DataRowView drv = (DataRowView)e.Item.DataItem;
			
			if (LastDept != drv["DEPT_NAME"].ToString()) {
				lblDept.Text = drv["DEPT_NAME"].ToString();
				LastDept = drv["DEPT_NAME"].ToString();
			}

			lblPosName.Text = drv["POS_NO"] + " : " + drv["MGR_NAME"].ToString();
			lblClass.Text = GL.CINT (drv["PNPO_CLASS"]).ToString ();

		}

		#endregion


		protected void btnBack_Click(object sender, System.EventArgs e)
		{
			pnlList.Visible = true;
			PnlShow.Visible = false;
			ClearForm();
			if (!string.IsNullOrEmpty(Request.QueryString["POS_NO"])) {
				//Response.Redirect("CP_View_Personal.aspx")
				BindPersonal();
			}

		}
		public CP_View_Personal_Team()
		{
			Load += Page_Load;
		}






	}
}

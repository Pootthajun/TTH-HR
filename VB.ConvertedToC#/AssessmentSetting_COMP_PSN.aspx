﻿<%@ Page  Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPage.Master" CodeBehind="AssessmentSetting_COMP_PSN.aspx.cs" Inherits="VB.AssessmentSetting_COMP_PSN" %>

<%@ Register src="WUC_DateReporter.ascx" tagname="WUC_DateReporter" tagprefix="uc1" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<%@ Register src="PageNavigation.ascx" tagname="PageNavigation" tagprefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">



</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">      
<ContentTemplate>
<asp:ToolkitScriptManager ID="toolkit1" runat="server"></asp:ToolkitScriptManager>

<div class="container-fluid">
				<!-- BEGIN PAGE HEADER-->
				<div class="row-fluid" id="fixedPageHeader">
					<div class="span12"> 
						<!-- BEGIN PAGE TITLE & BREADCRUMB-->
						<h3 class="page-user-md">การประเมินสมรรถนะรายบุคคล  (Competency) <font color="blue">(ScreenID : S-HDR-09)</font>
                        <asp:Label ID="lblReportTime" Font-Size="14px" ForeColor="Green" runat="Server"></asp:Label>
                        </h3>					
									
						<ul class="breadcrumb">
                            <li>
                        	    <i class="icon-user"></i> <a href="javascript:;">การประเมินพนักงานรายบุคคล</a><i class="icon-angle-right"></i>
                        	</li>
                        	<li>
                        	    <i class="icon-th-list"></i> <a href="javascript:;">การประเมินสมรรถนะ (Competency)</a>
                        	</li>
                        
                        	
                            <uc1:WUC_DateReporter ID="WUC_DateReporter1" runat="server" />
                        </ul>
						<!-- END PAGE TITLE & BREADCRUMB-->
									        
				     </div>

				
			    </div>
		        <!-- END PAGE HEADER-->
				
				<asp:Panel ID="pnlList" runat="server" Visible="True">
                    <div class="row-fluid">
					
					    <div class="span12">
						    <!-- BEGIN SAMPLE TABLE PORTLET-->
							                   	<asp:Panel CssClass="row-fluid form-horizontal" ID="pnlSearch" runat="server" DefaultButton="btnSearch">
												     
												     <div class="row-fluid form-horizontal">
												          <div class="span4 ">
														    <div class="control-group">
													            <label class="control-label"><i class="icon-retweet"></i> สำหรับรอบการประเมิน</label>
													            <div class="controls">
														            <asp:DropDownList ID="ddlRound" OnSelectedIndexChanged="ddlRound_SelectedIndexChanged" runat="server" AutoPostBack="true" CssClass="medium m-wrap">
														            </asp:DropDownList>
														            <asp:Button ID="btnSearch" OnClick="Search" runat="server" Style="display:none" />
														        </div>
												            </div>
													    </div>	
													    <div class="span6 ">
														    <div class="control-group">
													            <label class="control-label"><i class="icon-user"></i> ชื่อ</label>
													            <div class="controls">
														            <asp:TextBox ID="txt_Search_Name" OnTextChanged="Search" runat="server" AutoPostBack="true" CssClass="m-wrap medium" placeholder="ค้นหาจากชื่อ/เลขประจำตัว/ตำแหน่ง"></asp:TextBox>
													            </div>
												            </div>
													    </div>	
												     </div>
												    <div class="row-fluid form-horizontal">
												        <div class="span4 ">
													        <div class="control-group">
													            <label class="control-label"><i class="icon-sitemap"></i> ฝ่าย</label>
													            <div class="controls">
														            <asp:DropDownList ID="ddlSector" OnSelectedIndexChanged="Search"  runat="server" AutoPostBack="true" CssClass="medium m-wrap">
														            </asp:DropDownList>
														        </div>
												            </div>
													    </div>
													     <div class="span6 ">
														    <div class="control-group">
													            <label class="control-label"><i class="icon-sitemap"></i> ชื่อหน่วยงาน</label>
													            <div class="controls">
														            <asp:TextBox ID="txt_Search_Organize" OnTextChanged="Search" runat="server" AutoPostBack="true" CssClass="m-wrap medium" placeholder="ค้นหาจากหน่วยงาน/ตำแหน่ง"></asp:TextBox>
														        </div>
												            </div>
													    </div>													    
												    </div>
												    <div class="row-fluid form-horizontal">
												        <div class="span4 ">
													        <div class="control-group">
													            <label class="control-label"><i class="icon-list-ol"></i> กลุ่มระดับ</label>
													            <div class="controls">
														            <asp:DropDownList ID="ddl_Search_Class" OnSelectedIndexChanged="Search" runat="server" AutoPostBack="true" CssClass="medium m-wrap">
														            </asp:DropDownList>
														        </div>
												            </div>
													    </div>
												        <div class="span6 ">
														    <div class="control-group">
													            <label class="control-label"><i class="icon-bolt"></i> ความคืบหน้า</label>
													            <div class="controls">
														            <asp:DropDownList ID="ddlStatus" OnSelectedIndexChanged="Search" runat="server" AutoPostBack="true" CssClass="medium m-wrap">
														                <asp:ListItem Text="ทั้งหมด" Value="-1" Selected="true"></asp:ListItem>
														                <asp:ListItem Text="ยังไม่ส่งแบบประเมิน" Value="0"></asp:ListItem>
					                                                    <asp:ListItem Text="รออนุมัติแบบประเมิน" Value="1"></asp:ListItem>
					                                                    <asp:ListItem Text="อนุมัติแบบประเมินแล้ว" Value="2"></asp:ListItem>
					                                                    <asp:ListItem Text="พนักงานประเมินตนเอง" Value="3"></asp:ListItem>
					                                                    <asp:ListItem Text="รออนุมัติผลการประเมิน" Value="4"></asp:ListItem>
					                                                    <asp:ListItem Text="ประเมินเสร็จสมบูรณ์" Value="5"></asp:ListItem>
														            </asp:DropDownList>
														        </div>
												            </div>
													    </div>				
												    </div>
																																			   
												</asp:Panel>
								            
							<div class="portlet-body no-more-tables">
								<asp:Label ID="lblCountPSN" runat="server" Font-Size="14px" Font-Bold="true"></asp:Label>
								<table class="table table-full-width table-advance dataTable no-more-tables table-hover">
									<thead>
										<tr>
											<th><i class="icon-sitemap"></i> ฝ่าย</th>
											<th><i class="icon-sitemap"></i> หน่วยงาน</th>
											<th><i class="icon-user"></i> ชื่อ</th>
											<th><i class="icon-briefcase"></i> ตำแหน่ง</th>
											<th><i class="icon-bookmark"></i> ระดับ</th>
											<th><i class="icon-briefcase"></i> ประเภท</th>
											<th><i class="icon-user-md"></i> ผู้ประเมิน</th>
											<th><i class="icon-info"></i> สถานะ</th>
											<th><i class="icon-bolt"></i> ดำเนินการ</th>
										</tr>
									</thead>
									<tbody>
										<asp:Repeater ID="rptCOMP" OnItemCommand="rptCOMP_ItemCommand" OnItemDataBound="rptCOMP_ItemDataBound" runat="server">
									        <ItemTemplate>
									            <tr>                                        
											        <td data-title="ฝ่าย"><asp:Label ID="lnkPSNSector" runat="server"></asp:Label></td>
											        <td data-title="หน่วยงาน"><asp:Label ID="lnkPSNDept" runat="server"></asp:Label></td>
											        <td data-title="ชื่อ"><asp:Label ID="lblPSNName" runat="server"></asp:Label></td>
											        <td data-title="ตำแหน่ง"><asp:Label ID="lblPSNPos" runat="server"></asp:Label></td>
											        <td data-title="ระดับ"><asp:Label ID="lblPSNClass" runat="server"></asp:Label></td>
											        <td data-title="ประเภท"><asp:Label ID="lblPSNType" runat="server"></asp:Label></td>
											        <td data-title="ผู้ประเมิน"><asp:Label ID="lblPSNASSESSOR" runat="server"></asp:Label></td>
											        <td data-title="สถานะ"><asp:Label ID="lblPSNCOMPStatus" runat="server"></asp:Label></td>
											        <td data-title="ดำเนินการ">
											            <asp:Button ID="btnPSNEdit" runat="server" CssClass="btn mini blue" CommandName="Edit" Text="การประเมิน" />											           
										            </td>
										        </tr>	
									        </ItemTemplate>
										</asp:Repeater>
																							
									</tbody>
								</table>
								<asp:PageNavigation ID="Pager" OnPageChanging="Pager_PageChanging" MaximunPageCount="10" PageSize="20" runat="server" />
							</div>
						
						<!-- END TABLE PORTLET-->
						
					</div>
                </div>
              </asp:Panel>
				
				
			<asp:Panel ID="pnlEdit" runat="server" Visible="False">
			                     <div class="form-horizontal form-view">
                			             <div class="btn-group pull-left">
                                                <asp:Button ID="btnBack_TOP" OnClick="btnBack_TOP_Click" runat="server" CssClass="btn" Text="ย้อนกลับ" />
                                          </div>
                			            <div class="btn-group pull-right">                                               
			                                <a data-toggle="dropdown" class="btn  dropdown-toggle">พิมพ์ <i class="icon-angle-down"></i></a>										
			                                <ul class="dropdown-menu pull-right">                                                       
                                                <li><a id="btnCOMPFormPDF" runat="server" target="_blank">ใบประเมิน รูปแบบ PDF</a></li>
                                                <li><a id="btnCOMPFormExcel" runat="server" target="_blank">ใบประเมิน รูปแบบ Excel</a></li>	
                                                <li><a id="btnIDPFormPDF" runat="server" target="_blank">แผนพัฒนารายบุคคล รูปแบบ PDF</a></li>
                                                <li><a id="btnIDPFormExcel" runat="server" target="_blank">แผนพัฒนารายบุคคล รูปแบบ Excel</a></li>	
                                                <li><a id="btnIDPResultFormPDF" runat="server" target="_blank">ผลการพัฒนาตามแผนพัฒนารายบุคคล รูปแบบ PDF</a></li>
                                                <li><a id="btnIDPResultFormExcel" runat="server" target="_blank">ผลการพัฒนาตามแผนพัฒนารายบุคคล รูปแบบ Excel</a></li>
                                                <li><a id="btnIDPRemark_Plan" href="Doc/RemarkIDP/Remark_PlanIDP.pdf" runat="server" target="_blank">Download คำชี้แจง แผนการพัฒนารายบุคคล</a></li>
                                                <li><a id="btnIDPRemark_Result" href="Doc/RemarkIDP/Remark_ResultIDP.pdf" runat="server" target="_blank">Download คำชี้แจง ผลการพัฒนาตามแผนพัฒนารายบุคคล</a></li>				                                </ul>
                                            
		                                </div>
                                        <%--<asp:Panel CssClass="form-actions" id="Panel1" runat="server">
								               <asp:Button ID="Button1" runat="server" CssClass="btn" Text="ย้อนกลับ" />
								        </asp:Panel>--%>

                			            
                			            <h3 style="text-align:center; margin-top:0px;">
					                      สถานะความคืบหน้า (Competency) <asp:Label ID="lblRound" runat="server"></asp:Label> : 
					                      <asp:DropDownList ID="ddlCOMPStatus" OnSelectedIndexChanged="ddlCOMPStatus_SelectedIndexChanged" runat="server" AutoPostBack="true"
					                      style="background-color:White; border:none; color:Black; font-size:24px; padding-top:0px; height:35px; width:280px;">
					                        <asp:ListItem Text="ไม่พบแบบประเมิน" Value="-1"></asp:ListItem>
					                        <asp:ListItem Text="ยังไม่ส่งแบบประเมิน" Value="0"></asp:ListItem>
					                        <asp:ListItem Text="รออนุมัติแบบประเมิน" Value="1"></asp:ListItem>
					                        <asp:ListItem Text="อนุมัติแบบประเมินแล้ว" Value="2"></asp:ListItem>
					                        <asp:ListItem Text="พนักงานประเมินตนเอง" Value="3"></asp:ListItem>
					                        <asp:ListItem Text="รออนุมัติผลการประเมิน" Value="4"></asp:ListItem>
					                        <asp:ListItem Text="ประเมินเสร็จสมบูรณ์" Value="5"></asp:ListItem>
					                      </asp:DropDownList>
					                      <font style="font-style:italic; color:Silver;">(เลือกเพื่อปรับสถานะ)</font>
					                    </h3>  
    			                     
    			                     <div class="row-fluid" style="border-bottom:1px solid #EEEEEE;">
				                        <div class="span6 ">
				                            <div class="control-group">
							                    <label class="control-label" style="width:100px;">ประเภท :</label>
							                    <div class="controls" style="margin-left: 120px;">
								                    <asp:Label ID="lblPSNType" runat="server" CssClass="text bold"></asp:Label>
							                    </div>
						                    </div>
				                        </div>					        
					                  </div>
					                     
    			                     <div class="row-fluid" style="border-bottom:1px solid #EEEEEE;">
					                        <div class="span4 ">
					                            <div class="control-group">
								                    <label class="control-label" style="width:100px;">ผู้รับการประเมิน :</label>
								                    <div class="controls" style="margin-left: 120px;">
									                    <asp:Label ID="lblPSNName" runat="server" CssClass="text bold"></asp:Label>
								                    </div>
							                    </div>
					                        </div>
					                        <div class="span3 ">
					                            <div class="control-group">
								                    <label class="control-label" style="width:100px;">ตำแหน่ง :</label>
								                    <div class="controls" style="margin-left: 120px;">
									                    <asp:Label ID="lblPSNPos" runat="server" CssClass="text bold"></asp:Label>
								                    </div>
							                    </div>
					                        </div>
					                        <div class="span5 ">
					                            <div class="control-group">
								                    <label class="control-label" style="width:100px;">สังกัด :</label>
								                    <div class="controls" style="margin-left: 120px;">
									                    <asp:Label ID="lblPSNDept" runat="server" CssClass="text bold"></asp:Label>
								                    </div>
							                    </div>
					                        </div>
					                     </div>
                					     
					                      <div class="row-fluid" style="margin-top:10px;">
					                        <div class="span4 ">
					                            <div class="control-group">
								                    <label class="control-label" style="width:100px;">ผู้ประเมิน :
								                    <asp:LinkButton ID="btn_Fixed_Assessor" OnClick="btn_Fixed_Assessor_Click" runat="server" CssClass="btn red mini"><i class="icon-screenshot"></i> เพิ่มเติม</asp:LinkButton>
								                    </label>
								                    <div class="controls" style="margin-left: 120px;">
									                    <asp:DropDownList ID="ddlASSName" OnSelectedIndexChanged="ddlASSName_SelectedIndexChanged" runat="Server" AutoPostBack="True" style="font-weight:bold; color:Black; border:none;">									        
									                    </asp:DropDownList>
									                </div>
							                    </div>
					                        </div>
					                        <div class="span3 ">
					                            <div class="control-group">
								                    <label class="control-label" style="width:100px;">ตำแหน่ง :</label>
								                    <div class="controls" style="margin-left: 120px;">
									                    <asp:Label ID="lblASSPos" runat="server" CssClass="text bold"></asp:Label>
								                    </div>
							                    </div>
					                        </div>
					                        <div class="span5 ">
					                            <div class="control-group">
								                    <label class="control-label" style="width:100px;">สังกัด :</label>
								                    <div class="controls" style="margin-left: 120px;">
									                    <asp:Label ID="lblASSDept" runat="server" CssClass="text bold"></asp:Label>
								                    </div>
							                    </div>
					                        </div>
					                     </div>
    			                </div>
    			            
    			             <!-- BEGIN FORM   background-color:Transparent; 
										                              border:0px none transparent; color:black;   -->
				            <div class="portlet-body no-more-tables">
				                        <table class="table table-bordered no-more-tables" style="background-color:White;">
									        <thead>
										        <tr>
											        <th rowspan="2" style="text-align:center; background-color:Silver;"> ลำดับ</th>
											        <th rowspan="2" style="text-align:center; background-color:Silver;"> สมรรถนะ</th>
											        <th rowspan="2" style="text-align:center; background-color:Silver;"> มาตรฐานพฤติกรรม</th>
											        <th colspan="5" style="text-align:center; background-color:Silver;"> คะแนนตามระดับค่าเป้าหมาย</th>
											        <th rowspan="2" style="text-align:center; background-color:Silver;"> คะแนน</th>
											        <th rowspan="2" style="text-align:center; background-color:Silver;"> น้ำหนัก(%)<br><asp:Button ID="btnAVG_Weight" OnClick="btnAVG_Weight_Click" runat="server" CssClass=" btn mini  blue " Text="เฉลี่ยน้ำหนัก" /></th>											        
										        </tr>
                                                <tr>
										          <th class="AssLevel1" style="text-align:center; font-weight:bold; vertical-align:middle;">1</th>
										          <th class="AssLevel2" style="text-align:center; font-weight:bold; vertical-align:middle;">2</th>
										          <th class="AssLevel3" style="text-align:center; font-weight:bold; vertical-align:middle;">3</th>
										          <th class="AssLevel4" style="text-align:center; font-weight:bold; vertical-align:middle;">4</th>
										          <th class="AssLevel5" style="text-align:center; font-weight:bold; vertical-align:middle;">5</th>
								              </tr>
									        </thead>
									        <tbody>
									             <asp:Panel ID="pnlCOMP" runat="server">
									             
									                    <asp:TextBox ID="txtCOMPTypeID" runat="server" style="display:none;" Text="-1"></asp:TextBox>
				                                        <asp:TextBox ID="txtMasterNo" runat="server" style="display:none;" Text="0"></asp:TextBox>
				                                        <asp:Textbox ID="txtBehaviorNo" runat="server" style="display:none;" Text="0"></asp:Textbox>
				                                        <asp:Button ID="btnBehaviorDialog" OnClick="btnBehaviorDialog_Click" runat="server" style="display:none;"/>
				                                        <asp:Button ID="btnBehaviorDelete" OnClick="btnBehaviorDelete_Click" runat="server" style="display:none;"/>
									             
									                    <asp:Repeater ID="rptAss" OnItemCommand="rptAss_ItemCommand" OnItemDataBound="rptAss_ItemDataBound" runat="server">
									                        <ItemTemplate>
									                        <tr id="row_COMP_type" runat="server">
									                            <td colspan="10" style="font-weight:bold; font-size:18px; background-color:#CCCCCC;" id="cell_COMP_type" runat="server">สมรรถนะหลัก Core Competency, สมรรถนะตามสายงาน Functional Competency, สมรรถนะตามสายระดับ Managerial Competency</td>
									                        </tr>
        									                
									                        <tr>
										                      <td data-title="ลำดับ" rowspan="4" id="ass_no" runat="server" style="text-align:center; font-weight:bold; vertical-align:top;">001</td>
										                      <td data-title="สมรรถนะ" id="COMP_Name" class="TextLevel0" runat="server" style="background-color:#f8f8f8; ">xxxxxxxxxxxx xxxxxxxxxxxx</td>
										                      <td data-title="มาตรฐานพฤติกรรม" id="target" runat="server" class="TextLevel0" style="background-color:#f8f8f8; ">xxxxxxxxxxxx xxxxxxxxxxxx</td>
										                      <td data-title="คลิกเพื่อประเมินระดับที่ 1" id="choice1" class="TextLevel0" runat="server" style="background-color:White;">xxxxxxxxxxxx xxxxxxxxxxxx xxxxxxxxxxxx xxxxxxxxxxxx xxxxxxxxxxxx xxxxxxxxxxxx</td>
                                                              <td data-title="คลิกเพื่อประเมินระดับที่ 2" id="choice2" class="TextLevel0" runat="server" style="background-color:White;">xxxxxxxxxxxx xxxxxxxxxxxx xxxxxxxxxxxx xxxxxxxxxxxx xxxxxxxxxxxx xxxxxxxxxxxx</td>
                                                              <td data-title="คลิกเพื่อประเมินระดับที่ 3" id="choice3" class="TextLevel0" runat="server" style="background-color:White;">xxxxxxxxxxxx xxxxxxxxxxxx xxxxxxxxxxxx xxxxxxxxxxxx xxxxxxxxxxxx xxxxxxxxxxxx</td>
                                                              <td data-title="คลิกเพื่อประเมินระดับที่ 4" id="choice4" class="TextLevel0" runat="server" style="background-color:White;">xxxxxxxxxxxx xxxxxxxxxxxx xxxxxxxxxxxx xxxxxxxxxxxx xxxxxxxxxxxx xxxxxxxxxxxx</td>
                                                              <td data-title="คลิกเพื่อประเมินระดับที่ 5" id="choice5" class="TextLevel0" runat="server" style="background-color:White;">xxxxxxxxxxxx xxxxxxxxxxxx xxxxxxxxxxxx xxxxxxxxxxxx xxxxxxxxxxxx xxxxxxxxxxxx</td>
										                      <td data-title="คะแนน" rowspan="6" title="คะแนน" id="mark" runat="server" style="text-align:center; font-size:16px; font-weight:bold; vertical-align:middle;">-</td>
										                      <td data-title="น้ำหนัก" title="น้ำหนัก" id="cel_weight" runat="server" rowspan="6" style="text-align:center; background-color:#f8f8f8; vertical-align:middle;">
        										                  <asp:Textbox ID="txtWeight" MaxLength="5" runat="server" ReadOnly="true"  style="width:90%; font-size:16px;  margin-top:15px;
										                              font-weight:bold; text-align:center;background-color:#f8f8f8; color:black; float:left; text-align:center" Text=""  placeholder="น้ำหนัก ??"></asp:Textbox>
    										                      <asp:Button ID="btnWeight" runat="server" style="display:none;" CommandName="Weight" />        										              
										                      </td>
									                        </tr>
									                        <tr id="trBehavior" runat="server">
									                           <td id="tdBehavior1" runat="server" colspan="2" data-title="พฤติกรรมบ่งชี้" style=" font:inherit; background-color:#f8f8f8; text-align:center; font-size:14px;">
									                           
									                           พฤติกรรมบ่งชี้ <i class="icon-long-arrow-right"></i><br>
									                           <asp:Button ID="btnAddBehavior" runat="server" Text="(Click เพื่อเพิ่มรายการ)" style=" border:none; text-decoration:none; cursor:pointer; color:Gray; background-color:#f8f8f8;" CommandName="AddBehavior" ></asp:Button>
									                           <br><br><asp:Image ID="imgAlert" runat="server" ImageUrl="~/images/alert.gif" Visible="false" />
									                           <br><asp:Label ID="lblAlert" runat="server" ForeColor="Red"></asp:Label>
									                           </td>
									                           <td id="tdBehavior2" runat="server" colspan="2"> 
									                           
									                               <asp:Repeater ID="rptBehavior" runat="server">
									                                   <ItemTemplate>
									                                            <div style="width:100%; vertical-align:top; margin-bottom:10px;">
									                                                <a tabindex="-1" class="select2-search-choice-close" onclick="return false;" href="#"></a>
									                                                <a ID="aBehavior" runat="server" style="text-decoration:none; width:80%; float:left;" href="javascript:;"></a>
									                                                <a ID="aDelete"  runat="server" class="btn mini" style="float:right;" href="javascript:;">ลบ</a>
									                                            </div><br>
									                                   </ItemTemplate>
									                               </asp:Repeater>									                               
									                           </td>
									                           <td id="tdGAP1" runat="server" data-title="หลักสูตรการพัฒนา" style=" font:inherit; background-color:#f8f8f8; text-align:center; font-size:14px;">
									                                    วิธีการพัฒนา<br>เพื่อปิด<br>ช่องว่างสมรรถนะ
									                                    <%--<br><span style="color:Gray;">( Click ที่ชื่อหลักสูตรเพื่อเลือก <i class="icon-long-arrow-right"></i> )</span>--%>
									                                    <br/><asp:Button ID="btnAddGAP" runat="server" Text="(Click เพื่อเลือกวิธีการพัฒนา)" style=" border:none; text-decoration:none; cursor:pointer; color:Gray; background-color:#f8f8f8;" CommandName="AddGAP"></asp:Button>
                                                               </td>
									                           <td colspan="2"  id="tdGAP2" runat="server">									                           
									                               <asp:Repeater ID="rptGAP" runat="server">
									                                   <ItemTemplate>
									                                            <div style="width:100%; vertical-align:top; margin-bottom:10px;">
									                                                <a id="aGAP" runat="server"   style="text-decoration:none;" >
									                                                    <img ID="imgSelect" runat="Server" src=""/>
									                                                    <asp:Label ID="lblGAP" runat="server"></asp:Label>
									                                                </a>
									                                            </div>
									                                   </ItemTemplate>
									                               </asp:Repeater>									                               
									                           </td>
        									                   
									                        </tr>
									                        <tr>
									                           <td colspan="2" data-title="หมายเหตุ (ผู้ถูกประเมิน)" style=" font:inherit; background-color:#f8f8f8; text-align:center; font-size:14px;">หมายเหตุ <br>(ผู้ถูกประเมิน)</td>
									                           <td colspan="5" data-title="หมายเหตุ (ผู้ถูกประเมิน)" style="padding:0px;"><asp:Textbox MaxLength ="1900"  ID="txtPSN" runat="server" style="width:96%; padding:5px; background-color:Transparent; height:100%; min-height:53px; border:0px none transparent; margin:0px;" TextMode="Multiline" Text=""  Columns="50" onkeypress="return this.value.length<4000"   ></asp:Textbox>
                                                               
                                                               </td>
									                        </tr>
									                        <tr>
									                            <td colspan="2" data-title="หมายเหตุ (หัวหน้างาน/ผู้ประเมิน)" style=" font:inherit; background-color:#f8f8f8; text-align:center; font-size:14px;">หมายเหตุ <br>(ผู้ประเมิน)</td>
									                            <td colspan="5" data-title="หมายเหตุ (ผู้ประเมิน/หัวหน้างาน)" style="padding:0px;"><asp:Textbox ID="txtMGR" MaxLength="4000" runat="server" style="width:96%; padding:5px; background-color:Transparent; height:100%; min-height:53px; border:0px none transparent;  margin:0px;" TextMode="Multiline" Text=""  onkeypress="return this.value.length<4000"   ></asp:Textbox>
									                            <asp:Button ID="btnRemark_PSN" runat="server" style="display:none;" CommandName="Remark_PSN" />
									                            <asp:Button ID="btnRemark_MGR" runat="server" style="display:none;" CommandName="Remark_MGR" />
									                            </td>
									                        </tr>
									                        
									                        <tr>
									                              <td rowspan="2" data-title="ลบ" id="cel_delete" runat="server" style="cursor:pointer; font-weight:bold; text-align:center; padding:0px !important; background-color:#eeeeee;">
									                                ลบ<asp:Button ID="btn_Delete" runat="server" style="display:none;" CommandName="Delete" Text="" />
									                                <asp:ConfirmButtonExtender TargetControlID="btn_Delete" ID="cfm_Delete" runat="server"></asp:ConfirmButtonExtender>
									                              </td>
									                              
									                              <td id="cel_officer" runat="server" class="TextLevel0" colspan="2" style="text-align:center; font-weight:bold; padding:0px !important; height:10px !important;" >พนักงานประเมินตนเอง</td>
									                              <td id="selOfficialColor1" runat="server" style="cursor:pointer; padding:0px !important; height:10px !important; border-right:0px none;"><asp:Button ID="btnOff_1" runat="server" style="display:none;" CommandName="SelectOff" /></td>
                                                                  <td id="selOfficialColor2" runat="server" style="cursor:pointer; padding:0px !important; height:10px !important; border-right:0px none; border-left:0px none;"><asp:Button ID="btnOff_2" runat="server" style="display:none;" CommandName="SelectOff" /></td>
                                                                  <td id="selOfficialColor3" runat="server" style="cursor:pointer; padding:0px !important; height:10px !important; border-right:0px none; border-left:0px none;"><asp:Button ID="btnOff_3" runat="server" style="display:none;" CommandName="SelectOff" /></td>
                                                                  <td id="selOfficialColor4" runat="server" style="cursor:pointer; padding:0px !important; height:10px !important; border-right:0px none; border-left:0px none;"><asp:Button ID="btnOff_4" runat="server" style="display:none;" CommandName="SelectOff" /></td>
                                                                  <td id="selOfficialColor5" runat="server" style="cursor:pointer; padding:0px !important; height:10px !important; border-left:0px none;"><asp:Button ID="btnOff_5" runat="server" style="display:none;" CommandName="SelectOff" /></td>
                                                                 
									                        </tr>
									                        <tr>
									                              <td id="cel_header" runat="server" class="TextLevel0" colspan="2" style="text-align:center; font-weight:bold; padding:0px !important; height:10px !important;" >ผู้บังคับบัญชาประเมิน</td>
									                              <td data-title="คลิกเพื่อประเมินระดับที่ 1" id="selHeaderColor1" runat="server" style="cursor:pointer; padding:0px !important; height:10px !important; border-right:0px none;"><asp:Button ID="btnHead_1" runat="server" style="display:none;" CommandName="SelectHead" /></td>
                                                                  <td data-title="คลิกเพื่อประเมินระดับที่ 2" id="selHeaderColor2" runat="server" style="cursor:pointer; padding:0px !important; height:10px !important; border-right:0px none; border-left:0px none;"><asp:Button ID="btnHead_2" runat="server" style="display:none;" CommandName="SelectHead" /></td>
                                                                  <td data-title="คลิกเพื่อประเมินระดับที่ 3" id="selHeaderColor3" runat="server" style="cursor:pointer; padding:0px !important; height:10px !important; border-right:0px none; border-left:0px none;"><asp:Button ID="btnHead_3" runat="server" style="display:none;" CommandName="SelectHead" /></td>
                                                                  <td data-title="คลิกเพื่อประเมินระดับที่ 4" id="selHeaderColor4" runat="server" style="cursor:pointer; padding:0px !important; height:10px !important; border-right:0px none; border-left:0px none;"><asp:Button ID="btnHead_4" runat="server" style="display:none;" CommandName="SelectHead" /></td>
                                                                  <td data-title="คลิกเพื่อประเมินระดับที่ 5" id="selHeaderColor5" runat="server" style="cursor:pointer; padding:0px !important; height:10px !important; border-left:0px none;"><asp:Button ID="btnHead_5" runat="server" style="display:none;" CommandName="SelectHead" /></td>
                                                                  
									                        </tr>
									                        </ItemTemplate>
									                        <FooterTemplate>
        									                
									                             <tfoot>
									                                <tr>
									                                    <td colspan="8" style="text-align:center; background-color:#EEEEEE; font-size:14px; font-weight:bold;">
									                                    ผลรวมการประมิน
									                                    </td>
									                                    <td colspan="2" style="text-align:center; background-color:#EEEEEE; font-size:14px; font-weight:bold;">
									                                    คะแนนที่ได้
									                                    </td>
									                                </tr>
									                                <tr>
									                                    <th colspan="3" style="text-align:center; background-color:#EEEEEE; border-bottom:1px solid #DDDDDD; font-size:14px; font-weight:bold;" id="cell_total" runat="server">
									                                   รวม
									                                    </th>
									                                      <th id="cell_sum_1" runat="server" style="text-align:center; border-bottom:1px solid #DDDDDD;; font-size:14px; font-weight:bold; vertical-align:middle;">-</th>
										                                  <th id="cell_sum_2" runat="server" style="text-align:center; border-bottom:1px solid #DDDDDD;; font-size:14px; font-weight:bold; vertical-align:middle;">-</th>
										                                  <th id="cell_sum_3" runat="server" style="text-align:center; border-bottom:1px solid #DDDDDD;; font-size:14px; font-weight:bold; vertical-align:middle;">-</th>
										                                  <th id="cell_sum_4" runat="server" style="text-align:center; border-bottom:1px solid #DDDDDD;; font-size:14px; font-weight:bold; vertical-align:middle;">-</th>
										                                  <th id="cell_sum_5" runat="server" style="text-align:center; border-bottom:1px solid #DDDDDD;; font-size:14px; font-weight:bold; vertical-align:middle;">-</th>
								                                          <th id="cell_sum_raw" runat="server" style="text-align:center; border-bottom:1px solid #DDDDDD; font-size:16px; font-weight:bold;" colspan="2">-</th>
									                                </tr>
									                                <tr>
									                                    <th colspan="8" style="background-color:#FFFFFF; border:none;"></th>
									                                    <th style="text-align:center; background-color:#EEEEEE; font-size:14px; font-weight:bold;" colspan="2" >คิดเป็น</th>									                          
									                                </tr>
									                                <tr>
									                                    <th colspan="8" style="background-color:#FFFFFF; border:none;"></th>
									                                    <th id="cell_sum_mark" runat="server" style="text-align:center; font-size:24px; font-weight:bold;" colspan="2">-</th>									                          
									                                </tr>
									                            </tfoot>
        									                
									                        </FooterTemplate>
									                    </asp:Repeater>										                    
									              </asp:Panel>
									        </tbody>
									        
								        </table>
								        
								        <asp:Panel CssClass="form-actions" id="pnlActivity" runat="server">
								               <asp:Button ID="btnBack" OnClick="btnBack_Click" runat="server" CssClass="btn" Text="ย้อนกลับ" />
								        </asp:Panel>	
					</div>
						
				    <!-- END FORM-->  
				    
			</asp:Panel> 
			
			<asp:Panel ID="pnlAddAss" DefaultButton="btn_Search_Fixed_Ass" runat="server" Visible="False">
                    <div style="z-index: 1049;" class="modal-backdrop fade in"></div>
                    <div style="z-index: 1050; max-width:90%; min-width:70%; left:30%; max-height:95%; top:5%;" class="modal">
                        
				            <div class="modal-header">
	                            <h4><i class="icon-search"></i> เลือกผู้ประเมินเพิ่มเติม (ตั้งแต่อดีตจนถึงปัจจุบัน)</h4>
                            </div>
                            <div class="modal-body" style="max-height:85%;">
	                                  	<div >
											<div class="row-fluid">																			
												    <div class="row-fluid">												           					
												            <div class="span4 ">
														        <div class="control-group">
													                <label class="control-label"><i class="icon-sitemap"></i> หน่วยงาน/ตำแหน่ง</label>
													                <div class="controls">
														                <asp:TextBox ID="txt_Search_ASS_POS" OnTextChanged="btn_Fixed_Assessor_Click" runat="server" AutoPostBack="true" CssClass="m-wrap medium" placeholder="ค้นหาจากหน่วยงาน/ตำแหน่ง"></asp:TextBox>
														                <asp:Button ID="btn_Search_Fixed_Ass" OnClick="btn_Fixed_Assessor_Click" runat="server" style="display:none;" />
														            </div>
												                </div>
													        </div>													    
													       <div class="span4 ">
												                <div class="control-group">
													                <label class="control-label"><i class="icon-user"></i> ชื่อ/เลขประจำตัว</label>
													                <div class="controls">
														                <asp:TextBox ID="TextBox2" OnTextChanged="btn_Fixed_Assessor_Click" runat="server" AutoPostBack="true" CssClass="m-wrap medium" placeholder="ค้นหาจากชื่อ/เลขประจำตัว"></asp:TextBox>
														                <asp:Button ID="txt_Search_ASS_Name" OnClick="btn_Fixed_Assessor_Click" runat="server" Text="" style="display:none;" />
													                </div>
												                </div>														        
													        </div>			
													        
												    </div>
												    	
												     <asp:Label ID="lblCountFixedAssessor" runat="server" Font-Size="14px" Font-Bold="true"></asp:Label>
								                    <table class="table table-full-width table-advance dataTable no-more-tables table-hover">
									                    <thead>
										                    <tr>
											                    <th><i class="icon-sitemap"></i> หน่วยงาน</th>
											                    <th><i class="icon-user"></i>พนักงาน</th>
											                    <th><i class="icon-briefcase"></i> ตำแหน่ง</th>
											                    <th><i class="icon-bookmark"></i> ระดับ</th>											                    
											                    <th><i class="icon-bolt"></i> เลือก</th>
										                    </tr>
									                    </thead>
									                    <tbody>
									                        <asp:Repeater ID="rptFixedAssessor" OnItemCommand="rptFixedAssessor_ItemCommand" OnItemDataBound="rptFixedAssessor_ItemDataBound" runat="server">
									                            <ItemTemplate>
    									                            <tr>                                        
											                            <td data-title="หน่วยงาน"><asp:Label ID="lblPSNDept" runat="server"></asp:Label></td>
											                            <td data-title="พนักงาน"><asp:Label ID="lblPSNName" runat="server"></asp:Label></td>
											                            <td data-title="ตำแหน่ง"><asp:Label ID="lblPSNPos" runat="server"></asp:Label></td>
											                            <td data-title="ระดับ"><asp:Label ID="lblPSNClass" runat="server"></asp:Label></td>											                            
											                            <td data-title="เลือก">
											                                <asp:Button ID="btnSelect" runat="server" CssClass="btn mini blue" CommandName="Select" Text="เลือก" />
											                                <asp:ConfirmButtonExtender ID="cfmbtnSelect" TargetControlID="btnSelect" runat="server" ></asp:ConfirmButtonExtender>
										                                </td>
										                            </tr>	
									                            </ItemTemplate>
									                        </asp:Repeater>
									                    </tbody>
								                    </table>
                    								
								                    <asp:PageNavigation ID="PagerFixedAssessor" OnPageChanging="PagerFixedAssessor_PageChanging" MaximunPageCount="10" PageSize="7" runat="server" />
											  
											</div>
										</div>
                            </div>                            
                            
                            <asp:LinkButton ID="btnCloseFixedAss" OnClick="btnCloseFixedAss_Click"  runat="server" CssClass="fancybox-item fancybox-close" ToolTip="Close" />
                     
                    </div>
               </asp:Panel>
			
			  <asp:Panel CssClass="modal" style="top:20%; width:400px; position:fixed;" id="ModalBehavior" runat="server" Visible="False" >
					<div class="modal-header">										
						<h3>ลำดับที่ <asp:Label ID="lblBehaviorNo" runat="server" Text="0"></asp:Label></h3>
					</div>
					<div class="modal-body">
						<div style="height:120px">
							<div class="row-fluid">
							        <asp:TextBox TextMode="MultiLine" ID="txtBehavior" runat="server"
							         CssClass="m-wrap" Width="95%" Height="110px"></asp:TextBox>														       
							</div>
						</div>
					</div>
					<div class="modal-footer">
					    <asp:Button ID="btnOK" OnClick="btnOK_Click" runat="server" CssClass="btn blue" Text="บันทึก" />
					    <asp:Button ID="btnClose" OnClick="btnClose_Click" runat="server" CssClass="btn" Text="ยกเลิก" />																
					</div> 
				</asp:Panel>


         
         <%-- เลือกวิธีการพัฒนา --%>
		    <asp:Panel CssClass="modal" style="top:10%;left:40%; position:fixed; width:600px; "  id="ModalGAP" runat="server"  Visible ="false"  >
            <div style="padding: 0px; width: auto;  margin-top :0px; margin-bottom :0px; top: 0px; left: 0px;" class="fancybox-skin">
            <div class="fancybox-outer">
				<div class="modal-header">										
					<h3><asp:Label ID="lblHeader_ModalGAP" runat="server">เลือกวิธีการพัฒนา</asp:Label></h3>
					<asp:Label ID="lblTMPGAP" runat="server" style="display:none;"></asp:Label>
				</div>
				<div class="modal-body"  >					            
					<div class="row-fluid form-horizontal">
                        
                       								       
					</div>
					 <div class="portlet-body no-more-tables">
    								
							<asp:Label ID="lblCountGAP" runat="server" Font-Size="14px" Font-Bold="true"></asp:Label>
							<table class="table table-full-width  no-more-tables table-hover">
								<thead>
									<tr>
										<th style="text-align:center;"><i class="icon-caret-right"></i> รหัส</th>
										<th style="text-align:center;"><i class="icon-book"></i> หลักสูตร</th>											    
										<th style="text-align:center;"><asp:ImageButton ID="btnGAPAll"   runat="server" ImageUrl="images/none.png"  OnClick="btnGAPAll_Click" Height="24px" ToolTip="Click เพื่อเปลี่ยน" /> เลือกทั้งหมด</th>
									</tr>										    
								</thead>
								<tbody>
									<asp:Repeater ID="rptGAPDialog" OnItemCommand="rptGAPDialog_ItemCommand" OnItemDataBound="rptGAPDialog_ItemDataBound" runat="server">
									    <ItemTemplate>
    									    <tr>                                        
											    <td data-title="รหัส" style="text-align:center;"><asp:Label ID="lblNo" runat="server"></asp:Label></td>
											    <td data-title="หลักสูตร"><asp:Label ID="lblName" runat="server"></asp:Label></td>											           
											    <td data-title="ดำเนินการ" style="text-align:center;">
                                                    <asp:ImageButton ID="btnEditGAP"   runat="server" ImageUrl="images/none.png"  CommandName="Select" Height="24px" ToolTip="Click เพื่อเปลี่ยน" />

										        </td>
										    </tr>	
									    </ItemTemplate>
									</asp:Repeater>					
								</tbody>
							</table>
    								
							<asp:PageNavigation ID="PagerGAP" OnPageChanging="PagerGAP_PageChanging" MaximunPageCount="10" PageSize="5" runat="server" />
						</div>				            
				</div>
				<div class="modal-footer">
					<asp:Button ID="btnCloseGAP" OnClick="btnCloseGAP_Click" runat="server" CssClass="btn" Text="ยกเลิก" />
					<asp:Button ID="btnOKGAP" OnClick="btnOKGAP_Click" runat="server" CssClass="btn blue" Text="ตกลง" />										
				</div>

                </div>
                <a title="ปิด" id="btnModalGAP_Close"  onserverclick="btnModalGAP_Close_ServerClick" runat ="server"   class="fancybox-item fancybox-close" ></a>
            </div>
			</asp:Panel>
            

				<%--  GAP Handler  --%>
				<div style="display:none;">
				    <asp:TextBox ID="txtGAPID" runat="server" Text="0"></asp:TextBox>
				    <asp:TextBox ID="txtGAPName" runat="server" Text="0"></asp:TextBox>
				    <asp:Button ID="btnToggleGAP" OnClick="btnToggleGAP_Click" runat="server" />
				</div>
</div>	
</ContentTemplate>           
</asp:UpdatePanel>

<script language="javascript">
    function toggleGAP(COMP_TYPE_ID, MASTER_NO, GAP_ID, GAP_NAME) {
        document.getElementById("ctl00_ContentPlaceHolder1_txtCOMPTypeID").value = COMP_TYPE_ID;
        document.getElementById("ctl00_ContentPlaceHolder1_txtMasterNo").value = MASTER_NO;
        document.getElementById("ctl00_ContentPlaceHolder1_txtGAPID").value = GAP_ID;
        document.getElementById("ctl00_ContentPlaceHolder1_txtGAPName").value = GAP_NAME;
        document.getElementById("ctl00_ContentPlaceHolder1_btnToggleGAP").click();
    }

</script>

</asp:Content>


using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Xml.Linq;
using System.Diagnostics;
using System.Collections.Specialized;
using System.Configuration;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Caching;
using System.Web.SessionState;
using System.Web.Security;
using System.Web.Profile;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
namespace VB
{

	public partial class RPT_A_DEPTAssStatus : System.Web.UI.Page
	{


		HRBL BL = new HRBL();
		GenericLib GL = new GenericLib();

		Converter C = new Converter();
		public int R_Year {
			get {
				try {
					return GL.CINT( GL.SplitString(ddlRound.Items[ddlRound.SelectedIndex].Value, "-")[0]);
				} catch (Exception ex) {
					return 0;
				}
			}
		}

		public int R_Round {
			get {
				try {
					return GL.CINT(GL.SplitString(ddlRound.Items[ddlRound.SelectedIndex].Value, "-")[1]);
				} catch (Exception ex) {
					return 0;
				}
			}
		}

		protected void Page_Load(object sender, System.EventArgs e)
		{
			if ((Session["USER_PSNL_NO"] == null)) {
				ScriptManager.RegisterStartupScript(this.Page, typeof(string), "Alert", "showAlert('กรุณาลงชื่อเข้าใช้ระบบ');", true);
				Response.Redirect("Login.aspx");
				return;
			}

			if (!IsPostBack) {
				//----------------- ทำสองที่ Page Load กับ Update Status ---------------
				BL.BindDDlYearRound(ddlRound);
				ddlRound.Items.RemoveAt(0);
				BL.BindDDlSector(ddlSector);

				BindPersonalList();
			}

			SetCheckboxStyle();
		}

		private void SetCheckboxStyle()
		{
			if (chkKPIYes.Checked) {
				chkKPIYes.CssClass = "checked";
			} else {
				chkKPIYes.CssClass = "";
			}
			if (chkKPINo.Checked) {
				chkKPINo.CssClass = "checked";
			} else {
				chkKPINo.CssClass = "";
			}
			if (chkCOMPYes.Checked) {
				chkCOMPYes.CssClass = "checked";
			} else {
				chkCOMPYes.CssClass = "";
			}
			if (chkCOMPNo.Checked) {
				chkCOMPNo.CssClass = "checked";
			} else {
				chkCOMPNo.CssClass = "";
			}
		}

        protected void btnSearch_Click(object sender, System.EventArgs e)
		{
			BindPersonalList();
		}

		private void BindPersonalList()
		{
			string SQL = "";
			SQL += " SELECT KPI.R_Year,KPI.R_Round,KPI.SECTOR_CODE,KPI.SECTOR_NAME,KPI.DEPT_CODE,KPI.DEPT_NAME," + "\n";

			SQL += " SUM(CASE KPI.KPI_Status WHEN 5 THEN 1 ELSE 0 END) KPI_Yes," + "\n";
			SQL += " SUM(CASE KPI.KPI_Status WHEN 5 THEN 0 ELSE 1 END) KPI_No," + "\n";
			SQL += " SUM(CASE COMP.COMP_Status WHEN 5 THEN 1 ELSE 0 END) COMP_Yes," + "\n";
			SQL += " SUM(CASE COMP.COMP_Status WHEN 5 THEN 0 ELSE 1 END) COMP_No," + "\n";
			SQL += " COUNT(KPI.PSNL_NO) Total_PSN " + "\n";
			SQL += " FROM vw_RPT_KPI_Status KPI " + "\n";
			SQL += " LEFT JOIN vw_RPT_COMP_Status COMP ON KPI.R_Year=COMP.R_Year AND KPI.R_Round=COMP.R_Round AND KPI.PSNL_NO=COMP.PSNL_NO" + "\n";

			SQL += " WHERE KPI.R_Year=" + R_Year + "\n";
			SQL += " AND KPI.R_Round=" + R_Round + "\n";
			string Title = ddlRound.Items[ddlRound.SelectedIndex].Text + " ";


			if (ddlSector.SelectedIndex > 0) {
				SQL += " AND ( KPI.SECTOR_CODE='" + ddlSector.Items[ddlSector.SelectedIndex].Value + "' " + "\n";
				if (ddlSector.Items[ddlSector.SelectedIndex].Value == "3200") {
					SQL += " AND  KPI.DEPT_CODE NOT IN ('32000300'))    " + "\n";
				//-----------ฝ่ายโรงงานผลิตยาสูบ 3
				} else if (ddlSector.Items[ddlSector.SelectedIndex].Value == "3203") {
					SQL += " OR  KPI.DEPT_CODE IN ('32000300','32030000','32030100','32030200','32030300','32030500'))  " + "\n";
				//-----------ฝ่ายโรงงานผลิตยาสูบ 4
				} else if (ddlSector.Items[ddlSector.SelectedIndex].Value == "3204") {
					SQL += " OR  KPI.DEPT_CODE IN ('32040000','32040100','32040200','32040300','32040500','32040300'))  " + "\n";
				//-----------ฝ่ายโรงงานผลิตยาสูบ 5
				} else if (ddlSector.Items[ddlSector.SelectedIndex].Value == "3205") {
					SQL += " OR  KPI.DEPT_CODE IN ('32050000','32050100','32050200','32050300','32050500'))\t  " + "\n";
				} else {
					SQL += ")   " + "\n";
				}

				Title += GL.SplitString(ddlSector.Items[ddlSector.SelectedIndex].Text, ":")[1].Trim() + " ";
			}
			if (!string.IsNullOrEmpty(txtDeptName.Text)) {
				SQL += " AND (KPI.DEPT_Name LIKE '%" + txtDeptName.Text.Replace("'", "''") + "%' OR KPI.Sector_Name LIKE '%" + txtDeptName.Text.Replace("'", "''") + "%') " + "\n";
				//Title &= " หน่วยงาน '" & txtDeptName.Text & "' "
			}
			SQL += " GROUP BY KPI.R_Year,KPI.R_Round,KPI.SECTOR_CODE,KPI.SECTOR_NAME,KPI.DEPT_CODE,KPI.DEPT_NAME" + "\n";

			string Filter = "";
			int KPIStat = 0;
			if ((chkKPIYes.Checked & chkKPINo.Checked) || (!chkKPIYes.Checked & !chkKPINo.Checked))
            {
                KPIStat = -1;
            }
            else if (chkKPIYes.Checked)
            {
                Filter += " SUM(CASE KPI.KPI_Status WHEN 5 THEN 1 ELSE 0 END)=COUNT(KPI.PSNL_NO) AND ";
                KPIStat = 1;
            }
            else if (chkKPINo.Checked)
            {
                Filter += " SUM(CASE KPI.KPI_Status WHEN 5 THEN 0 ELSE 1 END)>0 AND ";
                KPIStat = 0;
            }


			int COMPStat = 0;
            if ((chkCOMPYes.Checked & chkCOMPNo.Checked) || (!chkCOMPYes.Checked & !chkCOMPNo.Checked))
            {
                COMPStat = -1;
            }
            else if (chkCOMPYes.Checked)
            {
                Filter += " SUM(CASE COMP.COMP_Status WHEN 5 THEN 1 ELSE 0 END)=COUNT(KPI.PSNL_NO) AND ";
                COMPStat = 1;
            }
            else if (chkCOMPNo.Checked)
            {
                Filter += " SUM(CASE COMP.COMP_Status WHEN 5 THEN 0 ELSE 1 END)>0 AND ";
                COMPStat = 0;
            }
		
			if (!string.IsNullOrEmpty(Filter)) {
				SQL += " HAVING " + (Filter).Substring(0, Filter.Length - 4) + "\n";
			}

			if (KPIStat == COMPStat) {
				switch (KPIStat) {
					case -1:

						break;
					case 0:
						Title += "ที่การประเมินตัวชี้วัดและสมรรรถนะยังไม่เสร็จ";
						break;
					case 1:
						Title += "การประเมินเสร็จสมบูรณ์";
						break;
				}
			} else if (KPIStat == -1) {
				switch (COMPStat) {
					case 0:
						Title += "ที่การประเมินสมรรถนะยังไม่เสร็จ";
						break;
					case 1:
						Title += "ที่การประเมินสมรรถนะเสร็จสมบูรณ์";
						break;
				}
			} else if (KPIStat == 0) {
				switch (COMPStat) {
					case -1:
						Title += "ที่การประเมินตัวชี้วัดยังไม่เสร็จ";
						break;
					case 1:
						Title += "ที่การประเมินสมรรถนะเสร็จแล้วแต่ตัวชี้วัดยังไม่เสร็จ";
						break;
				}
			} else if (KPIStat == 1) {
				switch (COMPStat) {
					case -1:
						Title += "ที่การประเมินตัวชี้วัดสมบูรณ์";
						break;
					case 0:
						Title += "ที่การประเมินตัวชี้วัดเสร็จแล้วแต่สมรรถนะยังไม่เสร็จ";
						break;
				}
			}

			SQL += " ORDER BY KPI.R_Year,KPI.R_Round,KPI.SECTOR_CODE,KPI.DEPT_CODE" + "\n";

			SqlDataAdapter DA = new SqlDataAdapter(SQL, BL.ConnectionString());
			DA.SelectCommand.CommandTimeout = 90;
			DataTable DT = new DataTable();
			DA.Fill(DT);

			DataTable TMP = DT.Copy();
			TMP.Rows.Clear();
			//------------- Optimize to Calculate Sum---------------
			string[] Col = {
				"SECTOR_CODE",
				"SECTOR_Name"
			};
			DataTable SECTOR = DT.DefaultView.ToTable(true, Col);
			//------------- Add Each Sector ------------------------
			for (int s = 0; s <= SECTOR.Rows.Count - 1; s++) {
				DT.DefaultView.RowFilter = "SECTOR_CODE='" + SECTOR.Rows[s]["SECTOR_CODE"] + "'";
				TMP.Merge(DT.DefaultView.ToTable().Copy());
				if (DT.DefaultView.Count > 0) {
					DataRow Footer = TMP.NewRow();
					Footer["SECTOR_CODE"] = SECTOR.Rows[s]["SECTOR_CODE"];
					Footer["SECTOR_NAME"] = SECTOR.Rows[s]["SECTOR_NAME"];
					Footer["DEPT_NAME"] = "รวม";
					Footer["Total_PSN"] = DT.Compute("SUM(Total_PSN)", DT.DefaultView.RowFilter);
					Footer["KPI_Yes"] = DT.Compute("SUM(KPI_Yes)", DT.DefaultView.RowFilter);
					Footer["KPI_No"] = DT.Compute("SUM(KPI_No)", DT.DefaultView.RowFilter);
					Footer["COMP_Yes"] = DT.Compute("SUM(COMP_Yes)", DT.DefaultView.RowFilter);
					Footer["COMP_No"] = DT.Compute("SUM(COMP_No)", DT.DefaultView.RowFilter);
					TMP.Rows.Add(Footer);
				}
			}
			DT.DefaultView.RowFilter = "";

			Session["RPT_A_DEPTAssStatus"] = TMP;
			//--------- Title --------------
			Session["RPT_A_DEPTAssStatus_Title"] = Title;
			//--------- Sub Title ----------
			Session["RPT_A_DEPTAssStatus_SubTitle"] = "รายงาน ณ วันที่ " + DateTime.Now.Day + " " + C.ToMonthNameTH(DateTime.Now.Month) + " พ.ศ." + (DateTime.Now.Year + 543);
			Session["RPT_A_DEPTAssStatus_SubTitle"] += "  พบ   " + GL.StringFormatNumber(DT.Rows.Count, 0) + " รายการ ";

			Pager.SesssionSourceName = "RPT_A_DEPTAssStatus";
			Pager.RenderLayout();
			lblTitle.Text = Title;
			if (DT.Rows.Count == 0) {
				lblCountPSN.Text = "ไม่พบรายการดังกล่าว";
			} else {
				lblCountPSN.Text = "พบ " + GL.StringFormatNumber(DT.Rows.Count, 0) + " รายการ";
			}

		}

		protected void Pager_PageChanging(PageNavigation Sender)
		{
			Pager.TheRepeater = rptASS;
		}

		//------------ For Grouping -----------
		string LastSector = "";
		protected void rptASS_ItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
		{
			if (e.Item.ItemType != ListItemType.AlternatingItem & e.Item.ItemType != ListItemType.Item)
				return;

            Label lblSector = (Label)e.Item.FindControl("lblSector");
            Label lblDept = (Label)e.Item.FindControl("lblDept");
            Label lbl_KPI_Yes = (Label)e.Item.FindControl("lbl_KPI_Yes");
            Label lbl_KPI_No = (Label)e.Item.FindControl("lbl_KPI_No");
            Label lbl_COMP_Yes = (Label)e.Item.FindControl("lbl_COMP_Yes");
            Label lbl_COMP_No = (Label)e.Item.FindControl("lbl_COMP_No");
            Label lblTotal = (Label)e.Item.FindControl("lblTotal");

            DataRowView drv = (DataRowView)e.Item.DataItem;

			if (drv["SECTOR_NAME"].ToString() != LastSector) {
				lblSector.Text = drv["SECTOR_NAME"].ToString();
				LastSector = drv["SECTOR_NAME"].ToString();
				for (int i = 1; i <= 7; i++) {
                    HtmlTableCell td = (HtmlTableCell)e.Item.FindControl("td" + i);
					td.Style["border-top-width"] = "2px";
					td.Style["border-top-color"] = "#999999";
				}
			} else {
                HtmlTableCell td1 = (HtmlTableCell)e.Item.FindControl("td1");
				td1.Style["border-top"] = "none";
			}

			if (Conversion.Val(drv["KPI_Yes"]) == 0) {
				lbl_KPI_Yes.Text = "-";
			} else {
				lbl_KPI_Yes.Text = GL.StringFormatNumber(drv["KPI_Yes"], 0);
			}
			if (Conversion.Val(drv["KPI_No"]) == 0) {
				lbl_KPI_No.Text = "-";
			} else {
				lbl_KPI_No.Text = GL.StringFormatNumber(drv["KPI_No"], 0);
			}
			if (Conversion.Val(drv["COMP_Yes"]) == 0) {
				lbl_COMP_Yes.Text = "-";
			} else {
				lbl_COMP_Yes.Text = GL.StringFormatNumber(drv["COMP_Yes"], 0);
			}
			if (Conversion.Val(drv["COMP_No"]) == 0) {
				lbl_COMP_No.Text = "-";
			} else {
				lbl_COMP_No.Text = GL.StringFormatNumber(drv["COMP_No"], 0);
			}

			if (drv["DEPT_NAME"].ToString() != drv["SECTOR_NAME"].ToString()) {
				lblDept.Text = drv["DEPT_NAME"].ToString().Replace(drv["SECTOR_NAME"].ToString(), "").Trim();
			} else {
				lblDept.Text = drv["DEPT_NAME"].ToString();
			}

			lblTotal.Text = GL.StringFormatNumber(drv["Total_PSN"], 0);

			//--------------- Set Summary -------------
			if ( drv["DEPT_NAME"].ToString() == "รวม") {
				lblTotal.Font.Bold = true;
				lbl_KPI_Yes.Font.Bold = true;
				lbl_KPI_No.Font.Bold = true;
				lbl_COMP_Yes.Font.Bold = true;
				lbl_COMP_No.Font.Bold = true;
                HtmlTableCell td2 = (HtmlTableCell)e.Item.FindControl("td2");
				td2.Style["text-align"] = "center";
				td2.Style["font-weight"] = "bold";
				for (int i = 2; i <= 7; i++) {
                    HtmlTableCell td = (HtmlTableCell)e.Item.FindControl("td" + i);
					td.Style["border-top-width"] = "2px";
					td.Style["border-top-color"] = "#999999";
				}
			}


		}
		public RPT_A_DEPTAssStatus()
		{
			Load += Page_Load;
		}

	}
}

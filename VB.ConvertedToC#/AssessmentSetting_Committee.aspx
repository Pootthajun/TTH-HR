﻿<%@ Page  Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPage.Master" CodeBehind="AssessmentSetting_Committee.aspx.cs" Inherits="VB.AssessmentSetting_Committee" %>

<%@ Register src="WUC_DateReporter.ascx" tagname="WUC_DateReporter" tagprefix="uc1" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<%@ Register src="PageNavigation.ascx" tagname="PageNavigation" tagprefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <link type="text/css" rel="stylesheet" href="assets/plugins/font-awesome/css/font-awesome.css" />
	
    <link type="text/css" rel="stylesheet" href="assets/css/pages/search.css" />
	<link type="text/css" rel="stylesheet" href="assets/css/pages/profile.css" />
	<link type="text/css" rel="stylesheet" href="assets/plugins/chosen-bootstrap/chosen/chosen.css" />
	<style>
	    .portfolio-btn a:hover {
            background: none repeat scroll 0 0 red !important;
	    }
	</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<asp:UpdatePanel ID="UpdatePanel1" runat="server">				           
<ContentTemplate>
<asp:ToolkitScriptManager ID="toolkit1" runat="server"></asp:ToolkitScriptManager>

<div class="container-fluid">
				<!-- BEGIN PAGE HEADER-->
				<div class="row-fluid" id="fixedPageHeader">
					<div class="span12"> 
						<!-- BEGIN PAGE TITLE & BREADCRUMB-->
						<h3 class="page-user-md">ผู้ประเมินแต่ละหน่วยงาน <font color="blue">(ScreenID : S-HDR-03)</font></h3>					
									
						<ul class="breadcrumb">
                            <li>
                            	<i class="icon-th-list"></i><a href="javascript:;">การกำหนดข้อมูลการประเมิน</a><i class="icon-angle-right"></i>
                            </li>
                        	<li><i class="icon-user-md"></i> <a href="javascript:;">ผู้ประเมินแต่ละหน่วยงาน</a></li>                        	
                            <uc1:WUC_DateReporter ID="WUC_DateReporter1" runat="server" />
                        </ul>
						<!-- END PAGE TITLE & BREADCRUMB-->
									        
				     </div>

				
			    </div>
		        <!-- END PAGE HEADER-->
				
				<asp:Panel ID="pnlList" runat="server" Visible="True">
                        <div class="row-fluid">        					
					        <div class="span12">
						        <!-- BEGIN SAMPLE TABLE PORTLET-->        								                           
							            <h4 class="control-label"> <i class="icon-retweet"></i> สำหรับรอบการประเมิน        							    
								            <asp:DropDownList ID="ddlRound" OnSelectedIndexChanged="btnSearch_Click" runat="server" AutoPostBack="true" CssClass="medium m-wrap">
									        </asp:DropDownList>
						                    &nbsp; &nbsp;
						                    ค้นหา
						                    <asp:TextBox  ID="txtFilter" OnTextChanged="btnSearch_Click" runat="server" AutoPostBack="true" CssClass="m-wrap large" style=" background-color:White; margin-top:5px;margin-left:10px;" placeholder="หน่วยงาน/ผู้บังคับบัญชา/ผู้ที่ได้รับมอบหมายให้ประเมิน"></asp:TextBox>
						                    &nbsp; &nbsp;
						                   <asp:CheckBox ID="chkBlank" OnCheckedChanged="btnSearch_Click" runat="server" Text="" AutoPostBack="true" /> แสดงเฉพาะที่ยังไม่มีผู้ประเมิน
						                   <asp:Button ID="btnSearch" OnClick="btnSearch_Click" runat="server" Style="display:none" />
							           </h4>
							           
												           								        
						            <div class="portlet-body no-more-tables">
						            <asp:Label ID="lblCountList" runat="server" Font-Size="14px" Font-Bold="true"></asp:Label>
								        <table class="table table-full-width dataTable no-more-tables table-hover">
									        <thead>
										        <tr>
											        <th><i class="icon-sitemap"></i> ฝ่าย</th>
											        <th><i class="icon-sitemap"></i> หน่วยงาน</th>
											        <th><i class="icon-user-md"></i> ผู้ประเมินหลัก</th>
											        <th><i class="icon-check"></i> ผู้ที่ได้รับมอบหมายเพิ่มเติม</th>
											        <th><i class="icon-bolt"></i> ดำเนินการ</th>
											        <th></th>
										        </tr>
									        </thead>
									        <tbody>
										        <asp:Repeater ID="rptList" OnItemCommand="rptList_ItemCommand" OnItemDataBound="rptList_ItemDataBound" runat="server"> 
										            <ItemTemplate>
										                    <tr>
											                    <td class="highlight" data-title="ฝ่าย"><asp:Label ID="lblSector" runat="server"></asp:Label></td>
											                    <td class="highlight" data-title="หน่วยงาน"><asp:Label ID="lblDept" runat="server"></asp:Label></td>
											                    <td data-title="ผู้ประเมินหลัก">
											                        <asp:Label ID="lblDefault" runat="server"></asp:Label>
											                 
											                    </td>
											                    <td data-title="ผู้ที่ได้รับมอบหมายเพิ่มเติม">
											                        <asp:Label ID="lblAssigned" runat="server"></asp:Label>
											                   
											                    </td>
											                    <td data-title="ดำเนินการ">
											                        <asp:LinkButton ID="btnEdit" runat="server" CssClass="btn mini blue" CommandName="Edit"><i class="icon-bolt"></i> ผู้ประเมิน</asp:LinkButton></td>
										                    </tr>	
										            </ItemTemplate>
										        </asp:Repeater>        															
									        </tbody>
								        </table>
								        
								        <asp:PageNavigation ID="Pager" MaximunPageCount="5" PageSize="20" runat="server" />
							        </div>
        						<!-- END SAMPLE TABLE PORTLET-->       						
					        </div>
                        </div>
               </asp:Panel>
               
              <asp:Panel ID="pnlEdit" runat="server" Visible="False">
				    <div class="portlet-body form">
							<h3 class="form-section"><i class="icon-retweet"></i> 
							
							แต่งตั้งผู้ประเมินสำหรับ <asp:Label ID="lblEditDept" runat="server"></asp:Label> 
							<asp:Label ID="lblEditRound" runat="server"></asp:Label>
												
							</h3>
								<!-- BEGIN FORM-->
								
							    			    
								<div class="form-horizontal form-view" id="divView" runat="server">									
									<h3 class="form-section"><i class="icon-user-md"></i> <asp:Label ID="lblHeaderDefault" runat="server" Text="ผู้ประเมินหลัก"></asp:Label></h3>
									<div class="row-fluid">													
										<div class="span6 ">
											<div class="control-group">
												<label for="firstName" class="control-label">ชื่อ:</label>
												<div class="controls">
													<asp:Label ID="lblMainName" runat="server" CssClass="text bold"></asp:Label>
												</div>
											</div>
										</div>
										<!--/span-->
										<div class="span6 ">
											<div class="control-group">
												<label class="control-label">เลขประจำตัว:</label>
												<div class="controls">
													<asp:Label ID="lblMainCode" runat="server" CssClass="text bold"></asp:Label>
												</div>
											</div>
										</div>
										<!--/span-->
									</div>
									<!--/row-->
									<div class="row-fluid">
									    <div class="span6 ">
											<div class="control-group">
												<label for="lastName" class="control-label">ตำแหน่ง:</label>
												<div class="controls">
													<asp:Label ID="lblMainPos" runat="server" CssClass="text bold"></asp:Label>
												</div>
											</div>
										</div>
										<!--/span-->
										
										<div class="span6 ">
											<div class="control-group">
												<label class="control-label">ระดับ:</label>
												<div class="controls">
													<asp:Label ID="lblMainClass" runat="server" CssClass="text bold"></asp:Label>
												</div>
											</div>
										</div>
										<!--/span-->
									</div>
									<!--/row-->        
									           
									<h3 class="form-section"><i class="icon-check"></i> <asp:Label ID="lblHeaderAssigned" runat="server" Text="ผู้ได้รับมอบหมายให้ประเมินเพิ่มเติม"></asp:Label></h3>
									<div class="row-fluid" id="pnlAssignedList" runat="server">
										<div class="span12 ">
											<div class="control-group">
												<label class="control-label">&nbsp;</label>
												
												    <div class="controls">
												    <div class="span10">
													    <div class="portlet-body no-more-tables">
					                                        <table class="table  table-hover">
						                                        <thead>
							                                        <tr>
								                                        <th>#</th>
								                                        <th><i class="icon-user-md"></i> เลขประจำตัว</th>
								                                        <th><i class="icon-user-md"></i> ชื่อ</th>
								                                        <th><i class="icon-bookmark"></i> ตำแหน่ง</th>
								                                        <th><i class="icon-sitemap"></i> หน่วยงาน</th>
								                                        <th><i class="icon-bolt"></i> ดำเนินการ</th>
							                                        </tr>
						                                        </thead>
						                                        <tbody>
							                                        <asp:Repeater ID="rptAssigned" OnItemCommand="rptAssigned_ItemCommand" OnItemDataBound="rptAssigned_ItemDataBound" runat="server">
							                                            <ItemTemplate>
							                                                    <tr>
								                                                    <td data-title="#"><asp:Label ID="lblNo" runat="server"></asp:Label></td>
								                                                    <td data-title="เลขประจำตัว"><asp:Label ID="lblCode" runat="server"></asp:Label></td>
								                                                    <td data-title="ชื่อ"><asp:Label ID="lblName" runat="server"></asp:Label></td>
								                                                    <td data-title="ตำแหน่ง"><asp:Label ID="lblPos" runat="server"></asp:Label></td>
								                                                    <td data-title="หน่วยงาน"><asp:Label ID="lblDept" runat="server"></asp:Label></td>
								                                                    <td data-title="ดำเนินการ"><asp:Button CssClass="btn red mini" Text="ยกเลิก" ID="btnDelete" runat="server" CommandName="Delete"/></td>
								                                                    <asp:ConfirmButtonExtender TargetControlID="btnDelete" ID="cfm_Delete" runat="server"></asp:ConfirmButtonExtender>
							                                                    </tr>
							                                            </ItemTemplate>
							                                        </asp:Repeater>							                                        									                    
						                                        </tbody>
					                                        </table>
				                                        </div>
												    </div>
												</div>
											</div>
										</div>
									</div>
									
									<div class="form-actions">
									    <a class="btn purple" href="javascript:;" onclick="document.getElementById('ctl00_ContentPlaceHolder1_pnlAdd').style.visibility='visible'; document.getElementById('ctl00_ContentPlaceHolder1_btnAdd').click();">เพิ่มผู้ประเมิน</a>
									    <asp:Button CssClass="btn" runat="server" ID="btnCancel" OnClick="btnCancel_Click" Text="ย้อนกลับ" />
										<asp:Button runat="server" ID="btnAdd" OnClick="btnAdd_Click" Text="เพิ่มผู้ประเมิน" style="display:none;" />
									</div>
								</div>
								
							
                <asp:Panel ID="pnlAdd" DefaultButton="btn_Search_Add_List" runat="server" style="visibility:hidden;">
                    <div style="z-index: 10049;" class="modal-backdrop fade in"></div>
                    <div style="z-index: 10050; max-width:90%; min-width:70%; left:30%; max-height:95%; top:5%;" class="modal">
                        
				            <div class="modal-header">
	                            <h3><i class="icon-search"></i> เพิ่มผู้ประเมิน</h3>
                            </div>
                            <div class="modal-body" style="max-height:85%;">
	                                  	<div >
											<div class="row-fluid">
																			
												    <div class="row-fluid">
												           										        
												    
												            <div class="span4 ">
													            <div class="control-group">
													                <label class="control-label"><i class="icon-sitemap"></i> ฝ่าย</label>
													                <div class="controls">
														                 <asp:DropDownList ID="ddl_Search_Sector" OnSelectedIndexChanged="Search_Changed" runat="server" AutoPostBack="true" CssClass="medium m-wrap">
														                </asp:DropDownList>
														            </div>
												                </div>
													        </div>
													         <div class="span4 ">
														        <div class="control-group">
													                <label class="control-label"><i class="icon-sitemap"></i> หน่วยงาน/ตำแหน่ง</label>
													                <div class="controls">
														                <asp:TextBox ID="txt_Search_Organize" OnTextChanged="Search_Changed" runat="server" AutoPostBack="true" CssClass="m-wrap medium" placeholder="ค้นหาจากหน่วยงาน/ตำแหน่ง"></asp:TextBox>
														            </div>
												                </div>
													        </div>
													    
													       <div class="span4 ">
												                <div class="control-group">
													                <label class="control-label"><i class="icon-user"></i> ชื่อ</label>
													                <div class="controls">
														                <asp:TextBox ID="txt_Search_Name" OnTextChanged="Search_Changed" runat="server" AutoPostBack="true" CssClass="m-wrap medium" placeholder="ค้นหาจากชื่อ/เลขประจำตัว"></asp:TextBox>
														                <asp:Button ID="btn_Search_Add_List"  OnClick="Search_Changed" runat="server" Text="" style="display:none;" />
													                </div>
												                </div>														        
													        </div>			
													        
												    </div>
												    	
												     <asp:Label ID="lblCountPSN" runat="server" Font-Size="14px" Font-Bold="true"></asp:Label>
								                    <table class="table table-full-width table-advance dataTable no-more-tables table-hover">
									                    <thead>
										                    <tr>
											                    <th><i class="icon-sitemap"></i> หน่วยงาน</th>
											                    <th><i class="icon-user"></i>เลขประจำตัว</th>
											                    <th><i class="icon-user"></i>ชื่อ</th>											                    
											                    <th><i class="icon-briefcase"></i> ตำแหน่งการบริหาร</th>
											                    <th><i class="icon-bookmark"></i> ระดับ</th>
											                    <th><i class="icon-bolt"></i> เลือก</th>
										                    </tr>
									                    </thead>
									                    <tbody>
									                        <asp:Repeater ID="rptPSN" OnItemCommand="rptPSN_ItemCommand" OnItemDataBound="rptPSN_ItemDataBound" runat="server">
									                            <ItemTemplate>
    									                            <tr>                                        
											                            <td data-title="หน่วยงาน"><asp:Label ID="lnkPSNOrganize" runat="server"></asp:Label></td>
											                            <td data-title="เลขประจำตัว"><asp:Label ID="lblPSNNo" runat="server"></asp:Label></td>
											                            <td data-title="ชื่อ"><asp:Label ID="lblPSNName" runat="server"></asp:Label></td>
											                            <td data-title="ตำแหน่งการบริหาร"><asp:Label ID="lblPSNMGR" runat="server"></asp:Label></td>
											                            <td data-title="ระดับ"><asp:Label ID="lblPSNClass" runat="server"></asp:Label></td>
											                            <td data-title="เลือก">
											                                <asp:Button ID="btnSelect" runat="server" CssClass="btn mini blue" CommandName="Select" Text="เลือก" />
											                                <asp:ConfirmButtonExtender ID="cfmbtnSelect" TargetControlID="btnSelect" runat="server" ></asp:ConfirmButtonExtender>
										                                </td>
										                            </tr>	
									                            </ItemTemplate>
									                        </asp:Repeater>
									                    </tbody>
								                    </table>
                    								
								                    <asp:PageNavigation ID="PagerPSN" OnPageChanging="PagerPSN_PageChanging" MaximunPageCount="10" PageSize="7" runat="server" />
											  
											</div>
										</div>
                            </div>                            
                            <a href="javascript:;" onclick="document.getElementById('ctl00_ContentPlaceHolder1_pnlAdd').style.visibility='hidden'; document.getElementById('ctl00_ContentPlaceHolder1_btnClosePNLAdd').click();" class="fancybox-item fancybox-close" title="Close"></a>
                            <asp:Button ID="btnClosePNLAdd" OnClick="btnClosePNLAdd_Click" runat="server" style="display:none;" />
                        <%--</div>--%>
                    </div>
               </asp:Panel>
								<!-- END FORM-->  
				</div>
					    
		</asp:Panel>

							 
</ContentTemplate>				                
</asp:UpdatePanel>		 
</asp:Content>



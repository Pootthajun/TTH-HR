using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Xml.Linq;
using System.Diagnostics;
using System.Collections.Specialized;
using System.Configuration;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Caching;
using System.Web.SessionState;
using System.Web.Security;
using System.Web.Profile;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
namespace VB
{
    public partial class Print_RPT_W_Excel : System.Web.UI.Page
    {        
        HRBL BL = new HRBL();
        GenericLib GL = new GenericLib();

        public string DEPT_CODE
        {
            get
            {
                try
                {
                    if (Request.QueryString["D"] == null) return "";
                    return Request.QueryString["D"];
                }
                catch (Exception ex)
                {
                    return "";
                }
            }
        }

        public string MINOR_CODE
        {
            get
            {
                try
                {
                    if (Request.QueryString["M"] == null) return "";
                    return Request.QueryString["M"];
                }
                catch (Exception ex)
                {
                    return "00";
                }
            }
        }


        protected void Page_Load(object sender, System.EventArgs e)
        {
            if ((Session["USER_PSNL_NO"] == null))
            {
                ScriptManager.RegisterStartupScript(this.Page, typeof(string), "Alert", "showAlert('��س�ŧ����������к�');", true);
                Response.Redirect("../Login.aspx");
                return;

            }

            if (!Convert.ToBoolean(Session["USER_Is_Workload_Admin"]) & !Convert.ToBoolean(Session["USER_Is_Workload_User"]) & !Convert.ToBoolean(Session["USER_Is_Manager"]))
            {
                ScriptManager.RegisterStartupScript(this.Page, typeof(string), "Alert", "showAlert('�س������Է�����Ҷ֧��������ǹ���'); window.history.back();", true);
                return;
            }

            BindWholeTable();
            setHeaderText();

            ///////---------------- To Excel Format--------------------------
            DataTable DEPT = BL.GetDeptInfo(DEPT_CODE, MINOR_CODE);

            string Filename = "���ҧ���СԨ�ҹ_" + DEPT_CODE;
            if (DEPT.Rows.Count > 0)
            {
                string DEPT_NAME = DEPT.Rows[0]["DEPT_Name"].ToString();
                Filename += "_" + DEPT_NAME;
            }
            else
            {
                Filename += "_Unknow";
            }
            Filename += "_" + DateTime.Now.ToString("yyyyMMdd") + ".xls";

            Response.AddHeader("content-disposition", "attachment;filename=" + Filename);
            Response.ContentType = "application/ms-excel";
            Response.ContentEncoding = System.Text.Encoding.Unicode;
            Response.BinaryWrite(System.Text.Encoding.Unicode.GetPreamble());
        }

        private void BindWholeTable()
        {
            BindClass();
            BindPSN();
            BindHeader();
            //----------- Add Class Header + Personal Detail--------
            BindJob();
        }

        private void setHeaderText()
        {
            lblH1.Text = "�ӴѺ";
            lblH2.Text = "�ҹ";
            lblH3.Text = "�ӹǹ<br>��黯Ժѵ�";
            lblH4.Text = "�ӹǹ<br/>�ҷշ����<br/>㹡��<br/>��Ժѵ�<br/>��ͤ���";
            lblH5.Text = "���"; //�ӹǹ����<br/>(�������)<br/>������<br/>��û�Ժѵ�<br/>� 1 ��
            lblH6.Text = "�ӹǹ����<br/>(�������)<br/>������<br/>��û�Ժѵ�<br/>� 1 ��)";
            lblH7.Text = "�ӹǹ<br/>�ҷշ����<br/>� 1 ��"; //�ӹǹ<br/>�ҷշ����<br/>� 1 ��
            lblH8.Text = "���������<br/>�����㹡�û�Ժѵ�"; //���������<br/>�����㹡�û�Ժѵ�
        }

        //--------- ������ Ἱ� -------------
        DataTable DeptClass;
        DataTable PSNData;
        private void BindClass()
        {
            string SQL = "SELECT Class_ID,PNPO_CLASS_Start,PNPO_CLASS_End\n";
            SQL += " FROM tb_HR_Job_Mapping_Class\n";
            SQL += " WHERE DEPT_CODE='" + DEPT_CODE.Replace("'", "''") + "' AND MINOR_CODE='" + MINOR_CODE.Replace("'", "''") + "' \n";
            SQL += " ORDER BY Class_ID DESC\n";
            SqlDataAdapter DA = new SqlDataAdapter(SQL, BL.ConnectionString());
            DeptClass = new DataTable();
            DA.Fill(DeptClass);

            //----------- ��� Default ��� �óշ�� �ѧ������駤���дѺ --------------
            if (DeptClass.Rows.Count == 0)
            {
                BL.SetJobMappingAutoClass(DEPT_CODE, MINOR_CODE);
                DA = new SqlDataAdapter(SQL, BL.ConnectionString());
                DeptClass = new DataTable();
                DA.Fill(DeptClass);
            }
        }

        private void BindPSN()
        {
            string SQL = " SELECT \n";
            SQL += " CLS.Class_ID,PSN_ID,REF_PSNL_NO PSNL_NO,Slot_No,Slot_Name\n";
            SQL += " FROM tb_HR_Job_Mapping_Class CLS\n";
            SQL += " LEFT JOIN tb_HR_Workload_PSN PSN ON PSN.Class_ID=CLS.Class_ID\n";
            SQL += " \t\t\t\t\t\t\tAND PSN.DEPT_CODE=CLS.DEPT_CODE \n";
            SQL += " \t\t\t\t\t\t\tAND PSN.MINOR_CODE=CLS.MINOR_CODE\n";
            SQL += " WHERE CLS.DEPT_CODE='" + DEPT_CODE.Replace("'", "''") + "' AND CLS.MINOR_CODE='" + MINOR_CODE.Replace("'", "''") + "'\n";
            SQL += " ORDER BY CLS.Class_ID DESC,Slot_No ASC\n";

            SqlDataAdapter DA = new SqlDataAdapter(SQL, BL.ConnectionString());
            PSNData = new DataTable();
            DA.Fill(PSNData);

            //----------- ��� Default ��� �óշ�� �ѧ������駤�Һؤ�� --------------
            PSNData.DefaultView.RowFilter = "PSN_ID IS NOT NULL";
            if (PSNData.DefaultView.Count == 0)
            {
                BL.SetWorkloadDefaultPSN(DEPT_CODE, MINOR_CODE);
                DA = new SqlDataAdapter(SQL, BL.ConnectionString());
                PSNData = new DataTable();
                DA.Fill(PSNData);
            }
        }

        private void BindHeader()
        {
            //----------- Add Class Header--------
            rptClass.DataSource = DeptClass;
            rptClass.DataBind();
            //----------- Add Personal Detail--------
            rptPSN.DataSource = PSNData;
            rptPSN.DataBind();
        }

        protected void rptClass_ItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType != ListItemType.Item & e.Item.ItemType != ListItemType.AlternatingItem)
                return;

            HtmlTableCell thClass = (HtmlTableCell)e.Item.FindControl("thClass");
            Label lblClassName = (Label)e.Item.FindControl("lblClassName");
            DataRowView drv = (DataRowView)e.Item.DataItem;
            int _start = GL.CINT(drv["PNPO_CLASS_Start"]);
            int _end = GL.CINT(drv["PNPO_CLASS_End"]);
            if (_start == _end)
            {
                lblClassName.Text = "��ѡ�ҹ�дѺ " + _start + " <i class='icon-angle-down'></i>";
            }
            else
            {
                lblClassName.Text = "��ѡ�ҹ�дѺ " + _start + "-" + _end + " <i class='icon-angle-down'></i>";
            }

            //---------------- Set Binding Data ----------
            lblClassName.Attributes["Class_ID"] = drv["Class_ID"].ToString();
            lblClassName.Attributes["PNPO_CLASS_Start"] = drv["PNPO_CLASS_Start"].ToString();
            lblClassName.Attributes["PNPO_CLASS_End"] = drv["PNPO_CLASS_End"].ToString();

            //---------------- Set ColSpan ---------------
            PSNData.DefaultView.RowFilter = "Class_ID=" + drv["Class_ID"];
            if (PSNData.DefaultView.Count > 1)
            {
                thClass.ColSpan = PSNData.DefaultView.Count;
            }
            PSNData.DefaultView.RowFilter = "";
        }

        protected void rptPSN_ItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType != ListItemType.Item & e.Item.ItemType != ListItemType.AlternatingItem)
                return;

            Label lblPSNName = (Label)e.Item.FindControl("lblPSNName");
            DataRowView drv = (DataRowView)e.Item.DataItem;

            if (!GL.IsEqualNull(drv["Slot_Name"]) || !GL.IsEqualNull(drv["PSN_ID"]))
            {
                lblPSNName.Text = drv["Slot_Name"].ToString();
                //lblPSNName.Attributes["Slot_Name"] = drv["Slot_Name"].ToString();               
            }
            else
            {
                lblPSNName.Text = "";
            }
            //---------------- Set Binding Data ----------
            lblPSNName.Attributes["Class_ID"] = drv["Class_ID"].ToString();
        }

        //---------- ������Ἱ���ҹ -----------
        DataTable JobData;
        //----------- ���������� -------------
        DataTable SlotData;

        DataTable MapClass;
        private void BindJob()
        {
            string SQL = null;
            SQL = "  SELECT Job_LEVEL,Job_No,\n";
            SQL += "   Header.Job_ID,Job_Parent,Header.Job_Name,NumPerson,MinPerTime,TimePerYear,\n";
            SQL += "   CAST(COUNT(Detail.Class_ID) AS BIT) Editable\n";
            SQL += "   FROM tb_HR_Job_Mapping Header  \n";
            SQL += "   LEFT JOIN tb_HR_Job_Mapping_Detail Detail ON Header.Job_ID=Detail.Job_ID\n";
            SQL += "   WHERE PNDP_DEPT_CODE='" + DEPT_CODE.Replace("'", "''") + "' AND PNDP_MINOR_CODE='" + MINOR_CODE.Replace("'", "''") + "' \n";
            SQL += "   GROUP BY PNDP_DEPT_CODE,PNDP_MINOR_CODE,Job_LEVEL,Job_No,Header.Job_ID,Job_Parent,Header.Job_Name,\n";
            SQL += "   NumPerson,MinPerTime,TimePerYear\n";
            SQL += "   ORDER BY  Header.Job_LEVEL,Header.Job_No\n";

            SqlDataAdapter DA = new SqlDataAdapter(SQL, BL.ConnectionString());
            JobData = new DataTable();
            DA.Fill(JobData);

            //----------- Sort For Binding Structure----------
            DataTable DT = JobData.Copy();
            DT.Rows.Clear();
            DT.AcceptChanges();
            JobData.DefaultView.RowFilter = "Job_LEVEL=1";
            DataTable L1 = JobData.DefaultView.ToTable().Copy();
            JobData.DefaultView.RowFilter = "Job_LEVEL=2";
            DataTable L2 = JobData.DefaultView.ToTable().Copy();
            JobData.DefaultView.RowFilter = "Job_LEVEL=3";
            DataTable L3 = JobData.DefaultView.ToTable().Copy();
            for (int i = 0; i <= L1.Rows.Count - 1; i++)
            {
                DataRow R1 = DT.NewRow();
                R1.ItemArray = L1.Rows[i].ItemArray;
                DT.Rows.Add(R1);
                L2.DefaultView.RowFilter = "Job_Parent=" + R1["Job_ID"];
                for (int j = 0; j <= L2.DefaultView.Count - 1; j++)
                {
                    DataRow R2 = DT.NewRow();
                    R2.ItemArray = L2.DefaultView[j].Row.ItemArray;
                    DT.Rows.Add(R2);
                    L3.DefaultView.RowFilter = "Job_Parent=" + R2["Job_ID"];
                    for (int k = 0; k <= L3.DefaultView.Count - 1; k++)
                    {
                        DataRow R3 = DT.NewRow();
                        R3.ItemArray = L3.DefaultView[k].Row.ItemArray;
                        DT.Rows.Add(R3);
                    }
                }
            }
            // ERROR: Not supported in C#: OnErrorStatement

            JobData = DT.Copy();
            JobData.Columns.Add("PersonMin", typeof(long), "NumPerson*MinPerTime");
            JobData.Columns.Add("TotalMin", typeof(long), "PersonMin*TimePerYear");
            //----------- Job Data -------------
            string[] Col = {
                "Job_LEVEL",
                "Job_No",
                "Job_ID",
                "Job_Parent",
                "Job_Name",
                "Editable",
                "NumPerson",
                "MinPerTime",
                "TimePerYear"
            };
            DT.DefaultView.RowFilter = "";
            DT = DT.DefaultView.ToTable(true, Col);
            DataTable _job = DT.Copy();

            //----------- Get Slot Data ---------
            DT.DefaultView.RowFilter = "Editable=1";
            string _jid = "0,";
            for (int i = 0; i <= DT.DefaultView.Count - 1; i++)
            {
                _jid += DT.DefaultView[i]["Job_ID"] + ",";
            }
            if (!string.IsNullOrEmpty(_jid))
                _jid = _jid.Substring(0, _jid.Length - 1);
            SQL = " SELECT Header.Job_ID,Slot_ID,Slot.PSN_ID,MinPerYear\n";
            SQL += "   FROM tb_HR_Job_Mapping Header  \n";
            SQL += "   INNER JOIN tb_HR_Workload_Slot Slot ON Header.Job_ID=Slot.Job_ID\n";
            SQL += "   INNER JOIN tb_HR_Workload_PSN PSN ON Slot.PSN_ID=PSN.PSN_ID\n";
            SQL += "   INNER JOIN tb_HR_Job_Mapping_Class CLS ON PSN.Class_ID=CLS.Class_ID\n";
            SQL += "                            AND PSN.DEPT_CODE=CLS.DEPT_CODE\n";
            SQL += "                            AND PSN.MINOR_CODE=CLS.MINOR_CODE\n";
            SQL += "    WHERE Header.Job_ID IN (" + _jid + ")\n";
            SlotData = new DataTable();
            DA = new SqlDataAdapter(SQL, BL.ConnectionString());
            DA.Fill(SlotData);
            //----------- SlotData -------------


            //----------- Get Mapping Class------
            SQL = " SELECT Header.Job_ID,Detail.Class_ID\n";
            SQL += " FROM tb_HR_Job_Mapping Header  \n";
            SQL += " INNER JOIN tb_HR_Job_Mapping_Detail Detail ON Header.Job_ID=Detail.Job_ID\n";
            MapClass = new DataTable();
            DA = new SqlDataAdapter(SQL, BL.ConnectionString());
            DA.Fill(MapClass);
            //----------- for check requirement---

            rptJob.DataSource = JobData;
            rptJob.DataBind();

        }

        protected void rptJob_ItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
        {
            object _sumMin;
            DataTable DT;

            switch (e.Item.ItemType)
            {
                case ListItemType.Item:
                case ListItemType.AlternatingItem:

                    Label lblNo = (Label)e.Item.FindControl("lblNo");
                    Label lblTask = (Label)e.Item.FindControl("lblTask");
                    HtmlTableCell tdTask = (HtmlTableCell)e.Item.FindControl("tdTask");
                    Label lblNumPerson = (Label)e.Item.FindControl("lblNumPerson");
                    Label lblMinPerTime = (Label)e.Item.FindControl("lblMinPerTime");
                    Label lblPersonMin = (Label)e.Item.FindControl("lblPersonMin");
                    Label lblTimePerYear = (Label)e.Item.FindControl("lblTimePerYear");
                    Label lblTotalMin = (Label)e.Item.FindControl("lblTotalMin");

                    HtmlTableCell cellNumPerson = (HtmlTableCell)e.Item.FindControl("cellNumPerson");
                    HtmlTableCell cellMinPerTime = (HtmlTableCell)e.Item.FindControl("cellMinPerTime");
                    HtmlTableCell cellTimePerYear = (HtmlTableCell)e.Item.FindControl("cellTimePerYear");
                    HtmlTableCell cellTotalMin = (HtmlTableCell)e.Item.FindControl("cellTotalMin");
                    HtmlTableCell cellSumMin = (HtmlTableCell)e.Item.FindControl("cellSumMin");
                    Label lblSumMin = (Label)e.Item.FindControl("lblSumMin");
                    //---------- Job Header -----------

                    DataRowView drv = (DataRowView)e.Item.DataItem;

                    lblNo.Attributes["Job_ID"] = drv["Job_ID"].ToString();
                    if (drv["Job_LEVEL"].ToString() == "1")
                        lblNo.Text = drv["Job_No"].ToString();
                    if (!GL.IsEqualNull(drv["PersonMin"]))
                        lblPersonMin.Text = GL.StringFormatNumber(drv["PersonMin"].ToString(), 0);

                    //----------- Calculate Sum Min -------------
                    _sumMin = SlotData.Compute("SUM(MinPerYear)", "Job_ID=" + drv["Job_ID"]);
                    if (!GL.IsEqualNull(_sumMin))
                    {
                        lblSumMin.Text = GL.StringFormatNumber(_sumMin.ToString(), 0);
                    }

                    if (!GL.IsEqualNull(drv["TotalMin"]))
                    {
                        lblTotalMin.Text = GL.StringFormatNumber(drv["TotalMin"], 0);
                        //-------------------- Calculate Matching ------------------
                        if (GL.IsEqualNull(_sumMin) || _sumMin.ToString() != drv["TotalMin"].ToString())
                        {
                            cellTotalMin.Attributes["class"] += " WorkLoadBGNotMatch";
                            cellTotalMin.Style.Remove("background-color");
                            cellTotalMin.Style.Remove("color");
                            cellSumMin.Attributes["class"] += " WorkLoadBGNotMatch";
                            cellSumMin.Style.Remove("background-color");
                            cellSumMin.Style.Remove("color");

                            if (GL.IsEqualNull(_sumMin))
                            {
                                cellTotalMin.Attributes["title"] = "���һ����Թ��м�������ҷ�軯Ժѵ������ҡѹ";
                            }
                            else if (GL.CDBL(drv["TotalMin"]) > GL.CDBL(_sumMin))
                            {
                                cellTotalMin.Attributes["title"] = "��������ҷ�軯ԺѵԹ��¡������� " + GL.StringFormatNumber(GL.CDBL(drv["TotalMin"]) - GL.CDBL(_sumMin), 0) + " �ҷ�";
                            }
                            else if (GL.CDBL(drv["TotalMin"]) < GL.CDBL(_sumMin))
                            {
                                cellTotalMin.Attributes["title"] = "���ҷ�軯Ժѵ��ҡ���ҡ������� " + GL.StringFormatNumber(GL.CDBL(_sumMin) - GL.CDBL(drv["TotalMin"]), 0) + " �ҷ�";
                            }

                        }
                        else
                        {
                            cellTotalMin.Attributes["class"] += " WorkLoadBGMatch";
                            cellTotalMin.Style.Remove("background-color");
                            cellTotalMin.Style.Remove("color");
                            cellSumMin.Attributes["class"] += " WorkLoadBGMatch";
                            cellSumMin.Style.Remove("background-color");
                            cellSumMin.Style.Remove("color");
                        }
                    }



                    switch (GL.CINT(drv["Job_LEVEL"]))
                    {
                        case 1:
                            lblTask.Text = drv["Job_Name"].ToString().Replace("\n", "<br>");
                            lblTask.Font.Bold = true;
                            break;
                        case 2:
                            lblTask.Text = drv["Job_Name"].ToString().Replace("\n", "<br>");
                            //------------ Check Has Sub ----------------
                            DT = JobData.Copy();
                            int _parent_no = (int)DT.Compute("MAX(Job_No)", "Job_ID=" + drv["Job_Parent"]);
                            DT.DefaultView.RowFilter = "Job_LEVEL=2 AND Job_Parent=" + drv["Job_Parent"];
                            //--------- ����¡�÷������� Sub ���ǡѹ ------------ 
                            DataTable tmp = DT.DefaultView.ToTable().Copy();
                            for (int i = 0; i <= tmp.Rows.Count - 1; i++)
                            {
                                DT.DefaultView.RowFilter = "Job_Parent=" + tmp.Rows[i]["Job_ID"];
                                if (DT.DefaultView.Count > 0)
                                {
                                    lblTask.Text = _parent_no + "." + drv["Job_No"] + " " + drv["Job_Name"].ToString().Replace("\n", "<br>");
                                    lblTask.Font.Bold = true;
                                    break; // TODO: might not be correct. Was : Exit For
                                }
                            }

                            break;
                        case 3:
                            lblTask.Text = drv["Job_Name"].ToString().Replace("\n", "<br>");
                            tdTask.Style["padding-left"] = "30px";
                            break;
                    }

                    bool Editable = GL.CBOOL(drv["Editable"]);

                    lblNumPerson.Visible = Editable;
                    lblMinPerTime.Visible = Editable;
                    lblPersonMin.Visible = Editable;
                    lblTimePerYear.Visible = Editable;
                    lblTotalMin.Visible = Editable;
                    if (Editable)
                    {
                        if (!GL.IsEqualNull(drv["NumPerson"]))
                        {
                            lblNumPerson.Text = GL.StringFormatNumber(drv["NumPerson"], 0);
                        }
                        cellNumPerson.Attributes["class"] += " WorkLoadBGNotFill";

                        if (!GL.IsEqualNull(drv["MinPerTime"]))
                        {
                            lblMinPerTime.Text = GL.StringFormatNumber(drv["MinPerTime"], 0);
                        }
                        cellMinPerTime.Attributes["class"] += " WorkLoadBGNotFill";

                        if (!GL.IsEqualNull(drv["TimePerYear"]))
                        {
                            lblTimePerYear.Text = GL.StringFormatNumber(drv["TimePerYear"], 0);
                        }
                        cellTimePerYear.Attributes["class"] += " WorkLoadBGNotFill";
                    }

                    //---------- Bind Slot ---------
                    Repeater rptSlot = (Repeater)e.Item.FindControl("rptSlot");
                    rptSlot.ItemDataBound += rptSlot_ItemDataBound;
                    rptSlot.DataSource = PSNData;
                    rptSlot.DataBind();

                    break;
                case ListItemType.Footer:

                    Label lblF1 = (Label)e.Item.FindControl("lblF1");
                    Label lblF2 = (Label)e.Item.FindControl("lblF2");
                    lblF1.Text = "�ӹǹ�ҷշ����㹡�û�Ժѵ�";
                    lblF2.Text = "�Դ�繨ӹǹ�������";

                    //------------- Calculate Summary -------------
                    DT = JobData.Copy();

                    double _result;
                    object Tmp = DT.Compute("SUM(TotalMin)", "");
                    if (!GL.IsEqualNull(Tmp))
                    {
                        Label lblTotalMinPerYear = (Label)e.Item.FindControl("lblTotalMinPerYear");
                        Label lblTotalHourPerYear = (Label)e.Item.FindControl("lblTotalHourPerYear");
                        Label lblFTEYear = (Label)e.Item.FindControl("lblFTEYear");


                        _result = GL.CDBL(Tmp);
                        lblTotalMinPerYear.Text = GL.StringFormatNumber(_result, 0);
                        _result /= 60;
                        lblTotalHourPerYear.Text = GL.StringFormatNumber(_result, 2);
                        _result /= BL.MasterFTE();
                        lblFTEYear.Text = GL.StringFormatNumber(_result, 2);
                    }

                    _sumMin = SlotData.Compute("SUM(MinPerYear)", "");
                    if (!GL.IsEqualNull(_sumMin))
                    {
                        Label lblSumMinPerYear = (Label)e.Item.FindControl("lblSumMinPerYear");
                        Label lblSumHourPerYear = (Label)e.Item.FindControl("lblSumHourPerYear");
                        Label lblFTESum = (Label)e.Item.FindControl("lblFTESum");

                        _result = GL.CDBL(_sumMin);
                        lblSumMinPerYear.Text = GL.StringFormatNumber(_result.ToString(), 0);
                        _result /= 60;
                        lblSumHourPerYear.Text = GL.StringFormatNumber(_result.ToString(), 2);
                        _result /= BL.MasterFTE();
                        lblFTESum.Text = GL.StringFormatNumber(_result.ToString(), 2);
                    }

                    //------------- Display Total Personal Minute Per Year ---------
                    Repeater rptMin = (Repeater)e.Item.FindControl("rptMin");
                    rptMin.ItemDataBound += rptMin_ItemDataBound;
                    rptMin.DataSource = PSNData;
                    rptMin.DataBind();

                    //------------- Display Total Personal Hour Per Year ---------
                    Repeater rptHour = (Repeater)e.Item.FindControl("rptHour");
                    rptHour.ItemDataBound += rptHour_ItemDataBound;
                    rptHour.DataSource = PSNData;
                    rptHour.DataBind();

                    //------------- Display Total Personal FTE Per Year ---------
                    Repeater rptFTE = (Repeater)e.Item.FindControl("rptFTE");
                    rptFTE.ItemDataBound += rptFTE_ItemDataBound;
                    rptFTE.DataSource = PSNData;
                    rptFTE.DataBind();
                    break;
            }
        }

        protected void rptSlot_ItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType != ListItemType.Item & e.Item.ItemType != ListItemType.AlternatingItem)
                return;

            Label lblSlot = (Label)e.Item.FindControl("lblSlot");
            HtmlTableCell cellSlot = (HtmlTableCell)e.Item.FindControl("cellSlot");
            DataTable Slot = (DataTable)SlotData.Copy();
            DataTable Job = (DataTable)JobData.Copy();

            RepeaterItem ParentItem = (RepeaterItem)e.Item.Parent.Parent;
            Label lblNo = (Label)ParentItem.FindControl("lblNo");

            DataRowView drv = (DataRowView)e.Item.DataItem;

            //-------------- ����դ������ Slot ------------
            if (GL.IsEqualNull(drv["PSN_ID"]))
            {
                lblSlot.Visible = false;
                return;
            }

            Job.DefaultView.RowFilter = "Job_ID=" + lblNo.Attributes["Job_ID"];

            if (Job.DefaultView.Count > 0 && GL.CBOOL(Job.DefaultView[0]["Editable"]))
            {
                lblSlot.Visible = true;
                Slot.DefaultView.RowFilter = "Job_ID=" + lblNo.Attributes["Job_ID"] + " AND PSN_ID=" + drv["PSN_ID"];
                if (Slot.DefaultView.Count > 0 && !GL.IsEqualNull(Slot.DefaultView[0]["MinPerYear"]))
                {
                    lblSlot.Text = GL.StringFormatNumber(Slot.DefaultView[0]["MinPerYear"].ToString(), 0);
                }
                //------------ Check Requirement---------
                MapClass.DefaultView.RowFilter = "Job_ID=" + lblNo.Attributes["Job_ID"] + " AND Class_ID=" + drv["Class_ID"];
                if (MapClass.DefaultView.Count > 0)
                {
                    cellSlot.Attributes["class"] += " WorkLoadBGNotFill";
                }
            }
            else
            {
                lblSlot.Visible = false;
            }
        }

        protected void rptMin_ItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
        {

            if (e.Item.ItemType != ListItemType.Item & e.Item.ItemType != ListItemType.AlternatingItem)
                return;

            DataRowView drv = (DataRowView)e.Item.DataItem;

            if (GL.IsEqualNull(drv["PSN_ID"]))
                return;

            DataTable DT = SlotData.Copy();
            object Tmp = DT.Compute("SUM(MinPerYear)", "PSN_ID=" + drv["PSN_ID"].ToString());
            if (!GL.IsEqualNull(Tmp))
            {
                Label lblMin = (Label)e.Item.FindControl("lblMin");
                lblMin.Text = GL.StringFormatNumber(Tmp.ToString(), 0).ToString();
            }
        }

        protected void rptHour_ItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType != ListItemType.Item & e.Item.ItemType != ListItemType.AlternatingItem)
                return;

            DataRowView drv = (DataRowView)e.Item.DataItem;

            if (GL.IsEqualNull(drv["PSN_ID"]))
                return;

            DataTable DT = SlotData.Copy();
            object Tmp = DT.Compute("SUM(MinPerYear)", "PSN_ID=" + drv["PSN_ID"]);


            if (!GL.IsEqualNull(Tmp))
            {
                Label lblHour = (Label)e.Item.FindControl("lblHour");

                double _result = GL.CDBL(Tmp);
                _result /= 60;
                lblHour.Text = GL.StringFormatNumber(_result.ToString(), 2);
            }
        }

        protected void rptFTE_ItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType != ListItemType.Item & e.Item.ItemType != ListItemType.AlternatingItem)
                return;
            DataRowView drv = (DataRowView)e.Item.DataItem;
            if (GL.IsEqualNull(drv["PSN_ID"]))
                return;

            DataTable DT = SlotData.Copy();
            object Tmp = DT.Compute("SUM(MinPerYear)", "PSN_ID=" + drv["PSN_ID"]);
            if (!GL.IsEqualNull(Tmp))
            {
                Label lblFTE = (Label)e.Item.FindControl("lblFTE");
                double _result = GL.CDBL(Tmp);
                _result /= 60;
                _result /= BL.MasterFTE();
                lblFTE.Text = GL.StringFormatNumber(_result.ToString(), 2);
            }
        }

    }
}

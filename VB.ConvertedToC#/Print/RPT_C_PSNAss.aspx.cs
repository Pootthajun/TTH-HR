using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Xml.Linq;
using System.Diagnostics;
using System.Collections.Specialized;
using System.Configuration;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Caching;
using System.Web.SessionState;
using System.Web.Security;
using System.Web.Profile;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CrystalDecisions.Web;
using CrystalDecisions.CrystalReports.Engine;
namespace VB
{

	public partial class Print_RPT_C_PSNAss : System.Web.UI.Page
	{



		Converter C = new Converter();
        GenericLib GL=new GenericLib();
        ReportDocument cc;
		protected void Page_Load(object sender, System.EventArgs e)
		{
			if ((Session["USER_PSNL_NO"] == null)) {
				ScriptManager.RegisterStartupScript(this.Page, typeof(string), "Alert", "alert('กรุณาลงชื่อเข้าใช้ระบบ');", true);
				Response.Redirect("Login.aspx");
				return;
			}

			cc = new ReportDocument();
            DataTable DT = (DataTable)Session["RPT_C_PSNAss_DT"];
			cc.Load(Server.MapPath("../Report/RPT_C_PSNAss.rpt"));
			cc.SetDataSource(DT);
			cc.SetParameterValue("Round", Session["RPT_C_PSNAss_Round"]);
			cc.SetParameterValue("PSNL_NO", Session["RPT_C_PSNAss_PSNL_NO"]);
			cc.SetParameterValue("Status", Session["RPT_C_PSNAss_Status"]);
			cc.SetParameterValue("Name", Session["RPT_C_PSNAss_Name"]);
			//---------------- Set Header -------------------
			cc.SetParameterValue("Dept", Session["RPT_C_PSNAss_Dept"]);
			cc.SetParameterValue("Type", Session["RPT_C_PSNAss_Type"]);
			cc.SetParameterValue("Pos", Session["RPT_C_PSNAss_Pos"]);
			cc.SetParameterValue("Class", Session["RPT_C_PSNAss_Class"]);
			//------------------------------
			CrystalReportViewer1.ReportSource = cc;

            byte[] B;
			switch (Request.QueryString["Mode"].ToUpper()) {
				case "PDF":
					string FilePath = Server.MapPath("../Temp/" + Session.SessionID + DateAndTime.Now.ToOADate().ToString().Replace(".", "") + ".pdf");
					//------------- Show File----------
					Response.AddHeader("Content-Type", "application/pdf");
					Response.AppendHeader("Content-Disposition", "filename=แบบประเมินของพนักงานรายบุคคล" + DateAndTime.Now.Day + "Competency" + "-" + DateAndTime.Now.Year + ".pdf");
					B = C.StreamToByte(cc.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat));
					Response.BinaryWrite(B);
					break;
				default:
					break;
			}


		}
		public Print_RPT_C_PSNAss()
		{
			Load += Page_Load;
		}
        private void Page_Unload(object sender, System.EventArgs e)
        {
            if (cc != null)
            {
                cc.Close();
                cc.Dispose();
            }
        }

	}
}

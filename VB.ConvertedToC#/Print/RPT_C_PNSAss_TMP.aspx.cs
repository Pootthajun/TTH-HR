﻿using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Xml.Linq;
using System.Diagnostics;
using System.Collections.Specialized;
using System.Configuration;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Caching;
using System.Web.SessionState;
using System.Web.Security;
using System.Web.Profile;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using CrystalDecisions.Web;
using CrystalDecisions.CrystalReports.Engine;
namespace VB
{

	public partial class RPT_C_PNSAss_TMP : System.Web.UI.Page
	{


		Converter C = new Converter();
        GenericLib GL = new GenericLib();
		HRBL BL = new HRBL();
        ReportDocument cc;
        public int R_Year
        {
            get { return Convert.ToInt32(Request.QueryString["R_Year"]); }
        }

        public int R_Round
        {
            get { return Convert.ToInt32(Request.QueryString["R_Round"]); }
        }

        public string PSNL_NO
        {
            get { return Request.QueryString["PSNL_NO"]; }
        }

        public string Mode
        {
            get { return Request.QueryString["MODE"]; }
        }

        public int Status
        {
            get { return Convert.ToInt32(Request.QueryString["Status"]); }
        }



		protected void Page_Load(object sender, System.EventArgs e)
		{
			if ((Session["USER_PSNL_NO"] == null)) {
				ScriptManager.RegisterStartupScript(this.Page, typeof(string), "Alert", "alert('กรุณาลงชื่อเข้าใช้ระบบ');", true);
				Response.Redirect("../Login.aspx");
				return;
			}

            //------------------- Get Personal Info --------------------
            HRBL.PersonalInfo PSN = BL.GetAssessmentPersonalInfo(R_Year, R_Round, PSNL_NO, Status);

            DataTable DT = BL.GetCOMPBundle(PSNL_NO, R_Year, R_Round, GL.CINT(PSN.CLSGP_ID), GL.CINT(PSN.PSNL_TYPE), PSN.FN_ID, PSN.FN_Type, HRBL.CompetencyType.All);

            DataTable BT = new DataTable();
            string SQL = "";

            SQL += " SELECT COMP_Type_Id,Master_No,BHV_No,BHV_Content\n";
            SQL += " FROM tb_HR_COMP_Behavior TOP_BHV\n";
            SQL += " WHERE PSNL_NO='" + PSNL_NO.Replace("'", "''") + "' AND R_Year=" + R_Year + " AND R_Round=" + R_Round + "\n";
            SQL += " ORDER BY COMP_Type_Id,Master_No,BHV_No\n";
            //BHV_Count

            SqlDataAdapter DA = new SqlDataAdapter(SQL, BL.ConnectionString());
            DA.Fill(BT);

            cc = new ReportDocument();
            cc.Load(Server.MapPath("../Report/RPT_C_PNSAss.rpt"));
            cc.Subreports["BHV"].SetDataSource(BT);
            cc.SetDataSource(DT);

            cc.SetParameterValue("PSN_Name", PSN.PSNL_Fullname);
            cc.SetParameterValue("PSN_Dept", PSN.DEPT_NAME);
            cc.SetParameterValue("PSN_Pos", (string.IsNullOrEmpty(PSN.MGR_NAME) ? PSN.FN_NAME : PSN.MGR_NAME));
            cc.SetParameterValue("PSNL_NO", PSNL_NO);
            cc.SetParameterValue("R_Year", R_Year);
            cc.SetParameterValue("PSN_Type", "พนักงาน" + PSN.WAGE_NAME + " ระดับ " + Convert.ToInt32(PSN.PNPS_CLASS));

            //-------------- Get Assessor List ------------
            DropDownList DDL = new DropDownList();
            //  Dim ASS_CODE As String = BL.Get_Selected_Assessor_Code(PSNL_NO, HRBL.AssessmentType.KPI, R_Year, R_Round)
            BL.BindDDLAssessor(DDL, R_Year, R_Round, PSNL_NO);

            //-------------- เอา DDL มาคิด เพราะใน BL จะคิด Default ของผู้ประเมินมาใส่ใน DDL อยู่แล้ว-------------------
            cc.SetParameterValue("ASS_Code", ASSESSOR_CODE(DDL));
            cc.SetParameterValue("ASS_Name", ASSESSOR_NAME(DDL));
            cc.SetParameterValue("ASS_Pos", ASSESSOR_POS(DDL));
            cc.SetParameterValue("ASS_Dept", ASSESSOR_DEPT(DDL));
            cc.SetParameterValue("R_Round", R_Round);

			//------------------------------
			CrystalReportViewer1.ReportSource = cc;
            byte[] B;

			switch (Request.QueryString["Mode"].ToUpper()) {
				case "PDF":
					Response.AddHeader("Content-Type", "application/pdf");
					Response.AppendHeader("Content-Disposition", "filename=COMP-" + PSNL_NO + "_" + DateAndTime.Now.Year + DateAndTime.Now.Month.ToString().PadLeft(2, GL.chr0) + DateAndTime.Now.Day.ToString().PadLeft(2, GL.chr0) + ".pdf");
					B = C.StreamToByte(cc.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat));
					Response.BinaryWrite(B);
					break;
				case "EXCEL":
					Response.AddHeader("Content-Type", "application/vnd.ms-excel");
					Response.AppendHeader("Content-Disposition", "filename=COMP-" + PSNL_NO + "_" + DateAndTime.Now.Year + DateAndTime.Now.Month.ToString().PadLeft(2, GL.chr0) + DateAndTime.Now.Day.ToString().PadLeft(2, GL.chr0) + ".xls");
					B = C.StreamToByte(cc.ExportToStream(CrystalDecisions.Shared.ExportFormatType.Excel));
					Response.BinaryWrite(B);
					break;
				default:
					break;
			}

		}
        		private string ASSESSOR_CODE(DropDownList DDL)
		{
			try {
				string[] tmp = Strings.Split(DDL.Items[DDL.SelectedIndex].Value, ":::");
				return tmp[0];
			} catch (Exception ex) {
				return "";
			}
		}

		private string ASSESSOR_NAME(DropDownList DDL)
		{
			try {
				return DDL.Items[DDL.SelectedIndex].Text;
			} catch (Exception ex) {
				return "";
			}
		}
		private string ASSESSOR_POS(DropDownList DDL)
		{
			try {
				string[] tmp = Strings.Split(DDL.Items[DDL.SelectedIndex].Value, ":::");
				return tmp[1];
			} catch (Exception ex) {
				return "";
			}
		}
		private string ASSESSOR_DEPT(DropDownList DDL)
		{
			try {
				string[] tmp = Strings.Split(DDL.Items[DDL.SelectedIndex].Value, ":::");
				return tmp[2];
			} catch (Exception ex) {
				return "";
			}
		}
		public RPT_C_PNSAss_TMP()
		{
			Load += Page_Load;
		}
        private void Page_Unload(object sender, System.EventArgs e)
        {
            if (cc != null)
            {
                cc.Close();
                cc.Dispose();
            }
        }



	}
}

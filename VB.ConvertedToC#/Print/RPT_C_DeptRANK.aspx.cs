using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Xml.Linq;
using System.Diagnostics;
using System.Collections.Specialized;
using System.Configuration;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Caching;
using System.Web.SessionState;
using System.Web.Security;
using System.Web.Profile;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using CrystalDecisions.Web;
using CrystalDecisions.CrystalReports.Engine;
namespace VB
{

	public partial class Print_RPT_C_DeptRANK : System.Web.UI.Page
	{


        GenericLib GL = new GenericLib();
		Converter C = new Converter();
        ReportDocument cc;
		protected void Page_Load(object sender, System.EventArgs e)
		{
			if ((Session["USER_PSNL_NO"] == null)) {
				ScriptManager.RegisterStartupScript(this.Page, typeof(string), "Alert", "alert('กรุณาลงชื่อเข้าใช้ระบบ');", true);
				Response.Redirect("../Login.aspx");
				return;
			}

            DataTable DT = (DataTable)Session["RPT_C_DeptRANK"];
			if ((DT == null)) {
				ScriptManager.RegisterStartupScript(this.Page, typeof(string), "Alert", "alert('เลือกเงื่อนไขการรายงาน'); window.close();", true);
				return;
			}

			//---------------- Format DT ---------------
			DataTable COMP = DT.Copy();
			COMP.Columns.Add("New_Sector", typeof(int));
			COMP.Columns.Add("New_Dept", typeof(int));
			COMP.Columns.Add("New_Rank", typeof(int));

			string LastSector = "";
			string LastDept = "";
			string LastRank = "";
			for (int i = 0; i <= COMP.Rows.Count - 1; i++) {
				if (COMP.Rows[i]["SECTOR_NAME"].ToString() != LastSector) {
					COMP.Rows[i]["New_Sector"] = 1;
					LastSector = COMP.Rows[i]["SECTOR_NAME"].ToString();
					LastDept = "";
					LastRank = "";
				}
				if (COMP.Rows[i]["DEPT_NAME"].ToString() != LastDept) {
					COMP.Rows[i]["New_Dept"] = 1;
					LastDept = COMP.Rows[i]["DEPT_NAME"].ToString();
					LastRank = "";
				}
				if (COMP.Rows[i]["RANK_Name"].ToString() != LastRank) {
					COMP.Rows[i]["New_Rank"] = 1;
					LastRank = COMP.Rows[i]["RANK_Name"].ToString();
				}
			}

			cc = new ReportDocument();
			cc.Load(Server.MapPath("../Report/RPT_C_DeptRANK.rpt"));
			cc.SetDataSource(COMP);
			cc.SetParameterValue("Title", Session["RPT_C_DeptRANK_Title"]);
			cc.SetParameterValue("SubTitle", Session["RPT_C_DeptRANK_SubTitle"]);

			CrystalReportViewer1.ReportSource = cc;
            byte[] B;
			switch (Request.QueryString["Mode"].ToUpper()) {
				case "PDF":
					Response.AddHeader("Content-Type", "application/pdf");
					Response.AppendHeader("Content-Disposition", "filename=ผลการประเมินสมรรถนะตามช่วงคะแนน_COMP_" + "_" + DateAndTime.Now.Year + DateAndTime.Now.Month.ToString().PadLeft(2, GL.chr0) + DateAndTime.Now.Day.ToString().PadLeft(2, GL.chr0) + ".pdf");
					B = C.StreamToByte(cc.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat));
					Response.BinaryWrite(B);
					break;
				case "EXCEL":
					Response.AddHeader("Content-Type", "application/vnd.ms-excel");
					Response.AppendHeader("Content-Disposition", "filename=ผลการประเมินสมรรถนะตามช่วงคะแนน_COMP_" + "_" + DateAndTime.Now.Year + DateAndTime.Now.Month.ToString().PadLeft(2, GL.chr0) + DateAndTime.Now.Day.ToString().PadLeft(2, GL.chr0) + ".xls");
					B = C.StreamToByte(cc.ExportToStream(CrystalDecisions.Shared.ExportFormatType.Excel));
					Response.BinaryWrite(B);
					break;
				default:

					break;
			}

		}
		public Print_RPT_C_DeptRANK()
		{
			Load += Page_Load;
		}
        private void Page_Unload(object sender, System.EventArgs e)
        {
            if (cc != null)
            {
                cc.Close();
                cc.Dispose();
            }
        }

	}
}

using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Xml.Linq;
using System.Diagnostics;
using System.Collections.Specialized;
using System.Configuration;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Caching;
using System.Web.SessionState;
using System.Web.Security;
using System.Web.Profile;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using CrystalDecisions.Web;
using CrystalDecisions.CrystalReports.Engine;
namespace VB
{

	public partial class Print_RPT_C_IDP : System.Web.UI.Page
	{


        GenericLib GL = new GenericLib();
		Converter C = new Converter();
        ReportDocument cc;
		protected void Page_Load(object sender, System.EventArgs e)
		{
			if ((Session["USER_PSNL_NO"] == null)) {
				ScriptManager.RegisterStartupScript(this.Page, typeof(string), "Alert", "alert('กรุณาลงชื่อเข้าใช้ระบบ');", true);
				Response.Redirect("../Login.aspx");
				return;
			}

			DataTable DT = ((DataTable)Session["RPT_C_IDP"]).Copy();
			if ((DT == null)) {
				ScriptManager.RegisterStartupScript(this.Page, typeof(string), "Alert", "alert('เลือกเงื่อนไขการรายงาน'); window.close();", true);
				return;
			}

			DT.Columns.Add("NewGroup", typeof(int));
			string LastDept = "";
			string LastType = "";
			string LastComp = "";

			for (int i = 0; i <= DT.Rows.Count - 1; i++) {
				DT.Rows[i]["COMP_Comp"] = DT.Rows[i]["COMP_Comp"].ToString().Replace("<br>", "\n");
				//---------- แยกหน่วยงาน---------
				if (DT.Rows[i]["DEPT_NAME"].ToString() != LastDept) {
					LastDept = DT.Rows[i]["DEPT_NAME"].ToString();
					DT.Rows[i]["NewGroup"] = 1;
				//---------- แยกประเภทสมรรถนะ---------
				} else if (LastType != DT.Rows[i]["COMP_Type_Name_TH"].ToString()) {
					LastType = DT.Rows[i]["COMP_Type_Name_TH"].ToString();
					DT.Rows[i]["NewGroup"] = 1;
				//---------- แยกสมรรถนะ---------
				} else if (LastComp != DT.Rows[i]["COMP_COMP"].ToString()) {
					LastComp = DT.Rows[i]["COMP_COMP"].ToString();
					DT.Rows[i]["NewGroup"] = 1;
				//---------- แยกคน---------
				} else {
					DT.Rows[i]["NewGroup"] = 0;
				}

			}
			DT.AcceptChanges();

			cc = new ReportDocument();
			cc.Load(Server.MapPath("../Report/RPT_C_IDP.rpt"));
			cc.SetDataSource(DT);
			cc.SetParameterValue("Title", Session["RPT_C_IDP_Title"]);

			CrystalReportViewer1.ReportSource = cc;
            byte[] B;

			switch (Request.QueryString["Mode"].ToUpper()) {
				case "PDF":
					Response.AddHeader("Content-Type", "application/pdf");
					Response.AppendHeader("Content-Disposition", "filename=รายงานวิเคราะห์ช่องว่างสมรรถนะ_" + DateAndTime.Now.Year + DateAndTime.Now.Month.ToString().PadLeft(2, GL.chr0) + DateAndTime.Now.Day.ToString().PadLeft(2, GL.chr0) + ".pdf");
					B = C.StreamToByte(cc.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat));
					Response.BinaryWrite(B);
					break;
				case "EXCEL":
					Response.AddHeader("Content-Type", "application/vnd.ms-excel");
					Response.AppendHeader("Content-Disposition", "filename=รายงานวิเคราะห์ช่องว่างสมรรถนะ_" + DateAndTime.Now.Year + DateAndTime.Now.Month.ToString().PadLeft(2, GL.chr0) + DateAndTime.Now.Day.ToString().PadLeft(2, GL.chr0) + ".xls");
					B = C.StreamToByte(cc.ExportToStream(CrystalDecisions.Shared.ExportFormatType.Excel));
					Response.BinaryWrite(B);
					break;
				default:

					break;
			}

		}
		public Print_RPT_C_IDP()
		{
			Load += Page_Load;
		}
        private void Page_Unload(object sender, System.EventArgs e)
        {
            if (cc != null)
            {
                cc.Close();
                cc.Dispose();
            }
        }
	}
}

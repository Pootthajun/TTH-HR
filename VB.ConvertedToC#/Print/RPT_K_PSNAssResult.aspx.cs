﻿using Microsoft.VisualBasic;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CrystalDecisions.Web;
using CrystalDecisions.CrystalReports.Engine;

namespace VB
{
    public partial class Print_RPT_K_PSNAssResult : System.Web.UI.Page
    {

        GenericLib GL = new GenericLib();
        Converter C = new Converter();
        ReportDocument cc;

        protected void Page_Load(object sender, EventArgs e)
        {
            if ((Session["USER_PSNL_NO"] == null))
            {
                ScriptManager.RegisterStartupScript(this.Page, typeof(string), "Alert", "alert('กรุณาลงชื่อเข้าใช้ระบบ');", true);
                Response.Redirect("../Login.aspx");
                return;
            }

            DataTable DT = (DataTable)Session["RPT_K_PSNAssResult"];
            if ((DT == null))
            {
                ScriptManager.RegisterStartupScript(this.Page, typeof(string), "Alert", "alert('เลือกเงื่อนไขการรายงาน'); window.close();", true);
                return;
            }

            DataTable KPI = DT.Copy();
            cc = new ReportDocument();
            cc.Load(Server.MapPath("../Report/RPT_K_PSNAssResult.rpt"));
            cc.SetDataSource(KPI);
            cc.SetParameterValue("Title", Session["RPT_K_PSNAssResult_Title"]);
            cc.SetParameterValue("SubTitle", Session["RPT_K_PSNAssResult_SubTitle"]);
            //Reporter
            cc.SetParameterValue("ReporterName", Session["USER_Full_Name"]);
            cc.SetParameterValue("ReporterNo", Session["USER_PSNL_NO"]);
            cc.SetParameterValue("PrintDate", DateTime.Today.Day.ToString() + "/" + DateTime.Today.Month.ToString() + (DateTime.Today.Year+543).ToString());
            //Selected_Level
            cc.SetParameterValue("Selected_Level", Session["RPT_K_PSNAssResult_Selected_Level"]);

            CrystalReportViewer1.ReportSource = cc;
            byte[] B;

            switch (Request.QueryString["Mode"].ToUpper())
            {
                case "PDF":
                    Response.AddHeader("Content-Type", "application/pdf");
                    Response.AppendHeader("Content-Disposition", "filename=จำนวนตัวชี้วัดของส่วนงาน_KPI_" + "_" + DateAndTime.Now.Year + DateAndTime.Now.Month.ToString().PadLeft(2, GL.chr0) + DateAndTime.Now.Day.ToString().PadLeft(2, GL.chr0) + ".pdf");
                    B = C.StreamToByte(cc.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat));
                    Response.BinaryWrite(B);
                    break;
                case "EXCEL":
                    Response.AddHeader("Content-Type", "application/vnd.ms-excel");
                    Response.AppendHeader("Content-Disposition", "filename=จำนวนตัวชี้วัดของส่วนงาน_KPI_" + "_" + DateAndTime.Now.Year + DateAndTime.Now.Month.ToString().PadLeft(2, GL.chr0) + DateAndTime.Now.Day.ToString().PadLeft(2, GL.chr0) + ".xls");
                    B = C.StreamToByte(cc.ExportToStream(CrystalDecisions.Shared.ExportFormatType.Excel));
                    Response.BinaryWrite(B);
                    break;
                default:

                    break;
            }
        }

        public Print_RPT_K_PSNAssResult()
		{
			Load += Page_Load;
            Unload += Page_Unload;
		}

        private void Page_Unload(object sender, System.EventArgs e)
        {
            if (cc != null)
            {
                cc.Close();
                cc.Dispose();
            }
        }

    }
}
using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Xml.Linq;
using System.Diagnostics;
using System.Collections.Specialized;
using System.Configuration;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Caching;
using System.Web.SessionState;
using System.Web.Security;
using System.Web.Profile;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using CrystalDecisions.Web;
using CrystalDecisions.CrystalReports.Engine;
namespace VB
{

	public partial class Print_RPT_A_Assessor_Assigned : System.Web.UI.Page
	{

		Converter C = new Converter();

		GenericLib GL = new GenericLib();
        public int R_Year
        {
            get { return Convert.ToInt32((Request.QueryString["R_Year"]).ToString()); }
        }

        ReportDocument cc;
		protected void Page_Load(object sender, System.EventArgs e)
		{
			if ((Session["USER_PSNL_NO"] == null)) {
				ScriptManager.RegisterStartupScript(this.Page, typeof(string), "Alert", "alert('กรุณาลงชื่อเข้าใช้ระบบ');", true);
				Response.Redirect("../Login.aspx");
				return;
			}

			cc = new ReportDocument();
			cc.Load(Server.MapPath("../Report/RPT_A_Assessor_Assigned.rpt"));
			DataTable DT = new DataTable();
            DT = (DataTable)Session["AssessmentSetting_MGR"];

			//-------------- หา List คนที่ถูก Assigned ในรอบนั้น ------------
            DataTable ASSIGNED = (DataTable)Session["AssessmentSetting_Assessor_Assigned_Assigned"];

			DT.Columns.Add("PSN_NAME");


			if (DT.Rows.Count > 0) {
				for (int i = 0; i <= DT.Rows.Count - 1; i++) {
					if (GL.IsEqualNull(DT.Rows[i]["MGR_PSNL_NO"])) {
						ASSIGNED.DefaultView.RowFilter = "ASSESSOR_POS='" + DT.Rows[i]["ASSESSOR_POS"] + "'";
						for (int j = 0; j <= ASSIGNED.DefaultView.Count - 1; j++) {
							DT.Rows[i]["PSN_NAME"] = ASSIGNED.DefaultView[j]["MGR_PSNL_NO"] + " : " + ASSIGNED.DefaultView[j]["MGR_PSNL_Fullname"];
						}
					}
				}
			}

			cc.SetDataSource(DT);
			DT.Columns.Remove("PSN_NAME");
			cc.SetParameterValue("Count_List", Session["AssessmentSetting_MGR_Count_List"]);
			//------------------------------
			CrystalReportViewer1.ReportSource = cc;
            byte[] B;
			switch (Request.QueryString["Mode"].ToUpper()) {
				case "PDF":
					Response.AddHeader("Content-Type", "application/pdf");
					Response.AppendHeader("Content-Disposition", "filename=รายงานแต่งตั้งผู้ประเมิน_" + R_Year + DateAndTime.Now.Year + DateAndTime.Now.Month.ToString().PadLeft(2, GL.chr0) + DateAndTime.Now.Day.ToString().PadLeft(2, GL.chr0) + ".pdf");
					B = C.StreamToByte(cc.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat));
					Response.BinaryWrite(B);
					break;
				case "EXCEL":
					Response.AddHeader("Content-Type", "application/vnd.ms-excel");
					Response.AppendHeader("Content-Disposition", "filename=รายงานแต่งตั้งผู้ประเมิน_" + R_Year + DateAndTime.Now.Year + DateAndTime.Now.Month.ToString().PadLeft(2, GL.chr0) + DateAndTime.Now.Day.ToString().PadLeft(2, GL.chr0) + ".xls");
					B = C.StreamToByte(cc.ExportToStream(CrystalDecisions.Shared.ExportFormatType.Excel));
					Response.BinaryWrite(B);
					break;
				default:

					break;
			}

		}
		public Print_RPT_A_Assessor_Assigned()
		{
			Load += Page_Load;
		}

        private void Page_Unload(object sender, System.EventArgs e)
        {
            if (cc != null)
            {
                cc.Close();
                cc.Dispose();
            }
        }


	}
}


using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Xml.Linq;
using System.Diagnostics;
using System.Collections.Specialized;
using System.Configuration;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Caching;
using System.Web.SessionState;
using System.Web.Security;
using System.Web.Profile;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using CrystalDecisions.Web;
using CrystalDecisions.CrystalReports.Engine;
namespace VB
{

	public partial class Print_RPT_A_LEAVE : System.Web.UI.Page
	{


		Converter C = new Converter();

		GenericLib GL = new GenericLib();
        ReportDocument cc;
		protected void Page_Load(object sender, System.EventArgs e)
		{
			if ((Session["USER_PSNL_NO"] == null)) {
				ScriptManager.RegisterStartupScript(this.Page, typeof(string), "Alert", "alert('กรุณาลงชื่อเข้าใช้ระบบ');", true);
				Response.Redirect("../Login.aspx");
				return;
			}

            DataTable DT = (DataTable)Session["RPT_A_LEAVE"];
			if ((DT == null)) {
				ScriptManager.RegisterStartupScript(this.Page, typeof(string), "Alert", "alert('เลือกเงื่อนไขการรายงาน'); window.close();", true);
				return;
			}

			//---------------- Format DT ---------------
			DataTable LEAVE = DT.Copy();
			LEAVE.Columns.Add("New_Sector", typeof(int));
			LEAVE.Columns.Add("New_Dept", typeof(int));

			LEAVE.Columns.Add("_Class", typeof(string));
			LEAVE.Columns.Add("_Day", typeof(string));
			LEAVE.Columns.Add("_ACTIVITY", typeof(string));
			LEAVE.Columns.Add("_HEALTH", typeof(string));

			string LastSector = "";
			string LastDept = "";
			for (int i = 0; i <= LEAVE.Rows.Count - 1; i++) {
				if (LEAVE.Rows[i]["SECTOR_NAME"].ToString() != LastSector) {
					LEAVE.Rows[i]["New_Sector"] = 1;
					LastSector = LEAVE.Rows[i]["SECTOR_NAME"].ToString();
					LastDept = "";
				}
				if (LEAVE.Rows[i]["DEPT_NAME"].ToString() != LastDept) {
					LEAVE.Rows[i]["New_Dept"] = 1;
					LastDept = LEAVE.Rows[i]["DEPT_NAME"].ToString();
				}

				LEAVE.Rows[i]["_CLASS"] = Convert.ToInt32(LEAVE.Rows[i]["PNPS_Class"]);

				string L_Day = GL.StringFormatNumber(LEAVE.Rows[i]["L_Day"], 1);
                double _tmp = Conversion.Val(L_Day);
                L_Day = _tmp.ToString();
				if (L_Day == "0")
					L_Day = "-";
				LEAVE.Rows[i]["_DAY"] = L_Day;

				string L_Health = GL.StringFormatNumber(LEAVE.Rows[i]["L_Health"], 1);
                _tmp = Conversion.Val(L_Health);
                L_Health = _tmp.ToString();
				if (L_Health == "0")
					L_Health = "-";
				LEAVE.Rows[i]["_HEALTH"] = L_Health;

				string L_Activity = GL.StringFormatNumber(LEAVE.Rows[i]["L_Activity"], 1);
                _tmp = Conversion.Val(L_Activity);
                L_Activity = _tmp.ToString();
				if (L_Activity == "0")
					L_Activity = "-";
				LEAVE.Rows[i]["_ACTIVITY"] = L_Activity;

				//-------------- ชื่อหน่วยงาน -------------
				if (LEAVE.Rows[i]["DEPT_NAME"].ToString() != LEAVE.Rows[i]["SECTOR_NAME"].ToString()) {
					LEAVE.Rows[i]["DEPT_NAME"] = LEAVE.Rows[i]["DEPT_NAME"].ToString().Replace(LEAVE.Rows[i]["SECTOR_NAME"].ToString(), "").Trim();
				}
			}

			cc = new ReportDocument();
			cc.Load(Server.MapPath("../Report/RPT_A_LEAVE.rpt"));
			cc.SetDataSource(LEAVE);
			cc.SetParameterValue("Title", Session["RPT_A_LEAVE_Title"]);
			cc.SetParameterValue("SubTitle", Session["RPT_A_LEAVE_SubTitle"]);

			CrystalReportViewer1.ReportSource = cc;
            byte[] B;
			switch (Request.QueryString["Mode"].ToUpper()) {
				case "PDF":
					Response.AddHeader("Content-Type", "application/pdf");
					Response.AppendHeader("Content-Disposition", "filename=รายงานสรุปวันลา_" + DateAndTime.Now.Year + DateAndTime.Now.Month.ToString().PadLeft(2, GL.chr0) + DateAndTime.Now.Day.ToString().PadLeft(2, GL.chr0) + ".pdf");
					B = C.StreamToByte(cc.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat));
					Response.BinaryWrite(B);
					break;
				case "EXCEL":
					Response.AddHeader("Content-Type", "application/vnd.ms-excel");
					Response.AppendHeader("Content-Disposition", "filename=รายงานสรุปวันลา__" + DateAndTime.Now.Year + DateAndTime.Now.Month.ToString().PadLeft(2, GL.chr0) + DateAndTime.Now.Day.ToString().PadLeft(2, GL.chr0) + ".xls");
					B = C.StreamToByte(cc.ExportToStream(CrystalDecisions.Shared.ExportFormatType.Excel));
					Response.BinaryWrite(B);
					break;
				default:

					break;
			}

		}
		public Print_RPT_A_LEAVE()
		{
			Load += Page_Load;
		}
        private void Page_Unload(object sender, System.EventArgs e)
        {
            if (cc != null)
            {
                cc.Close();
                cc.Dispose();
            }
        }
	}
}

﻿using Microsoft.VisualBasic;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CrystalDecisions.Web;
using CrystalDecisions.CrystalReports.Engine;

namespace VB
{
    public partial class Print_RPT_A_DEPTAssCompleted : System.Web.UI.Page
    {
        GenericLib GL = new GenericLib();
        Converter C = new Converter();
        ReportDocument cc;

        protected void Page_Load(object sender, EventArgs e)
        {
            if ((Session["USER_PSNL_NO"] == null))
            {
                ScriptManager.RegisterStartupScript(this.Page, typeof(string), "Alert", "alert('กรุณาลงชื่อเข้าใช้ระบบ');", true);
                Response.Redirect("../Login.aspx");
                return;
            }

            DataTable DT = (DataTable)Session["RPT_A_DEPTAssCompleted"];
            if ((DT == null))
            {
                ScriptManager.RegisterStartupScript(this.Page, typeof(string), "Alert", "alert('เลือกเงื่อนไขการรายงาน'); window.close();", true);
                return;
            }

            DataTable ALL = DT.Copy();
            cc = new ReportDocument();
            cc.Load(Server.MapPath("../Report/RPT_A_DEPTAssCompleted.rpt"));
            cc.SetDataSource(ALL);
            cc.SetParameterValue("Title", Session["RPT_A_DEPTAssCompleted_Title"]);
            cc.SetParameterValue("SubTitle", Session["RPT_A_DEPTAssCompleted_SubTitle"]);
            //Reporter
            cc.SetParameterValue("ReporterName", Session["USER_Full_Name"]);
            cc.SetParameterValue("ReporterNo", Session["USER_PSNL_NO"]);
            cc.SetParameterValue("PrintDate", DateTime.Today.Day.ToString() + "/" + DateTime.Today.Month.ToString() + "/" +(DateTime.Today.Year + 543).ToString());


            cc.SetParameterValue("COUNT_DEPT",ALL.Compute("COUNT(DEPT_NAME)",""));
            cc.SetParameterValue("COUNT_KPI_OK", ALL.Compute("COUNT(DEPT_NAME)", "KPI_OK=1"));
            cc.SetParameterValue("COUNT_KPI_LATE", ALL.Compute("COUNT(DEPT_NAME)", "KPI_Late=1"));
            cc.SetParameterValue("COUNT_COMP_OK", ALL.Compute("COUNT(DEPT_NAME)", "COMP_OK=1"));
            cc.SetParameterValue("COUNT_COMP_LATE", ALL.Compute("COUNT(DEPT_NAME)", "COMP_Late=1"));


            //Calculate summary
            string KPI_OK="";
            string KPI_Late="";
            string COMP_OK="";
            string COMP_Late="";            
            if (ALL.Rows.Count > 0)
            {
                KPI_OK = calculatepercent((Int32)ALL.Compute("COUNT(KPI_OK)", "KPI_OK=1"),ALL.Rows.Count) + " %";
                KPI_Late = calculatepercent((Int32)ALL.Compute("COUNT(KPI_Late)", "KPI_Late=1"), ALL.Rows.Count) + " %";
                COMP_OK = calculatepercent((Int32)ALL.Compute("COUNT(COMP_OK)", "COMP_OK=1"), ALL.Rows.Count) + " %";
                COMP_Late = calculatepercent((Int32)ALL.Compute("COUNT(COMP_Late)", "COMP_Late=1"), ALL.Rows.Count) + " %";
            }

            cc.SetParameterValue("KPI_OK",KPI_OK );
            cc.SetParameterValue("KPI_Late", KPI_Late);
            cc.SetParameterValue("COMP_OK", COMP_OK);
            cc.SetParameterValue("COMP_Late", COMP_Late);
            cc.SetParameterValue("KPI_Title", Session["RPT_A_DEPTAssCompleted_SubTitle_KPI_Title"]);
            cc.SetParameterValue("COMP_Title", Session["RPT_A_DEPTAssCompleted_SubTitle_COMP_Title"]);

            CrystalReportViewer1.ReportSource = cc;
            byte[] B;

            switch (Request.QueryString["Mode"].ToUpper())
            {
                case "PDF":
                    Response.AddHeader("Content-Type", "application/pdf");
                    Response.AppendHeader("Content-Disposition", "filename=รายงานการส่งคะแนนประเมินผล_KPI_Competency_" + "_" + DateAndTime.Now.Year + DateAndTime.Now.Month.ToString().PadLeft(2, GL.chr0) + DateAndTime.Now.Day.ToString().PadLeft(2, GL.chr0) + ".pdf");
                    B = C.StreamToByte(cc.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat));
                    Response.BinaryWrite(B);
                    break;
                case "EXCEL":
                    Response.AddHeader("Content-Type", "application/vnd.ms-excel");
                    Response.AppendHeader("Content-Disposition", "filename=รายงานการส่งคะแนนประเมินผล_KPI_Competency_" + "_" + DateAndTime.Now.Year + DateAndTime.Now.Month.ToString().PadLeft(2, GL.chr0) + DateAndTime.Now.Day.ToString().PadLeft(2, GL.chr0) + ".xls");
                    B = C.StreamToByte(cc.ExportToStream(CrystalDecisions.Shared.ExportFormatType.Excel));
                    Response.BinaryWrite(B);
                    break;
                default:

                    break;
            }
        }

        private string calculatepercent(int Num,int Total)
        {
            Double tmp = Num;
            Double total = Total;
            return (tmp * 100 / total).ToString("N");
        }

        public Print_RPT_A_DEPTAssCompleted()
		{
			Load += Page_Load;
            Unload += Page_Unload;
		}

        private void Page_Unload(object sender, System.EventArgs e)
        {
            if (cc != null)
            {
                cc.Close();
                cc.Dispose();
            }
        }

    }
}
using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Xml.Linq;
using System.Diagnostics;
using System.Collections.Specialized;
using System.Configuration;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Caching;
using System.Web.SessionState;
using System.Web.Security;
using System.Web.Profile;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using CrystalDecisions.Web;
using CrystalDecisions.CrystalReports.Engine;
namespace VB
{

	public partial class Print_RPT_IDP_PSN : System.Web.UI.Page
	{
        Converter C = new Converter();
        GenericLib GL = new GenericLib();
		HRBL BL = new HRBL();
        ReportDocument cc;
		public int R_Year {
			get { return Convert.ToInt32(Request.QueryString["R_Year"]); }
		}

		public int R_Round {
			get { return  Convert.ToInt32(Request.QueryString["R_Round"]); }
		}

		public string PSNL_NO {
			get { return Request.QueryString["PSNL_NO"]; }
		}

		public string Mode {
			get { return Request.QueryString["MODE"]; }
		}

		public int Status {
            get { return Convert.ToInt32((Request.QueryString["Status"].ToString())); }
		}
        
		protected void Page_Load(object sender, System.EventArgs e)
		{
			if ((Session["USER_PSNL_NO"] == null)) {
				ScriptManager.RegisterStartupScript(this.Page, typeof(string), "Alert", "alert('กรุณาลงชื่อเข้าใช้ระบบ');", true);
				Response.Redirect("../Login.aspx");
				return;
			}

			//------------------- Get Personal Info --------------------
			HRBL.PersonalInfo PSN = BL.GetAssessmentPersonalInfo(R_Year, R_Round, PSNL_NO, Status);

			//-------------- Get Assessor List ------------
			DropDownList DDL = new DropDownList();
			//Dim ASS_CODE As String = BL.Get_Selected_Assessor_Code(PSNL_NO, HRBL.AssessmentType.KPI, R_Year, R_Round)
			BL.BindDDLAssessor(DDL, R_Year, R_Round, PSNL_NO);

			//-------------------- IDP ---เฉพาะที่  AND Selected=1--------------------'
			DataTable GT1 = new DataTable();
			GT1.Columns.Add("PSNL_No", typeof(string), "'" + PSNL_NO.Replace("'", "''") + "'");
			GT1.Rows.Add();
            
            //string SQL_Filter = "";

            // SQL_Filter += " SELECT COMP_Comp FROM ( \n"; 
            // SQL_Filter += " SELECT DISTINCT @PSNL_No AS PSNL_No, \n";
            // SQL_Filter += " GAP.COMP_Type_Id,COMP_Type_Name_TH,COMP_Comp,GAP_No,GAP_ID,GAP_Name,GAP,Selected \n";
            // SQL_Filter += " FROM \n";
            // SQL_Filter += " ( \n";
            // SQL_Filter += " SELECT COMP_Type_Id,COMP_Comp,GAP_No,GAP_ID,GAP_Name,@Threshold-Final_Score GAP ,Selected  FROM vw_COMP_CORE_GAP  WHERE R_Year=@R_Year AND R_Round=@R_Round AND PSNL_NO=@PSNL_No   AND Selected=1 \n";
            // SQL_Filter += " UNION ALL \n";
            // SQL_Filter += " SELECT COMP_Type_Id,COMP_Comp,GAP_No,GAP_ID,GAP_Name,@Threshold-Final_Score GAP,Selected FROM vw_COMP_FN_GAP WHERE R_Year=@R_Year AND R_Round=@R_Round AND PSNL_NO=@PSNL_No   AND Selected=1 \n";
            // SQL_Filter += " UNION ALL \n";
            // SQL_Filter += " SELECT COMP_Type_Id,COMP_Comp,GAP_No,GAP_ID,GAP_Name,@Threshold-Final_Score GAP,Selected FROM vw_COMP_MGR_GAP WHERE R_Year=@R_Year AND R_Round=@R_Round AND PSNL_NO=@PSNL_No  AND Selected=1 \n";
            // SQL_Filter += " ) GAP  \n";
            // SQL_Filter += " LEFT JOIN tb_HR_COMP_Type ON GAP.COMP_Type_Id=tb_HR_COMP_Type.COMP_Type_Id \n";
            // SQL_Filter += " ) AS T \n";
            // SQL_Filter += " GROUP BY COMP_Comp \n";

                string SQL = "";
                SQL = " DECLARE @PSNL_No AS nvarchar(50)='" + PSNL_NO.Replace("'", "''") + "'\n";
                SQL += " DECLARE @R_Year As Int=" + R_Year + "\n";
                SQL += " DECLARE @R_Round As Int=" + R_Round + "\n";
                SQL += " DECLARE @Threshold AS INT =5\n"; 
 
                SQL += " SELECT DISTINCT @PSNL_No AS PSNL_No, \n";
                SQL += " GAP.COMP_Type_Id,COMP_Type_Order,COMP_Type_Name_TH,COMP_Comp,GAP_No,GAP_ID,GAP_Name,GAP,Selected \n";
                SQL += " FROM \n";
                SQL += " ( \n";
                SQL += " SELECT COMP_Type_Id, 'A' COMP_Type_Order,COMP_Comp,GAP_No,GAP_ID,GAP_Name,@Threshold-Final_Score GAP ,Selected  FROM vw_COMP_CORE_GAP  WHERE R_Year=@R_Year AND R_Round=@R_Round AND PSNL_NO=@PSNL_No    AND Selected=1 \n";
                SQL += " UNION ALL \n";
                SQL += " SELECT COMP_Type_Id, 'C' COMP_Type_Order,COMP_Comp,GAP_No,GAP_ID,GAP_Name,@Threshold-Final_Score GAP,Selected FROM vw_COMP_FN_GAP WHERE R_Year=@R_Year AND R_Round=@R_Round AND PSNL_NO=@PSNL_No   AND Selected=1 \n";
                SQL += " UNION ALL \n";
                SQL += " SELECT COMP_Type_Id, 'B' COMP_Type_Order,COMP_Comp,GAP_No,GAP_ID,GAP_Name,@Threshold-Final_Score GAP,Selected FROM vw_COMP_MGR_GAP WHERE R_Year=@R_Year AND R_Round=@R_Round AND PSNL_NO=@PSNL_No    AND Selected=1 \n";
                SQL += " ) GAP  \n";
                SQL += " LEFT JOIN tb_HR_COMP_Type ON GAP.COMP_Type_Id=tb_HR_COMP_Type.COMP_Type_Id \n";
 
                //SQL += " UNION ALL \n";

                //SQL += " SELECT DISTINCT @PSNL_No AS PSNL_No, \n";
                //SQL += " GAP.COMP_Type_Id,COMP_Type_Order,COMP_Type_Name_TH,COMP_Comp,GAP_No,GAP_ID,GAP_Name,GAP,Selected \n";
                //SQL += " FROM \n";
                //SQL += " ( \n";
                //SQL += " SELECT COMP_Type_Id, 'A' COMP_Type_Order,COMP_Comp,GAP_No,GAP_ID,GAP_Name,@Threshold-Final_Score GAP ,Selected  FROM vw_COMP_CORE_GAP  WHERE R_Year=@R_Year AND R_Round=@R_Round AND PSNL_NO=@PSNL_No AND Final_Score<@Threshold \n";
                //SQL += " UNION ALL \n";
                //SQL += " SELECT COMP_Type_Id, 'C' COMP_Type_Order,COMP_Comp,GAP_No,GAP_ID,GAP_Name,@Threshold-Final_Score GAP,Selected FROM vw_COMP_FN_GAP WHERE R_Year=@R_Year AND R_Round=@R_Round AND PSNL_NO=@PSNL_No AND Final_Score<@Threshold  \n";
                //SQL += " UNION ALL \n";
                //SQL += " SELECT COMP_Type_Id, 'B' COMP_Type_Order,COMP_Comp,GAP_No,GAP_ID,GAP_Name,@Threshold-Final_Score GAP,Selected FROM vw_COMP_MGR_GAP WHERE R_Year=@R_Year AND R_Round=@R_Round AND PSNL_NO=@PSNL_No AND Final_Score<@Threshold   \n";
                //SQL += " ) GAP  \n";
                //SQL += " LEFT JOIN tb_HR_COMP_Type ON GAP.COMP_Type_Id=tb_HR_COMP_Type.COMP_Type_Id \n";
                //SQL += " WHERE  COMP_Comp NOT IN (" + SQL_Filter  + ") \n";

                SQL += "  ORDER BY COMP_Type_Order,COMP_Comp,GAP_No,GAP_Name \n";




			SqlDataAdapter DA = new SqlDataAdapter(SQL, BL.ConnectionString());
			DataTable GT2 = new DataTable();
			DA.Fill(GT2);

			string Last_Comp_Type = "";
			string Last_Comp_No = "";
			int GAP_No = 0;
			for (int i = 0; i <= GT2.Rows.Count - 1; i++) {
                string _COMP_Type_Name_TH=GT2.Rows[i]["COMP_Type_Name_TH"].ToString();

                string _COMP_Comp = GT2.Rows[i]["COMP_Comp"].ToString();
                switch (GL.CINT(GT2.Rows[i]["COMP_Type_Id"]))
                    {
                        case 1:
                            GT2.Rows[i]["COMP_Type_Name_TH"] = "สมรรถนะหลัก (Core Competency)";
                            break;
                        case 2:
                            GT2.Rows[i]["COMP_Type_Name_TH"] = "สมรรถนะตามสายงาน (Functional Competency)";
                            break;
                        case 3:
                            GT2.Rows[i]["COMP_Type_Name_TH"] = "สมรรถนะตามสายระดับ (Managerial Competency)";
                            break;
                    }

                if (_COMP_Type_Name_TH != Last_Comp_Type)
                {
                    Last_Comp_Type = _COMP_Type_Name_TH;
                    Last_Comp_No = _COMP_Comp;
					GAP_No = 0;
                }
                else if (_COMP_Comp != Last_Comp_No)
                {
					Last_Comp_No = _COMP_Comp;
					GAP_No = 0;
				} else {
					GT2.Rows[i]["GAP"] = DBNull.Value;
				}
				GAP_No = GAP_No + 1;
				GT2.Rows[i]["GAP_No"] = GAP_No;
			}
			//------------ Delete Only One GAP_Name---------------'
			GT2.Columns.Add("TMP_GAP");

			for (int i = 0; i <= GT2.Rows.Count - 1; i++) {
				GT2.DefaultView.RowFilter = "COMP_Type_Id=" + GT2.Rows[i]["COMP_Type_Id"] + " AND COMP_Comp='" + GT2.Rows[i]["COMP_Comp"].ToString().Replace("'", "''") + "'";
                if (GT2.DefaultView.Count > 1)
                {
                    GT2.Rows[i]["TMP_GAP"] = GT2.Rows[i]["GAP_No"] + ". ";
                }
                else if (GT2.DefaultView.Count == 1)
                {
                    GT2.Rows[i]["TMP_GAP"] =  "1. ";

                }
			}
			GT2.Columns.Remove("GAP_No");
			GT2.Columns["TMP_GAP"].ColumnName = "GAP_No";
			GT2.DefaultView.RowFilter = "";

			HRBL.PersonalInfo ASS = BL.GetAssessmentPersonalInfo(R_Year, R_Round, ASSESSOR_CODE(DDL), Status);

			cc = new ReportDocument();
			cc.Load(Server.MapPath("../Report/RPT_IDP_PSN.rpt"));
            cc.SetDataSource(GT2);

			cc.SetParameterValue("R_Year", R_Year);
			cc.SetParameterValue("R_Round", R_Round);
			cc.SetParameterValue("PSNL_NO", PSNL_NO);
			cc.SetParameterValue("PSN_Name", PSN.PSNL_Fullname);
			cc.SetParameterValue("PSN_Dept", (PSN.DEPT_NAME != PSN.SECTOR_NAME ? Strings.Trim(PSN.DEPT_NAME.Replace(PSN.SECTOR_NAME, "")) : PSN.DEPT_NAME));
			cc.SetParameterValue("PSN_Pos", (string.IsNullOrEmpty(PSN.MGR_NAME) ? PSN.FN_NAME : PSN.MGR_NAME));
			cc.SetParameterValue("ASS_Name", ASS.PSNL_Fullname);
			cc.SetParameterValue("ASS_Pos", ASSESSOR_POS(DDL));
			cc.SetParameterValue("ASS_Dept", ASS.DEPT_NAME);
			cc.SetParameterValue("ASS_CODE", ASS.PSNL_No);
			cc.SetParameterValue("PSN_Class", Convert.ToInt32(PSN.PNPS_CLASS));
			cc.SetParameterValue("ASS_Class", Convert.ToInt32(ASS.PNPS_CLASS));
			cc.SetParameterValue("PSN_Sector", PSN.SECTOR_NAME);
            cc.SetParameterValue("TotalRecord", GT2.Rows.Count);
            String Date_GAP = BL.GetDateR_End(R_Year, R_Round);
            cc.SetParameterValue("Date_GAP", Date_GAP);


            ////------เพิ่ม parameter to Subreport GAP------
            //cc.SetParameterValue("PSN_Name", PSN.PSNL_Fullname, "GAP");
            //cc.SetParameterValue("ASS_Name", ASS.PSNL_Fullname, "GAP");
            //String Date_GAP = BL.GetDateR_End(R_Year, R_Round);
            //cc.SetParameterValue("Date_GAP", Date_GAP);  // IDP ช่วงวันที่พัฒนา  fix 6 เดือน ถัดจากวันปิดรอบ
          
            string PrintedDate = DateAndTime.Now.Day + " " + C.ToMonthShortTH(DateAndTime.Now.Month) + " " + (DateAndTime.Now.Year + 543);
			cc.SetParameterValue("PrintedDate", PrintedDate);
			//------------------------------
			CrystalReportViewer1.ReportSource = cc;
            byte[] B;

			switch (Request.QueryString["Mode"].ToUpper()) {
				case "PDF":
					Response.AddHeader("Content-Type", "application/pdf");
					Response.AppendHeader("Content-Disposition", "filename=IDP-" + PSNL_NO + "_" + DateAndTime.Now.Year + DateAndTime.Now.Month.ToString().PadLeft(2, GL.chr0) + DateAndTime.Now.Day.ToString().PadLeft(2, GL.chr0) + ".pdf");
					B = C.StreamToByte(cc.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat));
					Response.BinaryWrite(B);
					break;
				case "EXCEL":
					Response.AddHeader("Content-Type", "application/vnd.ms-excel");
					Response.AppendHeader("Content-Disposition", "filename=IDP-" + PSNL_NO + "_" + DateAndTime.Now.Year + DateAndTime.Now.Month.ToString().PadLeft(2, GL.chr0) + DateAndTime.Now.Day.ToString().PadLeft(2, GL.chr0) + ".xls");
					B = C.StreamToByte(cc.ExportToStream(CrystalDecisions.Shared.ExportFormatType.Excel));
					Response.BinaryWrite(B);
					break;
				default:
					break;
			}

		}

		private string ASSESSOR_CODE(DropDownList DDL)
		{
			try {
				string[] tmp = Strings.Split(DDL.Items[DDL.SelectedIndex].Value, ":::");
				return tmp[0];
			} catch (Exception ex) {
				return "";
			}
		}

		private string ASSESSOR_NAME(DropDownList DDL)
		{
			try {
				return DDL.Items[DDL.SelectedIndex].Text;
			} catch (Exception ex) {
				return "";
			}
		}
		private string ASSESSOR_POS(DropDownList DDL)
		{
			try {
				string[] tmp = Strings.Split(DDL.Items[DDL.SelectedIndex].Value, ":::");
				return tmp[1];
			} catch (Exception ex) {
				return "";
			}
		}
		private string ASSESSOR_DEPT(DropDownList DDL)
		{
			try {
				string[] tmp = Strings.Split(DDL.Items[DDL.SelectedIndex].Value, ":::");
				return tmp[2];
			} catch (Exception ex) {
				return "";
			}
		}
		public Print_RPT_IDP_PSN()
		{
			Load += Page_Load;
		}
        private void Page_Unload(object sender, System.EventArgs e)
        {
            if (cc != null)
            {
                cc.Close();
                cc.Dispose();
            }
        }


	}
}

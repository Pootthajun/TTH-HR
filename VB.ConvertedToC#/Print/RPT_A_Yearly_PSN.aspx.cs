using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Xml.Linq;
using System.Diagnostics;
using System.Collections.Specialized;
using System.Configuration;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Caching;
using System.Web.SessionState;
using System.Web.Security;
using System.Web.Profile;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using CrystalDecisions.Web;
using CrystalDecisions.CrystalReports.Engine;
namespace VB
{

	public partial class Print_RPT_A_Yearly_PSN : System.Web.UI.Page
	{
        Converter C = new Converter();
		GenericLib GL = new GenericLib();
		HRBL BL = new HRBL();
        ReportDocument cc;
		public int R_Year {
			get { return Convert.ToInt32((Request.QueryString["R_Year"]).ToString()); }
		}

		public string PSNL_NO {
            get { return Request.QueryString["PSNL_NO"].ToString (); }
		}

		public string Mode {
			get { return Request.QueryString["MODE"].ToString (); }
		}

		public int Round {
			get { return Convert.ToInt32((Request.QueryString["Round"])); }
		}

		public int Status {
            get { return Convert.ToInt32(Request.QueryString["Status"]); }
		}


        protected void Page_Load(object sender, System.EventArgs e)
        {
            if ((Session["USER_PSNL_NO"] == null))
            {
                ScriptManager.RegisterStartupScript(this.Page, typeof(string), "Alert", "alert('กรุณาลงชื่อเข้าใช้ระบบ');", true);
                Response.Redirect("../Login.aspx");
                return;
            }

            //------------------- Get Personal Info --------------------

            string SQL = "SELECT * FROM vw_RPT_Yearly_Result WHERE R_Year=" + R_Year + " AND PSNL_NO='" + PSNL_NO + "'";

            SqlDataAdapter DA = new SqlDataAdapter(SQL, BL.ConnectionString());
            DA.SelectCommand.CommandTimeout = 1000;
            DataTable DT = new DataTable();
            
            DA.Fill(DT);
            HRBL.PersonalInfo PSN = BL.GetAssessmentPersonalInfo(R_Year, 2, PSNL_NO, Status);
            if ((PSN.PSNL_No == null) || string.IsNullOrEmpty(PSN.PSNL_No))
                PSN = BL.GetAssessmentPersonalInfo(R_Year, 1, PSNL_NO, Status);

            cc = new ReportDocument();
            if (Round == 1)
            {
                cc.Load(Server.MapPath("../Report/RPT_A_Yearly_PSN_R1.rpt"));

                //PSN = BL.GetAssessmentPersonalInfo(R_Year, 1, PSNL_NO, Status);
                //cc.SetParameterValue("POS_NO", PSN.POS_NO);
                //cc.SetParameterValue("POS_Name", PSN.POS_Name);
                //cc.SetParameterValue("DEPT_NAME", PSN.DEPT_NAME);

            }
            else
            {
                cc.Load(Server.MapPath("../Report/RPT_A_Yearly_PSN.rpt"));
            }
            cc.SetDataSource(DT);

            //-------------- Get Assessor List ------------
            DropDownList DDL = new DropDownList();

            BL.BindDDLAssessor(DDL, R_Year, 2, PSNL_NO);
            if (DDL.Items.Count < 2)
            {
                BL.BindDDLAssessor(DDL, R_Year, 1, PSNL_NO);
            }


            //'-------------- เอา DDL มาคิด เพราะใน BL จะคิด Default ของผู้ประเมินมาใส่ใน DDL อยู่แล้ว-------------------

            cc.SetParameterValue("MGR_Name", ASSESSOR_NAME(DDL));
            cc.SetParameterValue("MGR_Pos", ASSESSOR_POS(DDL));
            if (Round == 1)
            {
                PSN = BL.GetAssessmentPersonalInfo(R_Year, 1, PSNL_NO, Status);
                cc.SetParameterValue("POS_NO", PSN.POS_NO);
                cc.SetParameterValue("POS_Name", PSN.POS_Name);
                cc.SetParameterValue("DEPT_NAME", PSN.DEPT_NAME);
            }
            //--------------- get SubReport Datsource-------------------------------
            SQL = " DECLARE @PSNL_No AS nvarchar(50)='" + PSNL_NO + "';\n";
            SQL += "  DECLARE @R_Year AS Int=" + R_Year + ";\n";
            if (DT.Rows.Count > 0)
            {
                SQL += "  DECLARE @POS_NO AS nvarchar(50)='" + DT.Rows[0]["POS_NO"] + "';\n";
            }
            SQL += " SELECT @R_Year R_Year,\n";
            SQL += "   @PSNL_No PSNL_No, \n";
            SQL += "   0 Checked,\n";
            SQL += "   Master_Rank.RANK_No,Rank_Thai,Master_Rank.RANK_Name,\n";
            SQL += "   CASE Master_Rank.RANK_No WHEN 1 THEN 0 ELSE MAX(ASS_Rank.RANK_Start) END RANK_Start,\n";
            SQL += "   CASE Master_Rank.RANK_No WHEN 5 THEN 500 ELSE MAX(ASS_Rank.RANK_End) END RANK_End\n";
            SQL += "    FROM \n";
            SQL += "    (\n";
            SQL += "    SELECT 5 AS RANK_No,'2.0 ขั้น' RANK_Name,'เท่ากับ ดีเยี่ยม' Rank_Thai\n";
            SQL += "    UNION ALL SELECT 4 AS RANK_No,'1.5 ขั้น' RANK_Name,'เท่ากับ ดีมาก' Rank_Thai\n";
            SQL += "    UNION ALL SELECT 3 AS RANK_No,'1.0 ขั้น' RANK_Name,'เท่ากับ ดี' Rank_Thai\n";
            SQL += "    UNION ALL SELECT 2 AS RANK_No,'0.5 ขั้น' RANK_Name,'เท่ากับ พอใช้' Rank_Thai\n";
            SQL += "    UNION ALL SELECT 1 AS RANK_No,'ไม่ผ่านเกณฑ์' RANK_Name,'เท่ากับ ควรปรับปรุง' Rank_Thai\n";
            SQL += "    ) Master_Rank\n";
            SQL += "    INNER JOIN vw_PN_PSNL_ALL PSN ON PSN.PSNL_NO=@PSNL_No\n";

            //--==========ต้องเอาตำแหน่ง ของรอบนั้นเพื่อหาว่าตำแหน่งไหนประเมิน ไปหาช่วงคะแนน=============================

            if (DT.Rows.Count > 0)
            {
                //// หาตำแหน่งผู้ประเมิน ในตำแหน่งของพนักงาน  เนื่องจากตำแหน่งดึงจาก Master ตอนตั้งรอบ
                //string Sql_Assessor_Pos = "";
                //Sql_Assessor_Pos += " select * From tb_HR_Assessor_Pos where POS_NO='" + DT.Rows[0]["POS_NO"] + "' ";
                //DataTable DT_Assessor_Pos = new DataTable();
                //SqlDataAdapter DA_Assessor_Pos = new SqlDataAdapter(Sql_Assessor_Pos, BL.ConnectionString());
                //DA_Assessor_Pos.Fill(DT_Assessor_Pos);

                //----หาตำแหน่งผู้ประเมินจากใบประเมิน แต่ละปี
                string Sql_Assessor_Pos = "";
                Sql_Assessor_Pos += " select PSN_ASSESSOR_POS From tb_HR_Assessment_PSN where psnl_no='" + PSNL_NO + "' AND R_Year=" + R_Year + " AND PSN_ASSESSOR_POS IS NOT NULL ";
                DataTable DT_Assessor_Pos = new DataTable();
                SqlDataAdapter DA_Assessor_Pos = new SqlDataAdapter(Sql_Assessor_Pos, BL.ConnectionString());
                DA_Assessor_Pos.Fill(DT_Assessor_Pos);

                if (DT_Assessor_Pos.Rows.Count > 0)
                {
                    //SQL += "  INNER JOIN tb_HR_Assessor_Pos Master_POS ON Master_POS.POS_NO=@POS_NO  \n";
                    //SQL += "    LEFT JOIN tb_HR_Assessor_Result_RANK ASS_Rank ON Master_Rank.RANK_No=ASS_Rank.RANK_No\n";
                    //SQL += "  \t\t\t\t\t\t\t\t\tAND ASS_Rank.R_Year=@R_Year AND ASS_Rank.ASSESSOR_POS=Master_POS.ASSESSOR_POS \n";
                    //SQL += "    \n";

                    SQL += "    LEFT JOIN tb_HR_Assessor_Result_RANK ASS_Rank ON Master_Rank.RANK_No=ASS_Rank.RANK_No\n";
                    SQL += "  \t\t\t\t\t\t\t\t\tAND ASS_Rank.R_Year=@R_Year AND ASS_Rank.ASSESSOR_POS=" + DT_Assessor_Pos.Rows[0]["PSN_ASSESSOR_POS"] + " \n";
                    SQL += "    \n";
                }
                else
                {
                    SQL += "    INNER JOIN tb_HR_Assessor_Pos Master_POS ON Master_POS.POS_NO=PSN.POS_NO\n";
                    SQL += "    LEFT JOIN tb_HR_Assessor_Result_RANK ASS_Rank ON Master_Rank.RANK_No=ASS_Rank.RANK_No\n";
                    SQL += "  \t\t\t\t\t\t\t\t\tAND ASS_Rank.R_Year=@R_Year AND ASS_Rank.ASSESSOR_POS='" + ASSESSOR_POS_No(DDL) + "' \n";
                    SQL += "    \n";
                }
            }
            else
            {
                SQL += "    INNER JOIN tb_HR_Assessor_Pos Master_POS ON Master_POS.POS_NO=PSN.POS_NO\n";
                SQL += "    LEFT JOIN tb_HR_Assessor_Result_RANK ASS_Rank ON Master_Rank.RANK_No=ASS_Rank.RANK_No\n";
                SQL += "  \t\t\t\t\t\t\t\t\tAND ASS_Rank.R_Year=@R_Year AND ASS_Rank.ASSESSOR_POS=Master_POS.ASSESSOR_POS \n";
                SQL += "    \n";

            }

            SQL += "    \n";
            SQL += "   GROUP BY Master_Rank.RANK_No,Rank_Thai,Master_Rank.RANK_Name --,ASS_Rank.RANK_Start,ASS_Rank.RANK_End \n";
            SQL += "   ORDER BY Master_Rank.RANK_No DESC\n";


            DataTable ST = new DataTable();
            DA = new SqlDataAdapter(SQL, BL.ConnectionString());
            DA.Fill(ST);

            var YearScore = 0;
            //--------------- Set Default Value------------
            if (DT.Rows.Count > 0)
                YearScore = Convert.ToInt32(DT.Rows[0]["Year_Score"]);



            cc.SetParameterValue("RANK_1", "");
            cc.SetParameterValue("RANK_2", "");
            cc.SetParameterValue("RANK_3", "");
            cc.SetParameterValue("RANK_4", "");
            cc.SetParameterValue("RANK_5", "");
            cc.SetParameterValue("Checked_Value", -1);
            for (int i = 0; i <= ST.Rows.Count - 1; i++)
            {
                string _rank = ST.Rows[i]["RANK_Start"].ToString() + " - " + ST.Rows[i]["RANK_End"].ToString();
                cc.SetParameterValue("RANK_" + ST.Rows[i]["RANK_No"], _rank);
                if (!GL.IsEqualNull(ST.Rows[i]["RANK_Start"]) & !GL.IsEqualNull(ST.Rows[i]["RANK_End"]))
                {
                    if (YearScore >= GL.CINT(ST.Rows[i]["RANK_Start"]) & YearScore <= GL.CINT(ST.Rows[i]["RANK_End"]))
                    {
                        if (GL.CINT(DT.Rows[0]["KPI_Status_1"]) == 0 | GL.CINT(DT.Rows[0]["KPI_Status_2"]) == 0 | GL.CINT(DT.Rows[0]["COMP_Status_1"]) == 0 | GL.CINT(DT.Rows[0]["COMP_Status_2"]) == 0)
                        {
                            cc.SetParameterValue("Checked_Value", 0);
                        }
                        else
                        {
                            cc.SetParameterValue("Checked_Value", ST.Rows[i]["RANK_No"]);
                        }
                        //cc.SetParameterValue("Checked_Value", ST.Rows(i).Item("RANK_No"))
                    }
                }
            }

            //------------------------------
            CrystalReportViewer1.ReportSource = cc;

            byte[] B;
            switch (Request.QueryString["Mode"].ToUpper())
            {
                case "PDF":
                    Response.AddHeader("Content-Type", "application/pdf");
                    Response.AppendHeader("Content-Disposition", "filename=ผลการประเมินประจำปี_" + R_Year + "_" + "-" + PSNL_NO + "_" + DateAndTime.Now.Year + DateAndTime.Now.Month.ToString().PadLeft(2, GL.chr0) + DateAndTime.Now.Day.ToString().PadLeft(2, GL.chr0) + ".pdf");
                    B = C.StreamToByte(cc.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat));
                    Response.BinaryWrite(B);
                    break;
                case "EXCEL":
                    Response.AddHeader("Content-Type", "application/vnd.ms-excel");
                    Response.AppendHeader("Content-Disposition", "filename=ผลการประเมินประจำปี_" + R_Year + "_" + "-" + PSNL_NO + "_" + DateAndTime.Now.Year + DateAndTime.Now.Month.ToString().PadLeft(2, GL.chr0) + DateAndTime.Now.Day.ToString().PadLeft(2, GL.chr0) + ".xls");
                    B = C.StreamToByte(cc.ExportToStream(CrystalDecisions.Shared.ExportFormatType.Excel));
                    Response.BinaryWrite(B);
                    break;
                default:

                    break;
            }

        }

		private string ASSESSOR_CODE(DropDownList DDL)
		{
			try {
				string[] tmp = Strings.Split(DDL.Items[DDL.SelectedIndex].Value, ":::");
				return tmp[0];
			} catch (Exception ex) {
				return "";
			}
		}

		private string ASSESSOR_NAME(DropDownList DDL)
		{
			try {
				return DDL.Items[DDL.SelectedIndex].Text;
			} catch (Exception ex) {
				return "";
			}
		}
		private string ASSESSOR_POS(DropDownList DDL)
		{
			try {
				string[] tmp = Strings.Split(DDL.Items[DDL.SelectedIndex].Value, ":::");
				return tmp[1];
			} catch (Exception ex) {
				return "";
			}
		}

        private string ASSESSOR_POS_No(DropDownList DDL)
        {
            try
            {
                string[] tmp = Strings.Split(DDL.Items[DDL.SelectedIndex].Value, ":::");
                // หาตำแหน่ง ณ วันที่ประเมิน 
                string Sql_Assessor_Pos = "";
                //Sql_Assessor_Pos += " select POS_NO From vw_PN_PSNL_ALL where PSNL_NO='" + tmp[0] + "' ";


                Sql_Assessor_Pos += " select ASSESSOR_POS From  vw_HR_ASSESSOR_PSN where MGR_PSNL_NO= '" + tmp[0] + "' AND PSN_PSNL_NO= '" + PSNL_NO + "' AND R_year =" + R_Year + " ";
                DataTable DT_Assessor_Pos = new DataTable();
                SqlDataAdapter DA_Assessor_Pos = new SqlDataAdapter(Sql_Assessor_Pos, BL.ConnectionString());
                DA_Assessor_Pos.Fill(DT_Assessor_Pos);
                if (DT_Assessor_Pos.Rows.Count > 0)
                {
                    return DT_Assessor_Pos.Rows[0]["ASSESSOR_POS"].ToString();
                }
                else
                {
                    return "";
                }
            }
            catch (Exception ex)
            {
                return "";
            }
        }

		private string ASSESSOR_DEPT(DropDownList DDL)
		{
			try {
				string[] tmp = Strings.Split(DDL.Items[DDL.SelectedIndex].Value, ":::");
				return tmp[2];
			} catch (Exception ex) {
				return "";
			}
		}
		public Print_RPT_A_Yearly_PSN()
		{
			Load += Page_Load;
		}
        private void Page_Unload(object sender, System.EventArgs e)
        {
            if (cc != null)
            {
                cc.Close();
                cc.Dispose();
            }
        }
	}
}

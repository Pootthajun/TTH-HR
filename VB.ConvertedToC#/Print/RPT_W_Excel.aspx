<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="RPT_W_Excel.aspx.cs" Inherits="VB.Print_RPT_W_Excel" %>

<%@ Register assembly="CrystalDecisions.Web, Version=13.0.2000.0, Culture=neutral, PublicKeyToken=692fbea5521e1304" namespace="CrystalDecisions.Web" tagprefix="CR"   %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>ตารางภาระกิจงาน</title>
    <meta charset="window-874" />
</head>
<body>
    <form id="formMain" runat="server">
    
    <table border="1" cellpadding="0" cellspacing="0">
		<thead>
			<tr>
				<th rowspan="2"><asp:Label ID="lblH1" runat="server" Text="ลำดับ"></asp:Label></th>
				<th rowspan="2"><asp:Label ID="lblH2" runat="server" Text="งาน"></asp:Label></th>
				<th rowspan="2"><asp:Label ID="lblH3" runat="server" Text="จำนวน<br/>ผู้ปฏิบัติ"></asp:Label></th>
				<th rowspan="2"><asp:Label ID="lblH4" runat="server" Text="จำนวน<br/>นาทีที่ใช้<br/>ในการ<br/>ปฏิบัติ<br/>ต่อครั้ง"></asp:Label></th>
				<th rowspan="2"><asp:Label ID="lblH5" runat="server" Text="รวม"></asp:Label></th>
				<th rowspan="2"><asp:Label ID="lblH6" runat="server" Text="จำนวนครั้ง<br/>(ความถี่)<br/>ที่ใช้ใน<br/>การปฏิบัติ<br/>ใน 1 ปี"></asp:Label></th>
				<th rowspan="2"><asp:Label ID="lblH7" runat="server" Text="จำนวน<br/>นาทีที่ใช้<br/>ใน 1 ปี"></asp:Label></th>
				<th rowspan="2"><asp:Label ID="lblH8" runat="server" Text="ผลรวมเวลา<br/>ที่ใช้ในการปฏิบัติ"></asp:Label></th>
				<asp:Repeater ID="rptClass" OnItemDataBound="rptClass_ItemDataBound" runat="server">
					<ItemTemplate>
						<th id="thClass" runat="server">
							<asp:Label ID="lblClassName" runat="server"></asp:Label>                            
						</th>
					</ItemTemplate>
				</asp:Repeater>											        
			</tr>					            
			<tr>
				<asp:Repeater ID="rptPSN" OnItemDataBound="rptPSN_ItemDataBound" runat="server">
					<ItemTemplate>
						<th>
							<asp:Label ID="lblPSNName" runat="server"></asp:Label>
						</th>
					</ItemTemplate>
				</asp:Repeater>									                
			</tr>                                         
		</thead>
		<tbody>
			<asp:Repeater ID="rptJob" OnItemDataBound="rptJob_ItemDataBound" runat="server">
				<ItemTemplate>
									                   
						<tr>
							<td><asp:Label ID="lblNo" runat="server"></asp:Label></td>
							<td id="tdTask" runat="server"><asp:Label ID="lblTask" runat="server"></asp:Label></td>
							<td id="cellNumPerson" runat="server"><asp:Label id="lblNumPerson" runat="server"></asp:Label></td>
                            <td id="cellMinPerTime" runat="server"><asp:Label id="lblMinPerTime" runat="server"></asp:Label></td>
                            <td><asp:Label id="lblPersonMin" runat="server"></asp:Label></td>
                            <td id="cellTimePerYear" runat="server"><asp:Label id="lblTimePerYear" runat="server"></asp:Label></td>
                            <td id="cellTotalMin" runat="server" ><asp:Label id="lblTotalMin" runat="server" ></asp:Label></td>
                            <td id="cellSumMin" runat="server"><asp:Label id="lblSumMin" runat="server" ></asp:Label></td>
                            <asp:Repeater ID="rptSlot" runat="server">
                                <ItemTemplate>
                                    <td id="cellSlot" runat="server"><asp:Label id="lblSlot" runat="server"></asp:Label></td>
                                </ItemTemplate>
                            </asp:Repeater>
						</tr>
									                    
				</ItemTemplate>
				<FooterTemplate>
					<tr>
						<td>&nbsp;</td>
						<td colspan="5"><asp:Label ID="lblF1" runat="server"></asp:Label></td>
						<td><asp:Label ID="lblTotalMinPerYear" runat="server"></asp:Label></td>
						<td><asp:Label ID="lblSumMinPerYear" runat="server"></asp:Label></td>
						<asp:Repeater ID="rptMin" runat="server">
						<ItemTemplate>
							<td><asp:Label ID="lblMin" runat="server"></asp:Label></td>
						</ItemTemplate>
						</asp:Repeater>
					</tr>									                    
					<tr>
						<td>&nbsp;</td>
						<td colspan="5"><asp:Label ID="lblF2" runat="server"></asp:Label></td>
						<td><asp:Label ID="lblTotalHourPerYear" runat="server"></asp:Label></td>
						<td><asp:Label ID="lblSumHourPerYear" runat="server"></asp:Label></td>
						<asp:Repeater ID="rptHour" runat="server">
						<ItemTemplate>
							<td><asp:Label ID="lblHour" runat="server"></asp:Label></td>
						</ItemTemplate>
						</asp:Repeater>
					</tr>          
					<tr>
						<td>&nbsp;</td>
						<td colspan="5">FTE</td>
						<td><asp:Label ID="lblFTEYear" runat="server"></asp:Label></td>
						<td><asp:Label ID="lblFTESum" runat="server"></asp:Label></td>
						<asp:Repeater ID="rptFTE" runat="server">
						<ItemTemplate>
								<td><asp:Label ID="lblFTE" runat="server"></asp:Label></td>
						</ItemTemplate>
						</asp:Repeater>
					</tr>
				</FooterTemplate>
			</asp:Repeater>									            									   	       							
		</tbody>
	</table>
    </form>
</body>
</html>

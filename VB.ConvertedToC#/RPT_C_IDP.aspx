﻿<%@ Page  Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPage.Master" CodeBehind="RPT_C_IDP.aspx.cs" Inherits="VB.RPT_C_IDP" %>

<%@ Register src="WUC_DateReporter.ascx" tagname="WUC_DateReporter" tagprefix="uc1" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<%@ Register src="PageNavigation.ascx" tagname="PageNavigation" tagprefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<asp:ToolkitScriptManager ID="toolkit1" runat="server"></asp:ToolkitScriptManager>
<asp:UpdatePanel ID="UpdatePanel1" runat="server">      
<ContentTemplate>
<div class="container-fluid">
            	<!-- BEGIN PAGE HEADER-->
				<div class="row-fluid" id="fixedPageHeader">
					<div class="span12"> 
						<!-- BEGIN PAGE TITLE & BREADCRUMB-->
						<h3>รายงานวิเคราะห์ช่องว่างสมรรถนะ (GAP Analysis)<font color="blue"> (ScreenID : R-COMP-08)</font></h3>						
						<ul class="breadcrumb">
                            <li>
                            	<i class="icon-copy"></i> <a href="javascript:;">รายงาน</a><i class="icon-angle-right"></i>
                            </li>
                            <li>
                            	<i class="icon-copy"></i> <a href="javascript:;">การประเมินผลสมรรถนะ</a><i class="icon-angle-right"></i>
                            </li>
                            <li>
                            	<i class="icon-copy"></i> <a href="javascript:;">รายงานประจำรอบ</a><i class="icon-angle-right"></i>
                            </li>
                        	<li>
                        	    <i class="icon-stackexchange"></i> <a href="javascript:;">รายงานวิเคราะห์ช่องว่างสมรรถนะ</a>
                        	</li>                      	
                        	
                            <uc1:WUC_DateReporter ID="WUC_DateReporter1" runat="server" />
                        </ul>
						<!-- END PAGE TITLE & BREADCRUMB-->
									        
				     </div>

				
			    </div>
		        <!-- END PAGE HEADER-->
                
                <asp:Panel ID="pnlList" runat="server" Visible="True" DefaultButton="btnSearch">
				<div class="row-fluid">
					
					<div class="span12">
						<!-- BEGIN SAMPLE TABLE PORTLET-->
                                        <div class="row-fluid form-horizontal">
                			                  <div class="btn-group pull-left">
                                                    <asp:Button ID="btnSearch" OnClick="btnSearch_Click"  runat="server" CssClass="btn green" Text="ค้นหา" />
                                              </div> 
				                            <div class="btn-group pull-right">                                    
								                <button data-toggle="dropdown" class="btn dropdown-toggle" id="Button1">พิมพ์ <i class="icon-angle-down"></i></button>										
								                <ul class="dropdown-menu pull-right">                                                       
                                                    <li><a href="Print/RPT_C_IDP.aspx?Mode=PDF" target="_blank">รูปแบบ PDF</a></li>
                                                    <li><a href="Print/RPT_C_IDP.aspx?Mode=EXCEL" target="_blank">รูปแบบ Excel</a></li>											
										         </ul>
							                </div>
									     </div>       
						                 <div class="row-fluid form-horizontal">
						                             <div class="span3 ">
														<div class="control-group">
													        <label class="control-label" style="width:100px;"> รอบการประเมิน</label>
													        <div class="controls" style="margin-left:120px;">
														        <asp:DropDownList ID="ddlRound" runat="server" CssClass="medium m-wrap">
							                                    </asp:DropDownList>
													        </div>
												        </div>
													</div>
													
													 <div class="span3 ">
												        <div class="control-group" >
											                <label class="control-label" style="width:100px;"> หน่วยงาน</label>
											                <div class="controls" style="margin-left:120px;">
												                <asp:TextBox ID="txtDept" runat="server" CssClass="medium m-wrap" PlaceHolder="ชื่อ/รหัสหน่วยงาน"></asp:TextBox>		
											                </div>
										                </div>
											        </div>			   
										  </div>
												
												<div class="row-fluid form-horizontal" >
											         <div class="span3">
												        <div class="control-group" >
											                <label class="control-label" style="width:100px;"> ประเภทพนักงาน</label>
											                <div class="controls" style="margin-left:120px;">
												                <asp:DropDownList ID="ddlPSNType" runat="server" CssClass="medium m-wrap">
												                </asp:DropDownList>
											                </div>
										                </div>
											        </div>	
											        
											        <div class="span3 ">
												        <div class="control-group" >
											                <label class="control-label" style="width:100px;"> ระดับ</label>
											                <div class="controls" style="margin-left:120px;">
												                <asp:DropDownList ID="ddlClassGroupFrom" runat="server" Width ="80px" CssClass=" m-wrap">
												                </asp:DropDownList>
                                                                 - 
                                                                <asp:DropDownList ID="ddlClassGroupTo" runat="server" Width ="80px" CssClass=" m-wrap">
												                </asp:DropDownList>		
											                </div>
										                </div>
											        </div>
											        
											        <div class="span3 ">
												        <div class="control-group">
											                <label class="control-label" style="width:100px;"> สายงาน</label>
											                <div class="controls" style="margin-left:120px;">
												                <asp:DropDownList ID="ddlFN" runat="server"  CssClass="medium m-wrap">
												                </asp:DropDownList>
											                </div>
										                </div>
											        </div>	
											     </div>
											     
									            <div class="row-fluid form-horizontal">
											         <div class="span6">
												        <div class="control-group">
													        <label class="control-label" style="width:100px;"> สมรรถนะ</label>
													        <div class="controls" style="padding-top:8px; font-size:14px; margin-left:120px;">											                            
											                            <asp:CheckBox ID="chkCOMP1" runat="server" Text="" Checked="true" />หลัก (Core) &nbsp; &nbsp;
											                            <asp:CheckBox ID="chkCOMP3" runat="server" Text="" Checked="true" />สายระดับ (Managerial)&nbsp; &nbsp;
											                            <asp:CheckBox ID="chkCOMP2" runat="server" Text="" Checked="true" />สายงาน (Functional) 
											                </div>
												        </div>
                                                        <div class="control-group" >
											                <label class="control-label" style="width:100px;"> หัวข้อสมรรถนะ</label>
											                <div class="controls" style="margin-left:120px;">
												                <asp:TextBox ID="txtCOMP_COMP" runat="server" CssClass=" span12 m-wrap" PlaceHolder="หัวข้อสมรรถนะ" ></asp:TextBox>		
											                </div>
										                </div>
													</div>
													
													<div class="span5">
												        <div class="control-group">
													        <label class="control-label" style="width:100px;"> ช่องว่าง</label>
													        <div class="controls" style="padding-top:8px; font-size:14px; margin-left:120px;">
											                            
											                            <asp:CheckBox ID="chkGAP0" runat="server" Text="" Checked="true" />0 &nbsp; &nbsp;
											                            <asp:CheckBox ID="chkGAP1" runat="server" Text="" Checked="true" />-1 &nbsp; &nbsp;
											                            <asp:CheckBox ID="chkGAP2" runat="server" Text="" Checked="true" />-2 &nbsp; &nbsp;
											                            <asp:CheckBox ID="chkGAP3" runat="server" Text="" Checked="true" />-3 &nbsp; &nbsp;
											                            <asp:CheckBox ID="chkGAP4" runat="server" Text="" Checked="true" />-4 &nbsp; &nbsp;
											                </div>
												        </div>
													</div>
									            </div>
												
								            
							<div class="portlet-body no-more-tables">
								<%--<asp:Label ID="lblCountPSN" runat="server" Font-Size="14px" Font-Bold="true"></asp:Label> &nbsp;--%>
								<div style="text-align:center; width:100%; padding-top:10px; padding-bottom:20px; font-size:14px; font-weight:bold;">
								รายงานผลการวิเคราะห์ช่องว่างสมรรถนะ <asp:Label ID="lblTitle" runat="server"></asp:Label> 
								</div>
								
								<table class="table table-full-width table-bordered dataTable table-hover">
									<thead>
										<tr>
											<th style="text-align:center;" colspan="2"> สมรรถนะ</th>
											<th style="text-align:center;"> ชื่อ</th>
											<th style="text-align:center;"> ตำแหน่ง</th>
											<th style="text-align:center;"> ประเภท</th>
											<th style="text-align:center;"> ระดับ</th>
											<th style="text-align:center;"> คะแนน</th>
											<th style="text-align:center;"> ช่องว่าง</th>
											<th style="text-align:center;"> พิมพ์</th>											
										</tr>
									</thead>
									<tbody>
								<asp:Repeater ID="rptCOMP" OnItemDataBound="rptCOMP_ItemDataBound" runat="server">
							        <ItemTemplate>
							            <tr id="tdDept" runat="server">
							                <td colspan="9" style="text-align:center; font-weight:bold; font-size:18px; background-color:#EEEEEE;" >
							                    <asp:Label ID="lblOrganize" runat="server"></asp:Label>
							                </td>
							            </tr>
							            <tr id="tdType" runat="server">
							                <td colspan="9" style="text-align:left; font-size:14px; font-weight:bold;" >
							                    <asp:Label ID="lblCOMPType" runat="server"></asp:Label>
							                </td>
							            </tr>
							            <tr>
							                <td data-title="การประเมิน" id="td1" runat="server" style="width:50px; border-right:1px solid white;">&nbsp;</td>
							                <td data-title="การประเมิน" id="td2" runat="server" ><asp:Label ID="lblCOMPComp" style="border-left:none !important;" runat="server"></asp:Label></td>
							                <td data-title="ชื่อ" id="td3" runat="server"><asp:Label ID="lblNo" runat="server"></asp:Label> : <asp:Label ID="lblPSNName" runat="server"></asp:Label></td>
							                <td data-title="ตำแหน่ง" style="text-align:center;" id="td4" runat="server"><asp:Label ID="lblPSNPos" runat="server"></asp:Label></td>
							                <td data-title="ประเภท" style="text-align:center;" id="td5" runat="server"><asp:Label ID="lblPSNType" runat="server"></asp:Label></td>
							                <td data-title="ระดับ" style="text-align:center;" id="td6" runat="server"><asp:Label ID="lblClass" runat="server"></asp:Label></td>	
							                <td data-title="คะแนน" style="text-align:center; font-weight:bold; color:gray;" id="td7" runat="server"><asp:Label ID="lblScore" runat="server"></asp:Label></td>	
							                <td data-title="ช่องว่าง" style="text-align:center; color:Red; font-weight:bold;" id="td8" runat="server"><asp:Label ID="lblGAP" runat="server"></asp:Label></td>							                
									        <td data-title="พิมพ์ " style="text-align:center;" id="td9" runat="server">   
                                               <a class="btn mini blue" id="btnPrint" runat="server" title="พิมพ์" style="cursor:pointer;" target="_blank"><i class="icon-print">&nbsp;</i></a> 
                                            </td>
								        </tr>	
							        </ItemTemplate>
								</asp:Repeater>
																							
									</tbody>
								</table>
								<asp:PageNavigation ID="Pager" OnPageChanging="Pager_PageChanging" MaximunPageCount="10" PageSize="20" runat="server" />
							</div>
					</div>
                </div>  
                </asp:Panel>
                
</div>
</ContentTemplate>           
</asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ScriptPlaceHolder" Runat="Server">
</asp:Content>


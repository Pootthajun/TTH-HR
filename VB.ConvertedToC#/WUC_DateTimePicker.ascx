﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="WUC_DateTimePicker.ascx.cs" Inherits="VB.WUC_DateTimePicker" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>

<asp:TextBox CssClass="m-wrap small" ID="txtDisplay" runat="server" placeholder="เลือกวันที่"/>
<asp:LinkButton CssClass="DateTimeSpan" ID="lnkPopup" runat="server"><i class="icon-calendar" style="cursor:pointer !important; color:black;" ></i></asp:LinkButton>
<asp:CalendarExtender ID="AjaxCalendar" runat="server" PopupPosition="BottomRight" TargetControlID="txtValue" Format="dd/MM/yyyy" />

<asp:TextBox style="width:0px !important; height:0px !important;position:relative; left:-20px; top:10px; z-index:-10;" runat="server" ID="txtValue" onkeypress="return false;" onkeydown="return false;"></asp:TextBox>
using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Xml.Linq;
using System.Diagnostics;
using System.Collections.Specialized;
using System.Configuration;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Caching;
using System.Web.SessionState;
using System.Web.Security;
using System.Web.Profile;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
namespace VB
{

	public class textControlLib
	{


		HRBL BL = new HRBL();
		public void ImplementJavaMoneyText(TextBox Obj)
		{
			Obj.Attributes["OnChange"] = "this.value=formatmoney(this.value,'0','999999999999');";
			Obj.Style["Text-Align"] = "Right";
		}

		public void ImplementJavaIntegerText(TextBox Obj)
		{
			Obj.Attributes["OnChange"] = "this.value=formatinteger(this.value,'0','999999999999');";
			Obj.Style["Text-Align"] = "Right";
		}

		public void ImplementJavaFloatText(TextBox Obj)
		{
			Obj.Attributes["OnChange"] = "this.value=formatfloat(this.value,'0','999999999999');";
			Obj.Style["Text-Align"] = "Right";
		}

		public void ImplementJavaOnlyNumberText(TextBox Obj)
		{
			Obj.Attributes["OnChange"] = "this.value=formatonlynumber(this.value);";
		}

		public void ImplementPreventCalendarMinDate(DateTime mindate, TextBox TextBox, string captionText)
		{
			string tmpDate = BL.DateTimeToEngDate(mindate);
			TextBox.Attributes["onChange"] = "preventCalendarMinDate('" + tmpDate + "','" + TextBox.ClientID + "','" + captionText + "');";
		}

		public void ImplementPreventCalendarMinDateNotClearText(DateTime mindate, TextBox TextBox, string captionText, string oldValue)
		{
			string tmpDate = BL.DateTimeToEngDate(mindate);
			TextBox.Attributes["onChange"] = "preventCalendarMinDateNotClearText('" + tmpDate + "','" + TextBox.ClientID + "','" + captionText + "','" + oldValue + "');";
		}

	}
}

using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Xml.Linq;
using System.Diagnostics;
using System.Collections.Specialized;
using System.Configuration;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Caching;
using System.Web.SessionState;
using System.Web.Security;
using System.Web.Profile;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using iTextSharp.text.pdf;
using System.IO;
namespace VB
{

	public class ReportManagement
	{


		public void MergePDF(string[] PDFList, string OutputPath)
		{
			iTextSharp.text.Document.Compress = true;

			string PDFFileName = PDFList[0];
			PdfReader reader = new PdfReader(PDFFileName);
			iTextSharp.text.Document document = new iTextSharp.text.Document(reader.GetPageSizeWithRotation(1));

			FileStream fs = new FileStream(OutputPath, FileMode.Create);
			PdfCopy copy = new PdfCopy(document, fs);
			copy.SetPdfVersion(PdfWriter.PDF_VERSION_1_5);
			copy.CompressionLevel = PdfStream.BEST_SPEED;

			PdfStamper stamper = new PdfStamper(reader, fs);
			stamper.SetFullCompression();

			document.Open();

			//----------------- Read First File -------------
			for (int p = 1; p <= reader.NumberOfPages; p++) {
				copy.AddPage(copy.GetImportedPage(reader, p));
				copy.SetPdfVersion(PdfWriter.PDF_VERSION_1_5);
				copy.CompressionLevel = PdfStream.BEST_SPEED;
			}
			reader.Close();

			//-------------------Read Other File-------------
			for (int i = 1; i <= PDFList.Length - 1; i++) {
				reader = new PdfReader(PDFList[i]);
				for (int p = 1; p <= reader.NumberOfPages; p++) {
					copy.AddPage(copy.GetImportedPage(reader, p));
					copy.SetPdfVersion(PdfWriter.PDF_VERSION_1_5);
					copy.CompressionLevel = PdfStream.BEST_SPEED;
				}
				reader.Close();
			}

			document.Close();
			copy.Close();
			fs.Close();
			fs.Dispose();

			//------------------ Delete Temp File ------------
			for (int i = 0; i <= PDFList.Length - 1; i++) {
				try {
					File.Delete(PDFList[i]);
				} catch (Exception ex) {
				}
			}

		}

	}
}

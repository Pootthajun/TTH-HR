//using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.OracleClient;
using System.Linq;
using System.Xml.Linq;
using System.Diagnostics;
using System.Collections.Specialized;
using System.Configuration;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Caching;
using System.Web.SessionState;
using System.Web.Security;
using System.Web.Profile;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;

namespace VB
{

    public class HRBL
    {
        GenericLib GL = new GenericLib();
        Converter C = new Converter();

        public string ConnectionString()
        {
            object result = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            return (string)result;
        }

        public string OracleConnectionString()
        {
            object result = ConfigurationManager.ConnectionStrings["OracleConnectionString"].ConnectionString;
            return (string)result;
        }

        public string LDAP_PATH()
        {
            object result = ConfigurationManager.AppSettings["LDAP_PATH"];
            return (string)result;
        }

        public string SYSTEM_DOMAIN()
        {
            object result = ConfigurationManager.AppSettings["SYSTEM_DOMAIN"];
            return (string)result;
        }

        public double MasterFTE()
        {
            object result = ConfigurationManager.AppSettings["MASTER_FTE"];
            return GL.CDBL(result);
        }

        public string AppVersion()
        {
            return ConfigurationManager.AppSettings["VERSION"].ToString();
        }

        public bool Login_With_TTM_Credential()
        {
            object result = ConfigurationManager.AppSettings["Login_With_TTM_Credential"];
            return GL.CBOOL(result); ;
        }

        public int Start_Skip_Behavior_Round()
        {
            return GL.CINT(ConfigurationManager.AppSettings["Start_Skip_Behavior_Round"]);
        }

        #region "HR Round"
        public struct HR_Round_Structure
        {
            /// <summary>
            /// ปีที่ประเมิน
            /// </summary>
            public int Year;
            /// <summary>
            /// รอบที่ประเมิน
            /// </summary>
            public int Round;
        }

        public HR_Round_Structure LastRound()
        {
            string SQL = "SELECT Top 1 R_Year,R_Round FROM tb_HR_Round ORDER BY R_Year DESC,R_Round DESC";
            SqlDataAdapter DA = new SqlDataAdapter(SQL, ConnectionString());
            DataTable DT = new DataTable();
            DA.Fill(DT);

            HR_Round_Structure Result = new HR_Round_Structure();
            if (DT.Rows.Count == 0)
                return Result;
            Result.Year = GL.CINT(DT.Rows[0]["R_Year"]);
            Result.Round = GL.CINT(DT.Rows[0]["R_Round"]);
            return Result;
        }

        private HR_Round_Structure GetIncompletedRound()
        {
            string SQL = "SELECT R_Year,R_Round FROM tb_HR_Round WHERE ISNULL(R_Completed,0)=0 ORDER BY R_Year DESC,R_Round DESC";
            SqlDataAdapter DA = new SqlDataAdapter(SQL, ConnectionString());
            DataTable DT = new DataTable();
            DA.Fill(DT);

            HR_Round_Structure Result = new HR_Round_Structure();
            if (DT.Rows.Count == 0)
                return Result;
            Result.Year = GL.CINT(DT.Rows[0]["R_Year"]);
            Result.Round = GL.CINT(DT.Rows[0]["R_Round"]);
            return Result;
        }

        private int[] GetIncompletedYear()
        {
            string SQL = "SELECT DISTINCT R_Year FROM tb_HR_Round WHERE ISNULL(R_Completed,0)=0 ORDER BY R_Year DESC DESC";
            SqlDataAdapter DA = new SqlDataAdapter(SQL, ConnectionString());
            DataTable DT = new DataTable();
            DA.Fill(DT);

            int[] Result = {
				
			};
            if (DT.Rows.Count == 0)
                return Result;
            for (int i = 0; i <= DT.Rows.Count - 1; i++)
            {
                GenericLib GL = new GenericLib();
                GL.PushArray_Integer(Result, GL.CINT(DT.Rows[0]["R_Year"].ToString()));
            }
            return Result;
        }


        #endregion

        #region "DateFormat"
        //dd/MM/yyyy
        public bool IsEngDate(string TheDate)
        {
            string[] _tmp = GL.SplitString(TheDate, "/");
            if (_tmp.Length != 3)
                return false;

            double _tmp0 = GL.CDBL(_tmp[0].ToString());
            double _tmp1 = GL.CDBL(_tmp[1].ToString());
            double _tmp2 = GL.CDBL(_tmp[2].ToString());


            if (_tmp0 > 31 | _tmp0 < 1)
            {
                return false;
            }
            if (_tmp1 > 12 | _tmp1 < 1)
            {
                return false;
            }
            if (_tmp2 < 1900 | _tmp2 > 2100)
            {
                return false;
            }
            return true;
        }

        public bool IsThaiDate(string TheDate)
        {
            string[] _tmp = GL.SplitString(TheDate, "/");
            if (_tmp.Length != 3)
                return false;

            double _tmp0 = GL.CDBL(_tmp[0].ToString());
            double _tmp1 = GL.CDBL(_tmp[1].ToString());
            double _tmp2 = GL.CDBL(_tmp[2].ToString());


            if (_tmp0 > 31 | _tmp0 < 1)
            {
                return false;
            }
            if (_tmp1 > 12 | _tmp1 < 1)
            {
                return false;
            }
            if (_tmp2 < 2400 | _tmp2 > 2600)
            {
                return false;
            }
            return true;
        }


        public string EngToThaiDate(string EngDate)
        {
            if (!IsEngDate(EngDate))
                return "";
            string[] _tmp = GL.SplitString(EngDate, "/");
            return _tmp[0].PadLeft(2, GL.chr0) + "/" + _tmp[1].PadLeft(2, GL.chr0) + "/" + (Convert.ToInt32(_tmp[2]) + 543);
        }

        public string DateTimeToEngDate(DateTime TheDate)
        {
            return TheDate.Day.ToString().PadLeft(2, GL.chr0) + "/" + TheDate.Month.ToString().PadLeft(2, GL.chr0) + "/" + TheDate.Year;
        }

        public string DateTimeToThaiDate(DateTime TheDate)
        {
            return TheDate.Day.ToString().PadLeft(2, GL.chr0) + "/" + TheDate.Month.ToString().PadLeft(2, GL.chr0) + "/" + (TheDate.Year + 543);
        }

        public DateTime EngDateToDateTime(string EngDate)
        {
            if (!IsEngDate(EngDate))
            {
                return DateTime.FromOADate(0);
            }
            else
            {
                return DateTime.ParseExact(EngDate, "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);
            }
        }

        public string DateTimeToThaiDateTime(DateTime TheDate)
        {
            return TheDate.Day.ToString().PadLeft(2, GL.chr0) + "/" + TheDate.Month.ToString().PadLeft(2, GL.chr0) + "/" + (TheDate.Year + 543) + "    " + TheDate.Hour.ToString().PadLeft(2, GL.chr0) + ":" + TheDate.Minute.ToString().PadLeft(2, GL.chr0) + ":" + TheDate.Second.ToString().PadLeft(2, GL.chr0) + " " + string.Format("{0:tt}", TheDate);
        }




        #endregion

        #region "BindingControl"

        public class PersonalInfo
        {
            public string PSNL_No = "";
            public string PSNL_Fullname = "";
            public string PNPS_CLASS = "";
            public string CLSGP_ID = "";
            public string PSNL_TYPE = "";
            public string POS_NO = "";
            public string WAGE_TYPE = "";
            public string WAGE_NAME = "";
            public string SECTOR_CODE = "";
            public string DEPT_CODE = "";
            public string MINOR_CODE = "";
            public string SECTOR_NAME = "";
            public string DEPT_NAME = "";
            public string FN_ID = "";
            public string FLD_Name = "";
            public string FN_CODE = "";
            public string FN_Type = "";
            public string FN_NAME = "";
            public string MGR_CODE = "";
            public string MGR_NAME = "";
            public string POS_Name = "";
        }

        //public class AssessmentPeriod
        //{
        //    public const int Creating = 1;
        //    public const int Assessment = 2;
        //}

        public class DataManager
        {
            /// <summary>
            /// ตารางข้อมูลที่กำลังทำงานด้วย
            /// </summary>
            public DataTable Table;
            /// <summary>
            /// sqlDataAdaptor ที่ใช้จัดการข้อมูล
            /// </summary>
            public SqlDataAdapter Adaptor;
            /// <summary>
            /// คำสั่งที่ใช้ดึงข้อมูล
            /// </summary>
            public string SQL;
        }

        public class AssessmentStatus
        {
            /// <summary>
            /// ไม่พบแบบประเมิน
            /// </summary>
            public const int NotFound = -1;
            /// <summary>
            /// พนักงานสร้าง
            /// </summary>
            public const int Creating = 0;
            /// <summary>
            /// พนักงานส่งให้หัวหน้าอนุมัติแบบ
            /// </summary>
            public const int WaitCreatingApproved = 1;
            /// <summary>
            /// หัวหน้าส่งแบบประเมิน
            /// </summary>
            public const int CreatedApproved = 2;
            /// <summary>
            /// ระหว่างรอผลประเมินตนเอง
            /// </summary>
            public const int WaitAssessment = 3;
            /// <summary>
            /// รอหัวหน้าอนุมัติผลการประเมิน
            /// </summary>
            public const int WaitConfirmAssessment = 4;
            /// <summary>
            /// ประเมินเสร็จสมบูรณ์
            /// </summary>
            public const int AssessmentCompleted = 5;
        }

        public class CompetencyType
        {
            /// <summary>
            /// ไม่พบ
            /// </summary>
            public const int Unknow = -1;
            /// <summary>
            /// ทั้งหมด
            /// </summary>
            public const int All = 0;
            /// <summary>
            /// สมรรถนะหลัก
            /// </summary>
            public const int Core = 1;
            /// <summary>
            /// สมรรถนะตามสายงาน
            /// </summary>
            public const int Functional = 2;
            /// <summary>
            /// สมรรถนะตามสายระดับ
            /// </summary>
            public const int Managerial = 3;
        }

        public class AssessmentType
        {
            public const int KPI = 1;
            public const int Competency = 2;
        }

        public void KeepLoginLog(string PSNL_No)
        {
            SqlDataAdapter DA = new SqlDataAdapter("SELECT * FROM tb_HR_Login_Log WHERE 1=0", ConnectionString());
            DataTable DT = new DataTable();
            DA.Fill(DT);
            DataRow DR = DT.NewRow();
            DR["PSNL_NO"] = PSNL_No;
            DR["Last_Login"] = DateTime.Now;
            DT.Rows.Add(DR);
            SqlCommandBuilder cmd = new SqlCommandBuilder(DA);
            DA.Update(DT);
        }

        public int GetNewKPINo(int R_Year, int R_Round, string PNPS_PSNL_NO)
        {
            string SQL = "SELECT ISNULL(MAX(KPI_No),0)+1 KPI_No\n";
            SQL += " FROM tb_HR_KPI_Detail\n";
            SQL += " WHERE PSNL_NO='" + PNPS_PSNL_NO.Replace("'", "''") + "' AND R_Year=" + R_Year + " AND R_Round=" + R_Round + "\n";
            SqlDataAdapter DA = new SqlDataAdapter(SQL, ConnectionString());
            DataTable DT = new DataTable();
            DA.Fill(DT);
            return GL.CINT(DT.Rows[0][0]);
        }

        public int GetNewCORENo(int R_Year, int R_Round, int PSNL_CLASS_GROUP, string PSNL_TYPE)
        {
            string SQL = "SELECT ISNULL(MAX(CORE_No),0)+1 CORE_No";
            SQL += " FROM tb_HR_COMP_Master_Core";
            SQL += " WHERE R_Year=" + R_Year + " AND \n";
            SQL += "        R_Round=" + R_Round + " AND \n";
            SQL += "        CLSGP_ID=" + PSNL_CLASS_GROUP + " AND \n";
            SQL += "        PNPO_TYPE='" + PSNL_TYPE.Replace("'", "''") + "'";

            SqlDataAdapter DA = new SqlDataAdapter(SQL, ConnectionString());
            DataTable DT = new DataTable();
            DA.Fill(DT);
            return GL.CINT(DT.Rows[0][0]);
        }

        public int GetNewFNNo(int R_Year, int R_Round, int PSNL_CLASS_GROUP, string PSNL_TYPE, string FN_ID, string FN_Type)
        {
            string SQL = "SELECT ISNULL(MAX(FN_No),0)+1 FN_No";
            SQL += " FROM tb_HR_COMP_Master_FN";
            SQL += " WHERE R_Year=" + R_Year + " AND \n";
            SQL += "        R_Round=" + R_Round + " AND \n";
            SQL += "        CLSGP_ID=" + PSNL_CLASS_GROUP + " AND \n";
            SQL += "        PNPO_TYPE='" + PSNL_TYPE.Replace("'", "''") + "' AND ";
            SQL += "        FN_ID='" + FN_ID.Replace("'", "''") + "' AND ";
            SQL += "        FN_Type='" + FN_Type.Replace("'", "''") + "'";

            SqlDataAdapter DA = new SqlDataAdapter(SQL, ConnectionString());
            DataTable DT = new DataTable();
            DA.Fill(DT);
            return GL.CINT(DT.Rows[0][0]);
        }

        public int GetNewWorkLoadPSNID()
        {
            string SQL = "SELECT ISNULL(MAX(PSN_ID),0)+1 PSN_ID \n";
            SQL += " FROM tb_HR_Workload_PSN\n";

            SqlDataAdapter DA = new SqlDataAdapter(SQL, ConnectionString());
            DataTable DT = new DataTable();
            DA.Fill(DT);
            return GL.CINT(DT.Rows[0][0]);
        }

        public int GetNewWorkLoadSlotID()
        {
            string SQL = "SELECT ISNULL(MAX(Slot_ID),0)+1 Slot_ID \n";
            SQL += " FROM tb_HR_Workload_Slot\n";

            SqlDataAdapter DA = new SqlDataAdapter(SQL, ConnectionString());
            DataTable DT = new DataTable();
            DA.Fill(DT);
            return GL.CINT(DT.Rows[0][0]);
        }

        public int GetNewMGRNo(int R_Year, int R_Round, int PSNL_CLASS_GROUP, string PSNL_TYPE)
        {
            string SQL = "SELECT ISNULL(MAX(MGR_No),0)+1 MGR_No";
            SQL += " FROM tb_HR_COMP_Master_MGR";
            SQL += " WHERE R_Year=" + R_Year + " AND \n";
            SQL += "        R_Round=" + R_Round + " AND \n";
            SQL += "        CLSGP_ID=" + PSNL_CLASS_GROUP + " AND \n";
            SQL += "        PNPO_TYPE='" + PSNL_TYPE.Replace("'", "''") + "'";

            SqlDataAdapter DA = new SqlDataAdapter(SQL, ConnectionString());
            DataTable DT = new DataTable();
            DA.Fill(DT);
            return GL.CINT(DT.Rows[0][0]);
        }

        public int GetNewJobMappingID()
        {
            string SQL = "SELECT ISNULL(MAX(Job_ID),0)+1 Job_ID ";
            SQL += " FROM tb_HR_Job_Mapping";

            SqlDataAdapter DA = new SqlDataAdapter(SQL, ConnectionString());
            DataTable DT = new DataTable();
            DA.Fill(DT);
            return GL.CINT(DT.Rows[0][0]);
        }

        public int GetNewGAPID()
        {
            string SQL = "SELECT ISNULL(MAX(GAP_ID),0)+1 GAP_ID";
            SQL += " FROM vw_HR_GAP";

            SqlDataAdapter DA = new SqlDataAdapter(SQL, ConnectionString());
            DataTable DT = new DataTable();
            DA.Fill(DT);
            return GL.CINT(DT.Rows[0][0]);

        }

        public DataTable GetGAPDetailPSN(string PSNL_No, int COMP_TYPE, int R_Year, int R_Round)
        {

            string SQL = "SELECT * FROM ";
            string Filter = " WHERE PSNL_No='" + PSNL_No.Replace("'", "''") + "'  ";
            if (R_Year != 0)
            {
                Filter += " AND R_Year=" + R_Year;
            }
            if (R_Round != 0)
            {
                Filter += " AND R_Round=" + R_Round;
            }
            switch (COMP_TYPE)
            {
                case CompetencyType.Core:
                    SQL += " vw_COMP_CORE_GAP " + Filter;
                    break;
                case CompetencyType.Functional:
                    SQL += " vw_COMP_FN_GAP " + Filter;
                    break;
                case CompetencyType.Managerial:
                    SQL += " vw_COMP_MGR_GAP " + Filter;
                    break;
                case CompetencyType.All:
                    SQL += " (SELECT * FROM vw_COMP_CORE_GAP " + Filter + "\n";
                    SQL += " UNION ALL \n";
                    SQL += " SELECT * FROM vw_COMP_FN_GAP " + Filter + "\n";
                    SQL += " UNION ALL \n";
                    SQL += " SELECT * FROM vw_COMP_MGR_GAP " + Filter + "\n";
                    SQL += " ) A \n";
                    break;
                default:
                    SQL += " vw_COMP_MGR_GAP WHERE 1=0 " + Filter;
                    break;
            }

            SQL += " ORDER BY R_Year,R_Round,COMP_Type_Id,Master_No,GAP_No";
            SqlDataAdapter DA = new SqlDataAdapter(SQL, ConnectionString());
            DataTable DT = new DataTable();
            DA.Fill(DT);
            return DT;
        }

        public DataTable GetGAPDetailPSN(string PSNL_No, int COMP_TYPE)
        {
            return GetGAPDetailPSN(PSNL_No, COMP_TYPE, 0, 0);
        }

        public DataTable GetCOMPCoreHeader(int R_Year, int R_Round, int PSNL_CLASS_GROUP, int PSNL_TYPE)
        {
            string SQL = "SELECT PNPO_CLASS_Start,PNPO_CLASS_End,PSNL_Type_Name\n";
            SQL += " FROM vw_HR_COMP_Master_Core_Header \n";
            SQL += " WHERE R_Year=" + R_Year + " AND \n";
            SQL += "        R_Round=" + R_Round + " AND \n";
            SQL += "        CLSGP_ID=" + PSNL_CLASS_GROUP + " AND \n";
            SQL += "        PSNL_Type_Code=" + PSNL_TYPE;

            SqlDataAdapter DA = new SqlDataAdapter(SQL, ConnectionString());
            DataTable DT = new DataTable();
            DA.Fill(DT);

            return DT;

        }

        public DataTable GetDeptInfo(string DEPT_CODE, string MINOR_CODE)
        {
            string SQL = "SELECT DEPT_CODE,MINOR_CODE,DEPT_NAME\n";
            SQL += " FROM vw_HR_Department\n";
            SQL += " WHERE DEPT_CODE='" + DEPT_CODE.Replace("'", "''") + "' AND MINOR_CODE='" + MINOR_CODE.Replace("'", "''") + "' \n";
            SqlDataAdapter DA = new SqlDataAdapter(SQL, ConnectionString());
            DataTable DT = new DataTable();
            DA.Fill(DT);
            return DT;
        }

        public DataTable GetCOMPFNHeader(int R_Year, int R_Round, int PSNL_CLASS_GROUP, int PSNL_TYPE, string FN_ID, string FN_Type)
        {
            string SQL = "SELECT PNPO_CLASS_Start,PNPO_CLASS_End,PSNL_Type_Name,FN_ID,FN_Type,FN_Name\n";
            SQL += " FROM vw_HR_COMP_Master_FN_Header \n";
            SQL += " WHERE R_Year=" + R_Year + " AND \n";
            SQL += "        R_Round=" + R_Round + " AND \n";
            SQL += "        CLSGP_ID=" + PSNL_CLASS_GROUP + " AND \n";
            SQL += "        PSNL_Type_Code=" + PSNL_TYPE + " AND \n";
            SQL += "        FN_ID='" + FN_ID + "' AND \n";
            SQL += "        FN_Type='" + FN_Type + "'\n";

            SqlDataAdapter DA = new SqlDataAdapter(SQL, ConnectionString());
            DataTable DT = new DataTable();
            DA.Fill(DT);

            return DT;

        }

        public DataTable GetCOMPMGRHeader(int R_Year, int R_Round, int PSNL_CLASS_GROUP, int PSNL_TYPE)
        {
            string SQL = "SELECT PNPO_CLASS_Start,PNPO_CLASS_End,PSNL_Type_Name\n";
            SQL += " FROM vw_HR_COMP_Master_MGR_Header \n";
            SQL += " WHERE R_Year=" + R_Year + " AND \n";
            SQL += "        R_Round=" + R_Round + " AND \n";
            SQL += "        CLSGP_ID=" + PSNL_CLASS_GROUP + " AND \n";
            SQL += "        PSNL_Type_Code=" + PSNL_TYPE;

            SqlDataAdapter DA = new SqlDataAdapter(SQL, ConnectionString());
            DataTable DT = new DataTable();
            DA.Fill(DT);

            return DT;
        }


        public DataManager GetCOMPMasterCoreDetail(int R_Year, int R_Round, int PSNL_CLASS_GROUP, int PSNL_TYPE, int CORE_No)
        {
            string SQL = " SELECT R_Year,R_Round,CLSGP_ID,PNPO_TYPE,CORE_No,CORE_Comp,CORE_Std\n";
            SQL += "    ,CORE_Choice_1,CORE_Choice_2,CORE_Choice_3,CORE_Choice_4,CORE_Choice_5\n";
            SQL += "    ,Create_By,Create_Time,Update_By,Update_Time\n";
            SQL += "   FROM tb_HR_COMP_Master_Core\n";
            SQL += "   WHERE R_Year=" + R_Year + " AND R_Round=" + R_Round + " AND CLSGP_ID=" + PSNL_CLASS_GROUP + " AND PNPO_TYPE='" + PSNL_TYPE + "'\n";
            if (CORE_No != 0)
            {
                SQL += " AND CORE_No=" + CORE_No + "\n";
            }
            SQL += "   ORDER BY CORE_No\n";

            SqlDataAdapter DA = new SqlDataAdapter(SQL, ConnectionString());
            DataTable DT = new DataTable();
            DA.Fill(DT);

            DataManager DM = new DataManager();
            DM.SQL = SQL;
            DM.Table = DT;
            DM.Adaptor = DA;

            return DM;
        }

        public DataManager GetCOMPMasterCoreDetail(int R_Year, int R_Round, int PSNL_CLASS_GROUP, int PSNL_TYPE)
        {
            return GetCOMPMasterCoreDetail(R_Year, R_Round, PSNL_CLASS_GROUP, PSNL_TYPE, 0);
        }

        public DataManager GetCOMPMasterFNDetail(int R_Year, int R_Round, int PSNL_CLASS_GROUP, int PSNL_TYPE, string FN_ID, string FN_Type, int FN_No)
        {
            string SQL = " SELECT R_Year,R_Round,CLSGP_ID,PNPO_TYPE,FN_ID,FN_Type,FN_No,FN_Comp,FN_Std\n";
            SQL += "    ,FN_Choice_1,FN_Choice_2,FN_Choice_3,FN_Choice_4,FN_Choice_5\n";
            SQL += "    ,Create_By,Create_Time,Update_By,Update_Time\n";
            SQL += "   FROM tb_HR_COMP_Master_FN\n";
            SQL += "   WHERE R_Year=" + R_Year + " AND R_Round=" + R_Round + " AND CLSGP_ID=" + PSNL_CLASS_GROUP + " AND PNPO_TYPE='" + PSNL_TYPE + "'\n";
            SQL += "   AND FN_ID='" + FN_ID + "' AND FN_Type='" + FN_Type + "'\n";
            if (FN_No != 0)
            {
                SQL += " AND FN_No=" + FN_No + "\n";
            }
            SQL += "   ORDER BY FN_No\n";

            SqlDataAdapter DA = new SqlDataAdapter(SQL, ConnectionString());
            DataTable DT = new DataTable();
            DA.Fill(DT);

            DataManager DM = new DataManager();
            DM.SQL = SQL;
            DM.Table = DT;
            DM.Adaptor = DA;

            return DM;
        }

        public DataManager GetCOMPMasterFNDetail(int R_Year, int R_Round, int PSNL_CLASS_GROUP, int PSNL_TYPE, string FN_ID, string FN_Type)
        {
            return GetCOMPMasterFNDetail(R_Year, R_Round, PSNL_CLASS_GROUP, PSNL_TYPE, FN_ID, FN_Type, 0);
        }

        public DataManager GetCOMPMasterMGRDetail(int R_Year, int R_Round, int PSNL_CLASS_GROUP, int PSNL_TYPE, int MGR_No)
        {
            string SQL = " SELECT R_Year,R_Round,CLSGP_ID,PNPO_TYPE,MGR_No,MGR_Comp,MGR_Std\n";
            SQL += "    ,MGR_Choice_1,MGR_Choice_2,MGR_Choice_3,MGR_Choice_4,MGR_Choice_5\n";
            SQL += "    ,Create_By,Create_Time,Update_By,Update_Time\n";
            SQL += "   FROM tb_HR_COMP_Master_MGR\n";
            SQL += "   WHERE R_Year=" + R_Year + " AND R_Round=" + R_Round + " AND CLSGP_ID=" + PSNL_CLASS_GROUP + " AND PNPO_TYPE='" + PSNL_TYPE + "'\n";
            if (MGR_No != 0)
            {
                SQL += " AND MGR_No=" + MGR_No + "\n";
            }
            SQL += "   ORDER BY MGR_No\n";

            SqlDataAdapter DA = new SqlDataAdapter(SQL, ConnectionString());
            DataTable DT = new DataTable();
            DA.Fill(DT);

            DataManager DM = new DataManager();
            DM.SQL = SQL;
            DM.Table = DT;
            DM.Adaptor = DA;

            return DM;
        }

        public DataManager GetCOMPMasterMGRDetail(int R_Year, int R_Round, int PSNL_CLASS_GROUP, int PSNL_TYPE)
        {
            return GetCOMPMasterMGRDetail(R_Year, R_Round, PSNL_CLASS_GROUP, PSNL_TYPE, 0);
        }

        public DataManager GetCOMPMasterCoreGAP(int R_Year, int R_Round, int PSNL_CLASS_GROUP, int PSNL_TYPE, int CORE_No)
        {

            string SQL = " SELECT R_Year,R_Round,CLSGP_ID,PNPO_TYPE,CORE_No,GAP_No,COMP.GAP_ID,GAP_Name\n";
            SQL += " FROM tb_HR_COMP_Master_Core_GAP COMP \n";
            SQL += " INNER JOIN vw_HR_GAP GAP ON COMP.GAP_ID=GAP.GAP_ID \n";
            SQL += " WHERE R_Year=" + R_Year + " AND R_Round=" + R_Round + " AND CLSGP_ID=" + PSNL_CLASS_GROUP + " AND PNPO_TYPE='" + PSNL_TYPE + "'\n";
            if (CORE_No != 0)
            {
                SQL += " AND CORE_No=" + CORE_No + "\n";
            }
            SQL += " AND GAP.Is_Active=1\n";
            SQL += " ORDER BY CORE_No,GAP_No\n";

            SqlDataAdapter DA = new SqlDataAdapter(SQL, ConnectionString());
            DataTable DT = new DataTable();
            DA.Fill(DT);
            DataManager DM = new DataManager();
            DM.SQL = SQL;
            DM.Table = DT;
            DM.Adaptor = DA;

            return DM;

        }

        public DataManager GetCOMPMasterCoreGAP(int R_Year, int R_Round, int PSNL_CLASS_GROUP, int PSNL_TYPE)
        {
            return GetCOMPMasterCoreGAP(R_Year, R_Round, PSNL_CLASS_GROUP, PSNL_TYPE, 0);
        }

        public DataManager GetCOMPMasterFNGAP(int R_Year, int R_Round, int PSNL_CLASS_GROUP, int PSNL_TYPE, string FN_ID, string FN_Type, int FN_No)
        {

            string SQL = " SELECT R_Year,R_Round,CLSGP_ID,PNPO_TYPE,FN_ID,FN_Type,FN_No,GAP_No,COMP.GAP_ID,GAP_Name\n";
            SQL += " FROM tb_HR_COMP_Master_FN_GAP COMP\n";
            SQL += " INNER JOIN vw_HR_GAP GAP ON COMP.GAP_ID=GAP.GAP_ID \n";
            SQL += " WHERE R_Year=" + R_Year + " AND R_Round=" + R_Round + " AND CLSGP_ID=" + PSNL_CLASS_GROUP + " AND PNPO_TYPE='" + PSNL_TYPE + "'\n";
            SQL += " AND FN_ID='" + FN_ID + "' AND FN_Type='" + FN_Type + "' \n";
            if (FN_No != 0)
            {
                SQL += " AND FN_No=" + FN_No + "\n";
            }
            SQL += " AND GAP.Is_Active=1\n";
            SQL += " ORDER BY FN_No,GAP_No\n";

            SqlDataAdapter DA = new SqlDataAdapter(SQL, ConnectionString());
            DataTable DT = new DataTable();
            DA.Fill(DT);
            DataManager DM = new DataManager();
            DM.SQL = SQL;
            DM.Table = DT;
            DM.Adaptor = DA;

            return DM;

        }

        public DataManager GetCOMPMasterFNGAP(int R_Year, int R_Round, int PSNL_CLASS_GROUP, int PSNL_TYPE, string FN_ID, string FN_Type)
        {
            return GetCOMPMasterFNGAP(R_Year, R_Round, PSNL_CLASS_GROUP, PSNL_TYPE, FN_ID, FN_Type, 0);
        }

        public DataManager GetCOMPMasterMGRGAP(int R_Year, int R_Round, int PSNL_CLASS_GROUP, int PSNL_TYPE, int MGR_No)
        {

            string SQL = " SELECT R_Year,R_Round,CLSGP_ID,PNPO_TYPE,MGR_No,GAP_No,COMP.GAP_ID,GAP_Name\n";
            SQL += " FROM tb_HR_COMP_Master_MGR_GAP COMP\n";
            SQL += " INNER JOIN vw_HR_GAP GAP ON COMP.GAP_ID=GAP.GAP_ID \n";
            SQL += " WHERE R_Year=" + R_Year + " AND R_Round=" + R_Round + " AND CLSGP_ID=" + PSNL_CLASS_GROUP + " AND PNPO_TYPE='" + PSNL_TYPE + "'\n";
            if (MGR_No != 0)
            {
                SQL += " AND MGR_No=" + MGR_No + "\n";
            }
            SQL += " AND GAP.Is_Active=1\n";
            SQL += " ORDER BY MGR_No,GAP_No\n";

            SqlDataAdapter DA = new SqlDataAdapter(SQL, ConnectionString());
            DataTable DT = new DataTable();
            DA.Fill(DT);

            DataManager DM = new DataManager();
            DM.SQL = SQL;
            DM.Table = DT;
            DM.Adaptor = DA;

            return DM;

        }

        public DataManager GetCOMPMasterMGRGAP(int R_Year, int R_Round, int PSNL_CLASS_GROUP, int PSNL_TYPE)
        {
            return GetCOMPMasterMGRGAP(R_Year, R_Round, PSNL_CLASS_GROUP, PSNL_TYPE, 0);
        }

        public DataTable GetCOMPBundle(string PSNL_NO, int R_Year, int R_Round, int PSNL_CLASS_GROUP, int PSNL_TYPE, string FN_ID, string FN_Type, int Only_COMP_Type, int Only_Master_No)
        {
            string SQL = "";
           //  หา Status ของใบประเมิน COPM ของพนักงาน 
            int COPM_Status = GetCOMPStatus(R_Year, R_Round, PSNL_NO);
            if (COPM_Status >= 1)  //ข้อมูลแบบประเมิน competency และนำหนักเฉลี่ยของแบบประเมิน ฟรีซเมื่อสถานะเป็นรออนุมัติแบบประเมิน (1) เป็นต้นไป
            {
                // ดึงจากประวัติไม่มีการเปลี่ยนแปลงตาม Master  "sp_COMP_Bundle_Last_Round"
                SQL = "EXEC dbo.sp_COMP_Bundle_Last_Round '" + PSNL_NO.Replace("'", "''") + "'," + R_Year + "," + R_Round + "," + PSNL_CLASS_GROUP + "," + PSNL_TYPE + ",'" + FN_ID.Replace("'", "''") + "','" + FN_Type.Replace("'", "''") + "',";
            }
            else {
                // เปลี่ยนแปลงตาม Master  "sp_COMP_Bundle" ก่อนส่งแบบให้ผู้ประเมินอนุมัติ
                SQL = "EXEC dbo.sp_COMP_Bundle '" + PSNL_NO.Replace("'", "''") + "'," + R_Year + "," + R_Round + "," + PSNL_CLASS_GROUP + "," + PSNL_TYPE + ",'" + FN_ID.Replace("'", "''") + "','" + FN_Type.Replace("'", "''") + "',";

            }

            //string SQL = "EXEC dbo.sp_COMP_Bundle '" + PSNL_NO.Replace("'", "''") + "'," + R_Year + "," + R_Round + "," + PSNL_CLASS_GROUP + "," + PSNL_TYPE + ",'" + FN_ID.Replace("'", "''") + "','" + FN_Type.Replace("'", "''") + "',";

            if (Only_COMP_Type == CompetencyType.All)
            {
                SQL += "Null,\n";
            }
            else
            {
                SQL += Only_COMP_Type + ",\n";
            }
            if (Only_Master_No > 0)
            {
                SQL += Only_Master_No + "\n";
            }
            else
            {
                SQL += "Null\n";
            }
            SqlDataAdapter DA = new SqlDataAdapter(SQL, ConnectionString());
            DataTable DT = new DataTable();
            DA.Fill(DT);

            DT.Columns.Add("COMP_NO", typeof(int));
            for (int i = 0; i <= DT.Rows.Count - 1; i++)
            {
                DT.Rows[i]["COMP_No"] = i + 1;
            }

            return DT;
        }

        public DataTable GetCOMPBundle(string PSNL_NO, int R_Year, int R_Round, int PSNL_CLASS_GROUP, int PSNL_TYPE, string FN_ID, string FN_Type, int Only_COMP_Type)
        {
            return GetCOMPBundle(PSNL_NO, R_Year, R_Round, PSNL_CLASS_GROUP, PSNL_TYPE, FN_ID, FN_Type, Only_COMP_Type, 0);
        }

        public DataTable GetCOMPBundle(string PSNL_NO, int R_Year, int R_Round, int PSNL_CLASS_GROUP, int PSNL_TYPE, string FN_ID, string FN_Type)
        {
            return GetCOMPBundle(PSNL_NO, R_Year, R_Round, PSNL_CLASS_GROUP, PSNL_TYPE, FN_ID, FN_Type, CompetencyType.All);
        }

        public DataManager GetCOMPBehavior(string PSNL_NO, int R_Year, int R_Round, int Only_COMP_Type, int Only_Master_No)
        {
            string SQL = "SELECT PSNL_NO,R_Year,R_Round,COMP_Type_Id,Master_No,BHV_No,BHV_Content FROM tb_HR_COMP_Behavior ";
            SQL += " WHERE PSNL_NO='" + PSNL_NO.Replace("'", "''") + "' \n";
            SQL += " AND R_Year=" + R_Year + "\n";
            SQL += " AND R_Round=" + R_Round + "\n";
            if (Only_COMP_Type != CompetencyType.All)
            {
                SQL += " AND COMP_Type_Id=" + Only_COMP_Type + "\n";
            }
            if (Only_Master_No > 0)
            {
                //Master_No
                SQL += " AND Master_No=" + Only_Master_No + "\n";
            }

            SQL += " ORDER BY COMP_Type_Id,Master_No";
            SqlDataAdapter DA = new SqlDataAdapter(SQL, ConnectionString());
            DataTable DT = new DataTable();
            DA.Fill(DT);

            DataManager DM = new DataManager();
            DM.SQL = SQL;
            DM.Table = DT;
            DM.Adaptor = DA;

            return DM;
        }

        public DataManager GetCOMPBehavior(string PSNL_NO, int R_Year, int R_Round, int Only_COMP_Type)
        {
            return GetCOMPBehavior(PSNL_NO, R_Year, R_Round, Only_COMP_Type, 0);
        }

        public DataManager GetCOMPBehavior(string PSNL_NO, int R_Year, int R_Round)
        {
            return GetCOMPBehavior(PSNL_NO, R_Year, R_Round, CompetencyType.All);
        }

        public bool AutoAddBehavior(string PSNL_NO, int R_Year, int R_Round, int Only_COMP_Type, int Only_Master_No,int Score)
        {
            try
            {
                //------------- Clear All First----------
                string SQL = "DELETE FROM tb_HR_COMP_Behavior \n";
                SQL += " WHERE PSNL_NO='" + PSNL_NO.Replace("'", "''") + "' \n";
                SQL += " AND R_Year=" + R_Year + "\n";
                SQL += " AND R_Round=" + R_Round + "\n";
                SQL += " AND COMP_Type_Id=" + Only_COMP_Type + "\n";
                //Master_No
                SQL += " AND Master_No=" + Only_Master_No + "\n";
                SqlConnection CONN = new SqlConnection(ConnectionString());
                CONN.Open();
                SqlCommand COM = new SqlCommand();
                COM.Connection = CONN;
                COM.CommandText = SQL;
                COM.CommandType = CommandType.Text;
                COM.ExecuteNonQuery();
                CONN.Close();
                CONN.Dispose();

                DataManager DM = GetCOMPBehavior(PSNL_NO, R_Year, R_Round, Only_COMP_Type, Only_Master_No);
                //BHV_No
                for (int i = 0; i < Score; i++)
                {
                    DataRow DR = DM.Table.NewRow();
                    DR["PSNL_NO"] = PSNL_NO;
                    DR["R_Year"] = R_Year;
                    DR["R_Round"] = R_Round;
                    DR["COMP_Type_Id"] = Only_COMP_Type;
                    DR["Master_No"] = Only_Master_No;
                    DR["BHV_No"] = i + 1;
                    DR["BHV_Content"] = "[Blank]";
                    DM.Table.Rows.Add(DR);
                }
                SqlCommandBuilder cmd = new SqlCommandBuilder(DM.Adaptor);
                DM.Adaptor.Update(DM.Table);
            }catch(Exception e)
            {
                return false;
            }
            return true;
        }

        public int GetCOMPStatus(int R_Year, int R_Round, string PSNL_NO)
        {
            string SQL = "  SELECT COMP_Status \n";
            SQL += " FROM tb_HR_COMP_Header \n";
            SQL += " WHERE R_Year =" + R_Year + " And R_Round =" + R_Round + " AND PSNL_NO='" + PSNL_NO.Replace("'", "''") + "'\n";
            SqlDataAdapter DA = new SqlDataAdapter(SQL, ConnectionString());
            DataTable DT = new DataTable();
            DA.Fill(DT);
            if (DT.Rows.Count == 0)
            {
                return AssessmentStatus.NotFound;
            }
            else if (GL.IsEqualNull(DT.Rows[0]["COMP_Status"]))
            {
                return AssessmentStatus.Creating;
            }
            else
            {
                return GL.CINT(DT.Rows[0]["COMP_Status"]);
            }
        }

        public void Update_COMP_Status_To_Assessment_Period(string PSNL_NO, int R_Year, int R_Round)
        {
            string SQL = "EXEC dbo.sp_Update_COMP_Status_To_Assessment_Period ";
            if (!string.IsNullOrEmpty(PSNL_NO))
            {
                SQL += " '" + PSNL_NO.Replace("'", "''") + "',";
            }
            else
            {
                SQL += "NULL,";
            }
            if (R_Year != 0)
            {
                SQL += R_Year + ",";
            }
            else
            {
                SQL += "NULL,";
            }
            if (R_Round != 0)
            {
                SQL += R_Round;
            }
            else
            {
                SQL += "NULL";
            }

            SqlConnection Conn = new SqlConnection(ConnectionString());
            Conn.Open();
            SqlCommand Comm = new SqlCommand();
            var _with1 = Comm;
            _with1.Connection = Conn;
            _with1.CommandType = CommandType.Text;
            _with1.CommandText = SQL;
            _with1.CommandTimeout = 90;
            _with1.ExecuteNonQuery();
            _with1.Dispose();
            Conn.Close();
            Conn.Dispose();

        }

        public void Update_COMP_Status_To_Assessment_Period(string PSNL_NO, int R_Year)
        {
            Update_COMP_Status_To_Assessment_Period(PSNL_NO, R_Year, 0);
        }

        public void Update_COMP_Status_To_Assessment_Period(string PSNL_NO)
        {
            Update_COMP_Status_To_Assessment_Period(PSNL_NO, 0);
        }

        public void Update_COMP_Status_To_Assessment_Period()
        {
            Update_COMP_Status_To_Assessment_Period("");
        }

        //------update ข้อมูลผู้ประเมิน กรณี admin หรือพนักงาน ไม่ได้กดเข้าใบประเมิน ที่ตะต้องสร้างใน tb_HR_Actual_Assessor  ดังนั้น เข้าหน้าหัวหน้าจะประเมินลูกน้อง ระบบจะ create ให้สำหรับพนักงานที่ยังไม่กดเข้าใบประเมิน
        public void CreateHR_Actual_Assessor(int R_Year, int R_Round, string PSNL_NO, string MGR_PSNL_NO)
        {

            //----------------- Update Information For History PSN Structure ------------------
            SqlDataAdapter DA;
            DataTable DT;
            DataRow DR;
            DataTable PSN = new DataTable();
            string SQL = "SELECT * FROM vw_PN_PSNL WHERE PSNL_NO='" + PSNL_NO.Replace("'", "''") + "'";
            DA = new SqlDataAdapter(SQL, ConnectionString());
            DA.Fill(PSN);

            SQL = "SELECT * ";
            SQL += " FROM tb_HR_Assessment_PSN ";
            SQL += " WHERE PSNL_NO='" + PSNL_NO.Replace("'", "''") + "' AND R_Year=" + R_Year + " AND R_Round=" + R_Round;
            DA = new SqlDataAdapter(SQL, ConnectionString());
            DT = new DataTable();
            DA.Fill(DT);


            DataTable TMP = GetAssessorList(R_Year, R_Round, PSNL_NO);
            TMP.DefaultView.RowFilter = "MGR_PSNL_NO='" + MGR_PSNL_NO.Replace("'", "''") + "'";

            if (PSN.Rows.Count > 0)
            {
                if (DT.Rows.Count == 0)
                {
                    DR = DT.NewRow();
                    DR["PSNL_NO"] = PSNL_NO;
                    DR["R_Year"] = R_Year;
                    DR["R_Round"] = R_Round;
                    if (TMP.DefaultView.Count > 0)
                    {
                        try
                        {
                            DR["PSN_ASSESSOR_POS"] = TMP.DefaultView[0]["ASSESSOR_POS"];
                        }
                        catch
                        {
                        }
                    }
                    DR["PSN_PSNL_NO"] = PSNL_NO;
                    DR["PSN_PSNL_Fullname"] = PSN.Rows[0]["PSNL_Fullname"].ToString();
                    DT.Rows.Add(DR);
                }
                else
                {
                    DR = DT.Rows[0];
                }

                //============ป้องกันการ update ตำแหน่ง ณ สิ้นรอบประเมินและปิดรอบประเมิน======
                if (DT.Rows.Count == 0 | string.IsNullOrEmpty(DT.Rows[0]["PSN_PNPS_CLASS"].ToString()))
                {
                    DR["PSN_PNPS_CLASS"] = PSN.Rows[0]["PNPS_CLASS"].ToString();
                    DR["PSN_PSNL_TYPE"] = PSN.Rows[0]["PSNL_TYPE"].ToString();
                    DR["PSN_POS_NO"] = PSN.Rows[0]["POS_NO"].ToString();
                    DR["PSN_WAGE_TYPE"] = PSN.Rows[0]["WAGE_TYPE"].ToString();
                    DR["PSN_WAGE_NAME"] = PSN.Rows[0]["WAGE_NAME"].ToString();
                    DR["PSN_DEPT_CODE"] = PSN.Rows[0]["DEPT_CODE"].ToString();
                    DR["PSN_MINOR_CODE"] = PSN.Rows[0]["MINOR_CODE"].ToString();
                    DR["PSN_SECTOR_CODE"] = PSN.Rows[0]["SECTOR_CODE"].ToString().Substring(0, 2);

                    //DR["PSN_SECTOR_CODE"] =  StringSplitOptions.Left(PSN.Rows[0]["SECTOR_CODE"].ToString(), 2);
                    DR["PSN_SECTOR_NAME"] = PSN.Rows[0]["SECTOR_NAME"].ToString();
                    DR["PSN_DEPT_NAME"] = PSN.Rows[0]["DEPT_NAME"].ToString();
                    DR["PSN_FN_ID"] = PSN.Rows[0]["FN_ID"].ToString();
                    DR["PSN_FLD_Name"] = PSN.Rows[0]["FLD_Name"].ToString();
                    DR["PSN_FN_CODE"] = PSN.Rows[0]["FN_CODE"].ToString();
                    DR["PSN_FN_TYPE"] = PSN.Rows[0]["FN_TYPE"].ToString();
                    DR["PSN_FN_NAME"] = PSN.Rows[0]["FN_NAME"].ToString();
                    if (!GL.IsEqualNull(PSN.Rows[0]["MGR_CODE"]))
                    {
                        DR["PSN_MGR_CODE"] = PSN.Rows[0]["MGR_CODE"];
                    }
                    if (!GL.IsEqualNull(PSN.Rows[0]["MGR_NAME"]))
                    {
                        DR["PSN_MGR_NAME"] = PSN.Rows[0]["MGR_NAME"];
                    }
                }
                else { }
                try
                {
                    DR["PSN_PNPS_RETIRE_DATE"] = GetPSNRetireDate(PSNL_NO);
                }
                catch
                {
                }
                DR["Update_By"] = MGR_PSNL_NO;
                DR["Update_Time"] = DateTime.Now;
                SqlCommandBuilder cmd = new SqlCommandBuilder(DA);
                DA.Update(DT);




            }
        }

    //    select PSNL_NO from tb_HR_Actual_Assessor where MGR_PSNL_NO ='002316' AND R_Year=2560 AND R_Round =1
    //AND  PSNL_NO NOT IN (select PSN_PSNL_NO from vw_HR_ASSESSOR_PSN where MGR_PSNL_NO ='002316'  AND R_Year=2560  AND R_Round =1)



        //-----------ตรวจสอบสถนะ KPI  ถ้า อนุมัติแบบประเมินแล้วไม่ต้องดึงตำแหน่งใหม่ไปแสดง  และไม่มีการ update ข้อมูล-----------------
        public PersonalInfo GetAssessmentPersonalInfo(int R_Year, int R_Round, string PSNL_No, int Status)
        {

            PersonalInfo Result = new PersonalInfo();

            string SQL = "";
            SQL += " DECLARE @R_Year AS Int=" + R_Year + "\n";
            SQL += " DECLARE @R_Round As Int=" + R_Round + "\n";
            SQL += " DECLARE @PSNL_NO As nvarchar(6)='" + PSNL_No.Replace("'", "''") + "'\n";
            SQL += " \n";
            SQL += " SELECT DISTINCT\n";
            SQL += "   PSN.PSN_PSNL_NO PSNL_No,\n";
            SQL += "   PSN.PSN_PSNL_Fullname PSNL_Fullname,\n";
            SQL += "   PSN.PSN_PNPS_CLASS PNPS_CLASS,\n";
            SQL += "   PSN.PSN_PSNL_TYPE PSNL_TYPE,\n";
            SQL += "   PSN.PSN_POS_NO POS_NO,\n";
            SQL += "   PSN.PSN_WAGE_TYPE WAGE_TYPE,\n";
            SQL += "   PSN.PSN_WAGE_NAME WAGE_NAME,\n";
            SQL += "   PSN.PSN_SECTOR_CODE SECTOR_CODE,\n";
            SQL += "   PSN.PSN_DEPT_CODE DEPT_CODE,\n";
            SQL += "   PSN.PSN_MINOR_Code MINOR_Code,\n";
            SQL += "   PSN.PSN_SECTOR_NAME SECTOR_NAME,\n";
            SQL += "   PSN.PSN_DEPT_Name DEPT_Name,\n";
            SQL += "   PSN.PSN_FN_ID FN_ID,\n";
            SQL += "   PSN.PSN_FLD_Name FLD_Name,\n";
            SQL += "   PSN.PSN_FN_CODE FN_CODE,\n";
            SQL += "   PSN.PSN_FN_TYPE FN_TYPE, \n";
            SQL += "   PSN.PSN_FN_NAME FN_NAME,\n";
            SQL += "   PSN.PSN_MGR_CODE MGR_CODE,\n";
            SQL += "   PSN.PSN_MGR_NAME MGR_NAME,\n";
            SQL += "   ISNULL(CG.CLSGP_ID,0) CLSGP_ID  \n";
            SQL += "   ,ISNULL(PSN.PSN_MGR_NAME,PSN.PSN_FN_NAME) POS_Name  \n";
            SQL += "   FROM vw_HR_PSN_BY_Round PSN \n";
            SQL += "  \n";
            SQL += "   LEFT JOIN tb_HR_PNPO_CLASS_GROUP CG ON PSN.PSN_PNPS_CLASS BETWEEN CG.PNPO_CLASS_Start AND CG.PNPO_CLASS_End\n";
            SQL += "                                  AND CG.R_Year=@R_Year AND CG.R_Round=@R_Round\n";
            SQL += "     \n";
            SQL += "   WHERE PSN.PSN_PSNL_NO=@PSNL_NO AND PSN.R_Year=@R_Year AND PSN.R_Round=@R_Round\n";

            SqlDataAdapter DA = new SqlDataAdapter(SQL, ConnectionString());
            DataTable DT = new DataTable();
            DA.Fill(DT);


            if (DT.Rows.Count == 0)
            {
                SQL = " DECLARE @R_Year AS Int=" + R_Year + "\n";
                SQL += " DECLARE @R_Round As Int=" + R_Round + "\n";
                SQL += " DECLARE @PSNL_NO As nvarchar(6)='" + PSNL_No.Replace("'", "''") + "'\n";
                SQL += " \n";
                SQL += " SELECT \n";
                SQL += "  PSN.PSN_PSNL_NO PSNL_No,\n";
                SQL += "  PSN.PSN_PSNL_Fullname PSNL_Fullname,\n";
                SQL += "  PSN.PSN_PNPS_CLASS PNPS_CLASS,\n";
                SQL += "  PSN.PSN_PSNL_TYPE PSNL_TYPE,\n";
                SQL += "  PSN.PSN_POS_NO POS_NO,\n";
                SQL += "  PSN.PSN_WAGE_TYPE WAGE_TYPE,\n";
                SQL += "  PSN.PSN_WAGE_NAME WAGE_NAME,\n";
                SQL += "  PSN.PSN_SECTOR_CODE SECTOR_CODE,\n";
                SQL += "  PSN.PSN_DEPT_CODE DEPT_CODE,\n";
                SQL += "  PSN.PSN_MINOR_Code MINOR_Code,\n";
                SQL += "  PSN.PSN_SECTOR_NAME SECTOR_NAME,\n";
                SQL += "  PSN.PSN_DEPT_Name DEPT_Name,\n";
                SQL += "  PSN.PSN_FN_ID FN_ID,\n";
                SQL += "  PSN.PSN_FLD_Name FLD_Name,\n";
                SQL += "  PSN.PSN_FN_CODE FN_CODE,\n";
                SQL += "  ISNULL(PSN.PSN_FN_TYPE,'00' ) FN_TYPE, \n";
                SQL += "  PSN.PSN_FN_NAME FN_NAME,\n";
                SQL += "  PSN.PSN_MGR_CODE MGR_CODE,\n";
                SQL += "  PSN.PSN_MGR_NAME MGR_NAME,\n";
                SQL += "  ISNULL(CG.CLSGP_ID,0) CLSGP_ID  \n";
                SQL += "  ,ISNULL(PSN.PSN_MGR_NAME,PSN.PSN_FN_NAME) POS_Name  \n";
                SQL += "  FROM vw_HR_PSN_History PSN   \n";
                SQL += "  LEFT JOIN tb_HR_PNPO_CLASS_GROUP CG ON PSN.PSN_PNPS_CLASS BETWEEN CG.PNPO_CLASS_Start AND CG.PNPO_CLASS_End\n";
                SQL += "                                 AND CG.R_Year=@R_Year AND CG.R_Round=@R_Round\n";

                SQL += "  WHERE PSN.PSN_PSNL_NO=@PSNL_NO  AND PSN.R_Year=@R_Year AND PSN.R_Round=@R_Round  \n";

                DA = new SqlDataAdapter(SQL, ConnectionString());
                DT = new DataTable();
                DA.Fill(DT);
            }

            if (DT.Rows.Count == 0)
            {
                SQL = " DECLARE @R_Year AS Int=" + R_Year + "\n";
                SQL += " DECLARE @R_Round As Int=" + R_Round + "\n";
                SQL += " DECLARE @PSNL_NO As nvarchar(6)='" + PSNL_No.Replace("'", "''") + "'\n";
                SQL += " \n";
                SQL += " SELECT ";
                SQL += "    PSN.PNPS_PSNL_NO PSNL_No,";
                SQL += "    PSN.PNPS_TITLE+PSN.PNPS_PSNL_NAME+' '+PSN.PNPS_PSNL_SURNAME PSNL_Fullname,";
                SQL += "    PSN.PNPS_CLASS PNPS_CLASS,";
                SQL += "    POS.PNPO_TYPE PSNL_TYPE,";
                SQL += "    POS.POS_NO POS_NO,";
                SQL += "    POS.PNPO_TYPE WAGE_TYPE,";
                SQL += "    POS.PNPO_TYPE_NAME WAGE_NAME,";
                //------เพิ่มชื่อ SECTOR_CODE 
                SQL += "    ISNULL(vw_PN.SECTOR_CODE,LEFT(POS.DEPT_CODE,2)) SECTOR_CODE,";
                SQL += "    POS.DEPT_CODE DEPT_CODE,";
                SQL += "    POS.MINOR_Code MINOR_Code,";
                SQL += "    POS.SECTOR_NAME SECTOR_NAME,";
                SQL += "    POS.DEPT_Name DEPT_Name,";
                SQL += "    POS.FN_ID FN_ID,";
                SQL += "    POS.FLD_Name FLD_Name,";
                SQL += "    POS.FLD_CODE FN_CODE,";
                SQL += "    ISNULL(vw_PN.FN_TYPE,'00') FN_TYPE,";
                SQL += "    POS.FN_NAME FN_NAME,";
                SQL += "    POS.MGR_CODE MGR_CODE,";
                SQL += "    POS.MGR_NAME MGR_NAME,";
                SQL += "    ISNULL(CG.CLSGP_ID,0) CLSGP_ID  ";
                SQL += "   ,ISNULL(POS.MGR_NAME,POS.FN_NAME) POS_Name  \n";
                SQL += "    FROM tb_PN_PERSONAL_VIEW PSN";
                SQL += "    LEFT JOIN vw_PN_Position POS ON PSN.PNPS_POSITION_NO=POS.POS_NO";
                SQL += "    LEFT JOIN vw_PN_PSNL vw_PN ON POS.POS_NO=vw_PN.POS_NO";
                SQL += "    LEFT JOIN tb_HR_PNPO_CLASS_GROUP CG ON PSN.PNPS_CLASS BETWEEN CG.PNPO_CLASS_Start AND CG.PNPO_CLASS_End";
                SQL += "                                   AND CG.R_Year=@R_Year AND CG.R_Round=@R_Round";
                SQL += "  WHERE PSN.PNPS_PSNL_NO=@PSNL_NO";

                DA = new SqlDataAdapter(SQL, ConnectionString());
                DT = new DataTable();
                DA.Fill(DT);

            }
            if (DT.Rows.Count == 0)
                return Result;

            //----------------------ใช้ดารดึงแบบเดิม---------------- 
            Result.PSNL_No = DT.Rows[0]["PSNL_No"].ToString();
            Result.PSNL_Fullname = DT.Rows[0]["PSNL_Fullname"].ToString();
            Result.PNPS_CLASS = DT.Rows[0]["PNPS_CLASS"].ToString();
            Result.CLSGP_ID = DT.Rows[0]["CLSGP_ID"].ToString();
            Result.PSNL_TYPE = DT.Rows[0]["PSNL_TYPE"].ToString();
            Result.POS_NO = DT.Rows[0]["POS_NO"].ToString();
            Result.WAGE_TYPE = DT.Rows[0]["WAGE_TYPE"].ToString();
            Result.WAGE_NAME = DT.Rows[0]["WAGE_NAME"].ToString();
            Result.SECTOR_CODE = DT.Rows[0]["SECTOR_CODE"].ToString();
            Result.DEPT_CODE = DT.Rows[0]["DEPT_Code"].ToString();
            Result.MINOR_CODE = DT.Rows[0]["MINOR_Code"].ToString();
            Result.SECTOR_NAME = DT.Rows[0]["SECTOR_NAME"].ToString();
            Result.DEPT_NAME = DT.Rows[0]["DEPT_Name"].ToString();
            Result.FN_ID = DT.Rows[0]["FN_ID"].ToString();
            Result.FLD_Name = DT.Rows[0]["FLD_Name"].ToString();
            Result.FN_CODE = DT.Rows[0]["FN_CODE"].ToString();
            Result.FN_Type = DT.Rows[0]["FN_Type"].ToString();
            Result.FN_NAME = DT.Rows[0]["FN_NAME"].ToString();
            Result.MGR_CODE = DT.Rows[0]["MGR_CODE"].ToString();
            Result.MGR_NAME = DT.Rows[0]["MGR_NAME"].ToString();
            Result.POS_Name = DT.Rows[0]["POS_Name"].ToString();
            if (Status >= AssessmentStatus.CreatedApproved)
            {
                //----------------------ใช้ตำแหน่งจาก tb_HR_Assessment_PSN---------------- 
                //============ป้องกันการ update ตำแหน่งหลังอนุมัติแบบแล้ว=======อนุมัติแบบประเมินแล้ว======
                SQL = "SELECT * ";
                SQL += " FROM tb_HR_Assessment_PSN ";
                SQL += " WHERE PSNL_NO='" + PSNL_No.Replace("'", "''") + "' AND R_Year=" + R_Year + " AND R_Round=" + R_Round;
                DA = new SqlDataAdapter(SQL, ConnectionString());
                DataTable DT_Pos = new DataTable();
                DA.Fill(DT_Pos);
                if (DT_Pos.Rows.Count > 0)
                {
                    Result.PSNL_Fullname = DT_Pos.Rows[0]["PSN_PSNL_Fullname"].ToString();
                    Result.DEPT_NAME = DT_Pos.Rows[0]["PSN_DEPT_NAME"].ToString();
                    Result.MGR_NAME = DT_Pos.Rows[0]["PSN_MGR_NAME"].ToString();
                    Result.FN_NAME = DT_Pos.Rows[0]["PSN_FN_NAME"].ToString();
                    Result.PNPS_CLASS = DT_Pos.Rows[0]["PSN_PNPS_CLASS"].ToString();
                    Result.WAGE_NAME = DT_Pos.Rows[0]["PSN_WAGE_NAME"].ToString();
                    Result.SECTOR_NAME = DT_Pos.Rows[0]["PSN_SECTOR_NAME"].ToString();

                    Result.CLSGP_ID = DT.Rows[0]["CLSGP_ID"].ToString();
                    Result.PSNL_TYPE = DT.Rows[0]["PSNL_TYPE"].ToString();
                    Result.FN_ID = DT.Rows[0]["FN_ID"].ToString();
                    Result.FN_Type = DT.Rows[0]["FN_Type"].ToString();
                }

            }

            return Result;

        }

        public DataTable PersonalCareerPath(string PSNL_No)
        {
            string SQL = "";


            SQL += "SELECT * FROM vw_Path_RPT_Yearly_Result \n";
            SQL += " WHERE PSNL_NO IS NOT NULL \n";
            SQL += " AND PSNL_NO='" + PSNL_No + "'";
            SQL += "ORDER BY SECTOR_NAME ASC ,SECTOR_CODE ASC,DEPT_CODE ASC ,PNPS_CLASS DESC  ,Year_Score  DESC \n";
            SqlDataAdapter DA = new SqlDataAdapter(SQL, ConnectionString());
            DataTable DT = new DataTable();
            DA.Fill(DT);

            if (DT.Rows.Count == 0)
            {
                SQL = "";
                SQL += " SELECT  vw_PN_PSNL.PSNL_NO \n";
                SQL += " ,vw_PN_PSNL.PSNL_Fullname \n";
                SQL += " ,vw_PN_PSNL.PNPS_CLASS \n";
                SQL += " ,vw_PN_PSNL.PSNL_TYPE \n";
                SQL += " ,vw_PN_PSNL.POS_NO \n";
                SQL += " ,ISNULL(vw_PN_PSNL.MGR_NAME,vw_PN_PSNL.FN_NAME) POS_Name \n";
                SQL += " ,vw_PN_PSNL.DEPT_CODE \n";
                SQL += " ,vw_PN_PSNL.SECTOR_CODE \n";
                SQL += " ,vw_PN_PSNL.SECTOR_NAME \n";
                SQL += " ,vw_PN_PSNL.DEPT_NAME \n";
                SQL += " ,vw_PN_PSNL.FN_ID \n";
                SQL += " ,vw_PN_PSNL.FLD_Name \n";
                SQL += " ,vw_PN_PSNL.FN_CODE \n";
                SQL += " ,vw_PN_PSNL.FN_TYPE \n";
                SQL += " ,vw_PN_PSNL.FN_NAME \n";
                SQL += " ,vw_PN_PSNL.MGR_CODE \n";
                SQL += " ,vw_PN_PSNL.MGR_NAME \n";
                SQL += " ,vw_PN_PSNL.PSNL_STAT \n";
                SQL += " ,vw_PN_PSNL.STAT_NAME  \n";
                SQL += " ,CASE  \n";
                SQL += " WHEN ISNULL (MAX(History_PUNISH.Count_PUNISH),0) >0 THEN 'เคยรับโทษ' \n";
                SQL += " ELSE 'ไม่เคยรับโทษ' END History_PUNISH \n";
                SQL += " ,CASE  \n";
                SQL += " WHEN ISNULL (MAX(History_PUNISH.Count_PUNISH),0) >0 THEN 0 \n";
                SQL += " ELSE 1 END History_Status_PUNISH \n";
                SQL += " FROM vw_PN_PSNL_ALL vw_PN_PSNL \n";
                SQL += " LEFT JOIN vw_Path_Count_History_PUNISH History_PUNISH ON vw_PN_PSNL.PSNL_NO=History_PUNISH.PSNL_NO \n";
                SQL += " WHERE vw_PN_PSNL.PSNL_NO IS NOT NULL  \n";
                SQL += " AND vw_PN_PSNL.PSNL_NO='" + PSNL_No + "' \n";
                SQL += " GROUP BY vw_PN_PSNL.PSNL_NO \n";
                SQL += " ,vw_PN_PSNL.PSNL_Fullname \n";
                SQL += " ,vw_PN_PSNL.PNPS_CLASS \n";
                SQL += " ,vw_PN_PSNL.PSNL_TYPE \n";
                SQL += " ,vw_PN_PSNL.POS_NO \n";
                SQL += " ,vw_PN_PSNL.DEPT_CODE \n";
                SQL += " ,vw_PN_PSNL.SECTOR_CODE \n";
                SQL += " ,vw_PN_PSNL.SECTOR_NAME \n";
                SQL += " ,vw_PN_PSNL.DEPT_NAME \n";
                SQL += " ,vw_PN_PSNL.FN_ID \n";
                SQL += " ,vw_PN_PSNL.FLD_Name \n";
                SQL += " ,vw_PN_PSNL.FN_CODE \n";
                SQL += " ,vw_PN_PSNL.FN_TYPE \n";
                SQL += " ,vw_PN_PSNL.FN_NAME \n";
                SQL += " ,vw_PN_PSNL.MGR_CODE \n";
                SQL += " ,vw_PN_PSNL.MGR_NAME \n";
                SQL += " ,vw_PN_PSNL.PSNL_STAT \n";
                SQL += " ,vw_PN_PSNL.STAT_NAME \n";
                SQL += " ,vw_PN_PSNL.PSNL_NO \n";
                SQL += " ,History_PUNISH.Count_PUNISH \n";
                SQL += " ORDER BY SECTOR_NAME ASC ,SECTOR_CODE ASC,DEPT_CODE ASC ,PNPS_CLASS DESC  \n";
                DA = new SqlDataAdapter(SQL, ConnectionString());
                DT = new DataTable();
                DA.Fill(DT);
            }

            return DT;
        }

        public DataTable GetAssessorList(int R_Year, int R_Round, string PSNL_No)
        {

            string SQL = "";
            SQL += " DECLARE @R_Year AS INT=" + R_Year + "\n";
            SQL += " DECLARE @R_Round AS INT=" + R_Round + "\n";
            SQL += " DECLARE @PSNL_NO AS INT='" + PSNL_No.Replace("'", "''") + "'\n\n";

            SQL += " SELECT *,MGR_PSNL_NO MGR_BY,MGR_PSNL_Fullname MGR_Name,ISNULL(ISNULL(MGR_MGR_NAME,MGR_FN_NAME),'') MGR_Pos,MGR_DEPT_NAME MGR_DEPT ";
            SQL += " FROM vw_HR_ASSESSOR_PSN ";
            SQL += " WHERE R_Year=@R_Year AND R_Round=@R_Round AND PSN_PSNL_NO=@PSNL_NO";
            SQL += " ";
            //-------------- นับจาก Saved(1) > Default(0) > Assigned(2) ------------
            SQL += " ORDER BY CASE MGR_Type WHEN 1 THEN 0 WHEN 0 THEN 1 ELSE MGR_Type END \n";

            SqlDataAdapter DA = new SqlDataAdapter(SQL, ConnectionString());
            DataTable DT = new DataTable();
            DA.Fill(DT);

            return DT;
        }

        public int GetKPIStatus(int R_Year, int R_Round, string PSNL_NO)
        {
            int ret;
            string SQL = " SELECT KPI_Status\n";
            SQL += " FROM tb_HR_KPI_Header ";
            SQL += " WHERE R_Year = " + R_Year + " And R_Round = " + R_Round + " AND PSNL_NO='" + PSNL_NO.Replace("'", "''") + "'\n";
            SqlDataAdapter DA = new SqlDataAdapter(SQL, ConnectionString());
            DataTable DT = new DataTable();
            DA.Fill(DT);
            if (DT.Rows.Count == 0)
            {
                ret = AssessmentStatus.NotFound;
            }
            else if (GL.IsEqualNull(DT.Rows[0]["KPI_Status"]))
            {
                ret = AssessmentStatus.Creating;
            }
            else
            {
                ret = GL.CINT(DT.Rows[0]["KPI_Status"]);
            }
            return ret;
        }

        /// <summary>
        /// ตรวจสอบว่าเวลาที่ระบุตามประเภทการประเมิน และเทียบกับสถานะการประเมินเกินกว่าเวลาที่กำหนดหรือเปล่า
        /// </summary>
        /// <param name="CheckedTime">เวลาที่ต้องการตรวจสอบ</param>
        /// <param name="R_Year">ปี</param>
        /// <param name="R_Round">รอบ</param>
        /// <param name="Assessment_Type">ประเภทการประเมิน</param>
        /// <param name="Assessment_Status">สถานะของใบประเมิน</param>
        public Boolean IsTimeInAssessmentPeriod(DateTime CheckedTime, int R_Year, int R_Round, int Assessment_Type, int Assessment_Status, string DEPT_CODE)
        {

            String SQL = "SELECT ";

            String _ass_type = "";

            switch (Assessment_Type)
            {
                case HRBL.AssessmentType.KPI:
                    _ass_type = "K_";
                    break;
                case HRBL.AssessmentType.Competency:
                    _ass_type = "C_";
                    break;
            }

            String Start_Field = "";
            String End_Field = "";

            switch (Assessment_Status)
            {

                case AssessmentStatus.NotFound:
                case AssessmentStatus.Creating:
                case AssessmentStatus.WaitCreatingApproved: //------------- พนักงานกำหนดแบบ - หัวหน้าอนุมัติ แบบ -------------
                    Start_Field = _ass_type + "CP_Start";
                    End_Field = _ass_type + "CP_End";
                    break;
                case AssessmentStatus.CreatedApproved:
                case AssessmentStatus.WaitAssessment: //---------- พนักงานประเมินตนเอง -----------
                    Start_Field = _ass_type + "AP_Start";
                    End_Field = _ass_type + "AP_End";
                    break;
                case AssessmentStatus.WaitConfirmAssessment: //------------- รอหัวหน้าอนุมติผล ---------------
                    Start_Field = _ass_type + "MGR_Start";
                    End_Field = _ass_type + "MGR_End";
                    break;
                case AssessmentStatus.AssessmentCompleted: //------------- อนุมัติผลแล้ว --------------------
                    Start_Field = "null";
                    End_Field = "null";
                    break;


            }

            SQL += Start_Field + " T_Start,DATEADD(DAY,1," + End_Field + ") T_End \n";
            SQL += " FROM vw_HR_Custom_Period\n";
            SQL += " WHERE R_Year=" + GL.CINT(R_Year) + " AND R_Round=" + GL.CINT(R_Round) + " AND " + Start_Field + " IS NOT NULL AND " + End_Field + " IS NOT NULL";
            SQL += " AND DEPT_CODE='" + DEPT_CODE.ToString() + "'";
            SqlDataAdapter DA = new SqlDataAdapter(SQL, ConnectionString());
            DataTable DT = new DataTable();
            DA.Fill(DT);

            if (DT.Rows.Count == 0)
            {

                return false;
            }
            else if ((DateTime)CheckedTime < (DateTime)DT.Rows[0]["T_Start"] || (DateTime)CheckedTime > (DateTime)DT.Rows[0]["T_End"])
            {
                return false;

            }
            else
            {
                return true;
            }

        }

        /// <summary>
        /// ตรวจสอบว่าเวลาที่ระบุปัจจุบัน ยังอยู่ในช่วงรอบที่ประเมินหรือไม่
        /// </summary>
        /// <param name="CheckedTime">เวลาที่ต้องการตรวจสอบ</param>
        /// <param name="R_Year">ปี</param>
        /// <param name="R_Round">รอบ</param>
        public Boolean IsTimeInPeriod(DateTime CheckedTime, int R_Year, int R_Round)
        {
            string SQL = "SELECT R_Year,R_Round,R_Start,R_End,ISNULL(R_Completed,0) R_Completed  FROM tb_HR_Round  WHERE R_Year=" + GL.CINT(R_Year) + " AND R_Round=" + GL.CINT(R_Round) + "";
            SqlDataAdapter DA = new SqlDataAdapter(SQL, ConnectionString());
            DataTable DT = new DataTable();
            DA.Fill(DT);

            // false ไม่ได้อยู่ในรอบ
            // true อยู่ในรอบที่กำลังประเมิน

            if (DT.Rows.Count == 0)
            {
                return false;
            }
            else if (((DateTime)CheckedTime < (DateTime)DT.Rows[0]["R_Start"] || (DateTime)CheckedTime > (DateTime)DT.Rows[0]["R_End"]))
            {
                return false;
            }
            else
            {
                return true;
            }

        }


        public void Update_KPI_Status_To_Assessment_Period(string PSNL_NO, int R_Year, int R_Round)
        {
            string SQL = "EXEC dbo.sp_Update_KPI_Status_To_Assessment_Period ";
            if (!string.IsNullOrEmpty(PSNL_NO))
            {
                SQL += " '" + PSNL_NO.Replace("'", "''") + "',";
            }
            else
            {
                SQL += "NULL,";
            }
            if (R_Year != 0)
            {
                SQL += R_Year + ",";
            }
            else
            {
                SQL += "NULL,";
            }
            if (R_Round != 0)
            {
                SQL += R_Round;
            }
            else
            {
                SQL += "NULL";
            }

            SqlConnection Conn = new SqlConnection(ConnectionString());
            Conn.Open();
            SqlCommand Comm = new SqlCommand();
            Comm.Connection = Conn;
            Comm.CommandType = CommandType.Text;
            Comm.CommandText = SQL;
            Comm.CommandTimeout = 90;
            Comm.ExecuteNonQuery();
            Comm.Dispose();
            Conn.Close();
            Conn.Dispose();

        }

        public void Update_COMP_Weight_Float_To_COMP_Weight_Int(string PSNL_NO, int R_Year, int R_Round, Boolean Auto_Weight)
        {
            string SQL = "EXEC dbo.sp_Update_COMP_Weight_ToInt ";
            if (!string.IsNullOrEmpty(PSNL_NO))
            {
                SQL += " '" + PSNL_NO.Replace("'", "''") + "',";
            }
            else
            {
                SQL += "NULL,";
            }
            if (R_Year != 0)
            {
                SQL += R_Year + ",";
            }
            else
            {
                SQL += "NULL,";
            }
            if (R_Round != 0)
            {
                SQL += R_Round + ",";
            }
            else
            {
                SQL += "NULL";
            }

            if (Auto_Weight != false)
            {
                SQL += "1";
            }
            else
            {
                SQL += "0";
            }

            SqlConnection Conn = new SqlConnection(ConnectionString());
            Conn.Open();
            SqlCommand Comm = new SqlCommand();
            Comm.Connection = Conn;
            Comm.CommandType = CommandType.Text;
            Comm.CommandText = SQL;
            Comm.CommandTimeout = 90;
            Comm.ExecuteNonQuery();
            Comm.Dispose();
            Conn.Close();
            Conn.Dispose();

        }


        public void Update_KPI_Status_To_Assessment_Period(string PSNL_NO, int R_Year)
        {
            Update_KPI_Status_To_Assessment_Period(PSNL_NO, R_Year, 0);
        }

        public void Update_KPI_Status_To_Assessment_Period(string PSNL_NO)
        {
            Update_KPI_Status_To_Assessment_Period(PSNL_NO, 0);
        }

        public void Update_KPI_Status_To_Assessment_Period()
        {
            Update_KPI_Status_To_Assessment_Period("");
        }

        // IDP ช่วงวันที่พัฒนา  fix 6 เดือน ถัดจากวันปิดรอบ
        public string GetDateR_End(int R_Year, int R_Round)
        {
            //string SQL = "SELECT 	DAY(R_END) DAY_START,MONTH(R_END) MONTH_START,Year(R_END) Year_START,DAY(DATEADD(DAY,180,R_END)) DAY_END,MONTH(DATEADD(DAY,180,R_END)) MONTH_END,Year(DATEADD(DAY,180,R_END)) Year_END  FROM tb_HR_Round  WHERE R_Year=" + GL.CINT(R_Year) + " AND R_Round=" + GL.CINT(R_Round) + "";

            string SQL = "SELECT ";
            SQL += " MONTH(DATEADD(month, 1, R_End)) MONTH_START"  + "";
            SQL += " ,Year(DATEADD(month, 1, R_End)) Year_START " + "";

            SQL += " ,MONTH(DATEADD(month, 6, R_End)) MONTH_END " + "";
            SQL += " ,Year(DATEADD(month, 6, R_End)) Year_END "  + "";
            SQL += " FROM tb_HR_Round  WHERE R_Year=" + GL.CINT(R_Year) + " AND R_Round=" + GL.CINT(R_Round) + "";
          
            
            SqlDataAdapter DA = new SqlDataAdapter(SQL, ConnectionString());
            DataTable DT = new DataTable();
            DA.Fill(DT);
            if (DT.Rows.Count == 0)
            {
                return "";
            }
            else
            {
                //return DT.Rows[0]["DAY_END"] + " " + C.ToMonthNameTH(GL.CINT(DT.Rows[0]["MONTH_END"])) + " " + (GL.CINT(DT.Rows[0]["Year_END"]) + 543);
                return C.ToMonthNameTH(GL.CINT(DT.Rows[0]["MONTH_START"])) + " " + (GL.CINT(DT.Rows[0]["Year_START"]) + 543) + " - " + C.ToMonthNameTH(GL.CINT(DT.Rows[0]["MONTH_END"])) + " " + (GL.CINT(DT.Rows[0]["Year_END"]) + 543);
            }

        }

        public DataTable GetRoundInfo(int R_Year, int R_Round)
        {
            string SQL = "SELECT * FROM vw_HR_Round WHERE R_Year=" + R_Year + " AND R_Round=" + R_Round;
            SqlDataAdapter DA = new SqlDataAdapter(SQL, ConnectionString());
            DataTable DT = new DataTable();
            DA.Fill(DT);
            return DT;
        }

        public object GetPSNRetireDate(string PSNL_NO)
        {
            string SQL = "SELECT PNPS_RETIRE_DATE FROM tb_PN_PERSONAL_VIEW WHERE PNPS_PSNL_NO='" + PSNL_NO.Replace("'", "''") + "'";
            SqlDataAdapter DA = new SqlDataAdapter(SQL, ConnectionString());
            DataTable DT = new DataTable();
            DA.Fill(DT);
            if (DT.Rows.Count == 0)
            {
                return DBNull.Value;
            }
            else
            {
                return DT.Rows[0][0];
            }

        }

        public DataManager GetKPIDetail(int R_Year, int R_Round, string PNPS_PSNL_NO, int KPI_No)
        {
            string SQL = "SELECT PSNL_NO,R_Year,R_Round\n";
            SQL += " ,KPI_No,KPI_Job,KPI_Target_Text\n";
            SQL += " ,KPI_Choice_1,KPI_Choice_2,KPI_Choice_3,KPI_Choice_4,KPI_Choice_5\n";
            SQL += " ,KPI_Target_Choice,KPI_Answer_PSN,KPI_Answer_MGR,KPI_Remark_PSN,KPI_Remark_MGR,KPI_Weight\n";
            SQL += " FROM tb_HR_KPI_Detail\n";
            SQL += " WHERE R_Year=" + R_Year + " AND R_Round=" + R_Round + " AND PSNL_NO='" + PNPS_PSNL_NO.Replace("'", "''") + "'";
            if (KPI_No != 0)
            {
                SQL += " AND KPI_No=" + KPI_No + "\n";
            }
            SQL += " ORDER BY KPI_No\n";
            SqlDataAdapter DA = new SqlDataAdapter(SQL, ConnectionString());
            DataTable DT = new DataTable();
            DA.Fill(DT);

            DataManager Result = new DataManager();
            Result.SQL = SQL;
            Result.Table = DT;
            Result.Adaptor = DA;
            return Result;
        }

        public DataManager GetKPIDetail(int R_Year, int R_Round, string PNPS_PSNL_NO)
        {
            return GetKPIDetail(R_Year, R_Round, PNPS_PSNL_NO, 0);
        }

        public DataManager OverwriteKPIDetail(int R_Year, int R_Round, string PNPS_PSNL_NO, int KPI_No)
        {
            string SQL = "DELETE \n";
            SQL += " FROM tb_HR_KPI_Detail\n";
            SQL += " WHERE R_Year=" + R_Year + " AND R_Round=" + R_Round + " AND PSNL_NO='" + PNPS_PSNL_NO.Replace("'", "''") + "'";
            if (KPI_No != 0)
            {
                SQL += " AND KPI_No=" + KPI_No + "\n";
            }
            SqlDataAdapter DA = new SqlDataAdapter(SQL, ConnectionString());
            DataTable DT = new DataTable();
            DA.Fill(DT);

            DataManager Result = new DataManager();
            Result.SQL = SQL;
            Result.Table = DT;
            Result.Adaptor = DA;
            return Result;
        }

        public DataManager OverwriteKPIDetail(int R_Year, int R_Round, string PNPS_PSNL_NO)
        {
            return OverwriteKPIDetail(R_Year, R_Round, PNPS_PSNL_NO, 0);
        }

        public DataTable GetKPISummary(int R_Year, int R_Round, string PSNL_NO)
        {
            string SQL = "";
            SQL += " SELECT R.R_Year,R.R_Round,PSN.PNPS_PSNL_NO PSNL_NO,\n";
            SQL += " COUNT(KD.KPI_No) AssTotal,SUM(KD.KPI_Answer_MGR*KD.KPI_Weight)/5 AssPercent,'KPI' AssType,\n";
            SQL += " dbo.udf_GetAssessmentStatusName(KH.KPI_Status) AssStatus\n";
            SQL += " FROM tb_HR_Round R\n";
            SQL += " CROSS JOIN tb_PN_PERSONAL_VIEW PSN\n";
            SQL += " LEFT JOIN tb_HR_KPI_Header KH ON R.R_Year=KH.R_Year AND R.R_Round=KH.R_Round AND KH.PNPS_PSNL_NO=PSN.PNPS_PSNL_NO\n";
            SQL += " LEFT JOIN tb_HR_KPI_Detail KD ON KH.R_Year=KD.R_Year AND KH.R_Round=KD.R_Round AND KH.PNPS_PSNL_NO=KD.PNPS_PSNL_NO\n";

            string Filter = "";
            if (R_Year != 0)
            {
                Filter += " R.R_Year=" + R_Year + " AND ";
            }
            if (R_Round != 0)
            {
                Filter += " R.R_Round=" + R_Round + " AND ";
            }
            if (!string.IsNullOrEmpty(PSNL_NO))
            {
                Filter += " PSN.PNPS_PSNL_NO='" + PSNL_NO.Replace("'", "''") + "' AND ";
            }
            if (!string.IsNullOrEmpty(Filter))
            {
                SQL += " WHERE " + Filter.Substring(0, Filter.Length - 4) + "\n";
            }

            SQL += " GROUP BY R.R_Year,R.R_Round,PSN.PNPS_PSNL_NO,KH.KPI_Status";
            SQL += " ORDER BY R.R_Year,R.R_Round,PSN.PNPS_PSNL_NO";

            SqlDataAdapter DA = new SqlDataAdapter(SQL, ConnectionString());
            DataTable DT = new DataTable();
            DA.Fill(DT);

            return DT;
        }

        public DataTable GetKPISummary(int R_Year, int R_Round)
        {
            return GetKPISummary(R_Year, R_Round, "");
        }
        //เพิ่ม ,0
        public DataTable GetKPISummary(int R_Year)
        {
            return GetKPISummary(R_Year, 0);
        }

        public string GetAssessmentStatusName(int KPI_Status)
        {
            switch (KPI_Status)
            {
                case HRBL.AssessmentStatus.NotFound:
                    return "ไม่พบ";
                case HRBL.AssessmentStatus.Creating:
                    return "ยังไม่ส่งแบบประเมิน";
                case HRBL.AssessmentStatus.WaitCreatingApproved:
                    return "รออนุมัติแบบประเมิน";
                case HRBL.AssessmentStatus.CreatedApproved:
                    return "อนุมัติแบบประเมินแล้ว";
                case HRBL.AssessmentStatus.WaitAssessment:
                    return "พนักงานประเมินตนเอง";
                case HRBL.AssessmentStatus.WaitConfirmAssessment:
                    return "รออนุมัติผลการประเมิน";
                case HRBL.AssessmentStatus.AssessmentCompleted:
                    return "ประเมินเสร็จสมบูรณ์";
                default:
                    return "ไม่พบ";
            }
        }

        public void SetJobMappingAutoClass(string DEPT_CODE, string MINOR_CODE)
        {
            DataTable RoundData = GetAllAssessmentRound();
            RoundData.DefaultView.Sort = "R_Year DESC,R_Round DESC";
            RoundData = RoundData.DefaultView.ToTable();

            if (RoundData.Rows.Count == 0)
                return;
            string SQL = "SELECT * FROM tb_HR_PNPO_CLASS_GROUP WHERE R_Year=" + RoundData.Rows[0]["R_Year"] + " AND R_Round=" + RoundData.Rows[0]["R_Round"] + " ORDER BY CLSGP_ID";
            SqlDataAdapter DA = new SqlDataAdapter(SQL, ConnectionString());
            DataTable ClassData = new DataTable();
            DA.Fill(ClassData);

            if (ClassData.Rows.Count == 0)
                return;

            DataTable DT = new DataTable();
            SQL = "DELETE FROM tb_HR_Job_Mapping_Class WHERE DEPT_CODE='" + DEPT_CODE.Replace("'", "''") + "' AND MINOR_CODE='" + MINOR_CODE.Replace("'", "''") + "'";
            DA = new SqlDataAdapter(SQL, ConnectionString());
            DA.Fill(DT);

            SQL = "SELECT * FROM tb_HR_Job_Mapping_Class WHERE DEPT_CODE='" + DEPT_CODE.Replace("'", "''") + "' AND MINOR_CODE='" + MINOR_CODE.Replace("'", "''") + "'";
            DA = new SqlDataAdapter(SQL, ConnectionString());
            DT = new DataTable();
            DA.Fill(DT);

            for (int i = 0; i <= ClassData.Rows.Count - 1; i++)
            {
                DataRow DR = DT.NewRow();
                DR["DEPT_CODE"] = DEPT_CODE;
                DR["MINOR_CODE"] = MINOR_CODE;
                DR["Class_ID"] = i + 1;
                DR["PNPO_CLASS_Start"] = ClassData.Rows[i]["PNPO_CLASS_Start"];
                DR["PNPO_CLASS_End"] = ClassData.Rows[i]["PNPO_CLASS_End"];
                DR["Update_By"] = 0;
                DR["Update_Time"] = DateTime.Now;
                DT.Rows.Add(DR);
            }

            SqlCommandBuilder cmd = new SqlCommandBuilder(DA);
            DA.Update(DT);

        }


        public void SetWorkloadDefaultPSN(string DEPT_CODE, string MINOR_CODE, int Class_ID)
        {
            string SQL = " SELECT PSNL_NO,PSNL_Fullname + '(' + CAST(CAST(PNPS_CLASS AS INT) AS VARCHAR) + ')' PSNL_Fullname,Class_ID\n";
            SQL += " FROM vw_PN_PSNL PSN\n";
            SQL += " INNER JOIN tb_HR_Job_Mapping_Class CLS ON PSN.DEPT_CODE=CLS.DEPT_CODE \n";
            SQL += " \t\t\t\t\t\t\t\t\t\tAND PSN.MINOR_CODE=CLS.MINOR_CODE\n";
            SQL += " \t\t\t\t\t\t\t\t\t\tAND CAST(PNPS_CLASS AS INT) BETWEEN CLS.PNPO_CLASS_Start AND CLS.PNPO_CLASS_End\n";
            SQL += " WHERE PNPS_RETIRE_DATE>GETDATE() AND PSNL_STAT='02'\n";
            SQL += " AND PSN.DEPT_CODE='" + DEPT_CODE.Replace("'", "''") + "' AND PSN.MINOR_CODE='" + MINOR_CODE.Replace("'", "''") + "'\n";
            if (Class_ID != 0)
            {
                SQL += " AND CLS.Class_ID=" + Class_ID + "\n";
            }
            SQL += " ORDER BY CAST(PNPS_CLASS AS INT) DESC,PSN.POS_NO ASC\n";

            SqlDataAdapter DA = new SqlDataAdapter(SQL, ConnectionString());
            DataTable PSN = new DataTable();
            DA.Fill(PSN);

            //----------- Get Exists Person In Slot-----------
            SQL = " SELECT * FROM tb_HR_Workload_PSN \n";
            SQL += " WHERE DEPT_CODE='" + DEPT_CODE.Replace("'", "''") + "' AND MINOR_CODE='" + MINOR_CODE.Replace("'", "''") + "'\n";
            if (Class_ID != 0)
            {
                SQL += " AND Class_ID=" + Class_ID + "\n";
            }
            DA = new SqlDataAdapter(SQL, ConnectionString());
            DataTable Target = new DataTable();
            DA.Fill(Target);

            //--------- Compare ------------
            for (int i = Target.Rows.Count - 1; i >= 0; i += -1)
            {
                //------------ เอาคนที่ Manual เพิ่มเข้าไปใหม่ออก -----------
                if (GL.IsEqualNull(Target.Rows[i]["REF_PSNL_NO"]) || string.IsNullOrEmpty(Target.Rows[i]["REF_PSNL_NO"].ToString()))
                {
                    RemoveWorkLoadSlot_ByPerson(GL.CINT(Target.Rows[i]["PSN_ID"]));
                    Target.Rows[i].Delete();
                }
                else
                {
                    //---------- เอาคนที่ไม่มีออกไป ---------
                    PSN.DefaultView.RowFilter = "PSNL_NO='" + Target.Rows[i]["REF_PSNL_NO"].ToString().Replace("'", "''") + "'";
                    if (PSN.DefaultView.Count == 0)
                    {
                        RemoveWorkLoadSlot_ByPerson(GL.CINT(Target.Rows[i]["PSN_ID"]));
                        Target.Rows[i].Delete();
                    }
                }
            }

            SqlCommandBuilder cmd = new SqlCommandBuilder(DA);
            DA.Update(Target);
            Target.AcceptChanges();

            for (int i = 0; i <= PSN.Rows.Count - 1; i++)
            {
                Target.DefaultView.RowFilter = "REF_PSNL_NO='" + PSN.Rows[i]["PSNL_NO"].ToString().Replace("'", "''") + "'";
                if (Target.DefaultView.Count > 0)
                {
                    //------------ เอาคนที่อยู่ผิด Class ปรับให้อยู่ถูก Class ------
                    Target.DefaultView[0].Row["Class_ID"] = PSN.Rows[i]["Class_ID"];
                    Target.DefaultView[0].Row["Slot_Name"] = PSN.Rows[i]["PSNL_Fullname"];
                }
                else
                {
                    //----------- ยังไม่มีในของเดิมให้เพิ่มเข้าไปใหม่ ---------------
                    DataRow DR = Target.NewRow();
                    DR["PSN_ID"] = GetNewWorkLoadPSNID();
                    DR["DEPT_CODE"] = DEPT_CODE;
                    DR["MINOR_CODE"] = MINOR_CODE;
                    DR["Class_ID"] = PSN.Rows[i]["Class_ID"];
                    DR["Slot_No"] = DBNull.Value;
                    DR["Slot_Name"] = PSN.Rows[i]["PSNL_Fullname"];
                    DR["REF_PSNL_NO"] = PSN.Rows[i]["PSNL_NO"];
                    DR["Update_By"] = 0;
                    DR["Update_Time"] = DateTime.Now;
                    Target.Rows.Add(DR);
                }
                try
                {
                    cmd = new SqlCommandBuilder(DA);
                    DA.Update(Target);
                    Target.AcceptChanges();
                }
                catch
                {
                }
            }
            Target.DefaultView.RowFilter = "";
            Target.DefaultView.Sort = "Class_ID DESC";

            string[] Col = { "Class_ID" };
            //-------------- Reorder Slot No ----------
            DataTable CLS = Target.DefaultView.ToTable(true, Col).Copy();
            for (int i = 0; i <= CLS.Rows.Count - 1; i++)
            {
                Target.DefaultView.RowFilter = "Class_ID=" + CLS.Rows[i]["Class_ID"];
                for (int j = 0; j <= Target.DefaultView.Count - 1; j++)
                {
                    Target.DefaultView[j]["Slot_No"] = j + 1;
                }
            }
            try
            {
                cmd = new SqlCommandBuilder(DA);
                DA.Update(Target);
                Target.AcceptChanges();
            }
            catch
            {
            }
        }

        public void SetWorkloadDefaultPSN(string DEPT_CODE, string MINOR_CODE)
        {
            SetWorkloadDefaultPSN(DEPT_CODE, MINOR_CODE, 0);
        }

        public void RemoveWorkLoadSlot_BySlot(int Slot_ID)
        {
            SqlConnection Conn = new SqlConnection(ConnectionString());
            Conn.Open();
            SqlCommand Comm = new SqlCommand();
            Comm.Connection = Conn;
            Comm.CommandType = CommandType.Text;
            Comm.CommandText = "DELETE FROM tb_HR_Workload_Slot WHERE Slot_ID=" + Slot_ID;
            Comm.ExecuteNonQuery();
            Comm.Dispose();
            Conn.Close();
            Conn.Dispose();
        }

        public void RemoveWorkLoadSlot_ByPerson(int PSN_ID)
        {
            SqlConnection Conn = new SqlConnection(ConnectionString());
            Conn.Open();
            SqlCommand Comm = new SqlCommand();
            Comm.Connection = Conn;
            Comm.CommandType = CommandType.Text;
            Comm.CommandText = "DELETE FROM tb_HR_Workload_Slot WHERE PSN_ID=" + PSN_ID;
            Comm.ExecuteNonQuery();
            Comm.Dispose();
            Conn.Close();
            Conn.Dispose();
        }


        public DataTable GetAllAssessmentRound()
        {
            string SQL = "SELECT R_Year,R_Round From tb_HR_Round ORDER BY R_Year,R_Round";
            SqlDataAdapter DA = new SqlDataAdapter(SQL, ConnectionString());
            DataTable DT = new DataTable();
            DA.Fill(DT);

            return DT;
        }

        public DataTable GetTypeAssessmentRound(int R_Year, int R_Round, string PSNL_NO)
        {

            PersonalInfo PSNInfo = GetAssessmentPersonalInfo(R_Year, R_Round, PSNL_NO, 0);

            string SQL = " SELECT R.R_Year, R.R_Round  ";
            SQL += " FROM tb_HR_Round R   ";
            SQL += " LEFT JOIN tb_HR_Round_Type T  ON R.R_Year =T.R_Year AND  R.R_Round =T.R_Round  ";
            SQL += " LEFT JOIN tb_HR_Round_Class Class ON R.R_Year =Class.R_Year AND  R.R_Round =Class.R_Round  ";
            SQL += " WHERE T.PNPS_PSNL_TYPE =" + PSNInfo.WAGE_TYPE + "   AND Class.PNPS_CLASS =" + PSNInfo.PNPS_CLASS;


            SqlDataAdapter DA = new SqlDataAdapter(SQL, ConnectionString());
            DataTable DT = new DataTable();
            DA.Fill(DT);

            return DT;
        }

        public bool Is_Round_Completed(int R_Year, int R_Round)
        {
            string SQL = "SELECT ISNULL(R_Completed,0) R_Completed FROM tb_HR_Round WHERE R_Year=" + R_Year + " AND R_Round=" + R_Round;
            SqlDataAdapter DA = new SqlDataAdapter(SQL, ConnectionString());
            DataTable DT = new DataTable();
            DA.Fill(DT);
            if (DT.Rows.Count > 0 && !GL.IsEqualNull(DT.Rows[0]["R_Completed"]) && GL.CBOOL(DT.Rows[0]["R_Completed"]))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool Is_Round_Exists(int R_Year, int R_Round)
        {
            string SQL = "SELECT ISNULL(R_Completed,0) R_Completed FROM tb_HR_Round WHERE R_Year=" + R_Year + " AND R_Round=" + R_Round;
            SqlDataAdapter DA = new SqlDataAdapter(SQL, ConnectionString());
            DataTable DT = new DataTable();
            DA.Fill(DT);
            if (DT.Rows.Count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }



        public DataTable GetTeamDepartment(int R_Year, int R_Round, string Manager_Code)
        {
            string SQL = "";
            SQL = "SELECT DISTINCT PSN_DEPT_CODE DEPT_CODE,PSN_MINOR_CODE MINOR_CODE,PSN_DEPT_NAME DEPT_NAME\n";
            SQL += " FROM vw_HR_ASSESSOR_PSN\n";
            SQL += " WHERE R_Year=" + R_Year + " AND R_Round=" + R_Round + " AND MGR_PSNL_NO='" + Manager_Code.Replace("'", "''") + "'\n";
            SQL += " ORDER BY PSN_DEPT_CODE\n";
            SqlDataAdapter DA = new SqlDataAdapter(SQL, ConnectionString());
            DataTable DT = new DataTable();
            DA.Fill(DT);
            return DT;
        }

        public DataTable GetAssessorRole(int R_Year, int R_Round, string PSNL_No)
        {
            string SQL = "";
            SQL = "SELECT ASSESSOR_POS,\n";
            SQL += " CASE ASSESSOR_POS WHEN '00001' THEN ISNULL(POS.MGR_NAME,POS.FN_Name) ELSE\n";
            SQL += " ISNULL(POS.MGR_NAME,POS.FN_Name) + ' ' + POS.DEPT_NAME END\n";
            SQL += " MGR_POS_NAME \n";
            SQL += " FROM vw_HR_ASSESSOR_New ASS\n";
            SQL += " INNER JOIN vw_PN_Position POS ON ASS.ASSESSOR_POS=POS.POS_NO\n";
            SQL += " WHERE R_Year =" + R_Year + " AND R_Round=" + R_Round + " AND MGR_PSNL_NO='" + PSNL_No.Replace("'", "''") + "'\n";
            SQL += " ORDER BY ASSESSOR_POS";
            SqlDataAdapter DA = new SqlDataAdapter(SQL, ConnectionString());
            DataTable DT = new DataTable();
            DA.Fill(DT);
            return DT;
        }

        public DataTable PSNClassLevel()
        {
            DataTable DT = new DataTable();
            DT.Columns.Add("Level", typeof(int));
            DT.Columns.Add("Selected", typeof(bool));
            for (int i = 1; i <= 13; i++)
            {
                DataRow DR = DT.NewRow();
                DR["Level"] = i;
                DR["Selected"] = true;
                DT.Rows.Add(DR);
            }
            return DT;
        }

        public DataTable PSNTypeList()
        {
            string SQL = "SELECT PSNL_Type_Code,PSNL_Type_Name\n";
            SQL += " FROM vw_PN_PSNL_Type \n";
            SQL += " ORDER BY  PSNL_Type_Code DESC\n";
            SqlDataAdapter DA = new SqlDataAdapter(SQL, ConnectionString());
            DataTable DT = new DataTable();
            DA.Fill(DT);
            return DT;
        }


        public void BindDDlCreateYear(DropDownList ddl, int R_Year)
        {
            ddl.Items.Clear();
            ddl.Items.Add(new ListItem("เลือกปี", 0.ToString()));
            //------------2557 = ปีเริ่มต้น +544 = ปีปัจจุบัน + 1 
            for (int i = 2557; i <= DateTime.Now.Year + 544; i++)
            {
                ListItem Item = new ListItem(i.ToString(), i.ToString());
                ddl.Items.Add(Item);
            }
            if (R_Year != 0)
            {
                for (int i = 0; i <= ddl.Items.Count - 1; i++)
                {
                    if (ddl.Items[i].Value.ToString() == R_Year.ToString())
                    {
                        ddl.SelectedIndex = i;
                        break; // TODO: might not be correct. Was : Exit For
                    }
                }
            }
        }

        public void BindDDlCreateYear(DropDownList ddl)
        {
            BindDDlCreateYear(ddl, 0);
        }

        //---ดึงข้อมูลประวัติ ระดับและประเภทที่เคยครอง
        public DataTable Class_Type_PSN_History(string PSNL_NO)
        {
            string SQL = "";
            SQL += " --------ดึงข้อมูลประวัติ ระดับและประเภทที่เคยครอง-----------------\n";
            SQL += " DECLARE @PSNL_NO As nvarchar(6)='" + PSNL_NO.Replace("'", "''") + "'\n";
            SQL += " SELECT * FROM (      \n";      
            SQL += " SELECT DISTINCT PSN_PNPS_CLASS,PSN_PSNL_TYPE\n";
            SQL += " FROM vw_HR_PSN_History WHERE PSN_PSNL_NO=@PSNL_NO\n";
            SQL += " UNION ALL \n";
            SQL += " SELECT PNPS_CLASS PSN_PNPS_CLASS, WAGE_TYPE PSN_PSNL_TYPE\n";
            SQL += " FROM vw_PN_PSNL WHERE PSNL_NO=@PSNL_NO\n";
            SQL += " ) AS RoundAll\n";
            SQL += " WHERE PSN_PNPS_CLASS IS NOT NULL OR PSN_PSNL_TYPE IS NOT NULL\n";              
			 
            SqlDataAdapter DA = new SqlDataAdapter(SQL, ConnectionString());
            DataTable DT = new DataTable();
            DA.Fill(DT);
            return DT;
        }
        //----ดึงรอบประเมิน จากประเภทและระดับทั้งหมด
        public DataTable Class_Type_RoundAll()
        {
            string SQL = "";
            SQL += " SELECT R_Year,R_Round,PNPS_CLASS PSN_PNPS_CLASS, PNPS_PSNL_TYPE PSN_PSNL_TYPE\n";
            SQL += " FROM vw_Round_All \n";
            SqlDataAdapter DA = new SqlDataAdapter(SQL, ConnectionString());
            DataTable DT = new DataTable();
            DA.Fill(DT);
            return DT;
        }

        public void BindDDlYearRound_ForTYPE(DropDownList ddl, int R_Year, int R_Round, string PSNL_NO)
        {
            DataTable DT= new DataTable();
            DT.Columns.Add("R_Year", typeof(Int32));
            DT.Columns.Add("R_Round", typeof(Int32));            
            DataRow DR;
            DataTable DT_All = Class_Type_RoundAll();
            DataTable DT_History = Class_Type_PSN_History(PSNL_NO);
            for (int i = 0; i <= DT_History.Rows.Count - 1; i++)
            {
                DT_All.DefaultView.RowFilter = " PSN_PNPS_CLASS=" + DT_History.Rows[i]["PSN_PNPS_CLASS"] + " AND PSN_PSNL_TYPE=" + DT_History.Rows[i]["PSN_PSNL_TYPE"] + "";
                if (DT_All.DefaultView.Count > 0)
                {
                    for (int j = 0; j <= DT_All.DefaultView.Count - 1; j++)
                    {
                        DR = DT.NewRow();
                        DR["R_Year"] = DT_All.DefaultView[j]["R_Year"];
                        DR["R_Round"] = DT_All.DefaultView[j]["R_Round"];
                        DT.Rows.Add(DR);
                    }
                }                
            }
                DT.DefaultView.Sort = "R_Year ASC , R_Round ASC";
                DT = DT.DefaultView.ToTable(true, "R_Year", "R_Round");

            //DataTable DT = GetTypeAssessmentRound(R_Year, R_Round, PSNL_NO);
            ddl.Items.Clear();
            ddl.Items.Add(new ListItem("เลือกรอบ", "0-0"));
            //------------2557 = ปีเริ่มต้น +544 = ปีปัจจุบัน + 1 
            for (int i = 0; i <= DT.Rows.Count - 1; i++)
            {
                ListItem Item = new ListItem("ปี " + DT.Rows[i]["R_Year"] + " รอบ " + DT.Rows[i]["R_Round"], DT.Rows[i]["R_Year"] + "-" + DT.Rows[i]["R_Round"]);
                ddl.Items.Add(Item);
            }
            ddl.SelectedIndex = ddl.Items.Count - 1;
            if (R_Year > 0 & R_Round > 0)
            {
                for (int i = 0; i <= ddl.Items.Count - 1; i++)
                {
                    if (ddl.Items[i].Value == R_Year + "-" + R_Round)
                    {
                        ddl.SelectedIndex = i;
                        break; // TODO: might not be correct. Was : Exit For
                    }
                }
            }


        }

        public void BindDDlYearRound_ForTYPE(DropDownList ddl, int R_Year, int R_Round)
        {
            BindDDlYearRound_ForTYPE(ddl, R_Year, R_Round, "");
        }
        public void BindDDlYearRound_ForTYPE(DropDownList ddl, int R_Year)
        {
            BindDDlYearRound_ForTYPE(ddl, R_Year, 0);
        }
        public void BindDDlYearRound_ForTYPE(DropDownList ddl)
        {
            BindDDlYearRound_ForTYPE(ddl, 0);
        }



        //        public void BindDDlYearRound(DropDownList ddl, int R_Year, int R_Round)
        //        {
        //            BindDDlYearRound(ddl, R_Year, R_Round,0);

        ////            SELECT DISTINCT Round.R_Year,Round.R_Round 
        ////From tb_HR_Round Round 
        ////RIGHT JOIN tb_HR_Round_Type Type ON  Round.R_Year =Type.R_Year 
        ////WHERE Type.PNPS_PSNL_TYPE =9
        ////ORDER BY Round.R_Year,Round.R_Round




        //        }

        public void BindDDlYearRound(DropDownList ddl, int R_Year, int R_Round)
        {
            DataTable DT = GetAllAssessmentRound();
            ddl.Items.Clear();
            ddl.Items.Add(new ListItem("เลือกรอบ", "0-0"));
            //------------2557 = ปีเริ่มต้น +544 = ปีปัจจุบัน + 1 
            for (int i = 0; i <= DT.Rows.Count - 1; i++)
            {
                ListItem Item = new ListItem("ปี " + DT.Rows[i]["R_Year"] + " รอบ " + DT.Rows[i]["R_Round"], DT.Rows[i]["R_Year"] + "-" + DT.Rows[i]["R_Round"]);
                ddl.Items.Add(Item);
            }
            ddl.SelectedIndex = ddl.Items.Count - 1;
            if (R_Year > 0 & R_Round > 0)
            {
                for (int i = 0; i <= ddl.Items.Count - 1; i++)
                {
                    if (ddl.Items[i].Value == R_Year + "-" + R_Round)
                    {
                        ddl.SelectedIndex = i;
                        break; // TODO: might not be correct. Was : Exit For
                    }
                }
            }
        }

        public void BindDDlYearRound(DropDownList ddl, int R_Year)
        {
            BindDDlYearRound(ddl, R_Year, 0);
        }

        public void BindDDlYearRound(DropDownList ddl)
        {
            BindDDlYearRound(ddl, 0);
        }


        public void BindDDlYear(DropDownList ddl, int R_Year)
        {
            DataTable DT = GetAllAssessmentRound();
            string[] Col = { "R_Year" };
            DT = DT.DefaultView.ToTable(true, Col);
            ddl.Items.Clear();
            ddl.Items.Add(new ListItem("เลือกปี", "0"));
            //------------2557 = ปีเริ่มต้น +544 = ปีปัจจุบัน + 1 
            for (int i = 0; i <= DT.Rows.Count - 1; i++)
            {
                ListItem Item = new ListItem(DT.Rows[i]["R_Year"].ToString(), DT.Rows[i]["R_Year"].ToString());
                ddl.Items.Add(Item);
            }
            ddl.SelectedIndex = ddl.Items.Count - 1;
            if (R_Year > 0)
            {
                for (int i = 0; i <= ddl.Items.Count - 1; i++)
                {
                    if (ddl.Items[i].Value.ToString() == R_Year.ToString())
                    {
                        ddl.SelectedIndex = i;
                        break; // TODO: might not be correct. Was : Exit For
                    }
                }
            }
        }

        public void BindDDlYear(DropDownList ddl)
        {
            BindDDlYear(ddl, 0);
        }


        public void BindDDlYearIncompleted(DropDownList ddl, int R_Year)
        {
            int[] Y = GetIncompletedYear();
            ddl.Items.Clear();
            ddl.Items.Add(new ListItem("เลือกปี", "0"));
            for (int i = 0; i <= Y.Length - 1; i++)
            {
                ListItem Item = new ListItem(Y[i].ToString(), Y[i].ToString());
                ddl.Items.Add(Item);
            }
            ddl.SelectedIndex = ddl.Items.Count - 1;
            if (R_Year > 0)
            {
                for (int i = 0; i <= ddl.Items.Count - 1; i++)
                {
                    if (ddl.Items[i].Value == R_Year.ToString())
                    {
                        ddl.SelectedIndex = i;
                        break; // TODO: might not be correct. Was : Exit For
                    }
                }
            }
        }

        public void BindDDlYearIncompleted(DropDownList ddl)
        {
            BindDDlYearIncompleted(ddl, 0);
        }

        public void BindDDlCOMPClassGroup(DropDownList ddl, int R_Year, int R_Round, int SelectedValue)
        {
            string SQL = "SELECT CLSGP_ID,PNPO_CLASS_Start,PNPO_CLASS_End\n";
            SQL += " FROM tb_HR_PNPO_CLASS_GROUP \n";
            SQL += " WHERE R_Year = " + R_Year + " And R_Round = " + R_Round + "\n";
            SQL += " ORDER BY PNPO_CLASS_Start \n";
            SqlDataAdapter DA = new SqlDataAdapter(SQL, ConnectionString());
            DataTable DT = new DataTable();
            DA.Fill(DT);

            ddl.Items.Clear();
            ddl.Items.Add(new ListItem("ทั้งหมด", "0"));
            for (int i = 0; i <= DT.Rows.Count - 1; i++)
            {
                int mn = Convert.ToInt32(DT.Rows[i]["PNPO_CLASS_Start"]);
                int mx = Convert.ToInt32(DT.Rows[i]["PNPO_CLASS_End"]);
                ListItem Item = new ListItem();
                Item.Value = DT.Rows[i]["CLSGP_ID"].ToString();
                if (mn == mx)
                {
                    Item.Text = mn.ToString();
                }
                else
                {
                    Item.Text = mn.ToString() + " - " + mx.ToString();
                }

                ddl.Items.Add(Item);
            }
            ddl.SelectedIndex = 0;
            if (SelectedValue > 0)
            {
                for (int i = 0; i <= ddl.Items.Count - 1; i++)
                {
                    if (ddl.Items[i].Value == SelectedValue.ToString())
                    {
                        ddl.SelectedIndex = i;
                        break; // TODO: might not be correct. Was : Exit For
                    }
                }
            }
        }

        public void BindDDlCOMPClassGroup(DropDownList ddl, int R_Year, int R_Round)
        {
            BindDDlCOMPClassGroup(ddl, R_Year, R_Round, 0);
        }



        public void BindDDlClassAll(DropDownList ddl, string SelectedText)
        {
            DataTable DT = PSNClassLevel();

            ddl.Items.Clear();
            ddl.Items.Add(new ListItem("ทั้งหมด", "-"));
            for (int i = 0; i <= DT.Rows.Count - 1; i++)
            {
                ListItem Item = new ListItem(DT.Rows[i]["Level"].ToString(), DT.Rows[i]["Level"].ToString());
                ddl.Items.Add(Item);
            }

            ddl.SelectedIndex = 0;
            if (!string.IsNullOrEmpty(SelectedText))
            {
                for (int i = 0; i <= ddl.Items.Count - 1; i++)
                {
                    if (ddl.Items[i].Text == SelectedText)
                    {
                        ddl.SelectedIndex = i;
                        break; // TODO: might not be correct. Was : Exit For
                    }
                }
            }

        }
        public void BindDDlClassAll(DropDownList ddl)
        {
            BindDDlClassAll(ddl, "");
        }

        public void BindDDlPSNType(DropDownList ddl, int SelectedValue)
        {
            string SQL = "SELECT PSNL_Type_Code,PSNL_Type_Name\n";
            SQL += " FROM vw_PN_PSNL_Type \n";
            SQL += " ORDER BY PSNL_Type_Code DESC \n";
            SqlDataAdapter DA = new SqlDataAdapter(SQL, ConnectionString());
            DataTable DT = new DataTable();
            DA.Fill(DT);

            ddl.Items.Clear();
            ddl.Items.Add(new ListItem("ทั้งหมด", "-1"));
            for (int i = 0; i <= DT.Rows.Count - 1; i++)
            {
                ListItem Item = new ListItem(DT.Rows[i]["PSNL_Type_Name"].ToString(), DT.Rows[i]["PSNL_Type_Code"].ToString());
                ddl.Items.Add(Item);
            }

            ddl.SelectedIndex = 0;
            if (SelectedValue > 0)
            {
                for (int i = 0; i <= ddl.Items.Count - 1; i++)
                {
                    if (ddl.Items[i].Value == SelectedValue.ToString())
                    {
                        ddl.SelectedIndex = i;
                        break; // TODO: might not be correct. Was : Exit For
                    }
                }
            }
        }

        public void BindDDlPSNType(DropDownList ddl)
        {
            BindDDlPSNType(ddl, -1);
        }


        public void BindDDlFLDCode(DropDownList ddl, string SelectedText)
        {
            string SQL = "SELECT DISTINCT FN_ID,FN_TYPE,FN_NAME FROM vw_HR_FLD\n";
            SQL += "ORDER BY FN_NAME\n";
            SqlDataAdapter DA = new SqlDataAdapter(SQL, ConnectionString());
            DataTable DT = new DataTable();
            DA.Fill(DT);

            ddl.Items.Clear();
            ddl.Items.Add(new ListItem("ทั้งหมด", "-"));
            for (int i = 0; i <= DT.Rows.Count - 1; i++)
            {
                ListItem Item = new ListItem(DT.Rows[i]["FN_NAME"].ToString(), DT.Rows[i]["FN_ID"].ToString() + "-" + DT.Rows[i]["FN_TYPE"].ToString());
                ddl.Items.Add(Item);
            }

            ddl.SelectedIndex = 0;
            if (!string.IsNullOrEmpty(SelectedText))
            {
                for (int i = 0; i <= ddl.Items.Count - 1; i++)
                {
                    if (ddl.Items[i].Text == SelectedText)
                    {
                        ddl.SelectedIndex = i;
                        break; // TODO: might not be correct. Was : Exit For
                    }
                }
            }

        }

        public void BindDDlFLDCode(DropDownList ddl)
        {
            BindDDlFLDCode(ddl, "");
        }

        public void BindDDlSector(DropDownList ddl, string SECTOR_CODE)
        {
            string SQL = "SELECT SECTOR_CODE,SECTOR_NAME From vw_PN_Sector ORDER BY SECTOR_CODE";
            SqlDataAdapter DA = new SqlDataAdapter(SQL, ConnectionString());
            DataTable DT = new DataTable();
            DA.Fill(DT);

            ddl.Items.Clear();
            ddl.Items.Add(new ListItem("เลือกฝ่าย", ""));
            //------------2557 = ปีเริ่มต้น +544 = ปีปัจจุบัน + 1 
            for (int i = 0; i <= DT.Rows.Count - 1; i++)
            {
                //if (DT.Rows[i]["SECTOR_CODE"].ToString() != "3200")
                //{
                ListItem Item = new ListItem(DT.Rows[i]["SECTOR_CODE"].ToString() + " : " + DT.Rows[i]["SECTOR_NAME"].ToString(), DT.Rows[i]["SECTOR_CODE"].ToString());
                ddl.Items.Add(Item);
                //}

            }
            ddl.SelectedIndex = 0;
            if (!string.IsNullOrEmpty(SECTOR_CODE))
            {
                for (int i = 0; i <= ddl.Items.Count - 1; i++)
                {
                    if (ddl.Items[i].Value == SECTOR_CODE)
                    {
                        ddl.SelectedIndex = i;
                        break; // TODO: might not be correct. Was : Exit For
                    }
                }
            }
        }

        public void BindDDlSector(DropDownList ddl)
        {
            BindDDlSector(ddl, "");
        }

        public void BindDDlAssessorRole(DropDownList ddl, int R_Year, int R_Round, string PSNL_No, string ASSESSOR_POS)
        {
            ddl.Items.Clear();
            ddl.Items.Add(new ListItem("ทั้งหมด", ""));

            DataTable DT = GetAssessorRole(R_Year, R_Round, PSNL_No);
            for (int i = 0; i <= DT.Rows.Count - 1; i++)
            {
                ListItem Item = new ListItem(DT.Rows[i]["MGR_POS_NAME"].ToString(), DT.Rows[i]["ASSESSOR_POS"].ToString());
                ddl.Items.Add(Item);
            }
            if (!string.IsNullOrEmpty(ASSESSOR_POS))
            {
                for (int i = 0; i <= ddl.Items.Count - 1; i++)
                {
                    if (ddl.Items[i].Value == ASSESSOR_POS)
                    {
                        ddl.SelectedIndex = i;
                        break; // TODO: might not be correct. Was : Exit For
                    }
                }
            }

        }

        public void BindDDlAssessorRole(DropDownList ddl, int R_Year, int R_Round, string PSNL_No)
        {
            BindDDlAssessorRole(ddl, R_Year, R_Round, PSNL_No, "");
        }



        public void BindDDlTeamDepartment(DropDownList ddl, int R_Year, int R_Round, string Manager_Code, string DEPT_CODE, string MINOR_CODE)
        {
            ddl.Items.Clear();
            ddl.Items.Add(new ListItem("ทั้งหมด", ""));

            DataTable DT = GetTeamDepartment(R_Year, R_Round, Manager_Code);

            string DEPT_Check = "";
            for (int i = 0; i <= DT.Rows.Count - 1; i++)
            {

                string DEPT_Current = DT.Rows[i]["DEPT_CODE"].ToString() + DT.Rows[i]["MINOR_CODE"].ToString();
                if (DEPT_Current.ToString() != DEPT_Check.ToString())
                {
                    ListItem Item = new ListItem(DT.Rows[i]["DEPT_NAME"].ToString(), DT.Rows[i]["DEPT_CODE"].ToString() + "-" + DT.Rows[i]["MINOR_CODE"].ToString());
                    ddl.Items.Add(Item);
                }

                DEPT_Check = DEPT_Current;

            }
            ddl.SelectedIndex = 0;
            if (!string.IsNullOrEmpty(DEPT_CODE) & !string.IsNullOrEmpty(MINOR_CODE))
            {
                for (int i = 0; i <= ddl.Items.Count - 1; i++)
                {
                    if (ddl.Items[i].Value == DEPT_CODE + "-" + MINOR_CODE)
                    {
                        ddl.SelectedIndex = i;
                        break; // TODO: might not be correct. Was : Exit For
                    }
                }
            }

        }

        public void BindDDlTeamDepartment(DropDownList ddl, int R_Year, int R_Round, string Manager_Code, string DEPT_CODE)
        {
            BindDDlTeamDepartment(ddl, R_Year, R_Round, Manager_Code, DEPT_CODE, "");
        }

        public void BindDDlTeamDepartment(DropDownList ddl, int R_Year, int R_Round, string Manager_Code)
        {
            BindDDlTeamDepartment(ddl, R_Year, R_Round, Manager_Code, "");
        }

        public void BindDDlWorkloadDepartment(DropDownList ddl, string SECTOR_CODE, string DEPT_CODE)
        {
            DataTable DT = GetWorkloadDept(SECTOR_CODE, DEPT_CODE);

            ddl.Items.Clear();
            ddl.Items.Add(new ListItem("ทั้งหมด", ""));
            for (int i = 0; i <= DT.Rows.Count - 1; i++)
            {
                ListItem Item = new ListItem(DT.Rows[i]["DEPT_CODE"].ToString() + " : " + DT.Rows[i]["DEPT_NAME"].ToString(), DT.Rows[i]["DEPT_CODE"].ToString());
                ddl.Items.Add(Item);
            }
            ddl.SelectedIndex = 0;
            if (!string.IsNullOrEmpty(DEPT_CODE))
            {
                for (int i = 0; i <= ddl.Items.Count - 1; i++)
                {
                    if (ddl.Items[i].Value == DEPT_CODE)
                    {
                        ddl.SelectedIndex = i;
                        break; // TODO: might not be correct. Was : Exit For
                    }
                }
            }
        }

        public DataTable GetWorkloadDept(string SECTOR_CODE, string DEPT_CODE)
        {
            string SQL = "SELECT * FROM vw_HR_Department \n";
            string Filter = "";
            if (SECTOR_CODE != "")
            {
                Filter += " SECTOR_CODE='" + SECTOR_CODE.Replace("'", "''") + "' AND ";
            }
            if (DEPT_CODE != "")
            {
                Filter += " DEPT_CODE='" + DEPT_CODE.Replace("'", "''") + "' AND ";
            }
            if (Filter != "") SQL += " WHERE " + Filter.Substring(0, Filter.Length - 4) + "\n";
            Filter += "ORDER BY DEPT_CODE,DEPT_NAME";

            SqlDataAdapter DA = new SqlDataAdapter(SQL, ConnectionString());
            DataTable DT = new DataTable();
            DA.Fill(DT);
            return DT;
        }

        public void BindDDlDepartment(DropDownList ddl, string SECTOR_CODE, string DEPT_CODE, string MINOR_CODE)
        {
            ddl.Items.Clear();
            ddl.Items.Add(new ListItem("ทั้งหมด", ""));

            string SQL = "SELECT DEPT_CODE,MINOR_CODE,DEPT_NAME\n";
            SQL += " FROM vw_HR_Department\n";
            if (!string.IsNullOrEmpty(SECTOR_CODE))
            {
                SQL += " WHERE SECTOR_CODE='" + SECTOR_CODE.Replace("'", "''") + "' \n";
            }
            SQL += "ORDER BY DEPT_NAME\n";

            SqlDataAdapter DA = new SqlDataAdapter(SQL, ConnectionString());
            DataTable DT = new DataTable();
            DA.Fill(DT);

            ddl.Items.Clear();
            ddl.Items.Add(new ListItem("ทั้งหมด", ""));
            for (int i = 0; i <= DT.Rows.Count - 1; i++)
            {
                ListItem Item = new ListItem(DT.Rows[i]["DEPT_NAME"].ToString(), DT.Rows[i]["DEPT_CODE"].ToString() + "-" + DT.Rows[i]["MINOR_CODE"].ToString());
                ddl.Items.Add(Item);
            }
            ddl.SelectedIndex = 0;
            if (!string.IsNullOrEmpty(DEPT_CODE) & !string.IsNullOrEmpty(MINOR_CODE))
            {
                for (int i = 0; i <= ddl.Items.Count - 1; i++)
                {
                    if (ddl.Items[i].Value == DEPT_CODE + "-" + MINOR_CODE)
                    {
                        ddl.SelectedIndex = i;
                        break; // TODO: might not be correct. Was : Exit For
                    }
                }
            }
        }

        public void BindDDlDepartment(DropDownList ddl, string SECTOR_CODE, string DEPT_CODE)
        {
            BindDDlDepartment(ddl, SECTOR_CODE, DEPT_CODE, "");
        }

        public void BindDDlDepartment(DropDownList ddl, string SECTOR_CODE)
        {
            BindDDlDepartment(ddl, SECTOR_CODE, "");
        }

        public void BindDDlDepartment(DropDownList ddl)
        {
            BindDDlDepartment(ddl, "");
        }


        public void BindDDLAssessor(DropDownList ddl, int R_Year, int R_Round, string PSNL_No)
        {
            //------------- หาคนที่เคยเลือก ------------------
            string ASSESSOR_CODE = "";
            string SQL = "SELECT * \n";
            SQL += " FROM tb_HR_Actual_Assessor \n";
            SQL += " WHERE R_Year = " + R_Year + " And R_Round = " + R_Round + " And PSNL_No ='" + PSNL_No.Replace("'", "''") + "'\n";
            SqlDataAdapter DA = new SqlDataAdapter(SQL, ConnectionString());
            DataTable DT = new DataTable();
            DA.Fill(DT);
            if (DT.Rows.Count > 0 && !GL.IsEqualNull(DT.Rows[0]["MGR_PSNL_NO"]))
            {
                ASSESSOR_CODE = DT.Rows[0]["MGR_PSNL_NO"].ToString();
            }

            DT = GetAssessorList(R_Year, R_Round, PSNL_No);
            //MGR_BY ไม่ซ้ำ 

            //----------- Default Manager ยังว่าง ---------------
            if (string.IsNullOrEmpty(ASSESSOR_CODE) & DT.Rows.Count > 0)
            {
                DT.DefaultView.RowFilter = "MGR_Type=1 ";
                //----------- เลือกคนที่เคยเลือกในรอบนั้น ---------------
                if (DT.DefaultView.Count > 0)
                {
                    ASSESSOR_CODE = DT.DefaultView[0]["MGR_By"].ToString();
                }
                DT.DefaultView.RowFilter = "";
                //----------- เลือกคนแรกใน List ---------------
                if (string.IsNullOrEmpty(ASSESSOR_CODE))
                {
                    ASSESSOR_CODE = DT.Rows[0]["MGR_By"].ToString();
                }
            }

            string[] Col = {
				"MGR_By",
				"MGR_Name",
				"MGR_Pos",
				"MGR_DEPT"
			};
            DT = DT.DefaultView.ToTable(true, Col);

            ddl.Items.Clear();
            ddl.Items.Add(new ListItem("...", ""));

            int SelectedIndex = 0;
            for (int i = 0; i <= DT.Rows.Count - 1; i++)
            {
                ListItem Item = new ListItem(DT.Rows[i]["MGR_Name"].ToString(), DT.Rows[i]["MGR_BY"].ToString() + ":::" + DT.Rows[i]["MGR_Pos"].ToString() + ":::" + DT.Rows[i]["MGR_DEPT"].ToString());
                ddl.Items.Add(Item);
                //----------- เลือกคนที่คิดได้ ---------------
                if ((Convert.ToInt32(SelectedIndex) == Convert.ToInt32(0)) & (Convert.ToInt32(DT.Rows[i]["MGR_BY"]) == Convert.ToInt32(ASSESSOR_CODE)))
                {
                    SelectedIndex = ddl.Items.Count - 1;
                }
            }

            ddl.SelectedIndex = SelectedIndex;


        }

        public void BindGroup_Course(DropDownList ddl, string SUBJ_G_ID)
        {
            string SQL = "SELECT * FROM tb_PK_SUBJ_G";
            SqlDataAdapter DA = new SqlDataAdapter(SQL, ConnectionString());
            DataTable DT = new DataTable();
            DA.Fill(DT);

            ddl.Items.Clear();
            ddl.Items.Add(new ListItem("เลือกกลุ่มหลักสูตร", ""));
            //------------2557 = ปีเริ่มต้น +544 = ปีปัจจุบัน + 1 
            for (int i = 0; i <= DT.Rows.Count - 1; i++)
            {
                ListItem Item = new ListItem(DT.Rows[i]["SUBJ_G_ID"].ToString() + " : " + DT.Rows[i]["SUBJ_G_DESC"].ToString(), DT.Rows[i]["SUBJ_G_ID"].ToString());
                ddl.Items.Add(Item);
            }
            ddl.SelectedIndex = 0;
            if (!string.IsNullOrEmpty(SUBJ_G_ID))
            {
                for (int i = 0; i <= ddl.Items.Count - 1; i++)
                {
                    if (ddl.Items[i].Value == SUBJ_G_ID)
                    {
                        ddl.SelectedIndex = i;
                        break; // TODO: might not be correct. Was : Exit For
                    }
                }
            }
        }

        public void BindGroup_Course(DropDownList ddl)
        {
            BindGroup_Course(ddl, "");
        }



        //Carrer Path


        public DataTable GetCP_Header(string PSNL_No)
        {
            string SQL = "";
            SQL += "  SELECT  vw.SECTOR_CODE, vw.SECTOR_NAME,vw.DEPT_NAME,vw.DEPT_CODE,vw.PSNL_NO,vw.PSNL_Fullname, vw.POS_NO, vw.PNPS_CLASS,vw.WAGE_NAME,ISNULL(MGR_NAME,FN_NAME) POS_Name\n";
            SQL += "  ,ISNULL(Result.Year_Score,0) Year_Score\n";
            SQL += "  ---------------------------------Count history------------------------------------------\n";
            SQL += "  ,ISNULL ((History_Pos.Count_Positions),0) AS History_Pos\n";
            SQL += "  ---------------------------------Count Course------------------------------------------\n";
            SQL += "  ,ISNULL ((History_Course.Count_Course),0) AS History_Course\n";
            SQL += "  ---------------------------------Count PUNISH------------------------------------------\n";
            SQL += "  ,CASE \n";
            SQL += "  WHEN ISNULL ((History_PUNISH.Count_PUNISH),0) >0 THEN 'เคยรับโทษ'\n";
            SQL += "  ELSE 'ไม่เคยรับโทษ' END History_PUNISH\n";
            SQL += "  ,CASE \n";
            SQL += "  WHEN ISNULL ((History_PUNISH.Count_PUNISH),0) >0 THEN 0\n";
            SQL += "  ELSE 1 END History_Status_PUNISH\n";
            SQL += "  FROM vw_PN_PSNL vw \n";
            SQL += "  LEFT JOIN vw_Path_3Round_Result Result ON vw.PSNL_NO =Result.PSNL_NO\n";

            SQL += "  ----------------------History Position----------------------------------------\n";
            SQL += "  LEFT JOIN vw_Path_Count_History_Position History_Pos ON vw.PSNL_NO=History_Pos.PSNL_NO\n";
            SQL += "  -----------------------History Course---------------------------------------\n";
            SQL += "  LEFT JOIN vw_Path_Count_History_Course History_Course ON vw.PSNL_NO=History_Course.PSNL_NO\n";
            SQL += "  -----------------------History Course---------------------------------------\n";
            SQL += "  LEFT JOIN vw_Path_Count_History_PUNISH History_PUNISH ON vw.PSNL_NO=History_PUNISH.PSNL_NO\n";
            SQL += "  WHERE vw.PSNL_NO IS NOT NULL ";
            SQL += "  AND vw.PSNL_NO ='" + PSNL_No + "' \n";
            SQL += "  ORDER BY SECTOR_CODE,DEPT_CODE,vw.PNPS_CLASS DESC,POS_NO\n";


            SqlDataAdapter DA = new SqlDataAdapter(SQL, ConnectionString());
            DataTable DT = new DataTable();
            DA.Fill(DT);

            return DT;
        }


        #endregion

        #region "Workload Simulation"

        public int GetNewWorkLoadSimID()
        {
            string SQL = "SELECT ISNULL(MAX(SIM_ID),0)+1 SIM_ID \n";
            SQL += " FROM tb_SIM\n";
            SqlDataAdapter DA = new SqlDataAdapter(SQL, ConnectionString());
            DataTable DT = new DataTable();
            DA.Fill(DT);
            return GL.CINT(DT.Rows[0][0]);
        }

        public int GetNewWorkLoadSimPSNID(int SIM_ID)
        {
            string SQL = "SELECT ISNULL(MAX(PSN_ID),0)+1 PSN_ID \n";
            SQL += " FROM tb_SIM_Workload_PSN\n";
            SQL += " WHERE SIM_ID=" + SIM_ID + "\n";
            SqlDataAdapter DA = new SqlDataAdapter(SQL, ConnectionString());
            DataTable DT = new DataTable();
            DA.Fill(DT);
            return GL.CINT(DT.Rows[0][0]);
        }

        public int GetNewWorkLoadSimSlotID(int SIM_ID)
        {
            string SQL = "SELECT ISNULL(MAX(Slot_ID),0)+1 Slot_ID \n";
            SQL += " FROM tb_SIM_Workload_Slot\n";
            SQL += " WHERE SIM_ID=" + SIM_ID + "\n";

            SqlDataAdapter DA = new SqlDataAdapter(SQL, ConnectionString());
            DataTable DT = new DataTable();
            DA.Fill(DT);
            return GL.CINT(DT.Rows[0][0]);
        }

        public void RemoveWorkLoadSimSlot_ByPerson(int SIM_ID, int PSN_ID)
        {
            SqlConnection Conn = new SqlConnection(ConnectionString());
            Conn.Open();
            SqlCommand Comm = new SqlCommand();
            var _with9 = Comm;
            _with9.Connection = Conn;
            _with9.CommandType = CommandType.Text;
            _with9.CommandText = "DELETE FROM tb_SIM_Workload_Slot WHERE SIM_ID=" + SIM_ID + " AND PSN_ID=" + PSN_ID;
            _with9.ExecuteNonQuery();
            _with9.Dispose();
            Conn.Close();
            Conn.Dispose();
        }


        public void CloneWorkloadJobForSimulate(int SIM_ID, string DEPT_CODE, string MINOR_CODE, string PSNL_NO)
        {
            string SQL = "";
            //------------ Save tb_SIM_DEPT-------------------------
            SQL += "INSERT INTO tb_SIM_DEPT \n";
            SQL += "SELECT " + SIM_ID + ",PNDP_DEPT_CODE,PNDP_MINOR_CODE,(SELECT ISNULL(MAX(DEPT_No),0)+1 FROM tb_SIM_DEPT WHERE SIM_ID=" + SIM_ID + "),PNDP_DEPARTMENT_NAME,PNDP_SECTOR_NAME \n";
            SQL += "FROM tb_PN_DEPARTMENT_R \n";
            SQL += "WHERE PNDP_DEPT_CODE='" + DEPT_CODE.Replace("'", "''") + "' AND PNDP_MINOR_CODE='" + MINOR_CODE + "'\n\n";

            //------------ Save tb_SIM_Job_Mapping_Class -----------
            SQL += "INSERT INTO tb_SIM_Job_Mapping_Class \n";
            SQL += "SELECT " + SIM_ID + ",DEPT_CODE,MINOR_CODE,Class_ID,PNPO_CLASS_Start,PNPO_CLASS_End \n";
            SQL += "FROM tb_HR_Job_Mapping_Class ";
            SQL += "WHERE DEPT_CODE='" + DEPT_CODE.Replace("'", "''") + "' AND MINOR_CODE='" + MINOR_CODE + "'\n\n";

            //------------ Save tb_SIM_Job_Mapping -----------------
            SQL += "INSERT INTO tb_SIM_Job_Mapping \n";
            SQL += "SELECT " + SIM_ID + ",Job_ID,PNDP_DEPT_CODE,PNDP_MINOR_CODE,Job_LEVEL,Job_Parent,Job_No,Job_Name,NumPerson,MinPerTime,TimePerYear,Update_By,Update_Time\n";
            SQL += "FROM tb_HR_Job_Mapping \n";
            SQL += "WHERE PNDP_DEPT_CODE='" + DEPT_CODE.Replace("'", "''") + "' AND PNDP_MINOR_CODE='" + MINOR_CODE + "'\n\n";

            //------------ Save tb_SIM_Job_Mapping_Detail ----------
            SQL += "INSERT INTO tb_SIM_Job_Mapping_Detail \n";
            SQL += "SELECT " + SIM_ID + ",Job_ID,Class_ID,Job_Name,Update_By,Update_Time\n";
            SQL += "FROM tb_HR_Job_Mapping_Detail \n";
            SQL += "WHERE Job_ID IN (SELECT Job_ID FROM tb_HR_Job_Mapping WHERE PNDP_DEPT_CODE='" + DEPT_CODE.Replace("'", "''") + "' AND PNDP_MINOR_CODE='" + MINOR_CODE + "')\n\n";

            //------------ Save tb_SIM_Workload_PSN ----------------
            SQL += "INSERT INTO tb_SIM_Workload_PSN \n";
            SQL += "SELECT " + SIM_ID + ",PSN_ID,DEPT_CODE,MINOR_CODE,Class_ID,Slot_No,Slot_Name,REF_PSNL_NO,Update_By,Update_Time\n";
            SQL += "FROM tb_HR_Workload_PSN \n";
            SQL += "WHERE DEPT_CODE='" + DEPT_CODE.Replace("'", "''") + "' AND MINOR_CODE='" + MINOR_CODE + "'\n\n";

            //------------ Save tb_SIM_Workload_Slot ---------------
            SQL += "INSERT INTO tb_SIM_Workload_Slot \n";
            SQL += "SELECT " + SIM_ID + ",Slot_ID,PSN_ID,Job_ID,MinPerYear,Update_By,Update_Time\n";
            SQL += "FROM tb_HR_Workload_Slot \n";
            SQL += "WHERE Job_ID IN (SELECT Job_ID FROM tb_HR_Job_Mapping WHERE PNDP_DEPT_CODE='" + DEPT_CODE.Replace("'", "''") + "' AND PNDP_MINOR_CODE='" + MINOR_CODE + "')\n\n";

            SqlConnection Conn = new SqlConnection(ConnectionString());
            Conn.Open();
            SqlCommand COMM = new SqlCommand();
            var _with10 = COMM;
            _with10.CommandType = CommandType.Text;
            _with10.Connection = Conn;
            _with10.CommandText = SQL;
            _with10.ExecuteNonQuery();
            _with10.Dispose();
            Conn.Close();

        }

        public void SetSIMAutoClass(int SIM_ID, string DEPT_CODE, string MINOR_CODE)
        {
            DataTable RoundData = GetAllAssessmentRound();
            RoundData.DefaultView.Sort = "R_Year DESC,R_Round DESC";
            RoundData = RoundData.DefaultView.ToTable();

            if (RoundData.Rows.Count == 0)
                return;
            string SQL = "SELECT * FROM tb_HR_PNPO_CLASS_GROUP WHERE R_Year=" + RoundData.Rows[0]["R_Year"] + " AND R_Round=" + RoundData.Rows[0]["R_Round"] + " ORDER BY CLSGP_ID";
            SqlDataAdapter DA = new SqlDataAdapter(SQL, ConnectionString());
            DataTable ClassData = new DataTable();
            DA.Fill(ClassData);

            if (ClassData.Rows.Count == 0)
                return;

            //------------- Insert DEPT first if Not Exists --------
            DataTable DT = new DataTable();
            SQL = " IF NOT EXISTS(SELECT * FROM tb_SIM_DEPT WHERE SIM_ID=" + SIM_ID + " AND DEPT_CODE='" + DEPT_CODE.Replace("'", "''") + "' AND MINOR_CODE='" + MINOR_CODE.Replace("'", "''") + "')\n";
            SQL += " BEGIN\n";
            SQL += " \tINSERT INTO tb_SIM_DEPT\n";
            SQL += " \tSELECT \n";
            SQL += " \t\t" + SIM_ID + " SIM_ID,\n";
            SQL += " \t\t'" + DEPT_CODE.Replace("'", "''") + "' DEPT_CODE,\n";
            SQL += " \t\t'" + MINOR_CODE.Replace("'", "''") + "' MINOR_CODE,\n";
            SQL += " \t\t(SELECT ISNULL(MAX(DEPT_No),0)+1 FROM tb_SIM_DEPT WHERE SIM_ID=" + SIM_ID + ") DEPT_No,\n";
            SQL += " \t\tDEPT_NAME,\n";
            SQL += " \t\tSECTOR_NAME\n";
            SQL += " \tFROM vw_HR_Department WHERE DEPT_CODE='" + DEPT_CODE.Replace("'", "''") + "' AND MINOR_CODE='" + MINOR_CODE.Replace("'", "''") + "'\n";
            SQL += " END\n";
            DA = new SqlDataAdapter(SQL, ConnectionString());
            DA.Fill(DT);

            SQL = "DELETE FROM tb_SIM_Job_Mapping_Class WHERE SIM_ID=" + SIM_ID + " AND DEPT_CODE='" + DEPT_CODE.Replace("'", "''") + "' AND MINOR_CODE='" + MINOR_CODE.Replace("'", "''") + "'";
            DA = new SqlDataAdapter(SQL, ConnectionString());
            DA.Fill(DT);

            SQL = "SELECT * FROM tb_SIM_Job_Mapping_Class WHERE SIM_ID=" + SIM_ID + " AND DEPT_CODE='" + DEPT_CODE.Replace("'", "''") + "' AND MINOR_CODE='" + MINOR_CODE.Replace("'", "''") + "'";
            DA = new SqlDataAdapter(SQL, ConnectionString());
            DT = new DataTable();
            DA.Fill(DT);

            for (int i = 0; i <= ClassData.Rows.Count - 1; i++)
            {
                DataRow DR = DT.NewRow();
                DR["SIM_ID"] = SIM_ID;
                DR["DEPT_CODE"] = DEPT_CODE;
                DR["MINOR_CODE"] = MINOR_CODE;
                DR["Class_ID"] = i + 1;
                DR["PNPO_CLASS_Start"] = ClassData.Rows[i]["PNPO_CLASS_Start"];
                DR["PNPO_CLASS_End"] = ClassData.Rows[i]["PNPO_CLASS_End"];
                DT.Rows.Add(DR);
            }

            SqlCommandBuilder cmd = new SqlCommandBuilder(DA);
            DA.Update(DT);

        }

        public void DeleteWorkloadSIM(int SIM_ID)
        {
            string SQL = "";
            SQL += " DELETE FROM tb_SIM_Workload_Slot WHERE SIM_ID=" + SIM_ID + "\n";
            SQL += " DELETE FROM tb_SIM_Workload_PSN WHERE SIM_ID=" + SIM_ID + "\n";
            SQL += " DELETE FROM tb_SIM_Job_Mapping_Detail WHERE SIM_ID=" + SIM_ID + "\n";
            SQL += " DELETE FROM tb_SIM_Job_Mapping WHERE SIM_ID=" + SIM_ID + "\n";
            SQL += " DELETE FROM tb_SIM_Job_Mapping_Class WHERE SIM_ID=" + SIM_ID + "\n";
            SQL += " DELETE FROM tb_SIM_DEPT WHERE SIM_ID=" + SIM_ID + "\n";
            SQL += " DELETE FROM tb_SIM WHERE SIM_ID=" + SIM_ID + "\n";

            SqlConnection Conn = new SqlConnection(ConnectionString());
            Conn.Open();
            SqlCommand COMM = new SqlCommand();
            var _with11 = COMM;
            _with11.CommandType = CommandType.Text;
            _with11.Connection = Conn;
            _with11.CommandText = SQL;
            _with11.ExecuteNonQuery();
            _with11.Dispose();
            Conn.Close();
        }


        public void SetWorkloadSIMDefaultPSN(int SIM_ID, string DEPT_CODE, string MINOR_CODE, int Class_ID)
        {
            //----------- Get Current Organize Person -----------

            string SQL = " SELECT PSNL_NO,PSNL_Fullname + '(' + CAST(CAST(PNPS_CLASS AS INT) AS VARCHAR) + ')' PSNL_Fullname,Class_ID\n";
            SQL += " FROM vw_PN_PSNL PSN\n";
            SQL += " INNER JOIN tb_SIM_Job_Mapping_Class CLS ON PSN.DEPT_CODE=CLS.DEPT_CODE \n";
            SQL += " \t\t\t\t\t\t\t\t\t\tAND PSN.MINOR_CODE=CLS.MINOR_CODE\n";
            SQL += " \t\t\t\t\t\t\t\t\t\tAND CAST(PNPS_CLASS AS INT) BETWEEN CLS.PNPO_CLASS_Start AND CLS.PNPO_CLASS_End\n";
            SQL += " WHERE CLS.SIM_ID=" + SIM_ID + " AND PNPS_RETIRE_DATE>GETDATE() AND PSNL_STAT='02'\n";
            SQL += " AND PSN.DEPT_CODE='" + DEPT_CODE.Replace("'", "''") + "' AND PSN.MINOR_CODE='" + MINOR_CODE.Replace("'", "''") + "'\n";
            if (Class_ID != 0)
            {
                SQL += " AND CLS.Class_ID=" + Class_ID + "\n";
            }
            SQL += " ORDER BY CAST(PNPS_CLASS AS INT) DESC,PSNL_Fullname\n";
            SqlDataAdapter DA = new SqlDataAdapter(SQL, ConnectionString());
            DataTable PSN = new DataTable();
            DA.Fill(PSN);

            //----------- Get Exists Person In Slot-----------
            SQL = " SELECT * FROM tb_SIM_Workload_PSN \n";
            SQL += " WHERE SIM_ID=" + SIM_ID + " AND DEPT_CODE='" + DEPT_CODE.Replace("'", "''") + "' AND MINOR_CODE='" + MINOR_CODE.Replace("'", "''") + "'\n";
            if (Class_ID != 0)
            {
                SQL += " AND Class_ID=" + Class_ID + "\n";
            }
            DA = new SqlDataAdapter(SQL, ConnectionString());
            DataTable Target = new DataTable();
            DA.Fill(Target);

            //--------- Compare ------------
            for (int i = Target.Rows.Count - 1; i >= 0; i += -1)
            {
                //------------ เอาคนที่ Manual เพิ่มเข้าไปใหม่ออก -----------
                if (GL.IsEqualNull(Target.Rows[i]["REF_PSNL_NO"]) || string.IsNullOrEmpty(Target.Rows[i]["REF_PSNL_NO"].ToString()))
                {
                    RemoveWorkLoadSimSlot_ByPerson(SIM_ID, GL.CINT(Target.Rows[i]["PSN_ID"]));
                    Target.Rows[i].Delete();
                }
                else
                {
                    //---------- เอาคนที่ไม่มีออกไป ---------
                    PSN.DefaultView.RowFilter = "PSNL_NO='" + Target.Rows[i]["REF_PSNL_NO"].ToString().Replace("'", "''") + "'";
                    if (PSN.DefaultView.Count == 0)
                    {
                        RemoveWorkLoadSimSlot_ByPerson(SIM_ID, GL.CINT(Target.Rows[i]["PSN_ID"]));
                        Target.Rows[i].Delete();
                    }
                }
            }

            SqlCommandBuilder cmd = new SqlCommandBuilder(DA);
            DA.Update(Target);
            Target.AcceptChanges();

            for (int i = 0; i <= PSN.Rows.Count - 1; i++)
            {
                Target.DefaultView.RowFilter = "REF_PSNL_NO='" + PSN.Rows[i]["PSNL_NO"].ToString().Replace("'", "''") + "'";
                if (Target.DefaultView.Count > 0)
                {
                    //------------ เอาคนที่อยู่ผิด Class ปรับให้อยู่ถูก Class ------
                    Target.DefaultView[0].Row["Class_ID"] = PSN.Rows[i]["Class_ID"];
                    Target.DefaultView[0].Row["Slot_Name"] = PSN.Rows[i]["PSNL_Fullname"];
                }
                else
                {
                    //----------- ยังไม่มีในของเดิมให้เพิ่มเข้าไปใหม่ ---------------
                    DataRow DR = Target.NewRow();
                    DR["SIM_ID"] = SIM_ID;
                    DR["PSN_ID"] = GetNewWorkLoadSimPSNID(SIM_ID);
                    DR["DEPT_CODE"] = DEPT_CODE;
                    DR["MINOR_CODE"] = MINOR_CODE;
                    DR["Class_ID"] = PSN.Rows[i]["Class_ID"];
                    DR["Slot_No"] = DBNull.Value;
                    DR["Slot_Name"] = PSN.Rows[i]["PSNL_Fullname"];
                    DR["REF_PSNL_NO"] = PSN.Rows[i]["PSNL_NO"];
                    DR["Update_By"] = 0;
                    DR["Update_Time"] = DateTime.Now;
                    Target.Rows.Add(DR);
                }
                try
                {
                    cmd = new SqlCommandBuilder(DA);
                    DA.Update(Target);
                    Target.AcceptChanges();
                }
                catch
                {
                }
            }
            Target.DefaultView.RowFilter = "";
            Target.DefaultView.Sort = "Class_ID,REF_PSNL_NO";

            string[] Col = { "Class_ID" };
            //-------------- Reorder Slot No ----------
            DataTable CLS = Target.DefaultView.ToTable(true, Col).Copy();
            for (int i = 0; i <= CLS.Rows.Count - 1; i++)
            {
                Target.DefaultView.RowFilter = "Class_ID=" + CLS.Rows[i]["Class_ID"];
                for (int j = 0; j <= Target.DefaultView.Count - 1; j++)
                {
                    Target.DefaultView[j]["Slot_No"] = j + 1;
                }
            }
            try
            {
                cmd = new SqlCommandBuilder(DA);
                DA.Update(Target);
                Target.AcceptChanges();
            }
            catch
            {
            }
        }

        public void SetWorkloadSIMDefaultPSN(int SIM_ID, string DEPT_CODE, string MINOR_CODE)
        {
            SetWorkloadSIMDefaultPSN(SIM_ID, DEPT_CODE, MINOR_CODE, 0);
        }

        #endregion

        public void Export_Assessment_Result_To_Oracle(DataTable SQL_DT)
        {
            if ((SQL_DT == null) || SQL_DT.Rows.Count == 0)
                return;

            int Y = GL.CINT(SQL_DT.Rows[0]["R_Year"]);
          
            //pa_eva_result
            string SQL = "SELECT * FROM pa_eva_result WHERE PAER_YEAR=" + Y.ToString();
            System.Data.OracleClient.OracleDataAdapter DA = new System.Data.OracleClient.OracleDataAdapter(SQL, OracleConnectionString());
            DataTable DT = new DataTable();
            DA.Fill(DT);

            //PAER_YEAR
            //PAER_SCORE1
            //PAER_SCORE2
            //PAER_LEAVE
            //UPD_BY
            //UPD_DATE
            //PAER_DE_SCORE
            //PNPS_PSNL_NO
            //PAER_LEVEL_TYPE


            // 1. UPDATE รายการที่ส่งข้อมูลไปยัง Oracle
            string SQL_Export_Status_Oracle = " SELECT * FROM tb_Export_Status_Oracle WHERE R_Year='" + Y.ToString() + "' ";
            SqlDataAdapter DA_Export_Status_Oracle = new SqlDataAdapter(SQL_Export_Status_Oracle, ConnectionString());
            DataTable DT_Export_Status_Oracle = new DataTable();
            DA_Export_Status_Oracle.Fill(DT_Export_Status_Oracle);

            for (int i = 0; i <= SQL_DT.Rows.Count - 1; i++)
            {
                DT.DefaultView.RowFilter = "PNPS_PSNL_NO='" + SQL_DT.Rows[i]["PSNL_NO"].ToString().Replace("'", "''") + "'";
                DataRow DR = null;

                if (DT.DefaultView.Count == 0)
                {
                    DR = DT.NewRow();
                }
                else
                {
                    DR = DT.DefaultView[0].Row;
                }

                for (int j = 0; j <= DT.Columns.Count - 1; j++)
                {
                    if (SQL_DT.Columns.IndexOf(DT.Columns[j].ColumnName) > -1 && !GL.IsEqualNull(SQL_DT.Rows[i][DT.Columns[j].ColumnName]))
                    {
                        DR[DT.Columns[j].ColumnName] = SQL_DT.Rows[i][DT.Columns[j].ColumnName];
                    }
                    else
                    {
                        DR[DT.Columns[j].ColumnName] = DBNull.Value;
                    }
                }
                if (DT.DefaultView.Count == 0)
                    DT.Rows.Add(DR);


                //=============================================
                //=============================================

                // 2. UPDATE รายการที่ส่งข้อมูลไปยัง Oracle
                DT_Export_Status_Oracle.DefaultView.RowFilter = " PSNL_NO='" + SQL_DT.Rows[i]["PSNL_NO"].ToString().Replace("'", "''") + "'";
                DataRow DR_Export_Status_Oracle = null;
                if (DT_Export_Status_Oracle.DefaultView.Count == 0)
                {
                    DR_Export_Status_Oracle = DT_Export_Status_Oracle.NewRow();
                    DR_Export_Status_Oracle["R_Year"] = Y.ToString();
                    DR_Export_Status_Oracle["PSNL_NO"] = SQL_DT.Rows[i]["PSNL_NO"].ToString();
                    DR_Export_Status_Oracle["Create_Time"] = DateTime.Now;
                    //DR["Create_By"] = User_By;
                }
                else
                {
                    DR_Export_Status_Oracle = DT_Export_Status_Oracle.DefaultView[0].Row;

                    DR_Export_Status_Oracle["Create_Time"] = DateTime.Now;
                  

                }
                if (DT_Export_Status_Oracle.DefaultView.Count == 0)
                    DT_Export_Status_Oracle.Rows.Add(DR_Export_Status_Oracle);
                //=============================================
                //=============================================


            }
            System.Data.OracleClient.OracleCommandBuilder cmd = new System.Data.OracleClient.OracleCommandBuilder(DA);
            DA.Update(DT);


            SqlCommandBuilder cmd_Export_Status_Oracle = new SqlCommandBuilder(DA_Export_Status_Oracle);
            DA_Export_Status_Oracle.Update(DT_Export_Status_Oracle);



        }

        public void Export_Assessment_KPI_Result_To_Oracle(DataTable SQL_DT)
        {
            if ((SQL_DT == null) || SQL_DT.Rows.Count == 0)
                return;

            int Y = GL.CINT(SQL_DT.Rows[0]["R_Year"]);
            int R = GL.CINT(SQL_DT.Rows[0]["R_Round"]);
            //pa_kpi_result
            string SQL = "SELECT * FROM pa_kpi_result WHERE PAKR_YEAR=" + Y.ToString() + " AND PAKR_ROUND=" + R;
            System.Data.OracleClient.OracleDataAdapter DA = new System.Data.OracleClient.OracleDataAdapter(SQL, OracleConnectionString());
            DataTable DT_Oracle = new DataTable();
            DA.Fill(DT_Oracle);

            //PAKR_YEAR
            //PAKR_ROUND
            //PAKR_PSNL_NO
            //PAKR_KPI_FULL_MARKS
            //PAKR_KPI_SCORE
            //PAKR_KPI_PERCENT_WEIGHT
            //UPD_BY
            //UPD_DATE

            // 1. UPDATE รายการที่ส่งข้อมูลไปยัง Oracle
            string SQL_Export_Status_Oracle = " SELECT * FROM tb_Export_KPI_Status_Oracle WHERE R_Year='" + Y.ToString() + "' AND R_Round='"+ R.ToString ()+"' ";
            SqlDataAdapter DA_Export_Status_Oracle = new SqlDataAdapter(SQL_Export_Status_Oracle, ConnectionString());
            DataTable DT_Export_Status_Oracle = new DataTable();
            DA_Export_Status_Oracle.Fill(DT_Export_Status_Oracle);

            for (int i = 0; i <= SQL_DT.Rows.Count - 1; i++)
            {
                DT_Oracle.DefaultView.RowFilter = "PAKR_PSNL_NO='" + SQL_DT.Rows[i]["PSNL_NO"].ToString().Replace("'", "''") + "'";
                DataRow DR = null;

                if (DT_Oracle.DefaultView.Count == 0)
                {
                    DR = DT_Oracle.NewRow();
                    DT_Oracle.Rows.Add(DR);
                }
                else
                {
                    DR = DT_Oracle.DefaultView[0].Row;
                }
                DR["PAKR_YEAR"] = SQL_DT.Rows[i]["R_Year"];
                DR["PAKR_ROUND"] = SQL_DT.Rows[i]["R_Round"];
                DR["PAKR_PSNL_NO"] = SQL_DT.Rows[i]["PSNL_NO"];
                if (!GL.IsEqualNull(SQL_DT.Rows[i]["TotalMark"]))
                {
                    DR["PAKR_KPI_FULL_MARKS"] = SQL_DT.Rows[i]["TotalMark"];
                }
                if (!GL.IsEqualNull(SQL_DT.Rows[i]["Result"]))
                {
                    DR["PAKR_KPI_SCORE"] = SQL_DT.Rows[i]["Result"];
                }
                if (!GL.IsEqualNull(SQL_DT.Rows[i]["Weight_KPI"]))
                {
                    DR["PAKR_KPI_PERCENT_WEIGHT"] = SQL_DT.Rows[i]["Weight_KPI"];
                }
                if (!GL.IsEqualNull(SQL_DT.Rows[i]["Update_By"]))
                {
                    DR["UPD_BY"] = SQL_DT.Rows[i]["Update_By"];
                }
                if (!GL.IsEqualNull(SQL_DT.Rows[i]["Update_Time"]))
                {
                    DR["UPD_DATE"] = SQL_DT.Rows[i]["Update_Time"];
                }


                //=============================================
                //=============================================

                // 2. UPDATE รายการที่ส่งข้อมูลไปยัง Oracle
                DT_Export_Status_Oracle.DefaultView.RowFilter = " PSNL_NO='" + SQL_DT.Rows[i]["PSNL_NO"].ToString().Replace("'", "''") + "'";
                DataRow DR_Export_Status_Oracle = null;
                if (DT_Export_Status_Oracle.DefaultView.Count == 0)
                {
                    DR_Export_Status_Oracle = DT_Export_Status_Oracle.NewRow();
                    DR_Export_Status_Oracle["R_Year"] = Y.ToString();
                    DR_Export_Status_Oracle["R_Round"] = R.ToString();

                    DR_Export_Status_Oracle["PSNL_NO"] = SQL_DT.Rows[i]["PSNL_NO"].ToString();
                    DR_Export_Status_Oracle["Create_Time"] = DateTime.Now;
                    //DR["Create_By"] = User_By;
                }
                else
                {
                    DR_Export_Status_Oracle = DT_Export_Status_Oracle.DefaultView[0].Row;

                    DR_Export_Status_Oracle["Create_Time"] = DateTime.Now;
                    //DR["Create_By"] = User_By;

                }
                if (DT_Export_Status_Oracle.DefaultView.Count == 0)
                    DT_Export_Status_Oracle.Rows.Add(DR_Export_Status_Oracle);
                //=============================================
                //=============================================



            }
            System.Data.OracleClient.OracleCommandBuilder cmd = new System.Data.OracleClient.OracleCommandBuilder(DA);
            DA.Update(DT_Oracle);

            SqlCommandBuilder cmd_Export_Status_Oracle = new SqlCommandBuilder(DA_Export_Status_Oracle);
            DA_Export_Status_Oracle.Update(DT_Export_Status_Oracle);

        }

        public void Export_Assessment_COMP_Result_To_Oracle(DataTable SQL_DT)
        {
            if ((SQL_DT == null) || SQL_DT.Rows.Count == 0)
                return;

            int Y = GL.CINT(SQL_DT.Rows[0]["R_Year"]);
            int R = GL.CINT(SQL_DT.Rows[0]["R_Round"]);
            //pa_kpi_result
            string SQL = "SELECT * FROM pa_comp_result WHERE PACR_YEAR=" + Y.ToString() + " AND PACR_ROUND=" + R; 
            System.Data.OracleClient.OracleDataAdapter DA = new System.Data.OracleClient.OracleDataAdapter(SQL, OracleConnectionString());
            DataTable DT_Oracle = new DataTable();
            DA.Fill(DT_Oracle);

            //PACR_YEAR
            //PACR_ROUND
            //PACR_PSNL_NO
            //PACR_COMP_FULL_MARKS
            //PAKR_COMP_SCORE
            //PAKR_COMP_PERCENT_WEIGHT
            //UPD_BY
            //UPD_DATE

            // 1. UPDATE รายการที่ส่งข้อมูลไปยัง Oracle
            string SQL_Export_Status_Oracle = " SELECT * FROM tb_Export_COMP_Status_Oracle WHERE R_Year='" + Y.ToString() + "' AND R_Round='" + R.ToString() + "' ";
            SqlDataAdapter DA_Export_Status_Oracle = new SqlDataAdapter(SQL_Export_Status_Oracle, ConnectionString());
            DataTable DT_Export_Status_Oracle = new DataTable();
            DA_Export_Status_Oracle.Fill(DT_Export_Status_Oracle);

            for (int i = 0; i <= SQL_DT.Rows.Count - 1; i++)
            {
                DT_Oracle.DefaultView.RowFilter = "PACR_PSNL_NO='" + SQL_DT.Rows[i]["PSNL_NO"].ToString().Replace("'", "''") + "'";
                DataRow DR = null;

                if (DT_Oracle.DefaultView.Count == 0)
                {
                    DR = DT_Oracle.NewRow();
                    DT_Oracle.Rows.Add(DR);
                }
                else
                {
                    DR = DT_Oracle.DefaultView[0].Row;
                }
                DR["PACR_YEAR"] = SQL_DT.Rows[i]["R_Year"];
                DR["PACR_ROUND"] = SQL_DT.Rows[i]["R_Round"];
                DR["PACR_PSNL_NO"] = SQL_DT.Rows[i]["PSNL_NO"];
                if (!GL.IsEqualNull(SQL_DT.Rows[i]["TotalMark"]))
                {
                    DR["PACR_COMP_FULL_MARKS"] = SQL_DT.Rows[i]["TotalMark"];
                }
                if (!GL.IsEqualNull(SQL_DT.Rows[i]["Result"]))
                {
                    DR["PACR_COMP_SCORE"] = SQL_DT.Rows[i]["Result"];
                }
                if (!GL.IsEqualNull(SQL_DT.Rows[i]["Weight_COMP"]))
                {
                    DR["PACR_COMP_PERCENT_WEIGHT"] = SQL_DT.Rows[i]["Weight_COMP"];
                }
                if (!GL.IsEqualNull(SQL_DT.Rows[i]["Update_By"]))
                {
                    DR["UPD_BY"] = SQL_DT.Rows[i]["Update_By"];
                }
                if (!GL.IsEqualNull(SQL_DT.Rows[i]["Update_Time"]))
                {
                    DR["UPD_DATE"] = SQL_DT.Rows[i]["Update_Time"];
                }



                //=============================================
                //=============================================

                // 2. UPDATE รายการที่ส่งข้อมูลไปยัง Oracle
                DT_Export_Status_Oracle.DefaultView.RowFilter = " PSNL_NO='" + SQL_DT.Rows[i]["PSNL_NO"].ToString().Replace("'", "''") + "'";
                DataRow DR_Export_Status_Oracle = null;
                if (DT_Export_Status_Oracle.DefaultView.Count == 0)
                {
                    DR_Export_Status_Oracle = DT_Export_Status_Oracle.NewRow();
                    DR_Export_Status_Oracle["R_Year"] = Y.ToString();
                    DR_Export_Status_Oracle["R_Round"] = R.ToString();

                    DR_Export_Status_Oracle["PSNL_NO"] = SQL_DT.Rows[i]["PSNL_NO"].ToString();
                    DR_Export_Status_Oracle["Create_Time"] = DateTime.Now;
                    //DR["Create_By"] = User_By;
                }
                else
                {
                    DR_Export_Status_Oracle = DT_Export_Status_Oracle.DefaultView[0].Row;

                    DR_Export_Status_Oracle["Create_Time"] = DateTime.Now;
                    //DR["Create_By"] = User_By;

                }
                if (DT_Export_Status_Oracle.DefaultView.Count == 0)
                    DT_Export_Status_Oracle.Rows.Add(DR_Export_Status_Oracle);
                //=============================================
                //=============================================



            }
            System.Data.OracleClient.OracleCommandBuilder cmd = new System.Data.OracleClient.OracleCommandBuilder(DA);
            DA.Update(DT_Oracle);

            SqlCommandBuilder cmd_Export_Status_Oracle = new SqlCommandBuilder(DA_Export_Status_Oracle);
            DA_Export_Status_Oracle.Update(DT_Export_Status_Oracle);

        }

    }
}

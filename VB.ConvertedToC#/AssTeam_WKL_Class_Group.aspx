﻿<%@ Page  Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPage.Master" CodeBehind="AssTeam_WKL_Class_Group.aspx.cs" Inherits="VB.AssTeam_WKL_Class_Group" %>

<%@ Register src="WUC_DateReporter.ascx" tagname="WUC_DateReporter" tagprefix="uc1" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<asp:ToolkitScriptManager ID="toolkit1" runat="server"></asp:ToolkitScriptManager>
<asp:UpdatePanel ID="UpdatePanel1" runat="server">
<ContentTemplate>
<div class="container-fluid">
				<!-- BEGIN PAGE HEADER-->
				<div class="row-fluid" id="fixedPageHeader">
					<div class="span12"> 
						<!-- BEGIN PAGE TITLE & BREADCRUMB-->
						<h3 class="page-title">การกำหนดกลุ่มระดับของหน่วยงาน <font color="blue">(ScreenID : S-MGR-06)</font></h3>
						
									
						<ul class="breadcrumb">
                            <li>
                            	<i class="icon-dashboard"></i><a href="javascript:;">การบริหารอัตรากำลัง (Workload)</a><i class="icon-angle-right"></i>
                            </li>
                        	<li>
                        	    <i class="icon-list-ol"></i> <a href="javascript:;">การกำหนดกลุ่มระดับของหน่วยงาน</a>
                        	</li>
                        	
                        	
                            <uc1:WUC_DateReporter ID="WUC_DateReporter1" runat="server" />
                        </ul>
						<!-- END PAGE TITLE & BREADCRUMB-->
									        
				     </div>

				
			    </div>
		        <!-- END PAGE HEADER-->
				
			
  
                <div class="portlet-body form">
				
				    	
						
					<!-- BEGIN FORM-->
					<div class="form-horizontal form-view">
						           
						<h3 class="form-section"><i class="icon-list-ol"></i> สำหรับ 
						<asp:DropDownList ID="ddlOrganize" OnSelectedIndexChanged="ddl_SelectedIndexChanged" runat="server" AutoPostBack="true" CssClass="m-wrap" style="font-size:20px; width:auto !important; margin-top:-5px; padding:0px;">
						</asp:DropDownList></h3>
						</h3>
						<div class="row-fluid">
							<div class="span12 ">
								<div class="control-group">
									<label class="control-label">&nbsp;</label>
									
									    <div class="controls">
									    <div class="span3">
										    <div class="portlet-body no-more-tables">
		                                        <table class="table table-full-width dataTable no-more-tables table-hover">
			                                        <thead>
				                                        <tr>
					                                        <th style="text-align:center;"><i class="icon-list-ol"></i> กลุ่มระดับ</th>
					                                        <th style="text-align:center;"><i class="icon-list-ol"></i> ถึง</th>
					                                        <th style="text-align:center;"> แทรก</th>
					                                        <th style="text-align:center;"> ลบ</th>											                                        
				                                        </tr>
			                                        </thead>
			                                        <tbody>
				                                        <asp:Repeater ID="rptClass" OnItemCommand="rptClass_ItemCommand" OnItemDataBound="rptClass_ItemDataBound" runat="server">
			                                                <ItemTemplate>
			                                                    <tr>
					                                                <td data-title="กลุ่มระดับ"><asp:TextBox ID="txtStart" runat="server" CssClass="m-wrap small" style="text-align:center;"></asp:TextBox></td>
					                                                <td data-title="ถึง"><asp:TextBox ID="txtEnd" runat="server" CssClass="m-wrap small" style="text-align:center;"></asp:TextBox></td>
					                                                <td data-title="แทรก"><asp:Button ID="btnInsert" runat="server" Text="แทรก" CssClass="btn purple mini" CommandName="Insert" /></td>
					                                                <td data-title="ลบ"><asp:Button ID="btnDelete" runat="server" Text="ลบ" CssClass="btn red mini" CommandName="Delete" /></td>
				                                                </tr>
			                                                </ItemTemplate>
			                                               
			                                            </asp:Repeater>											
			                                        </tbody>
		                                        </table>
	                                        </div>
									    </div>
									</div>
								</div>
							</div>
						</div>
						
						<asp:Panel ID="pnlAction" runat="server" CssClass="form-actions">
						    <asp:Button ID="btnAdd" OnClick="btnAdd_Click" CssClass="btn purple" runat="server" Text="เพิ่มกลุ่มระดับ" />
						    <asp:Button ID="btnSave" OnClick="btnSave_Click" CssClass="btn blue" runat="server" Text="ยืนยันบันทึก" />
						    <asp:Button ID="btnCancel" OnClick="ddl_SelectedIndexChanged" CssClass="btn" runat="server" Text="ยกเลิก" />
						    <asp:ConfirmButtonExtender ID="cfmbtnSave" ConfirmText="ยืนยันบันทึก" TargetControlID="btnSave" runat="server" ></asp:ConfirmButtonExtender>
						
						</asp:Panel>
					</div>
					<!-- END FORM-->  
				</div>
         
                             
              			
</div>
</ContentTemplate>           
</asp:UpdatePanel>

</asp:Content>


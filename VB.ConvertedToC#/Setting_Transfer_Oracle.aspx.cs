using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Xml.Linq;
using System.Diagnostics;
using System.Collections.Specialized;
using System.Configuration;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Caching;
using System.Web.SessionState;
using System.Web.Security;
using System.Web.Profile;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
namespace VB
{

	public partial class Setting_Transfer_Oracle : System.Web.UI.Page
	{


		HRBL BL = new HRBL();
		GenericLib GL = new GenericLib();
		Converter C = new Converter();

		public int R_Year {
			get {
				try {
					return GL.CINT( GL.SplitString(ddlYear.Items[ddlYear.SelectedIndex].Value, "-")[0]);
				} catch (Exception ex) {
					return 0;
				}
			}
		}


		protected void Page_Load(object sender, System.EventArgs e)
		{
			if ((Session["USER_PSNL_NO"] == null)) {
				ScriptManager.RegisterStartupScript(this.Page, typeof(string), "Alert", "showAlert('กรุณาลงชื่อเข้าใช้ระบบ');", true);
				Response.Redirect("Login.aspx");
				return;
			}

			if (!IsPostBack) {
				//----------------- ทำสองที่ Page Load กับ Update Status ---------------
				BL.BindDDlYear(ddlYear);
				ddlYear.Items.RemoveAt(0);
				BL.BindDDlSector(ddlSector);
				BindPersonalList();
				//Else
				//    ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "InitApp", "App.initUniform();", True)
			}

			SetCheckboxStyle();
		}

		private void SetCheckboxStyle()
		{
			if (chkKPIYes.Checked) {
				chkKPIYes.CssClass = "checked";
			} else {
				chkKPIYes.CssClass = "";
			}
			if (chkKPINo.Checked) {
				chkKPINo.CssClass = "checked";
			} else {
				chkKPINo.CssClass = "";
			}
			if (chkCOMPYes.Checked) {
				chkCOMPYes.CssClass = "checked";
			} else {
				chkCOMPYes.CssClass = "";
			}
			if (chkCOMPNo.Checked) {
				chkCOMPNo.CssClass = "checked";
			} else {
				chkCOMPNo.CssClass = "";
			}

			for (int i = 1; i <= 13; i++) {
                CheckBox chk = (CheckBox)pnlList.FindControl("chkClass" + i);
				if (chk.Checked) {
					chk.CssClass = "checked";
				} else {
					chk.CssClass = "";
				}
			}
		}

        protected void btnSearch_Click(object sender, System.EventArgs e)
		{
			BindPersonalList();
		}
        private void SetHeaderColumn()
        {
            string SQL = "SELECT * FROM tb_HR_Round WHERE R_Year=" + R_Year + "";
            SqlDataAdapter DA = new SqlDataAdapter(SQL, BL.ConnectionString());
            DataTable DT = new DataTable();
            DA.Fill(DT);

            if (DT.Rows.Count > 0)
            {
                for (int i = 0; i <= DT.Rows.Count - 1; i++)
                {
                    switch (GL.CINT(DT.Rows[i]["R_Round"]))
                    {
                        case 1:
                            lblHeaderKPI_1.Text = "KPI " + GL.CINT(DT.Rows[i]["Weight_KPI"]) + "%";
                            lblHeaderCOMP_1.Text = "COMP " + GL.CINT(DT.Rows[i]["Weight_COMP"]) + "%";
                            break;
                        case 2:
                            lblHeaderKPI_2.Text = "KPI " + GL.CINT(DT.Rows[i]["Weight_KPI"]) + "%";
                            lblHeaderCOMP_2.Text = "COMP " + GL.CINT(DT.Rows[i]["Weight_COMP"]) + "%";
                            break;
                    }

                }

            }
        }

		private void BindPersonalList()
		{
            SetHeaderColumn();
            string SQL = " SELECT vw_Export_Result_Oracle.* ";
            SQL += " ,CASE WHEN tb_Export_Status_Oracle.PSNL_NO IS NOT NULL THEN 1 ELSE 0 END Status_Oracle ";
            SQL += " FROM vw_Export_Result_Oracle ";
            SQL += " LEFT JOIN tb_Export_Status_Oracle ON vw_Export_Result_Oracle.R_Year = tb_Export_Status_Oracle.R_Year ";
            SQL += " AND vw_Export_Result_Oracle.PSNL_NO = tb_Export_Status_Oracle.PSNL_NO ";



            //SQL += " WHERE R_Year=" + R_Year + "\n";

            string Filter = "R_Year=" + R_Year + " ";
			string Title = "ประจำปี " + ddlYear.Items[ddlYear.SelectedIndex].Text + " ";

			//If ddlSector.SelectedIndex > 0 Then
			//    SQL &= " AND SECTOR_CODE = '" & ddlSector.SelectedValue & "' " & vbLf

			//End If

            if (ddlSector.SelectedIndex > 0)
            {

                Filter += " AND ( SECTOR_CODE='" + ddlSector.Items[ddlSector.SelectedIndex].Value + "' " + "\n";
                if (ddlSector.Items[ddlSector.SelectedIndex].Value == "3200")
                {
                    Filter += " AND  DEPT_CODE NOT IN ('32000300'))    " + "\n";
                    //-----------ฝ่ายโรงงานผลิตยาสูบ 3
                }
                else if (ddlSector.Items[ddlSector.SelectedIndex].Value == "3203")
                {
                    Filter += " OR  DEPT_CODE IN ('32000300','32030000','32030100','32030200','32030300','32030500'))  " + "\n";
                    //-----------ฝ่ายโรงงานผลิตยาสูบ 4
                }
                else if (ddlSector.Items[ddlSector.SelectedIndex].Value == "3204")
                {
                    Filter += " OR  DEPT_CODE IN ('32040000','32040100','32040200','32040300','32040500','32040300'))  " + "\n";
                    //-----------ฝ่ายโรงงานผลิตยาสูบ 5
                }
                else if (ddlSector.Items[ddlSector.SelectedIndex].Value == "3205")
                {
                    Filter += " OR  DEPT_CODE IN ('32050000','32050100','32050200','32050300','32050500'))\t  " + "\n";
                }
                else
                {
                    Filter += ")   " + "\n";
                }


                Title += GL.SplitString(ddlSector.Items[ddlSector.SelectedIndex].Text, ":")[1].Trim() + " ";
            }

            if (!string.IsNullOrEmpty(txtDeptName.Text))
            {
                Filter += " AND (DEPT_Name LIKE '%" + txtDeptName.Text.Replace("'", "''") + "%' OR Sector_Name LIKE '%" + txtDeptName.Text.Replace("'", "''") + "%') " + "\n";
                Title += " หน่วยงาน '" + txtDeptName.Text + "' ";
            }
            if (!string.IsNullOrEmpty(txtName.Text))
            {
                Filter += " AND (PSNL_Fullname LIKE '%" + txtName.Text.Replace("'", "''") + "%' OR PSNL_NO LIKE '%" + txtName.Text.Replace("'", "''") + "%') " + "\n";
                Title += " พนักงาน '" + txtName.Text + "' ";
            }

            if (ddlStatus.SelectedIndex > 0)
            {
                Filter += " AND Status_Oracle='" + ddlStatus.SelectedValue + "' " + "\n";
            }


            if ((chkClass1.Checked & chkClass2.Checked & chkClass3.Checked & chkClass4.Checked & chkClass5.Checked & chkClass6.Checked & chkClass7.Checked & chkClass8.Checked & chkClass9.Checked & chkClass10.Checked & chkClass11.Checked & chkClass12.Checked & chkClass13.Checked) | (!chkClass1.Checked & !chkClass2.Checked & !chkClass3.Checked & !chkClass4.Checked & !chkClass5.Checked & !chkClass6.Checked & !chkClass7.Checked & !chkClass8.Checked & !chkClass9.Checked & !chkClass10.Checked & !chkClass11.Checked & !chkClass12.Checked & !chkClass13.Checked))
            {
                //----------- Do nothing--------
            }
            else
            {
                string _classFilter = "";
                string _classTitle = "";
                for (int i = 1; i <= 13; i++)
                {
                    CheckBox chk = (CheckBox)pnlList.FindControl("chkClass" + i);
                    if (chk.Checked)
                    {
                        _classFilter += "'" + i.ToString().PadLeft(2, GL.chr0) + "',";
                        _classTitle += i + ",";
                    }
                }
                Title += " ระดับ " + _classTitle.Substring(0, _classTitle.Length - 1);
                Filter += " AND PNPS_CLASS IN (" + _classFilter.Substring(0, _classFilter.Length - 1) + ") " + "\n";
            }

            int KPIStat = 0;
            if ((chkKPIYes.Checked & chkKPINo.Checked) || (!chkKPIYes.Checked & !chkKPINo.Checked))
            {
                KPIStat = -1;
            }
            else if (chkKPIYes.Checked)
            {
                Filter += " AND KPI_Status_1=1 AND KPI_Status_2=1 " + "\n";
                KPIStat = 1;
            }
            else if (chkKPINo.Checked)
            {
                Filter += " AND (KPI_Status_1=0 OR KPI_Status_2=0) " + "\n";
                KPIStat = 0;
            }

            int COMPStat = 0;
            if ((chkCOMPYes.Checked & chkCOMPNo.Checked) || (!chkCOMPYes.Checked & !chkCOMPNo.Checked))
            {
                COMPStat = -1;
            }
            else if (chkCOMPYes.Checked)
            {
                Filter += " AND COMP_Status_1=1 AND COMP_Status_2=1 " + "\n";
                COMPStat = 1;
            }
            else if (chkCOMPNo.Checked)
            {
                Filter += " AND (COMP_Status_1=0 OR COMP_Status_2=0) " + "\n";
                COMPStat = 0;
            }

            if (KPIStat == COMPStat)
            {
                switch (KPIStat)
                {
                    case -1:
                        break;
                    case 0:
                        Title += "ที่การประเมินตัวชี้วัดและสมรรรถนะยังไม่เสร็จ";
                        break;
                    case 1:
                        Title += "การประเมินเสร็จสมบูรณ์";
                        break;
                }
            }
            else if (KPIStat == -1)
            {
                switch (COMPStat)
                {
                    case 0:
                        Title += "ที่การประเมินสมรรถนะยังไม่เสร็จ";
                        break;
                    case 1:
                        Title += "ที่การประเมินสมรรถนะเสร็จสมบูรณ์";
                        break;
                }
            }
            else if (KPIStat == 0)
            {
                switch (COMPStat)
                {
                    case -1:
                        Title += "ที่การประเมินตัวชี้วัดยังไม่เสร็จ";
                        break;
                    case 1:
                        Title += "ที่การประเมินสมรรถนะเสร็จแล้วแต่ตัวชี้วัดยังไม่เสร็จ";
                        break;
                }
            }
            else if (KPIStat == 1)
            {
                switch (COMPStat)
                {
                    case -1:
                        Title += "ที่การประเมินตัวชี้วัดสมบูรณ์";
                        break;
                    case 0:
                        Title += "ที่การประเมินตัวชี้วัดเสร็จแล้วแต่สมรรถนะยังไม่เสร็จ";
                        break;
                }
            }


			SQL += "  ORDER BY SECTOR_CODE,DEPT_CODE,Year_Score DESC,PNPS_CLASS DESC,POS_No" + "\n";

			SqlDataAdapter DA = new SqlDataAdapter(SQL, BL.ConnectionString());
			DA.SelectCommand.CommandTimeout = 200;
			DataTable DT = new DataTable();
			DA.Fill(DT);

            DT.DefaultView.RowFilter = Filter;

            Session["Setting_Transfer_Oracle"] = DT.DefaultView.ToTable();
            //--------- STAT --------------

			//'--------- Title --------------
			//Session("RPT_A_Yearly_Result_Title") = Title
			//'--------- Sub Title ----------
			//Session("RPT_A_Yearly_Result_SubTitle") = "รายงาน ณ วันที่ " & Now.Day & " " & C.ToMonthNameTH(Now.Month) & " พ.ศ." & (Now.Year + 543)
			//Session("RPT_A_Yearly_Result_SubTitle") &= "  พบ   " & GL.StringFormatNumber(DT.Rows.Count, 0) & " รายการ "

			Pager.SesssionSourceName = "Setting_Transfer_Oracle";
			Pager.RenderLayout();

			//------------------- Set Button --------------------

			if (DT.Rows.Count > 0) {
                btnSend_Top.Text = "Click เพื่อ ส่งผลการประเมิน " + Title + " จำนวน " + GL.StringFormatNumber(DT.DefaultView.Count, 0) + " รายการ ไปยัง Oracle";
                btnSend_Bottom.Text = "Click เพื่อ ส่งผลการประเมิน " + Title + " จำนวน " + GL.StringFormatNumber(DT.DefaultView.Count, 0) + " รายการ ไปยัง Oracle";
				btnSend_Top.CssClass = "btn green btn-block";
				btnSend_Top.Enabled = true;
			} else {
				btnSend_Top.Text = "ผลการประเมิน " + Title + " (ไม่พบรายการดังกล่าว)";
				btnSend_Top.CssClass = "btn btn-block";
				btnSend_Top.Enabled = false;
				btnSend_Bottom.Visible = false;
			}



		}

		protected void Pager_PageChanging(PageNavigation Sender)
		{
			Pager.TheRepeater = rptASS;
		}

		//------------ For Grouping -----------
		string LastSector = "";
		string LastDept = "";

		protected void rptASS_ItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
		{
			switch (e.Item.ItemType) {
				case ListItemType.AlternatingItem:
				case ListItemType.Item:
                    Label lblSector = (Label)e.Item.FindControl("lblSector");
                    Label lblDept = (Label)e.Item.FindControl("lblDept");
                    Label lblPSNNo = (Label)e.Item.FindControl("lblPSNNo");
                    Label lblPSNName = (Label)e.Item.FindControl("lblPSNName");
                    Label lblPSNPos = (Label)e.Item.FindControl("lblPSNPos");
                    Label lblPSNClass = (Label)e.Item.FindControl("lblPSNClass");

                    HtmlAnchor img_KPI_Status_1 = (HtmlAnchor)e.Item.FindControl("img_KPI_Status_1");
                    HtmlAnchor lbl_KPI_Score_1 = (HtmlAnchor)e.Item.FindControl("lbl_KPI_Score_1");
                    HtmlAnchor img_KPI_Status_2 = (HtmlAnchor)e.Item.FindControl("img_KPI_Status_2");
                    HtmlAnchor lbl_KPI_Score_2 = (HtmlAnchor)e.Item.FindControl("lbl_KPI_Score_2");
                    HtmlAnchor lbl_Score_1 = (HtmlAnchor)e.Item.FindControl("lbl_Score_1");
                    HtmlAnchor img_COMP_Status_1 = (HtmlAnchor)e.Item.FindControl("img_COMP_Status_1");
                    HtmlAnchor lbl_COMP_Score_1 = (HtmlAnchor)e.Item.FindControl("lbl_COMP_Score_1");
                    HtmlAnchor img_COMP_Status_2 = (HtmlAnchor)e.Item.FindControl("img_COMP_Status_2");
                    HtmlAnchor lbl_COMP_Score_2 = (HtmlAnchor)e.Item.FindControl("lbl_COMP_Score_2");
                    HtmlAnchor lbl_Score_2 = (HtmlAnchor)e.Item.FindControl("lbl_Score_2");

                    HtmlAnchor lbl_Score = (HtmlAnchor)e.Item.FindControl("lbl_Score");
                    HtmlAnchor lbl_Leave_Day = (HtmlAnchor)e.Item.FindControl("lbl_Leave_Day");
                    HtmlAnchor lbl_Leave_Score = (HtmlAnchor)e.Item.FindControl("lbl_Leave_Score");
                    HtmlAnchor lbl_Year_Result = (HtmlAnchor)e.Item.FindControl("lbl_Year_Result");
                    HtmlAnchor lbl_Wage_Rank = (HtmlAnchor)e.Item.FindControl("lbl_Wage_Rank");

                    HtmlTableRow tdSector = (HtmlTableRow)e.Item.FindControl("tdSector");
                    HtmlTableRow tdDept = (HtmlTableRow)e.Item.FindControl("tdDept");

                    Image imgStatusOracle = (Image)e.Item.FindControl("imgStatusOracle");


                    DataRowView drv = (DataRowView)e.Item.DataItem;

					//---------- แยกหน่วยงาน---------
					if (drv["SECTOR_NAME"].ToString() != LastSector) {
						LastSector = drv["SECTOR_NAME"].ToString();
						lblSector.Text = LastSector;
						LastDept = drv["DEPT_NAME"].ToString();
						lblDept.Text = LastDept;
					//---------- แยกสมรรถนะ---------
					} else if (LastDept != drv["DEPT_NAME"].ToString()) {
						tdSector.Visible = false;
						LastDept = drv["DEPT_NAME"].ToString();
						lblDept.Text = LastDept;
					} else {
						tdSector.Visible = false;
						tdDept.Visible = false;
					}

					lblPSNNo.Text = drv["PSNL_NO"].ToString();
					lblPSNName.Text = drv["PSNL_Fullname"].ToString();
					lblPSNPos.Text = drv["POS_Name"].ToString();
					if (!GL.IsEqualNull(drv["PNPS_CLASS"])) {
						lblPSNClass.Text = GL.CINT(drv["PNPS_CLASS"]).ToString();
					}

                    if (GL.CINT(drv["KPI_Status_1"]) == 1)
                    {
                        img_KPI_Status_1.InnerHtml = "<img src='images/check.png' />";
                        lbl_KPI_Score_1.InnerHtml = GL.StringFormatNumber(drv["KPI_Result_1"]);
                        lbl_KPI_Score_1.Style["color"] = "blue";
                    }
                    else
                    {
                        if (GL.CDBL(drv["KPI_Result_1"]) != 0)
                        {
                            lbl_KPI_Score_1.InnerHtml = GL.StringFormatNumber(drv["KPI_Result_1"]);
                        }
                        else {
                            lbl_KPI_Score_1.InnerHtml = "-";
                        }
                            img_KPI_Status_1.InnerHtml = "<img src='images/none.png' />";
                            lbl_KPI_Score_1.Style["color"] = "red";
                    }

                    if (GL.CINT(drv["KPI_Status_2"]) == 1)
                    {
                        img_KPI_Status_2.InnerHtml = "<img src='images/check.png' />";
                        lbl_KPI_Score_2.InnerHtml = GL.StringFormatNumber(drv["KPI_Result_2"]);
                        lbl_KPI_Score_2.Style["color"] = "blue";
                    }
                    else
                    {
                        if (GL.CDBL(drv["KPI_Result_2"]) != 0)
                        {
                            lbl_KPI_Score_2.InnerHtml = GL.StringFormatNumber(drv["KPI_Result_2"]);
                        }
                        else
                        {
                            lbl_KPI_Score_2.InnerHtml = "-";
                        }
                        img_KPI_Status_2.InnerHtml = "<img src='images/none.png' />";
                        lbl_KPI_Score_2.Style["color"] = "red";
                    }

                    if (GL.CINT(drv["COMP_Status_1"]) == 1)
                    {
                        img_COMP_Status_1.InnerHtml = "<img src='images/check.png' />";
                        lbl_COMP_Score_1.InnerHtml = GL.StringFormatNumber(drv["COMP_Result_1"]);
                        lbl_COMP_Score_1.Style["color"] = "blue";
                    }
                    else
                    {
                        if (GL.CDBL(drv["COMP_Result_1"]) != 0)
                        {
                            lbl_COMP_Score_1.InnerHtml = GL.StringFormatNumber(drv["COMP_Result_1"]);
                        }
                        else
                        {
                            lbl_COMP_Score_1.InnerHtml = "-";
                        }
                        img_COMP_Status_1.InnerHtml = "<img src='images/none.png' />";
                        lbl_COMP_Score_1.Style["color"] = "red";
                    }

                    if (GL.CINT(drv["COMP_Status_2"]) == 1)
                    {
                        img_COMP_Status_2.InnerHtml = "<img src='images/check.png' />";
                        lbl_COMP_Score_2.InnerHtml = GL.StringFormatNumber(drv["COMP_Result_2"]);
                        lbl_COMP_Score_2.Style["color"] = "blue";
                    }
                    else
                    {
                        if (GL.CDBL(drv["COMP_Result_2"]) != 0)
                        {
                            lbl_COMP_Score_2.InnerHtml = GL.StringFormatNumber(drv["COMP_Result_2"]);
                        }
                        else
                        {
                            lbl_COMP_Score_2.InnerHtml = "-";
                        }
                        img_COMP_Status_2.InnerHtml = "<img src='images/none.png' />";
                        lbl_COMP_Score_2.Style["color"] = "red";
                    }

                    if (GL.CDBL(drv["Ass_Score_1"]) != 0 && (GL.CINT(drv["KPI_Status_1"]) == 1 && GL.CINT(drv["COMP_Status_1"]) == 1))
                    {
				        lbl_Score_1.InnerHtml = GL.StringFormatNumber(drv["Ass_Score_1"]);
                        lbl_Score_1.Style["color"] = "blue";
                    }else{
                        if (GL.CDBL(drv["Ass_Score_1"]) != 0)
                        {
                            lbl_Score_1.InnerHtml = GL.StringFormatNumber(drv["Ass_Score_1"]);
                        }
                        else
                        {
                            lbl_Score_1.InnerHtml = "-";
                        }
                        lbl_Score_1.Style["color"] = "red";
			        }

                    if (GL.CDBL(drv["Ass_Score_2"]) != 0 && (GL.CINT(drv["KPI_Status_2"]) == 1 && GL.CINT(drv["COMP_Status_2"]) == 1))
                    {
				        lbl_Score_2.InnerHtml = GL.StringFormatNumber(drv["Ass_Score_2"]);
                        lbl_Score_2.Style["color"] = "blue";
                    }else{
                        if (GL.CDBL(drv["Ass_Score_2"]) != 0)
                        {
                            lbl_Score_2.InnerHtml = GL.StringFormatNumber(drv["Ass_Score_2"]);
                        }
                        else
                        {
                            lbl_Score_2.InnerHtml = "-";
                        }
                        lbl_Score_2.Style["color"] = "red";
			        }

					if (GL.CDBL(drv["Leave_Day"]) != 0 ) {
                        lbl_Leave_Day.InnerHtml = GL.StringFormatNumber(drv["Leave_Day"].ToString(), 2);
					} else {
						lbl_Leave_Day.InnerHtml = "-";
					}

                    if (GL.CDBL(drv["Ass_Score"]) > 0)
                    {
                        lbl_Score.InnerHtml = GL.StringFormatNumber(drv["Ass_Score"].ToString());
					} else {
						lbl_Score.InnerHtml = "-";
					}

					if (GL.CDBL(drv["Leave_Score"]) != 0) {
                        lbl_Leave_Score.InnerHtml = GL.StringFormatNumber(drv["Leave_Score"].ToString(), 2);
					} else {
						lbl_Leave_Score.InnerHtml = "-";
					}

                    if (GL.CDBL(drv["Year_Score"] ) > 0)
                    {
                        lbl_Year_Result.InnerHtml = GL.StringFormatNumber(drv["Year_Score"].ToString(), 2);
					} else {
						lbl_Year_Result.InnerHtml = "-";
					}

                    // 20161101 MA เพิ่มสถานะส่งข้อมูลไปยัง Oracle
                    if (drv["Status_Oracle"].ToString() == "1")
                    {
                        imgStatusOracle.ImageUrl ="images/check.png";
                        imgStatusOracle.ToolTip = "โอนแล้ว";
                    }
                    else {
                        imgStatusOracle.ImageUrl = "images/none.png";
                        imgStatusOracle.ToolTip = "ยังโอนแล้ว";
                    }


					img_KPI_Status_1.HRef = "Print/RPT_K_PNSAss.aspx?Mode=PDF&R_Year=" + drv["R_Year"].ToString() + "&R_Round=1&PSNL_NO=" + drv["PSNL_NO"].ToString() + "&t=8" + "&Status=" + GL.CINT(drv["KPI_StatusAss_1"]);
					lbl_KPI_Score_1.HRef = "Print/RPT_K_PNSAss.aspx?Mode=PDF&R_Year=" + drv["R_Year"].ToString() + "&R_Round=1&PSNL_NO=" + drv["PSNL_NO"].ToString() + "&t=8" + "&Status=" + GL.CINT(drv["KPI_StatusAss_1"]);
					img_KPI_Status_2.HRef = "Print/RPT_K_PNSAss.aspx?Mode=PDF&R_Year=" + drv["R_Year"].ToString() + "&R_Round=2&PSNL_NO=" + drv["PSNL_NO"].ToString() + "&t=8" + "&Status=" + GL.CINT(drv["KPI_StatusAss_2"]);
					lbl_KPI_Score_2.HRef = "Print/RPT_K_PNSAss.aspx?Mode=PDF&R_Year=" + drv["R_Year"].ToString() + "&R_Round=2&PSNL_NO=" + drv["PSNL_NO"].ToString() + "&t=8" + "&Status=" + GL.CINT(drv["KPI_StatusAss_2"]);


					img_COMP_Status_1.HRef = "Print/RPT_C_PNSAss.aspx?Mode=PDF&R_Year=" + drv["R_Year"].ToString() + "&R_Round=1&PSNL_NO=" + drv["PSNL_NO"].ToString() + "&t=8";
					lbl_COMP_Score_1.HRef = "Print/RPT_C_PNSAss.aspx?Mode=PDF&R_Year=" + drv["R_Year"].ToString() + "&R_Round=1&PSNL_NO=" + drv["PSNL_NO"].ToString() + "&t=8";
					img_COMP_Status_2.HRef = "Print/RPT_C_PNSAss.aspx?Mode=PDF&R_Year=" + drv["R_Year"].ToString() + "&R_Round=2&PSNL_NO=" + drv["PSNL_NO"].ToString() + "&t=8";
					lbl_COMP_Score_2.HRef = "Print/RPT_C_PNSAss.aspx?Mode=PDF&R_Year=" + drv["R_Year"].ToString() + "&R_Round=2&PSNL_NO=" + drv["PSNL_NO"].ToString() + "&t=8";

					lbl_Score_1.HRef = "Print/RPT_A_Yearly_PSN.aspx?R_Year=" + drv["R_Year"] + "&PSNL_No=" + drv["PSNL_NO"] + "&Mode=PDF&t=555";
					lbl_Score_2.HRef = "Print/RPT_A_Yearly_PSN.aspx?R_Year=" + drv["R_Year"] + "&PSNL_No=" + drv["PSNL_NO"] + "&Mode=PDF&t=555";
					lbl_Score.HRef = "Print/RPT_A_Yearly_PSN.aspx?R_Year=" + drv["R_Year"] + "&PSNL_No=" + drv["PSNL_NO"] + "&Mode=PDF&t=555";
					lbl_Leave_Day.HRef = "Print/RPT_A_Yearly_PSN.aspx?R_Year=" + drv["R_Year"] + "&PSNL_No=" + drv["PSNL_NO"] + "&Mode=PDF&t=555";
					lbl_Leave_Score.HRef = "Print/RPT_A_Yearly_PSN.aspx?R_Year=" + drv["R_Year"] + "&PSNL_No=" + drv["PSNL_NO"] + "&Mode=PDF&t=555";
					lbl_Year_Result.HRef = "Print/RPT_A_Yearly_PSN.aspx?R_Year=" + drv["R_Year"] + "&PSNL_No=" + drv["PSNL_NO"] + "&Mode=PDF&t=555";
					lbl_Wage_Rank.HRef = "Print/RPT_A_Yearly_PSN.aspx?R_Year=" + drv["R_Year"] + "&PSNL_No=" + drv["PSNL_NO"] + "&Mode=PDF&t=555";

					switch (drv["Rank_No"].ToString()) {
						case "5":
							lbl_Wage_Rank.InnerHtml = "2.0 ขั้น";
							lbl_Wage_Rank.Style["color"] = "green";
							break;
						case "4":
							lbl_Wage_Rank.InnerHtml = "1.5 ขั้น";
							lbl_Wage_Rank.Style["color"] = "green";
							break;
						case "3":
							lbl_Wage_Rank.InnerHtml = "1.0 ขั้น";
							lbl_Wage_Rank.Style["color"] = "blue";
							break;
						case "2":
							lbl_Wage_Rank.InnerHtml = "0.5 ขั้น";
							lbl_Wage_Rank.Style["color"] = "black";
							break;
						default:
							lbl_Wage_Rank.InnerHtml = "ไม่ผ่านเกณฑ์";
							lbl_Wage_Rank.Style["color"] = "red";
							break;
					}

					break;
				case ListItemType.Footer:
					btnSend_Bottom.Visible = rptASS.Items.Count > 10;

					break;
			}


		}

        protected void btnSend_Click(object sender, System.EventArgs e)
        {
            DataTable DT = ((DataTable)Session["Setting_Transfer_Oracle"]).Copy();
            //----------------- Remove unused column ------------------
            try
            {
                String Update_By = Session["USER_PSNL_NO"].ToString();
                BL.Export_Assessment_Result_To_Oracle(DT);
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this.Page, typeof(string), "Alert", "showAlert('ไม่สำเร็จ<br><br>" + ex.Message.Replace("'", "\'").Replace("\n", "<br>") + "');", true);
                return;
            }
            BindPersonalList();
            ScriptManager.RegisterStartupScript(this.Page, typeof(string), "Alert", "showAlert('ส่งข้อมูลสำเร็จ');", true);

        }
		public Setting_Transfer_Oracle()
		{
			Load += Page_Load;
		}

	}
}

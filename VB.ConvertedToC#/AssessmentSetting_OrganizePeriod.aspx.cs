using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Xml.Linq;
using System.Diagnostics;
using System.Collections.Specialized;
using System.Configuration;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Caching;
using System.Web.SessionState;
using System.Web.Security;
using System.Web.Profile;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
namespace VB
{

	public partial class AssessmentSetting_OrganizePeriod : System.Web.UI.Page
	{


		HRBL BL = new HRBL();
		GenericLib GL = new GenericLib();

		textControlLib TC = new textControlLib();
		protected void Page_Load(object sender, System.EventArgs e)
		{
			if ((Session["USER_PSNL_NO"] == null)) {
				ScriptManager.RegisterStartupScript(this.Page, typeof(string), "Alert", "showAlert('กรุณาลงชื่อเข้าใช้ระบบ');", true);
				Response.Redirect("Login.aspx");
				return;
			}

			if (!IsPostBack) {
				pnlEdit.Visible = false;
				pnlList.Visible = true;
				BindSearchForm();
				BindList();
			}
		}

		private void BindSearchForm()
		{
			BL.BindDDlYearRound(ddlSearchRound);
			txtSearchDept.Text = "";
		}

		protected void btnSearch_Click(object sender, System.EventArgs e)
		{
			BindList();
		}

		private void BindList()
		{
			string SQL = "SELECT * \n";
			SQL += " FROM vw_HR_Custom_Period \n";

			string R_Year = GL.SplitString(ddlSearchRound.Items[ddlSearchRound.SelectedIndex].Value, "-")[0];
			string R_Round = GL.SplitString(ddlSearchRound.Items[ddlSearchRound.SelectedIndex].Value, "-")[1];

			string WHERE = "R_Year=" + R_Year + " AND R_Round=" + R_Round + " AND SECTOR_CODE<>'10' AND ";
			if (!string.IsNullOrEmpty(txtSearchDept.Text)) {
				WHERE += "( DEPT_CODE LIKE '%" + txtSearchDept.Text.Replace("'", "''") + "%' OR \n";
				WHERE += " SECTOR_NAME LIKE '%" + txtSearchDept.Text.Replace("'", "''") + "%' OR \n";
				WHERE += " DEPT_NAME LIKE '%" + txtSearchDept.Text.Replace("'", "''") + "%' ) AND ";
			}
			if (chkAssigned.Checked) {
				WHERE += " (\n";
				WHERE += " DEPT_K_CP_Start IS NOT NULL OR DEPT_K_CP_End IS NOT NULL OR\n";
				WHERE += " DEPT_K_AP_Start IS NOT NULL OR DEPT_K_AP_End IS NOT NULL OR\n";
				WHERE += " DEPT_K_MGR_Start IS NOT NULL OR DEPT_K_MGR_End IS NOT NULL OR\n";
				WHERE += " DEPT_C_CP_Start IS NOT NULL OR DEPT_C_CP_End IS NOT NULL OR\n";
				WHERE += " DEPT_C_AP_Start IS NOT NULL OR DEPT_C_AP_End IS NOT NULL OR\n";
				WHERE += " DEPT_C_MGR_Start IS NOT NULL OR DEPT_C_MGR_End IS NOT NULL\n";
				WHERE += " ) AND ";
			}

			SQL += "WHERE " + WHERE.Substring(0, WHERE.Length - 4) + "\n";
			SQL += "ORDER BY SECTOR_CODE,DEPT_CODE,DEPT_NAME";

			SqlDataAdapter DA = new SqlDataAdapter(SQL, BL.ConnectionString());
			DataTable DT = new DataTable();
			DA.Fill(DT);

			Session["AssessmentSetting_OrganizePeriod"] = DT;
			Pager.SesssionSourceName = "AssessmentSetting_OrganizePeriod";
			Pager.RenderLayout();

			if (DT.Rows.Count == 0) {
				lblCountList.Text = "ไม่พบรายการดังกล่าว";
			} else {
				lblCountList.Text = "พบ " + GL.StringFormatNumber(DT.Rows.Count, 0) + " รายการ";
			}

		}

		protected void Pager_PageChanging(PageNavigation Sender)
		{
			Pager.TheRepeater = rptPeriod;
		}

		string LastDept = "";
		protected void rptPeriod_ItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
		{
			if (e.Item.ItemType != ListItemType.Item & e.Item.ItemType != ListItemType.AlternatingItem)
				return;

			Label lbl_Sector_Name =(Label) e.Item.FindControl("lbl_Sector_Name");
			Label lbl_Dept_Name =(Label) e.Item.FindControl("lbl_Dept_Name");
			Label lbl_KPI_CP_Start =(Label) e.Item.FindControl("lbl_KPI_CP_Start");
			Label lbl_KPI_CP_End =(Label) e.Item.FindControl("lbl_KPI_CP_End");
			Label lbl_KPI_AP_Start =(Label) e.Item.FindControl("lbl_KPI_AP_Start");
			Label lbl_KPI_AP_End =(Label) e.Item.FindControl("lbl_KPI_AP_End");
			Label lbl_COMP_CP_Start =(Label) e.Item.FindControl("lbl_COMP_CP_Start");
			Label lbl_COMP_CP_End =(Label) e.Item.FindControl("lbl_COMP_CP_End");
			Label lbl_COMP_AP_Start =(Label) e.Item.FindControl("lbl_COMP_AP_Start");
			Label lbl_COMP_AP_End =(Label) e.Item.FindControl("lbl_COMP_AP_End");
			LinkButton lnkAdjust =(LinkButton) e.Item.FindControl("lnkAdjust");

            DataRowView drv = (DataRowView)e.Item.DataItem;
			if (LastDept != drv["SECTOR_NAME"].ToString()) {
				lbl_Sector_Name.Text = drv["SECTOR_NAME"].ToString();
				LastDept = drv["SECTOR_NAME"].ToString();
			}
			lbl_Dept_Name.Text = drv["DEPT_NAME"].ToString();

			bool isChange = false;
			//------------------------ KPI Create Period--------------------------
			
			if (!GL.IsEqualNull(drv["DEPT_K_CP_Start"])) {
				lbl_KPI_CP_Start.Text =  BL.DateTimeToThaiDate((DateTime)drv["DEPT_K_CP_Start"]).ToString();				
				isChange = true;
			} else {
                lbl_KPI_CP_Start.Text = BL.DateTimeToThaiDate((DateTime)drv["TTM_K_CP_Start"]).ToString();
			}

			if (!GL.IsEqualNull(drv["DEPT_K_CP_End"])) {
                lbl_KPI_CP_End.Text = BL.DateTimeToThaiDate((DateTime)drv["DEPT_K_CP_End"]).ToString();
				isChange = true;
			} else {
                lbl_KPI_CP_End.Text = BL.DateTimeToThaiDate((DateTime)drv["TTM_K_CP_End"]).ToString();
			}
			//------------------------ KPI Assessment Period--------------------------
           
            //-----Start----มีการปรับเวลา----
            if (!GL.IsEqualNull(drv["DEPT_K_AP_Start"]) && !GL.IsEqualNull(drv["DEPT_K_MGR_Start"]) && (DateTime)drv["DEPT_K_AP_Start"] < (DateTime)drv["DEPT_K_MGR_Start"]){
                lbl_KPI_AP_Start.Text = BL.DateTimeToThaiDate((DateTime)drv["DEPT_K_AP_Start"]).ToString();
                isChange = true;
            } else if (GL.IsEqualNull(drv["DEPT_K_AP_Start"]) && GL.IsEqualNull(drv["DEPT_K_MGR_Start"])) {  //------ช่วงเวลาตามการกำหนดรอบ 
                if((DateTime)drv["TTM_K_AP_Start"] < (DateTime)drv["TTM_K_MGR_Start"]){
                lbl_KPI_AP_Start.Text = BL.DateTimeToThaiDate((DateTime)drv["TTM_K_AP_Start"]).ToString();
                }
                else {
                lbl_KPI_AP_Start.Text = BL.DateTimeToThaiDate((DateTime)drv["TTM_K_MGR_Start"]).ToString();
                }               
            } else {
                lbl_KPI_AP_Start.Text = BL.DateTimeToThaiDate((DateTime)drv["DEPT_K_MGR_Start"]).ToString();
                isChange = true;
            }
            if (!GL.IsEqualNull(drv["DEPT_K_MGR_End"])) {
                lbl_KPI_AP_End.Text = BL.DateTimeToThaiDate((DateTime)drv["DEPT_K_MGR_End"]).ToString();
                isChange = true;
            } else {
                lbl_KPI_AP_End.Text = BL.DateTimeToThaiDate((DateTime)drv["TTM_K_MGR_End"]).ToString();
            }
            
			//------------------------ COMP Create Period--------------------------
			if (!GL.IsEqualNull(drv["DEPT_C_CP_Start"])) {
                lbl_COMP_CP_Start.Text = BL.DateTimeToThaiDate((DateTime)drv["DEPT_C_CP_Start"]).ToString();
				isChange = true;
			} else {
                lbl_COMP_CP_Start.Text = BL.DateTimeToThaiDate((DateTime)drv["TTM_C_CP_Start"]).ToString();
			}
			if (!GL.IsEqualNull(drv["DEPT_C_CP_End"])) {
                lbl_COMP_CP_End.Text = BL.DateTimeToThaiDate((DateTime)drv["DEPT_C_CP_End"]).ToString();
				isChange = true;
			} else {
                lbl_COMP_CP_End.Text = BL.DateTimeToThaiDate((DateTime)drv["TTM_C_CP_End"]).ToString();
			}
			//------------------------ COMP Assessment Period--------------------------
            //if (!GL.IsEqualNull(drv["DEPT_C_MGR_Start"])) {
            //    lbl_COMP_AP_Start.Text = BL.DateTimeToThaiDate((DateTime)drv["DEPT_C_MGR_Start"]).ToString();
            //    isChange = true;
            //} else {
            //    lbl_COMP_AP_Start.Text = BL.DateTimeToThaiDate((DateTime)drv["TTM_C_MGR_Start"]).ToString();
            //}

            //-----Start----มีการปรับเวลา----
            if (!GL.IsEqualNull(drv["DEPT_C_AP_Start"]) && !GL.IsEqualNull(drv["DEPT_C_MGR_Start"]) && (DateTime)drv["DEPT_C_AP_Start"] < (DateTime)drv["DEPT_C_MGR_Start"]) {
                lbl_COMP_AP_Start.Text = BL.DateTimeToThaiDate((DateTime)drv["DEPT_C_AP_Start"]).ToString();
                isChange = true;
            } else if (GL.IsEqualNull(drv["DEPT_C_AP_Start"]) && GL.IsEqualNull(drv["DEPT_C_MGR_Start"])) {  //------ช่วงเวลาตามการกำหนดรอบ 
                if ((DateTime)drv["TTM_C_AP_Start"] < (DateTime)drv["TTM_C_MGR_Start"])
                {
                    lbl_COMP_AP_Start.Text = BL.DateTimeToThaiDate((DateTime)drv["TTM_C_AP_Start"]).ToString();
                } else {
                    lbl_COMP_AP_Start.Text = BL.DateTimeToThaiDate((DateTime)drv["TTM_C_MGR_Start"]).ToString();
                }
            } else {
                lbl_COMP_AP_Start.Text = BL.DateTimeToThaiDate((DateTime)drv["DEPT_C_MGR_Start"]).ToString();
                isChange = true;
            }

			if (!GL.IsEqualNull(drv["DEPT_C_MGR_End"])) {
                lbl_COMP_AP_End.Text = BL.DateTimeToThaiDate((DateTime)drv["DEPT_C_MGR_End"]).ToString();
				isChange = true;
			} else {
                lbl_COMP_AP_End.Text = BL.DateTimeToThaiDate((DateTime)drv["TTM_C_MGR_End"]).ToString();
			}

			if (isChange) {
				lbl_Sector_Name.ForeColor = System.Drawing.Color.Red;
				lbl_Dept_Name.ForeColor = System.Drawing.Color.Red;
				lbl_KPI_CP_Start.ForeColor = System.Drawing.Color.Red;
				lbl_KPI_CP_End.ForeColor = System.Drawing.Color.Red;
				lbl_KPI_AP_Start.ForeColor = System.Drawing.Color.Red;
				lbl_KPI_AP_End.ForeColor = System.Drawing.Color.Red;
				lbl_COMP_CP_Start.ForeColor = System.Drawing.Color.Red;
				lbl_COMP_CP_End.ForeColor = System.Drawing.Color.Red;
				lbl_COMP_AP_Start.ForeColor = System.Drawing.Color.Red;
				lbl_COMP_AP_End.ForeColor = System.Drawing.Color.Red;
			}

			lnkAdjust.Attributes["R_Year"] =GL.CINT ( drv["R_Year"]).ToString ();
			lnkAdjust.Attributes["R_Round"]  =GL.CINT (drv["R_Round"]).ToString ();
			lnkAdjust.Attributes["DEPT_CODE"] = drv["DEPT_CODE"].ToString ();
			lnkAdjust.Attributes["MINOR_CODE"] = drv["MINOR_CODE"].ToString ();
			lnkAdjust.CommandName = "Edit";
		}

		protected void rptPeriod_ItemCommand(object source, System.Web.UI.WebControls.RepeaterCommandEventArgs e)
		{
			switch (e.CommandName) {
				case "Edit":
					LinkButton lnkAdjust =(LinkButton) e.Item.FindControl("lnkAdjust");
					ClearEditForm();

					string R_Year = lnkAdjust.Attributes["R_Year"];
					string R_Round = lnkAdjust.Attributes["R_Round"];
					string DEPT_CODE = lnkAdjust.Attributes["DEPT_CODE"];
					string MINOR_CODE = lnkAdjust.Attributes["MINOR_CODE"];

					string SQL = "SELECT * FROM vw_HR_Custom_Period ";
					SQL += "WHERE R_Year=" + R_Year + " AND R_Round=" + R_Round + " AND DEPT_CODE='" + DEPT_CODE + "' AND MINOR_CODE='" + MINOR_CODE + "'";
					SqlDataAdapter DA = new SqlDataAdapter(SQL, BL.ConnectionString());
					DataTable DT = new DataTable();
					DA.Fill(DT);
					if (DT.Rows.Count == 0) {
						ScriptManager.RegisterStartupScript(this.Page, typeof(string), "alert", "showAlert('ไม่พบรายการดังกล่าว');", true);
						BindList();
						return;
					}

					if (!GL.IsEqualNull(DT.Rows[0]["DEPT_NAME"])) {
						lblEditDept.Text = DT.Rows[0]["DEPT_NAME"].ToString ();
					} else if (!GL.IsEqualNull(DT.Rows[0]["SECTOR_NAME"])) {
						lblEditDept.Text = DT.Rows[0]["SECTOR_NAME"].ToString ();
					}
					lblEditRound.Text = " ปี " + R_Year + " รอบ " + R_Round;

					lblEditRound.Attributes["R_Year"] = R_Year;
					lblEditRound.Attributes["R_Round"] = R_Round;
					lblEditRound.Attributes["DEPT_CODE"] = DEPT_CODE;
					lblEditRound.Attributes["MINOR_CODE"] = MINOR_CODE;

					//----------------- KPI ------------------
					if (!GL.IsEqualNull(DT.Rows[0]["DEPT_K_CP_Start"])) {
						dtp_KPI_CP_Start.Value = (DateTime)DT.Rows[0]["DEPT_K_CP_Start"];
					} else {
                        dtp_KPI_CP_Start.Value = (DateTime)DT.Rows[0]["TTM_K_CP_Start"];
					}
					if (!GL.IsEqualNull(DT.Rows[0]["DEPT_K_CP_End"])) {
                        dtp_KPI_CP_End.Value = (DateTime)DT.Rows[0]["DEPT_K_CP_End"];
					} else {
                        dtp_KPI_CP_End.Value = (DateTime)DT.Rows[0]["TTM_K_CP_End"];
					}
					if (!GL.IsEqualNull(DT.Rows[0]["DEPT_K_AP_Start"])) {
                        dtp_KPI_AP_Start.Value = (DateTime)DT.Rows[0]["DEPT_K_AP_Start"];
					} else {
                        dtp_KPI_AP_Start.Value = (DateTime)DT.Rows[0]["TTM_K_AP_Start"];
					}
					if (!GL.IsEqualNull(DT.Rows[0]["DEPT_K_AP_End"])) {
                        dtp_KPI_AP_End.Value = (DateTime)DT.Rows[0]["DEPT_K_AP_End"];
					} else {
                        dtp_KPI_AP_End.Value = (DateTime)DT.Rows[0]["TTM_K_AP_End"];
					}
					if (!GL.IsEqualNull(DT.Rows[0]["DEPT_K_MGR_Start"])) {
                        dtp_KPI_MGR_Start.Value = (DateTime)DT.Rows[0]["DEPT_K_MGR_Start"];
					} else {
                        dtp_KPI_MGR_Start.Value = (DateTime)DT.Rows[0]["TTM_K_MGR_Start"];
					}
					if (!GL.IsEqualNull(DT.Rows[0]["DEPT_K_MGR_End"])) {
                        dtp_KPI_MGR_End.Value = (DateTime)DT.Rows[0]["DEPT_K_MGR_End"];
					} else {
                        dtp_KPI_MGR_End.Value = (DateTime)DT.Rows[0]["TTM_K_MGR_End"];
					}
					//----------------- COMP ------------------
					if (!GL.IsEqualNull(DT.Rows[0]["DEPT_C_CP_Start"])) {
                        dtp_COMP_CP_Start.Value = (DateTime)DT.Rows[0]["DEPT_C_CP_Start"];
					} else {
                        dtp_COMP_CP_Start.Value = (DateTime)DT.Rows[0]["TTM_C_CP_Start"];
					}
					if (!GL.IsEqualNull(DT.Rows[0]["DEPT_C_CP_End"])) {
                        dtp_COMP_CP_End.Value = (DateTime)DT.Rows[0]["DEPT_C_CP_End"];
					} else {
                        dtp_COMP_CP_End.Value = (DateTime)DT.Rows[0]["TTM_C_CP_End"];
					}
					if (!GL.IsEqualNull(DT.Rows[0]["DEPT_C_AP_Start"])) {
                        dtp_COMP_AP_Start.Value = (DateTime)DT.Rows[0]["DEPT_C_AP_Start"];
					} else {
                        dtp_COMP_AP_Start.Value = (DateTime)DT.Rows[0]["TTM_C_AP_Start"];
					}
					if (!GL.IsEqualNull(DT.Rows[0]["DEPT_C_AP_End"])) {
                        dtp_COMP_AP_End.Value = (DateTime)DT.Rows[0]["DEPT_C_AP_End"];
					} else {
                        dtp_COMP_AP_End.Value = (DateTime)DT.Rows[0]["TTM_C_AP_End"];
					}
					if (!GL.IsEqualNull(DT.Rows[0]["DEPT_C_MGR_Start"])) {
                        dtp_COMP_MGR_Start.Value = (DateTime)DT.Rows[0]["DEPT_C_MGR_Start"];
					} else {
                        dtp_COMP_MGR_Start.Value = (DateTime)DT.Rows[0]["TTM_C_MGR_Start"];
					}
					if (!GL.IsEqualNull(DT.Rows[0]["DEPT_C_MGR_End"])) {
                        dtp_COMP_MGR_End.Value = (DateTime)DT.Rows[0]["DEPT_C_MGR_End"];
					} else {
                        dtp_COMP_MGR_End.Value = (DateTime)DT.Rows[0]["TTM_C_MGR_End"];
					}

					//--------------btnDefault Visibility---------------
					SQL = "";
					SQL += " SELECT R_Year FROM tb_HR_KPI_Create_Period WHERE R_Year=" + R_Year + " AND R_Round=" + R_Round + " AND PNDP_DEPT_CODE1='" + DEPT_CODE + "' AND PNDP_MINOR_CODE2='" + MINOR_CODE + "'\n";
					SQL += " UNION ALL\n";
					SQL += " SELECT R_Year FROM tb_HR_KPI_Assessment_Period WHERE R_Year=" + R_Year + " AND R_Round=" + R_Round + " AND PNDP_DEPT_CODE1='" + DEPT_CODE + "' AND PNDP_MINOR_CODE2='" + MINOR_CODE + "'\n";
					SQL += " UNION ALL\n";
					SQL += " SELECT R_Year FROM tb_HR_COMP_Create_Period WHERE R_Year=" + R_Year + " AND R_Round=" + R_Round + " AND PNDP_DEPT_CODE1='" + DEPT_CODE + "' AND PNDP_MINOR_CODE2='" + MINOR_CODE + "'\n";
					SQL += " UNION ALL\n";
					SQL += " SELECT R_Year FROM tb_HR_COMP_Assessment_Period WHERE R_Year=" + R_Year + " AND R_Round=" + R_Round + " AND PNDP_DEPT_CODE1='" + DEPT_CODE + "' AND PNDP_MINOR_CODE2='" + MINOR_CODE + "'\n";
					DA = new SqlDataAdapter(SQL, BL.ConnectionString());
					DT = new DataTable();
					DA.Fill(DT);
					btnDefault.Visible = DT.Rows.Count > 0;

					pnlList.Visible = false;
					pnlEdit.Visible = true;

					break;
			}
		}


		private void ClearEditForm()
		{
			lblEditDept.Text = "";
			lblEditRound.Text = "";
			//------------- Clear Key ---------------
			lblEditRound.Attributes["R_Year"] = "";
			lblEditRound.Attributes["R_Round"] = "";
			lblEditRound.Attributes["DEPT_CODE"] = "";
			lblEditRound.Attributes["MINOR_CODE"] = "";


			dtp_KPI_CP_Start.Value = DateTime.FromOADate(0);
			dtp_KPI_CP_End.Value = DateTime.FromOADate(0);
			dtp_KPI_AP_Start.Value = DateTime.FromOADate(0);
			dtp_KPI_AP_End.Value = DateTime.FromOADate(0);
			dtp_KPI_MGR_Start.Value = DateTime.FromOADate(0);
			dtp_KPI_MGR_End.Value = DateTime.FromOADate(0);
			dtp_COMP_CP_Start.Value = DateTime.FromOADate(0);
			dtp_COMP_CP_End.Value = DateTime.FromOADate(0);
			dtp_COMP_AP_Start.Value = DateTime.FromOADate(0);
			dtp_COMP_AP_End.Value = DateTime.FromOADate(0);
			dtp_COMP_MGR_Start.Value = DateTime.FromOADate(0);
			dtp_COMP_MGR_End.Value = DateTime.FromOADate(0);

		}

		protected void btnCancel_Click(object sender, System.EventArgs e)
		{
			BindList();
			pnlEdit.Visible = false;
			pnlList.Visible = true;
		}


		protected void btnSave_Click(object sender, System.EventArgs e)
		{
			//----------------------------------- Copy From Setting Round------------------------
			//----------------- KPI--------------
			if (!dtp_KPI_CP_Start.IsDateSelected) {
				ScriptManager.RegisterStartupScript(this.Page, typeof(string), "alert", "showAlert('เลือกวันกำหนดแบบประเมินตัวชี้วัด');", true);
				dtp_KPI_CP_Start.Focus();
				return;
			}

			if (!dtp_KPI_CP_End.IsDateSelected) {
				ScriptManager.RegisterStartupScript(this.Page, typeof(string), "alert", "showAlert('เลือกวันกำหนดแบบประเมินตัวชี้วัด');", true);
				dtp_KPI_CP_End.Focus();
				return;
			}

			if (dtp_KPI_CP_Start.Value >= dtp_KPI_CP_End.Value) {
				ScriptManager.RegisterStartupScript(this.Page, typeof(string), "alert", "showAlert('ตรวจสอบวันสิ้นสุดกำหนดแบบประเมินตัวชี้วัด');", true);
				dtp_KPI_CP_End.Focus();
				return;
			}

			if (!dtp_KPI_AP_Start.IsDateSelected) {
				ScriptManager.RegisterStartupScript(this.Page, typeof(string), "alert", "showAlert('เลือกวันกำหนดประเมินตัวชี้วัด');", true);
				dtp_KPI_AP_Start.Focus();
				return;
			}

			if (!dtp_KPI_AP_End.IsDateSelected) {
				ScriptManager.RegisterStartupScript(this.Page, typeof(string), "alert", "showAlert('เลือกวันกำหนดประเมินตัวชี้วัด');", true);
				dtp_KPI_AP_End.Focus();
				return;
			}
			if (dtp_KPI_AP_Start.Value >= dtp_KPI_AP_End.Value) {
				ScriptManager.RegisterStartupScript(this.Page, typeof(string), "alert", "showAlert('ตรวจสอบวันสิ้นสุดการประเมินตัวชี้วัด');", true);
				dtp_KPI_AP_End.Focus();
				return;
			}

			if (!dtp_KPI_MGR_Start.IsDateSelected) {
				ScriptManager.RegisterStartupScript(this.Page, typeof(string), "alert", "showAlert('เลือกวันกำหนดประเมินตัวชี้วัด');", true);
				dtp_KPI_MGR_Start.Focus();
				return;
			}
			if (!dtp_KPI_MGR_End.IsDateSelected) {
				ScriptManager.RegisterStartupScript(this.Page, typeof(string), "alert", "showAlert('เลือกวันกำหนดประเมินตัวชี้วัด');", true);
				dtp_KPI_MGR_End.Focus();
				return;
			}

			if (dtp_KPI_MGR_Start.Value >= dtp_KPI_MGR_End.Value) {
				ScriptManager.RegisterStartupScript(this.Page, typeof(string), "alert", "showAlert('ตรวจสอบวันสิ้นสุดการประเมินตัวชี้วัด');", true);
				dtp_KPI_MGR_End.Focus();
				return;
			}
			//----------------- COMP--------------
			if (!dtp_COMP_CP_Start.IsDateSelected) {
				ScriptManager.RegisterStartupScript(this.Page, typeof(string), "alert", "showAlert('เลือกวันกำหนดแบบประเมินสมรรถนะ');", true);
				dtp_COMP_CP_Start.Focus();
				return;
			}
			if (!dtp_COMP_CP_End.IsDateSelected) {
				ScriptManager.RegisterStartupScript(this.Page, typeof(string), "alert", "showAlert('เลือกวันกำหนดแบบประเมินสมรรถนะ');", true);
				dtp_COMP_CP_End.Focus();
				return;
			}
			if (dtp_COMP_CP_Start.Value >= dtp_COMP_CP_End.Value) {
				ScriptManager.RegisterStartupScript(this.Page, typeof(string), "alert", "showAlert('ตรวจสอบวันสิ้นสุดกำหนดแบบประเมินสมรรถนะ');", true);
				dtp_COMP_CP_End.Focus();
				return;
			}
			if (!dtp_COMP_AP_Start.IsDateSelected) {
				ScriptManager.RegisterStartupScript(this.Page, typeof(string), "alert", "showAlert('เลือกวันกำหนดประเมินสมรรถนะ');", true);
				dtp_COMP_AP_Start.Focus();
				return;
			}
			if (!dtp_COMP_AP_End.IsDateSelected) {
				ScriptManager.RegisterStartupScript(this.Page, typeof(string), "alert", "showAlert('เลือกวันกำหนดประเมินสมรรถนะ');", true);
				dtp_COMP_AP_End.Focus();
				return;
			}
			if (dtp_COMP_AP_Start.Value >= dtp_COMP_AP_End.Value) {
				ScriptManager.RegisterStartupScript(this.Page, typeof(string), "alert", "showAlert('ตรวจสอบวันสิ้นสุดการประเมินสมรรถนะ');", true);
				dtp_COMP_AP_End.Focus();
				return;
			}
			if (!dtp_COMP_MGR_Start.IsDateSelected) {
				ScriptManager.RegisterStartupScript(this.Page, typeof(string), "alert", "showAlert('เลือกวันกำหนดประเมินสมรรถนะ');", true);
				dtp_COMP_MGR_Start.Focus();
				return;
			}
			if (!dtp_COMP_MGR_End.IsDateSelected) {
				ScriptManager.RegisterStartupScript(this.Page, typeof(string), "alert", "showAlert('เลือกวันกำหนดประเมินสมรรถนะ');", true);
				dtp_COMP_MGR_End.Focus();
				return;
			}
			if (dtp_COMP_MGR_Start.Value >= dtp_COMP_MGR_End.Value) {
				ScriptManager.RegisterStartupScript(this.Page, typeof(string), "alert", "showAlert('ตรวจสอบวันสิ้นสุดการประเมินสมรรถนะ');", true);
				dtp_COMP_MGR_End.Focus();
				return;
			}

			string R_Year = lblEditRound.Attributes["R_Year"];
			string R_Round = lblEditRound.Attributes["R_Round"];
			string DEPT_CODE = lblEditRound.Attributes["DEPT_CODE"];
			string MINOR_CODE = lblEditRound.Attributes["MINOR_CODE"];


			string SQL = null;
			SqlDataAdapter DA = new SqlDataAdapter();
			DataTable DT = new DataTable();
			//---------------tb_HR_KPI_Create_Period------------------
			SQL = "SELECT * FROM tb_HR_KPI_Create_Period\n";
			SQL += " WHERE R_Year=" + R_Year + "\n";
			SQL += " AND R_Round=" + R_Round + "\n";
			SQL += " AND PNDP_DEPT_CODE1='" + DEPT_CODE + "'\n";
			SQL += " AND PNDP_MINOR_CODE2='" + MINOR_CODE + "'\n";
			DA = new SqlDataAdapter(SQL, BL.ConnectionString());
			DT = new DataTable();
			DA.Fill(DT);

			DataRow DR = null;
			if (DT.Rows.Count == 0) {
				DR = DT.NewRow();
				DR["R_Year"] = R_Year;
				DR["R_Round"] = R_Round;
				DR["PNDP_DEPT_CODE1"] = DEPT_CODE;
				DR["PNDP_MINOR_CODE2"] = MINOR_CODE;
				DR["CP_ID"] = 0;
				DR["Create_By"] = Session["USER_PSNL_NO"];
				DR["Create_Time"] = DateAndTime.Now;
			} else {
				DR = DT.Rows[0];
			}

			DR["CP_Start"] = dtp_KPI_CP_Start.Value;
			DR["CP_End"] = dtp_KPI_CP_End.Value;
			DR["Update_By"] = Session["USER_PSNL_NO"];
			DR["Update_Time"] = DateAndTime.Now;
			if (DT.Rows.Count == 0)
				DT.Rows.Add(DR);
			try {
				SqlCommandBuilder cmd = new SqlCommandBuilder(DA);
				DA.Update(DT);
			} catch (Exception ex) {
				ScriptManager.RegisterStartupScript(this.Page, typeof(string), "alert1", "showAlert('ไม่สามารถบันทึกได้');", true);
				ScriptManager.RegisterStartupScript(this.Page, typeof(string), "alert2", "showAlert('" + ex.Message.Replace("'", "\"") + "');", true);
				return;
			}

			//---------------tb_HR_KPI_Assessment_Period------------------
			SQL = "SELECT * FROM tb_HR_KPI_Assessment_Period\n";
			SQL += " WHERE R_Year=" + R_Year + "\n";
			SQL += " AND R_Round=" + R_Round + "\n";
			SQL += " AND PNDP_DEPT_CODE1='" + DEPT_CODE + "'\n";
			SQL += " AND PNDP_MINOR_CODE2='" + MINOR_CODE + "'\n";
			DA = new SqlDataAdapter(SQL, BL.ConnectionString());
			DT = new DataTable();
			DA.Fill(DT);
			if (DT.Rows.Count == 0) {
				DR = DT.NewRow();
				DR["R_Year"] = R_Year;
				DR["R_Round"] = R_Round;
				DR["PNDP_DEPT_CODE1"] = DEPT_CODE;
				DR["PNDP_MINOR_CODE2"] = MINOR_CODE;
				DR["AP_ID"] = 0;
				DR["Create_By"] = Session["USER_PSNL_NO"];
				DR["Create_Time"] = DateAndTime.Now;
			} else {
				DR = DT.Rows[0];
			}
			DR["AP_Start"] = dtp_KPI_AP_Start.Value;
			DR["AP_End"] = dtp_KPI_AP_End.Value;
			DR["MGR_Start"] = dtp_KPI_MGR_Start.Value;
			DR["MGR_End"] = dtp_KPI_MGR_End.Value;
			DR["Update_By"] = Session["USER_PSNL_NO"];
			DR["Update_Time"] = DateAndTime.Now;
			if (DT.Rows.Count == 0)
				DT.Rows.Add(DR);
			try {
				SqlCommandBuilder cmd = new SqlCommandBuilder(DA);
				DA.Update(DT);
			} catch (Exception ex) {
				ScriptManager.RegisterStartupScript(this.Page, typeof(string), "alert1", "showAlert('ไม่สามารถบันทึกได้');", true);
				ScriptManager.RegisterStartupScript(this.Page, typeof(string), "alert2", "showAlert('" + ex.Message.Replace("'", "\"") + "');", true);
				return;
			}
			//---------------tb_HR_COMP_Create_Period------------------
			SQL = "SELECT * FROM tb_HR_COMP_Create_Period\n";
			SQL += " WHERE R_Year=" + R_Year + "\n";
			SQL += " AND R_Round=" + R_Round + "\n";
			SQL += " AND PNDP_DEPT_CODE1='" + DEPT_CODE + "'\n";
			SQL += " AND PNDP_MINOR_CODE2='" + MINOR_CODE + "'\n";
			DA = new SqlDataAdapter(SQL, BL.ConnectionString());
			DT = new DataTable();
			DA.Fill(DT);

			if (DT.Rows.Count == 0) {
				DR = DT.NewRow();
				DR["R_Year"] = R_Year;
				DR["R_Round"] = R_Round;
				DR["PNDP_DEPT_CODE1"] = DEPT_CODE;
				DR["PNDP_MINOR_CODE2"] = MINOR_CODE;
				DR["CP_ID"] = 0;
				DR["Create_By"] = Session["USER_PSNL_NO"];
				DR["Create_Time"] = DateAndTime.Now;
			} else {
				DR = DT.Rows[0];
			}

			DR["CP_Start"] = dtp_COMP_CP_Start.Value;
			DR["CP_End"] = dtp_COMP_CP_End.Value;
			DR["Update_By"] = Session["USER_PSNL_NO"];
			DR["Update_Time"] = DateAndTime.Now;
			if (DT.Rows.Count == 0)
				DT.Rows.Add(DR);
			try {
				SqlCommandBuilder cmd = new SqlCommandBuilder(DA);
				DA.Update(DT);
			} catch (Exception ex) {
				ScriptManager.RegisterStartupScript(this.Page, typeof(string), "alert1", "showAlert('ไม่สามารถบันทึกได้');", true);
				ScriptManager.RegisterStartupScript(this.Page, typeof(string), "alert2", "showAlert('" + ex.Message.Replace("'", "\"") + "');", true);
				return;
			}
			//---------------tb_HR_COMP_Assessment_Period------------------
			SQL = "SELECT * FROM tb_HR_COMP_Assessment_Period\n";
			SQL += " WHERE R_Year=" + R_Year + "\n";
			SQL += " AND R_Round=" + R_Round + "\n";
			SQL += " AND PNDP_DEPT_CODE1='" + DEPT_CODE + "'\n";
			SQL += " AND PNDP_MINOR_CODE2='" + MINOR_CODE + "'\n";
			DA = new SqlDataAdapter(SQL, BL.ConnectionString());
			DT = new DataTable();
			DA.Fill(DT);
			if (DT.Rows.Count == 0) {
				DR = DT.NewRow();
				DR["R_Year"] = R_Year;
				DR["R_Round"] = R_Round;
				DR["PNDP_DEPT_CODE1"] = DEPT_CODE;
				DR["PNDP_MINOR_CODE2"] = MINOR_CODE;
				DR["AP_ID"] = 0;
				DR["Create_By"] = Session["USER_PSNL_NO"];
				DR["Create_Time"] = DateAndTime.Now;
			} else {
				DR = DT.Rows[0];
			}
			DR["AP_Start"] = dtp_COMP_AP_Start.Value;
			DR["AP_End"] = dtp_COMP_AP_End.Value;
			DR["MGR_Start"] = dtp_COMP_MGR_Start.Value;
			DR["MGR_End"] = dtp_COMP_MGR_End.Value;
			DR["Update_By"] = Session["USER_PSNL_NO"];
			DR["Update_Time"] = DateAndTime.Now;
			if (DT.Rows.Count == 0)
				DT.Rows.Add(DR);
			try {
				SqlCommandBuilder cmd = new SqlCommandBuilder(DA);
				DA.Update(DT);
			} catch (Exception ex) {
				ScriptManager.RegisterStartupScript(this.Page, typeof(string), "alert1", "showAlert('ไม่สามารถบันทึกได้');", true);
				ScriptManager.RegisterStartupScript(this.Page, typeof(string), "alert2", "showAlert('" + ex.Message.Replace("'", "\"") + "');", true);
				return;
			}

			ScriptManager.RegisterStartupScript(this.Page, typeof(string), "alert", "showAlert('บันทึกสำเร็จ');", true);
			BindList();
			pnlList.Visible = true;
			pnlEdit.Visible = false;
		}


		protected void btnDefault_Click(object sender, System.EventArgs e)
		{
			string R_Year = lblEditRound.Attributes["R_Year"];
			string R_Round = lblEditRound.Attributes["R_Round"];
			string DEPT_CODE = lblEditRound.Attributes["DEPT_CODE"];
			string MINOR_CODE = lblEditRound.Attributes["MINOR_CODE"];

			SqlConnection Conn = new SqlConnection(BL.ConnectionString());
			Conn.Open();

			string SQL = "";
			SQL += " DELETE FROM tb_HR_KPI_Create_Period WHERE R_Year=" + R_Year + " AND R_Round=" + R_Round + " AND PNDP_DEPT_CODE1='" + DEPT_CODE + "' AND PNDP_MINOR_CODE2='" + MINOR_CODE + "'\n";
			SQL += " DELETE FROM tb_HR_KPI_Assessment_Period WHERE R_Year=" + R_Year + " AND R_Round=" + R_Round + " AND PNDP_DEPT_CODE1='" + DEPT_CODE + "' AND PNDP_MINOR_CODE2='" + MINOR_CODE + "'\n";
			SQL += " DELETE FROM tb_HR_COMP_Create_Period WHERE R_Year=" + R_Year + " AND R_Round=" + R_Round + " AND PNDP_DEPT_CODE1='" + DEPT_CODE + "' AND PNDP_MINOR_CODE2='" + MINOR_CODE + "'\n";
			SQL += " DELETE FROM tb_HR_COMP_Assessment_Period WHERE R_Year=" + R_Year + " AND R_Round=" + R_Round + " AND PNDP_DEPT_CODE1='" + DEPT_CODE + "' AND PNDP_MINOR_CODE2='" + MINOR_CODE + "'\n";

			try {
				SqlCommand Comm = new SqlCommand();
				var _with1 = Comm;
				_with1.Connection = Conn;
				_with1.CommandType = CommandType.Text;
				_with1.CommandText = SQL;
				_with1.ExecuteNonQuery();
				_with1.Dispose();
			} catch (Exception ex) {
				ScriptManager.RegisterStartupScript(this.Page, typeof(string), "alert1", "showAlert('ไม่สามารถดำเนินการได้');", true);
				ScriptManager.RegisterStartupScript(this.Page, typeof(string), "alert2", "showAlert('" + ex.Message.Replace("'", "\"") + "');", true);
				Conn.Close();
				return;
			}

			Conn.Close();
			ScriptManager.RegisterStartupScript(this.Page, typeof(string), "alert", "showAlert('ปรับเวลาสำเร็จ');", true);
			BindList();
			pnlList.Visible = true;
			pnlEdit.Visible = false;
		}
		public AssessmentSetting_OrganizePeriod()
		{
			Load += Page_Load;
		}
	}
}

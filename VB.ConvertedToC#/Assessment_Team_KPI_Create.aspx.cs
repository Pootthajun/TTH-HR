using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Xml.Linq;
using System.Diagnostics;
using System.Collections.Specialized;
using System.Configuration;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Caching;
using System.Web.SessionState;
using System.Web.Security;
using System.Web.Profile;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
namespace VB
{

	public partial class Assessment_Team_KPI_Create : System.Web.UI.Page
	{


		HRBL BL = new HRBL();
		GenericLib GL = new GenericLib();

		textControlLib TC = new textControlLib();
		public int R_Year {
			get {
				try {
					return GL.CINT( GL.SplitString(ddlRound.Items[ddlRound.SelectedIndex].Value, "-")[0]);
				} catch (Exception ex) {
					return 0;
				}
			}
		}

		public int R_Round {
			get {
				try {
					return GL.CINT(GL.SplitString(ddlRound.Items[ddlRound.SelectedIndex].Value, "-")[1]);
				} catch (Exception ex) {
					return 0;
				}
			}
		}

		public string PSNL_NO {
			get { return lblPSNName.Attributes["PSNL_NO"]; }
			set { lblPSNName.Attributes["PSNL_NO"] = value; }
		}

		public string ASSESSOR_BY {
			get {
				try {
					string[] tmp = Strings.Split(ddlASSName.Items[ddlASSName.SelectedIndex].Value, ":::");
					return tmp[0];
				} catch (Exception ex) {
					return "";
				}
			}
		}

		public string ASSESSOR_NAME {
			get {
				try {
					return ddlASSName.Items[ddlASSName.SelectedIndex].Text;
				} catch (Exception ex) {
					return "";
				}
			}
		}

		public string ASSESSOR_POS {
			get {
				try {
					string[] tmp = Strings.Split(ddlASSName.Items[ddlASSName.SelectedIndex].Value, ":::");
					return tmp[1];
				} catch (Exception ex) {
					return "";
				}
			}
		}

		public string ASSESSOR_DEPT {
			get {
				try {
					string[] tmp = Strings.Split(ddlASSName.Items[ddlASSName.SelectedIndex].Value, ":::");
					return tmp[2];
				} catch (Exception ex) {
					return "";
				}
			}
		}

		public int KPI_No {
			get { return Convert.ToInt32(lblKPINo.Text); }
			set { lblKPINo.Text = value.ToString().PadLeft(2, GL.chr0); }
		}

		public int KPI_Status {
            get { return GL.CINT(lblKPIStatus.Attributes["KPI_Status"]); }
            set { lblKPIStatus.Attributes["KPI_Status"] = value.ToString(); }
		}

        public string DEPT_CODE
        {
            get
            {
                try
                {
                    return lblKPIStatus.Attributes["DEPT_CODE"];
                }
                catch (Exception ex)
                {
                    return "";
                }
            }
            set { lblKPIStatus.Attributes["DEPT_CODE"] = value.ToString(); }
        }


		protected void Page_Load(object sender, System.EventArgs e)
		{
			if ((Session["USER_PSNL_NO"] == null)) {
				ScriptManager.RegisterStartupScript(this.Page, typeof(string), "Alert", "showAlert('กรุณาลงชื่อเข้าใช้ระบบ');", true);
				Response.Redirect("Login.aspx");
				return;
			}

			if (!IsPostBack) {

                // Keep Start Time
                DateTime StartTime = DateTime.Now;

				BL.BindDDlYearRound(ddlRound);
               
				BindDDlAssessmentStatus();
                chk_CreateHR_Actual_Assessor();
				//-----------fn copy paste-------
				
                Update_KPI_Status_All_Team(); //ถ้ายังช้า ลบบรรทัดนี้ออก
				BindPersonalList();
                //----------------- ทำสองที่ Page Load กับ Update Status ---------------

				pnlList.Visible = true;
				pnlEdit.Visible = false;

                //Report Time To Entry This Page
                DateTime EndTime = DateTime.Now;
                lblReportTime.Text = "เริ่มเข้าสู่หน้านี้ " + StartTime.ToString("HH:mm:ss.ff") + " ถึง " + EndTime.ToString("HH:mm:ss.ff") + " ใช้เวลา " + Math.Round( GL.DiffTimeDecimalSeconds(StartTime,EndTime),2) + " วินาที";
			}
		}

        private void Update_KPI_Status_All_Team()
        {
            DataTable dt = GetData();
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                string _psnlNo = dt.Rows[i]["PSNL_NO"].ToString();
                BL.Update_KPI_Status_To_Assessment_Period(_psnlNo, R_Year, R_Round);
            }
        }

        private void chk_CreateHR_Actual_Assessor()
        {

            Boolean  IsCompleted =BL.Is_Round_Completed(R_Year, R_Round);
            if (!IsCompleted)
            {
            SqlDataAdapter DA;
            DataTable PSN = new DataTable();
            string SQL = "";
            SQL +=" select PSNL_NO from tb_HR_Actual_Assessor where MGR_PSNL_NO ='" + Session["USER_PSNL_NO"].ToString().Replace("'", "''") + "' AND R_Year='"+ R_Year+"' AND R_Round ='"+ R_Round +"' \n";
            SQL +=" AND  PSNL_NO NOT IN (select PSN_PSNL_NO from vw_HR_ASSESSOR_PSN where MGR_PSNL_NO ='" + Session["USER_PSNL_NO"].ToString().Replace("'", "''") + "'  AND R_Year='"+ R_Year+"'  AND R_Round ='"+ R_Round +"') \n";
            DA = new SqlDataAdapter(SQL, BL.ConnectionString());
            DA.Fill(PSN);
            if (PSN.Rows .Count > 0)
            {
                for (int i = 0; i < PSN.Rows .Count ; i++)
			    {
                    BL.CreateHR_Actual_Assessor(R_Year, R_Round, PSN.Rows[i]["PSNL_NO"].ToString(), Session["USER_PSNL_NO"].ToString().Replace("'", "''"));
			    }
            }
            }

        }

		private void BindDDlAssessmentStatus()
		{
			ddlStatus.Items.Clear();
			ddlStatus.Items.Add(new ListItem("ทั้งหมด", "0"));
			ddlStatus.Items.Add(new ListItem("ยังไม่ส่งแบบประเมิน", "1"));
			ddlStatus.Items.Add(new ListItem("อนุมัติแบบประเมินแล้ว", "2"));
		}

		protected void Search(object sender, System.EventArgs e)
		{
			BindPersonalList();
            Update_KPI_Status_All_Team();
            BindPersonalList();

			if (pnlEdit.Visible) {
				BindPersonal();
				BindAssessor();
				BindMasterKPI();

				SetPrintButton();
                    // มีเอาไว้คิดรอบที่กำลังทำ อยู่ในช่วงที่กำหนดหรือไม่  
                    Boolean IsInPeriod = BL.IsTimeInAssessmentPeriod(DateTime.Now, R_Year, R_Round, HRBL.AssessmentType.KPI, HRBL.AssessmentStatus.Creating, DEPT_CODE.ToString());
                    // หาว่ารอบปิดหรือยัง  
                    Boolean IsRoundCompleted = BL.Is_Round_Completed(R_Year, R_Round);
                    Boolean IsEditable = (KPI_Status <= HRBL.AssessmentStatus.WaitCreatingApproved) & IsInPeriod & !IsRoundCompleted;

                    lnkCopyAll.Enabled = true;

            
                    if ((IsEditable) && (Session["copyAll_KPI"] != null))
                    {
                        lnkPasteAll.Style["color"] = "black";
                        lnkPasteAll.Enabled = true;
                    }
                    else
                    {
                        lnkPasteAll.Style["color"] = "gray";
                        lnkPasteAll.Enabled = false;
                    }
			}
        }

        

		private void SetPrintButton()
		{
			btnPDF.HRef = "Print/RPT_K_PNSAss.aspx?MODE=PDF&R_Year=" + R_Year + "&R_Round=" + R_Round + "&PSNL_No=" + PSNL_NO + "&Status=" + GL.CINT(KPI_Status);
			btnExcel.HRef = "Print/RPT_K_PNSAss.aspx?MODE=EXCEL&R_Year=" + R_Year + "&R_Round=" + R_Round + "&PSNL_No=" + PSNL_NO + "&Status=" + GL.CINT(KPI_Status);
		}

        public DataTable GetData()
        {
            string SQL = "";


            SQL += " DECLARE @R_Year As Int=" + R_Year + "\n";
            SQL += " DECLARE @R_Round As Int=" + R_Round + "\n";
            SQL += " DECLARE @ASSESSOR_CODE AS nvarchar(50)='" + Session["USER_PSNL_NO"].ToString().Replace("'", "''") + "'\n";

            SQL += " Select DISTINCT\n";
            SQL += " Header.R_Year, Header.R_Round\n";
            SQL += " , Header.DEPT_CODE,Header.DEPT_Name,Header.PSNL_NO,Header.PSNL_Fullname\n";
            SQL += " ,PSN.PSN_PNPS_CLASS PSNL_CLASS,Header.POS_NO,Header.POS_Name,\n";
            SQL += " Header.KPI_Status, Header.KPI_Status_Name\n";
            SQL += " FROM vw_HR_ASSESSOR_PSN PSN\n";
            SQL += " inner JOIN vw_RPT_KPI_Status Header ON   PSN.PSN_PSNL_NO= Header.PSNL_NO \n";
            SQL += " \t\t\t\t\t\t\t\t\tAND Header.R_Year=@R_Year AND Header.R_Round=@R_Round\n";
            SQL += " LEFT JOIN vw_PN_PSNL_ALL PSN_ALL ON Header.PSNL_NO=PSN_ALL.PSNL_NO \n";
            SQL += " WHERE PSN.R_Year=@R_Year AND PSN.R_Round=@R_Round AND \n";
            SQL += " PSN.MGR_PSNL_NO=@ASSESSOR_CODE\n";
            SQL += "  AND (ISNULL(PSN_ALL.PNPS_RESIGN_DATE,PSN_ALL.PNPS_RETIRE_DATE) > (SELECT MAX(R_End) FROM tb_HR_Round WHERE R_Year=@R_Year AND R_Round=@R_Round)) \n";

            if (!string.IsNullOrEmpty(txtName.Text))
            {
                SQL += " AND (Header.PSNL_NO LIKE '%" + txtName.Text.Replace("'", "''") + "%' OR Header.PSNL_Fullname LIKE '%" + txtName.Text.Replace("'", "''") + "%')";
            }
            switch (ddlStatus.SelectedIndex)
            {
                case 1:
                    SQL += " AND (Header.KPI_Status IS NULL OR Header.KPI_Status<2)";
                    break;
                case 2:
                    SQL += " AND Header.KPI_Status>1";
                    break;
            }
            SQL += " ORDER BY Header.DEPT_CODE,PSNL_CLASS DESC,Header.POS_NO\n";

            SqlDataAdapter DA = new SqlDataAdapter(SQL, BL.ConnectionString());
            DataTable DT = new DataTable();
            DA.SelectCommand.CommandTimeout = 200;

            if (ddlRound.SelectedIndex > 0)
            {
                DA.Fill(DT);
            }

            Session["Assessment_Team_KPI_Create"] = DT;
            return DT;
        }
		private void BindPersonalList()
		{
            DataTable DT = new DataTable();
            DT = GetData();
			Pager.SesssionSourceName = "Assessment_Team_KPI_Create";

			Pager.RenderLayout();

			if (DT.Rows.Count == 0) {
				lblCountPSN.Text = "ไม่พบรายการดังกล่าว";
			} else {
				lblCountPSN.Text = "พบ " + GL.StringFormatNumber(DT.Rows.Count, 0) + " รายการ";
			}

		}

		protected void Pager_PageChanging(PageNavigation Sender)
		{
			Pager.TheRepeater = rptKPI;
		}

		protected void rptKPI_ItemCommand(object source, System.Web.UI.WebControls.RepeaterCommandEventArgs e)
		{
			switch (e.CommandName) {
				case "Edit":
					Button btnPSNEdit =(Button) e.Item.FindControl("btnPSNEdit");

					pnlList.Visible = false;
					pnlEdit.Visible = true;
					divView.Visible = true;
					divEdit.Visible = false;
					//-------------- ดึงข้อมูลบุคคล ------------
					PSNL_NO = btnPSNEdit.CommandArgument.ToString();
					BindPersonal();
					BindAssessor();
					BindMasterKPI();

					SaveHeader();
					//-----------Create First-------
					SetPrintButton();

					break;
				case "Delete":

					PSNL_NO = e.CommandArgument.ToString ();

					//---------------- Delete Detail -----------------
					string SQL = "DELETE FROM tb_HR_KPI_Detail \n";
					SQL += " WHERE R_Year=" + R_Year + " AND R_Round=" + R_Round + "\n";
					SQL += " AND PSNL_NO='" + PSNL_NO.Replace("'", "''") + "'\n";
					//---------------- Delete Header -----------------
					SQL += "DELETE FROM tb_HR_KPI_Header \n";
					SQL += " WHERE R_Year=" + R_Year + " AND R_Round=" + R_Round + "\n";
					SQL += " AND PSNL_NO='" + PSNL_NO.Replace("'", "''") + "'\n";

					SqlConnection Conn = new SqlConnection(BL.ConnectionString());
					Conn.Open();
					SqlCommand Comm = new SqlCommand();
					var _with1 = Comm;
					_with1.Connection = Conn;
					_with1.CommandType = CommandType.Text;
					_with1.CommandText = SQL;
					_with1.ExecuteNonQuery();
					_with1.Dispose();
					Conn.Close();
					Conn.Dispose();

					//---------- Refresh ----------
					BindPersonalList();

					break;
			}
		}

		private void BindMasterKPI()
		{
			//---------------- Set Status -------------------
			KPI_Status = BL.GetKPIStatus(R_Year, R_Round, PSNL_NO);
			lblKPIStatus.Text = BL.GetAssessmentStatusName(KPI_Status);

			//--------------- Bind Detail----------------------
			HRBL.DataManager DM = BL.GetKPIDetail(R_Year, R_Round, PSNL_NO);
			DataTable DT = DM.Table;
			SqlDataAdapter DA = DM.Adaptor;
			rptAss.DataSource = DM.Table;
			rptAss.DataBind();

            // มีเอาไว้คิดรอบที่กำลังทำ อยู่ในช่วงที่กำหนดหรือไม่  
            Boolean IsInPeriod = BL.IsTimeInAssessmentPeriod(DateTime.Now, R_Year, R_Round, HRBL.AssessmentType.KPI, HRBL.AssessmentStatus.Creating, DEPT_CODE.ToString());
            // หาว่ารอบปิดหรือยัง  
            Boolean IsRoundCompleted = BL.Is_Round_Completed(R_Year, R_Round);
            Boolean IsEditable = (KPI_Status <= HRBL.AssessmentStatus.WaitCreatingApproved) & IsInPeriod & !IsRoundCompleted;

            lnkPasteAll.Enabled = IsEditable;
            lnkPasteAll.Style["color"] = "gray";

                    btnAdd.Visible = IsEditable;
                    btnPreSend.Visible = IsEditable;
                    ddlASSName.Enabled = IsEditable; 
            
		}

		//------------ For Grouping -----------
		string LastOrgranizeName = "";
		protected void rptKPI_ItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
		{
			if (e.Item.ItemType != ListItemType.AlternatingItem & e.Item.ItemType != ListItemType.Item)
				return;

			Label lnkPSNOrganize =(Label) e.Item.FindControl("lnkPSNOrganize");
			Label lblPSNNo = (Label)e.Item.FindControl("lblPSNNo");
			Label lblPSNName = (Label)e.Item.FindControl("lblPSNName");
			 Label lblPSNPos = (Label)e.Item.FindControl("lblPSNPos");
			 Label lblPSNClass = (Label)e.Item.FindControl("lblPSNClass");
			Label lblPSNKPIStatus =(Label) e.Item.FindControl("lblPSNKPIStatus");
			Button btnPSNEdit =(Button) e.Item.FindControl("btnPSNEdit");
			  Button btnPSNDelete =(Button) e.Item.FindControl("btnPSNDelete");
			AjaxControlToolkit.ConfirmButtonExtender cfm_Delete =(AjaxControlToolkit.ConfirmButtonExtender) e.Item.FindControl("cfm_Delete");

            DataRowView drv = (DataRowView)e.Item.DataItem;
			if (Convert.ToString(drv["DEPT_NAME"]) != LastOrgranizeName) {
				lnkPSNOrganize.Text = drv["DEPT_NAME"].ToString ();
				LastOrgranizeName = drv["DEPT_NAME"].ToString ();
			}

			//lnkPSNOrganize.Attributes("DEPT_CODE") = drv["DEPT_CODE"]
			lblPSNNo.Text = drv["PSNL_NO"].ToString ();
			lblPSNName.Text = drv["PSNL_Fullname"].ToString ();
			lblPSNPos.Text = drv["POS_Name"].ToString ();
			lblPSNKPIStatus.Text = drv["KPI_Status_Name"].ToString ();
			if (Information.IsNumeric(GL.CINT (drv["PSNL_CLASS"]))) {
				lblPSNClass.Text = GL.CINT (drv["PSNL_CLASS"]).ToString ();
			}
			
            //------ปรับสีสถานะ-----------
            //----<0
            if (GL.IsEqualNull(drv["KPI_Status"]) || GL.CINT(drv["KPI_Status"]) == HRBL.AssessmentStatus.Creating)
            {
                lblPSNKPIStatus.ForeColor = System.Drawing.Color.Red;
                //----1
            }
            else if (GL.IsEqualNull(drv["KPI_Status"]) || GL.CINT(drv["KPI_Status"]) == HRBL.AssessmentStatus.WaitCreatingApproved)
            {
                lblPSNKPIStatus.ForeColor = System.Drawing.Color.DarkRed;
                //----2
            }
            else if (GL.IsEqualNull(drv["KPI_Status"]) || GL.CINT(drv["KPI_Status"]) == HRBL.AssessmentStatus.CreatedApproved)
            {
                lblPSNKPIStatus.ForeColor = System.Drawing.Color.Orange;
                //----3
            }
            else if (GL.IsEqualNull(drv["KPI_Status"]) || GL.CINT(drv["KPI_Status"]) == HRBL.AssessmentStatus.WaitAssessment)
            {
                lblPSNKPIStatus.ForeColor = System.Drawing.Color.DarkBlue;
                //----4
            }
            else if (GL.IsEqualNull(drv["KPI_Status"]) || GL.CINT(drv["KPI_Status"]) == HRBL.AssessmentStatus.WaitConfirmAssessment)
            {
                lblPSNKPIStatus.ForeColor = System.Drawing.Color.BlueViolet;
                //----5
            }
            else if (GL.IsEqualNull(drv["KPI_Status"]) || GL.CINT(drv["KPI_Status"]) == HRBL.AssessmentStatus.AssessmentCompleted)
            {
                lblPSNKPIStatus.ForeColor = System.Drawing.Color.Green;
            }



			btnPSNEdit.CommandArgument = drv["PSNL_NO"].ToString ();
			btnPSNDelete.CommandArgument = drv["PSNL_NO"].ToString ();

			//----------------- Set Delete Enabling ---------------
			if (GL.IsEqualNull(drv["KPI_Status"]) || GL.CINT ( drv["KPI_Status"]) == -1) {
				btnPSNDelete.Visible = false;
			} else if (GL.CINT (drv["KPI_Status"]) < 2) {
				cfm_Delete.ConfirmText = "ยืนยันลบแบบประเมินของ " + drv["PSNL_Fullname"];
			//-------------- KPI_Status>=2 -------------------
			} else {
				btnPSNDelete.Visible = false;
			}

		}

		private void BindPersonal()
		{
			//----------Personal Info ----------
			HRBL.PersonalInfo PSNInfo = BL.GetAssessmentPersonalInfo(R_Year, R_Round, PSNL_NO, KPI_Status);
			lblPSNName.Text = PSNInfo.PSNL_Fullname;
			lblPSNDept.Text = PSNInfo.DEPT_NAME;
			if (!string.IsNullOrEmpty(PSNInfo.MGR_NAME)) {
				lblPSNPos.Text = PSNInfo.MGR_NAME;
			} else {
				lblPSNPos.Text = PSNInfo.FN_NAME;
			}
            DEPT_CODE = PSNInfo.DEPT_CODE;
		}

		private void BindAssessor()
		{
			BL.BindDDLAssessor(ddlASSName, R_Year, R_Round, PSNL_NO);
			ddlASSName_SelectedIndexChanged(null, null);
		}

		protected void ddlASSName_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			lblASSPos.Text = ASSESSOR_POS;
			lblASSDept.Text = ASSESSOR_DEPT;
			SaveHeader();
		}

		protected void rptAss_ItemCommand(object source, System.Web.UI.WebControls.RepeaterCommandEventArgs e)
		{
            HtmlTableCell ass_no = (HtmlTableCell)e.Item.FindControl("ass_no");
            DataTable DT;
			switch (e.CommandName) {
				case "Edit":

					ClearEditForm();
					KPI_No = Convert.ToInt32(Conversion.Val(ass_no.InnerHtml));
					 DT = BL.GetKPIDetail(R_Year, R_Round, PSNL_NO, KPI_No).Table;

					if (DT.Rows.Count == 0) {
						ScriptManager.RegisterStartupScript(this.Page, typeof(string), "Alert", "showAlert('กำลังปรับปรุงข้อมูลล่าสุด');", true);
						BindMasterKPI();
						return;
					}

					//-----------------------------
                    txt_KPI_Job.Text = DT.DefaultView[0]["KPI_Job"].ToString().Replace("\n", "\n");
					txt_KPI_Target.Text = DT.DefaultView[0]["KPI_Target_Text"].ToString().Replace("\n", "\n");
					txt_KPI_Choice1.Text = DT.DefaultView[0]["KPI_Choice_1"].ToString().Replace("\n", "\n");
					txt_KPI_Choice2.Text = DT.DefaultView[0]["KPI_Choice_2"].ToString().Replace("\n", "\n");
					txt_KPI_Choice3.Text = DT.DefaultView[0]["KPI_Choice_3"].ToString().Replace("\n", "\n");
					txt_KPI_Choice4.Text = DT.DefaultView[0]["KPI_Choice_4"].ToString().Replace("\n", "\n");
					txt_KPI_Choice5.Text = DT.DefaultView[0]["KPI_Choice_5"].ToString().Replace("\n", "\n");
					txt_KPI_Weight.Text = DT.DefaultView[0]["KPI_Weight"].ToString();


					divView.Visible = false;
					divEdit.Visible = true;

					break;
				case "Delete":

					
					int KPI_No_Delete = Convert.ToInt32(Conversion.Val(ass_no.InnerHtml));

					string SQL = "DELETE FROM tb_HR_KPI_Detail \n";
					SQL += " WHERE R_Year=" + R_Year + " AND R_Round=" + R_Round + "\n";
                    SQL += " AND PSNL_NO='" + PSNL_NO.Replace("'", "''") + "' AND KPI_No=" + KPI_No_Delete + "\n";

					SqlConnection Conn = new SqlConnection(BL.ConnectionString());
					Conn.Open();
					SqlCommand Comm = new SqlCommand();
					var _with2 = Comm;
					_with2.Connection = Conn;
					_with2.CommandType = CommandType.Text;
					_with2.CommandText = SQL;
					_with2.ExecuteNonQuery();
					_with2.Dispose();
					Conn.Close();
					Conn.Dispose();

					//-----------------Start REORDER ---------------
					HRBL.DataManager DM = BL.GetKPIDetail(R_Year, R_Round, PSNL_NO);
					 DT = DM.Table;
					SqlDataAdapter DA = DM.Adaptor;
					//---------- Need To Reorder------------
					bool NeedToReOrder = false;
					for (int i = 0; i <= DT.Rows.Count - 1; i++) {
						if ( GL.CINT ( DT.Rows[i]["KPI_No"]) != i + 1) {
							DT.Rows[i]["KPI_No"] = i + 1;
							NeedToReOrder = true;
						}
					}

					if (NeedToReOrder) {
						SqlCommandBuilder cmd = new SqlCommandBuilder(DA);
						DA.Update(DT);
						DT.AcceptChanges();
					}
					//-----------------End REORDER ---------------

					BindMasterKPI();

					break;
			}
		}


		protected void rptAss_ItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
		{
			switch (e.Item.ItemType) {

				case ListItemType.AlternatingItem:
				case ListItemType.Item:

					HtmlTableCell ass_no =(HtmlTableCell)  e.Item.FindControl("ass_no");
					HtmlTableCell job =(HtmlTableCell) e.Item.FindControl("job");
					HtmlTableCell target =(HtmlTableCell) e.Item.FindControl("target");
					HtmlTableCell weight =(HtmlTableCell) e.Item.FindControl("weight");
					HtmlTableCell cel_officer =(HtmlTableCell) e.Item.FindControl("cel_officer");
					HtmlTableCell cel_delete =(HtmlTableCell) e.Item.FindControl("cel_delete");
					Button btn_Delete =(Button) e.Item.FindControl("btn_Delete");
					Label lblSelect =(Label) e.Item.FindControl("lblSelect");
					HtmlTableCell cel_edit =(HtmlTableCell) e.Item.FindControl("cel_edit");
					Button btn_Edit =(Button) e.Item.FindControl("btn_Edit");
					AjaxControlToolkit.ConfirmButtonExtender cfm_Delete =(AjaxControlToolkit.ConfirmButtonExtender) e.Item.FindControl("cfm_Delete");

                    DataRowView drv = (DataRowView)e.Item.DataItem;
					ass_no.InnerHtml = drv["KPI_No"].ToString ();
					//.ToString().PadLeft(2, GL.chr0)
					job.InnerHtml = drv["KPI_Job"].ToString().Replace("\n", "<br>");
					target.InnerHtml = drv["KPI_Target_Text"].ToString().Replace("\n", "<br>");
					weight.InnerHtml = drv["KPI_Weight"].ToString();

					for (int i = 1; i <= 5; i++) {
						HtmlTableCell choice =(HtmlTableCell) e.Item.FindControl("choice" + i);
						HtmlTableCell selOfficialColor =(HtmlTableCell) e.Item.FindControl("selOfficialColor" + i);
						choice.InnerHtml = drv["KPI_Choice_" + i.ToString()].ToString().Replace("\n", "<br>");
					}

                    // มีเอาไว้คิดรอบที่กำลังทำ อยู่ในช่วงที่กำหนดหรือไม่  
                    Boolean IsInPeriod = BL.IsTimeInAssessmentPeriod(DateTime.Now, R_Year, R_Round, HRBL.AssessmentType.KPI, HRBL.AssessmentStatus.Creating, DEPT_CODE.ToString());
                    // หาว่ารอบปิดหรือยัง  
                    Boolean IsRoundCompleted = BL.Is_Round_Completed(R_Year, R_Round);
                    Boolean IsEditable = KPI_Status <= HRBL.AssessmentStatus.WaitCreatingApproved & IsInPeriod & !IsRoundCompleted;


					//-------------- Set Enabling --------------
                    if (GL.CINT(KPI_Status) <= GL.CINT(HRBL.AssessmentStatus.WaitCreatingApproved) && IsEditable)
                    {
						cfm_Delete.ConfirmText = "ยืนยันลบแบบประเมินรายการที่ " + ass_no.InnerHtml;
						cel_delete.Attributes["onclick"] = "document.getElementById('" + (btn_Delete.ClientID) + "').click();";
                        cel_edit.Attributes["onclick"] = "document.getElementById('" + (btn_Edit.ClientID) + "').click();";
                        ass_no.Attributes["onclick"] = "document.getElementById('" + (btn_Edit.ClientID) + "').click();";
                        job.Attributes["onclick"] = "document.getElementById('" + (btn_Edit.ClientID) + "').click();";
                        target.Attributes["onclick"] = "document.getElementById('" + (btn_Edit.ClientID) + "').click();";
                        weight.Attributes["onclick"] = "document.getElementById('" + (btn_Edit.ClientID) + "').click();";

						for (int i = 1; i <= 5; i++) {
							HtmlTableCell choice =(HtmlTableCell) e.Item.FindControl("choice" + i);
                            choice.Attributes["onclick"] = "document.getElementById('" + (btn_Edit.ClientID) + "').click();";
						}
					} else {
						ass_no.Style["cursor"] = "default";
						ass_no.Attributes["title"] = "";
						job.Style["cursor"] = "default";
						job.Attributes["title"] = "";
						target.Style["cursor"] = "default";
						target.Attributes["title"] = "";
						weight.Style["cursor"] = "default";
						weight.Attributes["title"] = "";
						for (int i = 1; i <= 5; i++) {
							HtmlTableCell choice =(HtmlTableCell) e.Item.FindControl("choice" + i);
							choice.Style["cursor"] = "default";
							choice.Attributes["title"] = "";
						}

						weight.RowSpan = 2;
						ass_no.RowSpan = 2;
						cel_delete.Visible = false;
						cel_edit.Visible = false;
					}

					break;
				case ListItemType.Footer:
					//----------------Report Summary ------------
					//----------- Control Difinition ------------
					HtmlTableCell cell_sum_weight =(HtmlTableCell) e.Item.FindControl("cell_sum_weight");
					//------------ Report -------------
					DataTable DT =(DataTable) rptAss.DataSource;
					double SUM = 0;
					for (int i = 0; i <= DT.Rows.Count - 1; i++) {
						if (!GL.IsEqualNull(DT.Rows[i]["KPI_Weight"])) {
							SUM += GL.CDBL ( DT.Rows[i]["KPI_Weight"]);
						}
					}

					cell_sum_weight.InnerHtml = SUM + "%";
					break;
			}

		}


		private void ClearEditForm()
		{
			KPI_No = 0;
			txt_KPI_Job.Text = "";
			txt_KPI_Target.Text = "";
			txt_KPI_Choice1.Text = "";
			txt_KPI_Choice2.Text = "";
			txt_KPI_Choice3.Text = "";
			txt_KPI_Choice4.Text = "";
			txt_KPI_Choice5.Text = "";
			txt_KPI_Weight.Text = "";
			TC.ImplementJavaFloatText(txt_KPI_Weight);
			txt_KPI_Weight.Style["text-align"] = "center";

		}


		protected void btnAdd_Click(object sender, System.EventArgs e)
		{
			ClearEditForm();

			lblKPINo.Text = BL.GetNewKPINo(R_Year, R_Round, PSNL_NO).ToString ();
			//.ToString().PadLeft(2, GL.chr0)
			divView.Visible = false;
			divEdit.Visible = true;

		}

		protected void btnClose_Click(object sender, System.EventArgs e)
		{
			BindMasterKPI();
			divView.Visible = true;
			divEdit.Visible = false;
		}



		protected void btnOK_Click(object sender, System.EventArgs e)
		{
			//------------------ Save Detail-----------------
			HRBL.DataManager DM = BL.GetKPIDetail(R_Year, R_Round, PSNL_NO);
			DataTable DT = DM.Table;
			SqlDataAdapter DA = DM.Adaptor;
			DM.Table.DefaultView.RowFilter = "KPI_No=" + KPI_No;

			DataRow DR = null;
			if (DT.DefaultView.Count == 0) {
				DR = DT.NewRow();
				DR["PSNL_NO"] = PSNL_NO;
				DR["R_Year"] = R_Year;
				DR["R_Round"] = R_Round;
				DR["KPI_No"] = KPI_No;
			} else {
				DR = DT.DefaultView[0].Row;
			}

			DR["KPI_Job"] = txt_KPI_Job.Text;
			DR["KPI_Target_Text"] = txt_KPI_Target.Text;
			DR["KPI_Choice_1"] = txt_KPI_Choice1.Text;
			DR["KPI_Choice_2"] = txt_KPI_Choice2.Text;
			DR["KPI_Choice_3"] = txt_KPI_Choice3.Text;
			DR["KPI_Choice_4"] = txt_KPI_Choice4.Text;
			DR["KPI_Choice_5"] = txt_KPI_Choice5.Text;

			//------------- KPI Detail Not Need To Save Here-------------
			//DR("KPI_Target_Choice") = xxxxxxxxxxxxxxxx
			//DR("KPI_Answer_PSN") = xxxxxxxxxxxxxxxx
			//DR("KPI_Answer_MGR") = xxxxxxxxxxxxxxxx
			//DR("KPI_Remark_PSN") = xxxxxxxxxxxxxxxx
			//DR("KPI_Remark_MGR") = xxxxxxxxxxxxxxxx

			if (Information.IsNumeric(txt_KPI_Weight.Text)) {
				DR["KPI_Weight"] = Convert.ToDouble(txt_KPI_Weight.Text);
			} else {
				DR["KPI_Weight"] = DBNull.Value;
			}

			if (DT.DefaultView.Count == 0)
				DT.Rows.Add(DR);
			SqlCommandBuilder cmd = new SqlCommandBuilder(DA);
			DA.Update(DT);

			BindMasterKPI();
			divEdit.Visible = false;
			divView.Visible = true;

		}


		private void SaveHeader()
		{
			//------------- Check Round ---------------
			if (R_Year == 0 | R_Round == 0)
				return;
			//------------- Check Progress-------------
			if (KPI_Status > HRBL.AssessmentStatus.CreatedApproved)
				return;

			string SQL = "";
			SQL = " SELECT * \n";
			SQL += "  FROM tb_HR_KPI_Header\n";
			SQL += " WHERE PSNL_NO='" + PSNL_NO.Replace("'", "''") + "' AND R_Year=" + R_Year + " AND R_Round=" + R_Round;
			SqlDataAdapter DA = new SqlDataAdapter(SQL, BL.ConnectionString());
			DataTable DT = new DataTable();
			DA.Fill(DT);

			DataRow DR = null;
			if (DT.Rows.Count == 0) {
				DR = DT.NewRow();
				DR["PSNL_NO"] = PSNL_NO;
				DR["R_Year"] = R_Year;
				DR["R_Round"] = R_Round;
				DR["Create_By"] = Session["USER_PSNL_NO"];
				DR["Create_Time"] = DateAndTime.Now;
				DR["KPI_Status"] = 0;
			} else {
				DR = DT.Rows[0];
			}

			//------------- Round Detail------------- 
			SQL = "SELECT * FROM tb_HR_Round ";
			SQL += " WHERE R_Year=" + R_Year + " AND R_Round=" + R_Round;
			DataTable PN = new DataTable();
			SqlDataAdapter PA = new SqlDataAdapter(SQL, BL.ConnectionString());
			PA.Fill(PN);
			DR["R_Start"] = PN.Rows[0]["R_Start"];
			DR["R_End"] = PN.Rows[0]["R_End"];
			DR["R_Remark"] = "";

            //============ป้องกันการ update ตำแหน่ง ณ สิ้นรอบประเมินและปิดรอบประเมิน======

            Boolean IsInPeriod = BL.IsTimeInPeriod(DateTime.Now, R_Year, R_Round);
            Boolean IsRoundCompleted = BL.Is_Round_Completed(R_Year, R_Round);
            Boolean ckUpdate = IsInPeriod & !IsRoundCompleted;
            if (ckUpdate | DT.Rows.Count == 0 )

            {
				//------------- Personal Detail---------
				HRBL.PersonalInfo PSNInfo = BL.GetAssessmentPersonalInfo(R_Year, R_Round, PSNL_NO, KPI_Status);
				// Replace With New Updated Data If Exists
				if (!string.IsNullOrEmpty(PSNInfo.PSNL_No)) {
					if (!string.IsNullOrEmpty(PSNInfo.PSNL_No))
						DR["PSNL_No"] = PSNInfo.PSNL_No;
					else
						DR["PSNL_No"] = DBNull.Value;
					if (!string.IsNullOrEmpty(PSNInfo.PSNL_Fullname))
						DR["PSNL_Fullname"] = PSNInfo.PSNL_Fullname;
					else
						DR["PSNL_Fullname"] = DBNull.Value;
					if (!string.IsNullOrEmpty(PSNInfo.PNPS_CLASS))
						DR["PNPS_CLASS"] = PSNInfo.PNPS_CLASS;
					else
						DR["PNPS_CLASS"] = DBNull.Value;
					if (!string.IsNullOrEmpty(PSNInfo.PSNL_TYPE))
						DR["PSNL_TYPE"] = PSNInfo.PSNL_TYPE;
					else
						DR["PSNL_TYPE"] = DBNull.Value;
					if (!string.IsNullOrEmpty(PSNInfo.POS_NO))
						DR["POS_NO"] = PSNInfo.POS_NO;
					else
						DR["POS_NO"] = DBNull.Value;
					if (!string.IsNullOrEmpty(PSNInfo.WAGE_TYPE))
						DR["WAGE_TYPE"] = PSNInfo.WAGE_TYPE;
					else
						DR["WAGE_TYPE"] = DBNull.Value;
					if (!string.IsNullOrEmpty(PSNInfo.WAGE_NAME))
						DR["WAGE_NAME"] = PSNInfo.WAGE_NAME;
					else
						DR["WAGE_NAME"] = DBNull.Value;
					if (!string.IsNullOrEmpty(PSNInfo.SECTOR_CODE))
						DR["SECTOR_CODE"] = Strings.Left(PSNInfo.SECTOR_CODE, 2);
					else
						DR["SECTOR_CODE"] = DBNull.Value;
					if (!string.IsNullOrEmpty(PSNInfo.DEPT_CODE))
						DR["DEPT_CODE"] = PSNInfo.DEPT_CODE;
					else
						DR["DEPT_CODE"] = DBNull.Value;
					if (!string.IsNullOrEmpty(PSNInfo.MINOR_CODE))
						DR["MINOR_CODE"] = PSNInfo.MINOR_CODE;
					else
						DR["MINOR_CODE"] = DBNull.Value;
					if (!string.IsNullOrEmpty(PSNInfo.SECTOR_NAME))
						DR["SECTOR_NAME"] = PSNInfo.SECTOR_NAME;
					else
						DR["SECTOR_NAME"] = DBNull.Value;
					if (!string.IsNullOrEmpty(PSNInfo.DEPT_NAME))
						DR["DEPT_NAME"] = PSNInfo.DEPT_NAME;
					else
						DR["DEPT_NAME"] = DBNull.Value;
					if (!string.IsNullOrEmpty(PSNInfo.FN_ID))
						DR["FN_ID"] = PSNInfo.FN_ID;
					else
						DR["FN_ID"] = DBNull.Value;
					if (!string.IsNullOrEmpty(PSNInfo.FLD_Name))
						DR["FLD_Name"] = PSNInfo.FLD_Name;
					else
						DR["FLD_Name"] = DBNull.Value;
					if (!string.IsNullOrEmpty(PSNInfo.FN_CODE))
						DR["FN_CODE"] = PSNInfo.FN_CODE;
					else
						DR["FN_CODE"] = DBNull.Value;
					if (!string.IsNullOrEmpty(PSNInfo.FN_NAME))
						DR["FN_NAME"] = PSNInfo.FN_NAME;
					else
						DR["FN_NAME"] = DBNull.Value;
					if (!string.IsNullOrEmpty(PSNInfo.MGR_CODE))
						DR["MGR_CODE"] = PSNInfo.MGR_CODE;
					else
						DR["MGR_CODE"] = DBNull.Value;
					if (!string.IsNullOrEmpty(PSNInfo.MGR_NAME))
						DR["MGR_NAME"] = PSNInfo.MGR_NAME;
					else
						DR["MGR_NAME"] = DBNull.Value;
				}
			//------------- Assign Assessor -----------------------------
			DR["Create_Commit_By"] = ASSESSOR_BY;
			DR["Create_Commit_Name"] = ASSESSOR_NAME;
			DR["Create_Commit_DEPT"] = ASSESSOR_DEPT;
			DR["Create_Commit_POS"] = ASSESSOR_POS;
			}
			DR["Update_By"] = Session["USER_PSNL_NO"];
			DR["Update_Time"] = DateAndTime.Now;

			if (DT.Rows.Count == 0)
				DT.Rows.Add(DR);
			SqlCommandBuilder cmd = new SqlCommandBuilder(DA);
			DA.Update(DT);

			//----------------- ทำสองที่ Page Load กับ Update Status ---------------
            BL.Update_KPI_Status_To_Assessment_Period(PSNL_NO, R_Year, R_Round);
            
			//----------- Prevent Save----------
			if (KPI_Status > HRBL.AssessmentStatus.WaitCreatingApproved | BL.Is_Round_Completed(R_Year, R_Round)) {
				return;
			}

			DataTable TMP = BL.GetAssessorList(R_Year, R_Round, PSNL_NO);
			TMP.DefaultView.RowFilter = "MGR_PSNL_NO='" + ASSESSOR_BY.Replace("'", "''") + "'";

			//----------------- Update Information For History Assessor Structure ------------------
			SQL = "SELECT * ";
			SQL += " FROM tb_HR_Actual_Assessor ";
			SQL += " WHERE PSNL_NO='" + PSNL_NO + "' AND R_Year=" + R_Year + " AND R_Round=" + R_Round;
			DA = new SqlDataAdapter(SQL, BL.ConnectionString());
			DT = new DataTable();
			DA.Fill(DT);

			if (DT.Rows.Count > 0) {
				DT.Rows[0].Delete();
				cmd = new SqlCommandBuilder(DA);
				DA.Update(DT);
			}

			if (TMP.DefaultView.Count > 0) {
				DR = DT.NewRow();
				DR["PSNL_NO"] = PSNL_NO;
				DR["R_Year"] = R_Year;
				DR["R_Round"] = R_Round;
				DR["MGR_ASSESSOR_POS"] = TMP.DefaultView[0]["ASSESSOR_POS"];
				DR["MGR_PSNL_NO"] = TMP.DefaultView[0]["MGR_PSNL_NO"];
				DR["MGR_PSNL_Fullname"] = TMP.DefaultView[0]["MGR_PSNL_Fullname"];
				DR["MGR_PNPS_CLASS"] = TMP.DefaultView[0]["MGR_CLASS"];
				DR["MGR_PSNL_TYPE"] = TMP.DefaultView[0]["MGR_WAGE_TYPE"];
				DR["MGR_POS_NO"] = TMP.DefaultView[0]["MGR_POS_NO"];
				DR["MGR_WAGE_TYPE"] = TMP.DefaultView[0]["MGR_WAGE_TYPE"];
				DR["MGR_WAGE_NAME"] = TMP.DefaultView[0]["MGR_WAGE_NAME"];
				DR["MGR_DEPT_CODE"] = TMP.DefaultView[0]["MGR_DEPT_CODE"];
				//DR("MGR_MINOR_CODE") = TMP.DefaultView(0).Item("MGR_MINOR_CODE")
				DR["MGR_SECTOR_CODE"] = Strings.Left(TMP.DefaultView[0]["MGR_SECTOR_CODE"].ToString (), 2);
				DR["MGR_SECTOR_NAME"] = TMP.DefaultView[0]["MGR_SECTOR_NAME"];
				DR["MGR_DEPT_NAME"] = TMP.DefaultView[0]["MGR_DEPT_NAME"];
				//DR("MGR_FN_ID") = TMP.DefaultView(0).Item("MGR_FN_ID")
				//DR("MGR_FLD_Name") = TMP.DefaultView(0).Item("MGR_FLD_Name")
				DR["MGR_FN_CODE"] = TMP.DefaultView[0]["MGR_FN_CODE"];
				//DR("MGR_FN_TYPE") = TMP.DefaultView(0).Item("MGR_FN_TYPE")
				DR["MGR_FN_NAME"] = TMP.DefaultView[0]["MGR_FN_NAME"];
				if (!GL.IsEqualNull(TMP.DefaultView[0]["MGR_MGR_CODE"])) {
					DR["MGR_MGR_CODE"] = TMP.DefaultView[0]["MGR_MGR_CODE"];
				}
				if (!GL.IsEqualNull(TMP.DefaultView[0]["MGR_MGR_NAME"])) {
					DR["MGR_MGR_NAME"] = TMP.DefaultView[0]["MGR_MGR_NAME"];
				}
				try {
					 DR["MGR_PNPS_RETIRE_DATE"] = (BL.GetPSNRetireDate(TMP.DefaultView[0]["MGR_PSNL_NO"].ToString()));
				} catch {
				}
				DR["Update_By"] = Session["USER_PSNL_NO"];
				DR["Update_Time"] = DateAndTime.Now;
				DT.Rows.Add(DR);
				cmd = new SqlCommandBuilder(DA);
				DA.Update(DT);
			}

			//----------------- Update Information For History PSN Structure ------------------
			DataTable PSN = new DataTable();
			SQL = "SELECT * FROM vw_PN_PSNL WHERE PSNL_NO='" + PSNL_NO.Replace("'", "''") + "'";
			DA = new SqlDataAdapter(SQL, BL.ConnectionString());
			DA.Fill(PSN);

			SQL = "SELECT * ";
			SQL += " FROM tb_HR_Assessment_PSN ";
			SQL += " WHERE PSNL_NO='" + PSNL_NO.Replace("'", "''") + "' AND R_Year=" + R_Year + " AND R_Round=" + R_Round;
			DA = new SqlDataAdapter(SQL, BL.ConnectionString());
			DT = new DataTable();
			DA.Fill(DT);


			if (PSN.Rows.Count > 0) {
				if (DT.Rows.Count == 0) {
					DR = DT.NewRow();
					DR["PSNL_NO"] = PSNL_NO;
					DR["R_Year"] = R_Year;
					DR["R_Round"] = R_Round;
					if (TMP.DefaultView.Count > 0) {
						try {
							DR["PSN_ASSESSOR_POS"] = TMP.DefaultView[0]["ASSESSOR_POS"];
						} catch {
						}
					}
					DR["PSN_PSNL_NO"] = PSNL_NO;
					DR["PSN_PSNL_Fullname"] = PSN.Rows[0]["PSNL_Fullname"].ToString();
					DT.Rows.Add(DR);
				} else {
					DR = DT.Rows[0];
				}

                //============ป้องกันการ update ตำแหน่ง ณ สิ้นรอบประเมินและปิดรอบประเมิน======
                if (ckUpdate | DT.Rows.Count == 0 | string.IsNullOrEmpty(DT.Rows[0]["PSN_PNPS_CLASS"].ToString()))

                {
					DR["PSN_PNPS_CLASS"] = PSN.Rows[0]["PNPS_CLASS"].ToString();
					DR["PSN_PSNL_TYPE"] = PSN.Rows[0]["PSNL_TYPE"].ToString();
					DR["PSN_POS_NO"] = PSN.Rows[0]["POS_NO"].ToString();
					DR["PSN_WAGE_TYPE"] = PSN.Rows[0]["WAGE_TYPE"].ToString();
					DR["PSN_WAGE_NAME"] = PSN.Rows[0]["WAGE_NAME"].ToString();
					DR["PSN_DEPT_CODE"] = PSN.Rows[0]["DEPT_CODE"].ToString();
					DR["PSN_MINOR_CODE"] = PSN.Rows[0]["MINOR_CODE"].ToString();
					DR["PSN_SECTOR_CODE"] = Strings.Left(PSN.Rows[0]["SECTOR_CODE"].ToString(), 2);
					DR["PSN_SECTOR_NAME"] = PSN.Rows[0]["SECTOR_NAME"].ToString();
					DR["PSN_DEPT_NAME"] = PSN.Rows[0]["DEPT_NAME"].ToString();
					DR["PSN_FN_ID"] = PSN.Rows[0]["FN_ID"].ToString();
					DR["PSN_FLD_Name"] = PSN.Rows[0]["FLD_Name"].ToString();
					DR["PSN_FN_CODE"] = PSN.Rows[0]["FN_CODE"].ToString();
					DR["PSN_FN_TYPE"] = PSN.Rows[0]["FN_TYPE"].ToString();
					DR["PSN_FN_NAME"] = PSN.Rows[0]["FN_NAME"].ToString();
					if (!GL.IsEqualNull(PSN.Rows[0]["MGR_CODE"])) {
						DR["PSN_MGR_CODE"] = PSN.Rows[0]["MGR_CODE"];
					}
					if (!GL.IsEqualNull(PSN.Rows[0]["MGR_NAME"])) {
						DR["PSN_MGR_NAME"] = PSN.Rows[0]["MGR_NAME"];
					}
				}

				try {
					DR["PSN_PNPS_RETIRE_DATE"] = BL.GetPSNRetireDate(PSNL_NO);
				} catch {
				}
				DR["Update_By"] = Session["USER_PSNL_NO"];
				DR["Update_Time"] = DateAndTime.Now;
				cmd = new SqlCommandBuilder(DA);
				DA.Update(DT);

			}
		}


		protected void btnCancel_Click(object sender, System.EventArgs e)
		{
			BindPersonalList();
			pnlList.Visible = true;
			pnlEdit.Visible = false;

		}

		protected void btnPreSend_Click(object sender, System.EventArgs e)
		{
			DataTable DT = BL.GetKPIDetail(R_Year, R_Round, PSNL_NO).Table;
			if (DT.Rows.Count == 0) {
				ScriptManager.RegisterStartupScript(this.Page, typeof(string), "Alert", "showAlert('กรอกแบบประเมินก่อนส่ง');", true);
				return;
			}
			//DT.DefaultView.RowFilter = "KPI_Choice_1='' OR KPI_Choice_2='' OR KPI_Choice_3='' OR KPI_Choice_4='' OR KPI_Choice_5='' OR KPI_Job ='' OR KPI_Target_Text=''"
			//If DT.DefaultView.Count > 0 Then
			//    ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "showAlert('กรอกแบบประเมินให้สมบูรณ์');", True)
			//    Exit Sub
			//End If
			//DT.DefaultView.RowFilter = "KPI_Target_Choice IS NULL OR KPI_Target_Choice<=0 OR KPI_Target_Choice>5"
			//If DT.DefaultView.Count > 0 Then
			//    ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "showAlert('คลิกเพื่อตั้งเป้าหมายให้สมบูรณ์');", True)
			//    Exit Sub
			//End If
			DT.DefaultView.RowFilter = "KPI_Weight IS NULL ";
			//OR KPI_Weight<=0"
			if (DT.DefaultView.Count > 0) {
				ScriptManager.RegisterStartupScript(this.Page, typeof(string), "Alert", "showAlert('ตั้งน้ำหนักให้ครบ');", true);
				return;
			}
			object TotalWeight = DT.Compute("SUM(KPI_Weight)", "");
			if (GL.IsEqualNull(TotalWeight) || GL.CDBL ( TotalWeight) != 100) {
				ScriptManager.RegisterStartupScript(this.Page, typeof(string), "Alert", "showAlert('ตั้งน้ำหนักการประเมินรวมให้ครบ 100%');", true);
				return;
			}

			ScriptManager.RegisterStartupScript(this.Page, typeof(string), "Alert", "if(confirm('ยืนยันอนุมัติแบบประเมิน ??\\nหลังจากอนุมัติแบบประเมินแล้วคุณไม่สามารถแก้ไขได้')) document.getElementById('" + btnSend.ClientID + "').click();", true);
		}

		protected void btnSend_Click(object sender, System.EventArgs e)
		{
			//--------------- Check Something Incomplete ----------------
			DataTable DT = BL.GetKPIDetail(R_Year, R_Round, PSNL_NO).Table;

			//--------------------Saving--------------------
			string SQL = "";
			SQL = " SELECT * \n";
			SQL += "  FROM tb_HR_KPI_Header\n";
			SQL += " WHERE PSNL_NO='" + PSNL_NO.Replace("'", "''") + "' AND R_Year=" + R_Year + " AND R_Round=" + R_Round;
			SqlDataAdapter DA = new SqlDataAdapter(SQL, BL.ConnectionString());
			DT = new DataTable();
			DA.Fill(DT);

			DataRow DR = DT.Rows[0];
            HRBL.PersonalInfo PSNInfo = BL.GetAssessmentPersonalInfo(R_Year, R_Round, Session["USER_PSNL_NO"].ToString (), KPI_Status);
			DR["KPI_Status"] = HRBL.AssessmentStatus.CreatedApproved;
			DR["Update_By"] = Session["USER_PSNL_NO"];
			DR["Update_Time"] = DateAndTime.Now;

			DR["Create_Commit"] = true;
			DR["Create_Commit_By"] = Session["USER_PSNL_NO"];
			DR["Create_Commit_Name"] = PSNInfo.PSNL_Fullname;
			DR["Create_Commit_DEPT"] = PSNInfo.DEPT_NAME;
			if (!string.IsNullOrEmpty(PSNInfo.MGR_NAME)) {
				DR["Create_Commit_POS"] = PSNInfo.MGR_NAME;
			} else {
				DR["Create_Commit_POS"] = PSNInfo.FN_NAME;
			}
			DR["Create_Commit_Time"] = DateAndTime.Now;

			if (DT.Rows.Count == 0)
				DT.Rows.Add(DR);
			SqlCommandBuilder cmd = new SqlCommandBuilder(DA);
			DA.Update(DT);

			//----------------- ทำสองที่ Page Load กับ Update Status ---------------
			BL.Update_KPI_Status_To_Assessment_Period(PSNL_NO, R_Year, R_Round);

			// ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "showAlert('อนุมัติแบบประเมินสำเร็จ');", True)
			BindMasterKPI();

		}

		#region "copy-paste"

		protected void lnkCopyAll_Click(object sender, System.EventArgs e)
		{
			Session["copyAll_KPI"] = "";
			//--------------- Bind Detail----------------------
			HRBL.DataManager DM = BL.GetKPIDetail(R_Year, R_Round, PSNL_NO);
			DataTable DT = DM.Table;
			SqlDataAdapter DA = DM.Adaptor;
			Session["copyAll_KPI"] = DT;
		}

		protected void lnkPasteAll_Click(object sender, System.EventArgs e)
		{
			DataTable DT_Session =(DataTable) Session["copyAll_KPI"];

			if (DT_Session.Rows.Count == 0) {
			} else {
				//---------------Delete in table before Overwrite ----------------------
				HRBL.DataManager DM_Overwrite = BL.OverwriteKPIDetail(R_Year, R_Round, PSNL_NO);
				DataTable DT_Overwrite = DM_Overwrite.Table;
				SqlDataAdapter DA_Overwrite = DM_Overwrite.Adaptor;

				//------------------ Save Detail-----------------
				HRBL.DataManager DM = BL.GetKPIDetail(R_Year, R_Round, PSNL_NO);
				DataTable DT = DM.Table;
				SqlDataAdapter DA = DM.Adaptor;
				DataRow DR = null;
				if (DT.Rows.Count == 0) {

					for (int i = 0; i <= DT_Session.Rows.Count - 1; i++) {
						DR = DT.NewRow();
						DR["PSNL_NO"] = PSNL_NO;
						DR["R_Year"] = R_Year;
						DR["R_Round"] = R_Round;
						DR["KPI_No"] = DT_Session.Rows[i]["KPI_No"];
						DR["KPI_Job"] = DT_Session.Rows[i]["KPI_Job"];
						DR["KPI_Target_Text"] = DT_Session.Rows[i]["KPI_Target_Text"];
						DR["KPI_Choice_1"] = DT_Session.Rows[i]["KPI_Choice_1"];
						DR["KPI_Choice_2"] = DT_Session.Rows[i]["KPI_Choice_2"];
						DR["KPI_Choice_3"] = DT_Session.Rows[i]["KPI_Choice_3"];
						DR["KPI_Choice_4"] = DT_Session.Rows[i]["KPI_Choice_4"];
						DR["KPI_Choice_5"] = DT_Session.Rows[i]["KPI_Choice_5"];

						if (Information.IsNumeric(DT_Session.Rows[i]["KPI_Weight"])) {
							DR["KPI_Weight"] = DT_Session.Rows[i]["KPI_Weight"];
						} else {
							DR["KPI_Weight"] = DBNull.Value;
						}

						DT.Rows.Add(DR);
					}
				}

				SqlCommandBuilder cmd = new SqlCommandBuilder(DA);
				DA.Update(DT);

				BindMasterKPI();
				//divEdit.Visible = False
				//divView.Visible = True

			}
		}


		#endregion

		protected void ddlForm_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if (ddlForm.SelectedIndex != 1) {
				Response.Redirect("Assessment_Team_KPI_Assessment.aspx");
			}
		}
		public Assessment_Team_KPI_Create()
		{
			Load += Page_Load;
		}
	}
}

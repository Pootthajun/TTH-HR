﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.DirectoryServices;
using System.DirectoryServices.AccountManagement;
using System.DirectoryServices.Protocols;
using System.Net;

namespace VB
{
    

    public partial class TestLogin : System.Web.UI.Page
    {

        public HRBL BL=new HRBL();

        protected void Page_Load(object sender, EventArgs e)
        {

            lblResult.Text = "";
        }

        protected void btnGetDomain_Click(object sender, EventArgs e)
        {
            txtDomain.Text=BL.SYSTEM_DOMAIN();
        }


        protected void btnGetLDAP_Click(object sender, EventArgs e)
        {
            txtLDAP.Text = BL.LDAP_PATH();
        }

        protected void btnGetLDAPProtocol_Click(object sender, EventArgs e)
        {
            txtLDAPProtocol.Text = BL.LDAP_PATH();
        }

        public void AuthenticateUser_New(string Domain, string Username, string Password)
        {
            try
            {
                using (PrincipalContext pc = new PrincipalContext(ContextType.Domain, Domain))
                {
                    // validate the credentials
                    bool isValid = pc.ValidateCredentials(Username, Password);
                    if (isValid)
                    {
                        lblResult.Text = "Success";
                    }
                    else
                    {
                        lblResult.Text = "inValid";
                    }
                }
            }
            catch (Exception ex)
            {
                lblResult.Text ="Error : " + ex.Message;
            }
            
        }


        public void AuthenticateUser_Old(string Domain, string Username, string Password, string LDAP_Path)
        {

            
            string domainAndUsername = Domain + "\\" + Username;
            DirectoryEntry entry = new DirectoryEntry(LDAP_Path, domainAndUsername, Password);
            entry.AuthenticationType = AuthenticationTypes.Secure;
            try
            {
                DirectorySearcher search = new DirectorySearcher(entry);

                search.Filter = "(SAMAccountName=" + Username + ")";

                search.PropertiesToLoad.Add("cn");

                SearchResult result = search.FindOne();

                if (result == null)
                {
                    lblResult.Text = "Step1: User Not Found";
                    return;
                }
                LDAP_Path = result.Path;
                string _filterAttribute = (String)result.Properties["cn"][0];
            }
            catch (Exception ex)
            {
                lblResult.Text = "Step1: " + ex.Message;
                return;
            }
            lblResult.Text = "Step2: Success";
            return;

        }

        public void AuthenticateUser_LDAP(string Username, string Password, string LDAP_Path)
        {
            try
            {
                LdapConnection connection = new LdapConnection(LDAP_Path);
                NetworkCredential credential = new NetworkCredential(Username, Password);
                connection.Credential = credential;
                connection.Bind();
                lblResult.Text = "Success";
            }
            catch (Exception exc)
            {
                lblResult.Text ="Error : " + exc.Message;
            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
           
            if (rdoNew.Checked)
            {
                AuthenticateUser_New(txtDomain.Text, txtUser.Text, txtPass.Text);               
            }
            else if (rdoOld.Checked)
            {
                AuthenticateUser_Old(txtDomain.Text, txtUser.Text, txtPass.Text, txtLDAP.Text);
            }
            else { 
                AuthenticateUser_LDAP(txtUser.Text,txtPass.Text,txtLDAPProtocol.Text);
            }
        }


    }
}
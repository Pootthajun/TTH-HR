using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Xml.Linq;
using System.Diagnostics;
using System.Collections.Specialized;
using System.Configuration;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Caching;
using System.Web.SessionState;
using System.Web.Security;
using System.Web.Profile;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
namespace VB
{

	public partial class RPT_W_JobMapping : System.Web.UI.Page
	{


		HRBL BL = new HRBL();
		Converter C = new Converter();

		GenericLib GL = new GenericLib();
		protected void Page_Load(object sender, System.EventArgs e)
		{
			if ((Session["USER_PSNL_NO"] == null)) {
				ScriptManager.RegisterStartupScript(this.Page, typeof(string), "Alert", "showAlert('กรุณาลงชื่อเข้าใช้ระบบ');", true);
				Response.Redirect("Login.aspx");
				return;
			}


			if (!IsPostBack) {
				BL.BindDDlSector(ddlSector);
				BindJob();
			}

			SetCheckboxStyle();
		}

		private void SetCheckboxStyle()
		{
			if (chkYes.Checked) {
				chkYes.CssClass = "checked";
			} else {
				chkYes.CssClass = "";
			}
			if (chkNo.Checked) {
				chkNo.CssClass = "checked";
			} else {
				chkNo.CssClass = "";
			}
		}


        protected void btnSearch_Click(object sender, System.EventArgs e)
		{
			BindJob();
		}

		private void BindJob()
		{
			string SQL = "";

			SQL += "  SELECT DISTINCT DEPT.DEPT_CODE,DEPT.MINOR_CODE,DEPT.SECTOR_CODE,DEPT.SECTOR_NAME,DEPT.DEPT_CODE,DEPT.DEPT_NAME," + "\n";
			SQL += "  COUNT(Header.Job_ID) Total_Task,UB.Update_By,PSN.PSNL_Fullname,POS_NO,ISNULL(PSN.MGR_NAME,PSN.FN_NAME) POS_Name" + "\n";
			SQL += "  ,UT.UT Update_Time" + "\n";

			SQL += "  FROM vw_HR_Department DEPT" + "\n";
			SQL += "  LEFT JOIN tb_HR_Job_Mapping Header ON Header.PNDP_DEPT_CODE=DEPT.DEPT_CODE " + "\n";
			SQL += "\t\t\t\t\t\t\t\t\tAND Header.PNDP_MINOR_CODE=DEPT.MINOR_CODE" + "\n";
			SQL += "\t\t\t\t\t\t\t\t\tAND Job_LEVEL=1" + "\n";

			SQL += "  LEFT JOIN" + "\n";
			SQL += "  (SELECT PNDP_DEPT_CODE DEPT_CODE,PNDP_MINOR_CODE MINOR_CODE,MAX(Update_Time) UT " + "\n";
			SQL += "  FROM tb_HR_Job_Mapping WHERE Job_LEVEL=1 GROUP BY PNDP_DEPT_CODE,PNDP_MINOR_CODE) UT " + "\n";
			SQL += "\t\t\t\t\t\t\t\t\tON Header.PNDP_DEPT_CODE=UT.DEPT_CODE " + "\n";
			SQL += "\t\t\t\t\t\t\t\t\tAND Header.PNDP_MINOR_CODE=UT.MINOR_CODE" + "\n";
			SQL += "" + "\n";
			SQL += "   LEFT JOIN" + "\n";
			SQL += "   (SELECT PNDP_DEPT_CODE DEPT_CODE,PNDP_MINOR_CODE MINOR_CODE,Update_By,Update_Time" + "\n";
			SQL += "   FROM tb_HR_Job_Mapping WHERE Job_LEVEL=1) UB" + "\n";
			SQL += "\t\t\t\t\t\t\t\t\tON Header.PNDP_DEPT_CODE=UB.DEPT_CODE" + "\n";
			SQL += "\t\t\t\t\t\t\t\t\tAND Header.PNDP_MINOR_CODE=UB.MINOR_CODE" + "\n";
			SQL += "\t\t\t\t\t\t\t\t\tAND UB.Update_Time=UT.UT" + "\n";
			SQL += "\tLEFT JOIN vw_PN_PSNL PSN ON UB.Update_By=PSN.PSNL_NO" + "\n";
			SQL += "      " + "\n";

			string Title = "";
			string Filter = "";

			if (ddlSector.SelectedIndex > 0) {

				Filter += " ( LEFT(DEPT.DEPT_CODE,2)='" + ddlSector.Items[ddlSector.SelectedIndex].Value + "' " + "\n";
				Filter += " OR LEFT(DEPT.DEPT_CODE,4)='" + ddlSector.Items[ddlSector.SelectedIndex].Value + "'   " + "\n";

				if (ddlSector.Items[ddlSector.SelectedIndex].Value == "3200") {
					Filter += " AND DEPT.DEPT_CODE NOT IN ('32000300'))    AND" + "\n";
				} else if (ddlSector.Items[ddlSector.SelectedIndex].Value == "3203") {
					Filter += " OR DEPT.DEPT_CODE IN ('32000300','32030000','32030100','32030200','32030300','32030500'))  AND" + "\n";
				} else if (ddlSector.Items[ddlSector.SelectedIndex].Value == "3204") {
					Filter += " OR DEPT.DEPT_CODE IN ('32040000','32040100','32040200','32040300','32040500','32040300'))  AND" + "\n";
				} else if (ddlSector.Items[ddlSector.SelectedIndex].Value == "3205") {
					Filter += " OR DEPT.DEPT_CODE IN ('32050000','32050100','32050200','32050300','32050500'))  AND" + "\n";
				} else {
					Filter += ")   AND" + "\n";
				}

				Title += GL.SplitString(ddlSector.Items[ddlSector.SelectedIndex].Text, ":")[1].Trim() + " ";
			}
			if (!string.IsNullOrEmpty(txtDeptName.Text)) {
				Filter += "(DEPT.DEPT_NAME LIKE '" + txtDeptName.Text.Replace("'", "''") + "' OR DEPT.DEPT_Code LIKE '" + txtDeptName.Text.Replace("'", "''") + "') AND ";
				Title += " หน่วยงาน : '" + txtDeptName.Text + "'";
			}

			if (!string.IsNullOrEmpty(Filter)) {
				SQL += " WHERE " + Filter.Substring(0, Filter.Length - 4) + "\n";
			}

			SQL += "  GROUP BY DEPT.DEPT_CODE,DEPT.MINOR_CODE,DEPT.SECTOR_CODE,DEPT.SECTOR_NAME,DEPT.DEPT_CODE,DEPT.DEPT_NAME,UB.Update_By,UT.UT," + "\n";
			SQL += "  PSN.PSNL_Fullname,POS_NO,PSN.MGR_NAME,PSN.FN_NAME" + "\n";
			SQL += "  " + "\n";

			if (chkYes.Checked & !chkNo.Checked) {
				SQL += " Having COUNT(Header.Job_ID)>0 " + "\n";
				Title += " แสดงหน่วยงานที่ทำแผนที่งานแล้ว";
			} else if (!chkYes.Checked & chkNo.Checked) {
				SQL += " Having ISNULL(COUNT(Header.Job_ID),0)=0 " + "\n";
				Title += " แสดงหน่วยงานที่ยังไม่ทำแผนที่งาน";
			}

			SQL += "  ORDER BY DEPT.SECTOR_CODE,DEPT.DEPT_CODE" + "\n";

			SqlDataAdapter DA = new SqlDataAdapter(SQL, BL.ConnectionString());
			DataTable DT = new DataTable();
			DA.Fill(DT);

			string SubTitle = "";
			if (DT.Rows.Count == 0) {
				SubTitle = " ไม่พบรายการดังกล่าว";
			} else {
				SubTitle = " พบ " + GL.StringFormatNumber(DT.Rows.Count, 0) + " รายการ";
			}
			lblTitle.Text = Title.Replace("\n", "<br>") + "<br>" + SubTitle;

			Session["RPT_W_JobMapping"] = DT;
			//-----------------
			Session["RPT_W_JobMapping_Title"] = Title;
			//-----------------
			Session["RPT_W_JobMapping_SubTitle"] = "รายงาน ณ วันที่ " + DateTime.Now.Day + " " + C.ToMonthNameTH(DateTime.Now.Month) + " พ.ศ." + (DateTime.Now.Year + 543);
			Session["RPT_W_JobMapping_SubTitle"] += SubTitle;


			Pager.SesssionSourceName = "RPT_W_JobMapping";
			Pager.RenderLayout();

		}

		protected void Pager_PageChanging(PageNavigation Sender)
		{
			Pager.TheRepeater = rptJob;
		}

		string LastSectorName = "";
		protected void rptJob_ItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
		{
			if (e.Item.ItemType != ListItemType.AlternatingItem & e.Item.ItemType != ListItemType.Item)
				return;

            DataRowView drv = (DataRowView)e.Item.DataItem;

            Label lblSector = (Label)e.Item.FindControl("lblSector");
            Label lblPSNOrganize = (Label)e.Item.FindControl("lblPSNOrganize");
            Label lblTask = (Label)e.Item.FindControl("lblTask");
            Label lblMGR = (Label)e.Item.FindControl("lblMGR");
            Label lblMGRPos = (Label)e.Item.FindControl("lblMGRPos");
            Label lblUpdate = (Label)e.Item.FindControl("lblUpdate");

            HtmlTableCell tdSector = (HtmlTableCell)e.Item.FindControl("tdSector");

        
			if (drv["SECTOR_NAME"].ToString() != LastSectorName) {
				lblSector.Text = drv["SECTOR_NAME"].ToString();
				LastSectorName = drv["SECTOR_NAME"].ToString();
			} else {
				tdSector.Style["border-top"] = "none";
			}
			if (drv["DEPT_NAME"].ToString() != drv["SECTOR_NAME"].ToString()) {
				lblPSNOrganize.Text = drv["DEPT_NAME"].ToString().Replace(drv["SECTOR_NAME"].ToString(), "").Trim();
			} else {
				lblPSNOrganize.Text = drv["DEPT_NAME"].ToString();
			}
			if (GL.CINT(drv["Total_Task"]) > 0) {
				lblTask.Text = drv["Total_Task"].ToString();
			}
			if (!GL.IsEqualNull(drv["Update_By"])) {
                lblMGR.Text = drv["Update_By"].ToString() + " : " + drv["PSNL_Fullname"].ToString();
			}
			lblMGRPos.Text = drv["POS_Name"].ToString();
			if (!GL.IsEqualNull(drv["Update_Time"])) {
                DateTime TheDate = Convert.ToDateTime(drv["Update_Time"]);
				lblUpdate.Text = GL.ReportThaiDateTime(TheDate);
			}
		}
		public RPT_W_JobMapping()
		{
			Load += Page_Load;
		}
	}
}

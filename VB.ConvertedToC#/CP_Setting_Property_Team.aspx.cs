using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Xml.Linq;
using System.Diagnostics;
using System.Collections.Specialized;
using System.Configuration;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Caching;
using System.Web.SessionState;
using System.Web.Security;
using System.Web.Profile;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.IO;
using System.Drawing;
using AjaxControlToolkit;
namespace VB
{

	public partial class CP_Setting_Property_Team : System.Web.UI.Page
	{


		HRBL BL = new HRBL();
		GenericLib GL = new GenericLib();

		textControlLib CL = new textControlLib();

		protected void Page_Load(object sender, System.EventArgs e)
		{
			if ((Session["USER_PSNL_NO"] == null)) {
				ScriptManager.RegisterStartupScript(this.Page, typeof(string), "Alert", "showAlert('กรุณาลงชื่อเข้าใช้ระบบ');", true);
				Response.Redirect("Login.aspx");
				return;
			}
			if (!IsPostBack) {
				if (!string.IsNullOrEmpty(Request.QueryString["POS_NO"])) {
					PnlList.Visible = false;
					pnlEdit.Visible = true;

					ModalTraining.Visible = false;
					ModalTest.Visible = false;
					ModalPost.Visible = false;
					POS_NO = Request.QueryString["POS_NO"].ToString();
					BL.BindDDlSector(ddlSector);
					CurrentPos();
					return;
				}
				BL.BindDDlSector(ddlSector);
				BindPosition();
				ClearForm();

			}

		}

		public string POS_NO {
			get { return lblPos_Head.Attributes["POS_NO"]; }
			set { lblPos_Head.Attributes["POS_NO"] = value; }
		}

		public string POS_NO_Detail {
			get { return lblPos_Head.Attributes["POS_NO_Detail"]; }
			set { lblPos_Head.Attributes["POS_NO_Detail"] = value; }
		}
		public string SECTOR_CODE {
			get { return lblPSN_Head.Attributes["SECTOR_CODE"]; }
			set { lblPSN_Head.Attributes["SECTOR_CODE"] = value; }
		}
		public string DEPT_CODE {
			get { return lblPSN_Head.Attributes["DEPT_CODE"]; }
			set { lblPSN_Head.Attributes["DEPT_CODE"] = value; }
		}
		public string EXIST_Training {
			get { return lblTrainingNo.Attributes["EXIST_Training"]; }
			set { lblTrainingNo.Attributes["EXIST_Training"] = value; }
		}

		public string EXIST_Test {
			get { return lblTestNo.Attributes["EXIST_Test"]; }
			set { lblTestNo.Attributes["EXIST_Test"] = value; }
		}

		public string EXIST_Pos {
			get { return lblPosCurrent.Attributes["EXIST_Pos"]; }
			set { lblPosCurrent.Attributes["EXIST_Pos"] = value; }
		}

		public string Mode_Pos {
			get { return lblModePos.Attributes["Mode_Pos"]; }
			set { lblModePos.Attributes["Mode_Pos"] = value; }
		}


		private void ClearForm()
		{
			CL.ImplementJavaMoneyText(txtAss_Min_Score);
			CL.ImplementJavaFloatText(txtLeave_Score);
			PnlList.Visible = true;
			pnlEdit.Visible = false;
			ModalTraining.Visible = false;
			ModalTest.Visible = false;
			ModalPost.Visible = false;
			txtAss_Min_Score.Text = "";
			rptTraining = null;
			txtLeave_Score.Text = "";
			txtArrive_Late.Text = "";
			rptTest = null;
			chkPunishment.Checked = false;
			EXIST_Pos = "";
			lblModePos.Text = "";
			lblPSN_Head.Text = "";

		}


		private void BindPosition()
		{
			string SQL = "";

			SQL += " SELECT DISTINCT\n";
			SQL += " MGR_PSNL_NO, MGR_PSNL_Fullname\n";
			SQL += " ,PSN_SECTOR_CODE  SECTOR_CODE,PSN_SECTOR_NAME  SECTOR_NAME\n";
			SQL += " ,PSN_DEPT_CODE  DEPT_CODE,PSN_DEPT_NAME DEPT_NAME\n";
			SQL += " ,PSN_POS_NO  POS_NO,ISNULL(PSN_MGR_NAME,'-') MGR_NAME,ISNULL(PSN_FN_NAME,'-') FLD_NAME\n";
			SQL += " ,PSN_PNPS_CLASS PNPO_CLASS ,PSN_PSNL_NO PSNL_NO  ,PSN_PSNL_Fullname PSNL_Fullname\n";
			SQL += " ,Setting_Status.Status_Path\n";
			SQL += " --,POS.Update_Time   \n";
			SQL += " FROM  vw_HR_ASSESSOR_PSN PSN\n";
			SQL += " INNER JOIN vw_Path_Setting_Status Setting_Status ON Setting_Status.POS_No = PSN.PSN_POS_NO\n";
			SQL += " WHERE MGR_PSNL_NO ='" + Session["USER_PSNL_NO"].ToString().Replace("'", "''") + "'\n";



			if (ddlSector.SelectedIndex > 0) {

				SQL += " AND  ( LEFT(PSN_DEPT_CODE,2)='" + ddlSector.Items[ddlSector.SelectedIndex].Value + "' \n";
				SQL += " OR LEFT(PSN_DEPT_CODE,4)='" + ddlSector.Items[ddlSector.SelectedIndex].Value + "'   \n";

				if (ddlSector.Items[ddlSector.SelectedIndex].Value == "3200") {
					SQL += " AND  PSN_DEPT_CODE NOT IN ('32000300'))    \n";
				//-----------ฝ่ายโรงงานผลิตยาสูบ 3
				} else if (ddlSector.Items[ddlSector.SelectedIndex].Value == "3203") {
					SQL += " OR  PSN_DEPT_CODE IN ('32000300','32030000','32030100','32030200','32030300','32030500'))  \n";
				//-----------ฝ่ายโรงงานผลิตยาสูบ 4
				} else if (ddlSector.Items[ddlSector.SelectedIndex].Value == "3204") {
					SQL += " OR  PSN_DEPT_CODE IN ('32040000','32040100','32040200','32040300','32040500','32040300'))  \n";
				//-----------ฝ่ายโรงงานผลิตยาสูบ 5
				} else if (ddlSector.Items[ddlSector.SelectedIndex].Value == "3205") {
					SQL += " OR  PSN_DEPT_CODE IN ('32050000','32050100','32050200','32050300','32050500'))\t  \n";
				} else {
					SQL += ")   \n";
				}


			}
			if (!string.IsNullOrEmpty(txt_Search_Organize.Text)) {
				SQL += " AND (PSN_DEPT_NAME LIKE '%" + txt_Search_Organize.Text.Replace("'", "''") + "%' ) \n";
			}
			if (!string.IsNullOrEmpty(lblPOS.Text)) {
				SQL += " AND (PSN_POS_NO LIKE '%" + lblPOS.Text.Replace("'", "''") + "%' OR \n";
				SQL += " PSN_MGR_NAME LIKE '%" + lblPOS.Text.Replace("'", "''") + "%' OR \n";
				SQL += " PSN_FN_NAME LIKE '%" + lblPOS.Text.Replace("'", "''") + "%' )\n";
			}

			switch (GL.CINT (ddlSetting.SelectedValue)) {
				case 1:
					SQL += " AND (Setting_Status.Status_Path IN (1))\n";
					break;
				case 2:
					SQL += " AND (Setting_Status.Status_Path IN (0))\n";
					break;
				default:
					break;
			}

			SQL += " ORDER BY SECTOR_CODE,DEPT_CODE,POS_NO ";
			SqlDataAdapter DA = new SqlDataAdapter(SQL, BL.ConnectionString());
			DataTable DT = new DataTable();
			DA.Fill(DT);

			Session["Setting_Position"] = DT;
			Pager.SesssionSourceName = "Setting_Position";
			Pager.RenderLayout();

			if (DT.Rows.Count == 0) {
				lblCountList.Text = "ไม่พบรายการดังกล่าว";
			} else {
				lblCountList.Text = "พบ " + GL.StringFormatNumber(DT.Rows.Count, 0) + " รายการ";
			}

		}

		protected void btnSearch_Click(object sender, System.EventArgs e)
		{
			BindPosition();
		}

		protected void Pager_PageChanging(PageNavigation Sender)
		{
			Pager.TheRepeater = rptList;
		}

		string LastSector = "";

		string LastDept = "";
		protected void rptList_ItemCommand(object source, System.Web.UI.WebControls.RepeaterCommandEventArgs e)
		{
			switch (e.CommandName) {
				case "Setting":
					PnlList.Visible = false;
					pnlEdit.Visible = true;
					Label lblClass = (Label) e.Item.FindControl("lblClass");
					Label lblPosNo =(Label) e.Item.FindControl("lblPosNo");
					Label lblMGR =(Label) e.Item.FindControl("lblMGR");
                    Label lblDept_Temp = (Label)e.Item.FindControl("lblDept_Temp");
					Label lblPSN = (Label) e.Item.FindControl("lblPSN");
                    Label lblSector_No = (Label)e.Item.FindControl("lblSector_No");
                    Label lblDept_No = (Label)e.Item.FindControl("lblDept_No");
                    Button btnSetting = (Button)e.Item.FindControl("btnSetting");
					//-------------- ดึงข้อมูลตำแหน่ง------------
					POS_NO = btnSetting.CommandArgument;
					lblPos_Head.Text = lblPosNo.Text + " : " + lblMGR.Text;
					lblDept_Head.Text = lblDept_Temp.Text;
					lblClass_Head.Text = lblClass.Text;
					if (lblPSN.Text != "-") {
						lblPSN_Head.Text = lblPSN.Text;
						lblPSN_Head.ForeColor = System.Drawing.Color.Black;
					} else {
						lblPSN_Head.Text = "ไม่มีผู้ครองตำแหน่ง";
						lblPSN_Head.ForeColor = System.Drawing.Color.Red;
					}
					//------------ค้นหาเกณฑ์ของตำแหน่ง--------------------

					//-----ผลการประเมิน--,วันลา สาย ขาด ,โทษทางวินัย------
					Path_Setting();
					//-----การฝึกอบรม----------
					BindTrainingDetail();
					//-----การทดสอบ----------
					BindTestDetail();
					//----------------------

					//-----กำหนดเส้นทาง---------
					BindPath_Position();
					//------LEFT/RIGHT----------
					BindPosLeft();
					BindPosRight();
					//-----กำหนดเส้นทางในฝ่ายเดียวกัน---------
					SECTOR_CODE = lblSector_No.Text;
					DEPT_CODE = lblDept_No.Text;
					BindPosLevel();

					break;

			}
		}
		protected void rptList_ItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
		{
			if (e.Item.ItemType != ListItemType.Item & e.Item.ItemType != ListItemType.AlternatingItem)
				return;

			Label lblClass = (Label) e.Item.FindControl("lblClass");
			Label lblPosNo =(Label) e.Item.FindControl("lblPosNo");
			Label lblFLD =(Label) e.Item.FindControl("lblFLD");
			Label lblMGR =(Label) e.Item.FindControl("lblMGR");
			Label lblSector =(Label) e.Item.FindControl("lblSector");
			Label lblDept =(Label) e.Item.FindControl("lblDept");
            Label lblDept_Temp = (Label)e.Item.FindControl("lblDept_Temp");
			Label lblPSN = (Label) e.Item.FindControl("lblPSN");
            Label lblSector_No = (Label)e.Item.FindControl("lblSector_No");
            Label lblDept_No = (Label)e.Item.FindControl("lblDept_No");
            Button btnSetting = (Button)e.Item.FindControl("btnSetting");
            HtmlAnchor img_Status = (HtmlAnchor)e.Item.FindControl("img_Status");

            DataRowView drv = (DataRowView)e.Item.DataItem;
			if (LastSector != drv["SECTOR_NAME"].ToString()) {
				lblSector.Text = drv["SECTOR_NAME"].ToString();
				LastSector = drv["SECTOR_NAME"].ToString();
				lblDept.Text = drv["DEPT_NAME"].ToString();
				LastDept = drv["DEPT_NAME"].ToString();
			} else if (LastDept != drv["DEPT_NAME"].ToString()) {
				lblDept.Text = drv["DEPT_NAME"].ToString();
				LastDept = drv["DEPT_NAME"].ToString();
			}

			lblSector_No.Text = drv["SECTOR_CODE"].ToString ();
            lblDept_No.Text = drv["DEPT_CODE"].ToString();
			lblClass.Text = GL.CINT (drv["PNPO_CLASS"]).ToString ();
			lblDept_Temp.Text = drv["DEPT_NAME"].ToString();
			lblPosNo.Text = "<B>" + drv["POS_NO"].ToString() + "</B>";
			lblFLD.Text = "<B>" + drv["FLD_NAME"].ToString() + "</B>";
			lblMGR.Text = "<B>" + drv["MGR_NAME"].ToString() + "</B>";
			lblPSN.ForeColor = System.Drawing.Color.Gray;
			if (!GL.IsEqualNull(drv["PSNL_Fullname"])) {
				lblPSN.Text = drv["PSNL_NO"] + " : " + drv["PSNL_Fullname"].ToString();
			} else {
				lblPSN.Text = "-";
			}
			if (GL.CINT (drv["Status_Path"]) == 1) {
				img_Status.InnerHtml = "<img src='images/check.png' />";
			} else {
				img_Status.InnerHtml = "<img src='images/none.png' />";
			}





			btnSetting.CommandArgument = drv["POS_NO"].ToString();
		}


		private void Path_Setting()
		{
			string SQL = "";
			SQL += " SELECT * FROM tb_Path_Setting \n";
			SQL += " WHERE POS_No=" + POS_NO + "\n";
			SqlDataAdapter DA = new SqlDataAdapter(SQL, BL.ConnectionString());
			DataTable DT = new DataTable();
			DA.Fill(DT);

			if (DT.Rows.Count == 1) {
				if (Information.IsNumeric(DT.Rows[0]["Ass_Min_Score"])) {
					txtAss_Min_Score.Text = GL.StringFormatNumber(DT.Rows[0]["Ass_Min_Score"]);
				} else {
					txtAss_Min_Score.Text = "";
				}
				if (Information.IsNumeric(DT.Rows[0]["Leave_Score"])) {
					txtLeave_Score.Text = GL.StringFormatNumber(DT.Rows[0]["Leave_Score"]);
				} else {
					txtLeave_Score.Text = "";
				}
				if (Information.IsNumeric(DT.Rows[0]["Arrive_Late"])) {
					txtArrive_Late.Text = GL.StringFormatNumber(DT.Rows[0]["Arrive_Late"]);
				} else {
					txtArrive_Late.Text = "";
				}
				if ((bool )DT.Rows[0]["Punishment"] == true) {
					chkPunishment.Checked = true;
				} else {
					chkPunishment.Checked = false;
				}
			} else {
				txtAss_Min_Score.Text = "";
				rptTraining.DataSource = null;
				txtLeave_Score.Text = "";
				txtArrive_Late.Text = "";
				rptTest.DataSource = null;
				chkPunishment.Checked = false;
			}
		}


		//-----------------------ตารางหลักสูตร--------------------
		private void BindTrainingDetail()
		{
			string SQL = "";

			SQL += "   SELECT Training.POS_No,COURSE.COURSE_ID,COURSE.COURSE_DESC \n";
			SQL += "   FROM tb_Path_Setting_Training Training\n";
			SQL += "   LEFT JOIN tb_PK_COURSE COURSE ON Training.COURSE_ID=COURSE.COURSE_ID\n";
			SQL += "   WHERE Training.POS_NO =" + POS_NO + "\n";
			SQL += "   ORDER BY Training.POS_No,COURSE.COURSE_ID,COURSE.COURSE_DESC\n";

			SqlDataAdapter DA = new SqlDataAdapter(SQL, BL.ConnectionString());
			DataTable DT = new DataTable();
			DA.Fill(DT);
			rptTraining.DataSource = DT;
			rptTraining.DataBind();



		}

		protected void btnAddCourse_Click(object sender, System.EventArgs e)
		{
			//------------- Get Existing Training List ---------------
			string _Traininglist = "";
			foreach (RepeaterItem item in rptTraining.Items) {
				if (item.ItemType != ListItemType.AlternatingItem & item.ItemType != ListItemType.Item)
					continue;
                Button btnDelete = (Button)item.FindControl("btnDelete");
				_Traininglist += "'" + btnDelete.CommandArgument + "'" + ",";
			}
			if (!string.IsNullOrEmpty(_Traininglist)) {
				EXIST_Training = _Traininglist.Substring(0, _Traininglist.Length - 1);
			} else {
				EXIST_Training = "";
			}

			BindTrainingList();
			ModalTraining.Visible = true;
			ScriptManager.RegisterStartupScript(this.Page, typeof(string), "Focus", "document.getElementById('" + txtSearchTraining.ClientID + "').focus();", true);
		}

		protected void rptTraining_ItemCommand(object source, System.Web.UI.WebControls.RepeaterCommandEventArgs e)
		{
			switch (e.CommandName) {
				case "Delete":
					Button btnDelete =(Button) e.Item.FindControl("btnDelete");
					int COURSE_ID = GL.CINT(btnDelete.CommandArgument);

					string SQL = "";
					SQL += " DELETE FROM tb_Path_Setting_Training\n";
					SQL += " WHERE COURSE_ID=" + COURSE_ID + "\n";
					SQL += " AND POS_NO=" + POS_NO + "\n";
					SqlDataAdapter DA = new SqlDataAdapter(SQL, BL.ConnectionString());
					DataTable DT = new DataTable();
					DA.Fill(DT);
					rptTraining.DataSource = DT;
					rptTraining.DataBind();
					BindTrainingDetail();
					break;
			}


		}

		protected void rptTraining_ItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
		{
			if (e.Item.ItemType != ListItemType.AlternatingItem & e.Item.ItemType != ListItemType.Item)
				return;
			Label lblName =(Label) e.Item.FindControl("lblName");
			Button btnDelete =(Button) e.Item.FindControl("btnDelete");

            DataRowView drv = (DataRowView)e.Item.DataItem;
			
			lblName.Text = drv["COURSE_DESC"].ToString();
			btnDelete.CommandArgument = drv["COURSE_ID"].ToString ();
		}

		//-----------------------ตารางการทดสอบ--------------------
		private void BindTestDetail()
		{
			string SQL = "";

			SQL += " SELECT Setting_Test.POS_No,Test.CPSM_SUBJECT_CODE,Test.CPSM_SUBJECT_NAME,Setting_Test.Min_Score \n";
			SQL += " FROM tb_Path_Setting_Test Setting_Test\n";
			SQL += " LEFT JOIN tb_CP_SUBJECT_MASTER Test ON Setting_Test.SUBJECT_CODE=Test.CPSM_SUBJECT_CODE\n";
			SQL += " WHERE POS_NO =" + POS_NO + "\n";
			SQL += " ORDER BY Setting_Test.POS_No,Test.CPSM_SUBJECT_CODE,Test.CPSM_SUBJECT_NAME\n";

			SqlDataAdapter DA = new SqlDataAdapter(SQL, BL.ConnectionString());
			DataTable DT = new DataTable();
			DA.Fill(DT);
			rptTest.DataSource = DT;
			rptTest.DataBind();
		}


		protected void BtnAddTest_Click(object sender, System.EventArgs e)
		{
			//------------- Get Existing Training List ---------------
			string _Testlist = "";
			foreach (RepeaterItem item in rptTest.Items) {
				if (item.ItemType != ListItemType.AlternatingItem & item.ItemType != ListItemType.Item)
					continue;
                Button btnDelete = (Button)item.FindControl("btnDelete");
				_Testlist += "'" + btnDelete.CommandArgument + "'" + ",";
			}
			if (!string.IsNullOrEmpty(_Testlist)) {
				EXIST_Test = _Testlist.Substring(0, _Testlist.Length - 1);
			} else {
				EXIST_Test = "";
			}

			BindTestList();
			ModalTest.Visible = true;
			ScriptManager.RegisterStartupScript(this.Page, typeof(string), "Focus", "document.getElementById('" + txtSearchTest.ClientID + "').focus();", true);
		}

		protected void rptTest_ItemCommand(object source, System.Web.UI.WebControls.RepeaterCommandEventArgs e)
		{
            string SQL;
            DataTable DT;
            SqlCommandBuilder cmd;
            SqlDataAdapter DA;
            DataRow DR;
            string SUBJECT_CODE;

			switch (e.CommandName) {
				case "Delete":
					Button btnDelete =(Button) e.Item.FindControl("btnDelete");
					 SUBJECT_CODE = btnDelete.CommandArgument;

					 SQL = "";
					SQL += " DELETE FROM tb_Path_Setting_Test\n";
					SQL += " WHERE SUBJECT_CODE=" + SUBJECT_CODE + "\n";
					SQL += " AND POS_NO=" + POS_NO + "\n";
					 DA = new SqlDataAdapter(SQL, BL.ConnectionString());
					 DT = new DataTable();
					DA.Fill(DT);
					rptTest.DataSource = DT;
					rptTest.DataBind();
					BindTestDetail();
					break;
				case "Update":
                    TextBox txtScore = (TextBox)e.Item.FindControl("txtScore");
                    Button btnupdate = (Button)e.Item.FindControl("btnDelete");
					 SUBJECT_CODE = btnupdate.CommandArgument;

					 SQL = "SElECT * FROM tb_Path_Setting_Test \n";
					SQL += " WHERE POS_No=" + POS_NO + "\n";
					SQL += " AND SUBJECT_CODE=" + SUBJECT_CODE + "\n";

					 DT = new DataTable();
					 DA = new SqlDataAdapter(SQL, BL.ConnectionString());
					DA.Fill(DT);

					 DR = null;
					if (DT.Rows.Count == 0) {
						DR = DT.NewRow();
					} else {
						DR = DT.Rows[0];
					}
					if (string.IsNullOrEmpty(txtScore.Text)) {
						DR["Min_Score"] = GL.StringFormatNumber(0);
					} else {
						DR["Min_Score"] = Conversion.Val(txtScore.Text);
					}

					 DR["Update_By"] = Session["USER_PSNL_NO"].ToString();
					DR["Update_Time"] = DateAndTime.Now;
					if (DT.Rows.Count == 0)
						DT.Rows.Add(DR);

					 cmd = new SqlCommandBuilder();
					cmd = new SqlCommandBuilder(DA);
					DA.Update(DT);
					BindTestDetail();

					break;
			}
		}

		protected void rptTest_ItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
		{
			if (e.Item.ItemType != ListItemType.AlternatingItem & e.Item.ItemType != ListItemType.Item)
				return;
			Label lblName =(Label) e.Item.FindControl("lblName");
            TextBox txtScore = (TextBox)e.Item.FindControl("txtScore");
			Button btnDelete =(Button) e.Item.FindControl("btnDelete");
			Button btnUpdate =(Button) e.Item.FindControl("btnUpdate");
			HtmlTableCell cel_Score =(HtmlTableCell) e.Item.FindControl("cel_Score");


            DataRowView drv = (DataRowView)e.Item.DataItem;
			lblName.Text = drv["CPSM_SUBJECT_NAME"].ToString();

			cel_Score.Attributes["onclick"] = "document.getElementById('" + txtScore.ClientID + "').focus();";
			if (Information.IsNumeric(drv["Min_Score"])) {
				txtScore.Text = GL.StringFormatNumber(GL.CDBL(drv["Min_Score"]));
				//txtWeight.Text = drv["COMP_Weight"]
				if (GL.CINT (drv["Min_Score"]) > 100 | GL.CINT (drv["Min_Score"]) <= 0) {
					//cel_Score.Style.Item("background-color") = "#999999"
					txtScore.Style["color"] = "red";
				} else {
					//cel_Score.Style.Item("background-color") = "white"
					txtScore.Style["color"] = "black";
				}
			}
			CL.ImplementJavaFloatText(txtScore);
            txtScore.Attributes["onchange"] = "document.getElementById('" + btnUpdate.ClientID + "').click();";


            btnDelete.CommandArgument = drv["CPSM_SUBJECT_CODE"].ToString();
            btnUpdate.CommandArgument = drv["CPSM_SUBJECT_CODE"].ToString();
			//txtScore.Attributes("onchange") = "document.getElementById('" & btnupdate.ClientID & "').click();"
		}


		#region "TrainingDialog"

		protected void rptTrainingDialog_ItemCommand(object source, System.Web.UI.WebControls.RepeaterCommandEventArgs e)
		{
			switch (e.CommandName) {
				case "Select":
					Button btnTrainingSelect =(Button) e.Item.FindControl("btnTrainingSelect");
					string COURSE_ID = btnTrainingSelect.CommandArgument;

					string SQL_Setting = "";
					SQL_Setting += " SELECT * \n";
					SQL_Setting += " FROM tb_Path_Setting\n";
					SQL_Setting += " WHERE POS_No=" + POS_NO + "\n";
					SqlDataAdapter DA_Setting = new SqlDataAdapter(SQL_Setting, BL.ConnectionString());
					DataTable DT_Setting = new DataTable();
					DA_Setting.Fill(DT_Setting);
					if (DT_Setting.Rows.Count == 0) {
						AutoSave();
					}

					string SQL = "SElECT * FROM tb_Path_Setting_Training \n";
					SQL += " WHERE POS_No=" + POS_NO + "\n";
					SQL += " AND COURSE_ID='" + COURSE_ID + "'\n";

					DataTable DT = new DataTable();
					SqlDataAdapter DA = new SqlDataAdapter(SQL, BL.ConnectionString());
					DA.Fill(DT);

					DataRow DR = null;
					if (DT.Rows.Count == 0) {
						DR = DT.NewRow();
						DR["POS_No"] = POS_NO;
						DR["COURSE_ID"] = COURSE_ID;
					} else {
						DR = DT.Rows[0];
					}
                    DR["Update_By"] = Session["USER_PSNL_NO"].ToString();
					DR["Update_Time"] = DateAndTime.Now;
					if (DT.Rows.Count == 0)
						DT.Rows.Add(DR);

					SqlCommandBuilder cmd = new SqlCommandBuilder();
					cmd = new SqlCommandBuilder(DA);
					DA.Update(DT);
					//-----bind ตารางกำหนดหลักสูตร------
					BindTrainingDetail();
					ModalTraining.Visible = false;
					break;
			}
		}

		protected void rptTrainingDialog_ItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
		{
			if (e.Item.ItemType != ListItemType.AlternatingItem & e.Item.ItemType != ListItemType.Item)
				return;
			Label lblNo =(Label) e.Item.FindControl("lblNo");
			Label lblName =(Label) e.Item.FindControl("lblName");
			Button btnTrainingSelect =(Button) e.Item.FindControl("btnTrainingSelect");
            DataRowView drv = (DataRowView)e.Item.DataItem;
			lblNo.Text = drv["COURSE_ID"].ToString().PadLeft(3, GL.chr0);
			lblName.Text = drv["COURSE_DESC"].ToString().ToString();
			btnTrainingSelect.CommandArgument = drv["COURSE_ID"].ToString ();
		}

		protected void SearchTraining(object sender, System.EventArgs e)
		{
			BindTrainingList();
		}

		private void BindTrainingList()
		{
			string SQL = "";
			SQL += " SELECT * FROM tb_PK_COURSE\n";
			string Filter = "";
			if (!string.IsNullOrEmpty(txtSearchTraining.Text)) {
				Filter += " (COURSE_ID LIKE '%" + txtSearchTraining.Text.Replace("'", "''") + "%' OR \n";
				Filter += " COURSE_DESC LIKE '%" + txtSearchTraining.Text.Replace("'", "''") + "%') AND ";
			}

			if (!string.IsNullOrEmpty(EXIST_Training)) {
				Filter += " COURSE_ID NOT IN (" + EXIST_Training + ") AND ";
			}

			if (!string.IsNullOrEmpty(Filter)) {
				SQL += " WHERE " + Filter.Substring(0, Filter.Length - 4) + "\n";
			}
			SQL += " \n";

			SQL += " ORDER BY COURSE_ID,COURSE_DESC\n";

			SqlDataAdapter DA = new SqlDataAdapter(SQL, BL.ConnectionString());
			DataTable DT = new DataTable();
			DA.Fill(DT);

			Session["SettingPropertyTraining"] = DT;
			Pager_Training.SesssionSourceName = "SettingPropertyTraining";
			Pager_Training.RenderLayout();

			if (DT.Rows.Count == 0) {
				lblCountTraining.Text = "ไม่พบรายการดังกล่าว";
			} else {
				lblCountTraining.Text = "พบ " + GL.StringFormatNumber(DT.Rows.Count, 0) + " รายการ";
			}

		}

		protected void Pager_Training_PageChanging(PageNavigation Sender)
		{
			Pager_Training.TheRepeater = rptTrainingDialog;
		}

		//Protected Sub btnClose_Training_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose_Training.Click
		//    ModalTraining.Visible = False
		//End Sub

		#endregion



		#region "TestDialog"

		protected void rptTestDialog_ItemCommand(object source, System.Web.UI.WebControls.RepeaterCommandEventArgs e)
		{
			switch (e.CommandName) {
				case "Select":
                    Button btnTestSelect = (Button)e.Item.FindControl("btnTestSelect");
					string SUBJECT_CODE = btnTestSelect.CommandArgument;

					string SQL_Setting = "";
					SQL_Setting += " SELECT * \n";
					SQL_Setting += " FROM tb_Path_Setting\n";
					SQL_Setting += " WHERE POS_No=" + POS_NO + "\n";
					SqlDataAdapter DA_Setting = new SqlDataAdapter(SQL_Setting, BL.ConnectionString());
					DataTable DT_Setting = new DataTable();
					DA_Setting.Fill(DT_Setting);
					if (DT_Setting.Rows.Count == 0) {
						AutoSave();
					}


					string SQL = "SElECT * FROM tb_Path_Setting_Test \n";
					SQL += " WHERE POS_No=" + POS_NO + "\n";
					SQL += " AND SUBJECT_CODE=" + SUBJECT_CODE + "\n";

					DataTable DT = new DataTable();
					SqlDataAdapter DA = new SqlDataAdapter(SQL, BL.ConnectionString());
					DA.Fill(DT);

					DataRow DR = null;
					if (DT.Rows.Count == 0) {
						DR = DT.NewRow();
						DR["POS_No"] = POS_NO;
						DR["SUBJECT_CODE"] = SUBJECT_CODE;
					} else {
						DR = DT.Rows[0];
					}
					DR["Min_Score"] = GL.StringFormatNumber(0);
					 DR["Update_By"] = Session["USER_PSNL_NO"].ToString();
					DR["Update_Time"] = DateAndTime.Now;
					if (DT.Rows.Count == 0)
						DT.Rows.Add(DR);

					SqlCommandBuilder cmd = new SqlCommandBuilder();
					cmd = new SqlCommandBuilder(DA);
					DA.Update(DT);
					//-----bind ตารางกำหนดหลักสูตร------
					BindTestDetail();
					ModalTest.Visible = false;
					break;
			}

		}

		protected void rptTestDialog_ItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
		{
			if (e.Item.ItemType != ListItemType.AlternatingItem & e.Item.ItemType != ListItemType.Item)
				return;
			Label lblNo =(Label) e.Item.FindControl("lblNo");
			Label lblName =(Label) e.Item.FindControl("lblName");
			Button btnTestSelect =(Button) e.Item.FindControl("btnTestSelect");
            DataRowView drv = (DataRowView)e.Item.DataItem;
			lblNo.Text = drv["CPSM_SUBJECT_CODE"].ToString().PadLeft(3, GL.chr0);
			lblName.Text = drv["CPSM_SUBJECT_NAME"].ToString ();
			btnTestSelect.CommandArgument = drv["CPSM_SUBJECT_CODE"].ToString ();
		}


		protected void btnSearchTest_Click(object sender, System.EventArgs e)
		{
			BindTestList();
		}

		private void BindTestList()
		{
			string SQL = "";
			SQL += " SELECT * FROM tb_CP_SUBJECT_MASTER\n";
			string Filter = "";
			if (!string.IsNullOrEmpty(txtSearchTest.Text)) {
				Filter += " (CPSM_SUBJECT_CODE LIKE '%" + txtSearchTest.Text.Replace("'", "''") + "%' OR \n";
				Filter += " CPSM_SUBJECT_NAME LIKE '%" + txtSearchTest.Text.Replace("'", "''") + "%') AND ";
			}

			if (!string.IsNullOrEmpty(EXIST_Test)) {
				Filter += " CPSM_SUBJECT_CODE NOT IN (" + EXIST_Test + ") AND ";
			}

			if (!string.IsNullOrEmpty(Filter)) {
				SQL += " WHERE " + Filter.Substring(0, Filter.Length - 4) + "\n";
			}
			SQL += " \n";

			SQL += "  ORDER BY CPSM_SUBJECT_CODE,CPSM_SUBJECT_NAME\n";

			SqlDataAdapter DA = new SqlDataAdapter(SQL, BL.ConnectionString());
			DataTable DT = new DataTable();
			DA.Fill(DT);

			Session["SettingPropertyTest"] = DT;
			Pager_Test.SesssionSourceName = "SettingPropertyTest";
			Pager_Test.RenderLayout();

			if (DT.Rows.Count == 0) {
				lblCountTest.Text = "ไม่พบรายการดังกล่าว";
			} else {
				lblCountTest.Text = "พบ " + GL.StringFormatNumber(DT.Rows.Count, 0) + " รายการ";
			}

		}



		protected void Pager_Test_PageChanging(PageNavigation Sender)
		{
			Pager_Test.TheRepeater = rptTestDialog;
		}

		//Protected Sub btnClose_Test_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose_Test.Click
		//    ModalTest.Visible = False
		//End Sub
		#endregion

		protected void txtAss_Min_Score_TextChanged(object sender, System.EventArgs e)
		{
			AutoSave();
		}

		protected void btnOK_Click(object sender, System.EventArgs e)
		{
			AutoSave();
			ScriptManager.RegisterStartupScript(this.Page, typeof(string), "Alert", "showAlert('บันทึกเกณฑ์การเลื่อนตำแหน่งเรียบร้อยแล้ว');", true);
			BindPosition();
		}


		private void AutoSave()
		{
			string SQL = "";
			SQL += " SELECT * \n";
			SQL += " FROM tb_Path_Setting\n";
			SQL += " WHERE POS_No=" + POS_NO + "\n";
			SqlDataAdapter DA = new SqlDataAdapter(SQL, BL.ConnectionString());
			DataTable DT = new DataTable();
			DA.Fill(DT);

			DataRow DR = null;
			if (DT.Rows.Count == 0) {
				DR = DT.NewRow();
				DR["POS_No"] = POS_NO;

			} else {
				DR = DT.Rows[0];
			}
			if (!string.IsNullOrEmpty(txtAss_Min_Score.Text)) {
				DR["Ass_Min_Score"] = txtAss_Min_Score.Text.Replace(",", "");
			} else {
				DR["Ass_Min_Score"] = DBNull.Value;
			}
			if (!string.IsNullOrEmpty(txtLeave_Score.Text)) {
				DR["Leave_Score"] = txtLeave_Score.Text.Replace(",", "");
			} else {
				DR["Leave_Score"] = DBNull.Value;
			}
			if (!string.IsNullOrEmpty(txtArrive_Late.Text)) {
				DR["Arrive_Late"] = txtArrive_Late.Text.Replace(",", "");
			} else {
				DR["Arrive_Late"] = DBNull.Value;
			}
			if (chkPunishment.Checked) {
				DR["Punishment"] = true;
			} else {
				DR["Punishment"] = false;
			}
			 DR["Update_By"] = Session["USER_PSNL_NO"].ToString();
			DR["Update_Time"] = DateAndTime.Now;
			if (DT.Rows.Count == 0)
				DT.Rows.Add(DR);
			SqlCommandBuilder cmd = new SqlCommandBuilder();
			cmd = new SqlCommandBuilder(DA);
			DA.Update(DT);

		}

		protected void btnBack_Click(object sender, System.EventArgs e)
		{
			PnlList.Visible = true;
			pnlEdit.Visible = false;
			if (!string.IsNullOrEmpty(Request.QueryString["POS_NO"])) {
				BindPosition();
			}
			ClearForm();
		}

		private void BindPath_Position()
		{
			lblCurrentPos.Text = lblPos_Head.Text + " ระดับ " + lblClass_Head.Text + " หน่วยงาน" + lblDept_Head.Text;
		}


		#region "PosDialog"

		private void BindPostList()
		{
			ddlSector.SelectedIndex = 0;
			txt_Search_Organize.Text = "";
			lblPOS.Text = "";
			ddlSetting.SelectedValue = "0";

			string Filter = "";
			string SQL = "";
			SQL += " SELECT DEPT.SECTOR_NAME,DEPT.DEPT_NAME,POS.POS_NO,ISNULL(POS.MGR_NAME,'-') MGR_NAME,ISNULL(POS.FLD_NAME,'-') FLD_NAME,POS.PNPO_CLASS\n";
			SQL += "  FROM vw_PN_Position POS \n";
			SQL += "  INNER JOIN vw_HR_Department DEPT ON POS.DEPT_CODE=DEPT.DEPT_CODE AND POS.MINOR_CODE=DEPT.MINOR_CODE\n";
			SQL += "  LEFT JOIN vw_PN_PSNL PSN ON POS.POS_NO=PSN.POS_NO\n";
			SQL += "  INNER JOIN vw_Path_Setting_Status Setting_Status ON Setting_Status.POS_No = POS.POS_NO\n";
			SQL += "  WHERE POS.PNPO_CLASS IS NOT NULL \n";

			if (ddlSector.SelectedIndex > 0) {
				string[] Sector = Strings.Split(ddlSector.Items[ddlSector.SelectedIndex].Text, ":");
				string Sector_Name = Sector[1];
				SQL += " AND (DEPT.SECTOR_NAME LIKE '%" + Sector_Name.Replace(" ", "") + "%' )\n";
			}
			if (!string.IsNullOrEmpty(txt_Search_Organize.Text)) {
				SQL += " AND (DEPT.DEPT_NAME LIKE '%" + txt_Search_Organize.Text.Replace("'", "''") + "%' OR \n";
				SQL += " POS.MGR_NAME LIKE '%" + txt_Search_Organize.Text.Replace("'", "''") + "%' ) \n";
			}

			if (!string.IsNullOrEmpty(txtSearchPos.Text)) {
				SQL += " AND (POS.POS_NO LIKE '%" + txtSearchPos.Text.Replace("'", "''") + "%' OR \n";
				SQL += " POS.FLD_NAME LIKE '%" + txtSearchPos.Text.Replace("'", "''") + "%' ) \n";
			}


			//If lblPSNL_NO.Text <> "" Then
			//    SQL &= " AND (PSN.PSNL_NO LIKE '%" & lblPSNL_NO.Text.Replace("'", "''") & "%' OR " & vbLf
			//    SQL &= " PSN.PSNL_Fullname LIKE '%" & lblPSNL_NO.Text.Replace("'", "''") & "%' )" & vbLf
			//End If

			//Select Case ddlSetting.SelectedValue
			//    Case 1
			//        SQL &= " AND (Setting_Status.Status_Path IN (1))" & vbLf
			//    Case 2
			//        SQL &= " AND (Setting_Status.Status_Path IN (0))" & vbLf
			//    Case Else
			//End Select

			if (!string.IsNullOrEmpty(EXIST_Pos)) {
				Filter += " AND POS.POS_NO NOT IN (" + EXIST_Pos + "," + POS_NO + ")  \n";
			} else {
				Filter += " AND POS.POS_NO NOT IN (" + POS_NO + ")  \n";
			}

			//Select Case Mode_Pos
			//    Case "Left"
			//        Filter &= " AND POS.POS_NO NOT IN (" & EXIST_Pos & "," & POS_NO & ")  " & vbLf
			//    Case "Right"
			//        Filter &= " AND POS.POS_NO NOT IN (" & EXIST_Pos & "," & POS_NO & ")  " & vbLf
			//    Case Else
			//End Select

			switch (Mode_Pos) {
				case "Left":
					Filter += " AND  POS.PNPO_CLASS IN (" + Convert.ToInt32(lblClass_Head.Text) + "," + (Convert.ToInt32(lblClass_Head.Text) - 1) + ")\n";
					break;
				case "Right":
					Filter += " AND  POS.PNPO_CLASS IN (" + Convert.ToInt32(lblClass_Head.Text) + "," + (Convert.ToInt32(lblClass_Head.Text) + 1) + ")\n";
					break;
				default:
					break;
			}

			SQL += "  " + Filter;

			SQL += "  GROUP BY DEPT.SECTOR_NAME,DEPT.DEPT_NAME,POS.POS_NO,POS.FLD_NAME,POS.MGR_NAME,\n";
			SQL += "  POS.PNPO_CLASS, DEPT.SECTOR_CODE, DEPT.DEPT_CODE, POS.PNPO_CLASS, POS.POS_NO\n";
			SQL += "  ORDER BY DEPT.SECTOR_CODE,DEPT.DEPT_CODE,POS.PNPO_CLASS DESC,POS.POS_NO\n";

			SqlDataAdapter DA = new SqlDataAdapter(SQL, BL.ConnectionString());
			DataTable DT = new DataTable();
			DA.Fill(DT);

			EXIST_Pos = "";
			Session["SettingPropertyPos"] = DT;
			Pager_Pos.SesssionSourceName = "SettingPropertyPos";
			Pager_Pos.RenderLayout();



			if (DT.Rows.Count == 0) {
				lblCountPosNo.Text = "ไม่พบรายการดังกล่าว";
			} else {
				lblCountPosNo.Text = "พบ " + GL.StringFormatNumber(DT.Rows.Count, 0) + " รายการ";
			}

		}



		protected void btnAddLeft_ServerClick(object sender, System.EventArgs e)
		{
			//------------- Get Existing  List ---------------
			lblModePos.Text = "Left";
			Mode_Pos = lblModePos.Text;
			string _Poslist = "";
			foreach (RepeaterItem item in rptPosLeft.Items) {
				if (item.ItemType != ListItemType.AlternatingItem & item.ItemType != ListItemType.Item)
					continue;
                Button btnDeleteLeft = (Button)item.FindControl("btnDeleteLeft");
				_Poslist += "'" + btnDeleteLeft.CommandArgument + "'" + ",";
			}
			if (!string.IsNullOrEmpty(_Poslist)) {
				EXIST_Pos += _Poslist.Substring(0, _Poslist.Length - 1);
			} else {
				EXIST_Pos += "";
			}

			BindPostList();
			ModalPost.Visible = true;
			ScriptManager.RegisterStartupScript(this.Page, typeof(string), "Focus", "document.getElementById('" + txtSearchPos.ClientID + "').focus();", true);
		}

		protected void Pager_Pos_PageChanging(PageNavigation Sender)
		{
			Pager_Pos.TheRepeater = rptPosDialog;
		}

		string LastSectorPos = "";

		string LastDeptPos = "";
		protected void rptPosDialog_ItemCommand(object source, System.Web.UI.WebControls.RepeaterCommandEventArgs e)
		{
			switch (e.CommandName) {
				case "Select":
					Button btnPosSelect = (Button) e.Item.FindControl("btnPosSelect");
					string To_POS_No = btnPosSelect.CommandArgument;
					string Route = "";
					string SQL = "";
					SQL += "  SELECT * \n";
					SQL += "  FROM  tb_Path_Setting_Route \n";
					SQL += "  WHERE From_POS_No =" + POS_NO;
					SQL += "  AND To_POS_No=" + To_POS_No;
					switch (Mode_Pos) {
						case "Left":
							SQL += "  AND Route='Left' ";
							Route = "Left";
							break;
						case "Right":
							SQL += "  AND Route='Right' ";
							Route = "Right";
							break;
						default:
							break;
					}
					SqlDataAdapter DA = new SqlDataAdapter(SQL, BL.ConnectionString());
					DataTable DT = new DataTable();
					DA.Fill(DT);

					DataRow DR = null;
					if (DT.Rows.Count == 0) {
						DR = DT.NewRow();
						DR["From_POS_No"] = POS_NO;
					} else {
						DR = DT.Rows[0];
					}
					DR["To_POS_No"] = To_POS_No;
					DR["Route"] = Route;
					//DR("Update_By") = Session("")
					DR["Update_Time"] = DateAndTime.Now;
					if (DT.Rows.Count == 0)
						DT.Rows.Add(DR);
					SqlCommandBuilder cmd = new SqlCommandBuilder();
					cmd = new SqlCommandBuilder(DA);
					DA.Update(DT);
					//-----bind ตารางตำแหน่ง------

					BindPosLeft();
					BindPosRight();
					ModalPost.Visible = false;

					break;
			}



		}
		protected void rptPosDialog_ItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
		{
			if (e.Item.ItemType != ListItemType.AlternatingItem & e.Item.ItemType != ListItemType.Item)
				return;
			Label lblSector =(Label) e.Item.FindControl("lblSector");
			Label lblDept =(Label) e.Item.FindControl("lblDept");
			Label lblClass = (Label) e.Item.FindControl("lblClass");
			Label lblPosNo =(Label) e.Item.FindControl("lblPosNo");
			Label lblMGR =(Label) e.Item.FindControl("lblMGR");
			Label lblPSN = (Label) e.Item.FindControl("lblPSN");
			Button btnPosSelect = (Button) e.Item.FindControl("btnPosSelect");
            DataRowView drv = (DataRowView)e.Item.DataItem;
			if (LastSectorPos != drv["SECTOR_NAME"].ToString()) {
				lblSector.Text = drv["SECTOR_NAME"].ToString();
				LastSectorPos = drv["SECTOR_NAME"].ToString();
				lblDept.Text = drv["DEPT_NAME"].ToString();
				LastDeptPos = drv["DEPT_NAME"].ToString();
			} else if (LastDeptPos != drv["DEPT_NAME"].ToString()) {
				lblDept.Text = drv["DEPT_NAME"].ToString();
				LastDeptPos = drv["DEPT_NAME"].ToString();
			}
			lblClass.Text = GL.CINT (drv["PNPO_CLASS"]).ToString ();
			if (GL.IsEqualNull(drv["POS_NO"])) {
				lblPosNo.Text = "-";
			} else {
				lblPosNo.Text = drv["POS_NO"].ToString() + ":" + drv["FLD_NAME"].ToString();
			}
			lblMGR.Text = drv["MGR_NAME"].ToString();
			btnPosSelect.CommandArgument = drv["POS_NO"].ToString ();

		}

		//Protected Sub btnClose_Pos_Click(sender As Object, e As System.EventArgs) Handles btnClose_Pos.Click
		//    ModalPost.Visible = False
		//    Mode_Pos = ""
		//End Sub

		protected void btnSearchPos_Click(object sender, System.EventArgs e)
		{
			BindPostList();
		}


		private void BindPosLeft()
		{
			BindCourse_Detail();
			BindTest_Detail();

			string SQL = "";
			SQL += "  SELECT From_POS_No , To_POS_No , Route\n";
			SQL += "  ,vw_PN_Position.POS_NO\n";
			SQL += "  ,vw_PN_Position.DEPT_NAME\n";
			SQL += "  ,vw_PN_Position.PNPO_CLASS\n";
			SQL += "  ,vw_PN_Position.MGR_NAME\n";
			SQL += "  FROM  tb_Path_Setting_Route Setting_Route\n";
			SQL += "  LEFT JOIN tb_Path_Setting Path_Setting  ON Path_Setting.POS_No=Setting_Route.From_POS_No\n";
			SQL += "  INNER JOIN vw_PN_Position ON vw_PN_Position.POS_NO=Setting_Route.To_POS_No\n";
			SQL += "  WHERE From_POS_No =" + POS_NO;
			SQL += "  AND Route='Left' ";

			SQL += "  UNION \n";

			SQL += "  SELECT To_POS_No  From_POS_No \n";
			SQL += "  ,  From_POS_No To_POS_No \n";
			SQL += "  , Route\n";
			SQL += "  ,vw_PN_Position.POS_NO\n";
			SQL += "  ,vw_PN_Position.DEPT_NAME\n";
			SQL += "  ,vw_PN_Position.PNPO_CLASS\n";
			SQL += "  ,vw_PN_Position.MGR_NAME\n";
			SQL += "  FROM  tb_Path_Setting_Route Setting_Route\n";
			SQL += "  LEFT JOIN tb_Path_Setting Path_Setting  ON Path_Setting.POS_No=Setting_Route.To_POS_No\n";
			SQL += "  INNER JOIN vw_PN_Position ON vw_PN_Position.POS_NO=Setting_Route.From_POS_No\n";
			SQL += "  WHERE To_POS_No =" + POS_NO + "\n";
			SQL += "  AND Route='Right' ";

			SqlDataAdapter DA = new SqlDataAdapter(SQL, BL.ConnectionString());
			DataTable DT = new DataTable();
			DA.Fill(DT);
			rptPosLeft.DataSource = DT;
			rptPosLeft.DataBind();
		}

		private void BindPosRight()
		{
			BindCourse_Detail();
			BindTest_Detail();

			string SQL = "";
			SQL += "  SELECT From_POS_No , To_POS_No , Route\n";
			SQL += "  ,vw_PN_Position.POS_NO\n";
			SQL += "  ,vw_PN_Position.DEPT_NAME\n";
			SQL += "  ,vw_PN_Position.PNPO_CLASS\n";
			SQL += "  ,vw_PN_Position.MGR_NAME\n";
			SQL += "  FROM  tb_Path_Setting_Route Setting_Route\n";
			SQL += "  LEFT JOIN tb_Path_Setting Path_Setting  ON Path_Setting.POS_No=Setting_Route.From_POS_No\n";
			SQL += "  INNER JOIN vw_PN_Position ON vw_PN_Position.POS_NO=Setting_Route.To_POS_No\n";
			SQL += "  WHERE From_POS_No =" + POS_NO;
			SQL += "  AND Route='Right' ";



			SQL += "  UNION \n";

			SQL += "  SELECT To_POS_No  From_POS_No \n";
			SQL += "  ,  From_POS_No To_POS_No \n";
			SQL += "  , Route\n";
			SQL += "  ,vw_PN_Position.POS_NO\n";
			SQL += "  ,vw_PN_Position.DEPT_NAME\n";
			SQL += "  ,vw_PN_Position.PNPO_CLASS\n";
			SQL += "  ,vw_PN_Position.MGR_NAME\n";
			SQL += "  FROM  tb_Path_Setting_Route Setting_Route\n";
			SQL += "  LEFT JOIN tb_Path_Setting Path_Setting  ON Path_Setting.POS_No=Setting_Route.To_POS_No\n";
			SQL += "  INNER JOIN vw_PN_Position ON vw_PN_Position.POS_NO=Setting_Route.From_POS_No\n";
			SQL += "  WHERE To_POS_No =" + POS_NO + "\n";
			SQL += "  AND Route='Left' ";

			SqlDataAdapter DA = new SqlDataAdapter(SQL, BL.ConnectionString());
			DataTable DT = new DataTable();
			DA.Fill(DT);
			rptPosRight.DataSource = DT;
			rptPosRight.DataBind();
		}

		//Function CurrentPos() As DataTable

		private void CurrentPos()
		{
			string SQL = "";
			SQL += " SELECT DEPT.SECTOR_CODE,DEPT.SECTOR_NAME,DEPT.DEPT_CODE,DEPT.DEPT_NAME,POS.POS_NO,ISNULL(POS.MGR_NAME,'-') MGR_NAME,ISNULL(POS.FLD_NAME,'-') FLD_NAME,\n";
			SQL += "  POS.PNPO_CLASS,PSN.PSNL_NO,PSN.PSNL_Fullname,POS.Update_Time \n";
			SQL += "  ,Setting_Status.Status_Path\n";
			SQL += "  FROM vw_PN_Position POS \n";
			SQL += "  INNER JOIN vw_HR_Department DEPT ON POS.DEPT_CODE=DEPT.DEPT_CODE AND POS.MINOR_CODE=DEPT.MINOR_CODE\n";
			SQL += "  LEFT JOIN vw_PN_PSNL PSN ON POS.POS_NO=PSN.POS_NO\n";
			SQL += "  INNER JOIN vw_Path_Setting_Status Setting_Status ON Setting_Status.POS_No = POS.POS_NO\n";
			SQL += "  WHERE POS.PNPO_CLASS IS NOT NULL   \n";
			SQL += "  AND POS.POS_NO=" + POS_NO + "\n";
			SQL += "  ";
			SQL += "  GROUP BY DEPT.SECTOR_NAME,DEPT.DEPT_NAME,POS.POS_NO,POS.FLD_NAME,POS.MGR_NAME,\n";
			SQL += "  POS.PNPO_CLASS, PSN.PSNL_NO, PSN.PSNL_Fullname, POS.Update_Time, DEPT.SECTOR_CODE, DEPT.DEPT_CODE, POS.PNPO_CLASS, POS.POS_NO\n";
			SQL += "  , Setting_Status.Status_Path\n";
			SQL += "  ORDER BY DEPT.SECTOR_CODE,DEPT.DEPT_CODE,POS.PNPO_CLASS DESC,POS.POS_NO\n";
			SqlDataAdapter DA = new SqlDataAdapter(SQL, BL.ConnectionString());
			DataTable DT = new DataTable();
			DA.Fill(DT);
			if (DT.Rows.Count == 1) {
				//-----------------แสดงรายละเอียดตำแหน่ง--------------
				lblPos_Head.Text = DT.Rows[0]["POS_NO"].ToString() + " : " + DT.Rows[0]["MGR_NAME"].ToString();
				lblDept_Head.Text = DT.Rows[0]["DEPT_NAME"].ToString();
				if (!GL.IsEqualNull(DT.Rows[0]["PNPO_CLASS"])) {
                    lblClass_Head.Text = Convert.ToInt32(DT.Rows[0]["PNPO_CLASS"]).ToString();
				} else {
					lblClass_Head.Text = "-";
				}

				if (!GL.IsEqualNull(DT.Rows[0]["PSNL_NO"])) {
					lblPSN_Head.Text = DT.Rows[0]["PSNL_NO"].ToString() + " : " + DT.Rows[0]["PSNL_Fullname"].ToString();
					//lblPSN_Head.ForeColor = Drawing.Color.Gray
				} else {
					lblPSN_Head.Text = "ไม่มีผู้ครองตำแหน่ง";
					//lblPSN_Head.ForeColor = Drawing.Color.Red
				}

				//-----ผลการประเมิน--,วันลา สาย ขาด ,โทษทางวินัย------
				Path_Setting();
				//-----การฝึกอบรม----------
				BindTrainingDetail();
				//-----การทดสอบ----------
				BindTestDetail();
				//----------------------
				//-----กำหนดเส้นทาง---------
				BindPath_Position();
				//------LEFT/RIGHT----------
				BindPosLeft();
				BindPosRight();
				//-----กำหนดเส้นทางในฝ่ายเดียวกัน---------
				SECTOR_CODE = DT.Rows[0]["SECTOR_CODE"].ToString ();
				DEPT_CODE = DT.Rows[0]["DEPT_CODE"].ToString ();
				BindPosLevel();
				btnBack.Focus();
			}

			//Return DT
		}

		protected void rptPosLeft_ItemCommand(object source, System.Web.UI.WebControls.RepeaterCommandEventArgs e)
		{
            Button btnDeleteLeft = (Button)e.Item.FindControl("btnDeleteLeft");
            string POS_Left;
			switch (e.CommandName) {
				case "Delete":
					
					 POS_Left = btnDeleteLeft.CommandArgument;
					string SQL = "";
					SQL += "  SELECT * \n";
					SQL += "  FROM  tb_Path_Setting_Route  WHERE From_POS_No =" + POS_NO;
					SQL += "  AND To_POS_No=" + POS_Left;
					SQL += "  AND Route='Left' ";
					SqlDataAdapter DA = new SqlDataAdapter(SQL, BL.ConnectionString());
					DataTable DT = new DataTable();
					DA.Fill(DT);
					if (DT.Rows.Count == 1) {
						SQL = "  ";
						SQL += "  Delete From tb_Path_Setting_Route WHERE From_POS_No =" + POS_NO;
						SQL += "  AND To_POS_No=" + POS_Left;
						SQL += "  AND Route='Left' ";
						DA = new SqlDataAdapter(SQL, BL.ConnectionString());
						DT = new DataTable();
						DA.Fill(DT);
					} else {
						SQL = "";
						SQL += "  SELECT *  FROM  tb_Path_Setting_Route WHERE  To_POS_No=" + POS_NO;
						SQL += "  AND From_POS_No=" + POS_Left;
						SQL += "  AND Route='Right' ";
						DA = new SqlDataAdapter(SQL, BL.ConnectionString());
						DT = new DataTable();
						DA.Fill(DT);

						if (DT.Rows.Count > 0) {
							SQL = "";
							SQL += "  DELETE  FROM  tb_Path_Setting_Route WHERE  To_POS_No=" + POS_NO;
							SQL += "  AND From_POS_No=" + POS_Left;
							SQL += "  AND Route='Right' ";
							DA = new SqlDataAdapter(SQL, BL.ConnectionString());
							DT = new DataTable();
							DA.Fill(DT);
						}

						//ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "showAlert('ไม่สามารถลบตำแหน่งที่ ตำแหน่ง " & POS_NO & "  ไม่ได้กำหนดเอง');", True)
						//Exit Sub
					}
					BindPosLeft();
					break;
				case "lnkCurrentFromLeft":
					
					 POS_Left = btnDeleteLeft.CommandArgument;
					POS_NO = POS_Left;
					CurrentPos();
					break;
			}

		}

		DataTable DT_Course_Detail = null;

		DataTable DT_Test_Detail = null;
		protected void rptPosLeft_ItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
		{
			if (e.Item.ItemType != ListItemType.AlternatingItem & e.Item.ItemType != ListItemType.Item)
				return;
			Label lblPosLeft =(Label) e.Item.FindControl("lblPosLeft");
			Button btnDeleteLeft =(Button) e.Item.FindControl("btnDeleteLeft");
			HtmlAnchor tableLeft =(HtmlAnchor) e.Item.FindControl("tableLeft");

			HtmlAnchor btnDeleteLeft_Close =(HtmlAnchor) e.Item.FindControl("btnDeleteLeft_Close");
            HtmlAnchor lnkL =(HtmlAnchor) e.Item.FindControl("lnkL");
            DataRowView drv = (DataRowView)e.Item.DataItem;
			
			lblPosLeft.Text = drv["POS_NO"] + " : " + drv["MGR_NAME"].ToString() + " ระดับ" + Convert.ToInt32(drv["PNPO_CLASS"]) + " หน่วยงาน " + drv["DEPT_NAME"].ToString();
			btnDeleteLeft.CommandArgument = drv["POS_NO"].ToString ();
			btnDeleteLeft_Close.Attributes["onclick"] = "document.getElementById('" + btnDeleteLeft.ClientID + "').click();";
			btnDeleteLeft_Close.Attributes["btnDeleteLeft_Close"] = drv["POS_NO"].ToString ();


			//-----------------เปิด/ปิด ปุ่มลบ ในตำแหน่งที่ตำแหน่งปัจจุบันตั้งค่า--------------

			
			//----------------Show Detail-----------------
			POS_NO_Detail = drv["POS_NO"].ToString ();
			//'-----ผลการประเมิน--,วันลา สาย ขาด ,โทษทางวินัย------
            Label lblAss_Min_Score = (Label)e.Item.FindControl("lblAss_Min_Score");
			Label lblLeave_Score = (Label)e.Item.FindControl("lblLeave_Score");
			Label lblPunishment =(Label) e.Item.FindControl("lblPunishment");

			DataTable DT_Path_Setting_Detail = Path_Setting_Detail(POS_NO_Detail);
			if (DT_Path_Setting_Detail.Rows.Count == 1) {
				if (Information.IsNumeric(DT_Path_Setting_Detail.Rows[0]["Ass_Min_Score"])) {
					lblAss_Min_Score.Text = "ไม่น้อยกว่า " + GL.StringFormatNumber(DT_Path_Setting_Detail.Rows[0]["Ass_Min_Score"]) + " คะแนน";
				} else {
					lblAss_Min_Score.Text = " ไม่ระบุ";
				}
				///**********คะแนนวันลา*****Arrive_Late******/
				if (Information.IsNumeric(DT_Path_Setting_Detail.Rows[0]["Leave_Score"])) {
					lblLeave_Score.Text = " ไม่เกิน " + GL.StringFormatNumber(DT_Path_Setting_Detail.Rows[0]["Leave_Score"]) + " วัน";
				} else {
					lblLeave_Score.Text = " ไม่ระบุ";
				}

                if (GL.CBOOL(DT_Path_Setting_Detail.Rows[0]["Punishment"]))
                {
					lblPunishment.Text = "ต้องไม่มีประวัติมีโทษทางวินัยใน 1 ปี ย้อนหลัง";
					lblPunishment.ForeColor = System.Drawing.Color.Red;
				} else {
					lblPunishment.Text = "ไม่ระบุโทษทางวินัย";
					lblPunishment.ForeColor = System.Drawing.Color.Gray;
				}

			} else {
				lblAss_Min_Score.Text = "ยังไม่ระบุ";
				lblLeave_Score.Text = "ยังไม่ระบุ";
				lblPunishment.Text = "ไม่ระบุโทษทางวินัย";
			}

			//'-----การฝึกอบรม----------
            Repeater rptCourse = (Repeater)e.Item.FindControl("rptCourse");
            Label lblCount_Course = (Label)e.Item.FindControl("lblCount_Course");
			rptCourse.ItemDataBound += rptCourse_ItemDataBound;
			DT_Course_Detail.DefaultView.RowFilter = "POS_NO=" + drv["POS_NO"];
			lblCount_Course.Text = GL.CINT (DT_Course_Detail.DefaultView.Count).ToString ();
			rptCourse.DataSource = DT_Course_Detail.DefaultView.ToTable().Copy();
			rptCourse.DataBind();


			//'-----การทดสอบ----------
            Repeater rptTest = (Repeater)e.Item.FindControl("rptTest");
            Label lblCount_Test = (Label)e.Item.FindControl("lblCount_Test");
			rptTest.ItemDataBound += rptTest_Detail_ItemDataBound;
			DT_Test_Detail.DefaultView.RowFilter = "POS_NO=" + drv["POS_NO"];
			lblCount_Test.Text = DT_Test_Detail.DefaultView.Count.ToString ();
			rptTest.DataSource = DT_Test_Detail.DefaultView.ToTable().Copy();
			rptTest.DataBind();

			lnkL.HRef = "SettingProperty.aspx?POS_NO=" + drv["POS_NO"];
		}


		public DataTable Path_Setting_Detail(string POS_NO_Detail)
		{
			string SQL = "";
			SQL += " SELECT * FROM tb_Path_Setting \n";
			SQL += " WHERE POS_No=" + POS_NO_Detail + "\n";
			SqlDataAdapter DA = new SqlDataAdapter(SQL, BL.ConnectionString());
			DataTable DT = new DataTable();
			DA.Fill(DT);
			return DT;
		}


		private void BindCourse_Detail()
		{
			string SQL = "";
			SQL += "   SELECT Training.POS_No,COURSE.COURSE_ID,COURSE.COURSE_DESC \n";
			SQL += "   FROM tb_Path_Setting_Training Training\n";
			SQL += "   LEFT JOIN tb_PK_COURSE COURSE ON Training.COURSE_ID=COURSE.COURSE_ID\n";
			SQL += "   ORDER BY Training.POS_No,COURSE.COURSE_ID,COURSE.COURSE_DESC\n";
			SqlDataAdapter DA = new SqlDataAdapter(SQL, BL.ConnectionString());
			DataTable DT = new DataTable();
			DA.Fill(DT);
			DT_Course_Detail = DT;
		}

		protected void rptCourse_ItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
		{
			if (e.Item.ItemType != ListItemType.AlternatingItem & e.Item.ItemType != ListItemType.Item)
				return;
			Label lblCourse = (Label)e.Item.FindControl("lblCourse");

            DataRowView drv = (DataRowView)e.Item.DataItem;
			
			lblCourse.Text = drv["COURSE_DESC"].ToString();

		}

		private void BindTest_Detail()
		{
			string SQL = "";
			SQL += " SELECT Setting_Test.POS_No,Test.CPSM_SUBJECT_CODE,Test.CPSM_SUBJECT_NAME,Setting_Test.Min_Score \n";
			SQL += " FROM tb_Path_Setting_Test Setting_Test\n";
			SQL += " LEFT JOIN tb_CP_SUBJECT_MASTER Test ON Setting_Test.SUBJECT_CODE=Test.CPSM_SUBJECT_CODE\n";
			SQL += " ORDER BY Setting_Test.POS_No,Test.CPSM_SUBJECT_CODE,Test.CPSM_SUBJECT_NAME\n";
			SqlDataAdapter DA = new SqlDataAdapter(SQL, BL.ConnectionString());
			DataTable DT = new DataTable();
			DA.Fill(DT);
			DT_Test_Detail = DT;
		}

		protected void rptTest_Detail_ItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
		{
			if (e.Item.ItemType != ListItemType.AlternatingItem & e.Item.ItemType != ListItemType.Item)
				return;
            Label lblTest = (Label)e.Item.FindControl("lblTest");
            Label lblMin_Score = (Label)e.Item.FindControl("lblMin_Score");

            DataRowView drv = (DataRowView)e.Item.DataItem;
			
			lblTest.Text = drv["CPSM_SUBJECT_NAME"].ToString();
			if (Information.IsNumeric(drv["Min_Score"])) {
				lblMin_Score.Text = GL.StringFormatNumber(drv["Min_Score"], 2);
			} else {
				lblMin_Score.Text = "0.00";
			}

		}




		protected void rptPosRight_ItemCommand(object source, System.Web.UI.WebControls.RepeaterCommandEventArgs e)
		{
            Button btnDeleteRight = (Button)e.Item.FindControl("btnDeleteRight");
            string POS_Right;
			switch (e.CommandName) {
				case "Delete":
					
					 POS_Right = btnDeleteRight.CommandArgument;
					string SQL = "";
					SQL += "  SELECT *  FROM  tb_Path_Setting_Route \n";
					SQL += "  WHERE From_POS_No =" + POS_NO;
					SQL += "  AND To_POS_No=" + POS_Right;
					SQL += "  AND Route='Right' ";
					SqlDataAdapter DA = new SqlDataAdapter(SQL, BL.ConnectionString());
					DataTable DT = new DataTable();
					DA.Fill(DT);
					if (DT.Rows.Count == 1) {
						SQL = "  ";
						SQL += "  Delete From tb_Path_Setting_Route";
						SQL += "  WHERE From_POS_No =" + POS_NO;
						SQL += "  AND To_POS_No=" + POS_Right;
						SQL += "  AND Route='Right' ";
						DA = new SqlDataAdapter(SQL, BL.ConnectionString());
						DT = new DataTable();
						DA.Fill(DT);
					} else {
						SQL = "";
						SQL += "  SELECT *  FROM  tb_Path_Setting_Route WHERE  To_POS_No=" + POS_NO;
						SQL += "  AND From_POS_No=" + POS_Right;
						SQL += "  AND Route='Left' ";
						DA = new SqlDataAdapter(SQL, BL.ConnectionString());
						DT = new DataTable();
						DA.Fill(DT);

						if (DT.Rows.Count > 0) {
							SQL = "";
							SQL += "  DELETE  FROM  tb_Path_Setting_Route WHERE  To_POS_No=" + POS_NO;
							SQL += "  AND From_POS_No=" + POS_Right;
							SQL += "  AND Route='Left' ";
							DA = new SqlDataAdapter(SQL, BL.ConnectionString());
							DT = new DataTable();
							DA.Fill(DT);
						}
						//ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "showAlert('ไม่สามารถลบตำแหน่งที่ ตำแหน่ง " & POS_NO & "  ไม่ได้กำหนด');", True)
						//Exit Sub
					}
					BindPosRight();

					break;
				case "lnkCurrentFromRight":
                    
					 POS_Right = btnDeleteRight.CommandArgument;
					POS_NO = POS_Right;
					CurrentPos();
					break;
			}
		}

		protected void rptPosRight_ItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
		{
			if (e.Item.ItemType != ListItemType.AlternatingItem & e.Item.ItemType != ListItemType.Item)
				return;
            Label lblPosRight = (Label)e.Item.FindControl("lblPosRight");
            Button btnDeleteRight = (Button)e.Item.FindControl("btnDeleteRight");
            HtmlAnchor lnkR = (HtmlAnchor)e.Item.FindControl("lnkR");
			HtmlAnchor btnDeleteRight_Close = (HtmlAnchor)e.Item.FindControl("btnDeleteRight_Close");
            DataRowView drv = (DataRowView)e.Item.DataItem;

			lblPosRight.Text = drv["POS_NO"] + " : " + drv["MGR_NAME"].ToString() + " ระดับ" + Convert.ToInt32(drv["PNPO_CLASS"]) + " หน่วยงาน " + drv["DEPT_NAME"].ToString();
			btnDeleteRight.CommandArgument = drv["POS_NO"].ToString ();
			btnDeleteRight_Close.Attributes["onclick"] = "document.getElementById('" + btnDeleteRight.ClientID + "').click();";
            btnDeleteRight_Close.Attributes["btnDeleteRight_Close"] = drv["POS_NO"].ToString();

			//----------------Show Detail-----------------
			POS_NO_Detail = drv["POS_NO"].ToString ();
			//'-----ผลการประเมิน--,วันลา สาย ขาด ,โทษทางวินัย------
			Label lblAss_Min_Score = (Label)e.Item.FindControl("lblAss_Min_Score");
			Label lblLeave_Score = (Label)e.Item.FindControl("lblLeave_Score");
			Label lblPunishment =(Label) e.Item.FindControl("lblPunishment");

			DataTable DT_Path_Setting_Detail = Path_Setting_Detail(POS_NO_Detail);
			if (DT_Path_Setting_Detail.Rows.Count == 1) {
				if (Information.IsNumeric(DT_Path_Setting_Detail.Rows[0]["Ass_Min_Score"])) {
					lblAss_Min_Score.Text = "ไม่น้อยกว่า " + GL.StringFormatNumber(DT_Path_Setting_Detail.Rows[0]["Ass_Min_Score"]) + " คะแนน";
				} else {
					lblAss_Min_Score.Text = " ไม่ระบุ";
				}
				///**********คะแนนวันลา*****Arrive_Late******/
				if (Information.IsNumeric(DT_Path_Setting_Detail.Rows[0]["Leave_Score"])) {
					lblLeave_Score.Text = " ไม่เกิน " + GL.StringFormatNumber(DT_Path_Setting_Detail.Rows[0]["Leave_Score"]) + " วัน";
				} else {
					lblLeave_Score.Text = " ไม่ระบุ";
				}

				if (GL.CBOOL(DT_Path_Setting_Detail.Rows[0]["Punishment"])) {
					lblPunishment.Text = "ต้องไม่มีประวัติมีโทษทางวินัยใน 1 ปี ย้อนหลัง";
					lblPunishment.ForeColor = System.Drawing.Color.Red;
				} else {
					lblPunishment.Text = "ไม่ระบุโทษทางวินัย";
					lblPunishment.ForeColor = System.Drawing.Color.Gray;
				}

			} else {
				lblAss_Min_Score.Text = "ยังไม่ระบุ";
				lblLeave_Score.Text = "ยังไม่ระบุ";
				lblPunishment.Text = "ไม่ระบุโทษทางวินัย";
			}

			//'-----การฝึกอบรม----------
            Repeater rptCourse = (Repeater)e.Item.FindControl("rptCourse");
            Label lblCount_Course = (Label)e.Item.FindControl("lblCount_Course");
			rptCourse.ItemDataBound += rptCourse_ItemDataBound;
			DT_Course_Detail.DefaultView.RowFilter = "POS_NO=" + drv["POS_NO"];
            lblCount_Course.Text = DT_Course_Detail.DefaultView.Count.ToString();
			rptCourse.DataSource = DT_Course_Detail.DefaultView.ToTable().Copy();
			rptCourse.DataBind();


			//'-----การทดสอบ----------
            Repeater rptTest = (Repeater)e.Item.FindControl("rptTest");
			Label lblCount_Test =(Label) e.Item.FindControl("lblCount_Test");
			rptTest.ItemDataBound += rptTest_Detail_ItemDataBound;
			DT_Test_Detail.DefaultView.RowFilter = "POS_NO=" + drv["POS_NO"];
            lblCount_Test.Text = DT_Test_Detail.DefaultView.Count.ToString();
			rptTest.DataSource = DT_Test_Detail.DefaultView.ToTable().Copy();
			rptTest.DataBind();

			lnkR.HRef = "SettingProperty.aspx?POS_NO=" + drv["POS_NO"];
		}



		#endregion

		private void BindPosLevel()
		{
			string SQL = "";
			SQL += " SELECT DEPT.SECTOR_CODE,DEPT.SECTOR_NAME,DEPT.DEPT_CODE,DEPT.DEPT_NAME,POS.POS_NO,ISNULL(POS.MGR_NAME,'-') MGR_NAME,ISNULL(POS.FLD_NAME,'-') FLD_NAME,POS.PNPO_CLASS\n";
			SQL += "  FROM vw_PN_Position POS\n";
			SQL += " INNER JOIN vw_HR_Department DEPT ON POS.DEPT_CODE=DEPT.DEPT_CODE AND POS.MINOR_CODE=DEPT.MINOR_CODE\n";
			SQL += " LEFT JOIN vw_PN_PSNL PSN ON POS.POS_NO=PSN.POS_NO\n";
			SQL += " INNER JOIN vw_Path_Setting_Status Setting_Status ON Setting_Status.POS_No = POS.POS_NO\n";
			SQL += " WHERE POS.PNPO_CLASS Is Not NULL \n";
			SQL += " AND POS.POS_NO Not IN (" + POS_NO + ")\n";
			SQL += " AND POS.PNPO_CLASS>=" + Conversion.Val(lblClass_Head.Text) + "\n";
			SQL += " AND DEPT.SECTOR_CODE=" + SECTOR_CODE + "\n";
			SQL += " AND DEPT.DEPT_CODE=" + DEPT_CODE + "\n";
			SQL += " GROUP BY DEPT.SECTOR_NAME,DEPT.DEPT_NAME,POS.POS_NO,POS.FLD_NAME,POS.MGR_NAME,\n";
			SQL += " POS.PNPO_CLASS, DEPT.SECTOR_CODE, DEPT.DEPT_CODE, POS.PNPO_CLASS, POS.POS_NO\n";
			SQL += " ORDER BY DEPT.SECTOR_CODE,DEPT.DEPT_CODE,POS.PNPO_CLASS DESC,POS.POS_NO\n";

			SqlDataAdapter DA = new SqlDataAdapter(SQL, BL.ConnectionString());
			DataTable DT = new DataTable();
			DA.Fill(DT);
			rptPosLevel.DataSource = DT;
			rptPosLevel.DataBind();
		}




		protected void btnAddRight_ServerClick(object sender, System.EventArgs e)
		{
			//------------- Get Existing  List ---------------
			lblModePos.Text = "Right";
			Mode_Pos = lblModePos.Text;
			string _Poslist = "";
			foreach (RepeaterItem item in rptPosRight.Items) {
				if (item.ItemType != ListItemType.AlternatingItem & item.ItemType != ListItemType.Item)
					continue;
				Button btnDeleteRight =(Button) item.FindControl("btnDeleteRight");
				_Poslist += "'" + btnDeleteRight.CommandArgument + "'" + ",";
			}
			if (!string.IsNullOrEmpty(_Poslist)) {
				EXIST_Pos += _Poslist.Substring(0, _Poslist.Length - 1);
			} else {
				EXIST_Pos += "";
			}

			BindPostList();
			ModalPost.Visible = true;
			ScriptManager.RegisterStartupScript(this.Page, typeof(string), "Focus", "document.getElementById('" + txtSearchPos.ClientID + "').focus();", true);
		}

		protected void rptPosLevel_ItemCommand(object source, System.Web.UI.WebControls.RepeaterCommandEventArgs e)
		{
			switch (e.CommandName) {
				case "lnkCurrentFromTop":
                    Button btnDeleteTop = (Button)e.Item.FindControl("btnDeleteTop");
					string POS_Top = btnDeleteTop.CommandArgument;
					POS_NO = POS_Top;
					CurrentPos();
					break;
			}

		}


		protected void rptPosLevel_ItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
		{
			if (e.Item.ItemType != ListItemType.AlternatingItem & e.Item.ItemType != ListItemType.Item)
				return;
			Label lblPosLevel =(Label) e.Item.FindControl("lblPosLevel");
			Button btnDeleteTop =(Button) e.Item.FindControl("btnDeleteTop");

            DataRowView drv = (DataRowView)e.Item.DataItem;
			
			lblPosLevel.Text = drv["POS_NO"] + " : " + drv["MGR_NAME"].ToString() + " ระดับ" + GL.CINT (drv["PNPO_CLASS"]).ToString () + " หน่วยงาน " + drv["DEPT_NAME"].ToString();
			btnDeleteTop.CommandArgument = drv["POS_NO"].ToString ();
		}


		protected void btnModalPost_Close_ServerClick(object sender, System.EventArgs e)
		{
			ModalPost.Visible = false;
			Mode_Pos = "";
		}

		protected void btnModalTest_Close_ServerClick(object sender, System.EventArgs e)
		{
			ModalTest.Visible = false;
		}

		protected void btnModalTraining_Close_ServerClick(object sender, System.EventArgs e)
		{
			ModalTraining.Visible = false;
		}
		public CP_Setting_Property_Team()
		{
			Load += Page_Load;
		}
	}
}

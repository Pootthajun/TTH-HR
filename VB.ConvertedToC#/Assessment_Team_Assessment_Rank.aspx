﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPage.Master" CodeBehind="Assessment_Team_Assessment_Rank.aspx.cs" Inherits="VB.Assessment_Team_Assessment_Rank" %>

<%@ Register src="WUC_DateReporter.ascx" tagname="WUC_DateReporter" tagprefix="uc1" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<asp:ToolkitScriptManager ID="toolkit1" runat="server"></asp:ToolkitScriptManager>

<asp:UpdatePanel ID="UpdatePanel1" runat="server">      
<ContentTemplate>

<div class="container-fluid">
				<!-- BEGIN PAGE HEADER-->
				<div class="row-fluid" id="fixedPageHeader">
					<div class="span12"> 
						<!-- BEGIN PAGE TITLE & BREADCRUMB-->
						<h3 class="page-title">เกณฑ์สรุปการประเมินผลพนักงาน  <font color="blue">(ScreenID : S-MGR-01)</font></h3>
						
									
						<ul class="breadcrumb">
                            <li>
                            	<i class="icon-th-list"></i><a href="javascript:;">การประเมินพนักงาน</a><i class="icon-angle-right"></i>
                            </li>
                        	<li>
                        	    <i class="icon-btc"></i> <a href="javascript:;">เกณฑ์สรุปการประเมินผลพนักงาน</a>
                        	</li>
                        	                        	
                            <uc1:WUC_DateReporter ID="WUC_DateReporter1" runat="server" />
                        </ul>
						<!-- END PAGE TITLE & BREADCRUMB-->									        
				     </div>			
			    </div>
		        <!-- END PAGE HEADER-->
                                        <div class="portlet-body form">
											<!-- BEGIN FORM-->
											<div class="form-horizontal form-view">
												           
												<h4 class="form-section"><i class="icon-btc"></i> ช่วงคะแนนปี
												
												<asp:DropDownList ID="ddlYear" OnSelectedIndexChanged="ddlYear_SelectedIndexChanged" runat="server" AutoPostBack="true" CssClass="medium m-wrap" style="font-size:20px; margin-top:-5px;">
							                    </asp:DropDownList>
												
												สำหรับผู้ถูกประเมินของ
												<asp:DropDownList ID="ddlAssessor" OnSelectedIndexChanged="ddl_SelectedIndexChanged" runat="server" AutoPostBack="true" CssClass="m-wrap" style="font-size:20px; width:auto !important; margin-top:-5px; padding:0px;">
							                    </asp:DropDownList>
												 
												
												</h4>
                                                <asp:Panel ID="pnlActionRank" runat="server"  Visible="true">
												<div class="row-fluid">
													<div class="span12 ">
														<div class="control-group">
															<label class="control-label">&nbsp;</label>
															
															    <div class="controls">
															    <div class="span6">
																    <div class="portlet-body no-more-tables">
								                                        <table class="table table-full-width dataTable no-more-tables table-hover">
									                                        <thead>
										                                        <tr>
											                                        <th style="text-align:center;"><i class="icon-btc"></i> กำหนดสรุปผลการประเมิน </th>
											                                        <th ><i class="icon-list-ol"></i> จากคะแนน</th>
											                                        <th ><i class="icon-list-ol"></i> ถึง</th>	
											                                        <th >&nbsp;</th>										                                        
										                                        </tr>
									                                        </thead>
									                                        <tbody>
									                                            <asp:Repeater ID="rptRank" OnItemCommand="rptRank_ItemCommand" OnItemDataBound="rptRank_ItemDataBound" runat="server">
									                                                <ItemTemplate>
									                                                    <tr>
											                                                <td data-title="ขั้น" style="text-align:center; font-weight:bold;">
											                                                    <asp:Label ID="lblNo" runat="server" style="display:none;"></asp:Label>
											                                                    <asp:Label ID="lblName" runat="server"></asp:Label>
											                                                </td>
											                                                <td data-title="จากคะแนน"><asp:TextBox ID="txtStart" runat="server" CssClass="m-wrap small" style="text-align:center;"></asp:TextBox></td>
											                                                <td data-title="ถึง"><asp:TextBox ID="txtEnd" runat="server" CssClass="m-wrap small" style="text-align:center;"></asp:TextBox></td>
											                                                
										                                                </tr>
									                                                </ItemTemplate>
									                                               
									                                            </asp:Repeater>						
									                                        </tbody>
								                                        </table>
							                                        </div>
															    </div>
															</div>
														</div>
													</div>
												</div>
												</asp:Panel>
												<asp:Panel ID="pnlAction" runat="server" CssClass="form-actions" Visible="true">
												    <asp:Button ID="btnSave" OnClick="btnSave_Click" CssClass="btn blue" runat="server" Text="ยืนยันบันทึก" />
												    <asp:Button ID="btnCancel" OnClick="ddl_SelectedIndexChanged" CssClass="btn" runat="server" Text="ยกเลิก" />
												    <asp:ConfirmButtonExtender ID="cfmbtnSave" ConfirmText="ยืนยันบันทึก" TargetControlID="btnSave" runat="server" ></asp:ConfirmButtonExtender>
												</asp:Panel>
											</div>
											<!-- END FORM-->  
										</div>
         
                             
              			
</div>
</ContentTemplate>           
</asp:UpdatePanel>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ScriptPlaceHolder" Runat="Server">
</asp:Content>



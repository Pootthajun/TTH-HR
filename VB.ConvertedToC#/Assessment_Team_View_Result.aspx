﻿<%@ Page  Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPage.Master" CodeBehind="Assessment_Team_View_Result.aspx.cs" Inherits="VB.Assessment_Team_View_Result" %>

<%@ Register src="WUC_DateReporter.ascx" tagname="WUC_DateReporter" tagprefix="uc1" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<asp:ToolkitScriptManager ID="toolkit1" runat="server"></asp:ToolkitScriptManager>

<asp:UpdatePanel ID="UpdatePanel1" runat="server">      
<ContentTemplate>


<div class="container-fluid">
				<!-- BEGIN PAGE HEADER-->
				<div class="row-fluid" id="fixedPageHeader">
					<div class="span12"> 
						<!-- BEGIN PAGE TITLE & BREADCRUMB-->
						<h3 class="page-title">รายงานสรุปผลการประเมิน  <font color="blue">(ScreenID : S-MGR-06)</font></h3>
						
									
						<ul class="breadcrumb">
                            <li>
                            	<i class="icon-th-list"></i><a href="javascript:;">การประเมินพนักงาน</a><i class="icon-angle-right"></i>
                            </li>
                        	<li>
                        	    <i class="icon-sort-by-attributes-alt"></i> <a href="javascript:;">รายงานสรุปผลการประเมิน</a>
                        	</li>
                        	                        	
                            <uc1:WUC_DateReporter ID="WUC_DateReporter1" runat="server" />
                        </ul>
						<!-- END PAGE TITLE & BREADCRUMB-->									        
				     </div>			
			    </div>
		        <!-- END PAGE HEADER-->
                                     
         
                             
              			
</div>
</ContentTemplate>           
</asp:UpdatePanel>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ScriptPlaceHolder" Runat="Server">
</asp:Content>

﻿<%@ Page  Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPage.Master" CodeBehind="RPT_A_Yearly_Result.aspx.cs" Inherits="VB.RPT_A_Yearly_Result" %>

<%@ Register src="WUC_DateReporter.ascx" tagname="WUC_DateReporter" tagprefix="uc1" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<%@ Register src="PageNavigation.ascx" tagname="PageNavigation" tagprefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<asp:ToolkitScriptManager ID="toolkit1" runat="server"></asp:ToolkitScriptManager>

<asp:UpdatePanel ID="UpdatePanel1" runat="server">      
<ContentTemplate>
<div class="container-fluid">
				<!-- BEGIN PAGE HEADER-->
				<div class="row-fluid" id="fixedPageHeader">
					<div class="span12"> 
						<!-- BEGIN PAGE TITLE & BREADCRUMB-->
						<h3>สรุปผลการประเมินประจำปีของพนักงาน <font color="blue"> (ScreenID : R-ALL-03)</font>
                        <asp:Label ID="lblReportTime" Font-Size="14px" ForeColor="Green" runat="Server"></asp:Label>
                        </h3>						
						<ul class="breadcrumb">
                            <li>
                            	<i class="icon-copy"></i> <a href="javascript:;">รายงาน</a><i class="icon-angle-right"></i>
                            </li>
                            <li>
                            	<i class="icon-copy"></i> <a href="javascript:;">การประเมินผลตัวชี้วัดและสมรรถนะ</a><i class="icon-angle-right"></i>
                            </li>
                            <li>
                        	    <i class="icon-check"></i> <a href="javascript:;">สรุปผลการประเมินประจำปีของพนักงาน</a>
                        	</li>                      	
                        	
                            <uc1:WUC_DateReporter ID="WUC_DateReporter1" runat="server" />
                        </ul>
						<!-- END PAGE TITLE & BREADCRUMB-->						
				     </div>

				
			    </div>
		        <!-- END PAGE HEADER-->
				
                <asp:Panel ID="pnlList" runat="server" Visible="True" DefaultButton="btnSearch">
				<div class="row-fluid">
					
					<div class="span12">
						<!-- BEGIN SAMPLE TABLE PORTLET-->
                                          <div class="row-fluid form-horizontal">
                			                  <div class="btn-group pull-left">
                                                    <asp:Button ID="btnSearch" OnClick="btnSearch_Click"  runat="server" CssClass="btn green" Text="ค้นหา" />
                                              </div>                                        
 
						                     <div class="btn-group pull-right">                                    
								                <button data-toggle="dropdown" class="btn dropdown-toggle">พิมพ์ <i class="icon-angle-down"></i></button>										
								                <ul class="dropdown-menu pull-right">
                                               
                                                    <li><a href="Print/RPT_A_Yearly_Result.aspx?Mode=PDF" target="_blank">รูปแบบ PDF</a></li>
                                                    <li><a href="Print/RPT_A_Yearly_Result.aspx?Mode=EXCEL" target="_blank">รูปแบบ Excel</a></li>												
								                </ul>
							                </div>
                                        </div>
						                 
							            
						                 <div class="row-fluid form-horizontal">
						                             <div class="span5 ">
														<div class="control-group">
													        <label class="control-label"> ปี</label>
													        <div class="controls">
														        <asp:DropDownList ID="ddlYear" runat="server" CssClass="medium m-wrap">
							                                    </asp:DropDownList>
													        </div>
												        </div>
													</div>
													<div class="span5 ">
														<div class="control-group">
													        <label class="control-label"> ชื่อพนักงาน</label>
													        <div class="controls">
														        <asp:TextBox ID="txtName" runat="server" CssClass="medium m-wrap" Placeholder="ค้นหาชื่อพนักงาน/เลขประจำตัว"></asp:TextBox>		
													        </div>
												        </div>
													</div>	   
    										</div>
    										<div class="row-fluid form-horizontal">		
												     <div class="span5 ">
														<div class="control-group">
													        <label class="control-label"> ฝ่าย</label>
													        <div class="controls">
														        <asp:DropDownList ID="ddlSector" runat="server" CssClass="medium m-wrap">
														        </asp:DropDownList>
													        </div>
												        </div>
													</div>	
													
													<div class="span5 ">
														<div class="control-group">
													        <label class="control-label"> ชื่อหน่วยงาน</label>
													        <div class="controls">
														        <asp:TextBox ID="txtDeptName" runat="server" CssClass="medium m-wrap" Placeholder="ค้นหาชื่อฝ่าย/หน่วยงาน"></asp:TextBox>		
													        </div>
												        </div>
													</div>
											</div>
											<div class="row-fluid form-horizontal">
											    <div class="span12">
											        <div class="control-group">
											                <label class="control-label">ระดับ</label>
											                <div class="controls">
											                    <label class="checkbox">
											                        <asp:CheckBox ID="chkClass1" runat="server" Text=""/>&nbsp;1
											                    </label>
											                    <label class="checkbox">
											                        <asp:CheckBox ID="chkClass2" runat="server" Text=""/>&nbsp;2
											                    </label>
											                    <label class="checkbox">
											                        <asp:CheckBox ID="chkClass3" runat="server" Text=""/>&nbsp;3
											                    </label>
											                    <label class="checkbox">
											                        <asp:CheckBox ID="chkClass4" runat="server" Text=""/>&nbsp;4
											                    </label>
											                    <label class="checkbox">
											                        <asp:CheckBox ID="chkClass5" runat="server" Text=""/>&nbsp;5
											                    </label>
											                    <label class="checkbox">
											                        <asp:CheckBox ID="chkClass6" runat="server" Text=""/>&nbsp;6
											                    </label>
											                    <label class="checkbox">
											                        <asp:CheckBox ID="chkClass7" runat="server" Text=""/>&nbsp;7
											                    </label>
											                    <label class="checkbox">
											                        <asp:CheckBox ID="chkClass8" runat="server" Text=""/>&nbsp;8
											                    </label>
											                    <label class="checkbox">
											                        <asp:CheckBox ID="chkClass9" runat="server" Text=""/>&nbsp;9
											                    </label>
											                    <label class="checkbox">
											                        <asp:CheckBox ID="chkClass10" runat="server" Text=""/>&nbsp;10
											                    </label>
											                    <label class="checkbox">
											                        <asp:CheckBox ID="chkClass11" runat="server" Text=""/>&nbsp;11
											                    </label>
											                    <label class="checkbox">
											                        <asp:CheckBox ID="chkClass12" runat="server" Text=""/>&nbsp;12
											                    </label>
											                    <label class="checkbox">
											                        <asp:CheckBox ID="chkClass13" runat="server" Text=""/>&nbsp;13
											                    </label>
										                   </div>
											        </div>
											    </div>
											</div>
											<div class="row-fluid form-horizontal">
											    
											    
											        <div class="span5">
											            <div class="control-group">
										                    <label class="control-label">การประเมินตัวชี้วัด (KPI)</label>
										                    <div class="controls">
                                                                <label class="checkbox">
											                        <asp:CheckBox ID="chkKPINo" runat="server" Text=""/>&nbsp;ยังประเมินไม่เสร็จ
											                    </label>
                                                                <label class="checkbox">
											                        <asp:CheckBox ID="chkKPIYes" runat="server" Text=""/>&nbsp;ประเมินเสร็จสมบูรณ์
											                    </label>
										                    </div>
									                    </div>
											        </div>
											          
												    <div class="span6">
												        <div class="control-group">
													        <label class="control-label"> การประเมินสมรรถนะ (Competency)</label>
													        
                                                            <div class="controls">
                                                                <label class="checkbox">
											                        <asp:CheckBox ID="chkCOMPNo" runat="server" Text=""/>&nbsp;ยังประเมินไม่เสร็จ
											                    </label>
                                                                <label class="checkbox">
											                        <asp:CheckBox ID="chkCOMPYes" runat="server" Text=""/>&nbsp;ประเมินเสร็จสมบูรณ์
											                    </label>
										                    </div>
												        </div>
													</div>
											</div>
											
								    
								            
							<div class="portlet-body no-more-tables">
								<asp:Label ID="lblCountPSN" runat="server" Font-Size="14px" Font-Bold="true"></asp:Label>
								&nbsp;
								<asp:Label ID="lblTitle" runat="server" Font-Size="14px" Font-Bold="true" Width="100%" style="text-align:center"></asp:Label>
								<table class="table table-full-width table-bordered dataTable table-hover">
									<thead>
										<tr>
											<th rowspan="2" style="text-align:center;">พนักงาน</th>
											<th rowspan="2" style="text-align:center;">ตำแหน่ง</th>
											<th rowspan="2" style="text-align:center;">ระดับ</th>
                                            <th rowspan="2" style="text-align:center;">สถานภาพ</th> 
											<th colspan="3" style="text-align:center;">ครั้งที่ 1</th><%--/-2/--%>
											<th colspan="3" style="text-align:center;">ครั้งที่ 2</th><%--//--%>
											<th rowspan="2" style="text-align:center;">คะแนน<br>ประเมิน<br>เฉลี่ย</th>
											<th rowspan="2" style="text-align:center;">วันลา</th>
											<th rowspan="2" style="text-align:center;">คะแนน<br>วันลา</th>
											<th rowspan="2" style="text-align:center;">คะแนนสุทธิ<br>หลังหักวันลา</th>
										</tr>
										<tr>
										    <th style="text-align:center;" colspan="1"><asp:Label ID="lblHeaderKPI_1" Text="KPI" runat="server"></asp:Label></th><%--/-1/--%>
										    <th style="text-align:center;" colspan="1"><asp:Label ID="lblHeaderCOMP_1" Text="COMP" runat="server"></asp:Label></th><%--//--%>
										    <th style="text-align:center;">รวม 100%</th>
											<th style="text-align:center;" colspan="1"><asp:Label ID="lblHeaderKPI_2" Text="KPI" runat="server"></asp:Label></th><%--//--%>
										    <th style="text-align:center;" colspan="1"><asp:Label ID="lblHeaderCOMP_2" Text="COMP" runat="server"></asp:Label></th><%--//--%>
											<th style="text-align:center;">รวม 100%</th>
										</tr>
									</thead>
									<tbody>
										<asp:Repeater ID="rptASS" OnItemDataBound="rptASS_ItemDataBound" runat="server">
									        <ItemTemplate>
									            <tr id="tdSector" runat="server">
							                        <td colspan="18" style="text-align:center; font-weight:bold; font-size:18px; background-color:#EEEEEE;" >
							                            <asp:Label ID="lblSector" runat="server"></asp:Label>
							                        </td>
							                    </tr>
							                    <tr id="tdDept" runat="server">
							                        <td colspan="18" style="text-align:left; font-size:14px; font-weight:bold;" >
							                            <asp:Label ID="lblDept" runat="server"></asp:Label>
							                        </td>
							                    </tr>
									            <tr>    
									                <td data-title="พนักงาน" style="text-align:left;">
									                <asp:Label ID="lblPSNNo" runat="server"></asp:Label> : <asp:Label ID="lblPSNName" runat="server"></asp:Label>
									                </td>
									                <td data-title="ตำแหน่ง" ><asp:Label ID="lblPSNPos" runat="server"></asp:Label></td>
									                <td data-title="ระดับ" style="text-align:center;"><asp:Label ID="lblPSNClass" runat="server"></asp:Label></td>  
                                                    <td data-title="สถานะภาพ" style="text-align:center;"><asp:Label ID="lblSTAT" runat="server"></asp:Label></td>  
											        <td data-title="ประเมินตัวชี้วัด ครั้งที่ 1" style="text-align:center; display:none;"><a style=" display:none;" ID="img_KPI_Status_1" runat="server" target="_blank"></a></td>											        
											        <td data-title="คะแนนตัวชี้วัด ครั้งที่ 1" style="text-align:center;"><a ID="lbl_KPI_Score_1" runat="server" target="_blank"></a></td>
											        <td data-title="ประเมินสมรรถนะ ครั้งที่ 1" style="text-align:center;display:none;"><a style=" display:none;" ID="img_COMP_Status_1" runat="server" target="_blank"></a></td>
											        <td data-title="คะแนนสมรรถนะ ครั้งที่ 1" style="text-align:center;"><a ID="lbl_COMP_Score_1" runat="server" target="_blank"></a></td>
											        <td data-title="รวมคะแนนครั้งที่ 1" style="text-align:center; font-weight:bold;"><a ID="lbl_Score_1" runat="server" target="_blank" style="color:blue;"></a></td>
											        
											        <td data-title="ประเมินตัวชี้วัด ครั้งที่ 2" style="text-align:center; display:none;"><a style=" display:none;" ID="img_KPI_Status_2" runat="server" target="_blank"></a></td>
											        <td data-title="คะแนนตัวชี้วัด ครั้งที่ 2" style="text-align:center;"><a ID="lbl_KPI_Score_2" runat="server" target="_blank"></a></td>
											        <td data-title="ประเมินสมรรถนะ ครั้งที่ 2" style="text-align:center; display:none;"><a style=" display:none;" ID="img_COMP_Status_2" runat="server" target="_blank"></a></td>
											        <td data-title="คะแนนสมรรถนะ ครั้งที่ 2" style="text-align:center;"><a ID="lbl_COMP_Score_2" runat="server" target="_blank"></a></td>
											        <td data-title="รวมคะแนนครั้งที่ 2" style="text-align:center; font-weight:bold;"><a ID="lbl_Score_2" runat="server" target="_blank" style="color:blue;"></a></td>
											        
											        <td data-title="รวมคะแนนประเมิน" style="text-align:center;"><a ID="lbl_Score" runat="server" style="font-weight:bold; color:Green;" target="_blank"></a></td>
											        <td data-title="วันลา" style="text-align:center;"><a ID="lbl_Leave_Day" runat="server" style="font-weight:bold; color:red;" target="_blank"></a></td>
											        <td data-title="คะแนนวันลา" style="text-align:center;"><a ID="lbl_Leave_Score" runat="server" style="font-weight:bold; color:red;" target="_blank"></a></td>
											        <td data-title="คะแนนสุทธิหลังหักวันลา" style="text-align:center;"><a ID="lbl_Year_Result" runat="server" style="font-weight:bold; color:Green;" target="_blank"></a></td>
											    </tr>
									        </ItemTemplate>
										</asp:Repeater>
																							
									</tbody>
								</table>
								<asp:PageNavigation ID="Pager" OnPageChanging="Pager_PageChanging" MaximunPageCount="10" PageSize="20" runat="server" />
							</div>
					</div>
                </div>  
                </asp:Panel>  


</div>

</ContentTemplate>           
</asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ScriptPlaceHolder" Runat="Server">
</asp:Content>


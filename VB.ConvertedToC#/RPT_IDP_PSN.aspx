﻿<%@ Page  Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPage.Master" CodeBehind="RPT_IDP_PSN.aspx.cs" Inherits="VB.RPT_IDP_PSN" %>

<%@ Register src="WUC_DateReporter.ascx" tagname="WUC_DateReporter" tagprefix="uc1" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<%@ Register src="PageNavigation.ascx" tagname="PageNavigation" tagprefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<asp:ToolkitScriptManager ID="toolkit1" runat="server"></asp:ToolkitScriptManager>
<asp:UpdatePanel ID="UpdatePanel1" runat="server">
<ContentTemplate>
<div class="container-fluid">
                <!-- BEGIN PAGE HEADER-->
                <div class="row-fluid" id="fixedPageHeader">
					<div class="span12"> 
						<!-- BEGIN PAGE TITLE & BREADCRUMB-->
						<h3>แผนการพัฒนารายบุคคล <font color="blue">(ScreenID : S-HDR-17)</font></h3>					
						
						<ul class="breadcrumb">
                            <li>
                            	<i class="icon-book"></i> <a href="javascript:;">หลักสูตรการพัฒนา</a><i class="icon-angle-right"></i>
                            </li>
                        	<li>
                        	    <i class="icon-user"></i> <a href="javascript:;">แผนการพัฒนารายบุคคล</a>
                        	</li>                        	
                        	
                            <uc1:WUC_DateReporter ID="WUC_DateReporter1" runat="server" />
                        </ul>
						<!-- END PAGE TITLE & BREADCRUMB-->
									        
				     </div>			
			    </div>
		        <!-- END PAGE HEADER-->
		        
		        <asp:Panel ID="pnlList" runat="server" Visible="True">
                    <div class="row-fluid">
					
					    <div class="span12">
						    <!-- BEGIN SAMPLE TABLE PORTLET-->
                                    <div class="row-fluid form-horizontal">
                			                  <div class="btn-group pull-left">
                                                    <asp:Button ID="btnSearch" OnClick="btnSearch_Click"  runat="server" CssClass="btn green" Text="ค้นหา" />
                                              </div> 
                                    </div>
				                   	<asp:Panel CssClass="row-fluid form-horizontal" ID="pnlSearch" runat="server" >
									     
									     <div class="row-fluid form-horizontal">
									          <div class="span4 ">
											    <div class="control-group">
										            <label class="control-label"><i class="icon-retweet"></i> สำหรับรอบการประเมิน</label>
										            <div class="controls">
											            <asp:DropDownList ID="ddlRound" OnSelectedIndexChanged="ddlRound_SelectedIndexChanged" runat="server" AutoPostBack="false" CssClass="medium m-wrap">
											            </asp:DropDownList>
											        </div>
									            </div>
										    </div>	
										    <div class="span6 ">
											    <div class="control-group">
										            <label class="control-label"><i class="icon-user"></i> ชื่อ</label>
										            <div class="controls">
											            <asp:TextBox ID="txt_Search_Name" OnTextChanged="btnSearch_Click" runat="server" AutoPostBack="false" CssClass="m-wrap medium" placeholder="ค้นหาจากชื่อ/เลขประจำตัว/ตำแหน่ง"></asp:TextBox>
										            </div>
									            </div>
										    </div>	
									     </div>
									    <div class="row-fluid form-horizontal">
									        <div class="span4 ">
										        <div class="control-group">
										            <label class="control-label"><i class="icon-sitemap"></i> ชื่อหน่วยงาน</label>
										            <div class="controls">
											            <asp:TextBox ID="txt_Search_Organize" OnTextChanged="btnSearch_Click" runat="server" AutoPostBack="false" CssClass="m-wrap medium" placeholder="ค้นหาจากหน่วยงาน"></asp:TextBox>
											        </div>
									            </div>
										    </div>
										     <div class="span6 ">
											    <div class="control-group">
										            <label class="control-label"><i class="icon-list-ol"></i> กลุ่มระดับ</label>
										            <div class="controls">
											            <asp:DropDownList ID="ddl_Search_Class" OnSelectedIndexChanged="btnSearch_Click" runat="server" AutoPostBack="false" CssClass="medium m-wrap">
											            </asp:DropDownList>
											        </div>
									            </div>
										    </div>													    
									    </div>
									    <div class="row-fluid form-horizontal">
									        <div class="span4 ">
										        <div class="control-group">
										            <label class="control-label"><i class="icon-bolt"></i> ความคืบหน้า</label>
										            <div class="controls">
											            <asp:DropDownList ID="ddlStatus" OnSelectedIndexChanged="btnSearch_Click"  runat="server" AutoPostBack="false" CssClass="medium m-wrap">
											                <asp:ListItem Text="ทั้งหมด" Value="-1" Selected="true"></asp:ListItem>
											                <asp:ListItem Text="ยังไม่อนุมัติผลการประเมิน" Value="4"></asp:ListItem>
		                                                    <asp:ListItem Text="ประเมินเสร็จสมบูรณ์" Value="5"></asp:ListItem>
											            </asp:DropDownList>
											        </div>
									            </div>
										    </div>
									        <div class="span6 ">
										            <div class="control-group">
												        <label class="control-label"></label>
												        <div class="controls">
													        <asp:CheckBox ID="chkIsSet" OnCheckedChanged="btnSearch_Click"  runat="server" AutoPostBack="false" Text="" Checked="true" /> 
													        <span style="font-size:14px; position:relative; top:3px;">แสดงเฉพาะที่กำหนดแผนแล้ว</span>
												        </div>
											        </div>
										    </div>				
									    </div>
																																   
									</asp:Panel>
								            
						            <div class="portlet-body no-more-tables">
							            <asp:Label ID="lblCountPSN" runat="server" Font-Size="14px" Font-Bold="true"></asp:Label>
							            <table class="table table-striped table-full-width table-bordered dataTable table-hover">
								            <thead>
									            <tr>
										            <th> รอบ</th>
										            <th> ฝ่าย</th>
										            <th> หน่วยงาน</th>
										            <th> เลขประจำตัว</th>
										            <th> ชื่อ</th>
										            <th> ตำแหน่ง</th>
										            <th> ระดับ</th>
										            <th> สถานะการประเมิน</th>
                                                    <th> พิมพ์</th>
									            </tr>
								            </thead>
								            <tbody>
									            <asp:Repeater ID="rptCOMP" OnItemDataBound="rptCOMP_ItemDataBound" runat="server">
								                    <ItemTemplate>
								                        <tr>    
								                            <td data-title="รอบ"><asp:Label ID="lblRound" runat="server"></asp:Label></td>
								                            <td data-title="ฝ่าย"><asp:Label ID="lblSector" runat="server"></asp:Label></td>
										                    <td data-title="หน่วยงาน"><asp:Label ID="lblPSNOrganize" runat="server"></asp:Label></td>
										                    <td data-title="เลขประจำตัว"><asp:Label ID="lblPSNNo" runat="server"></asp:Label></td>
										                    <td data-title="ชื่อ"><asp:Label ID="lblPSNName" runat="server"></asp:Label></td>
										                    <td data-title="ตำแหน่ง"><asp:Label ID="lblPSNPos" runat="server"></asp:Label></td>
										                    <td data-title="ระดับ"><asp:Label ID="lblPSNClass" runat="server"></asp:Label></td>
										                    <td data-title="สถานะการประเมิน"><asp:Label ID="lblStatus" runat="server" ForeColor="Navy"></asp:Label></td>
                                                            <td data-title="พิมพ์ ">   
                                                               <a class="btn mini blue" id="btnPrint" runat="server" title="พิมพ์" style="cursor:pointer;"><i class="icon-print"></i></a> 
                                                            </td>
									                    </tr>	
								                    </ItemTemplate>
									            </asp:Repeater>
        																							
								            </tbody>
							            </table>
							            <asp:PageNavigation ID="Pager" OnPageChanging="Pager_PageChanging" MaximunPageCount="10" PageSize="20" runat="server" />
						            </div>
						
						<!-- END TABLE PORTLET-->
						
					</div>
                </div>
              </asp:Panel>
                
                   
</div>
</ContentTemplate>
</asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ScriptPlaceHolder" Runat="Server">
</asp:Content>


﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Threading;
using System.Globalization;
using System.Web.UI.HtmlControls;

namespace VB
{
    public partial class Setting_Transfer_COMP_Oracle : System.Web.UI.Page
    {
        HRBL BL = new HRBL();
        GenericLib GL = new GenericLib();
        Converter C = new Converter();

        public int R_Year
        {
            get
            {
                try
                {
                    return GL.CINT(GL.SplitString(ddlRound.Items[ddlRound.SelectedIndex].Value, "-")[0]);
                }
                catch (Exception ex)
                {
                    return 0;
                }
            }
        }

        public int R_Round
        {
            get
            {
                try
                {
                    return GL.CINT(GL.SplitString(ddlRound.Items[ddlRound.SelectedIndex].Value, "-")[1]);
                }
                catch (Exception ex)
                {
                    return 0;
                }
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if ((Session["USER_PSNL_NO"] == null))
            {
                ScriptManager.RegisterStartupScript(this.Page, typeof(string), "Alert", "showAlert('กรุณาลงชื่อเข้าใช้ระบบ');", true);
                Response.Redirect("Login.aspx");
                return;
            }

            if (!IsPostBack)
            {
                //----------------- ทำสองที่ Page Load กับ Update Status ---------------
                BL.BindDDlYearRound(ddlRound);
                ddlRound.Items.RemoveAt(0);
                BL.BindDDlSector(ddlSector);
                BindPersonalList();
            }

            Thread.CurrentThread.CurrentUICulture = new CultureInfo("th-TH");
        }

        protected void btnSearch_Click(object sender, System.EventArgs e)
        {
            BindPersonalList();
        }

        private void SetHeaderColumn()
        {
            string SQL = "SELECT * FROM tb_HR_Round WHERE R_Year=" + R_Year + " AND  R_Round=" + R_Round + "";
            SqlDataAdapter DA = new SqlDataAdapter(SQL, BL.ConnectionString());
            DataTable DT = new DataTable();
            DA.Fill(DT);

            if (DT.Rows.Count > 0)
            {
                //lbl_Wage.Text = "KPI " + GL.CINT(DT.Rows[0]["Weight_KPI"]) + "%";
                lbl_Wage.Text = "COMP " + GL.CINT(DT.Rows[0]["Weight_COMP"]) + "%";  
            }

        }
        private void BindPersonalList()
        {
            SetHeaderColumn();
            string SQL = "SELECT vw_Export_COMP_Result_Oracle.* \n";
            SQL += " ,CASE WHEN tb_Export_COMP_Status_Oracle.PSNL_NO IS NOT NULL THEN 1 ELSE 0 END Status_Oracle ";
            SQL += "FROM vw_Export_COMP_Result_Oracle\n";
            SQL += " LEFT JOIN tb_Export_COMP_Status_Oracle ON vw_Export_COMP_Result_Oracle.R_Year = tb_Export_COMP_Status_Oracle.R_Year AND  ";
            SQL += "           vw_Export_COMP_Result_Oracle.R_Round = tb_Export_COMP_Status_Oracle.R_Round  ";
            SQL += "           AND vw_Export_COMP_Result_Oracle.PSNL_NO = tb_Export_COMP_Status_Oracle.PSNL_NO  ";



            string Title = "ผลการประเมิน Competency ประจำรอบการประเมิน " + ddlRound.Items[ddlRound.SelectedIndex].Text;
            string Filter = "R_Year=" + R_Year + " AND R_Round=" + R_Round + " ";

            if (ddlSector.SelectedIndex > 0)
            {

                Filter += " AND ( SECTOR_CODE='" + ddlSector.Items[ddlSector.SelectedIndex].Value + "' " + "\n";
                if (ddlSector.Items[ddlSector.SelectedIndex].Value == "3200")
                {
                    Filter += " AND  DEPT_CODE NOT IN ('32000300'))    " + "\n";
                    //-----------ฝ่ายโรงงานผลิตยาสูบ 3
                }
                else if (ddlSector.Items[ddlSector.SelectedIndex].Value == "3203")
                {
                    Filter += " OR  DEPT_CODE IN ('32000300','32030000','32030100','32030200','32030300','32030500'))  " + "\n";
                    //-----------ฝ่ายโรงงานผลิตยาสูบ 4
                }
                else if (ddlSector.Items[ddlSector.SelectedIndex].Value == "3204")
                {
                    Filter += " OR  DEPT_CODE IN ('32040000','32040100','32040200','32040300','32040500','32040300'))  " + "\n";
                    //-----------ฝ่ายโรงงานผลิตยาสูบ 5
                }
                else if (ddlSector.Items[ddlSector.SelectedIndex].Value == "3205")
                {
                    Filter += " OR  DEPT_CODE IN ('32050000','32050100','32050200','32050300','32050500'))\t  " + "\n";
                }
                else
                {
                    Filter += ")   " + "\n";
                }


                Title += GL.SplitString(ddlSector.Items[ddlSector.SelectedIndex].Text, ":")[1].Trim() + " ";
            }

            if (!string.IsNullOrEmpty(txtDeptName.Text))
            {
                Filter += " AND (DEPT_Name LIKE '%" + txtDeptName.Text.Replace("'", "''") + "%' OR Sector_Name LIKE '%" + txtDeptName.Text.Replace("'", "''") + "%') " + "\n";
                Title += " หน่วยงาน '" + txtDeptName.Text + "' ";
            }
            if (!string.IsNullOrEmpty(txtName.Text))
            {
                Filter += " AND (PSNL_Fullname LIKE '%" + txtName.Text.Replace("'", "''") + "%' OR PSNL_NO LIKE '%" + txtName.Text.Replace("'", "''") + "%') " + "\n";
                Title += " พนักงาน '" + txtName.Text + "' ";
            }

            if (ddlStatus.SelectedIndex > 0)
            {
                Filter += " AND Status_Oracle='" + ddlStatus.SelectedValue + "' " + "\n";
            }


            if ((chkClass1.Checked & chkClass2.Checked & chkClass3.Checked & chkClass4.Checked & chkClass5.Checked & chkClass6.Checked & chkClass7.Checked & chkClass8.Checked & chkClass9.Checked & chkClass10.Checked & chkClass11.Checked & chkClass12.Checked & chkClass13.Checked) | (!chkClass1.Checked & !chkClass2.Checked & !chkClass3.Checked & !chkClass4.Checked & !chkClass5.Checked & !chkClass6.Checked & !chkClass7.Checked & !chkClass8.Checked & !chkClass9.Checked & !chkClass10.Checked & !chkClass11.Checked & !chkClass12.Checked & !chkClass13.Checked))
            {
                //----------- Do nothing--------
            }
            else
            {
                string _classFilter = "";
                string _classTitle = "";
                for (int i = 1; i <= 13; i++)
                {
                    CheckBox chk = (CheckBox)pnlList.FindControl("chkClass" + i);
                    if (chk.Checked)
                    {
                        _classFilter += "'" + i.ToString().PadLeft(2, GL.chr0) + "',";
                        _classTitle += i + ",";
                    }
                }
                Title += " ระดับ " + _classTitle.Substring(0, _classTitle.Length - 1);
                Filter += " AND PNPS_CLASS IN (" + _classFilter.Substring(0, _classFilter.Length - 1) + ") " + "\n";
            }

            int COMPStat = 0;
            if ((chkCOMPYes.Checked & chkCOMPNo.Checked) || (!chkCOMPYes.Checked & !chkCOMPNo.Checked))
            {
                COMPStat = -1;
            }
            else if (chkCOMPYes.Checked)
            {
                Filter += " AND COMP_Status=1" + " ";
                COMPStat = 1;
            }
            else if (chkCOMPNo.Checked)
            {
                Filter += " AND COMP_Status=0 " + " ";
                COMPStat = 0;
            }

            switch (COMPStat)
            {
                case 0:
                    Title += " ที่การประเมินสมรรถนะยังไม่เสร็จ";
                    break;
                case 1:
                    Title += " ที่การประเมินสมรรถนะเสร็จสมบูรณ์";
                    break;
            }

            SQL += "  ORDER BY SECTOR_CODE,DEPT_CODE,PNPS_CLASS DESC,POS_No" + "\n";


            SqlDataAdapter DA = new SqlDataAdapter(SQL, BL.ConnectionString());
            DA.SelectCommand.CommandTimeout = 200;
            DataTable DT = new DataTable();
            DA.Fill(DT);
            DT.DefaultView.RowFilter = Filter;
            Session["Setting_Transfer_COMP_Oracle"] = DT.DefaultView.ToTable();
            Pager.SesssionSourceName = "Setting_Transfer_COMP_Oracle";
            Pager.RenderLayout();

            if (DT.DefaultView .Count > 0)
            {
                btnSend_Top.Text = "Click เพื่อส่ง" + Title + " จำนวน " + GL.StringFormatNumber(DT.DefaultView.Count, 0) + " รายการ ไปยัง Oracle";
                btnSend_Bottom.Text = "Click เพื่อส่ง" + Title + " จำนวน " + GL.StringFormatNumber(DT.DefaultView.Count, 0) + " รายการ ไปยัง Oracle";
                btnSend_Top.CssClass = "btn green btn-block";
                btnSend_Top.Enabled = true;
            }
            else
            {
                btnSend_Top.Text = Title + " (ไม่พบรายการดังกล่าว)";
                btnSend_Top.CssClass = "btn btn-block";
                btnSend_Top.Enabled = false;
                btnSend_Bottom.Visible = false;
            }

        }

        protected void Pager_PageChanging(PageNavigation Sender)
        {
            Pager.TheRepeater = rptASS;
        }

        //------------ For Grouping -----------
        string LastSector = "";
        string LastDept = "";
        protected void rptASS_ItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType !=ListItemType.AlternatingItem && e.Item.ItemType !=ListItemType.Item) return;

            Label lblSector = (Label)e.Item.FindControl("lblSector");
            Label lblDept = (Label)e.Item.FindControl("lblDept");
            Label lblPSNNo = (Label)e.Item.FindControl("lblPSNNo");
            Label lblPSNName = (Label)e.Item.FindControl("lblPSNName");
            Label lblPSNPos = (Label)e.Item.FindControl("lblPSNPos");
            Label lblPSNClass = (Label)e.Item.FindControl("lblPSNClass");

            Label lblTotal = (Label)e.Item.FindControl("lblTotal");
            Label lblMark = (Label)e.Item.FindControl("lblMark");

            HtmlTableRow tdSector = (HtmlTableRow)e.Item.FindControl("tdSector");
            HtmlTableRow tdDept = (HtmlTableRow)e.Item.FindControl("tdDept");

            Image imgStatusOracle = (Image)e.Item.FindControl("imgStatusOracle");

           

            DataRowView drv = (DataRowView)e.Item.DataItem;

            //---------- แยกหน่วยงาน---------
            if (drv["SECTOR_NAME"].ToString() != LastSector)
            {
                LastSector = drv["SECTOR_NAME"].ToString();
                lblSector.Text = LastSector;
                LastDept = drv["DEPT_NAME"].ToString();
                lblDept.Text = LastDept;
                //---------- แยกสมรรถนะ---------
            }
            else if (LastDept != drv["DEPT_NAME"].ToString())
            {
                tdSector.Visible = false;
                LastDept = drv["DEPT_NAME"].ToString();
                lblDept.Text = LastDept;
            }
            else
            {
                tdSector.Visible = false;
                tdDept.Visible = false;
            }
            lblPSNNo.Text = drv["PSNL_NO"].ToString();
            lblPSNName.Text = drv["PSNL_Fullname"].ToString();
            lblPSNPos.Text = drv["POS_Name"].ToString();
            if (!GL.IsEqualNull(drv["PNPS_CLASS"]))
            {
                lblPSNClass.Text = GL.CINT(drv["PNPS_CLASS"]).ToString();
            }

            //lblTotal.Text = GL.StringFormatNumber(drv["TotalMark"]);
            if (!GL.IsEqualNull(drv["TotalMark"]))
            {
                lblTotal.Text = GL.StringFormatNumber(drv["TotalMark"]);
            }

            if (GL.CINT(drv["COMP_Status"]) == 1)
            {
                if (!GL.IsEqualNull(drv["Result"]))
                {
 lblMark.Text = GL.StringFormatNumber(drv["Result"]);
                lblMark.Style["color"] = "blue";
                }
               
            }
            else
            {
                if (!GL.IsEqualNull(drv["Result"]))
                {
                    if (GL.CDBL(drv["Result"]) != 0)
                    {
                        lblMark.Text = GL.StringFormatNumber(drv["Result"]);
                    }else
                {
                    lblMark.Text = "-";
                }

                }
                else
                {
                    lblMark.Text = "-";
                }
                
                lblMark.Style["color"] = "red";
            }


            // 20161101 MA เพิ่มสถานะส่งข้อมูลไปยัง Oracle
            if (drv["Status_Oracle"].ToString() == "1")
            {
                imgStatusOracle.ImageUrl = "images/check.png";
                imgStatusOracle.ToolTip = "โอนแล้ว";
            }
            else
            {
                imgStatusOracle.ImageUrl = "images/none.png";
                imgStatusOracle.ToolTip = "ยังโอนแล้ว";
            }



            //Label lblYear = (Label)e.Item.FindControl("lblYear");
            //Label lblRound = (Label)e.Item.FindControl("lblRound");
            //Label lblPSN = (Label)e.Item.FindControl("lblPSN");
            //Label lblTotal = (Label)e.Item.FindControl("lblTotal");
            //Label lblMark = (Label)e.Item.FindControl("lblMark");
            //Label lblWeight = (Label)e.Item.FindControl("lblWeight");
            //Label lblUpdateBy = (Label)e.Item.FindControl("lblUpdateBy");
            //Label lblUpdateTime = (Label)e.Item.FindControl("lblUpdateTime");

            //DataRowView drv = (DataRowView)e.Item.DataItem;

            //lblYear.Text=drv["R_Year"].ToString();
            //lblRound.Text = drv["R_Round"].ToString();
            //lblPSN.Text = drv["PSNL_NO"].ToString();
            //lblTotal.Text = drv["TotalMark"].ToString();
            //if (drv["Result"].ToString() == "" | drv["Result"].ToString() == "0")
            //{
            //    lblMark.ForeColor = System.Drawing.Color.Red;
            //}
            //else
            //{
            //    lblMark.ForeColor = System.Drawing.Color.Blue;
            //}
            //lblMark.Text = drv["Result"].ToString();
            //lblWeight.Text = drv["Weight_COMP"].ToString();
            //if (drv["Update_By"].ToString() == "System")
            //{
            //    lblUpdateBy.ForeColor = System.Drawing.Color.Green;
            //}
            //else {
            //    lblUpdateBy.ForeColor = System.Drawing.Color.Blue;
            //}
            //lblUpdateBy.Text = drv["Update_By"].ToString();
            //lblUpdateTime.Text =((DateTime)drv["Update_Time"]).ToString("dd MMM yyyy");
        }

        protected void btnSend_Click(object sender, System.EventArgs e)
        {
            DataTable DT = ((DataTable)Session["Setting_Transfer_COMP_Oracle"]).Copy();
            //----------------- Remove unused column ------------------
            try
            {
                String Update_By = Session["USER_PSNL_NO"].ToString();
                BL.Export_Assessment_COMP_Result_To_Oracle(DT);
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this.Page, typeof(string), "Alert", "showAlert('ไม่สำเร็จ<br><br>" + ex.Message.Replace("'", "\'").Replace("\n", "<br>") + "');", true);
                return;
            }
            BindPersonalList();
            ScriptManager.RegisterStartupScript(this.Page, typeof(string), "Alert", "showAlert('ส่งข้อมูลสำเร็จ');", true);

        }

		public Setting_Transfer_COMP_Oracle()
		{
			Load += Page_Load;
		}

    }
}
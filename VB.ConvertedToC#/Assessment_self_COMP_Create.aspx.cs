using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Xml.Linq;
using System.Diagnostics;
using System.Collections.Specialized;
using System.Configuration;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Caching;
using System.Web.SessionState;
using System.Web.Security;
using System.Web.Profile;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
namespace VB
{

	public partial class Assessment_self_COMP_Create : System.Web.UI.Page
	{

		HRBL BL = new HRBL();
		GenericLib GL = new GenericLib();

		textControlLib TC = new textControlLib();
		public int R_Year {
			get {
				try {
					return GL.CINT( GL.SplitString(ddlRound.Items[ddlRound.SelectedIndex].Value, "-")[0]);
				} catch (Exception ex) {
					return 0;
				}
			}
		}

		public int R_Round {
			get {
				try {
					return GL.CINT(GL.SplitString(ddlRound.Items[ddlRound.SelectedIndex].Value, "-")[1]);
				} catch (Exception ex) {
					return 0;
				}
			}
		}

		public string PSNL_NO {
			get {
				try {
					return Session["USER_PSNL_NO"].ToString ();
				} catch (Exception ex) {
					return "";
				}
			}
		}

        public int WAGE_TYPE
        {
            get { return (GL.CINT(lblCOMPStatus.Attributes["WAGE_TYPE"])); }
            set { lblCOMPStatus.Attributes["WAGE_TYPE"] = Convert.ToString(value); }
        }
        public int PNPS_CLASS
        {
            get { return (GL.CINT(lblCOMPStatus.Attributes["PNPS_CLASS"])); }
            set { lblCOMPStatus.Attributes["PNPS_CLASS"] = Convert.ToString(value); }
        }
		public string ASSESSOR_BY {
			get {
				try {
					string[] tmp = Strings.Split(ddlASSName.Items[ddlASSName.SelectedIndex].Value, ":::");
					return tmp[0];
				} catch (Exception ex) {
					return "";
				}
			}
		}

		public string ASSESSOR_NAME {
			get {
				try {
					return ddlASSName.Items[ddlASSName.SelectedIndex].Text;
				} catch (Exception ex) {
					return "";
				}
			}
		}

		public string ASSESSOR_POS {
			get {
				try {
					string[] tmp = Strings.Split(ddlASSName.Items[ddlASSName.SelectedIndex].Value, ":::");
					return tmp[1];
				} catch (Exception ex) {
					return "";
				}
			}
		}

		public string ASSESSOR_DEPT {
			get {
				try {
					string[] tmp = Strings.Split(ddlASSName.Items[ddlASSName.SelectedIndex].Value, ":::");
					return tmp[2];
				} catch (Exception ex) {
					return "";
				}
			}
		}
		
		public int COMP_Status {
			get { return (GL.CINT(lblCOMPStatus.Attributes["COMP_Status"])); }
			set { lblCOMPStatus.Attributes["COMP_Status"] = Convert.ToString(value); }
        }		
		
		public int PSNL_CLASS_GROUP {
			get {
				try {
					return Convert.ToInt32(Conversion.Val(lblName.Attributes["CLSGP_ID"]));
				} catch (Exception ex) {
					return 0;
				}
			}
            set { lblName.Attributes["CLSGP_ID"] = value.ToString(); }
		}

		public int PSNL_TYPE {
			get {
				try {
					return Convert.ToInt32(Conversion.Val(lblPSNType.Attributes["PSNL_Type_Code"]));
				} catch (Exception ex) {
					return -1;
				}
			}
            set { lblPSNType.Attributes["PSNL_Type_Code"] = value.ToString(); }
		}

		public string FN_CODE {
			get {
				try {
					return lblPSNPos.Attributes["FN_CODE"];
				} catch (Exception ex) {
					return "";
				}
			}
			set { lblPSNPos.Attributes["FN_CODE"] = value; }
		}

		public string FN_Type {
			get {
				try {
					return lblPSNType.Attributes["FN_Type"];
				} catch (Exception ex) {
					return "";
				}
			}
			set { lblPSNType.Attributes["FN_Type"] = value; }
		}

		public string FN_ID {
			get {
				try {
					return FN_CODE;
				} catch (Exception ex) {
					return "";
				}
			}
		}

        public string DEPT_CODE
        {
            get
            {
                try
                {
                    return lblPSNType.Attributes["DEPT_CODE"];
                }
                catch (Exception ex)
                {
                    return "";
                }
            }
            set { lblPSNType.Attributes["DEPT_CODE"] = value.ToString(); }
        }


		protected void Page_Load(object sender, System.EventArgs e)
		{
			if ((Session["USER_PSNL_NO"] == null)) {
				ScriptManager.RegisterStartupScript(this.Page, typeof(string), "Alert", "showAlert('กรุณาลงชื่อเข้าใช้ระบบ');", true);
				Response.Redirect("Login.aspx");
				return;
			}

			if (!IsPostBack) {			 
               
                //BL.BindDDlYearRound(ddlRound);
                // Keep Start Time
                DateTime StartTime = DateTime.Now;

                BL.BindDDlYearRound_ForTYPE(ddlRound, R_Year, R_Round, PSNL_NO);     //------------แสดงเฉพาะรอบที่มีสิทธิ์การเข้าประเมิน

                //----------------- ทำสองที่ Page Load กับ Update Status ---------------
                if (Session["ddlRound"] != null)
                {
                    ddlRound.SelectedIndex = GL.CINT(Session["ddlRound"]);
                }
                BL.Update_COMP_Status_To_Assessment_Period(PSNL_NO, R_Year, R_Round);
                SaveHeader();
                BindPersonal();
                First_AVG_Weight();
				BindAssessor();
				BindMasterCOMP();
				SetPrintButton();

                //SaveHeader();
                ////-----------Create First-------
                //BindMasterCOMP();
                //Report Time To Entry This Page
                DateTime EndTime = DateTime.Now;
                lblReportTime.Text = "เริ่มเข้าสู่หน้านี้ " + StartTime.ToString("HH:mm:ss.ff") + " ถึง " + EndTime.ToString("HH:mm:ss.ff") + " ใช้เวลา " + Math.Round( GL.DiffTimeDecimalSeconds(StartTime,EndTime),2) + " วินาที";
			}

			if (IsPostBack) {
				ScriptManager.RegisterStartupScript(this.Page, typeof(string), "Main", "activeLastFocusControl();", true);
			}

		}

        private void First_AVG_Weight()  //เข้ามาครั้งแรก ระบบเฉลี่ยให้ Auto
        {
            if (COMP_Status == 0 | GL.CINT(COMP_Status) == -1)
            {
                string SQL = "  SELECT COMP_Status \n";
                SQL += " FROM tb_HR_COMP_Header \n";
                SQL += " WHERE R_Year =" + R_Year + " And R_Round =" + R_Round + " AND PSNL_NO='" + PSNL_NO.Replace("'", "''") + "'\n";
                SqlDataAdapter DA = new SqlDataAdapter(SQL, BL.ConnectionString());
                DataTable DT = new DataTable();
                DA.Fill(DT);
                //if (DT.Rows.Count == 0)
                //{
                //    SaveHeader();
                //    AVG_Weight();
                //}else
                 if ((DT.Rows.Count > 0) || GL.IsEqualNull(DT.Rows[0]["COMP_Status"]))
                {
                    AVG_Weight();  //เฉลี่ย นน.
                }

            }

        }

        private void AVG_Weight()
        {
            DataTable DT = BL.GetCOMPBundle(PSNL_NO, R_Year, R_Round, PSNL_CLASS_GROUP, PSNL_TYPE, FN_ID, FN_Type, HRBL.CompetencyType.All);
            DT.DefaultView.Sort = " COMP_Type_Sort DESC ,Master_No DESC";
            DT = DT.DefaultView.ToTable();
            if (DT.Rows.Count > 0)
            {
                double AVG_Weight = (GL.CDBL(100) / GL.CDBL(DT.Rows.Count));
                Double SUM_Weight = 0;
                Double CDBL_MOD = (GL.CDBL(GL.StringFormatNumber(GL.CDBL(AVG_Weight), 2)) % GL.CDBL(0.50));
                //------Clere Detail (สำหรับใส่ตาม Master)-------
                string SQL_Del = "DELETE FROM tb_HR_COMP_Detail ";
                SQL_Del += " WHERE PSNL_NO='" + PSNL_NO.Replace("'", "''") + "'\n";
                SQL_Del += " AND R_Year=" + R_Year + "\n";
                SQL_Del += " AND R_Round=" + R_Round + "\n";
                SqlDataAdapter DADel = new SqlDataAdapter(SQL_Del, BL.ConnectionString());
                DataTable DTDel = new DataTable();
                DADel.Fill(DTDel);

                //------Save Detail (AVG Weight)-------
                string SQL = "SELECT * FROM tb_HR_COMP_Detail ";
                SQL += " WHERE PSNL_NO='" + PSNL_NO.Replace("'", "''") + "'\n";
                SQL += " AND R_Year=" + R_Year + "\n";
                SQL += " AND R_Round=" + R_Round + "\n";
                SqlDataAdapter DA = new SqlDataAdapter(SQL, BL.ConnectionString());
                DataTable DTAVG = new DataTable();
                DA.Fill(DTAVG);

                for (int i = 0; i <= DT.Rows.Count - 1; i++)
                {
                    DataRow DR = null;
                    DR = DTAVG.NewRow();
                    DR["PSNL_NO"] = PSNL_NO;
                    DR["R_Year"] = R_Year;
                    DR["R_Round"] = R_Round;
                    DR["COMP_Type_Id"] = DT.Rows[i]["COMP_Type_Id"];
                    DR["Master_No"] = DT.Rows[i]["Master_No"];
                    DR["Create_By"] = Session["USER_PSNL_NO"];
                    DR["Create_Time"] = DateAndTime.Now;

                    DR["COMP_Comp"] = DT.Rows[i]["COMP_Comp"].ToString();
                    DR["COMP_Std"] = DT.Rows[i]["COMP_Std"].ToString();
                    for (int j = 1; j <= 5; j++)
                    {
                        DR["COMP_Choice_" + j] = DT.Rows[i]["COMP_Choice_" + j];
                    }
                    if (GL.CINT(DT.Rows[i]["COMP_No"]) != 1)
                    {
                        if (GL.CDBL(CDBL_MOD) == 0)
                        {
                            DR["COMP_Weight"] = GL.StringFormatNumber(AVG_Weight);
                            SUM_Weight += GL.CDBL(GL.StringFormatNumber(AVG_Weight));
                        }
                        else
                        {
                            DR["COMP_Weight"] = Math.Floor(AVG_Weight);
                            SUM_Weight += Math.Floor(AVG_Weight);
                        }
                    }
                    else
                    {
                        DR["COMP_Weight"] = (GL.CDBL(100) - GL.CDBL(SUM_Weight));
                    }
                    DR["Update_By"] = Session["USER_PSNL_NO"];
                    DR["Update_Time"] = DateAndTime.Now;
                    DTAVG.Rows.Add(DR);
                }
                SqlCommandBuilder cmd = new SqlCommandBuilder(DA);
                DA.Update(DTAVG);
            }
            else
            {
                return;
            }
        }



		private void BindPersonal()
		{
			HRBL.PersonalInfo PSNInfo = BL.GetAssessmentPersonalInfo(R_Year, R_Round, PSNL_NO, COMP_Status);
			lblPSNName.Text = PSNInfo.PSNL_Fullname;
			lblPSNDept.Text = PSNInfo.DEPT_NAME;
			if (!string.IsNullOrEmpty(PSNInfo.MGR_NAME)) {
				lblPSNPos.Text = PSNInfo.MGR_NAME;
			} else {
				lblPSNPos.Text = PSNInfo.FN_NAME;
			}
			lblPSNType.Text = "พนักงาน " + PSNInfo.WAGE_NAME + " ระดับ " + Convert.ToInt32(PSNInfo.PNPS_CLASS);

			PSNL_CLASS_GROUP = GL.CINT ( PSNInfo.CLSGP_ID);
			PSNL_TYPE = GL.CINT ( PSNInfo.PSNL_TYPE);
			FN_CODE = PSNInfo.FN_ID;
			FN_Type = PSNInfo.FN_Type;
            DEPT_CODE = PSNInfo.DEPT_CODE;
            WAGE_TYPE = GL.CINT(PSNInfo.WAGE_TYPE);
            PNPS_CLASS = GL.CINT(PSNInfo.PNPS_CLASS);
		}

		private void BindAssessor()
		{
			BL.BindDDLAssessor(ddlASSName, R_Year, R_Round, PSNL_NO);
			ddlASSName_SelectedIndexChanged(null, null);
		}

		protected void ddlASSName_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			lblASSPos.Text = ASSESSOR_POS;
			lblASSDept.Text = ASSESSOR_DEPT;
			SaveHeader();
		}

		protected void ddlRound_SelectedIndexChanged(object sender, System.EventArgs e)
		{
            //COMP_Status = BL.GetCOMPStatus(R_Year, R_Round, PSNL_NO);
            //BindPersonal();
            //BindAssessor();
            //BindMasterCOMP();

            //SetPrintButton();

            GoToSelfCOMP();

		}

        private void GoToSelfCOMP()
        {
            Session["ddlRound"] = ddlRound.SelectedIndex;
            Session["R_Year"] = R_Year;
            Session["R_Round"] = R_Round;

            int COMP_Status = BL.GetCOMPStatus(R_Year, R_Round, Session["USER_PSNL_NO"].ToString());
            if (COMP_Status < HRBL.AssessmentStatus.WaitCreatingApproved)
            {
                Response.Redirect("Assessment_self_COMP_Create.aspx");
            }
            else
            {
                Response.Redirect("Assessment_self_COMP_Assessment.aspx");
            }

        }

		private void SetPrintButton()
		{
			btnCOMPFormPDF.HRef = "Print/RPT_C_PNSAss.aspx?MODE=PDF&R_Year=" + R_Year + "&R_Round=" + R_Round + "&PSNL_No=" + PSNL_NO + "&Status=" + GL.CINT(COMP_Status);
            //btnCOMPFormExcel.HRef = "Print/RPT_C_PNSAss.aspx?MODE=EXCEL&R_Year=" + R_Year + "&R_Round=" + R_Round + "&PSNL_No=" + PSNL_NO + "&Status=" + GL.CINT(COMP_Status);
            btnCOMPFormExcel.HRef = "Print/RPT_C_PNSAss.aspx?MODE=EXCEL&R_Year=" + R_Year + "&R_Round=" + R_Round + "&PSNL_No=" + PSNL_NO + "&Status=" + COMP_Status;


			btnIDPFormPDF.HRef = "Print/RPT_IDP_PSN.aspx?MODE=PDF&R_Year=" + R_Year + "&R_Round=" + R_Round + "&PSNL_No=" + PSNL_NO + "&Status=" + GL.CINT(COMP_Status);
			btnIDPFormExcel.HRef = "Print/RPT_IDP_PSN.aspx?MODE=EXCEL&R_Year=" + R_Year + "&R_Round=" + R_Round + "&PSNL_No=" + PSNL_NO + "&Status=" + GL.CINT(COMP_Status);

            btnIDPResultFormPDF.HRef = "Print/RPT_IDP_PSN_Result.aspx?MODE=PDF&R_Year=" + R_Year + "&R_Round=" + R_Round + "&PSNL_No=" + PSNL_NO + "&Status=" + COMP_Status;
            btnIDPResultFormExcel.HRef = "Print/RPT_IDP_PSN_Result.aspx?MODE=EXCEL&R_Year=" + R_Year + "&R_Round=" + R_Round + "&PSNL_No=" + PSNL_NO + "&Status=" + COMP_Status;

        }


		private void BindMasterCOMP()
		{
			COMP_Status = BL.GetCOMPStatus(R_Year, R_Round, PSNL_NO);
			lblCOMPStatus.Text = BL.GetAssessmentStatusName(COMP_Status);
            // มีเอาไว้คิดรอบที่กำลังทำ อยู่ในช่วงที่กำหนดหรือไม่  
            Boolean IsInPeriod = BL.IsTimeInAssessmentPeriod(DateTime.Now, R_Year, R_Round, HRBL.AssessmentType.Competency, HRBL.AssessmentStatus.Creating, DEPT_CODE.ToString());
            // หาว่ารอบปิดหรือยัง  
            Boolean IsRoundCompleted = BL.Is_Round_Completed(R_Year, R_Round);

            Boolean IsEditable = COMP_Status <= HRBL.AssessmentStatus.Creating & IsInPeriod & !IsRoundCompleted;

            pnlActivity.Visible = IsEditable;
            //pnlCOMP.Enabled = IsEditable;
            ddlASSName.Enabled = IsEditable;
            btnAVG_Weight.Enabled = IsEditable;


			DataTable DT = BL.GetCOMPBundle(PSNL_NO, R_Year, R_Round, PSNL_CLASS_GROUP, PSNL_TYPE, FN_ID, FN_Type, HRBL.CompetencyType.All);

			rptAss.DataSource = DT;
			rptAss.DataBind();
		}

		protected void rptAss_ItemCommand(object source, System.Web.UI.WebControls.RepeaterCommandEventArgs e)
		{
			switch (e.CommandName) {
				case "Update":

					Button btnUpdate = (Button) e.Item.FindControl("btnUpdate");
					TextBox txtWeight = (TextBox) e.Item.FindControl("txtWeight");
					int COMP_Type_Id =GL.CINT (btnUpdate.Attributes["COMP_Type_Id"]);
					int Master_No = GL.CINT (btnUpdate.Attributes["Master_No"]);
					HtmlTableCell COMP_Name =(HtmlTableCell)  e.Item.FindControl("COMP_Name");
					HtmlTableCell target =(HtmlTableCell)  e.Item.FindControl("target");

					//------Save Detail (Weight)-------
					string SQL = "SELECT * FROM tb_HR_COMP_Detail ";
					SQL += " WHERE PSNL_NO='" + PSNL_NO.Replace("'", "''") + "'\n";
					SQL += " AND R_Year=" + R_Year + "\n";
					SQL += " AND R_Round=" + R_Round + "\n";
					SQL += " AND COMP_Type_Id=" + COMP_Type_Id + "\n";
					SQL += " AND Master_No=" + Master_No + "\n";

					SqlDataAdapter DA = new SqlDataAdapter(SQL, BL.ConnectionString());
					DataTable DT = new DataTable();
					DA.Fill(DT);

					DataRow DR = null;
					if (DT.Rows.Count == 0) {
						DR = DT.NewRow();
						DR["PSNL_NO"] = PSNL_NO;
						DR["R_Year"] = R_Year;
						DR["R_Round"] = R_Round;
						DR["COMP_Type_Id"] = COMP_Type_Id;
						DR["Master_No"] = Master_No;
						DR["Create_By"] = Session["USER_PSNL_NO"];
						DR["Create_Time"] = DateAndTime.Now;
					} else {
						DR = DT.Rows[0];
					}
					DR["COMP_Comp"] = COMP_Name.InnerHtml;
					DR["COMP_Std"] = target.InnerHtml;
					for (int i = 1; i <= 5; i++) {
						HtmlTableCell choice =(HtmlTableCell) e.Item.FindControl("choice" + i);
						DR["COMP_Choice_" + i] = choice.InnerHtml;
					}

					if (string.IsNullOrEmpty(txtWeight.Text)) {
						DR["COMP_Weight"] = DBNull.Value;
					} else {
						DR["COMP_Weight"] = Conversion.Val(txtWeight.Text);
					}
					DR["Update_By"] = Session["USER_PSNL_NO"];
					DR["Update_Time"] = DateAndTime.Now;

					if (DT.Rows.Count == 0)
						DT.Rows.Add(DR);
					SqlCommandBuilder cmd = new SqlCommandBuilder(DA);
					DA.Update(DT);

					DT = BL.GetCOMPBundle(PSNL_NO, R_Year, R_Round, PSNL_CLASS_GROUP, PSNL_TYPE, FN_ID, FN_Type);
					//----------------- Recheck --------------------
					rptAss.DataSource = DT;
					rptAss.DataBind();

					break;
			}
		}

		int LastType = 0;
		protected void rptAss_ItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
		{
			switch (e.Item.ItemType) {

				case ListItemType.AlternatingItem:
				case ListItemType.Item:

					HtmlTableRow row_COMP_type =(HtmlTableRow) e.Item.FindControl("row_COMP_type");
					HtmlTableCell cell_COMP_type =(HtmlTableCell) e.Item.FindControl("cell_COMP_type");

					HtmlTableCell ass_no =(HtmlTableCell)  e.Item.FindControl("ass_no");
					HtmlTableCell COMP_Name =(HtmlTableCell)  e.Item.FindControl("COMP_Name");
					HtmlTableCell target =(HtmlTableCell) e.Item.FindControl("target");
					HtmlTableCell cel_weight =(HtmlTableCell)  e.Item.FindControl("cel_weight");
					TextBox txtWeight =(TextBox) e.Item.FindControl("txtWeight");
					//-------------------- Hidden Key Action ---------------------
					Button btnUpdate =(Button) e.Item.FindControl("btnUpdate");

                    DataRowView drv = (DataRowView)e.Item.DataItem;
					btnUpdate.Attributes["COMP_Type_Id"] = drv["COMP_Type_Id"].ToString ();
					btnUpdate.Attributes["Master_No"] = drv["Master_No"].ToString ();

					if (LastType != GL.CINT ( drv["COMP_Type_Id"])) {
						switch (GL.CINT (drv["COMP_Type_Id"])) {
							case 1:
								cell_COMP_type.InnerHtml = "สมรรถนะหลัก (Core Competency)";
								break;
							case 2:
								cell_COMP_type.InnerHtml = "สมรรถนะตามสายงาน (Functional Competency)";
								break;
							case 3:
								cell_COMP_type.InnerHtml = "สมรรถนะตามสายระดับ (Managerial Competency)";
								break;
						}
						row_COMP_type.Visible = true;
						LastType = GL.CINT (drv["COMP_Type_Id"]);
					} else {
						row_COMP_type.Visible = false;
					}

					ass_no.InnerHtml = drv["COMP_No"].ToString ();
					COMP_Name.InnerHtml = drv["COMP_Comp"].ToString().Replace("\n", "<br>");
					target.InnerHtml = drv["COMP_Std"].ToString().Replace("\n", "<br>");
					cel_weight.Attributes["onclick"] = "document.getElementById('" + txtWeight.ClientID + "').focus();";
                    txtWeight.Text = GL.StringFormatNumber(drv["COMP_Weight"]);

                    if (!GL.IsEqualNull(drv["COMP_Weight"]))
                    {
                        txtWeight.Text = GL.StringFormatNumber(drv["COMP_Weight"]);
                        if (GL.CDBL(drv["COMP_Weight"]) > 100)
                        {
                            cel_weight.Style["background-color"] = "red";
                        }
                        else
                        {
                            cel_weight.Style["background-color"] = "green";
                        }
                    }
                    else
                    {
                        cel_weight.Style["background-color"] = "red";
                        txtWeight.Style["color"] = "black";
                    }

                    //cel_weight.Style["background-color"] = "#999999";
					TC.ImplementJavaFloatText(txtWeight);
					txtWeight.Style["text-align"] = "center";


					for (int i = 1; i <= 5; i++) {
						HtmlTableCell choice =(HtmlTableCell) e.Item.FindControl("choice" + i);
                        choice.InnerHtml = (string)DataBinder.Eval(e.Item.DataItem, "COMP_Choice_" + i.ToString()).ToString().Replace("\n", "<br>");

					}

                    
                    Boolean IsInPeriod = BL.IsTimeInAssessmentPeriod(DateTime.Now, R_Year, R_Round, HRBL.AssessmentType.Competency, HRBL.AssessmentStatus.Creating, DEPT_CODE.ToString());
                    Boolean IsRoundCompleted = BL.Is_Round_Completed(R_Year, R_Round);
                    Boolean IsEditable = COMP_Status <= HRBL.AssessmentStatus.Creating & IsInPeriod & !IsRoundCompleted;

                    if (IsEditable)
                    {
                        txtWeight.Attributes["onchange"] = "document.getElementById('" + btnUpdate.ClientID + "').click();";
                    }
                    else
                    {
                        txtWeight.ReadOnly = true;
                    }

					//---------- Set LastFocusControl ------------
					txtWeight.Attributes["onfocus"] = "setLastFocusControl(this);";

					break;
				case ListItemType.Footer:
					//----------------Report Summary ------------

					//----------- Control Difinition ------------
                    HtmlTableCell cell_sum_weight = (HtmlTableCell)e.Item.FindControl("cell_sum_weight");

					//------------ Report -------------
                    DataTable DT = (DataTable)rptAss.DataSource;

					double SUM = 0;
					for (int i = 0; i <= DT.Rows.Count - 1; i++) {
						if (!GL.IsEqualNull(DT.Rows[i]["COMP_weight"]) && Information.IsNumeric(DT.Rows[i]["COMP_weight"])) {
                            SUM += GL.CDBL(GL.StringFormatNumber(GL.CDBL(DT.Rows[i]["COMP_weight"]), 2));
                            SUM = GL.CDBL(GL.StringFormatNumber(GL.CDBL(SUM), 2));
						}
					}

					if (SUM != 100) {
						cell_sum_weight.Style["background-color"] = "red";
					} else {
						cell_sum_weight.Style["background-color"] = "green";
					}
					cell_sum_weight.InnerHtml = SUM + "%";

					break;

			}


		}

         public Double SUMFooter()
        {
            DataTable DT = (DataTable)rptAss.DataSource;
            double SUM = 0;
            for (int j = 0; j <= DT.Rows.Count - 1; j++)
            {
                if (!GL.IsEqualNull(DT.Rows[j]["COMP_weight"]) && Information.IsNumeric(DT.Rows[j]["COMP_weight"]))
                {
                    SUM += GL.CDBL(GL.StringFormatNumber(GL.CDBL(DT.Rows[j]["COMP_weight"]), 2));
                    SUM = GL.CDBL(GL.StringFormatNumber(GL.CDBL(SUM), 2));
                }
            }

            return SUM;
        
        }

         // กรณี Master เปลี่ยนระหว่างรอบ ให้ update COMP_Detail ก่อนผู้ประเมินอนุมัติแบบ    
         //private void Update_COMP_Detail()
         //{
         //    DataTable DT = BL.GetCOMPBundle(PSNL_NO, R_Year, R_Round, PSNL_CLASS_GROUP, PSNL_TYPE, FN_ID, FN_Type, HRBL.CompetencyType.All);
         //    DT.DefaultView.Sort = " COMP_Type_Sort DESC ,Master_No DESC";
         //    DT = DT.DefaultView.ToTable();
         //    if (DT.Rows.Count > 0)
         //    {
         //        double AVG_Weight = (GL.CDBL(100) / GL.CDBL(DT.Rows.Count));
         //        Double SUM_Weight = 0;
         //        Double CDBL_MOD = (GL.CDBL(GL.StringFormatNumber(GL.CDBL(AVG_Weight), 2)) % GL.CDBL(0.50));
         //        for (int i = 0; i <= DT.Rows.Count - 1; i++)
         //        {
         //            //------Save Detail (AVG Weight)-------
         //            string SQL = "SELECT * FROM tb_HR_COMP_Detail ";
         //            SQL += " WHERE PSNL_NO='" + PSNL_NO.Replace("'", "''") + "'\n";
         //            SQL += " AND R_Year=" + R_Year + "\n";
         //            SQL += " AND R_Round=" + R_Round + "\n";
         //            SQL += " AND COMP_Type_Id=" + DT.Rows[i]["COMP_Type_Id"] + "\n";
         //            SQL += " AND Master_No=" + DT.Rows[i]["Master_No"] + "\n";

         //            SqlDataAdapter DA = new SqlDataAdapter(SQL, BL.ConnectionString());
         //            DataTable DTAVG = new DataTable();
         //            DA.Fill(DTAVG);

         //            DataRow DR = null;
         //            if (DTAVG.Rows.Count == 0)
         //            {
         //                DR = DTAVG.NewRow();
         //                DR["PSNL_NO"] = PSNL_NO;
         //                DR["R_Year"] = R_Year;
         //                DR["R_Round"] = R_Round;
         //                DR["COMP_Type_Id"] = DT.Rows[i]["COMP_Type_Id"];
         //                DR["Master_No"] = DT.Rows[i]["Master_No"];
         //                DR["Create_By"] = Session["USER_PSNL_NO"];
         //                DR["Create_Time"] = DateAndTime.Now;
         //            }
         //            else
         //            {
         //                DR = DTAVG.Rows[0];
         //            }
         //            DR["COMP_Comp"] = DT.Rows[i]["COMP_Comp"].ToString();
         //            DR["COMP_Std"] = DT.Rows[i]["COMP_Std"].ToString();
         //            for (int j = 1; j <= 5; j++)
         //            {
         //                DR["COMP_Choice_" + j] = DT.Rows[i]["COMP_Choice_" + j];
         //            }
         //            if (GL.CINT(DT.Rows[i]["COMP_No"]) != 1)
         //            {
         //                if (GL.CDBL(CDBL_MOD) == 0)
         //                {
         //                    DR["COMP_Weight"] = GL.StringFormatNumber(AVG_Weight);
         //                    SUM_Weight += GL.CDBL(GL.StringFormatNumber(AVG_Weight));
         //                }
         //                else
         //                {
         //                    DR["COMP_Weight"] = Math.Floor(AVG_Weight);
         //                    SUM_Weight += Math.Floor(AVG_Weight);
         //                }
         //            }
         //            else
         //            {
         //                DR["COMP_Weight"] = (GL.CDBL(100) - GL.CDBL(SUM_Weight));
         //            }
         //            DR["Update_By"] = Session["USER_PSNL_NO"];
         //            DR["Update_Time"] = DateAndTime.Now;
         //            if (DTAVG.Rows.Count == 0)
         //                DTAVG.Rows.Add(DR);
         //            SqlCommandBuilder cmd = new SqlCommandBuilder(DA);
         //            DA.Update(DTAVG);

         //        }

         //    }
         //    else
         //    {
         //        return;
         //    }
         //}

		protected void btnPreSend_Click(object sender, System.EventArgs e)
		{
			//--------------- Check Something Incomplete ----------------
			DataTable DT = BL.GetCOMPBundle(PSNL_NO, R_Year, R_Round, PSNL_CLASS_GROUP, PSNL_TYPE, FN_ID, FN_Type);
			if (DT.Rows.Count == 0) {
				ScriptManager.RegisterStartupScript(this.Page, typeof(string), "Alert", "showAlert('ไม่มีหัวข้อการประเมิน');", true);
				return;
			}

            if (string.IsNullOrEmpty(ASSESSOR_BY))
            {
                ScriptManager.RegisterStartupScript(this.Page, typeof(string), "Alert", "showAlert('เลือกหัวหน้าผู้ประเมิน');", true);
                return;
            }
			//DT.DefaultView.RowFilter = "COMP_Choice_1='' OR COMP_Choice_2='' OR COMP_Choice_3='' OR COMP_Choice_4='' OR COMP_Choice_5='' OR COMP_Comp='' OR COMP_Std =''"
			//If DT.DefaultView.Count > 0 Then
			//    ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "showAlert('แบบประเมินยังไม่สมบูรณ์');", True)
			//    Exit Sub
			//End If
			DT.DefaultView.RowFilter = "COMP_Weight IS NULL OR COMP_Weight<=0";
			if (DT.DefaultView.Count > 0) {
				ScriptManager.RegisterStartupScript(this.Page, typeof(string), "Alert", "showAlert('ตั้งน้ำหนักให้ครบ');", true);
				return;
			}


            //object TotalWeight = DT.Compute("SUM(COMP_Weight)", "");
            object TotalWeight = GL.CDBL(GL.StringFormatNumber(GL.CDBL(DT.Compute("SUM(COMP_Weight)", "")), 2));

            if (GL.IsEqualNull(TotalWeight) || GL.CDBL(TotalWeight) != 100)
            {
                ScriptManager.RegisterStartupScript(this.Page, typeof(string), "Alert", "showAlert('ตั้งน้ำหนักการประเมินให้ครบ 100%');", true);
                return;
            }


            //object TotalWeight = DT.Compute("SUM(COMP_Weight)", "");
            //if (GL.IsEqualNull(TotalWeight) || GL.CDBL ( TotalWeight) != 100) {
            //    ScriptManager.RegisterStartupScript(this.Page, typeof(string), "Alert", "showAlert('ตั้งน้ำหนักการประเมินให้ครบ 100%');", true);
            //    return;
            //}

			ScriptManager.RegisterStartupScript(this.Page, typeof(string), "Alert", "clearLastFocusControl(); if(confirm('ยืนยันส่งแบบประเมิน ??\\nหลังจากส่งแบบประเมินแล้วคุณไม่สามารถแก้ไขได้')) document.getElementById('" + btnSend.ClientID + "').click();", true);
		}

		protected void btnSend_Click(object sender, System.EventArgs e)
		{
			//--------------- Check Something Incomplete ----------------
			DataTable DT = BL.GetCOMPBundle(PSNL_NO, R_Year, R_Round, PSNL_CLASS_GROUP, PSNL_TYPE, FN_ID, FN_Type);

			//--------------------Saving--------------------
			string SQL = "";
			SQL = " SELECT * \n";
			SQL += "  FROM tb_HR_COMP_Header\n";
			SQL += " WHERE PSNL_NO='" + PSNL_NO.Replace("'", "''") + "' AND R_Year=" + R_Year + " AND R_Round=" + R_Round;
			SqlDataAdapter DA = new SqlDataAdapter(SQL, BL.ConnectionString());
			DT = new DataTable();
			DA.Fill(DT);

			DataRow DR = DT.Rows[0];
			DR["COMP_Status"] = HRBL.AssessmentStatus.WaitCreatingApproved;
			if (DT.Rows.Count == 0)
				DT.Rows.Add(DR);
			SqlCommandBuilder cmd = new SqlCommandBuilder(DA);
			DA.Update(DT);

			//----------------- ทำสองที่ Page Load กับ Update Status ---------------
			BL.Update_COMP_Status_To_Assessment_Period(PSNL_NO, R_Year, R_Round);

			ScriptManager.RegisterStartupScript(this.Page, typeof(string), "Alert", "showAlert('ส่งแบบประเมินสำเร็จ');", true);
			BindMasterCOMP();

		}



		private void SaveHeader()
		{
			//------------- Check Round ---------------
			if (R_Year == 0 | R_Round == 0)
				return;
			//------------- Check Progress-------------
			if (COMP_Status > HRBL.AssessmentStatus.Creating)
				return;

			string SQL = "";
			SQL = " SELECT * \n";
			SQL += "  FROM tb_HR_COMP_Header\n";
			SQL += " WHERE PSNL_NO='" + PSNL_NO.Replace("'", "''") + "' AND R_Year=" + R_Year + " AND R_Round=" + R_Round;
			SqlDataAdapter DA = new SqlDataAdapter(SQL, BL.ConnectionString());
			DataTable DT = new DataTable();
			DA.Fill(DT);

			DataRow DR = null;
			if (DT.Rows.Count == 0) {
				DR = DT.NewRow();
				DR["PSNL_NO"] = PSNL_NO;
				DR["R_Year"] = R_Year;
				DR["R_Round"] = R_Round;
				DR["Create_By"] = Session["USER_PSNL_NO"];
				DR["Create_Time"] = DateAndTime.Now;
				DR["COMP_Status"] = 0;
			} else {
				DR = DT.Rows[0];
                if (GL.CINT(COMP_Status) == -1)
                {
                    DR["COMP_Status"] = 0;
                }
			}

			//------------- Round Detail------------- 
			SQL = "SELECT * FROM tb_HR_Round ";
			SQL += " WHERE R_Year=" + R_Year + " AND R_Round=" + R_Round;
			DataTable PN = new DataTable();
			SqlDataAdapter PA = new SqlDataAdapter(SQL, BL.ConnectionString());
			PA.Fill(PN);
			DR["R_Start"] = PN.Rows[0]["R_Start"];
			DR["R_End"] = PN.Rows[0]["R_End"];
			DR["R_Remark"] = "";

            //============ป้องกันการ update ตำแหน่ง ณ สิ้นรอบประเมินและปิดรอบประเมิน======

            Boolean IsInPeriod = BL.IsTimeInPeriod(DateTime.Now, R_Year, R_Round);
            Boolean IsRoundCompleted = BL.Is_Round_Completed(R_Year, R_Round);
            Boolean ckUpdate =  IsInPeriod & !IsRoundCompleted;
            if (ckUpdate | DT.Rows.Count == 0)

            {
                //------------- Personal Detail---------
                HRBL.PersonalInfo PSNInfo = BL.GetAssessmentPersonalInfo(R_Year, R_Round, PSNL_NO, COMP_Status);
                // Replace With New Updated Data If Exists
                if (!string.IsNullOrEmpty(PSNInfo.PSNL_No))
                {
                    if (!string.IsNullOrEmpty(PSNInfo.PSNL_No))
                        DR["PSNL_No"] = PSNInfo.PSNL_No;
                    else
                        DR["PSNL_No"] = DBNull.Value;
                    if (!string.IsNullOrEmpty(PSNInfo.PSNL_Fullname))
                        DR["PSNL_Fullname"] = PSNInfo.PSNL_Fullname;
                    else
                        DR["PSNL_Fullname"] = DBNull.Value;
                    if (!string.IsNullOrEmpty(PSNInfo.PNPS_CLASS))
                        DR["PNPS_CLASS"] = PSNInfo.PNPS_CLASS;
                    else
                        DR["PNPS_CLASS"] = DBNull.Value;
                    if (!string.IsNullOrEmpty(PSNInfo.PSNL_TYPE))
                        DR["PSNL_TYPE"] = PSNInfo.PSNL_TYPE;
                    else
                        DR["PSNL_TYPE"] = DBNull.Value;
                    if (!string.IsNullOrEmpty(PSNInfo.POS_NO))
                        DR["POS_NO"] = PSNInfo.POS_NO;
                    else
                        DR["POS_NO"] = DBNull.Value;
                    if (!string.IsNullOrEmpty(PSNInfo.WAGE_TYPE))
                        DR["WAGE_TYPE"] = PSNInfo.WAGE_TYPE;
                    else
                        DR["WAGE_TYPE"] = DBNull.Value;
                    if (!string.IsNullOrEmpty(PSNInfo.WAGE_NAME))
                        DR["WAGE_NAME"] = PSNInfo.WAGE_NAME;
                    else
                        DR["WAGE_NAME"] = DBNull.Value;
                    if (!string.IsNullOrEmpty(PSNInfo.SECTOR_CODE))
                        DR["SECTOR_CODE"] = Strings.Left(PSNInfo.SECTOR_CODE, 2);
                    else
                        DR["SECTOR_CODE"] = DBNull.Value;
                    if (!string.IsNullOrEmpty(PSNInfo.DEPT_CODE))
                        DR["DEPT_CODE"] = PSNInfo.DEPT_CODE;
                    else
                        DR["DEPT_CODE"] = DBNull.Value;
                    if (!string.IsNullOrEmpty(PSNInfo.MINOR_CODE))
                        DR["MINOR_CODE"] = PSNInfo.MINOR_CODE;
                    else
                        DR["MINOR_CODE"] = DBNull.Value;
                    if (!string.IsNullOrEmpty(PSNInfo.SECTOR_NAME))
                        DR["SECTOR_NAME"] = PSNInfo.SECTOR_NAME;
                    else
                        DR["SECTOR_NAME"] = DBNull.Value;
                    if (!string.IsNullOrEmpty(PSNInfo.DEPT_NAME))
                        DR["DEPT_NAME"] = PSNInfo.DEPT_NAME;
                    else
                        DR["DEPT_NAME"] = DBNull.Value;
                    if (!string.IsNullOrEmpty(PSNInfo.FN_ID))
                        DR["FN_ID"] = PSNInfo.FN_ID;
                    else
                        DR["FN_ID"] = DBNull.Value;
                    if (!string.IsNullOrEmpty(PSNInfo.FLD_Name))
                        DR["FLD_Name"] = PSNInfo.FLD_Name;
                    else
                        DR["FLD_Name"] = DBNull.Value;
                    if (!string.IsNullOrEmpty(PSNInfo.FN_CODE))
                        DR["FN_CODE"] = PSNInfo.FN_CODE;
                    else
                        DR["FN_CODE"] = DBNull.Value;
                    if (!string.IsNullOrEmpty(PSNInfo.FN_NAME))
                        DR["FN_NAME"] = PSNInfo.FN_NAME;
                    else
                        DR["FN_NAME"] = DBNull.Value;
                    if (!string.IsNullOrEmpty(PSNInfo.MGR_CODE))
                        DR["MGR_CODE"] = PSNInfo.MGR_CODE;
                    else
                        DR["MGR_CODE"] = DBNull.Value;
                    if (!string.IsNullOrEmpty(PSNInfo.MGR_NAME))
                        DR["MGR_NAME"] = PSNInfo.MGR_NAME;
                    else
                        DR["MGR_NAME"] = DBNull.Value;
                }

                //------------- Assign Assessor ------------ Old -----------------------------

                DR["Create_Commit_By"] = ASSESSOR_BY;
                DR["Create_Commit_Name"] = ASSESSOR_NAME;
                DR["Create_Commit_DEPT"] = ASSESSOR_DEPT;
                DR["Create_Commit_POS"] = ASSESSOR_POS;
            }
			DR["Update_By"] = Session["USER_PSNL_NO"];
			DR["Update_Time"] = DateAndTime.Now;

			if (DT.Rows.Count == 0)
				DT.Rows.Add(DR);
			SqlCommandBuilder cmd = new SqlCommandBuilder(DA);
			DA.Update(DT);

			DataTable TMP = BL.GetAssessorList(R_Year, R_Round, PSNL_NO);
			TMP.DefaultView.RowFilter = "MGR_PSNL_NO='" + ASSESSOR_BY.Replace("'", "''") + "'";

			//----------------- Update Information For History Assessor Structure ------------------
			SQL = "SELECT * ";
			SQL += " FROM tb_HR_Actual_Assessor ";
			SQL += " WHERE PSNL_NO='" + PSNL_NO + "' AND R_Year=" + R_Year + " AND R_Round=" + R_Round;
			DA = new SqlDataAdapter(SQL, BL.ConnectionString());
			DT = new DataTable();
			DA.Fill(DT);

			if (DT.Rows.Count > 0) {
				DT.Rows[0].Delete();
				cmd = new SqlCommandBuilder(DA);
				DA.Update(DT);
			}

			if (TMP.DefaultView.Count > 0) {
				DR = DT.NewRow();
				DR["PSNL_NO"] = PSNL_NO;
				DR["R_Year"] = R_Year;
				DR["R_Round"] = R_Round;
				DR["MGR_ASSESSOR_POS"] = TMP.DefaultView[0]["ASSESSOR_POS"];
				DR["MGR_PSNL_NO"] = TMP.DefaultView[0]["MGR_PSNL_NO"];
				DR["MGR_PSNL_Fullname"] = TMP.DefaultView[0]["MGR_PSNL_Fullname"];
				DR["MGR_PNPS_CLASS"] = TMP.DefaultView[0]["MGR_CLASS"];
				DR["MGR_PSNL_TYPE"] = TMP.DefaultView[0]["MGR_WAGE_TYPE"];
				DR["MGR_POS_NO"] = TMP.DefaultView[0]["MGR_POS_NO"];
				DR["MGR_WAGE_TYPE"] = TMP.DefaultView[0]["MGR_WAGE_TYPE"];
				DR["MGR_WAGE_NAME"] = TMP.DefaultView[0]["MGR_WAGE_NAME"];
				DR["MGR_DEPT_CODE"] = TMP.DefaultView[0]["MGR_DEPT_CODE"];
				//DR("MGR_MINOR_CODE") = TMP.DefaultView(0).Item("MGR_MINOR_CODE")
				DR["MGR_SECTOR_CODE"] = Strings.Left(TMP.DefaultView[0]["MGR_SECTOR_CODE"].ToString (), 2);
                DR["MGR_SECTOR_NAME"] = TMP.DefaultView[0]["MGR_SECTOR_NAME"];
				DR["MGR_DEPT_NAME"] = TMP.DefaultView[0]["MGR_DEPT_NAME"];
				//DR("MGR_FN_ID") = TMP.DefaultView(0).Item("MGR_FN_ID")
				//DR("MGR_FLD_Name") = TMP.DefaultView(0).Item("MGR_FLD_Name")
				DR["MGR_FN_CODE"] = TMP.DefaultView[0]["MGR_FN_CODE"];
				//DR("MGR_FN_TYPE") = TMP.DefaultView(0).Item("MGR_FN_TYPE")
				DR["MGR_FN_NAME"] = TMP.DefaultView[0]["MGR_FN_NAME"];
				if (!GL.IsEqualNull(TMP.DefaultView[0]["MGR_MGR_CODE"])) {
					DR["MGR_MGR_CODE"] = TMP.DefaultView[0]["MGR_MGR_CODE"];
				}
				if (!GL.IsEqualNull(TMP.DefaultView[0]["MGR_MGR_NAME"])) {
					DR["MGR_MGR_NAME"] = TMP.DefaultView[0]["MGR_MGR_NAME"];
				}
				try {
					DR["MGR_PNPS_RETIRE_DATE"] = BL.GetPSNRetireDate(TMP.DefaultView[0]["MGR_PSNL_NO"].ToString ());
				} catch {
				}
				DR["Update_By"] = Session["USER_PSNL_NO"];
				DR["Update_Time"] = DateAndTime.Now;
				DT.Rows.Add(DR);
				cmd = new SqlCommandBuilder(DA);
				DA.Update(DT);
			}

			//----------------- Update Information For History PSN Structure ------------------
			DataTable PSN = new DataTable();
			SQL = "SELECT * FROM vw_PN_PSNL WHERE PSNL_NO='" + PSNL_NO.Replace("'", "''") + "'";
			DA = new SqlDataAdapter(SQL, BL.ConnectionString());
			DA.Fill(PSN);

			SQL = "SELECT * ";
			SQL += " FROM tb_HR_Assessment_PSN ";
			SQL += " WHERE PSNL_NO='" + PSNL_NO.Replace("'", "''") + "' AND R_Year=" + R_Year + " AND R_Round=" + R_Round;
			DA = new SqlDataAdapter(SQL, BL.ConnectionString());
			DT = new DataTable();
			DA.Fill(DT);

			if (PSN.Rows.Count > 0) {
				if (DT.Rows.Count == 0) {
					DR = DT.NewRow();
					DR["PSNL_NO"] = PSNL_NO;
					DR["R_Year"] = R_Year;
					DR["R_Round"] = R_Round;
					
					DT.Rows.Add(DR);
				} else {
					DR = DT.Rows[0];
				}

                //============ป้องกันการ update ตำแหน่ง ณ สิ้นรอบประเมินและปิดรอบประเมิน======
                if (ckUpdate | DT.Rows.Count == 0 | string.IsNullOrEmpty(DT.Rows[0]["PSN_PNPS_CLASS"].ToString()))

                {
                    if (TMP.DefaultView.Count > 0) {
						try {
							DR["PSN_ASSESSOR_POS"] = TMP.DefaultView[0]["ASSESSOR_POS"];
						} catch {
						}
					}
					DR["PSN_PSNL_NO"] = PSNL_NO;
					DR["PSN_PSNL_Fullname"] = PSN.Rows[0]["PSNL_Fullname"].ToString();
                    DR["PSN_PNPS_CLASS"] = PSN.Rows[0]["PNPS_CLASS"].ToString();
					DR["PSN_PSNL_TYPE"] = PSN.Rows[0]["PSNL_TYPE"].ToString();
					DR["PSN_POS_NO"] = PSN.Rows[0]["POS_NO"].ToString();
					DR["PSN_WAGE_TYPE"] = PSN.Rows[0]["WAGE_TYPE"].ToString();
					DR["PSN_WAGE_NAME"] = PSN.Rows[0]["WAGE_NAME"].ToString();
					DR["PSN_DEPT_CODE"] = PSN.Rows[0]["DEPT_CODE"].ToString();
					DR["PSN_MINOR_CODE"] = PSN.Rows[0]["MINOR_CODE"].ToString();
					DR["PSN_SECTOR_CODE"] = Strings.Left(PSN.Rows[0]["SECTOR_CODE"].ToString(), 2);
					DR["PSN_SECTOR_NAME"] = PSN.Rows[0]["SECTOR_NAME"].ToString();
					DR["PSN_DEPT_NAME"] = PSN.Rows[0]["DEPT_NAME"].ToString();
					DR["PSN_FN_ID"] = PSN.Rows[0]["FN_ID"].ToString();
					DR["PSN_FLD_Name"] = PSN.Rows[0]["FLD_Name"].ToString();
					DR["PSN_FN_CODE"] = PSN.Rows[0]["FN_CODE"].ToString();
					DR["PSN_FN_TYPE"] = PSN.Rows[0]["FN_TYPE"].ToString();
					DR["PSN_FN_NAME"] = PSN.Rows[0]["FN_NAME"].ToString();
					if (!GL.IsEqualNull(PSN.Rows[0]["MGR_CODE"])) {
						DR["PSN_MGR_CODE"] = PSN.Rows[0]["MGR_CODE"];
					}
					if (!GL.IsEqualNull(PSN.Rows[0]["MGR_NAME"])) {
						DR["PSN_MGR_NAME"] = PSN.Rows[0]["MGR_NAME"];
					}
				}

				try {
					DR["PSN_PNPS_RETIRE_DATE"] = BL.GetPSNRetireDate(PSNL_NO);
				} catch {
				}
				DR["Update_By"] = Session["USER_PSNL_NO"];
				DR["Update_Time"] = DateAndTime.Now;
				cmd = new SqlCommandBuilder(DA);
				DA.Update(DT);

			}
		}


		protected void btnAVG_Weight_Click(object sender, System.EventArgs e)
		{
			AVG_Weight();			
			BindMasterCOMP();
		}
		public Assessment_self_COMP_Create()
		{
			Load += Page_Load;
		}

	}
}

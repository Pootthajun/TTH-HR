using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Xml.Linq;
using System.Diagnostics;
using System.Collections.Specialized;
using System.Configuration;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Caching;
using System.Web.SessionState;
using System.Web.Security;
using System.Web.Profile;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
namespace VB
{

	public partial class RPT_C_MGRNotAss : System.Web.UI.Page
	{


		HRBL BL = new HRBL();
		GenericLib GL = new GenericLib();

		Converter C = new Converter();
		public int R_Year {
			get {
				try {
                    return GL.CINT(GL.SplitString(ddlRound.Items[ddlRound.SelectedIndex].Value, "-")[0]);
				} catch (Exception ex) {
					return 0;
				}
			}
		}

		public int R_Round {
			get {
				try {
					return GL.CINT(GL.SplitString(ddlRound.Items[ddlRound.SelectedIndex].Value, "-")[1]);
				} catch (Exception ex) {
					return 0;
				}
			}
		}

		protected void Page_Load(object sender, System.EventArgs e)
		{
			if ((Session["USER_PSNL_NO"] == null)) {
				ScriptManager.RegisterStartupScript(this.Page, typeof(string), "Alert", "showAlert('กรุณาลงชื่อเข้าใช้ระบบ');", true);
				Response.Redirect("Login.aspx");
				return;
			}

			if (!IsPostBack) {
				//----------------- ทำสองที่ Page Load กับ Update Status ---------------
				BL.BindDDlYearRound(ddlRound);
				ddlRound.Items.RemoveAt(0);
				BL.BindDDlSector(ddlSector);
				BL.BindDDlDepartment(ddlDept, ddlSector.SelectedValue);

				BindPersonalList();
			}
		}

        protected void btnSearch_Click(object sender, System.EventArgs e)
		{
			BindPersonalList();
		}

		protected void ddlSector_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			BL.BindDDlDepartment(ddlDept, ddlSector.SelectedValue);
			BindPersonalList();
		}

		private void BindPersonalList()
		{
			string SQL = "";
			SQL += " SELECT R_Year,R_Round,SECTOR_CODE,SECTOR_NAME,DEPT_CODE" + "\n";
			SQL += " ,DEPT_NAME,COUNT(1) PSNL_AMOUNT " + "\n";
			SQL += " FROM vw_RPT_COMP_Status " + "\n";
			SQL += " WHERE (COMP_Status IS NULL OR COMP_Status < 5)" + "\n";
			SQL += " AND R_Year=" + R_Year + "\n";
			SQL += " AND R_Round=" + R_Round + "\n";

			string Title = ddlRound.Items[ddlRound.SelectedIndex].Text + " ";


			if (ddlSector.SelectedIndex > 0) {
				SQL += " AND ( SECTOR_CODE='" + ddlSector.Items[ddlSector.SelectedIndex].Value + "' " + "\n";
				if (  ddlSector.SelectedValue == "3200") {
					SQL += " AND  DEPT_CODE NOT IN ('32000300'))    " + "\n";
				//-----------ฝ่ายโรงงานผลิตยาสูบ 3
				} else if (ddlSector.Items[ddlSector.SelectedIndex].Value == "3203") {
					SQL += " OR  DEPT_CODE IN ('32000300','32030000','32030100','32030200','32030300','32030500'))  " + "\n";
				//-----------ฝ่ายโรงงานผลิตยาสูบ 4
				} else if (ddlSector.Items[ddlSector.SelectedIndex].Value == "3204") {
					SQL += " OR  DEPT_CODE IN ('32040000','32040100','32040200','32040300','32040500','32040300'))  " + "\n";
				//-----------ฝ่ายโรงงานผลิตยาสูบ 5
				} else if (ddlSector.Items[ddlSector.SelectedIndex].Value == "3205") {
					SQL += " OR  DEPT_CODE IN ('32050000','32050100','32050200','32050300','32050500'))\t  " + "\n";
				} else {
					SQL += ")   " + "\n";
				}

				if (ddlDept.SelectedIndex < 1) {
					Title += GL.SplitString(ddlSector.Items[ddlSector.SelectedIndex].Text, ":")[1].Trim() + " ";
					//----- เลือกแสดงชื่อหน่วยงานเดียว --------
				}
			}
			if (ddlDept.SelectedIndex > 0) {
				SQL += " AND DEPT_CODE + '-' + MINOR_CODE = '" + ddlDept.SelectedValue + "' " + "\n";
				Title += ddlDept.Items[ddlDept.SelectedIndex].Text + " ";
				//----- เลือกแสดงชื่อหน่วยงานเดียว --------
			}
			SQL += " GROUP BY R_Year,R_Round,SECTOR_CODE,SECTOR_NAME,DEPT_CODE,DEPT_NAME";
			SQL += " ORDER BY SECTOR_CODE, DEPT_CODE";

			SqlDataAdapter DA = new SqlDataAdapter(SQL, BL.ConnectionString());
			DataTable DT = new DataTable();
			DA.Fill(DT);

			//--------------- Bind Manager ------------
			DataTable MT = new DataTable();
			SQL = "SELECT * " + "\n";
			SQL += " FROM vw_HR_ASSESSOR " + "\n";
			SQL += " WHERE (R_Year=" + R_Year + " OR R_Year IS NULL) " + "\n";
			SQL += " AND (R_Round=" + R_Round + " OR R_Round IS NULL)" + "\n";
			SQL += " AND ASSESSOR_NAME IS NOT NULL " + "\n";
			SQL += " ORDER BY ASS_TYPE" + "\n";
			DA = new SqlDataAdapter(SQL, BL.ConnectionString());
			DA.Fill(MT);
			DT.Columns.Add("MGR");
			for (int i = 0; i <= DT.Rows.Count - 1; i++) {
				MT.DefaultView.RowFilter = "DEPT_CODE='" + DT.Rows[i]["DEPT_CODE"] + "'";
				if (MT.DefaultView.Count == 0) {
					DT.Rows[i]["MGR"] = "-";
				} else {
					string tmp = "";
					for (int j = 0; j <= MT.DefaultView.Count - 1; j++) {
						tmp += MT.DefaultView[j]["ASSESSOR_CODE"] + " : " + MT.DefaultView[j]["ASSESSOR_NAME"] + "\n";
					}
					DT.Rows[i]["MGR"] = tmp.Substring(0, tmp.Length - 1);
				}
			}

			Session["RPT_C_MGRNotAss"] = DT;
			//--------- Title --------------
			Session["RPT_C_MGRNotAss_Title"] = Title;
			//--------- Sub Title ----------
			Session["RPT_C_MGRNotAss_SubTitle"] = "รายงาน ณ วันที่ " + DateTime.Now.Day + " " + C.ToMonthNameTH(DateTime.Now.Month) + " พ.ศ." + (DateTime.Now.Year + 543);
			if (DT.Rows.Count == 0) {
				Session["RPT_C_MGRNotAss_SubTitle"] += "  ไม่พบรายการดังกล่าว";
			} else {
				Session["RPT_C_MGRNotAss_SubTitle"] += "  พบ   " + GL.StringFormatNumber(DT.Rows.Count.ToString(), 0) + "  รายการ";
				object sumObject = DT.Compute("Sum(PSNL_AMOUNT)", "");
				if (Information.IsNumeric(sumObject)) {
                    Session["RPT_C_MGRNotAss_SubTitle"] += "      จำนวนพนักงาน " + GL.StringFormatNumber(sumObject.ToString(), 0) + " คน";
				}
			}

			Pager.SesssionSourceName = "RPT_C_MGRNotAss";
			Pager.RenderLayout();

			if (DT.Rows.Count == 0) {
				lblCountPSN.Text = "ไม่พบรายการดังกล่าว";
			} else {
				// Declare an object variable. 
				object sumObject = null;
				sumObject = DT.Compute("Sum(PSNL_AMOUNT)", "");
                lblCountPSN.Text = "พบ " + GL.StringFormatNumber(DT.Rows.Count.ToString(), 0) + " รายการ &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;จำนวนพนักงาน " + GL.StringFormatNumber(sumObject, 0) + " คน";
			}
		}

		protected void Pager_PageChanging(PageNavigation Sender)
		{
			Pager.TheRepeater = rptCOMP;
		}

		//------------ For Grouping -----------
		string LastSectorName = "";
		protected void rptKPI_ItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
		{
			if (e.Item.ItemType != ListItemType.AlternatingItem & e.Item.ItemType != ListItemType.Item)
				return;

            Label lblSector = (Label)e.Item.FindControl("lblSector");
            Label lblPSNOrganize = (Label)e.Item.FindControl("lblPSNOrganize");
            Label lblPSNLAmount = (Label)e.Item.FindControl("lblPSNLAmount");
            Label lblMGR = (Label)e.Item.FindControl("lblMGR");

            DataRowView drv = (DataRowView)e.Item.DataItem;

			if (drv["SECTOR_NAME"].ToString() != LastSectorName) {
				lblSector.Text = drv["SECTOR_NAME"].ToString();
				LastSectorName = drv["SECTOR_NAME"].ToString();
			}
			if (drv["DEPT_NAME"].ToString() != drv["SECTOR_NAME"].ToString()) {
				lblPSNOrganize.Text = drv["DEPT_NAME"].ToString().Replace(drv["SECTOR_NAME"].ToString(), "").Trim();
			} else {
				lblPSNOrganize.Text = drv["DEPT_NAME"].ToString();
			}
			lblPSNLAmount.Text = drv["PSNL_AMOUNT"].ToString();

			lblMGR.Text = drv["MGR"].ToString().Replace("\n", "<br>");

		}
		public RPT_C_MGRNotAss()
		{
			Load += Page_Load;
		}
	}
}

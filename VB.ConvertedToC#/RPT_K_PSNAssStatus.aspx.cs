using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Xml.Linq;
using System.Diagnostics;
using System.Collections.Specialized;
using System.Configuration;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Caching;
using System.Web.SessionState;
using System.Web.Security;
using System.Web.Profile;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
namespace VB
{

	public partial class RPT_K_PSNAssStatus : System.Web.UI.Page
	{


		HRBL BL = new HRBL();
		GenericLib GL = new GenericLib();

		Converter C = new Converter();
		public int R_Year {
			get {
				try {
					return GL.CINT( GL.SplitString(ddlRound.Items[ddlRound.SelectedIndex].Value, "-")[0]);
				} catch ( Exception ex) {
					return 0;
				}
			}
		}

		public int R_Round {
			get {
				try {
					return GL.CINT(GL.SplitString(ddlRound.Items[ddlRound.SelectedIndex].Value, "-")[1]);
				} catch (Exception ex) {
					return 0;
				}
			}
		}

		protected void Page_Load(object sender, System.EventArgs e)
		{
			if ((Session["USER_PSNL_NO"] == null)) {
				ScriptManager.RegisterStartupScript(this.Page, typeof(string), "Alert", "showAlert('กรุณาลงชื่อเข้าใช้ระบบ');", true);
				Response.Redirect("Login.aspx");
				return;
			}

			if (!IsPostBack) {
				//----------------- ทำสองที่ Page Load กับ Update Status ---------------
				BL.BindDDlYearRound(ddlRound);
				ddlRound.Items.RemoveAt(0);
				BL.BindDDlSector(ddlSector);
				BL.BindDDlDepartment(ddlDept, ddlSector.SelectedValue);

				BindPersonalList();
			}
		}

        protected void btnSearch_Click(object sender, System.EventArgs e)
		{
			BindPersonalList();
		}

		protected void ddlSector_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			BL.BindDDlDepartment(ddlDept, ddlSector.SelectedValue);			
		}

		private void BindPersonalList()
		{
			string SQL = "";
			SQL += " SELECT *" + "\n";
			SQL += " FROM vw_RPT_KPI_Result" + "\n";
			SQL += " WHERE R_Year=" + R_Year + "\n";
			SQL += " AND R_Round=" + R_Round + "\n";

			string Title = ddlRound.Items[ddlRound.SelectedIndex].Text + " ";


			if (ddlSector.SelectedIndex > 0) {
				SQL += " AND ( SECTOR_CODE='" + ddlSector.Items[ddlSector.SelectedIndex].Value + "' " + "\n";
				if (ddlSector.Items[ddlSector.SelectedIndex].Value == "3200") {
					SQL += " AND  DEPT_CODE NOT IN ('32000300'))    " + "\n";
				//-----------ฝ่ายโรงงานผลิตยาสูบ 3
				} else if (ddlSector.Items[ddlSector.SelectedIndex].Value == "3203") {
					SQL += " OR  DEPT_CODE IN ('32000300','32030000','32030100','32030200','32030300','32030500'))  " + "\n";
				//-----------ฝ่ายโรงงานผลิตยาสูบ 4
				} else if (ddlSector.Items[ddlSector.SelectedIndex].Value == "3204") {
					SQL += " OR  DEPT_CODE IN ('32040000','32040100','32040200','32040300','32040500','32040300'))  " + "\n";
				//-----------ฝ่ายโรงงานผลิตยาสูบ 5
				} else if (ddlSector.Items[ddlSector.SelectedIndex].Value == "3205") {
					SQL += " OR  DEPT_CODE IN ('32050000','32050100','32050200','32050300','32050500'))\t  " + "\n";
				} else {
					SQL += ")   " + "\n";
				}
				if (ddlDept.SelectedIndex < 1) {
					Title += GL.SplitString(ddlSector.Items[ddlSector.SelectedIndex].Text, ":")[1].Trim() + " ";
					//----- เลือกแสดงชื่อหน่วยงานเดียว --------
				}
			}
			if (ddlDept.SelectedIndex > 0) {
				SQL += " AND DEPT_CODE = '" + GL.SplitString(ddlDept.SelectedValue, "-")[0] + "' " + "\n";

				Title += ddlDept.Items[ddlDept.SelectedIndex].Text + " ";
				//----- เลือกแสดงชื่อหน่วยงานเดียว --------
			}
			if (!string.IsNullOrEmpty(txtName.Text)) {
				SQL += " AND (PSNL_Fullname LIKE '%" + txtName.Text.Replace("'", "''") + "%' OR PSNL_NO LIKE '%" + txtName.Text.Replace("'", "''") + "%') " + "\n";

				//Title &= " ชื่อ/รหัส '" & txtName.Text & "' " '----- เลือกแสดงชื่อหน่วยงานเดียว --------
			}



            string Filter = "";

            if ((chkStatus0.Checked & chkStatus1.Checked & chkStatus2.Checked & chkStatus3.Checked & chkStatus4.Checked & chkStatus5.Checked) | (!chkStatus0.Checked & !chkStatus1.Checked & !chkStatus2.Checked & !chkStatus3.Checked & !chkStatus4.Checked & !chkStatus5.Checked ))
            {
                //----------- Do nothing--------
            }
            else
            {
                string _StatusFilter = "";
                string _StatusTitle = "";
                for (int i = 0; i <= 5; i++)
                {
                    CheckBox chk = (CheckBox)pnlList.FindControl("chkStatus" + i);
                    if (chk.Checked)
                    {
                        _StatusFilter += "'" + i.ToString().PadLeft(2, GL.chr0) + "',";
                        _StatusTitle += i + ",";
                    }
                }
                Title += " ความคืบหน้า " + _StatusTitle.Substring(0, _StatusTitle.Length - 1);
                Filter += " ASS_Status IN (" + _StatusFilter.Substring(0, _StatusFilter.Length - 1) + ") " ;
            }




            SQL += " AND  PSN_STAT_NAME_ON_ROUND ='ปัจจุบัน' ";

			SQL += " ORDER BY SECTOR_CODE,DEPT_CODE,PNPS_CLASS DESC,POS_No";

			SqlDataAdapter DA = new SqlDataAdapter(SQL, BL.ConnectionString());
			DA.SelectCommand.CommandTimeout = 90;
			DataTable DT = new DataTable();
			DA.Fill(DT);

            DT.DefaultView.RowFilter = Filter;

            Session["RPT_K_PSNAssStatus"] = DT.DefaultView.ToTable();

			//--------- Title --------------
			Session["RPT_K_PSNAssStatus_Title"] = Title;
			//--------- Sub Title ----------
			Session["RPT_K_PSNAssStatus_SubTitle"] = "รายงาน ณ วันที่ " + DateTime.Now.Day + " " + C.ToMonthNameTH(DateTime.Now.Month) + " พ.ศ." + (DateTime.Now.Year + 543);

			Pager.SesssionSourceName = "RPT_K_PSNAssStatus";
			Pager.RenderLayout();

			if (DT.DefaultView.Count == 0) {
				lblCountPSN.Text = "ไม่พบรายการดังกล่าว";
			} else {
				lblCountPSN.Text = "พบ " + GL.StringFormatNumber(DT.DefaultView.Count, 0) + " รายการ";
			}
		}

		protected void Pager_PageChanging(PageNavigation Sender)
		{
			Pager.TheRepeater = rptKPI;
		}

		//------------ For Grouping -----------
		string LastSectorName = "";
		string LastOrgranizeName = "";
		protected void rptKPI_ItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
		{
			if (e.Item.ItemType != ListItemType.AlternatingItem & e.Item.ItemType != ListItemType.Item)
				return;

            Label lblSector = (Label)e.Item.FindControl("lblSector");
            Label lblPSNOrganize = (Label)e.Item.FindControl("lblPSNOrganize");
            Label lblPSNNo = (Label)e.Item.FindControl("lblPSNNo");
            Label lblPSNName = (Label)e.Item.FindControl("lblPSNName");
            Label lblPSNPos = (Label)e.Item.FindControl("lblPSNPos");
            Label lblPSNClass = (Label)e.Item.FindControl("lblPSNClass");
            Label lblStatus = (Label)e.Item.FindControl("lblStatus");
            Label lblScore = (Label)e.Item.FindControl("lblScore");
            Label lblPercent = (Label)e.Item.FindControl("lblPercent");
            HtmlAnchor btnPrint = (HtmlAnchor)e.Item.FindControl("btnPrint");

            DataRowView drv = (DataRowView)e.Item.DataItem;

			if (drv["SECTOR_NAME"].ToString() != LastSectorName) {
				lblSector.Text = drv["SECTOR_NAME"].ToString();
				LastSectorName = drv["SECTOR_NAME"].ToString();
			}
			if (drv["DEPT_NAME"].ToString() != LastOrgranizeName) {
				if (drv["DEPT_NAME"].ToString() != drv["SECTOR_NAME"].ToString()) {
					lblPSNOrganize.Text = drv["DEPT_NAME"].ToString().Replace(drv["SECTOR_NAME"].ToString(), "").Trim();
				} else {
					lblPSNOrganize.Text = drv["DEPT_NAME"].ToString();
				}
				LastOrgranizeName = drv["DEPT_NAME"].ToString();
			}

			lblPSNNo.Text = drv["PSNL_NO"].ToString();
            lblPSNName.Text = drv["PSNL_Fullname"].ToString();
            lblPSNPos.Text = drv["POS_Name"].ToString();
			lblPSNClass.Text =GL.CINT (drv["PNPS_CLASS"]).ToString();
            if (!GL.IsEqualNull(drv["ASS_Status"]) && drv["ASS_Status"].ToString() == "5")
            {
				lblStatus.ForeColor = System.Drawing.Color.Green;
			}
            lblStatus.Text = drv["ASS_Status_Name"].ToString();
			if (!GL.IsEqualNull(drv["RESULT"]) && GL.CINT( drv["RESULT"]) > 0) {
				lblScore.Text = drv["RESULT"] + "/500";
				lblPercent.Text = (GL.CDBL( drv["RESULT"]) / 5).ToString() + " %";
			} else {
				lblScore.Text = "-";
				lblPercent.Text = "-";
			}
            btnPrint.HRef = "Print/RPT_K_PNSAss.aspx?MODE=PDF&R_Year=" + GL.CINT(drv["R_Year"]) + "&R_Round=" + GL.CINT(drv["R_Round"]) + "&PSNL_No=" + drv["PSNL_NO"].ToString() + "&Status=" + GL.CINT(drv["ASS_Status"]);
        }
		public RPT_K_PSNAssStatus()
		{
			Load += Page_Load;
		}
	}
}

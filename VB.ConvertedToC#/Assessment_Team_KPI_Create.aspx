﻿<%@ Page  Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPage.Master" CodeBehind="Assessment_Team_KPI_Create.aspx.cs" Inherits="VB.Assessment_Team_KPI_Create" %>

<%@ Register src="WUC_DateReporter.ascx" tagname="WUC_DateReporter" tagprefix="uc1" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<%@ Register src="PageNavigation.ascx" tagname="PageNavigation" tagprefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<asp:ToolkitScriptManager ID="toolkit1" runat="server"></asp:ToolkitScriptManager>
<asp:UpdatePanel ID="UpdatePanel1" runat="server">
<ContentTemplate>

<div class="container-fluid">
				<!-- BEGIN PAGE HEADER-->
				<div class="row-fluid" id="fixedPageHeader">
					<div class="span12"> 
						<!-- BEGIN PAGE TITLE & BREADCRUMB-->
						<h3>กำหนดแบบประเมินตัวชี้วัดพนักงาน (KPI) <font color="blue">(ScreenID : S-MGR-02)</font>
                        <asp:Label ID="lblReportTime" Font-Size="14px" ForeColor="Green" runat="Server"></asp:Label>
                        </h3>

						<h3 id="pnlFilterYear" runat="server" visible="True">
						     สำหรับรอบการประเมิน
						     <asp:DropDownList ID="ddlRound" OnSelectedIndexChanged="Search" runat="server" AutoPostBack="true" CssClass="medium m-wrap" style="font-size:20px;">
							</asp:DropDownList>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;
                            <asp:DropDownList ID="ddlForm" OnSelectedIndexChanged="ddlForm_SelectedIndexChanged" runat="server" Visible ="false"  style="font-size:20px;" AutoPostBack="true" CssClass="medium m-wrap">
				                    <asp:ListItem Value="0" >ประเมินผลตัวชี้วัด  </asp:ListItem>
				                    <asp:ListItem Value="1" Selected="true">กำหนดแบบประเมิน </asp:ListItem>
			                </asp:DropDownList>                              
						</h3>		
						
						<ul class="breadcrumb">
                            <li>
                            	<i class="icon-group"></i> <a href="javascript:;">การประเมินพนักงาน</a><i class="icon-angle-right"></i>
                            </li>
                        	<li>
                        	    <i class="icon-th-list"></i> <a href="javascript:;">กำหนดแบบประเมินตัวชี้วัด (KPI)</a>
                        	</li>                        	
                        	
                            <uc1:WUC_DateReporter ID="WUC_DateReporter1" runat="server" />
                        </ul>
						<!-- END PAGE TITLE & BREADCRUMB-->
									        
				     </div>			
			    </div>
		        <!-- END PAGE HEADER-->
				
                <asp:Panel ID="pnlList" runat="server" Visible="True" DefaultButton="btnSearch"> 
                <div class="row-fluid">
					
					<div class="span12">
						<!-- BEGIN SAMPLE TABLE PORTLET-->
				                    
									<div class="row-fluid form-horizontal">
									    <div class="span4 ">
											<div class="control-group">
										        <label class="control-label"><i class="icon-user"></i> ชื่อ</label>
										        <div class="controls">
											        <asp:TextBox ID="txtName" OnTextChanged="Search" runat="server" AutoPostBack="true" CssClass="m-wrap medium" placeholder="ค้นหาจากชื่อ/เลขประจำตัว"></asp:TextBox>
										        </div>
									        </div>
										</div>
										<div class="span3 ">
											<div class="control-group">
										        <label class="control-label"><i class="icon-info-sign"></i> สถานะ</label>
										        <div class="controls">
											        <asp:DropDownList ID="ddlStatus" OnSelectedIndexChanged="Search" runat="server" AutoPostBack="true" CssClass="medium m-wrap">
											        </asp:DropDownList>
										        </div>
									        </div>
										</div>
									</div>
								    <asp:Button ID="btnSearch" OnClick="Search" runat="server" Text="" style="display:none;" />
								    
							<div class="portlet-body no-more-tables">
								
								<asp:Label ID="lblCountPSN" runat="server" Font-Size="14px" Font-Bold="true"></asp:Label>
								<table class="table table-full-width table-advance dataTable no-more-tables table-hover">
									<thead>
										<tr>
											<th><i class="icon-sitemap"></i> หน่วยงาน</th>
											<th><i class="icon-user"></i> เลขประจำตัว</th>
											<th><i class="icon-user"></i> ชื่อ</th>
											<th><i class="icon-briefcase"></i> ตำแหน่ง</th>
											<th><i class="icon-bookmark"></i> ระดับ</th>
											<th><i class="icon-info"></i> สถานะ</th>
											<th><i class="icon-bolt"></i> ดำเนินการ</th>
										</tr>
									</thead>
									<tbody>
										<asp:Repeater ID="rptKPI" OnItemCommand="rptKPI_ItemCommand" OnItemDataBound="rptKPI_ItemDataBound" runat="server">
									        <ItemTemplate>
    									        <tr>                                        
											        <td data-title="หน่วยงาน"><asp:Label ID="lnkPSNOrganize" runat="server"></asp:Label></td>
											        <td data-title="เลขประจำตัว"><asp:Label ID="lblPSNNo" runat="server"></asp:Label></td>
											        <td data-title="ชื่อ"><asp:Label ID="lblPSNName" runat="server"></asp:Label></td>
											        <td data-title="ตำแหน่ง"><asp:Label ID="lblPSNPos" runat="server"></asp:Label></td>
											        <td data-title="ตำแหน่ง"><asp:Label ID="lblPSNClass" runat="server"></asp:Label></td>
											        <td data-title="สถานะ"><asp:Label ID="lblPSNKPIStatus" runat="server"></asp:Label></td>
											        <td data-title="ดำเนินการ">
											            <asp:Button ID="btnPSNEdit" runat="server" CssClass="btn mini blue" CommandName="Edit" Text="การกำหนดแบบประเมิน" /> 
											            <asp:Button ID="btnPSNDelete" runat="server" CssClass="btn mini red" CommandName="Delete" Text="ลบแบบประเมิน" />
											            <asp:ConfirmButtonExtender TargetControlID="btnPSNDelete" ID="cfm_Delete" runat="server"></asp:ConfirmButtonExtender>
										            </td>
										        </tr>	
									        </ItemTemplate>
									    </asp:Repeater>					
									</tbody>
								</table>
								
								<asp:PageNavigation ID="Pager" OnPageChanging="Pager_PageChanging" MaximunPageCount="10" PageSize="20" runat="server" />
							</div>
						<!-- END SAMPLE TABLE PORTLET-->						
					</div>
                </div>
                </asp:Panel>
  
                <asp:Panel ID="pnlEdit" runat="server" Visible="False">              
				            <div class="portlet-body form">
            										
			                     <div class="form-horizontal form-view">
    			                         <div class="btn-group pull-right">
			                                <div class="btn-group">
                                                <a data-toggle="dropdown" class="btn  red dropdown-toggle"  data-trigger="hover" data-container="body" data-placement="top" data-content="Popover body goes here! Popover body goes here!" data-original-title="Popover in top">แก้ไข <i class="icon-angle-down"></i></a>										
			                                    <ul class="dropdown-menu pull-right" >                                                       
                                                    <%--<li><a id="btnCopyAll" runat="server" target="_blank">คัดลอก KPI</a></li>
                                                    <li><a id="btnPasteAll" runat="server" target="_blank">วาง</a></li>--%>
                                                    <li><a ><asp:LinkButton id="lnkCopyAll" OnClick="lnkCopyAll_Click" runat ="server"  ><i class="icon-copy"></i> คัดลอกทั้งหมด</asp:LinkButton></a></li>	
                                                    <li><a ><asp:LinkButton id="lnkPasteAll" OnClick="lnkPasteAll_Click" runat ="server" ><i class="icon-paste"></i> วางแทนที่ทั้งหมด</asp:LinkButton></a></li>											
			                                    </ul>
                                            </div>

                                            <div class="btn-group">
                                                <a data-toggle="dropdown" class="btn dropdown-toggle">พิมพ์ <i class="icon-angle-down"></i></a>										
			                                    <ul class="dropdown-menu pull-right">                                                       
                                                    <li><a id="btnPDF" runat="server" target="_blank">รูปแบบ PDF</a></li>
                                                    <li><a id="btnExcel" runat="server" target="_blank">รูปแบบ Excel</a></li>											
			                                    </ul>
                                            </div>                                    
		                                </div>
                                        <div id="boxAlertEdit" runat ="server"  class="alert" style =" margin-top :-10px;">
												<li>
													สำหรับ <b style ="">การคัดลอกและวาง</b> แบบประเมินตัวชี้วัดพนักงาน (KPI)  ทั้งตาราง
												</li>
												<li>
												    โดยสามารถ เปิดหน้าแบบประเมินที่ต้องการหรือเลือกจากรอบประเมินที่ต้องการ คลิกปุ่ม <b>แก้ไข > คัดลอกทั้งหมด</b>
												</li>
												<li>
												    <u>วิธีการวาง</u> เปิดหน้า กำหนดแบบประเมินตัวชี้วัด (KPI) ของพนักงานที่ต้องการ วางแบบประเมิน คลิกปุ่ม <b>แก้ไข > วางแทนที่ทั้งหมด</b>
												</li>
									  </div>
    			                         <%--<div class="btn-group pull-right">                                    
			                                <a data-toggle="dropdown" class="btn dropdown-toggle">พิมพ์ <i class="icon-angle-down"></i></a>										
			                                <ul class="dropdown-menu pull-right">                                                       
                                                <li><a id="btnPDF" runat="server" target="_blank">รูปแบบ PDF</a></li>
                                                <li><a id="btnExcel" runat="server" target="_blank">รูปแบบ Excel</a></li>											
			                                </ul>
		                                </div>--%>
    			                         	                               
					                                <h3 style="text-align:center; margin-top:0px;">
					                                    <asp:Label ID="lblKPIStatus" runat="server" KPI_Status="0"></asp:Label> (KPI)
					                                </h3>   
    			                          
    			                        <div class="row-fluid" style="border-bottom:1px solid #EEEEEE;">
					                        <div class="span4 ">
					                            <div class="control-group">
								                    <label class="control-label" style="width:100px;">ผู้รับการประเมิน :</label>
								                    <div class="controls" style="margin-left: 120px;">
									                    <asp:Label ID="lblPSNName" runat="server" CssClass="text bold"></asp:Label>
								                    </div>
							                    </div>
					                        </div>
					                        <div class="span3 ">
					                            <div class="control-group">
								                    <label class="control-label" style="width:100px;">ตำแหน่ง :</label>
								                    <div class="controls" style="margin-left: 120px;">
									                    <asp:Label ID="lblPSNPos" runat="server" CssClass="text bold"></asp:Label>
								                    </div>
							                    </div>
					                        </div>
					                        <div class="span5 ">
					                            <div class="control-group">
								                    <label class="control-label" style="width:100px;">สังกัด :</label>
								                    <div class="controls" style="margin-left: 120px;">
									                    <asp:Label ID="lblPSNDept" runat="server" CssClass="text bold"></asp:Label>
								                    </div>
							                    </div>
					                        </div>
					                     </div>
                					     
					                      <div class="row-fluid" style="margin-top:10px;">
					                        <div class="span4 ">
					                            <div class="control-group">
								                    <label class="control-label" style="width:100px;">ผู้ประเมิน :</label>
								                    <div class="controls" style="margin-left: 120px;">
									                    <asp:DropDownList ID="ddlASSName"  OnSelectedIndexChanged="ddlASSName_SelectedIndexChanged"  runat="Server" AutoPostBack="True" style="font-weight:bold; color:Black; border:none;">									        
									                    </asp:DropDownList>
									                </div>
							                    </div>
					                        </div>
					                        <div class="span3 ">
					                            <div class="control-group">
								                    <label class="control-label" style="width:100px;">ตำแหน่ง :</label>
								                    <div class="controls" style="margin-left: 120px;">
									                    <asp:Label ID="lblASSPos" runat="server" CssClass="text bold"></asp:Label>
								                    </div>
							                    </div>
					                        </div>
					                        <div class="span5 ">
					                            <div class="control-group">
								                    <label class="control-label" style="width:100px;">สังกัด :</label>
								                    <div class="controls" style="margin-left: 120px;">
									                    <asp:Label ID="lblASSDept" runat="server" CssClass="text bold"></asp:Label>
								                    </div>
							                    </div>
					                        </div>
					                     </div>
					             </div>
    			             			
			                        <!-- BEGIN FORM-->
			                        <div class="portlet-body no-more-tables" id="divView" runat="server">
	                                           <table class="table table-bordered no-more-tables" style="border-bottom:none; background-color:White;">
						                            <thead>
							                            <tr>
								                            <th rowspan="2" style="text-align:center; background-color:Silver;"> ลำดับ</th>
								                            <th rowspan="2" style="text-align:center; background-color:Silver;"> งาน</th>
								                            <th rowspan="2" style="text-align:center; background-color:Silver;"> ตัวชี้วัดผลงาน</th>
								                            <th colspan="5" style="text-align:center; background-color:Silver;"> คะแนนตามระดับค่าเป้าหมาย</th>
								                            <th rowspan="2" style="text-align:center; background-color:Silver;"> น้ำหนัก(%)</th>											        
							                            </tr>
                                                        <tr>
							                              <th style="text-align:center; font-weight:bold; vertical-align:middle; background-color:Silver;">1</th>
							                              <th style="text-align:center; font-weight:bold; vertical-align:middle; background-color:Silver;">2</th>
							                              <th style="text-align:center; font-weight:bold; vertical-align:middle; background-color:Silver;">3</th>
							                              <th style="text-align:center; font-weight:bold; vertical-align:middle; background-color:Silver;">4</th>
							                              <th style="text-align:center; font-weight:bold; vertical-align:middle; background-color:Silver;">5</th>
					                                  </tr>
						                            </thead>
						                            <tbody>
						                                <asp:Panel ID="pnlKPI" runat="server">							            
						                                    <asp:Repeater ID="rptAss" OnItemCommand="rptAss_ItemCommand" OnItemDataBound="rptAss_ItemDataBound" runat="server">
						                                    <ItemTemplate>
						                                    <tr>
							                                  <td data-title="ลำดับ" id="ass_no" runat="server" title="Click เพื่อแก้ไข" style="text-align:center; font-weight:bold; vertical-align:top; cursor:pointer;">001</td>
							                                  <td rowspan="2" data-title="งาน" id="job" title="Click เพื่อแก้ไข" runat="server" style="background-color:#f8f8f8; cursor:pointer;">xxxxxxxxxxxx xxxxxxxxxxxx</td>
							                                  <td rowspan="2" data-title="ตัวชี้วัดผลงาน" id="target" runat="server" title="Click เพื่อแก้ไข" class="TextLevel0" style="background-color:#f8f8f8; cursor:pointer; ">xxxxxxxxxxxx xxxxxxxxxxxx</td>
							                                  <td rowspan="2" data-title="ระดับ 1 คะแนน" title="Click เพื่อแก้ไข" id="choice1" class="TextLevel0" runat="server" style="cursor:pointer;">xxxxxxxxxxxx xxxxxxxxxxxx xxxxxxxxxxxx xxxxxxxxxxxx xxxxxxxxxxxx xxxxxxxxxxxx</td>
                                                              <td rowspan="2" data-title="ระดับ 2 คะแนน" title="Click เพื่อแก้ไข" id="choice2" class="TextLevel0" runat="server" style="cursor:pointer;">xxxxxxxxxxxx xxxxxxxxxxxx xxxxxxxxxxxx xxxxxxxxxxxx xxxxxxxxxxxx xxxxxxxxxxxx</td>
                                                              <td rowspan="2" data-title="ระดับ 3 คะแนน" title="Click เพื่อแก้ไข" id="choice3" class="TextLevel0" runat="server" style="cursor:pointer;">xxxxxxxxxxxx xxxxxxxxxxxx xxxxxxxxxxxx xxxxxxxxxxxx xxxxxxxxxxxx xxxxxxxxxxxx</td>
                                                              <td rowspan="2" data-title="ระดับ 4 คะแนน" title="Click เพื่อแก้ไข" id="choice4" class="TextLevel0" runat="server" style="cursor:pointer;">xxxxxxxxxxxx xxxxxxxxxxxx xxxxxxxxxxxx xxxxxxxxxxxx xxxxxxxxxxxx xxxxxxxxxxxx</td>
                                                              <td rowspan="2" data-title="ระดับ 5 คะแนน" title="Click เพื่อแก้ไข" id="choice5" class="TextLevel0" runat="server" style="cursor:pointer;">xxxxxxxxxxxx xxxxxxxxxxxx xxxxxxxxxxxx xxxxxxxxxxxx xxxxxxxxxxxx xxxxxxxxxxxx</td>
							                                  <td data-title="น้ำหนัก" id="weight" runat="server" title="Click เพื่อแก้ไข" style="text-align:center; cursor:pointer; font-size:16px; font-weight:bold; vertical-align:middle;">10%</td>
            										          
						                                    </tr>
						                                    <tr>
						                                          <td data-title="ลบ" id="cel_delete" runat="server" style="cursor:pointer; font-weight:bold; text-align:center; padding:0px !important; height:10px !important; background-color:silver;">
        									                      
						                                          <asp:Button ID="btn_Delete" runat="server" style="display:none; " CommandName="Delete" />ลบ
						                                          <asp:ConfirmButtonExtender TargetControlID="btn_Delete" ID="cfm_Delete" runat="server"></asp:ConfirmButtonExtender>
						                                          </td>
						                                          <td data-title="แก้ไข" id="cel_edit" runat="server" style="cursor:pointer; font-weight:bold; text-align:center; padding:0px !important; height:10px !important; background-color:Silver;"><asp:Button ID="btn_Edit" runat="server" style="display:none; " CommandName="Edit"  />แก้ไข</td>
						                                    </tr>
						                                    </ItemTemplate>
						                                    <FooterTemplate>									                
						                                     <tfoot>
						                                            <tr>
						                                                <td colspan="8" style="text-align:center; background-color:#EEEEEE; font-size:14px; font-weight:bold;">
						                                                ผลรวมน้ำหนัก
						                                                </td>
						                                                <td style="text-align:center; background-color:#EEEEEE; font-size:16px; font-weight:bold;" id="cell_sum_weight" runat="server"></td>
						                                            </tr>
						                                        </tfoot>									                
						                                    </FooterTemplate>
						                                </asp:Repeater>
						                                </asp:Panel>	
						                            </tbody>									        
					                            </table>
        								        		
							                    <asp:Panel CssClass="form-actions" id="pnlActivity" runat="server">
							                        <asp:Button ID="btnAdd" OnClick="btnAdd_Click" runat="server" CssClass="btn purple" Text="เพิ่มตัวชี้วัด" />
							                        <asp:Button ID="btnPreSend" OnClick="btnPreSend_Click" runat="server" CssClass="btn green" Text="อนุมัติแบบประเมิน (หลังจากอนุมัติแบบประเมินแล้วคุณไม่สามารถแก้ไขได้)" />
							                        <asp:Button ID="btnSend" OnClick="btnSend_Click" runat="server" Text="" style="display:none;" />
							                        <asp:Button ID="btnCancel" OnClick="btnCancel_Click" CssClass="btn" runat="server" Text="ย้อนกลับ" />							                        
							                    </asp:Panel>
        				                
						                </div>
						            
        					            <div class="row-fluid" id="divEdit" runat="server" >
                                            <div class="span12">
		                                                <div class="portlet-body form">
							                                <h3 class="form-section"><i class="icon-th-list"></i> 
							                                    กำหนดตัวชี้วัดผลงาน ลำดับ <asp:Label ID="lblKPINo" runat="server"></asp:Label>
							                                </h3>
							                                <div class="row-fluid form-horizontal">
							                                    <div class="span4 ">
									                                <div class="control-group">
								                                        <label class="control-label">งาน</label>
								                                        <div class="controls">
								                                            <asp:TextBox TextMode="MultiLine" runat="server" ID="txt_KPI_Job" CssClass="large m-wrap" Height="100px"></asp:TextBox>
									                                    </div>
							                                        </div>
							                                        <div class="control-group">
								                                        <label class="control-label">ตัวชี้วัดผลงาน</label>
								                                        <div class="controls">
									                                        <asp:TextBox TextMode="MultiLine" runat="server" ID="txt_KPI_Target" CssClass="large m-wrap" Height="100px"></asp:TextBox>
								                                        </div>
							                                        </div>
							                                        <div class="control-group">
								                                        <label class="control-label">1 คะแนน</label>
								                                        <div class="controls">
									                                        <asp:TextBox TextMode="MultiLine" runat="server" ID="txt_KPI_Choice1" CssClass="large m-wrap" Height="100px"></asp:TextBox>
								                                        </div>
							                                        </div>
							                                        <div class="control-group">
								                                        <label class="control-label">2 คะแนน</label>
								                                        <div class="controls">
									                                        <asp:TextBox TextMode="MultiLine" runat="server" ID="txt_KPI_Choice2" CssClass="large m-wrap" Height="100px"></asp:TextBox>
								                                        </div>
							                                        </div>
							                                        <div class="control-group">
								                                        <label class="control-label">3 คะแนน</label>
								                                        <div class="controls">
									                                        <asp:TextBox TextMode="MultiLine" runat="server" ID="txt_KPI_Choice3" CssClass="large m-wrap" Height="100px"></asp:TextBox>
								                                        </div>
							                                        </div>
							                                        <div class="control-group">
								                                        <label class="control-label">4 คะแนน</label>
								                                        <div class="controls">
									                                        <asp:TextBox TextMode="MultiLine" runat="server" ID="txt_KPI_Choice4" CssClass="large m-wrap" Height="100px"></asp:TextBox>
								                                        </div>
							                                        </div>
							                                        <div class="control-group">
								                                        <label class="control-label">5 คะแนน</label>
								                                        <div class="controls">
									                                        <asp:TextBox TextMode="MultiLine" runat="server" ID="txt_KPI_Choice5" CssClass="large m-wrap" Height="100px"></asp:TextBox>
								                                        </div>
							                                        </div>
							                                        <div class="control-group">
								                                        <label class="control-label">น้ำหนัก</label>
								                                        <div class="controls">
									                                        <asp:TextBox runat="server" ID="txt_KPI_Weight" CssClass="m-wrap small" ></asp:TextBox>
									                                        <span class="help-inline">%</span>
								                                        </div>
							                                        </div>
								                                </div>													
							                                </div>
						                                <div class="form-actions">
						                                    <asp:Button ID="btnOK" OnClick="btnOK_Click" runat="server" CssClass="btn blue" Text="บันทึก" />
					                                        <asp:Button ID="btnClose" OnClick="btnClose_Click" runat="server" CssClass="btn" Text="ยกเลิก" />
				                                        </div>												
					                                </div>
					                            </div>
                                        </div>	
				                <!-- END FORM-->  
            				   
			                </div>	
			                
			                
                                
			   </asp:Panel>
                  		 
</div>		

</ContentTemplate>           
</asp:UpdatePanel>				 
</asp:Content>


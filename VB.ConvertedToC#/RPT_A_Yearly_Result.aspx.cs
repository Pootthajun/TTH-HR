using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Xml.Linq;
using System.Diagnostics;
using System.Collections.Specialized;
using System.Configuration;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Caching;
using System.Web.SessionState;
using System.Web.Security;
using System.Web.Profile;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
namespace VB
{

	public partial class RPT_A_Yearly_Result : System.Web.UI.Page
	{


		HRBL BL = new HRBL();
		GenericLib GL = new GenericLib();

		Converter C = new Converter();
		public int R_Year {
			get {
				try {
					return GL.CINT( GL.SplitString(ddlYear.Items[ddlYear.SelectedIndex].Value, "-")[0]);
				} catch (Exception ex) {
					return 0;
				}
			}
		}

		protected void Page_Load(object sender, System.EventArgs e)
		{
			if ((Session["USER_PSNL_NO"] == null)) {
				ScriptManager.RegisterStartupScript(this.Page, typeof(string), "Alert", "showAlert('กรุณาลงชื่อเข้าใช้ระบบ');", true);
				Response.Redirect("Login.aspx");
				return;
			}

			if (!IsPostBack) {

                // Keep Start Time
                DateTime StartTime = DateTime.Now;

				//----------------- ทำสองที่ Page Load กับ Update Status ---------------
				BL.BindDDlYear(ddlYear);
				ddlYear.Items.RemoveAt(0);
				BL.BindDDlSector(ddlSector);
				BindPersonalList();

                //Report Time To Entry This Page
                DateTime EndTime = DateTime.Now;
                lblReportTime.Text = "เริ่มเข้าสู่หน้านี้ " + StartTime.ToString("HH:mm:ss.ff") + " ถึง " + EndTime.ToString("HH:mm:ss.ff") + " ใช้เวลา " + Math.Round( GL.DiffTimeDecimalSeconds(StartTime,EndTime),2) + " วินาที";
			}

			SetCheckboxStyle();
		}

		private void SetCheckboxStyle()
		{
			if (chkKPIYes.Checked) {
				chkKPIYes.CssClass = "checked";
			} else {
				chkKPIYes.CssClass = "";
			}
			if (chkKPINo.Checked) {
				chkKPINo.CssClass = "checked";
			} else {
				chkKPINo.CssClass = "";
			}
			if (chkCOMPYes.Checked) {
				chkCOMPYes.CssClass = "checked";
			} else {
				chkCOMPYes.CssClass = "";
			}
			if (chkCOMPNo.Checked) {
				chkCOMPNo.CssClass = "checked";
			} else {
				chkCOMPNo.CssClass = "";
			}

			for (int i = 1; i <= 13; i++) {
                CheckBox chk = (CheckBox)pnlList.FindControl("chkClass" + i);
				if (chk.Checked) {
					chk.CssClass = "checked";
				} else {
					chk.CssClass = "";
				}
			}
                        
		}

        protected void btnSearch_Click(object sender, System.EventArgs e)
		{
			BindPersonalList();
		}


        private void SetHeaderColumn()
        {
            string SQL = "SELECT * FROM tb_HR_Round WHERE R_Year=" + R_Year + "";
            SqlDataAdapter DA = new SqlDataAdapter(SQL, BL.ConnectionString());
            DataTable DT = new DataTable();
            DA.Fill(DT);

            if (DT.Rows.Count > 0)
            {
                for (int i = 0; i <= DT.Rows.Count - 1; i++)
                {
                    switch (GL.CINT(DT.Rows[i]["R_Round"]))
                    {
                        case 1:
                            lblHeaderKPI_1.Text = "KPI " + GL.CINT(DT.Rows[i]["Weight_KPI"]) + "%";
                            lblHeaderCOMP_1.Text = "COMP " + GL.CINT(DT.Rows[i]["Weight_COMP"]) + "%";
                            break;
                        case 2:
                            lblHeaderKPI_2.Text = "KPI " + GL.CINT(DT.Rows[i]["Weight_KPI"]) + "%";
                            lblHeaderCOMP_2.Text = "COMP " + GL.CINT(DT.Rows[i]["Weight_COMP"]) + "%";
                            break;
                    }

                }

            }

            Session["RPT_HeaderKPI_1"] = lblHeaderKPI_1.Text;
            Session["RPT_HeaderKPI_2"] = lblHeaderKPI_2.Text;
            Session["RPT_HeaderCOMP_1"] = lblHeaderCOMP_1.Text;
            Session["RPT_HeaderCOMP_2"] = lblHeaderCOMP_2.Text;

        }
		private void BindPersonalList()
		{
            SetHeaderColumn();
            string SQL = " SELECT DISTINCT vw_RPT_Yearly_Result.*,PSN.PSNL_STAT,PSN.STAT_NAME FROM vw_RPT_Yearly_Result vw_RPT_Yearly_Result " + "\n";
          
            SQL +=" LEFT JOIN vw_PN_PSNL_ALL PSN ON vw_RPT_Yearly_Result.PSNL_NO=PSN.PSNL_NO  " + "\n";
            //SQL += " WHERE (ISNULL(PSN.PNPS_RESIGN_DATE,PSN.PNPS_RETIRE_DATE) > (SELECT MAX(R_End) FROM tb_HR_Round WHERE R_Year=" + R_Year  + ")) " + "\n";



            string Filter = "R_Year=" + R_Year + " ";

			string Title = "ประจำปี " + ddlYear.Items[ddlYear.SelectedIndex].Text + " ";

			if (ddlSector.SelectedIndex > 0) {

                Filter += " AND ( SECTOR_CODE='" + ddlSector.Items[ddlSector.SelectedIndex].Value + "' " + "\n";
				if (ddlSector.Items[ddlSector.SelectedIndex].Value == "3200") {
                    Filter += " AND  DEPT_CODE NOT IN ('32000300'))    " + "\n";
				//-----------ฝ่ายโรงงานผลิตยาสูบ 3
				} else if (ddlSector.Items[ddlSector.SelectedIndex].Value == "3203") {
                    Filter += " OR  DEPT_CODE IN ('32000300','32030000','32030100','32030200','32030300','32030500'))  " + "\n";
				//-----------ฝ่ายโรงงานผลิตยาสูบ 4
				} else if (ddlSector.Items[ddlSector.SelectedIndex].Value == "3204") {
                    Filter += " OR  DEPT_CODE IN ('32040000','32040100','32040200','32040300','32040500','32040300'))  " + "\n";
				//-----------ฝ่ายโรงงานผลิตยาสูบ 5
				} else if (ddlSector.Items[ddlSector.SelectedIndex].Value == "3205") {
                    Filter += " OR  DEPT_CODE IN ('32050000','32050100','32050200','32050300','32050500'))\t  " + "\n";
				} else {
                    Filter += ")   " + "\n";
				}


				Title += GL.SplitString(ddlSector.Items[ddlSector.SelectedIndex].Text, ":")[1].Trim() + " ";
			}

			if (!string.IsNullOrEmpty(txtDeptName.Text)) {
                Filter += " AND (DEPT_Name LIKE '%" + txtDeptName.Text.Replace("'", "''") + "%' OR Sector_Name LIKE '%" + txtDeptName.Text.Replace("'", "''") + "%') " + "\n";
				//Title &= " หน่วยงาน '" & txtDeptName.Text & "' "
			}
			if (!string.IsNullOrEmpty(txtName.Text)) {
                Filter += " AND (PSNL_Fullname LIKE '%" + txtName.Text.Replace("'", "''") + "%' OR PSNL_NO LIKE '%" + txtName.Text.Replace("'", "''") + "%') " + "\n";
				//Title &= " พนักงาน '" & txtName.Text & "' "
			}

			if ((chkClass1.Checked & chkClass2.Checked & chkClass3.Checked & chkClass4.Checked & chkClass5.Checked & chkClass6.Checked & chkClass7.Checked & chkClass8.Checked & chkClass9.Checked & chkClass10.Checked & chkClass11.Checked & chkClass12.Checked & chkClass13.Checked) | (!chkClass1.Checked & !chkClass2.Checked & !chkClass3.Checked & !chkClass4.Checked & !chkClass5.Checked & !chkClass6.Checked & !chkClass7.Checked & !chkClass8.Checked & !chkClass9.Checked & !chkClass10.Checked & !chkClass11.Checked & !chkClass12.Checked & !chkClass13.Checked)) {
				//----------- Do nothing--------
			} else {
				string _classFilter = "";
				string _classTitle = "";
				for (int i = 1; i <= 13; i++) {
                    CheckBox chk = (CheckBox)pnlList.FindControl("chkClass" + i);
					if (chk.Checked) {
						_classFilter += "'" + i.ToString().PadLeft(2, GL.chr0) + "',";
						_classTitle += i + ",";
					}
				}
				Title += " ระดับ " + _classTitle.Substring(0, _classTitle.Length - 1);
                Filter += " AND PNPS_CLASS IN (" + _classFilter.Substring(0, _classFilter.Length - 1) + ") " + "\n";
			}

			int KPIStat = 0;
			if ((chkKPIYes.Checked & chkKPINo.Checked) || (!chkKPIYes.Checked & !chkKPINo.Checked))
            {
                KPIStat = -1;
            }
            else if (chkKPIYes.Checked)
            {
                Filter += " AND KPI_Status_1=1 AND KPI_Status_2=1 " + "\n";
                KPIStat = 1;
            }
            else if (chkKPINo.Checked)
            {
                Filter += " AND (KPI_Status_1=0 OR KPI_Status_2=0) " + "\n";
                KPIStat = 0;
            }

			int COMPStat = 0;
			if ((chkCOMPYes.Checked & chkCOMPNo.Checked) || (!chkCOMPYes.Checked & !chkCOMPNo.Checked))
            {
                COMPStat = -1;
            }
            else if (chkCOMPYes.Checked)
            {
                Filter += " AND COMP_Status_1=1 AND COMP_Status_2=1 " + "\n";
                COMPStat = 1;
            }
            else if (chkCOMPNo.Checked)
            {
                Filter += " AND (COMP_Status_1=0 OR COMP_Status_2=0) " + "\n";
                COMPStat = 0;
            }

			if (KPIStat == COMPStat) {
				switch (KPIStat) {
					case -1:
						break;
					case 0:
						Title += "ที่การประเมินตัวชี้วัดและสมรรรถนะยังไม่เสร็จ";
						break;
					case 1:
						Title += "การประเมินเสร็จสมบูรณ์";
						break;
				}
			} else if (KPIStat == -1) {
				switch (COMPStat) {
					case 0:
						Title += "ที่การประเมินสมรรถนะยังไม่เสร็จ";
						break;
					case 1:
						Title += "ที่การประเมินสมรรถนะเสร็จสมบูรณ์";
						break;
				}
			} else if (KPIStat == 0) {
				switch (COMPStat) {
					case -1:
						Title += "ที่การประเมินตัวชี้วัดยังไม่เสร็จ";
						break;
					case 1:
						Title += "ที่การประเมินสมรรถนะเสร็จแล้วแต่ตัวชี้วัดยังไม่เสร็จ";
						break;
				}
			} else if (KPIStat == 1) {
				switch (COMPStat) {
					case -1:
						Title += "ที่การประเมินตัวชี้วัดสมบูรณ์";
						break;
					case 0:
						Title += "ที่การประเมินตัวชี้วัดเสร็จแล้วแต่สมรรถนะยังไม่เสร็จ";
						break;
				}
			}

            SQL += "  ORDER BY SECTOR_CODE ASC,SECTOR_NAME ASC ,DEPT_CODE ASC ,PNPS_CLASS DESC ,Year_Score  DESC" + "\n";

			SqlDataAdapter DA = new SqlDataAdapter(SQL, BL.ConnectionString());
			DA.SelectCommand.CommandTimeout = 200;
			DataTable DT = new DataTable();
			DA.Fill(DT);
            
            DT.DefaultView.RowFilter =  Filter;

            Session["RPT_A_Yearly_Result"] = DT.DefaultView.ToTable();
			//--------- STAT --------------

			//--------- Title --------------
			Session["RPT_A_Yearly_Result_Title"] = Title;
			//--------- Sub Title ----------
			Session["RPT_A_Yearly_Result_SubTitle"] = "รายงาน ณ วันที่ " + DateTime.Now.Day + " " + C.ToMonthNameTH(DateTime.Now.Month) + " พ.ศ." + (DateTime.Now.Year + 543);
            Session["RPT_A_Yearly_Result_SubTitle"] += "  พบ   " + GL.StringFormatNumber(DT.DefaultView.Count, 0) + " รายการ ";

			Pager.SesssionSourceName = "RPT_A_Yearly_Result";
			Pager.RenderLayout();
			lblTitle.Text = Title;
			if (DT.DefaultView.Count == 0) {
				lblCountPSN.Text = "ไม่พบรายการดังกล่าว";
			} else {
                lblCountPSN.Text = "พบ " + GL.StringFormatNumber(DT.DefaultView.Count, 0) + " รายการ";
			}

		}

		protected void Pager_PageChanging(PageNavigation Sender)
		{
			Pager.TheRepeater = rptASS;
		}

		//------------ For Grouping -----------
		string LastSector = "";
		string LastDept = "";

		protected void rptASS_ItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
		{
			if (e.Item.ItemType != ListItemType.AlternatingItem & e.Item.ItemType != ListItemType.Item)
				return;

            Label lblSector = (Label)e.Item.FindControl("lblSector");
            Label lblDept = (Label)e.Item.FindControl("lblDept");
            Label lblPSNNo = (Label)e.Item.FindControl("lblPSNNo");
            Label lblPSNName = (Label)e.Item.FindControl("lblPSNName");
            Label lblPSNPos = (Label)e.Item.FindControl("lblPSNPos");
            Label lblPSNClass = (Label)e.Item.FindControl("lblPSNClass");
            Label lblSTAT = (Label)e.Item.FindControl("lblSTAT");

            HtmlAnchor img_KPI_Status_1 = (HtmlAnchor)e.Item.FindControl("img_KPI_Status_1");
            HtmlAnchor lbl_KPI_Score_1 = (HtmlAnchor)e.Item.FindControl("lbl_KPI_Score_1");
            HtmlAnchor img_KPI_Status_2 = (HtmlAnchor)e.Item.FindControl("img_KPI_Status_2");
            HtmlAnchor lbl_KPI_Score_2 = (HtmlAnchor)e.Item.FindControl("lbl_KPI_Score_2");
            HtmlAnchor lbl_Score_1 = (HtmlAnchor)e.Item.FindControl("lbl_Score_1");
            HtmlAnchor img_COMP_Status_1 = (HtmlAnchor)e.Item.FindControl("img_COMP_Status_1");
            HtmlAnchor lbl_COMP_Score_1 = (HtmlAnchor)e.Item.FindControl("lbl_COMP_Score_1");
            HtmlAnchor img_COMP_Status_2 = (HtmlAnchor)e.Item.FindControl("img_COMP_Status_2");
            HtmlAnchor lbl_COMP_Score_2 = (HtmlAnchor)e.Item.FindControl("lbl_COMP_Score_2");
            HtmlAnchor lbl_Score_2 = (HtmlAnchor)e.Item.FindControl("lbl_Score_2");

            HtmlAnchor lbl_Score = (HtmlAnchor)e.Item.FindControl("lbl_Score");
            HtmlAnchor lbl_Leave_Day = (HtmlAnchor)e.Item.FindControl("lbl_Leave_Day");
            HtmlAnchor lbl_Leave_Score = (HtmlAnchor)e.Item.FindControl("lbl_Leave_Score");
            HtmlAnchor lbl_Year_Result = (HtmlAnchor)e.Item.FindControl("lbl_Year_Result");

            HtmlTableRow tdSector = (HtmlTableRow)e.Item.FindControl("tdSector");
            HtmlTableRow tdDept = (HtmlTableRow)e.Item.FindControl("tdDept");

            DataRowView drv = (DataRowView)e.Item.DataItem;

			//---------- แยกหน่วยงาน---------
			if (drv["SECTOR_NAME"].ToString() != LastSector) {
				LastSector = drv["SECTOR_NAME"].ToString();
				lblSector.Text = LastSector;
				LastDept = drv["DEPT_NAME"].ToString();
				lblDept.Text = LastDept;
			//---------- แยกสมรรถนะ---------
			} else if (LastDept != drv["DEPT_NAME"].ToString()) {
				tdSector.Visible = false;
				LastDept = drv["DEPT_NAME"].ToString();
				lblDept.Text = LastDept;
			} else {
				tdSector.Visible = false;
				tdDept.Visible = false;
			}

			lblPSNNo.Text = drv["PSNL_NO"].ToString();
			lblPSNName.Text = drv["PSNL_Fullname"].ToString();
			lblPSNPos.Text = drv["POS_Name"].ToString();
			if (!GL.IsEqualNull(drv["PNPS_CLASS"])) {
				lblPSNClass.Text = GL.CINT(drv["PNPS_CLASS"]).ToString();
			}


           
            if (GL.CINT(drv["KPI_Status_1"]) == 1)
            {
                img_KPI_Status_1.InnerHtml = "<img src='images/check.png' />";
                lbl_KPI_Score_1.InnerHtml = GL.StringFormatNumber(drv["KPI_Result_1"]);
                lbl_KPI_Score_1.Style["color"] = "blue";
            }
            else
            {
                if (GL.CDBL(drv["KPI_Result_1"]) != 0)
                {
                    lbl_KPI_Score_1.InnerHtml = GL.StringFormatNumber(drv["KPI_Result_1"]);
                }
                else {
                    lbl_KPI_Score_1.InnerHtml = "-";
                }
                    img_KPI_Status_1.InnerHtml = "<img src='images/none.png' />";
                    lbl_KPI_Score_1.Style["color"] = "red";
            }

            if (GL.CINT(drv["KPI_Status_2"]) == 1)
            {
                img_KPI_Status_2.InnerHtml = "<img src='images/check.png' />";
                lbl_KPI_Score_2.InnerHtml = GL.StringFormatNumber(drv["KPI_Result_2"]);
                lbl_KPI_Score_2.Style["color"] = "blue";
            }
            else
            {
                if (GL.CDBL(drv["KPI_Result_2"]) != 0)
                {
                    lbl_KPI_Score_2.InnerHtml = GL.StringFormatNumber(drv["KPI_Result_2"]);
                }
                else
                {
                    lbl_KPI_Score_2.InnerHtml = "-";
                }
                img_KPI_Status_2.InnerHtml = "<img src='images/none.png' />";
                lbl_KPI_Score_2.Style["color"] = "red";
            }

            if (GL.CINT(drv["COMP_Status_1"]) == 1)
            {
                img_COMP_Status_1.InnerHtml = "<img src='images/check.png' />";
                lbl_COMP_Score_1.InnerHtml = GL.StringFormatNumber(drv["COMP_Result_1"]);
                lbl_COMP_Score_1.Style["color"] = "blue";
            }
            else
            {
                if (GL.CDBL(drv["COMP_Result_1"]) != 0)
                {
                    lbl_COMP_Score_1.InnerHtml = GL.StringFormatNumber(drv["COMP_Result_1"]);
                }
                else
                {
                    lbl_COMP_Score_1.InnerHtml = "-";
                }
                img_COMP_Status_1.InnerHtml = "<img src='images/none.png' />";
                lbl_COMP_Score_1.Style["color"] = "red";
            }

            if (GL.CINT(drv["COMP_Status_2"]) == 1)
            {
                img_COMP_Status_2.InnerHtml = "<img src='images/check.png' />";
                lbl_COMP_Score_2.InnerHtml = GL.StringFormatNumber(drv["COMP_Result_2"]);
                lbl_COMP_Score_2.Style["color"] = "blue";
            }
            else
            {
                if (GL.CDBL(drv["COMP_Result_2"]) != 0)
                {
                    lbl_COMP_Score_2.InnerHtml = GL.StringFormatNumber(drv["COMP_Result_2"]);
                }
                else
                {
                    lbl_COMP_Score_2.InnerHtml = "-";
                }
                img_COMP_Status_2.InnerHtml = "<img src='images/none.png' />";
                lbl_COMP_Score_2.Style["color"] = "red";
            }

            if (GL.CDBL(drv["Ass_Score_1"]) != 0 && (GL.CINT(drv["KPI_Status_1"]) == 1 && GL.CINT(drv["COMP_Status_1"]) == 1))
            {
				lbl_Score_1.InnerHtml = GL.StringFormatNumber(drv["Ass_Score_1"]);
                lbl_Score_1.Style["color"] = "blue";
            }else{
                if (GL.CDBL(drv["Ass_Score_1"]) != 0)
                {
                    lbl_Score_1.InnerHtml = GL.StringFormatNumber(drv["Ass_Score_1"]);
                }
                else
                {
                    lbl_Score_1.InnerHtml = "-";
                }
                lbl_Score_1.Style["color"] = "red";
			}

            if (GL.CDBL(drv["Ass_Score_2"]) != 0 && (GL.CINT(drv["KPI_Status_2"]) == 1 && GL.CINT(drv["COMP_Status_2"]) == 1))
            {
				lbl_Score_2.InnerHtml = GL.StringFormatNumber(drv["Ass_Score_2"]);
                lbl_Score_2.Style["color"] = "blue";
            }else{
                if (GL.CDBL(drv["Ass_Score_2"]) != 0)
                {
                    lbl_Score_2.InnerHtml = GL.StringFormatNumber(drv["Ass_Score_2"]);
                }
                else
                {
                    lbl_Score_2.InnerHtml = "-";
                }
                lbl_Score_2.Style["color"] = "red";
			}

            if (GL.CDBL(drv["Leave_Day"]) != 0)
            {
				lbl_Leave_Day.InnerHtml = GL.StringFormatNumber(drv["Leave_Day"], 2);
			} else {
				lbl_Leave_Day.InnerHtml = "-";
			}

            if (GL.CDBL(drv["Ass_Score"]) > 0)
            {
				lbl_Score.InnerHtml = GL.StringFormatNumber(drv["Ass_Score"]);
			} else {
				lbl_Score.InnerHtml = "-";
			}

            if (GL.CDBL(drv["Leave_Score"]) != 0)
            {
				lbl_Leave_Score.InnerHtml = GL.StringFormatNumber(drv["Leave_Score"], 2);
			} else {
				lbl_Leave_Score.InnerHtml = "-";
			}

            if (GL.CDBL(drv["Year_Score"]) > 0)
            {
				lbl_Year_Result.InnerHtml = GL.StringFormatNumber(drv["Year_Score"], 2);
			} else {
				lbl_Year_Result.InnerHtml = "-";
			}

			img_KPI_Status_1.HRef = "Print/RPT_K_PNSAss.aspx?Mode=PDF&R_Year=" + GL.CINT(drv["R_Year"]) + "&R_Round=1&PSNL_NO=" + drv["PSNL_NO"].ToString() + "&t=8" + "&Status=" + GL.CINT(drv["KPI_StatusAss_1"]);
			lbl_KPI_Score_1.HRef = "Print/RPT_K_PNSAss.aspx?Mode=PDF&R_Year=" + GL.CINT(drv["R_Year"]) + "&R_Round=1&PSNL_NO=" + drv["PSNL_NO"].ToString() + "&t=8" + "&Status=" + GL.CINT(drv["KPI_StatusAss_1"]);
			img_KPI_Status_2.HRef = "Print/RPT_K_PNSAss.aspx?Mode=PDF&R_Year=" + GL.CINT(drv["R_Year"]) + "&R_Round=2&PSNL_NO=" + drv["PSNL_NO"].ToString() + "&t=8" + "&Status=" + GL.CINT(drv["KPI_StatusAss_2"]);
			lbl_KPI_Score_2.HRef = "Print/RPT_K_PNSAss.aspx?Mode=PDF&R_Year=" + GL.CINT(drv["R_Year"]) + "&R_Round=2&PSNL_NO=" + drv["PSNL_NO"].ToString() + "&t=8" + "&Status=" + GL.CINT(drv["KPI_StatusAss_2"]);
            lbl_Score_1.HRef = "Print/RPT_A_Yearly_PSN.aspx?R_Year=" + GL.CINT(drv["R_Year"]) + "&PSNL_No=" + drv["PSNL_NO"].ToString() + "&Mode=PDF&t=555" + "&Round=1";

			img_COMP_Status_1.HRef = "Print/RPT_C_PNSAss.aspx?Mode=PDF&R_Year=" + GL.CINT(drv["R_Year"]) + "&R_Round=1&PSNL_NO=" + drv["PSNL_NO"].ToString() + "&t=8" + "&Status=" + GL.CINT(drv["COMP_StatusAss_1"]);
			lbl_COMP_Score_1.HRef = "Print/RPT_C_PNSAss.aspx?Mode=PDF&R_Year=" + GL.CINT(drv["R_Year"]) + "&R_Round=1&PSNL_NO=" + drv["PSNL_NO"].ToString() + "&t=8" + "&Status=" + GL.CINT(drv["COMP_StatusAss_1"]);
			img_COMP_Status_2.HRef = "Print/RPT_C_PNSAss.aspx?Mode=PDF&R_Year=" + GL.CINT(drv["R_Year"]) + "&R_Round=2&PSNL_NO=" + drv["PSNL_NO"].ToString() + "&t=8" + "&Status=" + GL.CINT(drv["COMP_StatusAss_2"]);
			lbl_COMP_Score_2.HRef = "Print/RPT_C_PNSAss.aspx?Mode=PDF&R_Year=" + GL.CINT(drv["R_Year"]) + "&R_Round=2&PSNL_NO=" + drv["PSNL_NO"].ToString() + "&t=8" + "&Status=" + GL.CINT(drv["COMP_StatusAss_2"]);
            lbl_Score_2.HRef = "Print/RPT_A_Yearly_PSN.aspx?R_Year=" + GL.CINT(drv["R_Year"]) + "&PSNL_No=" + drv["PSNL_NO"].ToString() + "&Mode=PDF&t=555";

            lbl_Score.HRef = "Print/RPT_A_Yearly_PSN.aspx?R_Year=" + GL.CINT(drv["R_Year"]) + "&PSNL_No=" + drv["PSNL_NO"].ToString() + "&Mode=PDF&t=555";
            lbl_Leave_Day.HRef = "Print/RPT_A_Yearly_PSN.aspx?R_Year=" + GL.CINT(drv["R_Year"]) + "&PSNL_No=" + drv["PSNL_NO"].ToString() + "&Mode=PDF&t=555";
            lbl_Leave_Score.HRef = "Print/RPT_A_Yearly_PSN.aspx?R_Year=" + GL.CINT(drv["R_Year"]) + "&PSNL_No=" + drv["PSNL_NO"].ToString() + "&Mode=PDF&t=555";
            lbl_Year_Result.HRef = "Print/RPT_A_Yearly_PSN.aspx?R_Year=" + GL.CINT(drv["R_Year"]) + "&PSNL_No=" + drv["PSNL_NO"].ToString() + "&Mode=PDF&t=555";



			 //------เพิ่ม STAT------
            lblSTAT.Text = drv["STAT_NAME"].ToString();
            if ( drv["STAT_NAME"].ToString() == "ปกติ") {
                lblSTAT.ForeColor = System.Drawing.Color.Black;
            } else {
                lblSTAT.ForeColor = System.Drawing.Color.Red;
            }

		}
		public RPT_A_Yearly_Result()
		{
			Load += Page_Load;
		}

	}
}

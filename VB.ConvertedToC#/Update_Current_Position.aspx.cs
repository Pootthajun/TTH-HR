﻿using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Xml.Linq;
using System.Diagnostics;
using System.Collections.Specialized;
using System.Configuration;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Caching;
using System.Web.SessionState;
using System.Web.Security;
using System.Web.Profile;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;

namespace VB
{
    public partial class Update_Current_Position : System.Web.UI.Page
    {

        HRBL BL = new HRBL();
        GenericLib GL = new GenericLib();

        public int R_Year
        {
            get
            {
                try
                {
                    return GL.CINT(GL.SplitString(ddlRound.Items[ddlRound.SelectedIndex].Value, "-")[0]);
                }
                catch (Exception ex)
                {
                    return 0;
                }
            }
        }

        public int R_Round
        {
            get
            {
                try
                {
                    return GL.CINT(GL.SplitString(ddlRound.Items[ddlRound.SelectedIndex].Value, "-")[1]);
                }
                catch (Exception ex)
                {
                    return 0;
                }
            }
        }

        public string PSNL_NO
        {
            get
            {
                try
                {
                    return Session["USER_PSNL_NO"].ToString();
                }
                catch (Exception ex)
                {
                    return "";
                }
            }
        }

        public string ASSESSOR_BY
        {
            get
            {
                try
                {
                    string[] tmp = Strings.Split(ddlASSName.Items[ddlASSName.SelectedIndex].Value, ":::");
                    return tmp[0];
                }
                catch (Exception ex)
                {
                    return "";
                }
            }
        }

        public string ASSESSOR_NAME
        {
            get
            {
                try
                {
                    return ddlASSName.Items[ddlASSName.SelectedIndex].Text;
                }
                catch (Exception ex)
                {
                    return "";
                }
            }
        }

        public string ASSESSOR_POS
        {
            get
            {
                try
                {
                    string[] tmp = Strings.Split(ddlASSName.Items[ddlASSName.SelectedIndex].Value, ":::");
                    return tmp[1];
                }
                catch (Exception ex)
                {
                    return "";
                }
            }
        }

        public string ASSESSOR_DEPT
        {
            get
            {
                try
                {
                    string[] tmp = Strings.Split(ddlASSName.Items[ddlASSName.SelectedIndex].Value, ":::");
                    return tmp[2];
                }
                catch (Exception ex)
                {
                    return "";
                }
            }
        }

        string Type = "KPI";

        protected void Page_Load(object sender, System.EventArgs e)
        {
            if (!IsPostBack) {
                BL.BindDDlYearRound(ddlRound);
                BL.BindDDlSector(ddlSector);
                for (int i = 0; i <= ddlSector.Items.Count - 1; i++)
                {
                    if (ddlSector.Items[i].Value == "10")
                    {
                        ddlSector.Items.RemoveAt(i);
                        break; 
                    }
                }

                BindPersonalList();
                BindPersonal_DataOracle();
                
            }

        }

        private void BindPersonal_DataOracle()
        {
            
                string SQL = "";
                SQL = " DECLARE @R_Year AS Int=" + R_Year + "\n";
                SQL += " DECLARE @R_Round As Int=" + R_Round + "\n";
                //SQL += " DECLARE @PSNL_NO As nvarchar(6)='" + PSNL_No.Replace("'", "''") + "'\n";
                SQL += " \n";
                SQL += " SELECT ";
                SQL += "    PSN.PNPS_PSNL_NO PSNL_No,";
                SQL += "    PSN.PNPS_TITLE+PSN.PNPS_PSNL_NAME+' '+PSN.PNPS_PSNL_SURNAME PSNL_Fullname,";
                SQL += "    PSN.PNPS_CLASS PNPS_CLASS,";
                SQL += "    POS.PNPO_TYPE PSNL_TYPE,";
                SQL += "    POS.POS_NO POS_NO,";
                SQL += "    POS.PNPO_TYPE WAGE_TYPE,";
                SQL += "    POS.PNPO_TYPE_NAME WAGE_NAME,";
                //------เพิ่มชื่อ SECTOR_CODE 
                SQL += "    ISNULL(vw_PN.SECTOR_CODE,LEFT(POS.DEPT_CODE,2)) SECTOR_CODE,";
                SQL += "    POS.DEPT_CODE DEPT_CODE,";
                SQL += "    POS.MINOR_Code MINOR_Code,";
                SQL += "    POS.SECTOR_NAME SECTOR_NAME,";
                SQL += "    POS.DEPT_Name DEPT_Name,";
                SQL += "    POS.FN_ID FN_ID,";
                SQL += "    POS.FLD_Name FLD_Name,";
                SQL += "    POS.FLD_CODE FN_CODE,";
                SQL += "    ISNULL(vw_PN.FN_TYPE,'00') FN_TYPE,";
                SQL += "    POS.FN_NAME FN_NAME,";
                SQL += "    POS.MGR_CODE MGR_CODE,";
                SQL += "    POS.MGR_NAME MGR_NAME,";
                SQL += "    ISNULL(CG.CLSGP_ID,0) CLSGP_ID  ";
                SQL += "   ,ISNULL(POS.MGR_NAME,POS.FN_NAME) POS_Name  \n";
                SQL += "    FROM tb_PN_PERSONAL_VIEW PSN";
                SQL += "    LEFT JOIN vw_PN_Position POS ON PSN.PNPS_POSITION_NO=POS.POS_NO";
                SQL += "    LEFT JOIN vw_PN_PSNL vw_PN ON POS.POS_NO=vw_PN.POS_NO";
                SQL += "    LEFT JOIN tb_HR_PNPO_CLASS_GROUP CG ON PSN.PNPS_CLASS BETWEEN CG.PNPO_CLASS_Start AND CG.PNPO_CLASS_End";
                SQL += "                                   AND CG.R_Year=@R_Year AND CG.R_Round=@R_Round";

                SqlDataAdapter DA = new SqlDataAdapter(SQL, BL.ConnectionString());
                DA.SelectCommand.CommandTimeout = 200;
                DataTable DT = new DataTable();
                DA.Fill(DT);

                Session["BindPersonal_DataOracle"] = DT;
        
        }


        private void BindPersonalList()
        {
            string SQL = "";
             SQL += " SELECT DISTINCT vw.R_Year,vw.R_Round,vw.SECTOR_CODE,vw.SECTOR_NAME,vw.DEPT_CODE,vw.DEPT_NAME,vw.PSNL_NO,vw.PSNL_Fullname,vw.PNPS_CLASS,vw.WAGE_NAME \n";
             SQL += " ,vw.POS_NO,vw.POS_Name,vw." + Type + "_Status,vw." + Type + "_Status_Name \n";
             SQL += " FROM vw_RPT_" + Type + "_Status vw \n";
             SQL += " LEFT JOIN vw_PN_PSNL_ALL PSN ON vw.PSNL_NO=PSN.PSNL_NO   \n";
             SQL += " WHERE (ISNULL(PNPS_RESIGN_DATE,PNPS_RETIRE_DATE) > (SELECT MAX(R_End) FROM tb_HR_Round WHERE  R_Year=" + R_Year + " AND R_Round=" + R_Round + ")) \n"; 
             SQL += " ORDER BY SECTOR_CODE,DEPT_CODE,PNPS_CLASS DESC,POS_NO \n";

            string filter = "SECTOR_CODE<>'10' AND R_Year=" + R_Year + " AND R_Round=" + R_Round + "\n";

             if (ddlSector.SelectedIndex > 0)
             {
                 filter += " AND (SECTOR_CODE='" + ddlSector.Items[ddlSector.SelectedIndex].Value + "' \n";
                 if (ddlSector.Items[ddlSector.SelectedIndex].Value == "3200")
                 {
                     filter += " AND DEPT_CODE NOT IN ('32000300'))\n ";
                     //-----------ฝ่ายโรงงานผลิตยาสูบ 3
                 }
                 else if (ddlSector.Items[ddlSector.SelectedIndex].Value == "3203")
                 {
                     filter += " OR DEPT_CODE IN ('32000300','32030000','32030100','32030200','32030300','32030500'))\n";
                     //-----------ฝ่ายโรงงานผลิตยาสูบ 4
                 }
                 else if (ddlSector.Items[ddlSector.SelectedIndex].Value == "3204")
                 {
                     filter += " OR DEPT_CODE IN ('32040000','32040100','32040200','32040300','32040500','32040300'))\n";
                     //-----------ฝ่ายโรงงานผลิตยาสูบ 5
                 }
                 else if (ddlSector.Items[ddlSector.SelectedIndex].Value == "3205")
                 {
                     filter += " OR DEPT_CODE IN ('32050000','32050100','32050200','32050300','32050500'))\t";
                 }
                 else
                 {
                     filter += ")\n";
                 }
             }

            SqlDataAdapter DA = new SqlDataAdapter(SQL, BL.ConnectionString());
            DA.SelectCommand.CommandTimeout = 200;
            DataTable DT = new DataTable();
            DA.Fill(DT);
            DT.DefaultView.RowFilter = filter;
            
            //-------พนักงานทั้งหมดที่ต้อง update ในรอบนั้นๆ-------------
            Session["Update_Position"] = DT.DefaultView.ToTable();

            if (DT.DefaultView.Count == 0)
            {
                lblCountPSN.Text = "ไม่พบรายการดังกล่าว";
            }
            else
            {
                lblCountPSN.Text = "พบ " + GL.StringFormatNumber(DT.DefaultView.Count, 0) + " รายการ";
            }
        }

      
        protected void btnOK_Click(object sender, EventArgs e)
        {
            
            UpdateHeader();
        }



            private void UpdateHeader()
            {
                int CountSuccess = 0;
                DataTable DT_Temp = new DataTable();
                DT_Temp = (DataTable)Session["Update_Position"];
                if ( DT_Temp.Rows.Count > 0)
                {
                    for (int i = 0; i <= DT_Temp.Rows.Count - 1; i++)
                    {
                        string PSNL_NO = DT_Temp.Rows[i]["PSNL_NO"].ToString();

                        int _Status = 0;

                        switch (ddlType.SelectedValue.ToString())
                        {
                            case "KPI":
                                _Status = BL.GetKPIStatus(R_Year, R_Round, PSNL_NO);
                                break;

                            case "COMP":
                                _Status = BL.GetCOMPStatus(R_Year, R_Round, PSNL_NO);
                                break;
                        }

                        string SQL = "";
                        SQL = " SELECT * \n";
                        SQL += " FROM tb_HR_" + Type + "_Header\n";
                        SQL += " WHERE PSNL_NO='" + PSNL_NO.Replace("'", "''") + "' AND R_Year=" + R_Year + " AND R_Round=" + R_Round;
                        SqlDataAdapter DA = new SqlDataAdapter(SQL, BL.ConnectionString());
                        DataTable DT = new DataTable();
                        DA.Fill(DT);

                        DataRow DR = null;
                        if (DT.Rows.Count == 0)
                        {
                            DR = DT.NewRow();
                            DR["PSNL_NO"] = PSNL_NO;
                            DR["R_Year"] = R_Year;
                            DR["R_Round"] = R_Round;
                            DR["Create_By"] = Session["USER_PSNL_NO"];
                            DR["Create_Time"] = DateAndTime.Now;
                        }
                        else
                        {
                            DR = DT.Rows[0];
                        }

                        //------------- Round Detail------------- 
                        SQL = "SELECT * FROM tb_HR_Round ";
                        SQL += " WHERE R_Year=" + R_Year + " AND R_Round=" + R_Round;
                        DataTable PN = new DataTable();
                        SqlDataAdapter PA = new SqlDataAdapter(SQL, BL.ConnectionString());
                        PA.Fill(PN);
                        DR["R_Start"] = PN.Rows[0]["R_Start"];
                        DR["R_End"] = PN.Rows[0]["R_End"];
                        DR["R_Remark"] = "";

                        //============ป้องกันการ update ตำแหน่ง ณ สิ้นรอบประเมินและปิดรอบประเมิน======

                        //Boolean IsInPeriod = BL.IsTimeInPeriod(DateTime.Now, R_Year, R_Round);
                        //Boolean IsRoundCompleted = BL.Is_Round_Completed(R_Year, R_Round);
                        //Boolean ckUpdate = IsInPeriod & !IsRoundCompleted;
                        //if (ckUpdate | DT.Rows.Count == 0)
                        //{
                            //------------- Personal Detail---------



                        DataTable DT_PSNInfo =(DataTable) Session["BindPersonal_DataOracle"];
                        DT_PSNInfo.DefaultView.RowFilter = "PSNL_NO ='" + PSNL_NO + "'";

                        if (DT_PSNInfo.DefaultView.Count > 0)
                        {






                            //HRBL.PersonalInfo PSNInfo = BL.GetAssessmentPersonalInfo(R_Year, R_Round, PSNL_NO, _Status);


                            if (!string.IsNullOrEmpty(DT_PSNInfo.DefaultView[0]["PSNL_NO"].ToString()))
                            {
                                if (!string.IsNullOrEmpty(DT_PSNInfo.DefaultView[0]["PSNL_No"].ToString ()))
                                    DR["PSNL_No"] = DT_PSNInfo.DefaultView[0]["PSNL_No"].ToString ();
                                else
                                    DR["PSNL_No"] = DBNull.Value;
                                if (!string.IsNullOrEmpty(DT_PSNInfo.DefaultView[0]["PSNL_Fullname"].ToString ()))
                                    DR["PSNL_Fullname"] = DT_PSNInfo.DefaultView[0]["PSNL_Fullname"].ToString ();
                                else
                                    DR["PSNL_Fullname"] = DBNull.Value;
                                if (!string.IsNullOrEmpty(DT_PSNInfo.DefaultView[0]["PNPS_CLASS"].ToString ()))
                                    DR["PNPS_CLASS"] = DT_PSNInfo.DefaultView[0]["PNPS_CLASS"].ToString ();
                                else
                                    DR["PNPS_CLASS"] = DBNull.Value;
                                if (!string.IsNullOrEmpty(DT_PSNInfo.DefaultView[0]["PSNL_TYPE"].ToString ()))
                                    DR["PSNL_TYPE"] = DT_PSNInfo.DefaultView[0]["PSNL_TYPE"].ToString ();
                                else
                                    DR["PSNL_TYPE"] = DBNull.Value;
                                if (!string.IsNullOrEmpty(DT_PSNInfo.DefaultView[0]["POS_NO"].ToString ()))
                                    DR["POS_NO"] = DT_PSNInfo.DefaultView[0]["POS_NO"].ToString ();
                                else
                                    DR["POS_NO"] = DBNull.Value;
                                if (!string.IsNullOrEmpty(DT_PSNInfo.DefaultView[0]["WAGE_TYPE"].ToString ()))
                                    DR["WAGE_TYPE"] = DT_PSNInfo.DefaultView[0]["WAGE_TYPE"].ToString ();
                                else
                                    DR["WAGE_TYPE"] = DBNull.Value;
                                if (!string.IsNullOrEmpty(DT_PSNInfo.DefaultView[0]["WAGE_NAME"].ToString ()))
                                    DR["WAGE_NAME"] = DT_PSNInfo.DefaultView[0]["WAGE_NAME"].ToString ();
                                else
                                    DR["WAGE_NAME"] = DBNull.Value;
                                if (!string.IsNullOrEmpty(DT_PSNInfo.DefaultView[0]["SECTOR_CODE"].ToString ()))
                                    DR["SECTOR_CODE"] = Strings.Left(DT_PSNInfo.DefaultView[0]["SECTOR_CODE"].ToString (), 2);
                                else
                                    DR["SECTOR_CODE"] = DBNull.Value;
                                if (!string.IsNullOrEmpty(DT_PSNInfo.DefaultView[0]["DEPT_CODE"].ToString ()))
                                    DR["DEPT_CODE"] = DT_PSNInfo.DefaultView[0]["DEPT_CODE"].ToString ();
                                else
                                    DR["DEPT_CODE"] = DBNull.Value;
                                if (!string.IsNullOrEmpty(DT_PSNInfo.DefaultView[0]["MINOR_CODE"].ToString ()))
                                    DR["MINOR_CODE"] = DT_PSNInfo.DefaultView[0]["MINOR_CODE"].ToString ();
                                else
                                    DR["MINOR_CODE"] = DBNull.Value;
                                if (!string.IsNullOrEmpty(DT_PSNInfo.DefaultView[0]["SECTOR_NAME"].ToString ()))
                                    DR["SECTOR_NAME"] = DT_PSNInfo.DefaultView[0]["SECTOR_NAME"].ToString ();
                                else
                                    DR["SECTOR_NAME"] = DBNull.Value;
                                if (!string.IsNullOrEmpty(DT_PSNInfo.DefaultView[0]["DEPT_NAME"].ToString ()))
                                    DR["DEPT_NAME"] = DT_PSNInfo.DefaultView[0]["DEPT_NAME"].ToString ();
                                else
                                    DR["DEPT_NAME"] = DBNull.Value;
                                if (!string.IsNullOrEmpty(DT_PSNInfo.DefaultView[0]["FN_ID"].ToString ()))
                                    DR["FN_ID"] = DT_PSNInfo.DefaultView[0]["FN_ID"].ToString ();
                                else
                                    DR["FN_ID"] = DBNull.Value;
                                if (!string.IsNullOrEmpty(DT_PSNInfo.DefaultView[0]["FLD_Name"].ToString ()))
                                    DR["FLD_Name"] = DT_PSNInfo.DefaultView[0]["FLD_Name"].ToString ();
                                else
                                    DR["FLD_Name"] = DBNull.Value;
                                if (!string.IsNullOrEmpty(DT_PSNInfo.DefaultView[0]["FN_CODE"].ToString ()))
                                    DR["FN_CODE"] = DT_PSNInfo.DefaultView[0]["FN_CODE"].ToString ();
                                else
                                    DR["FN_CODE"] = DBNull.Value;
                                if (!string.IsNullOrEmpty(DT_PSNInfo.DefaultView[0]["FN_NAME"].ToString ()))
                                    DR["FN_NAME"] = DT_PSNInfo.DefaultView[0]["FN_NAME"].ToString ();
                                else
                                    DR["FN_NAME"] = DBNull.Value;
                                if (!string.IsNullOrEmpty(DT_PSNInfo.DefaultView[0]["MGR_CODE"].ToString ()))
                                    DR["MGR_CODE"] = DT_PSNInfo.DefaultView[0]["MGR_CODE"].ToString ();
                                else
                                    DR["MGR_CODE"] = DBNull.Value;
                                if (!string.IsNullOrEmpty(DT_PSNInfo.DefaultView[0]["MGR_NAME"].ToString ()))
                                    DR["MGR_NAME"] = DT_PSNInfo.DefaultView[0]["MGR_NAME"].ToString();
                                else
                                    DR["MGR_NAME"] = DBNull.Value;
                            }
                        }
                            BindAssessor( PSNL_NO);
                            //------------- Assign Assessor--------- Old -----------------------------
                            DR["Create_Commit_By"] = ASSESSOR_BY;
                            DR["Create_Commit_Name"] = ASSESSOR_NAME;
                            DR["Create_Commit_DEPT"] = ASSESSOR_DEPT;
                            DR["Create_Commit_POS"] = ASSESSOR_POS;
                            DR["Assessment_Commit_By_MGR"] = ASSESSOR_BY;
                            DR["Assessment_Commit_Name"] = ASSESSOR_NAME;
                            DR["Assessment_Commit_DEPT"] = ASSESSOR_DEPT;
                            DR["Assessment_Commit_POS"] = ASSESSOR_POS;
                        //}
                        DR["Update_By"] = Session["USER_PSNL_NO"];
                        DR["Update_Time"] = DateAndTime.Now;

                        if (DT.Rows.Count == 0)
                            DT.Rows.Add(DR);
                        SqlCommandBuilder cmd = new SqlCommandBuilder(DA);
                        DA.Update(DT);

                        DataTable TMP = BL.GetAssessorList(R_Year, R_Round, PSNL_NO);
                        TMP.DefaultView.RowFilter = "MGR_PSNL_NO='" + ASSESSOR_BY.Replace("'", "''") + "'";

                        //----------------- Update Information For History Assessor Structure ------------------
                        SQL = "SELECT * ";
                        SQL += " FROM tb_HR_Actual_Assessor ";
                        SQL += " WHERE PSNL_NO='" + PSNL_NO + "' AND R_Year=" + R_Year + " AND R_Round=" + R_Round;
                        DA = new SqlDataAdapter(SQL, BL.ConnectionString());
                        DT = new DataTable();
                        DA.Fill(DT);

                        if (DT.Rows.Count > 0)
                        {
                            DT.Rows[0].Delete();
                            cmd = new SqlCommandBuilder(DA);
                            DA.Update(DT);
                        }

                        if (TMP.DefaultView.Count > 0)
                        {
                            DR = DT.NewRow();
                            DR["PSNL_NO"] = PSNL_NO;
                            DR["R_Year"] = R_Year;
                            DR["R_Round"] = R_Round;
                            DR["MGR_ASSESSOR_POS"] = TMP.DefaultView[0]["ASSESSOR_POS"];
                            DR["MGR_PSNL_NO"] = TMP.DefaultView[0]["MGR_PSNL_NO"];
                            DR["MGR_PSNL_Fullname"] = TMP.DefaultView[0]["MGR_PSNL_Fullname"];
                            DR["MGR_PNPS_CLASS"] = TMP.DefaultView[0]["MGR_CLASS"];
                            DR["MGR_PSNL_TYPE"] = TMP.DefaultView[0]["MGR_WAGE_TYPE"];
                            DR["MGR_POS_NO"] = TMP.DefaultView[0]["MGR_POS_NO"];
                            DR["MGR_WAGE_TYPE"] = TMP.DefaultView[0]["MGR_WAGE_TYPE"];
                            DR["MGR_WAGE_NAME"] = TMP.DefaultView[0]["MGR_WAGE_NAME"];
                            DR["MGR_DEPT_CODE"] = TMP.DefaultView[0]["MGR_DEPT_CODE"];
                            DR["MGR_SECTOR_CODE"] = Strings.Left(TMP.DefaultView[0]["MGR_SECTOR_CODE"].ToString(), 2);
                            DR["MGR_SECTOR_NAME"] = TMP.DefaultView[0]["MGR_SECTOR_NAME"];
                            DR["MGR_DEPT_NAME"] = TMP.DefaultView[0]["MGR_DEPT_NAME"];
                            DR["MGR_FN_CODE"] = TMP.DefaultView[0]["MGR_FN_CODE"];
                            DR["MGR_FN_NAME"] = TMP.DefaultView[0]["MGR_FN_NAME"];
                            if (!GL.IsEqualNull(TMP.DefaultView[0]["MGR_MGR_CODE"]))
                            {
                                DR["MGR_MGR_CODE"] = TMP.DefaultView[0]["MGR_MGR_CODE"];
                            }
                            if (!GL.IsEqualNull(TMP.DefaultView[0]["MGR_MGR_NAME"]))
                            {
                                DR["MGR_MGR_NAME"] = TMP.DefaultView[0]["MGR_MGR_NAME"];
                            }
                            try
                            {
                                DR["MGR_PNPS_RETIRE_DATE"] = (BL.GetPSNRetireDate(TMP.DefaultView[0]["MGR_PSNL_NO"].ToString()));
                            }
                            catch
                            {
                            }
                            DR["Update_By"] = Session["USER_PSNL_NO"];
                            DR["Update_Time"] = DateAndTime.Now;
                            DT.Rows.Add(DR);
                            cmd = new SqlCommandBuilder(DA);
                            DA.Update(DT);
                        }

                        //----------------- Update Information For History PSN Structure ------------------
                        DataTable PSN = new DataTable();
                        SQL = "SELECT * FROM vw_PN_PSNL WHERE PSNL_NO='" + PSNL_NO.Replace("'", "''") + "'";
                        DA = new SqlDataAdapter(SQL, BL.ConnectionString());
                        DA.Fill(PSN);

                        SQL = "SELECT * ";
                        SQL += " FROM tb_HR_Assessment_PSN ";
                        SQL += " WHERE PSNL_NO='" + PSNL_NO.Replace("'", "''") + "' AND R_Year=" + R_Year + " AND R_Round=" + R_Round;
                        DA = new SqlDataAdapter(SQL, BL.ConnectionString());
                        DT = new DataTable();
                        DA.Fill(DT);


                        if (PSN.Rows.Count > 0)
                        {
                            if (DT.Rows.Count == 0)
                            {
                                DR = DT.NewRow();
                                DR["PSNL_NO"] = PSNL_NO;
                                DR["R_Year"] = R_Year;
                                DR["R_Round"] = R_Round;
                                if (TMP.DefaultView.Count > 0)
                                {
                                    try
                                    {
                                        DR["PSN_ASSESSOR_POS"] = TMP.DefaultView[0]["ASSESSOR_POS"];
                                    }
                                    catch
                                    {
                                    }
                                }
                                DR["PSN_PSNL_NO"] = PSNL_NO;
                                DR["PSN_PSNL_Fullname"] = PSN.Rows[0]["PSNL_Fullname"].ToString();
                                DT.Rows.Add(DR);
                            }
                            else
                            {
                                DR = DT.Rows[0];
                            }

                            //============ป้องกันการ update ตำแหน่ง ณ สิ้นรอบประเมินและปิดรอบประเมิน======
                            //if ( DT.Rows.Count == 0 | string.IsNullOrEmpty(DT.Rows[0]["PSN_PNPS_CLASS"].ToString()))
                            //{
                                DR["PSN_PNPS_CLASS"] = PSN.Rows[0]["PNPS_CLASS"].ToString();
                                DR["PSN_PSNL_TYPE"] = PSN.Rows[0]["PSNL_TYPE"].ToString();
                                DR["PSN_POS_NO"] = PSN.Rows[0]["POS_NO"].ToString();
                                DR["PSN_WAGE_TYPE"] = PSN.Rows[0]["WAGE_TYPE"].ToString();
                                DR["PSN_WAGE_NAME"] = PSN.Rows[0]["WAGE_NAME"].ToString();
                                DR["PSN_DEPT_CODE"] = PSN.Rows[0]["DEPT_CODE"].ToString();
                                DR["PSN_MINOR_CODE"] = PSN.Rows[0]["MINOR_CODE"].ToString();
                                DR["PSN_SECTOR_CODE"] = Strings.Left(PSN.Rows[0]["SECTOR_CODE"].ToString(), 2);
                                DR["PSN_SECTOR_NAME"] = PSN.Rows[0]["SECTOR_NAME"].ToString();
                                DR["PSN_DEPT_NAME"] = PSN.Rows[0]["DEPT_NAME"].ToString();
                                DR["PSN_FN_ID"] = PSN.Rows[0]["FN_ID"].ToString();
                                DR["PSN_FLD_Name"] = PSN.Rows[0]["FLD_Name"].ToString();
                                DR["PSN_FN_CODE"] = PSN.Rows[0]["FN_CODE"].ToString();
                                DR["PSN_FN_TYPE"] = PSN.Rows[0]["FN_TYPE"].ToString();
                                DR["PSN_FN_NAME"] = PSN.Rows[0]["FN_NAME"].ToString();
                                if (!GL.IsEqualNull(PSN.Rows[0]["MGR_CODE"]))
                                {
                                    DR["PSN_MGR_CODE"] = PSN.Rows[0]["MGR_CODE"];
                                }
                                if (!GL.IsEqualNull(PSN.Rows[0]["MGR_NAME"]))
                                {
                                    DR["PSN_MGR_NAME"] = PSN.Rows[0]["MGR_NAME"];
                                }
                            //}

                            try
                            {
                                DR["PSN_PNPS_RETIRE_DATE"] = BL.GetPSNRetireDate(PSNL_NO);
                            }
                            catch
                            {
                            }
                            DR["Update_By"] = Session["USER_PSNL_NO"];
                            DR["Update_Time"] = DateAndTime.Now;
                            cmd = new SqlCommandBuilder(DA);
                            DA.Update(DT);

                        }



                        CountSuccess = CountSuccess + 1;
                    }

                    lblCountSuccess.Text = CountSuccess.ToString ();
                }

            
            
            }





            private void BindAssessor(string PSNL_NO)
            {
                BL.BindDDLAssessor(ddlASSName, R_Year, R_Round, PSNL_NO);
                
            }

            protected void ddlRound_SelectedIndexChanged(object sender, EventArgs e)
            {
                switch (ddlType.SelectedValue.ToString())
                {
                    case "KPI":
                        Type = "KPI";
                        break;

                    case "COMP":
                        Type = "COMP";
                        break;
                }

                BindPersonalList();
            }

            protected void ddlSector_SelectedIndexChanged(object sender, EventArgs e)
            {
                BindPersonalList();
            }
    }
}

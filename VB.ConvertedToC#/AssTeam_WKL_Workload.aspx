﻿<%@ Page  Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPage.Master" CodeBehind="AssTeam_WKL_Workload.aspx.cs" Inherits="VB.AssTeam_WKL_Workload" %>
<%@ Register src="WUC_DateReporter.ascx" tagname="WUC_DateReporter" tagprefix="uc1" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

	<!-- BEGIN PAGE LEVEL STYLES -->
	<link href="assets/plugins/glyphicons/css/glyphicons.css" rel="stylesheet" />
	<link href="assets/plugins/glyphicons_halflings/css/halflings.css" rel="stylesheet" />
	<!-- END PAGE LEVEL STYLES -->
	
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<asp:ToolkitScriptManager ID="toolkit1" runat="server"></asp:ToolkitScriptManager>

<asp:UpdatePanel ID="UpdatePanel1" runat="server">
<ContentTemplate>


<div class="container-fluid">
				<!-- BEGIN PAGE HEADER-->
				<div class="row-fluid" id="fixedPageHeader">
					<div class="span12"> 
						<!-- BEGIN PAGE TITLE & BREADCRUMB-->
						<h3 class="page-title">ตารางภารกิจงาน (Workload) <font color="blue">(ScreenID : S-MGR-08)</font></h3>
						
									
						<ul class="breadcrumb">
                            <li>
                            	<i class="icon-dashboard"></i><a href="javascript:;">การบริหารอัตรากำลัง (Workload)</a><i class="icon-angle-right"></i>
                            </li>
                        	<li>
                        	    <i class="icon-dashboard"></i> <a href="javascript:;">ตารางภารกิจงาน (Workload)</a>
                        	</li>
                        	
                        	
                            <uc1:WUC_DateReporter ID="WUC_DateReporter1" runat="server" />
                        </ul>
						<!-- END PAGE TITLE & BREADCRUMB-->
									        
				     </div>

				
			    </div>
		        <!-- END PAGE HEADER-->
				
			<div class="portlet-body form">
											
					<!-- BEGIN HEADER-->
					<div class="form-horizontal form-view">  
                    
                        <div class="btn-group pull-right">                                    
			                <asp:LinkButton ID="btnExcel" runat="server" OnClick="btnExcel_click" CssClass="btn green">ส่งออกเป็น Excel <i class="icon-upload-alt"></i></asp:LinkButton>
		                </div>
                                           
						<h3 class="form-section"><i class="icon-dashboard"></i> สำหรับ
						
						<asp:DropDownList ID="ddlOrganize" OnSelectedIndexChanged="ddlOrganize_SelectedIndexChanged" runat="server" AutoPostBack="true" CssClass="m-wrap" style="font-size:20px; width:auto !important; padding:0px;">
						</asp:DropDownList>
						    
						</h3>
					</div>
					
					 <!-- BEGIN FORM-->
					 <asp:Panel ID="pnlEdit" runat="Server" DefaultButton="btnCalculate">
					 					 
				                <div class="portlet-body no-more-tables">
				              
				                        <table class="table table-bordered" style="background-color:White;">
									        <thead>
										        <tr>
											        <th rowspan="2" style="text-align:center; background-color:#CCCCCC; padding:0px !important; width:30px; font-size:13px;"> ลำดับ</th>
											        <th rowspan="2" style="text-align:center; background-color:#CCCCCC; padding:0px !important; font-size:13px;"> งาน</th>
											        <th rowspan="2"style="text-align:center; background-color:#CCCCCC; padding:0px !important; font-size:13px; width:auto; min-width:100px;" >จำนวน<br>ผู้ปฏิบัติ</th>
											        <th rowspan="2"style="text-align:center; background-color:#CCCCCC; padding:0px !important; font-size:13px; width:auto; min-width:100px;">จำนวน<br>นาทีที่ใช้<br>ในการ<br>ปฏิบัติ<br>ต่อครั้ง</th>
											        <th rowspan="2"style="text-align:center; background-color:#CCCCCC; padding:0px !important; font-size:13px; width:auto; min-width:60px;">รวม</th>
											        <th rowspan="2"style="text-align:center; background-color:#CCCCCC; padding:0px !important; font-size:13px; width:auto; min-width:80px;">จำนวนครั้ง<br>(ความถี่)<br>ที่ใช้ใน<br>การปฏิบัติ<br>ใน 1 ปี</th>
											        <th rowspan="2"style="text-align:center; background-color:#CCCCCC; padding:0px !important; font-size:13px; width:auto; min-width:80px;">จำนวน<br>นาทีที่ใช้<br>ใน 1 ปี
											            <br>
											            <img src="images/arrow_blue_left.png" /><img src="images/sum.png" />
											        </th>
											        <th rowspan="2"style="text-align:center; background-color:#CCCCCC; padding:0px !important; font-size:13px; width:auto; min-width:80px;">ผลรวมเวลา<br>ที่ใช้ในการปฏิบัติ
											            <br>
											            <img src="images/sum.png" /><img src="images/arrow_blue_right.png" />
											        </th>
											        <asp:Repeater ID="rptClass" OnItemCommand="rptClass_ItemCommand" OnItemDataBound="rptClass_ItemDataBound" runat="server">
											            <ItemTemplate>
											                <th id="thClass" runat="server" style="text-align:center; background-color:white; padding:0px !important; font-size:13px; min-width:150px; padding:5px;">
											                   <div class="btn-group" style="text-align:left; cursor:pointer;" >
											                    <a id="lnkClassName" href="javascript:;" runat="Server" class="dropdown-toggle" style="width:100% !important; font-size:13px; font-weight:bold; border:0px; text-decoration:none; color:Black;" data-toggle="dropdown">xxxxxxxxx <i class='icon-angle-down'></i></a>
										                        <ul class="dropdown-menu pull-left">
										                          <li><a id="btnAddPSN" href="javascript:;" runat="server" style="cursor:pointer; text-decoration:none;"><i class="icon-user"></i> เพิ่มผู้ปฏิบัติ </a></li>
										                          <li><asp:LinkButton id="btnDefault" CommandName="Default" runat="server" style="cursor:pointer; text-decoration:none;"><i class="icon-download"></i> ปรับจำนวนผู้ปฏิบัติตามฐานข้อมูลจริง &nbsp;</asp:LinkButton></li>										                  
									                            </ul>
									                            <asp:ConfirmButtonExtender TargetControlID="btnDefault" ID="cfmbtnDefault" ConfirmText="ยืนยันปรับโครงสร้าง ?" runat="server"></asp:ConfirmButtonExtender>
									                          </div>
											                </th>
											            </ItemTemplate>
											        </asp:Repeater>
											        
									            </tr>      
									            
									            <tr>
											        <asp:Repeater ID="rptPSN" OnItemCommand="rptPSN_ItemCommand" OnItemDataBound="rptPSN_ItemDataBound" runat="server">
											            <ItemTemplate>
											                <th style="text-align:center; background-color:#e3ebf0; padding:0px !important; font-size:12px; font-weight:normal;">
									                          <div id="btnGroup" runat="server" class="btn-group" style="text-align:left; cursor:pointer; margin:0px 10px 0px 10px ; ">
										                        <a id="lnkPSNName" runat="server" href="javascript:;" class="dropdown-toggle" style="width:100% !important; background-color:#e3ebf0; font-size:12px; font-weight:normal; text-decoration:none; border:0px;" data-toggle="dropdown">xxxxxxxxx <i class="icon-angle-down"></i></a>
										                        <ul class="dropdown-menu pull-right">
										                          <li><a href="javascript:;" id="btnRename" runat="server"><i class="icon-edit"></i> เปลี่ยนชื่อ</a></li>
										                          <li><asp:LinkButton id="btnDelete" runat="server" CommandName="Delete"><i class="icon-trash"></i> ลบ</asp:LinkButton>
										                            <asp:ConfirmButtonExtender TargetControlID="btnDelete" ID="cfmBtnDelete" ConfirmText="ยืนยันลบเจ้าหน้าที่รายการนี้" runat="server"></asp:ConfirmButtonExtender>
										                          </li>										                  
									                            </ul>
									                          </div>
									                        </th>
									                    </ItemTemplate>
											        </asp:Repeater>									                
									            </tr>                                         
									        </thead>
									        <tbody>
									            <asp:Repeater ID="rptJob" OnItemDataBound="rptJob_ItemDataBound" runat="server">
									                <ItemTemplate>
									                   
									                        <tr>
									                          <td style="width:40px; text-align:center; padding-left:0px; padding-right:0px; "><asp:Label ID="lblNo" runat="server" Font-Bold="true" ></asp:Label></td>
									                          <td style="min-width:150px;" id="tdTask" runat="server"><asp:Label ID="lblTask" runat="server" style="min-width:150px;" ></asp:Label></td>
									                          <td class="WorkLoadTableCell" id="cellNumPerson" runat="server"><asp:TextBox id="txtNumPerson" runat="server" CssClass="WorkLoadTableTextbox" style="min-width:20px;" MaxLength="3"></asp:TextBox></td>
                                                              <td class="WorkLoadTableCell" id="cellMinPerTime" runat="server"><asp:TextBox id="txtMinPerTime" runat="server" CssClass="WorkLoadTableTextbox" style="min-width:30px;" MaxLength="5"></asp:TextBox></td>
                                                              <td class="WorkLoadTableCell" style="background-color:#eeeeee; color:#555555; text-align:right; padding-top:4px !important;"><asp:Label id="lblPersonMin" Font-Size="14px" runat="server" style="margin:6px 8px 6px 8px; font-weight:600;" ></asp:Label></td>
                                                              <td class="WorkLoadTableCell" id="cellTimePerYear" runat="server" style="" ><asp:TextBox id="txtTimePerYear" runat="server" CssClass="WorkLoadTableTextbox" style="min-width:20px;" MaxLength="3"></asp:TextBox></td>
                                                              <td class="WorkLoadTableCell" style="background-color:#eeeeee; color:#555555; text-align:right; padding-top:4px !important;" id="cellTotalMin" runat="server" ><asp:Label id="lblTotalMin" Font-Size="14px" style="margin:6px 8px 6px 8px; font-weight:600;" runat="server" ></asp:Label></td>
                                                              <td class="WorkLoadTableCell" style="background-color:#eeeeee; color:#555555; text-align:right; padding-top:4px !important;" id="cellSumMin" runat="server"><asp:Label id="lblSumMin" Font-Size="14px" style="margin:6px 8px 6px 8px; font-weight:600;" runat="server" ></asp:Label></td>
                                                              <asp:Repeater ID="rptSlot" runat="server">
                                                                  <ItemTemplate>
                                                                        <td class="WorkLoadTableCell" id="cellSlot" runat="server"><asp:TextBox id="txtSlot" runat="server" CssClass="WorkLoadTableTextbox" MaxLength="6"></asp:TextBox></td>
                                                                  </ItemTemplate>
                                                              </asp:Repeater>
									                        </tr>
									                    
									                </ItemTemplate>
									                <FooterTemplate>
									                 <tr>
									                      <td>&nbsp;</td>
									                      <td style="text-align:right; font-weight:bold; background-color:#F8F8F8 !important;" colspan="5">จำนวนนาทีที่ใช้ในการปฏิบัติ</td>
									                      <td class="WorkLoadSummaryCell"><asp:Label ID="lblTotalMinPerYear" runat="server"></asp:Label></td>
									                      <td class="WorkLoadSummaryCell"><asp:Label ID="lblSumMinPerYear" runat="server"></asp:Label></td>
									                      <asp:Repeater ID="rptMin" runat="server">
									                        <ItemTemplate>
									                            <td class="WorkLoadSummaryCell"><asp:Label ID="lblMin" runat="server"></asp:Label></td>
									                        </ItemTemplate>
									                      </asp:Repeater>
									                    </tr>
									                    
									                    <tr>
									                      <td>&nbsp;</td>
									                      <td style="text-align:right; font-weight:bold; background-color:#F8F8F8 !important;" colspan="5">คิดเป็นจำนวนชั่วโมง</td>
									                      <td class="WorkLoadSummaryCell"><asp:Label ID="lblTotalHourPerYear" runat="server"></asp:Label></td>
									                      <td class="WorkLoadSummaryCell"><asp:Label ID="lblSumHourPerYear" runat="server"></asp:Label></td>
									                      <asp:Repeater ID="rptHour" runat="server">
									                        <ItemTemplate>
									                            <td class="WorkLoadSummaryCell"><asp:Label ID="lblHour" runat="server"></asp:Label></td>
									                        </ItemTemplate>
									                      </asp:Repeater>
									                    </tr>
									                    
									                    <tr>
									                      <td>&nbsp;</td>
									                      <td style="text-align:right; font-weight:bold; background-color:#F8F8F8 !important;" colspan="5">FTE</td>
									                      <td class="WorkLoadSummaryCell"><asp:Label ID="lblFTEYear" runat="server"></asp:Label></td>
									                      <td class="WorkLoadSummaryCell"><asp:Label ID="lblFTESum" runat="server"></asp:Label></td>
									                      <asp:Repeater ID="rptFTE" runat="server">
									                        <ItemTemplate>
									                             <td class="WorkLoadSummaryCell"><asp:Label ID="lblFTE" runat="server"></asp:Label></td>
									                        </ItemTemplate>
									                      </asp:Repeater>
									                    </tr>
									                </FooterTemplate>
									            </asp:Repeater>
									            									   	       							
									        </tbody>
								        </table>
								        
						                <div class="form-actions">
										    
										    <asp:LinkButton CssClass="btn red" runat="server" ID="btnClear" OnClick="btnClear_Click" Text="ลบข้อมูลเวลาทั้งหมด" />
										    <asp:LinkButton CssClass="btn blue" runat="server" ID="btnDefault" OnClick="btnDefault_Click" Text="ปรับโครงสร้างผู้ปฏิบัติให้เป็นปัจจุบัน" />
										    <asp:ConfirmButtonExtender ID="dfmClear" runat="server" ConfirmText="ยืนยันลบตารางทั้งหมด ?" TargetControlID="btnClear"></asp:ConfirmButtonExtender>
										    <asp:ConfirmButtonExtender ID="cfmDefault" runat="server" ConfirmText="ยืนยันปรับโครงสร้าง ?" TargetControlID="btnDefault"></asp:ConfirmButtonExtender>
										    
										    
										    
										</div>
				               
							</div>
				    </asp:Panel>
				    <!-- END FORM-->  
				</div>
            
         <asp:Panel CssClass="modal" DefaultButton="btnOK" id="divModal" runat="server" style="width:400px; top:20%; visibility:hidden;" >
			<div class="modal-header">
				<h3><asp:Label ID="lblHeader" runat="server"></asp:Label></h3>
			</div>
			<div class="modal-body" style="padding:0px; overflow:hidden;">
			<asp:TextBox ID="txtDialog" MaxLength="255" runat="server" Rows="4" CssClass="large m-wrap" style="width:100% !important; height:100%; margin:0px;"></asp:TextBox>			        
			
			</div>
			<div class="modal-footer">
			    <asp:LinkButton ID="btnOK" OnClick="btnOK_Click" runat="server" CssClass="btn blue" Text="บันทึก" />
			    <a class="btn" onClick="document.getElementById('ctl00_ContentPlaceHolder1_divModal').style.visibility='hidden';">ยกเลิก</a>			    
			</div>			
			<asp:TextBox ID="txt_Job_ID" runat="server" style="width:0px; height:0px; display:none;" Text="0"></asp:TextBox>
			<asp:TextBox ID="txt_Value" runat="server" style="width:0px; height:0px; display:none;" Text=""></asp:TextBox>
            <asp:TextBox ID="txt_PSN_ID" runat="server" style="width:0px; height:0px; display:none;" Text="0"></asp:TextBox>     
            <asp:TextBox ID="txt_Mode" runat="server" style="width:0px; height:0px; display:none;" Text=""></asp:TextBox>
			<asp:Button ID="btnCalculate" OnClick="btnCalculate_Click" runat="server" style="display:none" />            
		</asp:Panel>

</div>
<script language="javascript">
    function showDialog(PSN_ID, PSN_Name, HeaderText, Mode) {
        document.getElementById("ctl00_ContentPlaceHolder1_txt_PSN_ID").value = PSN_ID;
        document.getElementById("ctl00_ContentPlaceHolder1_txtDialog").value = PSN_Name;
        document.getElementById("ctl00_ContentPlaceHolder1_txt_Mode").value = Mode;
        document.getElementById("ctl00_ContentPlaceHolder1_lblHeader").innerHTML = HeaderText;
        document.getElementById('ctl00_ContentPlaceHolder1_divModal').style.visibility = 'visible';
        document.getElementById("ctl00_ContentPlaceHolder1_txtDialog").focus();
    }

    function updateHeader(Job_ID, _Value, PSN_ID, Mode) {
        document.getElementById("ctl00_ContentPlaceHolder1_txt_Job_ID").value = Job_ID;
        document.getElementById("ctl00_ContentPlaceHolder1_txt_Value").value = _Value.toString().replace(',', '');
        document.getElementById("ctl00_ContentPlaceHolder1_txt_PSN_ID").value = PSN_ID;
        document.getElementById("ctl00_ContentPlaceHolder1_txt_Mode").value = Mode;
        document.getElementById("ctl00_ContentPlaceHolder1_btnCalculate").click();
    }

    function textboxFocused(Job_ID, _Value, PSN_ID, Mode) {
        document.getElementById("ctl00_ContentPlaceHolder1_txt_Job_ID").value = Job_ID;
        document.getElementById("ctl00_ContentPlaceHolder1_txt_Value").value = _Value;
        document.getElementById("ctl00_ContentPlaceHolder1_txt_PSN_ID").value = PSN_ID;
        document.getElementById("ctl00_ContentPlaceHolder1_txt_Mode").value = Mode;
    }
    
</script>
										
</ContentTemplate>           
</asp:UpdatePanel>

	
</asp:Content>



using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Xml.Linq;
using System.Diagnostics;
using System.Collections.Specialized;
using System.Configuration;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Caching;
using System.Web.SessionState;
using System.Web.Security;
using System.Web.Profile;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
namespace VB
{

	public partial class RPT_W_Workload : System.Web.UI.Page
	{


		HRBL BL = new HRBL();
		Converter C = new Converter();

		GenericLib GL = new GenericLib();
		protected void Page_Load(object sender, System.EventArgs e)
		{
			if ((Session["USER_PSNL_NO"] == null)) {
				ScriptManager.RegisterStartupScript(this.Page, typeof(string), "Alert", "showAlert('กรุณาลงชื่อเข้าใช้ระบบ');", true);
				Response.Redirect("Login.aspx");
				return;
			}


			if (!IsPostBack) {
				BL.BindDDlSector(ddlSector);
				BindJob();
			}

			SetCheckboxStyle();
		}

		private void SetCheckboxStyle()
		{
			if (chkYes.Checked) {
				chkYes.CssClass = "checked";
			} else {
				chkYes.CssClass = "";
			}
			if (chkNo.Checked) {
				chkNo.CssClass = "checked";
			} else {
				chkNo.CssClass = "";
			}

			if (chkOver.Checked) {
				chkOver.CssClass = "checked";
			} else {
				chkOver.CssClass = "";
			}
		}


        protected void btnSearch_Click(object sender, System.EventArgs e)
		{
			BindJob();
		}

		private void BindJob()
		{
			string SQL = "";

			SQL += " SELECT Dept.SECTOR_CODE,Dept.SECTOR_NAME,Dept.DEPT_CODE,Dept.DEPT_NAME, " + "\n";
			SQL += " ISNULL(WKL.NumSlot,0) NumSlot,Workforce,ISNULL(WKL.MinPerYear,0) MinPerYear,WKL.Update_Time" + "\n";
			SQL += " FROM vw_HR_Department Dept ";
			SQL += " LEFT JOIN vw_WKL_Dept_FTE WKL ON DEPT.DEPT_CODE=WKL.DEPT_CODE AND Dept.MINOR_CODE=WKL.MINOR_CODE" + "\n";

			string Title = "";
			string Filter = "";
			if (ddlSector.SelectedIndex > 0) {

				Filter += " ( LEFT(DEPT.DEPT_CODE,2)='" + ddlSector.Items[ddlSector.SelectedIndex].Value + "' " + "\n";
				Filter += " OR LEFT(DEPT.DEPT_CODE,4)='" + ddlSector.Items[ddlSector.SelectedIndex].Value + "'   " + "\n";

				if (ddlSector.Items[ddlSector.SelectedIndex].Value == "3200") {
					Filter += " AND DEPT.DEPT_CODE NOT IN ('32000300'))    AND" + "\n";
				} else if (ddlSector.Items[ddlSector.SelectedIndex].Value == "3203") {
					Filter += " OR DEPT.DEPT_CODE IN ('32000300','32030000','32030100','32030200','32030300','32030500'))  AND" + "\n";
				} else if (ddlSector.Items[ddlSector.SelectedIndex].Value == "3204") {
					Filter += " OR DEPT.DEPT_CODE IN ('32040000','32040100','32040200','32040300','32040500','32040300'))  AND" + "\n";
				} else if (ddlSector.Items[ddlSector.SelectedIndex].Value == "3205") {
					Filter += " OR DEPT.DEPT_CODE IN ('32050000','32050100','32050200','32050300','32050500'))  AND" + "\n";
				} else {
					Filter += ")   AND" + "\n";
				}


				Title += GL.SplitString(ddlSector.Items[ddlSector.SelectedIndex].Text, ":")[1].Trim() + " ";
			}
			if (!string.IsNullOrEmpty(txtDeptName.Text)) {
                Filter += "(Dept.DEPT_NAME LIKE '" + txtDeptName.Text.Replace("'", "''") + "' OR Dept.DEPT_Code LIKE '" + txtDeptName.Text.Replace("'", "''") + "') AND ";
				Title += " หน่วยงาน : '" + txtDeptName.Text + "'";
			}

			if (chkYes.Checked & !chkNo.Checked) {
				Filter += " Update_Time IS NOT NULL AND ISNULL(WKL.MinPerYear,0)>0 AND ";
				Title += " แสดงหน่วยงานที่ทำตารางภาระกิจงานแล้ว";
			} else if (!chkYes.Checked & chkNo.Checked) {
				Filter += " Update_Time IS NULL AND ISNULL(WKL.MinPerYear,0)=0 AND ";
				Title += " แสดงหน่วยงานที่ยังไม่ทำตารางภาระกิจงาน";
			}

			if (chkOver.Checked) {
				Filter += " ISNULL(WKL.MinPerYear,0)/60/" + BL.MasterFTE().ToString() + "/CASE WHEN ISNULL(Workforce,0)=0 THEN NULL ELSE Workforce END >1 AND ";
				Title += " เฉพาะหน่วยงานที่ FTE/พนักงาน >1";
			}


			if (!string.IsNullOrEmpty(Filter)) {
				SQL += " WHERE " + Filter.Substring(0, Filter.Length - 4) + "\n";
			}

			SQL += " ORDER BY Dept.SECTOR_CODE,Dept.DEPT_CODE";

			SqlDataAdapter DA = new SqlDataAdapter(SQL, BL.ConnectionString());
			DataTable DT = new DataTable();
			DA.Fill(DT);

			string SubTitle = "";
			if (DT.Rows.Count == 0) {
				SubTitle = " ไม่พบรายการดังกล่าว";
			} else {
				object MinPerYear = DT.Compute("SUM(MinPerYear)", "");
				object Workforce = DT.Compute("SUM(Workforce)", "");
				SubTitle = " พบ " + GL.StringFormatNumber(DT.Rows.Count.ToString(), 0) + " รายการ";
				if (!GL.IsEqualNull(MinPerYear)) {
					double FTE = GL.CDBL(MinPerYear.ToString()) / 60 / BL.MasterFTE();
					SubTitle += "\n" + " ผลรวม FTE = " + GL.StringFormatNumber(FTE.ToString());
					if (!GL.IsEqualNull(Workforce) && GL.CDBL( Workforce.ToString()) > 0) {
						SubTitle += ", จำนวนพนักงานรวม=" + Workforce;
                        double WF=GL.CDBL(Workforce);
                        if (WF == 0)
                        {
                            SubTitle += ", FTE/พนักงาน=???";
                        }
                        else {
                            SubTitle += ", FTE/พนักงาน=" + GL.StringFormatNumber((GL.CDBL(FTE) / WF).ToString());
                        }
                        
					}
				}

			}
			lblTitle.Text = Title.Replace("\n", "<br>") + "<br>" + SubTitle.Replace("\n", "<br>");

			Session["RPT_W_Workload"] = DT;
			//-----------------
			Session["RPT_W_Workload_Title"] = Title;
			//-----------------
			Session["RPT_W_Workload_SubTitle"] = "รายงาน ณ วันที่ " + DateTime.Now.Day + " " + C.ToMonthNameTH(DateTime.Now.Month) + " พ.ศ." + (DateTime.Now.Year + 543);
			Session["RPT_W_Workload_SubTitle"] += SubTitle;


			//rptJob.DataSource = DT
			//rptJob.DataBind()
			//Pager.Visible = False

			Pager.SesssionSourceName = "RPT_W_Workload";
			Pager.RenderLayout();


		}

		protected void Pager_PageChanging(PageNavigation Sender)
		{
			Pager.TheRepeater = rptJob;
		}

        protected void rptJob_ItemCommand(object sender, System.Web.UI.WebControls.RepeaterCommandEventArgs e)
        {
            switch (e.CommandName) {
                case "Excel":
                    LinkButton lnkExcel = (LinkButton)e.Item.FindControl("lnkExcel");
                    string D = lnkExcel.Attributes["DEPT_CODE"];
                    string M = lnkExcel.Attributes["MINOR_CODE"];
                    ScriptManager.RegisterStartupScript(this.Page, typeof(string), "Excel", "window.location.href='print/RPT_W_Excel.aspx?D=" + D + "&M=" + M + "';", true);
                    break;
            }
        }
      
		string LastSectorName = "";
		protected void rptJob_ItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
		{
			if (e.Item.ItemType != ListItemType.AlternatingItem & e.Item.ItemType != ListItemType.Item)
				return;

            DataRowView drv = (DataRowView)e.Item.DataItem;

            Label lblSector = (Label)e.Item.FindControl("lblSector");
            Label lblPSNOrganize = (Label)e.Item.FindControl("lblPSNOrganize");
            Image imgDone = (Image)e.Item.FindControl("imgDone");
			Image imgNo= (Image) e.Item.FindControl("imgNo");
            Label lblWorker = (Label)e.Item.FindControl("lblWorker");
            Label lblFTE = (Label)e.Item.FindControl("lblFTE");
            Label lblFTEPerson = (Label)e.Item.FindControl("lblFTEPerson");
            Label lblUpdate = (Label)e.Item.FindControl("lblUpdate");
            LinkButton lnkExcel = (LinkButton)e.Item.FindControl("lnkExcel");
			HtmlTableCell tdSector = (HtmlTableCell)e.Item.FindControl("tdSector");


            if (drv["SECTOR_NAME"].ToString() != LastSectorName) {
				lblSector.Text = drv["SECTOR_NAME"].ToString();
				LastSectorName = drv["SECTOR_NAME"].ToString();
			} else {
				tdSector.Style["border-top"] = "none";
			}
			if (drv["DEPT_NAME"].ToString() != drv["SECTOR_NAME"].ToString()) {
				lblPSNOrganize.Text = drv["DEPT_NAME"].ToString().Replace( drv["SECTOR_NAME"].ToString(),"");
			} else {
				lblPSNOrganize.Text = drv["DEPT_NAME"].ToString();
			}

			if (!GL.IsEqualNull(drv["Update_Time"]) & GL.CINT(drv["MinPerYear"].ToString()) > 0) {
				imgDone.Visible = true;
				imgNo.Visible = false;
				lblUpdate.Text = GL.ReportThaiDateTime((DateTime)drv["Update_Time"]);
				if (!GL.IsEqualNull(drv["Workforce"])) {
					lblWorker.Text = drv["Workforce"].ToString();
				}
			} else {
				imgDone.Visible = false;
				imgNo.Visible = true;
				lblUpdate.Text = "";
			}

			if ( GL.CINT( drv["MinPerYear"].ToString()) > 0) {
				double FTE = GL.CDBL( drv["MinPerYear"].ToString()) / (60*BL.MasterFTE());
				lblFTE.Text = GL.StringFormatNumber(FTE.ToString());

				double FTEPerson = FTE / GL.CDBL( drv["Workforce"].ToString());
				lblFTEPerson.Text = GL.StringFormatNumber(FTEPerson.ToString());
				if (FTEPerson > 1) {
					lblFTEPerson.ForeColor = System.Drawing.Color.Red;
				} else {
					lblFTEPerson.ForeColor = System.Drawing.Color.Green;
				}

                lnkExcel.Attributes["DEPT_CODE"] = drv["DEPT_CODE"].ToString();
                lnkExcel.Attributes["MINOR_CODE"] = "00";
                lnkExcel.Visible = true;
			}


		}

	}
}

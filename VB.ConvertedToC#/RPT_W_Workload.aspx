﻿<%@ Page  Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPage.Master" CodeBehind="RPT_W_Workload.aspx.cs" Inherits="VB.RPT_W_Workload" %>

<%@ Register src="WUC_DateReporter.ascx" tagname="WUC_DateReporter" tagprefix="uc1" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<%@ Register src="PageNavigation.ascx" tagname="PageNavigation" tagprefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<asp:ToolkitScriptManager ID="toolkit1" runat="server" ></asp:ToolkitScriptManager>

<asp:UpdatePanel ID="UpdatePanel1" runat="server">      
<ContentTemplate>
<div class="container-fluid">
				<!-- BEGIN PAGE HEADER-->
				<div class="row-fluid" id="fixedPageHeader">
					<div class="span12"> 
						<!-- BEGIN PAGE TITLE & BREADCRUMB-->
						<h3>ตารางภารกิจงาน (Workload) และ FTE <font color="blue">(ScreenID : R-Workload-02)</font></h3>						
						<ul class="breadcrumb">
                            <li>
                            	<i class="icon-copy"></i> <a href="javascript:;">รายงาน</a><i class="icon-angle-right"></i>
                            </li>
                            <li>
                            	<i class="icon-copy"></i> <a href="javascript:;">การบริหารอัตรากำลัง</a><i class="icon-angle-right"></i>
                            </li>
                            <li>
                        	    <i class="icon-file"></i> <a href="javascript:;">ตารางภารกิจงาน (Workload) และ FTE</a>
                        	</li>                      	
                        	
                            <uc1:WUC_DateReporter ID="WUC_DateReporter1" runat="server" />
                        </ul>
						<!-- END PAGE TITLE & BREADCRUMB-->									        
				     </div>			
			    </div>
		        <!-- END PAGE HEADER-->
				
                <asp:Panel ID="pnlList" runat="server" Visible="True" DefaultButton="btnSearch">
				<div class="row-fluid">
					
					<div class="span12">
						<!-- BEGIN SAMPLE TABLE PORTLET-->
                                         <div class="row-fluid form-horizontal">
                			                  <div class="btn-group pull-left">
                                                    <asp:Button ID="btnSearch" OnClick="btnSearch_Click"  runat="server" CssClass="btn green" Text="ค้นหา" />
                                              </div> 
						                        <div class="btn-group pull-right">                                    
								                <button data-toggle="dropdown" class="btn dropdown-toggle" id="Button1">พิมพ์ <i class="icon-angle-down"></i></button>										
								                <ul class="dropdown-menu pull-right">                                               
                                                    <li><a href="Print/RPT_W_Workload.aspx?Mode=PDF" target="_blank">รูปแบบ PDF</a></li>
                                                    <li><a href="Print/RPT_W_Workload.aspx?Mode=EXCEL" target="_blank">รูปแบบ Excel</a></li>												
								                </ul>
							                </div>
							             </div>
						                 <div class="row-fluid form-horizontal">
						                             <div class="span3 ">
														<div class="control-group">
													        <label class="control-label" style="width:100px;"> ฝ่าย</label>
													        <div class="controls" style="margin-left:120px;">
														        <asp:DropDownList ID="ddlSector" OnSelectedIndexChanged=btnSearch_Click runat="server" AutoPostBack="false" CssClass="medium m-wrap">
							                                    </asp:DropDownList>														        
													        </div>
												        </div>
													</div>
													
													<div class="span8 ">
														<div class="control-group">
													        <label class="control-label"> หน่วยงาน</label>
													        <div class="controls">
														        <asp:TextBox ID="txtDeptName" OnTextChanged=btnSearch_Click runat="server" AutoPostBack="false" CssClass="medium m-wrap" Placeholder="ค้นหาชื่อฝ่าย/หน่วยงาน"></asp:TextBox>		
													        </div>
												        </div>
													</div>
    										</div>
    										
    										<div class="row-fluid form-horizontal">
											        <div class="span5">
											            <div class="control-group">
										                    <label class="control-label" style="width:100px;">เงื่อนไข</label>
										                    
                                                            <div class="controls" style="margin-left:120px;">
                                                                <label class="checkbox">
											                        <asp:CheckBox ID="chkYes" OnCheckedChanged="btnSearch_Click" runat="server" AutoPostBack="false" Text=""/>&nbsp;ทำ Workload แล้ว
											                    </label>
                                                                <label class="checkbox">
											                        <asp:CheckBox ID="chkNo" OnCheckedChanged="btnSearch_Click" runat="server" AutoPostBack="false" Text=""/>&nbsp;ยังไม่ทำ Workload
											                    </label>
										                    </div>
									                    </div>
											        </div>		
											        <div class="span6">
											            <div class="control-group">
										                    <label class="control-label" style="width:100px;">เงื่อนไข</label>
                                                            <div class="controls" style="margin-left:120px;">
                                                                <label class="checkbox">
											                        <asp:CheckBox ID="chkOver" OnCheckedChanged="btnSearch_Click" runat="server" AutoPostBack="false" Text=""/>&nbsp;เฉพาะหน่วยงานที่ FTE/พนักงาน >1
											                    </label>
										                    </div>
									                    </div>
											        </div>										       
											</div>
    									
											
							<div class="portlet-body no-more-tables">
								
								<asp:Label ID="lblTitle" runat="server" Font-Size="14px" Font-Bold="true" Width="100%" style="text-align:center"></asp:Label>
								<table class="table table-full-width table-bordered dataTable table-hover">
									<thead>
										<tr>
											<th style="text-align:center;" > ฝ่าย</th>
											<th style="text-align:center;" > หน่วยงาน</th>
											<th style="text-align:center;" > การทำ Workload</th>
											<th style="text-align:center;" > FTE รวม</th>
											<th style="text-align:center;" > พนักงาน(คน)</th>									
											<th style="text-align:center;" > FTE/พนักงาน</th>
											<th style="text-align:center;" > แก้ไขล่าสุด</th>
                                            <th style="text-align:center;" > แสดงเป็น Excel</th>												
										</tr>										
									</thead>
									<tbody>
										<asp:Repeater ID="rptJob" OnItemDataBound="rptJob_ItemDataBound"  OnItemCommand="rptJob_ItemCommand" runat="server">
									        <ItemTemplate>
									            <tr>    
									                <td data-title="ฝ่าย" id="tdSector" runat="server"><asp:Label ID="lblSector" runat="server"></asp:Label></td>                                    
											        <td data-title="หน่วยงาน"><asp:Label ID="lblPSNOrganize" runat="server"></asp:Label></td>											        
											        <td data-title="การทำ Workload" style="text-align:center;">
											            <asp:Image ID="imgDone" runat="server" ImageUrl="~/images/check.png"></asp:Image>
											            <asp:Image ID="imgNo" runat="server" ImageUrl="~/images/none.png"></asp:Image>
											        </td>
											        <td data-title="FTE" style="text-align:center;"><asp:Label ID="lblFTE" runat="server"></asp:Label></td>
											        <td data-title="พนักงาน" style="text-align:center;"><asp:Label ID="lblWorker" runat="server"></asp:Label></td>
											        <td data-title="FTE/พนักงาน" style="text-align:center;"><asp:Label ID="lblFTEPerson" runat="server"></asp:Label></td>
											        <td data-title="แก้ไขล่าสุด" style="text-align:center;"><asp:Label ID="lblUpdate" runat="server"></asp:Label></td>
                                                    <td data-title="Excel" ><asp:LinkButton ID="lnkExcel" CssClass="btn mini green" runat="server" CommandName="Excel" Visible="false"><i class="icon-upload-alt"></i></asp:LinkButton></td>											        
										        </tr>	
									        </ItemTemplate>
										</asp:Repeater>
																							
									</tbody>
								</table>
								<asp:PageNavigation ID="Pager" OnPageChanging="Pager_PageChanging" MaximunPageCount="10" PageSize="20" runat="server" />
							</div>
					</div>
                </div>  
                </asp:Panel>  


</div>

</ContentTemplate>           
</asp:UpdatePanel>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ScriptPlaceHolder" Runat="Server">
</asp:Content>


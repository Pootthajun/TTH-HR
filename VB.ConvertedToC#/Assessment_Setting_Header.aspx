﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="Assessment_Setting_Header.aspx.cs" Inherits="VB.Assessment_Setting_Header" %>

<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<%@ Register src="WUC_DateReporter.ascx" tagname="WUC_DateReporter" tagprefix="uc1" %>
<%@ Register src="PageNavigation.ascx" tagname="PageNavigation" tagprefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<asp:ToolkitScriptManager ID="toolkit1" runat="server"></asp:ToolkitScriptManager>
<asp:UpdatePanel ID="UpdatePanel1" runat="server">
<ContentTemplate>
<div class="container-fluid">
				<!-- BEGIN PAGE HEADER-->
				<div class="row-fluid" id="fixedPageHeader">
					<div class="span12"> 
						<!-- BEGIN PAGE TITLE & BREADCRUMB-->
						<h3 class="page-user-md">จัดการข้อมูลใบประเมิน <font color="blue"></font>
                        <asp:Label ID="lblReportTime" Font-Size="14px" ForeColor="Green" runat="Server"></asp:Label>
                        </h3>					
									
						<ul class="breadcrumb">
                            <li>
                            	<i class="icon-cogs"></i><a href="javascript:;">จัดการระบบ/ข้อมูลพื้นฐาน</a><i class="icon-angle-right"></i>
                            </li>
                        	<li><i class="icon-share-alt"></i> <a href="javascript:;">จัดการข้อมูลใบประเมิน</a></li>
                        	
                            <uc1:WUC_DateReporter ID="WUC_DateReporter1" runat="server" />
                        </ul>
						<!-- END PAGE TITLE & BREADCRUMB-->
									        
				     </div>

				
			    </div>


                				<asp:Panel ID="pnlList" runat="server" Visible="True">
                    <div class="row-fluid">
					
					    <div class="span12">
						    <!-- BEGIN SAMPLE TABLE PORTLET-->
						
							
							  
							                   	<asp:Panel CssClass="row-fluid form-horizontal" ID="pnlSearch" runat="server" DefaultButton="btnSearch">
												     
												     <div class="row-fluid form-horizontal">
												          <div class="span4 ">
														    <div class="control-group">
													            <label class="control-label"><i class="icon-retweet"></i> สำหรับรอบการประเมิน</label>
													            <div class="controls">
														            <asp:DropDownList ID="ddlRound" OnSelectedIndexChanged="Search" runat="server" AutoPostBack="true" CssClass="medium m-wrap">
														            </asp:DropDownList>
														            <asp:Button ID="btnSearch" OnClick="Search" runat="server" Style="display:none" />
														        </div>
												            </div>
													    </div>	
													    <div class="span6 ">
														    <div class="control-group">
													            <label class="control-label"><i class="icon-user"></i> ชื่อ</label>
													            <div class="controls">
														            <asp:TextBox ID="txt_Search_Name" OnTextChanged ="Search" runat="server" AutoPostBack="true" CssClass="m-wrap medium" placeholder="ค้นหาจากชื่อ/เลขประจำตัว/ตำแหน่ง"></asp:TextBox>
													            </div>
												            </div>
													    </div>	
												     </div>
												    <div class="row-fluid form-horizontal">
												        <div class="span4 ">
													        <div class="control-group">
													            <label class="control-label"><i class="icon-sitemap"></i> ฝ่าย</label>
													            <div class="controls">
														            <asp:DropDownList ID="ddlSector" OnSelectedIndexChanged="Search" runat="server" AutoPostBack="true" CssClass="medium m-wrap">
														            </asp:DropDownList>
														        </div>
												            </div>
													    </div>
													     <div class="span6 ">
														    <div class="control-group">
													            <label class="control-label"><i class="icon-sitemap"></i> ชื่อหน่วยงาน</label>
													            <div class="controls">
														            <asp:TextBox ID="txt_Search_Organize" OnTextChanged="Search" runat="server" AutoPostBack="true" CssClass="m-wrap medium" placeholder="ค้นหาจากหน่วยงาน/ตำแหน่ง"></asp:TextBox>
														        </div>
												            </div>
													    </div>													    
												    </div>
												    <div class="row-fluid form-horizontal" id="searchStatus_" runat="server" visible ="false">
												        <div class="span6 ">
														    <div class="control-group">
													            <label class="control-label"><i class="icon-bolt"></i> ความคืบหน้า</label>
													            <div class="controls">
														            <asp:DropDownList ID="ddlStatus" OnSelectedIndexChanged="Search" runat="server" AutoPostBack="true" CssClass="medium m-wrap">
														                <asp:ListItem Text="ทั้งหมด" Value="-1" Selected="true"></asp:ListItem>
														                <asp:ListItem Text="ยังไม่ส่งแบบประเมิน" Value="0"></asp:ListItem>
					                                                    <asp:ListItem Text="รออนุมัติแบบประเมิน" Value="1"></asp:ListItem>
					                                                    <asp:ListItem Text="อนุมัติแบบประเมินแล้ว" Value="2"></asp:ListItem>
					                                                    <asp:ListItem Text="พนักงานประเมินตนเอง" Value="3"></asp:ListItem>
					                                                    <asp:ListItem Text="รออนุมัติผลการประเมิน" Value="4"></asp:ListItem>
					                                                    <asp:ListItem Text="ประเมินเสร็จสมบูรณ์" Value="5"></asp:ListItem>
														            </asp:DropDownList>
														        </div>
												            </div>
													    </div>				
												    </div>
																																			   
												</asp:Panel>
								            
							<div class="portlet-body no-more-tables">
								<asp:Label ID="lblCountPSN" runat="server" Font-Size="14px" Font-Bold="true"></asp:Label>
								<table class="table table-full-width table-advance dataTable no-more-tables table-hover">
									<thead>
										<tr>
											<th><i class="icon-sitemap"></i> ฝ่าย</th>
											<th><i class="icon-sitemap"></i> หน่วยงาน</th>
											<th><i class="icon-user"></i> ชื่อ</th>
											<th><i class="icon-briefcase"></i> ตำแหน่ง</th>
											<th><i class="icon-bookmark"></i> ระดับ</th>
											<th><i class="icon-briefcase"></i> ประเภท</th>
											<th><i class="icon-user-md"></i> ผู้ประเมิน</th>
											<%--<th><i class="icon-info"></i> สถานะ</th>--%>
											<th><i class="icon-bolt"></i> ดำเนินการ</th>
										</tr>
									</thead>
									<tbody>
										<asp:Repeater ID="rptKPI" OnItemCommand="rptKPI_ItemCommand" OnItemDataBound="rptKPI_ItemDataBound" runat="server">
									        <ItemTemplate>
									            <tr>                                        
											        <td data-title="ฝ่าย"><asp:Label ID="lnkPSNSector" runat="server"></asp:Label></td>
											        <td data-title="หน่วยงาน"><asp:Label ID="lnkPSNDept" runat="server"></asp:Label></td>
											        <td data-title="ชื่อ"><asp:Label ID="lblPSNName" runat="server"></asp:Label></td>
											        <td data-title="ตำแหน่ง"><asp:Label ID="lblPSNPos" runat="server"></asp:Label></td>
											        <td data-title="ระดับ"><asp:Label ID="lblPSNClass" runat="server"></asp:Label></td>
											        <td data-title="ประเภท"><asp:Label ID="lblPSNType" runat="server"></asp:Label></td>
											        <td data-title="ผู้ประเมิน"><asp:Label ID="lblPSNASSESSOR" runat="server"></asp:Label></td>
											        <td data-title="สถานะ"  id="tdStatus_" runat="server" visible ="false"><asp:Label ID="lblPSNKPIStatus" runat="server"></asp:Label></td>
											        <td data-title="ดำเนินการ">
											            <asp:Button ID="btnPSNEdit" runat="server" CssClass="btn mini blue" CommandName="Edit" Text="การประเมิน" />											           
										            </td>
										        </tr>	
									        </ItemTemplate>
										</asp:Repeater>
																							
									</tbody>
								</table>
								<asp:PageNavigation ID="Pager" OnPageChanging="Pager_PageChanging" MaximunPageCount="10" PageSize="20" runat="server" />
							</div>
						
						<!-- END TABLE PORTLET-->
						
					</div>
                </div>
              </asp:Panel>


     <asp:Panel ID="pnlEdit" runat="server" Visible="False">              
				           <div class="btn-group pull-left">
                                             <asp:Button ID="btnBack_TOP" OnClick="btnBack_Click" runat="server" CssClass="btn" Text="ย้อนกลับ" />
                                        </div>
			                
    			                    <div class="form-horizontal form-view"  style=" margin-top:50px;">
    			                
    			                       
                                        
                                            			                    
    			                    
                			        <h3 style="text-align:center; ">
					                  ข้อมูลใบประเมิน KPI & COMP  <asp:Label ID="lblRound" runat="server"></asp:Label> 
					                  <asp:DropDownList ID="ddlKPIStatus" OnSelectedIndexChanged="ddlKPIStatus_SelectedIndexChanged" Visible ="false"  runat="server" AutoPostBack="true"
					                  style="background-color:White; border:none; color:Black; font-size:24px; padding-top:0px; height:35px; width:280px;">
					                    <asp:ListItem Text="ไม่พบแบบประเมิน" Value="-1"></asp:ListItem>
					                    <asp:ListItem Text="ยังไม่ส่งแบบประเมิน" Value="0"></asp:ListItem>
					                    <asp:ListItem Text="รออนุมัติแบบประเมิน" Value="1"></asp:ListItem>
					                    <asp:ListItem Text="อนุมัติแบบประเมินแล้ว" Value="2"></asp:ListItem>
					                    <asp:ListItem Text="พนักงานประเมินตนเอง" Value="3"></asp:ListItem>
					                    <asp:ListItem Text="รออนุมัติผลการประเมิน" Value="4"></asp:ListItem>
					                    <asp:ListItem Text="ประเมินเสร็จสมบูรณ์" Value="5"></asp:ListItem>
					                  </asp:DropDownList> 
					                </h3>  
    			                   <div class="row-fluid" style =" margin-top :20px;">
    			                     <div class="row-fluid" style="border-bottom:1px solid #EEEEEE;">
					                        <div class="span4 ">
					                            <div class="control-group">
								                    <label class="control-label" style="width:100px;">ผู้รับการประเมิน :</label>
								                    <div class="controls" style="margin-left: 120px;">
									                    <asp:Label ID="lblPSNName" runat="server" CssClass="text bold"></asp:Label>
								                    </div>
							                    </div>
					                        </div>
					                        <div class="span3 ">
					                            <div class="control-group">
								                    <label class="control-label" style="width:100px;">ตำแหน่ง :
                                                    <asp:LinkButton ID="btn_Fixed_POS_PSN" OnClick="btn_Fixed_POS_PSN_Click" runat="server" CssClass="btn red mini"><i class="icon-screenshot"></i> เปลี่ยน</asp:LinkButton>
                                                    </label>
								                    <div class="controls" style="margin-left: 120px;">
									                    <asp:Label ID="lblPSNPos" runat="server" CssClass="text bold"></asp:Label>
								                    </div>
							                    </div>
					                        </div>
					                        <div class="span5 ">
					                            <div class="control-group">
								                    <label class="control-label" style="width:100px;">สังกัด :</label>
								                    <div class="controls" style="margin-left: 120px;">
									                    <asp:Label ID="lblPSNDept" runat="server" CssClass="text bold"></asp:Label>
								                    </div>
							                    </div>
					                        </div>
					                     </div>
                					     </div>
					                      <div class="row-fluid" style="margin-top:50px;">
					                        <div class="span4 ">
					                            <div class="control-group">
								                    <label class="control-label" style="width:100px;">ผู้ประเมิน :
								                    <asp:LinkButton ID="btn_Fixed_Assessor" OnClick="btn_Fixed_Assessor_Click" runat="server" CssClass="btn red mini"><i class="icon-screenshot"></i> เพิ่มเติม</asp:LinkButton>
								                    </label>
								                    <div class="controls" style="margin-left: 120px;">
									                    <asp:DropDownList ID="ddlASSName" OnSelectedIndexChanged="ddlASSName_SelectedIndexChanged" runat="Server" AutoPostBack="True" style="font-weight:bold; color:Black; border:none;">									        
									                    </asp:DropDownList>
									                    
									                </div>
							                    </div>
					                        </div>
					                        <div class="span3 ">
					                            <div class="control-group">
								                    <label class="control-label" style="width:100px;">ตำแหน่ง :
                                                    <asp:LinkButton ID="btn_Fixed_POS_Assessor" OnClick="btn_Fixed_POS_Assessor_Click" runat="server" CssClass="btn red mini"><i class="icon-screenshot"></i> เปลี่ยน</asp:LinkButton>
                                                    </label>
								                    <div class="controls" style="margin-left: 120px;">
									                    <asp:Label ID="lblASSPos" runat="server" CssClass="text bold"></asp:Label>
								                    </div>
							                    </div>
					                        </div>
					                        <div class="span5 ">
					                            <div class="control-group">
								                    <label class="control-label" style="width:100px;">สังกัด :</label>
								                    <div class="controls" style="margin-left: 120px;">
									                    <asp:Label ID="lblASSDept" runat="server" CssClass="text bold"></asp:Label>
								                    </div>
							                    </div>
					                        </div>
					                     </div>





                                    </div>

                                    				<asp:Panel ID="pnlAddAss" DefaultButton="btn_Search_Fixed_Ass" runat="server" Visible="False">
                    <div style="z-index: 1049;" class="modal-backdrop fade in"></div>
                    <div style="z-index: 1050; max-width:90%; min-width:70%; left:30%; max-height:95%; top:5%;" class="modal">
                        
				            <div class="modal-header">
	                            <h4><i class="icon-search"></i> เลือกผู้ประเมินเพิ่มเติม (ตั้งแต่อดีตจนถึงปัจจุบัน)</h4>
                            </div>
                            <div class="modal-body" style="max-height:85%;">
	                                  	<div >
											<div class="row-fluid">																			
												    <div class="row-fluid">												           					
												            <div class="span4 ">
														        <div class="control-group">
													                <label class="control-label"><i class="icon-sitemap"></i> หน่วยงาน/ตำแหน่ง</label>
													                <div class="controls">
														                <asp:TextBox ID="txt_Search_ASS_POS" OnTextChanged="btn_Fixed_Assessor_Click" runat="server" AutoPostBack="true" CssClass="m-wrap medium" placeholder="ค้นหาจากหน่วยงาน/ตำแหน่ง"></asp:TextBox>
														                <asp:Button ID="btn_Search_Fixed_Ass" OnClick="btn_Fixed_Assessor_Click" runat="server" style="display:none;" />
														            </div>
												                </div>
													        </div>													    
													       <div class="span4 ">
												                <div class="control-group">
													                <label class="control-label"><i class="icon-user"></i> ชื่อ/เลขประจำตัว</label>
													                <div class="controls">
														                <asp:TextBox ID="TextBox2" OnTextChanged="btn_Fixed_Assessor_Click" runat="server" AutoPostBack="true" CssClass="m-wrap medium" placeholder="ค้นหาจากชื่อ/เลขประจำตัว"></asp:TextBox>
														                <asp:Button ID="txt_Search_ASS_Name" OnClick="btn_Fixed_Assessor_Click" runat="server" Text="" style="display:none;" />
													                </div>
												                </div>														        
													        </div>			
													        
												    </div>
												    	
												     <asp:Label ID="lblCountFixedAssessor" runat="server" Font-Size="14px" Font-Bold="true"></asp:Label>
<table class="table table-full-width table-advance dataTable no-more-tables table-hover">
									                    <thead>
										                    <tr>
											                    <th><i class="icon-sitemap"></i> หน่วยงาน</th>
											                    <th><i class="icon-user"></i>พนักงาน</th>
											                    <th><i class="icon-briefcase"></i> ตำแหน่ง</th>
											                    <th><i class="icon-bookmark"></i> ระดับ</th>											                    
											                    <th><i class="icon-bolt"></i> เลือก</th>
										                    </tr>
									                    </thead>
									                    <tbody>
									                        <asp:Repeater ID="rptFixedAssessor" OnItemCommand="rptFixedAssessor_ItemCommand" OnItemDataBound="rptFixedAssessor_ItemDataBound" runat="server">
									                            <ItemTemplate>
    									                            <tr>                                        
											                            <td data-title="หน่วยงาน"><asp:Label ID="lblPSNDept" runat="server"></asp:Label></td>
											                            <td data-title="พนักงาน"><asp:Label ID="lblPSNName" runat="server"></asp:Label></td>
											                            <td data-title="ตำแหน่ง"><asp:Label ID="lblPSNPos" runat="server"></asp:Label></td>
											                            <td data-title="ระดับ"><asp:Label ID="lblPSNClass" runat="server"></asp:Label></td>											                            
											                            <td data-title="เลือก">
											                                <asp:Button ID="btnSelect" runat="server" CssClass="btn mini blue" CommandName="Select" Text="เลือก" />
											                                <asp:ConfirmButtonExtender ID="cfmbtnSelect" TargetControlID="btnSelect" runat="server" ></asp:ConfirmButtonExtender>
										                                </td>
										                            </tr>	
									                            </ItemTemplate>
									                        </asp:Repeater>
									                    </tbody>
								                    </table>                    								
								                    <asp:PageNavigation ID="PagerFixedAssessor" OnPageChanging="PagerFixedAssessor_PageChanging" MaximunPageCount="10" PageSize="7" runat="server" />
											  
											</div>
										</div>
                            </div>                            
                            
                            <asp:LinkButton ID="btnCloseFixedAss" OnClick="btnCloseFixedAss_Click" runat="server" CssClass="fancybox-item fancybox-close" ToolTip="Close" />
                     
                    </div>
               </asp:Panel>       

               <asp:Panel ID="pnlPOSDialog" DefaultButton="btn_Search_POS" runat="server" Visible="false">
                    <div style="z-index: 10049;" class="modal-backdrop fade in"></div>
                    <div style="z-index: 10050; max-width:90%; min-width:70%; left:30%; max-height:95%; top:5%;" class="modal">
                        
				            <div class="modal-header">
	                            <h3><i class="icon-search"></i> <asp:Label ID="lblChooseMode" runat="server" Text="พนักงาน/ผู้ถูกประเมิน"></asp:Label></h3>
                            </div>
                            <div class="modal-body" style="max-height:85%;">
	                                  	<div >
											<div class="row-fluid">
																			
												    <div class="row-fluid">												            
													         <div class="span4 ">
														        <div class="control-group">
													                <label class="control-label"><i class="icon-sitemap"></i> หน่วยงาน</label>
													                <div class="controls">
														                <asp:TextBox ID="txt_Dialog_DEPPSN" OnTextChanged="btn_Search_POS_Click" runat="server" AutoPostBack="true" CssClass="m-wrap medium" placeholder="ค้นหาจากฝ่าย/หน่วยงาน"></asp:TextBox>
														            </div>
												                </div>
													        </div>
													    
													       <div class="span4 ">
												                <div class="control-group">
													                <label class="control-label"><i class="icon-briefcase"></i> ตำแหน่ง</label>
													                <div class="controls">
														                <asp:TextBox ID="txt_Dialog_PosPSN" OnTextChanged="btn_Search_POS_Click" runat="server" AutoPostBack="true" CssClass="m-wrap medium" placeholder="ค้นหาจากเลขตำแหน่ง/ชื่อตำแหน่ง"></asp:TextBox>
														                <asp:Button ID="btn_Search_POS" OnClick="btn_Search_POS_Click"  runat="server" Text="" style="display:none;" />
													                </div>
												                </div>														        
													        </div>	
													         <div class="span4 ">
												                <div class="control-group">
													                <label class="control-label"><i class="icon-bookmark"></i> ระดับ</label>
													                <div class="controls">
														                <asp:TextBox ID="txt_Search_Class" OnTextChanged="btn_Search_POS_Click" runat="server" AutoPostBack="true" CssClass="m-wrap medium" placeholder="เลขระดับ"></asp:TextBox>
														            </div>
												                </div>														        
													        </div>												        
												    </div>
												    	
												     <asp:Label ID="Label1" runat="server" Font-Size="14px" Font-Bold="true"></asp:Label>
								                    <table class="table table-full-width table-advance dataTable no-more-tables table-hover">
									                    <thead>
										                    <tr>
											                    <th style="text-align:center;"><i class="icon-sitemap"></i> ฝ่าย</th>
											<th style="text-align:center;"><i class="icon-sitemap"></i> หน่วยงาน</th>
											<th style="text-align:center;"><i class="icon-bookmark"></i> ระดับ</th>
											<th style="text-align:center;"><i class="icon-sitemap"></i> เลขตำแหน่ง</th>
											<th style="text-align:center;"><i class="icon-briefcase"></i> ตำแหน่ง</th>
											<th style="text-align:center;"><i class="icon-bookmark"></i> ตำแหน่งการบริหาร</th>
											                    <th style="text-align:center;"><i class="icon-bolt"></i> เลือก</th>
										                    </tr>
									                    </thead>
									                    <tbody>
									                        <asp:Repeater ID="rptPOSDialog" OnItemCommand="rptPOSDialog_ItemCommand" OnItemDataBound="rptPOSDialog_ItemDataBound" runat="server">
									                            <ItemTemplate>
    									                            <tr>                                        
											                            <td data-title="ฝ่าย"><asp:Label ID="lblSector" runat="server"></asp:Label></td>
                                                        <td data-title="หน่วยงาน"><asp:Label ID="lblDept" runat="server"></asp:Label></td>
                                                        <td data-title="ระดับ"><asp:Label ID="lblClass" runat="server"></asp:Label></td>
    										            <td data-title="เลขตำแหน่ง"><asp:Label ID="lblPosNo" runat="server"></asp:Label></td>
    										            <td data-title="ตำแหน่ง"><asp:Label ID="lblFLD" runat="server"></asp:Label></td>
    										            <td data-title="ตำแหน่งทางการบริหาร"><asp:Label ID="lblMGR" runat="server"></asp:Label></td>  
											                            <td data-title="เลือก" style="text-align:center;">
											                                <input type="button" ID="btnPreSelect" runat="server" class="btn mini blue" value="เลือก" visible ="false" />
											                                <asp:Button ID="btnSelect" runat="server" CssClass="btn mini blue" Text="เลือก" CommandName="Select" />
										                                </td>
										                            </tr>	
									                            </ItemTemplate>
									                        </asp:Repeater>
									                    </tbody>
								                    </table>
                    								
								                    <asp:PageNavigation ID="PagerPOS" OnPageChanging="PagerPOS_PageChanging" MaximunPageCount="10" PageSize="7" runat="server" />
											  
											</div>
										</div>
                            </div>                            
                            <asp:LinkButton CssClass="fancybox-item fancybox-close" ToolTip="Close" ID="btnCloseDialog" OnClick="btnCloseDialog_Click" runat="server"></asp:LinkButton>
                                                  
            </div>
       </asp:Panel>


       <asp:Button ID="btnSave" OnClick="btnSave_Click" runat="server" Text="Click เพื่อบันทึกแก้ไขข้อมูลใบประเมินทั้ง KPI & Competency" style="text-align:center; width:100%; margin-top :60px; font-weight:bold; font-size:14px;" CssClass="btn green btn-block" />
								    <asp:ConfirmButtonExtender TargetControlID="btnSave" runat="server" ID="cfm_Top" ConfirmText="บันทึกการแก้ไขข้อมูบใบประเมิน ?"></asp:ConfirmButtonExtender>

      </asp:Panel>


</div>

</ContentTemplate>
</asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ScriptPlaceHolder" runat="server">
</asp:Content>

﻿using Microsoft.VisualBasic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data;
using System.Data.SqlClient;
using System.Threading;
using System.Globalization;

namespace VB
{
    public partial class RPT_A_DEPTAssCompleted : System.Web.UI.Page
    {
        HRBL BL = new HRBL();
        GenericLib GL = new GenericLib();

        Converter C = new Converter();
        public int R_Year
        {
            get
            {
                try
                {
                    return GL.CINT(GL.SplitString(ddlRound.Items[ddlRound.SelectedIndex].Value, "-")[0]);
                }
                catch (Exception ex)
                {
                    return 0;
                }
            }
        }

        public int R_Round
        {
            get
            {
                try
                {
                    return GL.CINT(GL.SplitString(ddlRound.Items[ddlRound.SelectedIndex].Value, "-")[1]);
                }
                catch (Exception ex)
                {
                    return 0;
                }
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if ((Session["USER_PSNL_NO"] == null))
            {
                ScriptManager.RegisterStartupScript(this.Page, typeof(string), "Alert", "showAlert('กรุณาลงชื่อเข้าใช้ระบบ');", true);
                Response.Redirect("Login.aspx");
                return;
            }

            if (!IsPostBack)
            {
                //----------------- ทำสองที่ Page Load กับ Update Status ---------------
                BL.BindDDlYearRound(ddlRound);
                ddlRound.Items.RemoveAt(0);
                BL.BindDDlSector(ddlSector);
                BL.BindDDlDepartment(ddlDept, ddlSector.SelectedValue);
                //txtManualDate.Attributes["ReadOnly"] = "true";
                BindDeptList();
            }
        }

        protected void btnSearch_Click(object sender, System.EventArgs e)
        {
            BindDeptList();
        }

        protected void ddlSector_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            BL.BindDDlDepartment(ddlDept, ddlSector.SelectedValue);
        }

        protected void ddlDateModeKPI_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            txtManualDateKPI.Visible = (ddlDateModeKPI.SelectedIndex == 1);
        }

        protected void ddlDateModeCOMP_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            txtManualDateCOMP.Visible = (ddlDateModeCOMP.SelectedIndex == 1);
        }

        private void BindDeptList()
        {
            string SQL = "";
            SQL += " SELECT COMP.R_Year,COMP.R_Round" + "\n";
            SQL += " ,COMP.SECTOR_CODE,COMP.SECTOR_NAME,COMP.DEPT_CODE,COMP.DEPT_NAME" + "\n";
            SQL += " ,COMP.Commit_Time COMP_Commit_Time,COMP_End,KPI.Commit_Time KPI_Commit_Time,KPI_End" + "\n";
            SQL += " FROM vw_COMP_Completed COMP" + "\n";
            SQL += " LEFT JOIN vw_KPI_Completed KPI ON COMP.R_Year=KPI.R_Year AND COMP.R_Round=KPI.R_Round" + "\n";
            SQL += " 							 AND COMP.DEPT_CODE=KPI.DEPT_CODE" + "\n";
            SQL += " WHERE COMP.R_Year=" + R_Year + "\n";
            SQL += " AND COMP.R_Round=" + R_Round + "\n";

            string Title = "";

            if (ddlSector.SelectedIndex > 0)
            {
                SQL += " AND ( COMP.SECTOR_CODE='" + ddlSector.Items[ddlSector.SelectedIndex].Value + "' " + "\n";
                if (ddlSector.Items[ddlSector.SelectedIndex].Value == "3200")
                {
                    SQL += " AND  COMP.DEPT_CODE NOT IN ('32000300'))    " + "\n";
                    //-----------ฝ่ายโรงงานผลิตยาสูบ 3
                }
                else if (ddlSector.Items[ddlSector.SelectedIndex].Value == "3203")
                {
                    SQL += " OR  COMP.DEPT_CODE IN ('32000300','32030000','32030100','32030200','32030300','32030500'))  " + "\n";
                    //-----------ฝ่ายโรงงานผลิตยาสูบ 4
                }
                else if (ddlSector.Items[ddlSector.SelectedIndex].Value == "3204")
                {
                    SQL += " OR  COMP.DEPT_CODE IN ('32040000','32040100','32040200','32040300','32040500','32040300'))  " + "\n";
                    //-----------ฝ่ายโรงงานผลิตยาสูบ 5
                }
                else if (ddlSector.Items[ddlSector.SelectedIndex].Value == "3205")
                {
                    SQL += " OR  COMP.DEPT_CODE IN ('32050000','32050100','32050200','32050300','32050500'))\t  " + "\n";
                }
                else
                {
                    SQL += ")   " + "\n";
                }

                if (ddlDept.SelectedIndex < 1)
                {
                    Title += GL.SplitString(ddlSector.Items[ddlSector.SelectedIndex].Text, ":")[1].Trim() + " ";
                    //----- เลือกแสดงชื่อหน่วยงานเดียว --------
                }
            }
            if (ddlDept.SelectedIndex > 0)
            {
                SQL += " AND COMP.DEPT_CODE = '" + GL.SplitString(ddlDept.SelectedValue, "-")[0] + "' " + "\n";

                Title += ddlDept.Items[ddlDept.SelectedIndex].Text + " ";
                //----- เลือกแสดงชื่อหน่วยงานเดียว --------
            }
            if (ddlDateModeKPI.SelectedIndex == 1 & txtManualDateKPI.Text == "")
            {
                ScriptManager.RegisterStartupScript(this.Page, typeof(string), "Alert", "showAlert('เลือกวันที่กำหนดส่ง KPI');", true);
                return;
            }
            if (ddlDateModeCOMP.SelectedIndex == 1 & txtManualDateCOMP.Text == "")
            {
                ScriptManager.RegisterStartupScript(this.Page, typeof(string), "Alert", "showAlert('เลือกวันที่กำหนดส่ง Competency');", true);
                return;
            }


            if (Title == "")
                Title += "";
            else
                Title = "ส่วนงาน/" + Title;
            string r = ddlRound.Items[ddlRound.SelectedIndex].Text;
            string[] splt = { " " };
            Title += r.Split(splt, StringSplitOptions.RemoveEmptyEntries)[2] + " " + r.Split(splt, StringSplitOptions.RemoveEmptyEntries)[3] + " " + r.Split(splt, StringSplitOptions.RemoveEmptyEntries)[0] + " " + r.Split(splt, StringSplitOptions.RemoveEmptyEntries)[1];
                        
            SQL += " ORDER BY SECTOR_CODE,DEPT_CODE" + "\n";

            SqlDataAdapter DA = new SqlDataAdapter(SQL, BL.ConnectionString());
            DA.SelectCommand.CommandTimeout = 90;
            DataTable DT = new DataTable();
            DA.Fill(DT);

            Thread.CurrentThread.CurrentCulture = new CultureInfo("th-TH");
            DataTable Round = (DataTable)BL.GetRoundInfo(R_Year, R_Round);
            
            DateTime COMP_End = (DateTime)DT.Rows[0]["COMP_End"];
            DateTime KPI_End = (DateTime)DT.Rows[0]["KPI_End"];

            switch (ddlDateModeKPI.SelectedIndex)
            {
                case 0:
                    break;
                case 1:
                    DateTime ManualDateKPI;
                    try
                    {
                        ManualDateKPI = C.StringToDate(txtManualDateKPI.Text, calManualDateKPI.Format);
                        ManualDateKPI = DateAndTime.DateAdd(DateInterval.Year, -543, ManualDateKPI);
                        
                        KPI_End = ManualDateKPI;
                    }
                    catch (Exception e) { string msg = e.Message; }
                    break;
            }
            switch (ddlDateModeCOMP.SelectedIndex)
            {
                case 0:
                    break;
                case 1:
                    DateTime ManualDateCOMP;
                    try
                    {
                        ManualDateCOMP = C.StringToDate(txtManualDateCOMP.Text, calManualDateCOMP.Format);
                        ManualDateCOMP = DateAndTime.DateAdd(DateInterval.Year, -543, ManualDateCOMP);

                        COMP_End = ManualDateCOMP;
                    }
                    catch (Exception e) { string msg = e.Message; }
                    break;
            }



            //------------ Addd Group Record ----------
            DT.Columns.Add("New_Sector", typeof(Int32));
            DT.Columns.Add("KPI_Date", typeof(string));
            DT.Columns.Add("KPI_OK", typeof(Boolean));
            DT.Columns.Add("KPI_Late", typeof(Boolean));
            DT.Columns.Add("COMP_Date", typeof(string));
            DT.Columns.Add("COMP_OK", typeof(Boolean));
            DT.Columns.Add("COMP_Late", typeof(Boolean));
            
            string lastsector= "";
            for (int i = 0; i < DT.Rows.Count; i++)
            {
                if (DT.Rows[i]["SECTOR_CODE"].ToString() + DT.Rows[i]["SECTOR_NAME"].ToString() != lastsector)
                {
                    DT.Rows[i]["New_Sector"] = 1;
                    lastsector = DT.Rows[i]["SECTOR_CODE"].ToString() + DT.Rows[i]["SECTOR_NAME"].ToString();
                }
                else { DT.Rows[i]["New_Sector"] = 0; }

                //KPI
                DateTime LatedDate = DateAndTime.DateAdd(DateInterval.Day, 1, KPI_End);

                if (!GL.IsEqualNull(DT.Rows[i]["KPI_Commit_Time"]))
                {
                    DateTime KPIDate = (DateTime)DT.Rows[i]["KPI_Commit_Time"];
                    DT.Rows[i]["KPI_Date"] = KPIDate.ToString("dd-MMM-yy");
                    DT.Rows[i]["KPI_Late"] = KPIDate >= LatedDate;
                    DT.Rows[i]["KPI_OK"] = KPIDate < LatedDate;                    
                }
                else
                {
                    DT.Rows[i]["KPI_Date"] = "";
                    DT.Rows[i]["KPI_Late"] = DateTime.Now >= LatedDate;
                    DT.Rows[i]["KPI_OK"] = false;  
                }

                //COMP
                LatedDate = DateAndTime.DateAdd(DateInterval.Day, 1, COMP_End);
                if (!GL.IsEqualNull(DT.Rows[i]["COMP_Commit_Time"]))
                {
                    DateTime COMPDate = (DateTime)DT.Rows[i]["COMP_Commit_Time"];
                    DT.Rows[i]["COMP_Date"] = COMPDate.ToString("dd-MMM-yy");
                    DT.Rows[i]["COMP_Late"] = COMPDate >= LatedDate;
                    DT.Rows[i]["COMP_OK"] = COMPDate < LatedDate;
                }
                else
                {
                    DT.Rows[i]["COMP_Date"] = "";
                    DT.Rows[i]["COMP_Late"] = DateTime.Now >= LatedDate;
                    DT.Rows[i]["COMP_OK"] = false;  
                }
            }

            //-------------- Calculate Total ----------------
            ViewState["KPITotal"] = DT.Compute("COUNT(DEPT_NAME)","");
            ViewState["KPIOK"] = DT.Compute("COUNT(DEPT_NAME)", "KPI_OK=1");
            ViewState["KPILate"] = DT.Compute("COUNT(DEPT_NAME)", "KPI_Late=1");
            ViewState["COMPTotal"] = DT.Compute("COUNT(DEPT_NAME)", "");
            ViewState["COMPOK"] = DT.Compute("COUNT(DEPT_NAME)", "COMP_OK=1");
            ViewState["COMPLate"] = DT.Compute("COUNT(DEPT_NAME)", "COMP_Late=1");

            /*-------------------------------------*/
            Session["RPT_A_DEPTAssCompleted"] = DT;
            //--------- Title --------------
            Session["RPT_A_DEPTAssCompleted_Title"] = Title;
            //--------- Sub Title ----------
            string KPI_Title = KPI_End.ToString("dd-MMM-yy");
            string COMP_Title = COMP_End.ToString("dd-MMM-yy");
            
            Session["RPT_A_DEPTAssCompleted_SubTitle"] =" พบ " + GL.StringFormatNumber(DT.Rows.Count, 0) + "  รายการ"; ;
            Session["RPT_A_DEPTAssCompleted_SubTitle_KPI_Title"] = KPI_Title;
            Session["RPT_A_DEPTAssCompleted_SubTitle_COMP_Title"] = COMP_Title;

            Pager.SesssionSourceName = "RPT_A_DEPTAssCompleted";
            Pager.RenderLayout();

            lblKPI_Title.Text = KPI_Title;
            lblCOMP_Title.Text = COMP_Title;

            if (DT.Rows.Count == 0)
            {
                lblCountPSN.Text = Title + " ไม่พบรายการดังกล่าว";
            }
            else
            {
                lblCountPSN.Text = Title + " พบ " + GL.StringFormatNumber(DT.Rows.Count, 0) + " รายการ";
            }

        }

        protected void Pager_PageChanging(PageNavigation Sender)
        {
            Pager.TheRepeater = rptAll;
        }

        protected void rptAll_ItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
        {

            switch(e.Item.ItemType)
            { 
                case  ListItemType.AlternatingItem :
                case  ListItemType.Item :
                            Label lblSector=(Label)e.Item.FindControl("lblSector");
                            Label lblDept = (Label)e.Item.FindControl("lblDept");
                            Label lblKPIDate = (Label)e.Item.FindControl("lblKPIDate");
                            Image imgKPIOK = (Image)e.Item.FindControl("imgKPIOK");
                            Image imgKPILate = (Image)e.Item.FindControl("imgKPILate");
                            Label lblCOMPDate = (Label)e.Item.FindControl("lblCOMPDate");
                            Image imgCOMPOK = (Image)e.Item.FindControl("imgCOMPOK");
                            Image imgCOMPLate = (Image)e.Item.FindControl("imgCOMPLate");

                            DataRowView drv = (DataRowView)e.Item.DataItem;

                            DateTime KPI_End = (DateTime)drv["KPI_End"];
                            DateTime COMP_End = (DateTime)drv["COMP_End"];

                            if (drv["New_Sector"].ToString() == "1")
                            {
                                lblSector.Text = drv["SECTOR_NAME"].ToString();
                                for (int i = 1; i <= 8; i++)
                                {
                                    HtmlTableCell td = (HtmlTableCell)e.Item.FindControl("td" + i);
                                    td.Style["border-top-width"] = "2px";
                                    td.Style["border-top-color"] = "#999999";
                                }
                            }
                            else
                            {
                                HtmlTableCell td1 = (HtmlTableCell)e.Item.FindControl("td1");
                                td1.Style["border-top"] = "none";
                            }

                            lblDept.Text = drv["DEPT_NAME"].ToString();
                            lblKPIDate.Text = drv["KPI_Date"].ToString();
                            lblCOMPDate.Text = drv["COMP_Date"].ToString();
                            imgKPIOK.Visible = (Boolean)drv["KPI_OK"];
                            imgKPILate.Visible = (Boolean)drv["KPI_Late"];
                            imgCOMPOK.Visible = (Boolean)drv["COMP_OK"];
                            imgCOMPLate.Visible = (Boolean)drv["COMP_Late"];
                    break;
                case ListItemType.Footer:
                    if (Pager.CurrentPage != Pager.PageCount )
                    { e.Item.Visible = false; return; }
                    DataTable DT = (DataTable)(rptAll.DataSource);
                    if (DT.Rows.Count == 0)
                    { e.Item.Visible = false; return; }

                    Label lblKPITotal = (Label)e.Item.FindControl("lblKPITotal");
                    Label lblKPIOK = (Label)e.Item.FindControl("lblKPIOK");
                    Label lblKPILate = (Label)e.Item.FindControl("lblKPILate");
                    Label lblCOMPTotal = (Label)e.Item.FindControl("lblCOMPTotal");
                    Label lblCOMPOK = (Label)e.Item.FindControl("lblCOMPOK");
                    Label lblCOMPLate = (Label)e.Item.FindControl("lblCOMPLate");

                    lblKPITotal.Text = Convert.ToInt32(ViewState["KPITotal"]).ToString("#,#");
                    lblKPIOK.Text = Convert.ToInt32(ViewState["KPIOK"]).ToString("#,#");
                    lblKPILate.Text = Convert.ToInt32(ViewState["KPILate"]).ToString("#,#");
                    lblCOMPTotal.Text = Convert.ToInt32(ViewState["COMPTotal"]).ToString("#,#");
                    lblCOMPOK.Text = Convert.ToInt32(ViewState["COMPOK"]).ToString("#,#");
                    lblCOMPLate.Text = Convert.ToInt32(ViewState["COMPLate"]).ToString("#,#");
                    break;
            }
        }

        public RPT_A_DEPTAssCompleted()
		{
			Load += Page_Load;
		}

    }
}
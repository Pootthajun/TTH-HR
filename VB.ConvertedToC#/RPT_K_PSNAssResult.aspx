﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="RPT_K_PSNAssResult.aspx.cs" Inherits="VB.RPT_K_PSNAssResult" %>

<%@ Register src="WUC_DateReporter.ascx" tagname="WUC_DateReporter" tagprefix="uc1" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<%@ Register src="PageNavigation.ascx" tagname="PageNavigation" tagprefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<asp:ToolkitScriptManager ID="toolkit1" runat="server"></asp:ToolkitScriptManager>
<asp:UpdatePanel ID="UpdatePanel1" runat="server">
<ContentTemplate>

<div class="container-fluid">
				<!-- BEGIN PAGE HEADER-->
				<div class="row-fluid" id="fixedPageHeader">
					<div class="span12"> 
						<!-- BEGIN PAGE TITLE & BREADCRUMB-->
						<h3>จำนวนตัวชี้วัดของส่วนงาน<font color="blue"> (ScreenID : R-KPI-08)</font></h3>						
						<ul class="breadcrumb">
                            <li>
                            	<i class="icon-copy"></i> <a href="javascript:;">รายงาน</a><i class="icon-angle-right"></i>
                            </li>
                            <li>
                            	<i class="icon-copy"></i> <a href="javascript:;">การประเมินผลตัวชี้วัด</a><i class="icon-angle-right"></i>
                            </li>
                            <li>
                            	<i class="icon-copy"></i> <a href="javascript:;">รายงานประจำรอบ</a><i class="icon-angle-right"></i>
                            </li>
                        	<li>
                        	    <i class="icon-check"></i> <a href="javascript:;">จำนวนตัวชี้วัดของส่วนงาน</a>
                        	</li>                        	
                            <uc1:WUC_DateReporter ID="WUC_DateReporter1" runat="server" />
                        </ul>
						<!-- END PAGE TITLE & BREADCRUMB-->									        
				     </div>				
			    </div>
		        <!-- END PAGE HEADER-->
				
                <asp:Panel ID="pnlList" runat="server" Visible="True" DefaultButton="btnSearch">
				<div class="row-fluid">
					
					<div class="span12">
						<!-- BEGIN SAMPLE TABLE PORTLET-->
                                        <div class="row-fluid form-horizontal">
                			                  <div class="btn-group pull-left">
                                                    <asp:Button ID="btnSearch" OnClick="btnSearch_Click"  runat="server" CssClass="btn green" Text="ค้นหา" />
                                              </div> 
				                            <div class="btn-group pull-right">                                    
								                <button data-toggle="dropdown" class="btn dropdown-toggle" id="Button1">พิมพ์ <i class="icon-angle-down"></i></button>										
								                <ul class="dropdown-menu pull-right">                                                       
                                                    <li><a href="Print/RPT_K_PSNAssResult.aspx?Mode=PDF" target="_blank">รูปแบบ PDF</a></li>
                                                    <li><a href="Print/RPT_K_PSNAssResult.aspx?Mode=EXCEL" target="_blank">รูปแบบ Excel</a></li>											
										        </ul>
							                </div>
									     </div>      
						                 <div class="row-fluid form-horizontal">
						                             <div class="span4 ">
														<div class="control-group">
													        <label class="control-label"> รอบการประเมิน</label>
													        <div class="controls">
														        <asp:DropDownList ID="ddlRound" runat="server" CssClass="medium m-wrap">
							                                    </asp:DropDownList>
													        </div>
												        </div>
													</div>
													
													<%--  <div class="span4 ">
														<div class="control-group">
													        <label class="control-label"> ชื่อ</label>
													        <div class="controls">
														        <asp:TextBox ID="txtName" runat="server" CssClass="m-wrap medium" placeholder="ค้นหาจากชื่อ/เลขประจำตัว"></asp:TextBox>
													        </div>
												        </div>
													</div>	--%>					   
												</div>
												
												<div class="row-fluid form-horizontal">
											         <div class="span4 ">
												        <div class="control-group">
											                <label class="control-label"> ฝ่าย</label>
											                <div class="controls">
												                <asp:DropDownList ID="ddlSector" OnSelectedIndexChanged="ddlSector_SelectedIndexChanged" runat="server" AutoPostBack="true" CssClass="medium m-wrap">
												                </asp:DropDownList>
											                </div>
										                </div>
											        </div>	
    												
											         <div class="span3 ">
												        <div class="control-group">
											                <label class="control-label"> หน่วยงาน</label>
											                <div class="controls">
												                <asp:DropDownList ID="ddlDept" runat="server" CssClass="medium m-wrap">
												                </asp:DropDownList>		
											                </div>
										                </div>
											        </div>		
											     </div>
											     
											     
									     
								            
							<div class="portlet-body no-more-tables">
								<asp:Label ID="lblCountPSN" runat="server" Font-Size="14px" Font-Bold="true"></asp:Label>
								<table class="table table-full-width table-bordered dataTable table-hover">
									<thead>
										<tr>
											<th style="text-align:center;" rowspan="2"> หน่วยงาน</th>
                                            <th style="text-align:center;" rowspan="2"> ชื่อ</th>
                                            <th style="text-align:center;" colspan="5"> ระดับค่าเป้าหมาย</th>
                                            <th style="text-align:center;" rowspan="2"> รวมจำนวน KPI (ข้อ)</th>																		
											<th style="text-align:center;" rowspan="2"> พิมพ์/รายละเอียด</th>											
										</tr>
                                        <tr>
                                            <th style="text-align:center;">1</th>
                                            <th style="text-align:center;">2</th>
                                            <th style="text-align:center;">3</th>
                                            <th style="text-align:center;">4</th>
                                            <th style="text-align:center;">5</th>
                                        </tr>
									</thead>
									<tbody>
										<asp:Repeater ID="rptKPI" OnItemDataBound="rptKPI_ItemDataBound" runat="server">
									        <ItemTemplate>
									            <tr>    
									                <td data-title="หน่วยงาน" id="td1" runat="server"><asp:Label ID="lblOrganize" runat="server"></asp:Label></td>
                                                    <td data-title="ชื่อ" id="td2" runat="server"><asp:Label ID="lblPSNName" runat="server"></asp:Label></td>
                                                    <td data-title="ระดับค่าเป้าหมาย 1 คะแนน" id="td3" runat="server" style="text-align:center;"><asp:Label ID="lblKPI1" runat="server"></asp:Label></td>
                                                    <td data-title="ระดับค่าเป้าหมาย 2 คะแนน" id="td4" runat="server" style="text-align:center;"><asp:Label ID="lblKPI2" runat="server"></asp:Label></td>
                                                    <td data-title="ระดับค่าเป้าหมาย 3 คะแนน" id="td5" runat="server" style="text-align:center;"><asp:Label ID="lblKPI3" runat="server"></asp:Label></td>
                                                    <td data-title="ระดับค่าเป้าหมาย 4 คะแนน" id="td6" runat="server" style="text-align:center;"><asp:Label ID="lblKPI4" runat="server"></asp:Label></td>
                                                    <td data-title="ระดับค่าเป้าหมาย 5 คะแนน" id="td7" runat="server" style="text-align:center;"><asp:Label ID="lblKPI5" runat="server"></asp:Label></td>									                
											        <td data-title="รวมจำนวน KPI (ข้อ)" id="td8" runat="server" style="text-align:center;"><asp:Label ID="lblNumKPI" runat="server"></asp:Label></td>
											        <td data-title="พิมพ์ " id="td9" runat="server" style="text-align:center; ">   
                                                       <a class="btn mini blue" id="btnPrint" runat="server" title="พิมพ์" style="cursor:pointer;" target="_blank"><i class="icon-print"></i></a> 
                                                    </td>
										        </tr>
                                                <tr id="trSumDept" runat="server" visible="false">
                                                    <td data-title="หน่วยงาน" style="border-top:none;">&nbsp;</td>
                                                    <td data-title="รวม" style="font-weight:bold;">รวมจำนวนตัวชี้วัดของ <asp:Label ID="lblSUMDeptName" runat="server"></asp:Label></td>
                                                    <td data-title="ระดับค่าเป้าหมาย 1 คะแนน" style="text-align:center; font-weight:bold;"><asp:Label ID="lblCountKPI1" runat="server"></asp:Label></td>
                                                    <td data-title="ระดับค่าเป้าหมาย 2 คะแนน" style="text-align:center; font-weight:bold;"><asp:Label ID="lblCountKPI2" runat="server"></asp:Label></td>
                                                    <td data-title="ระดับค่าเป้าหมาย 3 คะแนน" style="text-align:center; font-weight:bold;"><asp:Label ID="lblCountKPI3" runat="server"></asp:Label></td>
                                                    <td data-title="ระดับค่าเป้าหมาย 4 คะแนน" style="text-align:center; font-weight:bold;"><asp:Label ID="lblCountKPI4" runat="server"></asp:Label></td>
                                                    <td data-title="ระดับค่าเป้าหมาย 5 คะแนน" style="text-align:center; font-weight:bold;"><asp:Label ID="lblCountKPI5" runat="server"></asp:Label></td>									                
											        <td data-title="รวมจำนวน KPI (ข้อ)" style="text-align:center;"><asp:Label ID="lblCountKPI" runat="server" Font-Bold="True"></asp:Label></td>
											        <td data-title="" style="text-align:center; ">&nbsp;</td>
                                                </tr>
                                                <tr id="trAvgDept" runat="server" visible="false">
                                                    <td data-title="หน่วยงาน" style="border-top:none;">&nbsp;</td>
                                                    <td data-title="รวม" style="font-weight:bold;">ร้อยละตัวชี้วัดแต่ละข้อเท่ากับ</td>
                                                    <td data-title="ระดับค่าเป้าหมาย 1 คะแนน" style="text-align:center; font-weight:bold;"><asp:Label ID="lblAVGKPI1" runat="server"></asp:Label></td>
                                                    <td data-title="ระดับค่าเป้าหมาย 2 คะแนน" style="text-align:center; font-weight:bold;"><asp:Label ID="lblAVGKPI2" runat="server"></asp:Label></td>
                                                    <td data-title="ระดับค่าเป้าหมาย 3 คะแนน" style="text-align:center; font-weight:bold;"><asp:Label ID="lblAVGKPI3" runat="server"></asp:Label></td>
                                                    <td data-title="ระดับค่าเป้าหมาย 4 คะแนน" style="text-align:center; font-weight:bold;"><asp:Label ID="lblAVGKPI4" runat="server"></asp:Label></td>
                                                    <td data-title="ระดับค่าเป้าหมาย 5 คะแนน" style="text-align:center; font-weight:bold;"><asp:Label ID="lblAVGKPI5" runat="server"></asp:Label></td>									                
											        <td data-title="" style="text-align:center;">&nbsp;</td>
											        <td data-title="" style="text-align:center;">&nbsp;</td>
                                                </tr>

                                                <tr id="trSumSector" runat="server" visible="false">
                                                    <td data-title="รวมฝ่าย" colspan="2" style="font-weight:bold;">รวมจำนวนตัวชี้วัดของ <asp:Label ID="lblSUMSectorName" runat="server"></asp:Label></td>
                                                    <td data-title="ระดับค่าเป้าหมาย 1 คะแนน" style="text-align:center; font-weight:bold;"><asp:Label ID="lblCountSector1" runat="server"></asp:Label></td>
                                                    <td data-title="ระดับค่าเป้าหมาย 2 คะแนน" style="text-align:center; font-weight:bold;"><asp:Label ID="lblCountSector2" runat="server"></asp:Label></td>
                                                    <td data-title="ระดับค่าเป้าหมาย 3 คะแนน" style="text-align:center; font-weight:bold;"><asp:Label ID="lblCountSector3" runat="server"></asp:Label></td>
                                                    <td data-title="ระดับค่าเป้าหมาย 4 คะแนน" style="text-align:center; font-weight:bold;"><asp:Label ID="lblCountSector4" runat="server"></asp:Label></td>
                                                    <td data-title="ระดับค่าเป้าหมาย 5 คะแนน" style="text-align:center; font-weight:bold;"><asp:Label ID="lblCountSector5" runat="server"></asp:Label></td>									                
											        <td data-title="รวมจำนวน KPI (ข้อ)" style="text-align:center;"><asp:Label ID="lblCountSector" runat="server" Font-Bold="True"></asp:Label></td>
											        <td data-title="" style="text-align:center; ">&nbsp;</td>
                                                </tr>
                                                <tr id="trAvgSector" runat="server" visible="false">
                                                    <td data-title="เฉลี่ยฝ่าย" colspan="2"  style="font-weight:bold;">ร้อยละตัวชี้วัดแต่ละข้อเท่ากับ</td>
                                                    <td data-title="ระดับค่าเป้าหมาย 1 คะแนน" style="text-align:center; font-weight:bold;"><asp:Label ID="lblAVGSector1" runat="server"></asp:Label></td>
                                                    <td data-title="ระดับค่าเป้าหมาย 2 คะแนน" style="text-align:center; font-weight:bold;"><asp:Label ID="lblAVGSector2" runat="server"></asp:Label></td>
                                                    <td data-title="ระดับค่าเป้าหมาย 3 คะแนน" style="text-align:center; font-weight:bold;"><asp:Label ID="lblAVGSector3" runat="server"></asp:Label></td>
                                                    <td data-title="ระดับค่าเป้าหมาย 4 คะแนน" style="text-align:center; font-weight:bold;"><asp:Label ID="lblAVGSector4" runat="server"></asp:Label></td>
                                                    <td data-title="ระดับค่าเป้าหมาย 5 คะแนน" style="text-align:center; font-weight:bold;"><asp:Label ID="lblAVGSector5" runat="server"></asp:Label></td>									                
											        <td data-title="" style="text-align:center;">&nbsp;</td>
											        <td data-title="" style="text-align:center;">&nbsp;</td>
                                                </tr>
									        </ItemTemplate>
										</asp:Repeater>																							
									</tbody>
								</table>
								<asp:PageNavigation ID="Pager" OnPageChanging="Pager_PageChanging" MaximunPageCount="10" PageSize="20" runat="server" />
							</div>
					</div>
                </div>  
                </asp:Panel>  
</div>

</ContentTemplate>
</asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ScriptPlaceHolder" runat="server">
</asp:Content>

using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Xml.Linq;
using System.Diagnostics;
using System.Collections.Specialized;
using System.Configuration;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Caching;
using System.Web.SessionState;
using System.Web.Security;
using System.Web.Profile;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
namespace VB
{
	public partial class PageNavigation : System.Web.UI.UserControl
	{

		public event PageChangedEventHandler PageChanged;
		public delegate void PageChangedEventHandler(PageNavigation Sender);
		public event PageChangingEventHandler PageChanging;
		public delegate void PageChangingEventHandler(PageNavigation Sender);

        GenericLib GL=new GenericLib();

		public Repeater TheRepeater;
		public int MaximunPageCount {
			get { return GL.CINT( btnFirst.Attributes["MaximunPageCount"].ToString()); }
			set { btnFirst.Attributes["MaximunPageCount"] = value.ToString(); }
		}

		public int PageSize {
			//-------------1
			get { return GL.CINT( btnFirst.Attributes["PageSize"].ToString()); }
			set { btnFirst.Attributes["PageSize"] = value.ToString(); }
		}

		public string SesssionSourceName {
			//-------------2
			get { return btnFirst.Attributes["SourceName"]; }
			set { btnFirst.Attributes["SourceName"] = value; }
		}

		public DataTable Datasource {
			//-------------3
			get {
				if (string.IsNullOrEmpty(SesssionSourceName))
					return null;
                return (DataTable)Session[SesssionSourceName];
			}
		}

		public int PageCount {
			//-------------4
			get {
				if ((Datasource == null))
					return 0;
				DataTable Source = Datasource.Copy();
            	return GL.CINT(Math.Ceiling(GL.CDBL( Source.Rows.Count) / PageSize));
			}
		}

		public int CurrentPage {
			//-------------4
			get { return GL.CINT(btnFirst.Attributes["CurrentPage"].ToString()); }
			set {
				if (value > PageCount) {
					value = PageCount;
				}
				if (value <= 0) {
					value = 0;
				}
				btnFirst.Attributes["CurrentPage"] = value.ToString();
				RenderLayout();

			}
		}

		protected void rptPage_ItemCommand(object source, System.Web.UI.WebControls.RepeaterCommandEventArgs e)
		{
			if (e.Item.ItemType != ListItemType.AlternatingItem & e.Item.ItemType != ListItemType.Item)
				return;
            LinkButton btnPage = (LinkButton)e.Item.FindControl("btnPage");
			CurrentPage =GL.CINT( btnPage.Text.ToString());
		}

		protected void rptPage_ItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
		{
			if (e.Item.ItemType != ListItemType.AlternatingItem & e.Item.ItemType != ListItemType.Item)
				return;

            DataRowView drv = (DataRowView)e.Item.DataItem;
            LinkButton btnPage = (LinkButton)e.Item.FindControl("btnPage");
			btnPage.Text = drv["P"].ToString();
			if (drv["P"].ToString() == CurrentPage.ToString()) {
				btnPage.CssClass = "number current";
			} else {
				btnPage.CssClass = "number";
			}
		}

		protected void btnFirst_Click(object sender, System.EventArgs e)
		{
			CurrentPage = 1;
		}

		protected void btnBack_Click(object sender, System.EventArgs e)
		{
			CurrentPage -= 1;
		}

		protected void btnNext_Click(object sender, System.EventArgs e)
		{
			CurrentPage += 1;
		}

		protected void btnLast_Click(object sender, System.EventArgs e)
		{
			CurrentPage = PageCount;
		}


		public void RenderLayout()
		{
			if (PageChanging != null) {
				PageChanging(this);
			}

			//----------------- Set Pager---------------------
			if ((Datasource == null) || Datasource.Rows.Count == 0) {
				this.Visible = false;
				if ((TheRepeater != null)) {
					TheRepeater.DataSource = null;
					TheRepeater.DataBind();
				}
				return;
			}

			DataTable Source = Datasource.Copy();

			int TotalRecord = Source.Rows.Count;
			if (CurrentPage == 0) {
				CurrentPage = 1;
				return;
			} else if (CurrentPage > PageCount) {
				CurrentPage = PageCount;
				return;
			}

			int StartIndex = (CurrentPage - 1) * PageSize;
			int EndIndex = StartIndex + PageSize - 1;
			if (EndIndex > TotalRecord - 1 | TotalRecord == 0)
				EndIndex = TotalRecord - 1;
			//-------------- Set Display Record-----------

			DataTable NewSource = Source.Copy();
			NewSource.Rows.Clear();

			if (StartIndex < 0)
				StartIndex = 0;
			for (int i = StartIndex; i <= EndIndex; i++) {
				DataRow DR = NewSource.NewRow();
				DR.ItemArray = Source.Rows[i].ItemArray;
				NewSource.Rows.Add(DR);
			}



			//-----------Render Page Number------------
			int TotalGroup = GL.CINT(Math.Ceiling( GL.CDBL( PageCount )/ GL.CDBL(MaximunPageCount)));
			int ThisGroup = GL.CINT(Math.Ceiling(GL.CDBL( CurrentPage) / GL.CDBL(MaximunPageCount)));

			StartIndex = ((ThisGroup - 1) * MaximunPageCount) + 1;
			EndIndex = StartIndex + MaximunPageCount - 1;
			if (EndIndex > PageCount)
				EndIndex = PageCount;
			DataTable DT = new DataTable();
			DT.Columns.Add("P");
			for (int i = StartIndex; i <= EndIndex; i++) {
				DataRow DR = DT.NewRow();
				DR["P"] = i;
				DT.Rows.Add(DR);
			}
			rptPage.DataSource = DT;
			rptPage.DataBind();

			//-----------End Page Number------------

			btnNext.Visible = CurrentPage < PageCount;
			btnBack.Visible = CurrentPage > 1;
			btnFirst.Visible = CurrentPage != 1;
			btnLast.Visible = CurrentPage != PageCount;

			if ((TheRepeater == null))
				return;
			TheRepeater.DataSource = NewSource;
			TheRepeater.DataBind();

			if (PageChanged != null) {
				PageChanged(this);
			}

			this.Visible = PageCount > 1;

		}

	}
}

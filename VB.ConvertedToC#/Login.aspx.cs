using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Xml.Linq;
using System.Diagnostics;
using System.Collections.Specialized;
using System.Configuration;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Caching;
using System.Web.SessionState;
using System.Web.Security;
using System.Web.Profile;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.DirectoryServices;

namespace VB
{

	public partial class Login : System.Web.UI.Page
	{

		HRBL BL = new HRBL();

		GenericLib GL = new GenericLib();

		protected void btnLogin_Click(object sender, System.EventArgs e)
		{
			if (txtUserName.Text == "admin" & txtPassword.Text == "1qaz@WSX") {
				//---------------- Save Log--------------------
				BL.KeepLoginLog("0");
				Session["USER_PSNL_NO"] = "0";
				Session["USER_Full_Name"] = "Betimes";
				Session["USER_Is_PSN"] = false;
				Session["USER_Is_Admin"] = true;
				Session["USER_Is_Manager"] = false;
				Session["USER_Is_GAP_Editor"] = true;
				//-----Career Path------------------
				Session["USER_Is_Admin_CareerPath"] = true;
				Session["USER_Is_Setting_Property_POS"] = true;
				Session["USER_Is_View_PSN"] = true;
				Session["USER_Is_Manager_CareerPath"] = false;

                Session["USER_Is_Workload_Admin"] = true;
                Session["USER_Is_Workload_User"] = false;

				Response.Redirect("Overview.aspx");
				return;
			}

            if ((bool)BL.Login_With_TTM_Credential())
            {
                string Msg = "";
                if (!GL.AuthenticateUser(BL.SYSTEM_DOMAIN(), txtUserName.Text, txtPassword.Text, BL.LDAP_PATH(), ref Msg))
                {
                    // ก็อก 2 ลองใช้ฟังก์ชั่นเก่า ---------------
                    if (!GL.AuthenticateUser_Old(BL.SYSTEM_DOMAIN(), txtUserName.Text, txtPassword.Text, BL.LDAP_PATH(), ref Msg))
                    {
                        ScriptManager.RegisterStartupScript(this.Page, typeof(string), "Alert", "showAlert('ไม่สามารถเข้าสู่ระบบได้<br><br>" + Msg.Replace("'", "\\'").Replace("\n", "<br>").Replace(Constants.vbCr, "") + "<br>กรุณาตรวจสอบสิทธิ์การ Login เข้าสู่ระบบ " + BL.SYSTEM_DOMAIN() + " กับเจ้าหน้าที่อีกครั้ง');", true);
                        return;
                    }
                }
                if (!string.IsNullOrEmpty(Msg))
                {
                    ScriptManager.RegisterStartupScript(this.Page, typeof(string), "Alert", "showAlert('ไม่สามารถเข้าสู่ระบบได้<br><br>" + Msg.Replace("'", "\\'").Replace("\n", "<br>").Replace(Constants.vbCr, "") + "');", true);
                    return;
                }
            }

			//------------ For Test ----------------
			if (txtUserName.Text == "workload" & txtPassword.Text == "aaaaaaaa") {
				//---------------- Save Log--------------------
				BL.KeepLoginLog("0");
				Session["USER_PSNL_NO"] = "0";
				Session["USER_Full_Name"] = "Betimes";
				Session["USER_Is_PSN"] = false;
				Session["USER_Is_Admin"] = true;
				Session["USER_Is_Manager"] = false;
				Session["USER_Is_GAP_Editor"] = true;
				//-----Career Path------------------
				Session["USER_Is_Admin_CareerPath"] = true;
				Session["USER_Is_Setting_Property_POS"] = true;
				Session["USER_Is_View_PSN"] = true;
				Session["USER_Is_Manager_CareerPath"] = false;

                Session["USER_Is_Workload_Admin"] = true;
                Session["USER_Is_Workload_User"] = false;

				Response.Redirect("Overview.aspx");
				return;
			}

            string SQL = " SELECT PSN.PSNL_NO,PSNL_Fullname,DEPT_CODE,PSN.PNPS_CLASS,MGR_Name,FN_Name,HR_Password,Is_Admin,IS_GAP_Editor,Is_View_PSN,Is_Property_Pos,Is_Allow,Is_WKL_Admin,Is_WKL_User" + "\n";
			SQL += " FROM vw_PN_PSNL PSN" + "\n";
			SQL += " INNER JOIN tb_HR_User U ON PSN.PSNL_NO=U.PSNL_NO" + "\n";
			SQL += " WHERE HR_User_Name='" + Strings.Replace(txtUserName.Text, "'", "''") + "'" + "\n";
			SqlDataAdapter DA = new SqlDataAdapter(SQL, BL.ConnectionString());
			DataTable DT = new DataTable();
            DA.Fill(DT);
           
			if (DT.Rows.Count == 0) {
				ScriptManager.RegisterStartupScript(this.Page, typeof(string), "Alert", "showAlert('ไม่พบชื่อผู้ใช้ดังกล่าว<br>กรุณาติดต่อฝ่ายสารสนเทศ>');", true);
				return;
			}

            if (!BL.Login_With_TTM_Credential() & DT.Rows[0]["HR_Password"].ToString() != txtPassword.Text)
            {
                ScriptManager.RegisterStartupScript(this.Page, typeof(string), "Alert", "showAlert('รหัสผ่านไม่ถูกต้อง');", true);
                return;
                //---------------- Update Password (For LDAP Authen)-------------
            }
            else if (BL.Login_With_TTM_Credential() && (DT.Rows[0]["HR_Password"].ToString() != txtPassword.Text))
            {
                //-------------- Update Password-------------
                SqlCommand Comm = new SqlCommand();
                SqlConnection Conn = new SqlConnection(BL.ConnectionString());
                Conn.Open();
                var _with1 = Comm;
                _with1.CommandType = CommandType.Text;
                _with1.Connection = Conn;
                _with1.CommandText = "UPDATE tb_HR_User SET HR_Password='" + txtPassword.Text.Replace("'", "''") + "' WHERE HR_User_Name='" + txtUserName.Text.Replace("'", "''") + "'";
                _with1.ExecuteNonQuery();
                _with1.Dispose();
                Conn.Close();
            }

            if (GL.IsEqualNull(DT.Rows[0]["Is_Allow"]) || !Convert.ToBoolean(DT.Rows[0]["Is_Allow"]))
            {
				ScriptManager.RegisterStartupScript(this.Page, typeof(string), "Alert", "showAlert('ไม่สามารถเข้าสู่ระบบได้<br>กรุณาติดต่อฝ่ายทรัพยากรบุคคล');", true);
				return;
			}

			Session["USER_PSNL_NO"] = DT.Rows[0]["PSNL_NO"];
			Session["USER_Full_Name"] = DT.Rows[0]["PSNL_Fullname"];
            Session["USER_DEPT_CODE"] = DT.Rows[0]["DEPT_CODE"];
            Session["USER_Position"] = "";
            if (!GL.IsEqualNull(DT.Rows[0]["MGR_Name"]))
            {
                Session["USER_Position"] = DT.Rows[0]["MGR_Name"];
            }
            else if (!GL.IsEqualNull(DT.Rows[0]["FN_Name"]))
            {
                Session["USER_Position"] = DT.Rows[0]["FN_Name"];
            }
           			//------------------ Determine is Employee----------------
			Session["USER_Is_PSN"] = DT.Rows[0]["PNPS_CLASS"].ToString() != "14";
			//------------------ Determine... Is Admin--------------------
			Session["USER_Is_Admin"] = !GL.IsEqualNull(DT.Rows[0]["Is_Admin"]) && Convert.ToBoolean(DT.Rows[0]["Is_Admin"]);

			Session["USER_Is_GAP_Editor"] = !GL.IsEqualNull(DT.Rows[0]["Is_GAP_Editor"]) && Convert.ToBoolean(DT.Rows[0]["Is_GAP_Editor"]);
           
            //-----Workoload------------------
            Session["USER_Is_Workload_Admin"] = !GL.IsEqualNull(DT.Rows[0]["Is_WKL_Admin"]) && Convert.ToBoolean(DT.Rows[0]["Is_WKL_Admin"]);
            Session["USER_Is_Workload_User"] = !GL.IsEqualNull(DT.Rows[0]["Is_WKL_User"]) && Convert.ToBoolean(DT.Rows[0]["Is_WKL_User"]);

            //-----Career Path------------------
            Session["USER_Is_Admin_CareerPath"] = (!GL.IsEqualNull(DT.Rows[0]["Is_Property_Pos"]) && Convert.ToBoolean(DT.Rows[0]["Is_Property_Pos"])) && !GL.IsEqualNull(DT.Rows[0]["Is_View_PSN"]) && Convert.ToBoolean(DT.Rows[0]["Is_View_PSN"]); ;
            Session["USER_Is_View_PSN"] = !GL.IsEqualNull(DT.Rows[0]["Is_View_PSN"]) && Convert.ToBoolean(DT.Rows[0]["Is_View_PSN"]);
            Session["USER_Is_Setting_Property_POS"] = !GL.IsEqualNull(DT.Rows[0]["Is_Property_Pos"]) && Convert.ToBoolean(DT.Rows[0]["Is_Property_Pos"]);

			//---------------- Save Log--------------------
			BL.KeepLoginLog(Session["USER_PSNL_NO"].ToString());

			//------------------ Determine... Is Manager--------------------
			//SQL = " DECLARE @MGR_CODE As nvarchar(6)='" & txtUserName.Text.Replace("'", "''") & "'" & vbLf
			SQL = " SELECT * FROM vw_HR_ASSESSOR_New" + "\n";
			DA = new SqlDataAdapter(SQL, BL.ConnectionString());
			DT = new DataTable();
			DA.Fill(DT);
			//------------------ Filter Condition Assessment Still InCompleted --------------------------
            //// comment 20161101  เนื่องจากปิดรอบการประเมินหมดแล้วทำให้หัวหน้า login ไปไม่เจอเมนูประเมินพนักงาน  ดังนั้นแก้ไขให้สามารถดูข้อมูลประวัติได้
            //DT.DefaultView.RowFilter = "R_Completed=0 AND MGR_PSNL_NO='" + txtUserName.Text.Replace("'", "''") + "'";
            DT.DefaultView.RowFilter = " MGR_PSNL_NO='" + txtUserName.Text.Replace("'", "''") + "'";


			Session["USER_Is_Manager"] = DT.DefaultView.Count > 0;

            //SQL = "SELECT * FROM vw_PN_PSNL WHERE PSNL_NO='" + Session["USER_PSNL_NO"] + "'";
            //DA = new SqlDataAdapter(SQL, BL.ConnectionString());
            //DT = new DataTable();
            //DA.Fill(DT);

			

			Response.Redirect("Overview.aspx");

		}

		protected void Page_Load(object sender, System.EventArgs e)
		{
			if (!IsPostBack) {
				txtPassword.Attributes["onchange"] = "document.getElementById('" + txtStorePassword.ClientID + "').value=this.value;";
				//pnlTTMCredential.Visible = BL.Login_With_TTM_Credential
			} else {
				ScriptManager.RegisterStartupScript(this.Page, typeof(string), "RestorePassword", "document.getElementById('" + txtPassword.ClientID + "').value=document.getElementById('" + txtStorePassword.ClientID + "').value;", true);
			}
		}

		public Login()
		{
			Load += Page_Load;
		}

	}
}

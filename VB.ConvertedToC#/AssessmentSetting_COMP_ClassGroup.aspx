﻿<%@ Page  Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPage.Master" CodeBehind="AssessmentSetting_COMP_ClassGroup.aspx.cs" Inherits="VB.AssessmentSetting_COMP_ClassGroup" %>

<%@ Register src="WUC_DateReporter.ascx" tagname="WUC_DateReporter" tagprefix="uc1" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<asp:UpdatePanel ID="UpdatePanel1" runat="server">
<ContentTemplate>
<asp:ToolkitScriptManager ID="toolkit1" runat="server"></asp:ToolkitScriptManager>

<div class="container-fluid">
				<!-- BEGIN PAGE HEADER-->
				<div class="row-fluid" id="fixedPageHeader">
					<div class="span12"> 
						<!-- BEGIN PAGE TITLE & BREADCRUMB-->
						<h3 class="page-title">กลุ่มระดับ <font color="blue">(ScreenID : S-HDR-04)</font></h3>
						
									
						<ul class="breadcrumb">
                           
                        	<li>
                        	    <i class="icon-th-list"></i> <a href="javascript:;">การกำหนดข้อมูลสมรรถนะ</a><i class="icon-angle-right"></i>
                        	</li>
                        	<li>
                        	    <i class="icon-list-ol"></i> <a href="javascript:;">กลุ่มระดับ</a></i>
                        	</li>
                        	
                            <uc1:WUC_DateReporter ID="WUC_DateReporter1" runat="server" />
                        </ul>
						<!-- END PAGE TITLE & BREADCRUMB-->
									        
				     </div>

				
			    </div>
		        <!-- END PAGE HEADER-->
				
			
  
                                        <div class="portlet-body form">
												
											<!-- BEGIN FORM-->
											<div class="form-horizontal form-view">
												           
												<h3 class="form-section"><i class="icon-list-ol"></i> สำหรับรอบการประเมิน 
												    <asp:DropDownList ID="ddlRound" OnSelectedIndexChanged="ddlRound_SelectedIndexChanged" runat="server" AutoPostBack="true" CssClass="medium m-wrap header" style="font-size:20px;">
					                                </asp:DropDownList>
												</h3>
												<div class="row-fluid">
													<div class="span12 ">
														<div class="control-group">
															<label class="control-label">&nbsp;</label>
															
															    <div class="controls">
															    <div class="span3">
																    <div class="portlet-body no-more-tables">
								                                        <table class="table table-full-width dataTable no-more-tables table-hover">
									                                        <thead>
										                                        <tr>
											                                        <th style="text-align:center;"><i class="icon-list-ol"></i> กลุ่มระดับ</th>
											                                        <th style="text-align:center;"><i class="icon-list-ol"></i> ถึง</th>
											                                        <th style="text-align:center;"><i class="icon-bolt"></i> ลบ</th>											                                        
										                                        </tr>
									                                        </thead>
									                                        <tbody>
									                                            <asp:Repeater ID="rptClass" OnItemCommand="rptClass_ItemCommand" OnItemDataBound="rptClass_ItemDataBound" runat="server">
									                                                <ItemTemplate>
									                                                    <tr>
											                                                <td data-title="กลุ่มระดับ"><asp:TextBox ID="txtStart" runat="server" CssClass="m-wrap small"></asp:TextBox></td>
											                                                <td data-title="ถึง"><asp:TextBox ID="txtEnd" runat="server" CssClass="m-wrap small"></asp:TextBox></td>
											                                                <td data-title="ดำเนินการ"><asp:Button ID="btnDelete" runat="server" Text="ลบ" CssClass="btn red mini" CommandName="Delete" /></td>
										                                                </tr>
									                                                </ItemTemplate>									                                                
									                                            </asp:Repeater>						
									                                        </tbody>
								                                        </table>
							                                        </div>
															    </div>
															</div>
														</div>
													</div>
												</div>
												
												<asp:Panel ID="pnlAction" runat="server" CssClass="form-actions">
												    <asp:Button ID="btnAdd" OnClick="btnAdd_Click" CssClass="btn purple" runat="server" Text="เพิ่มกลุ่มระดับ" />
												    <asp:Button ID="btnSave" OnClick="btnSave_Click" CssClass="btn blue" runat="server" Text="ยืนยันบันทึก" />
												    <asp:ConfirmButtonExtender ID="cfmbtnSave" TargetControlID="btnSave" ConfirmText="ยืนยันบันทึก" runat="server" ></asp:ConfirmButtonExtender>
												</asp:Panel>
											</div>
											<!-- END FORM-->  
										</div>
         
                             
              			
</div>
</ContentTemplate>           
</asp:UpdatePanel>
</asp:Content>



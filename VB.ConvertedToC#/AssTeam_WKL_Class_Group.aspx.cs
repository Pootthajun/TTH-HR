using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Xml.Linq;
using System.Diagnostics;
using System.Collections.Specialized;
using System.Configuration;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Caching;
using System.Web.SessionState;
using System.Web.Security;
using System.Web.Profile;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
namespace VB
{

	public partial class AssTeam_WKL_Class_Group : System.Web.UI.Page
	{


		HRBL BL = new HRBL();
		GenericLib GL = new GenericLib();

		textControlLib TC = new textControlLib();
		public string DEPT_CODE {
			get {
				try {
					return GL.SplitString(ddlOrganize.Items[ddlOrganize.SelectedIndex].Value, "-")[0];
				} catch (Exception ex) {
					return "";
				}
			}
		}

		public string MINOR_CODE {
			get {
				try {
					return GL.SplitString(ddlOrganize.Items[ddlOrganize.SelectedIndex].Value, "-")[1];
				} catch (Exception ex) {
					return "";
				}
			}
		}


		public string PSNL_NO {
			get {
				try {
					return Session["USER_PSNL_NO"].ToString ();
				} catch {
					return "";
				}
			}
		}

		protected void Page_Load(object sender, System.EventArgs e)
		{
			if ((Session["USER_PSNL_NO"] == null)) {
				ScriptManager.RegisterStartupScript(this.Page, typeof(string), "Alert", "showAlert('กรุณาลงชื่อเข้าใช้ระบบ');", true);
				Response.Redirect("Login.aspx");
				return;
			}

            if (!Convert.ToBoolean(Session["USER_Is_Workload_Admin"]))
            {
                ScriptManager.RegisterStartupScript(this.Page, typeof(string), "Alert", "showAlert('คุณไม่มีสิทธิ์เข้าถึงข้อมูลส่วนนี้');", true);
                Response.Redirect("Overview.aspx");
                return;
            }
           

			if (!IsPostBack) {
				DataTable DT = BL.GetAllAssessmentRound();

                DT.DefaultView.Sort = " R_Year DESC , R_Round DESC";
                
				if (DT.Rows.Count > 0) {
                    BL.BindDDlTeamDepartment(ddlOrganize, GL.CINT(DT.DefaultView[0]["R_Year"]), GL.CINT(DT.DefaultView[0]["R_Round"]), PSNL_NO.ToString());
				}

				if (ddlOrganize.Items.Count < 1) {
				}
				try {
					ddlOrganize.Items.RemoveAt(0);
					ddlOrganize.SelectedIndex = 0;
				} catch (Exception ex) {
				}
				BindClass();
			}

		}

		protected void ddl_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			BindClass();
		}


		private void BindClass()
		{
			string SQL = "SELECT DEPT_CODE,MINOR_CODE,Class_ID,PNPO_CLASS_Start,PNPO_CLASS_End\n";
			SQL += " FROM tb_HR_Job_Mapping_Class\n";
			SQL += " WHERE DEPT_CODE='" + DEPT_CODE.Replace("'", "''") + "' AND MINOR_CODE='" + MINOR_CODE.Replace("'", "''") + "' \n";
			SQL += " ORDER BY Class_ID \n";
			SqlDataAdapter DA = new SqlDataAdapter(SQL, BL.ConnectionString());
			DataTable DT = new DataTable();
			DA.Fill(DT);

			//----------- ใส่ Default ให้ กรณีที่ ยังไม่ได้ตั้งค่าระดับ --------------
			if (DT.Rows.Count == 0) {
				BL.SetJobMappingAutoClass(DEPT_CODE, MINOR_CODE);
				DT = new DataTable();
				DA = new SqlDataAdapter(SQL, BL.ConnectionString());
				DA.Fill(DT);
			}

			rptClass.DataSource = DT;
			rptClass.DataBind();
			pnlAction.Visible = !string.IsNullOrEmpty(ddlOrganize.Text);

		}

		public void rptClass_ItemCommand(object source, System.Web.UI.WebControls.RepeaterCommandEventArgs e)
		{
			switch (e.CommandName) {
				case "Delete":
					DataTable DT = CurrentClassData();
					DT.Rows.RemoveAt(e.Item.ItemIndex);
					rptClass.DataSource = DT;
					rptClass.DataBind();

					break;
				case "Insert":
					 DT = CurrentClassData();
					DataRow DR = DT.NewRow();
					DR["Class_ID"] = DT.Rows[e.Item.ItemIndex]["Class_ID"];
					for (int i = e.Item.ItemIndex; i <= DT.Rows.Count - 1; i++) {
                        int tmp = GL.CINT(DT.Rows[i]["Class_ID"]) + 1;
                        DT.Rows[i]["Class_ID"] = tmp;
					}

					DT.Rows.InsertAt(DR, e.Item.ItemIndex);
					rptClass.DataSource = DT;
					rptClass.DataBind();
					break;
			}
		}

		public void rptClass_ItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
		{
			if (e.Item.ItemType != ListItemType.Item & e.Item.ItemType != ListItemType.AlternatingItem)
				return;

			TextBox txtStart =(TextBox) e.Item.FindControl("txtStart");
			TextBox txtEnd =(TextBox) e.Item.FindControl("txtEnd");
			Button btnDelete =(Button) e.Item.FindControl("btnDelete");
            Button btnInsert = (Button)e.Item.FindControl("btnInsert");
            DataRowView drv = (DataRowView)e.Item.DataItem;

			if (!GL.IsEqualNull(drv["PNPO_CLASS_Start"])) {
				txtStart.Text = GL.StringFormatNumber(drv["PNPO_CLASS_Start"], 0);
			}
			TC.ImplementJavaIntegerText(txtStart);
			txtStart.Style["text-align"] = "center";

			if (!GL.IsEqualNull(drv["PNPO_CLASS_End"])) {
				txtEnd.Text = GL.StringFormatNumber(drv["PNPO_CLASS_End"], 0);
			}
			TC.ImplementJavaIntegerText(txtEnd);
			txtEnd.Style["text-align"] = "center";

            btnDelete.CommandArgument = drv["Class_ID"].ToString();
            btnInsert.CommandArgument = drv["Class_ID"].ToString();
		}

		public DataTable CurrentClassData()
		{
			DataTable DT = new DataTable();
			DT.Columns.Add("Class_ID", typeof(int));
			DT.Columns.Add("PNPO_CLASS_Start", typeof(int));
			DT.Columns.Add("PNPO_CLASS_End", typeof(int));
			foreach (RepeaterItem Item in rptClass.Items) {
				if (Item.ItemType != ListItemType.Item & Item.ItemType != ListItemType.AlternatingItem)
					continue;
				TextBox txtStart =(TextBox) Item.FindControl("txtStart");
				TextBox txtEnd = (TextBox) Item.FindControl("txtEnd");
				Button btnDelete =(Button) Item.FindControl("btnDelete");
				DataRow DR = DT.NewRow();
				DR["Class_ID"] = btnDelete.CommandArgument;
				if (!string.IsNullOrEmpty(txtStart.Text)) {
					DR["PNPO_CLASS_Start"] = txtStart.Text;
				}
				if (!string.IsNullOrEmpty(txtEnd.Text)) {
					DR["PNPO_CLASS_End"] = txtEnd.Text;
				}
				DT.Rows.Add(DR);
			}
			return DT;
		}

		protected void btnAdd_Click(object sender, System.EventArgs e)
		{
			DataTable DT = CurrentClassData();
			DataRow DR = DT.NewRow();

			object NewClassID = DT.Compute("MAX(Class_ID)", "");
			if (GL.IsEqualNull(NewClassID))
				NewClassID = 0;
			DR["Class_ID"] = GL.CINT(NewClassID) + 1;
			DT.Rows.Add(DR);
			rptClass.DataSource = DT;
			rptClass.DataBind();
		}


		protected void btnSave_Click(object sender, System.EventArgs e)
		{
			DataTable DT = CurrentClassData();
			if (DT.Rows.Count == 0) {
				ScriptManager.RegisterStartupScript(this.Page, typeof(string), "Alert", "showAlert('กรอกข้อมูลให้สมบูรณ์');", true);
				return;
			}

			int mn = 1;
			int mx = 13;

			if (GL.IsEqualNull(DT.Rows[DT.Rows.Count - 1]["PNPO_CLASS_End"]) ||GL.CINT ( DT.Rows[DT.Rows.Count - 1]["PNPO_CLASS_End"]) > mx) {
				rptClass.Items[DT.Rows.Count - 1].FindControl("txtEnd").Focus();
				ScriptManager.RegisterStartupScript(this.Page, typeof(string), "Alert", "showAlert('ระดับสูงสุดคือ " + mx + "');", true);
				return;
			}

			//If GL.IsEqualNull(DT.Rows(0).Item("PNPO_CLASS_Start")) OrElse DT.Rows(0).Item("PNPO_CLASS_Start") <> mn Then
			//    rptClass.Items(0).FindControl("txtStart").Focus()
			//    ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "showAlert('ระดับต่ำสุดคือ " & mn & "');", True)
			//    Exit Sub
			//End If

			for (int i = 0; i <= DT.Rows.Count - 1; i++) {
				if (GL.IsEqualNull(DT.Rows[i]["PNPO_CLASS_Start"])) {
					rptClass.Items[i].FindControl("txtStart").Focus();
					ScriptManager.RegisterStartupScript(this.Page, typeof(string), "Alert", "showAlert('กรอกช่วงระดับให้สมบูรณ์');", true);
					return;
				}
				if (GL.IsEqualNull(DT.Rows[i]["PNPO_CLASS_End"])) {
					rptClass.Items[i].FindControl("txtEnd").Focus();
					ScriptManager.RegisterStartupScript(this.Page, typeof(string), "Alert", "showAlert('กรอกช่วงระดับให้สมบูรณ์');", true);
					return;
				}

				if (i > 0 && GL.CINT(DT.Rows[i]["PNPO_CLASS_Start"]) != GL.CINT(DT.Rows[i - 1]["PNPO_CLASS_End"]) + 1) {
					rptClass.Items[i].FindControl("txtStart").Focus();
					ScriptManager.RegisterStartupScript(this.Page, typeof(string), "Alert", "showAlert('การไล่ระดับไม่ถูกต้อง กรุณาตรวจสอบอีกครั้ง');", true);
					return;
				}

				if (GL.CINT(DT.Rows[i]["PNPO_CLASS_End"]) < GL.CINT(DT.Rows[i]["PNPO_CLASS_Start"])) {
					rptClass.Items[i].FindControl("txtEnd").Focus();
					ScriptManager.RegisterStartupScript(this.Page, typeof(string), "Alert", "showAlert('การไล่ระดับไม่ถูกต้อง กรุณาตรวจสอบอีกครั้ง');", true);
					return;
				}
			}

			//------------- จัดรูปแบบ DT ------------
			DT.Columns.Add("S", typeof(string));
			DT.Columns.Add("E", typeof(string));
			for (int i = 0; i <= DT.Rows.Count - 1; i++) {
				DT.Rows[i]["S"] = DT.Rows[i]["PNPO_CLASS_Start"].ToString().PadLeft(2, GL.chr0);
				DT.Rows[i]["E"] = DT.Rows[i]["PNPO_CLASS_END"].ToString().PadLeft(2, GL.chr0);
			}
			DT.Columns.Remove("PNPO_CLASS_Start");
			DT.Columns.Remove("PNPO_CLASS_END");
			DT.Columns["S"].ColumnName = "PNPO_CLASS_Start";
			DT.Columns["E"].ColumnName = "PNPO_CLASS_END";

			//----------------- ตรวจสอบว่ามี บุคคลากรอยู่ในตาราง tb_HR_Workload_PSN หรือเปล่า ----------------
			string SQL = "";
			SQL = " DECLARE @DEPT_CODE As nvarchar(50)='" + DEPT_CODE.Replace("'", "''") + "'\n";
			SQL += " DECLARE @MINOR_CODE AS nvarchar(50)='" + MINOR_CODE.Replace("'", "''") + "'\n";
			SQL += " SELECT PSN_ID,WKL.DEPT_CODE,WKL.MINOR_CODE,WKL.Class_ID,Slot_No,Slot_Name,REF_PSNL_NO\n";
			SQL += " ,PSN.PNPS_CLASS,CLS.PNPO_CLASS_Start CLASS_Start,CLS.PNPO_CLASS_End CLASS_End\n";
			SQL += " FROM tb_HR_Workload_PSN WKL\n";

			SQL += " LEFT JOIN tb_PN_PERSONAL_VIEW PSN ON WKL.REF_PSNL_NO=PSN.PNPS_PSNL_NO\n";
			SQL += " LEFT JOIN tb_HR_Job_Mapping_Class CLS ON CLS.DEPT_CODE=@DEPT_CODE AND CLS.MINOR_CODE=@MINOR_CODE\n";
			SQL += " \t\t\t\t\t\t\t\t\tAND CLS.Class_ID=WKL.Class_ID \n";
			SQL += " WHERE WKL.DEPT_CODE=@DEPT_CODE AND WKL.MINOR_CODE=@MINOR_CODE\n";
			DataTable Slot = new DataTable();
			SqlDataAdapter DA = new SqlDataAdapter(SQL, BL.ConnectionString());
			DA.Fill(Slot);

			SqlCommandBuilder cmd = new SqlCommandBuilder();
			string RndDept = DEPT_CODE + DateAndTime.Now.ToOADate().ToString().Replace(".", "");
			//---------------ถ้ามี ----------------

			if (Slot.Rows.Count > 0) {


				//--------------พยายามหา Class ให้คนเดิมๆ เพื่อเตรียมย้ายเข้าสู่กลุ่มใหม่------------
				for (int i = 0; i <= Slot.Rows.Count - 1; i++) {
					//--------- เป็น Slot ที่ Add ใหม่ ----------
					if (GL.IsEqualNull(Slot.Rows[i]["PNPS_CLASS"])) {
						//--------------- ตรวจสอบว่ามี (x) อยู่หรือไม่ ----------------
                        string SlotName = Slot.Rows[i]["Slot_Name"].ToString();
						if (SlotName.LastIndexOf("(") < SlotName.LastIndexOf(")") & SlotName.LastIndexOf("(") != -1) {
							string _SlotClass = SlotName.Substring(SlotName.LastIndexOf("(") + 1, SlotName.LastIndexOf(")") - SlotName.LastIndexOf("(") - 1);
							if (Information.IsNumeric(_SlotClass)) {
								Slot.Rows[i]["PNPS_CLASS"] = _SlotClass.PadLeft(2, GL.chr0);
							}
						} else {
							//Dim FloorClass As String = Math.Floor((CInt(Slot.Rows(i).Item("CLASS_Start")) + CInt(Slot.Rows(i).Item("CLASS_END"))) / 2).ToString().PadLeft(2, GL.chr0)
                            double cls = Math.Ceiling((GL.CDBL(Slot.Rows[i]["CLASS_Start"]) + GL.CDBL((Slot.Rows[i]["CLASS_END"]))) / 2);
                            string CeilingClass = cls.ToString().PadLeft(2, GL.chr0);
							Slot.Rows[i]["PNPS_CLASS"] = CeilingClass;
							
						}
					}
				}

				//--------------- จะมีบางคนที่หา Class อยู่ไม่ได้ ให้ลบออกจาก Slot ทั้งหมด------------------------------------
				Slot.DefaultView.RowFilter = "PNPS_CLASS IS NULL";

				SqlConnection _Conn = new SqlConnection(BL.ConnectionString());
				SqlCommand _Comm = new SqlCommand();
				for (int i = Slot.DefaultView.Count - 1; i >= 0; i += -1) {
					_Conn.Open();
					var _with1 = _Comm;
					_with1.Connection = _Conn;
					_with1.CommandType = CommandType.Text;
					_with1.CommandText = "DELETE FROM tb_HR_Workload_Slot WHERE PSN_ID=" + Slot.DefaultView[i]["PSN_ID"] + "\n";
					_with1.CommandText += "DELETE FROM tb_HR_Workload_PSN WHERE PSN_ID=" + Slot.DefaultView[i]["PSN_ID"];
					_with1.ExecuteNonQuery();
					_with1.Dispose();
					_Conn.Close();
					Slot.DefaultView[i].Row.Delete();
					Slot.AcceptChanges();
				}
				Slot.DefaultView.RowFilter = "";

				//-------------ถ้ายังมีคนอยู่---------------
				if (Slot.Rows.Count > 0) {
					//----------------- สร้าง DEPT ชั่วคราวเพื่อเตรียมเอาพนักงานไปฝากไว้ก่อน -------------------
					DataTable dpt = new DataTable();
					DA = new SqlDataAdapter("SELECT * FROM tb_HR_Job_Mapping_Class", BL.ConnectionString());
					DA.Fill(dpt);
					DataRow DR = dpt.NewRow();
					DR["DEPT_CODE"] = RndDept;
					DR["MINOR_CODE"] = "00";
					DR["Class_ID"] = 0;
					DR["PNPO_CLASS_Start"] = "00";
					DR["PNPO_CLASS_End"] = "00";
					dpt.Rows.Add(DR);
					cmd = new SqlCommandBuilder(DA);
					DA.Update(dpt);
					//----------------ย้ายคนเดิมไปไว้ DEPT ชั่วคราว โดย Update DEPT_CODE ในตาราง tb_HR_Workload_PSN-----------------
					_Comm = new SqlCommand();
					_Conn = new SqlConnection(BL.ConnectionString());
					_Conn.Open();
					var _with2 = _Comm;
					_with2.Connection = _Conn;
					_with2.CommandType = CommandType.Text;
					_with2.CommandText = "UPDATE tb_HR_Workload_PSN\n";
					_with2.CommandText += "SET DEPT_CODE='" + RndDept.Replace("'", "''") + "',MINOR_CODE='00',Class_ID=0\n";
					_with2.CommandText += "WHERE DEPT_CODE='" + DEPT_CODE.Replace("'", "''") + "'\n";
					_with2.ExecuteNonQuery();
					_with2.Dispose();
					_Conn.Close();
				}
			}

			//-----------------ลบ Class เก่า ------------------------
			SqlConnection Conn = new SqlConnection(BL.ConnectionString());
			Conn.Open();
			SqlCommand Comm = new SqlCommand();
			var _with3 = Comm;
			_with3.Connection = Conn;
			_with3.CommandType = CommandType.Text;
			_with3.CommandText = "DELETE FROM tb_HR_Job_Mapping_Class WHERE DEPT_CODE='" + DEPT_CODE.Replace("'", "''") + "' AND MINOR_CODE='" + MINOR_CODE.Replace("'", "''") + "'";
			_with3.ExecuteNonQuery();
			_with3.Dispose();
			Conn.Close();

			//-----------------เพิ่ม Class ใหม่ -----------------------
			SQL = "SELECT *\n";
			SQL += " FROM tb_HR_Job_Mapping_Class \n";
			SQL += " WHERE DEPT_CODE='" + DEPT_CODE.Replace("'", "''") + "' AND MINOR_CODE='" + MINOR_CODE.Replace("'", "''") + "'\n";
			SqlDataAdapter CA = new SqlDataAdapter(SQL, Conn);
			DataTable CT = new DataTable();
			CA.Fill(CT);
			for (int i = 0; i <= DT.Rows.Count - 1; i++) {
				DataRow CR = CT.NewRow();
				CR["DEPT_CODE"] = DEPT_CODE;
				CR["MINOR_CODE"] = MINOR_CODE;
				CR["Class_ID"] = i + 1;
				CR["PNPO_CLASS_Start"] = DT.Rows[i]["PNPO_CLASS_Start"];
				CR["PNPO_CLASS_End"] = DT.Rows[i]["PNPO_CLASS_End"];
				CR["Update_By"] = PSNL_NO;
				CR["Update_Time"] = DateAndTime.Now;
				CT.Rows.Add(CR);
			}
			//

			cmd = new SqlCommandBuilder(CA);
			CA.Update(CT);
			Conn.Close();
			DT = CT.Copy();

			//----------------- Update Class ใหม่ให้พนักงานเดิม --------
			if (Slot.Rows.Count > 0) {
				Conn = new SqlConnection(BL.ConnectionString());
				Conn.Open();
				Comm = new SqlCommand();
				var _with4 = Comm;
				_with4.Connection = Conn;
				_with4.CommandType = CommandType.Text;


				for (int i = 0; i <= Slot.Rows.Count - 1; i++) {
					DT.DefaultView.RowFilter = "'" + Slot.Rows[i]["PNPS_CLASS"] + "'>=PNPO_CLASS_Start AND '" + Slot.Rows[i]["PNPS_CLASS"] + "'<=PNPO_CLASS_End";
					if (DT.DefaultView.Count > 0) {
						_with4.CommandText = "UPDATE tb_HR_Workload_PSN\n";
						_with4.CommandText += "SET DEPT_CODE='" + DEPT_CODE.Replace("'", "''") + "',MINOR_CODE='" + MINOR_CODE.Replace("'", "''") + "',Class_ID=" + DT.DefaultView[0]["Class_ID"] + "\n";
						_with4.CommandText += "WHERE PSN_ID=" + Slot.Rows[i]["PSN_ID"] + "\n";
					} else {
						_with4.CommandText = "DELETE FROM tb_HR_Workload_Slot WHERE PSN_ID=" + Slot.Rows[i]["PSN_ID"] + "\n";
						_with4.CommandText += "DELETE FROM tb_HR_Workload_PSN WHERE PSN_ID=" + Slot.Rows[i]["PSN_ID"] + "\n";
					}
					_with4.ExecuteNonQuery();
				}

				//----------------- ลบ Slot ที่ยังค้าง-------------------
				_with4.CommandText = "DELETE FROM tb_HR_Workload_Slot WHERE PSN_ID IN (SELECT PSN_ID FROM tb_HR_Workload_PSN WHERE DEPT_CODE='" + RndDept.Replace("'", "''") + "' AND MINOR_CODE='00')\n";
				//----------------- ลบ PSN ที่ยังค้าง-------------------
				_with4.CommandText += "DELETE FROM tb_HR_Workload_PSN WHERE DEPT_CODE='" + RndDept.Replace("'", "''") + "' AND MINOR_CODE='00'\n";
				//----------------- ลบ DEPT ชั่วคราว-------------------
				_with4.CommandText += "DELETE FROM tb_HR_Job_Mapping_Class WHERE DEPT_CODE='" + RndDept.Replace("'", "''") + "' AND MINOR_CODE='00'";
				_with4.ExecuteNonQuery();
				_with4.Dispose();
				Conn.Close();
			}






			ScriptManager.RegisterStartupScript(this.Page, typeof(string), "alert", "showAlert('บันทึกสำเร็จ');", true);
			BindClass();
		}
		public AssTeam_WKL_Class_Group()
		{
			Load += Page_Load;
		}
	}
}

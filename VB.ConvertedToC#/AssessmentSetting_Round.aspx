﻿<%@ Page  Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPage.Master" CodeBehind="AssessmentSetting_Round.aspx.cs" Inherits="VB.AssessmentSetting_Round" %>

<%@ Register src="WUC_DateReporter.ascx" tagname="WUC_DateReporter" tagprefix="uc1" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<%@ Register src="WUC_DateTimePicker.ascx" tagname="WUC_DateTimePicker" tagprefix="asp" %>
<%@ Register src="PageNavigation.ascx" tagname="PageNavigation" tagprefix="uc2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<asp:ToolkitScriptManager ID="toolkit1" runat="server"></asp:ToolkitScriptManager>

<asp:UpdatePanel ID="UpdatePanel1" runat="server">      
<ContentTemplate>
<div class="container-fluid">
				<!-- BEGIN PAGE HEADER-->
				<div class="row-fluid" id="fixedPageHeader">
					<div class="span12"> 
						<!-- BEGIN PAGE TITLE & BREADCRUMB-->
						<h3 class="page-title">รอบการประเมิน <font color="blue">(ScreenID : S-HDR-01)</font></h3>
						
									
						<ul class="breadcrumb">
                            <li>
                            	<i class="icon-th-list"></i><a href="javascript:;">การกำหนดข้อมูลการประเมิน</a><i class="icon-angle-right"></i>
                            </li>
                        	<li><i class="icon-retweet"></i> <a href="javascript:;">รอบการประเมิน</a></li>
                        	
                            <uc1:WUC_DateReporter ID="WUC_DateReporter1" runat="server" />
                        </ul>
						<!-- END PAGE TITLE & BREADCRUMB-->
									        
				     </div>

				
			    </div>
		        <!-- END PAGE HEADER-->
				
		<asp:Panel ID="pnlList" runat="server" Visible="True">
			<div class="row-fluid">
					
					<div class="span12">
						<!-- BEGIN SAMPLE TABLE PORTLET-->
						<div class="portlet box">
							<div class="portlet-title" style="color:Black !important">
								<div class="caption"><i class="icon-retweet">
                                    </i>รอบการประเมินทั้งหมด</div>
								<div class="tools"></div>
							</div>
							<div class="portlet-body no-more-tables">
								<table class="table table-full-width no-more-tables table-hover">
									<thead>
										<tr>
											<th rowspan="3"><i class="icon-retweet"></i> ประจำปี</th>
											<th rowspan="3"><i class=" icon-retweet"></i> ครั้งที่</th>
											<th rowspan="2" colspan="2"><i class="icon-time"></i> ช่วงการประเมิน</th>
											<th colspan="4" style="text-align:center;"><i class="icon-th-list"></i> การประเมินผลตัวชี้วัด</th>
											<th colspan="4" style="text-align:center;"><i class="icon-th-list"></i> การประเมินผลสมรรถนะ</th>
											<th rowspan="3"><i class="icon-bolt"></i> สถานะ/ดำเนินการ</th>
											
										</tr>
										
										<tr>
										 	<th colspan="2" style="text-align:center;"><i class="icon-edit"></i> กำหนดแบบประเมิน</th>
											<th colspan="2" style="text-align:center;"><i class="icon-check"></i> ประเมินผล</th>
											<th colspan="2" style="text-align:center;"><i class="icon-edit"></i> กำหนดแบบประเมิน</th>
											<th colspan="2" style="text-align:center;"><i class="icon-check"></i> ประเมินผล</th>										
										</tr>
										
										<tr>											
											<th><i class="icon-time"></i> เริ่ม</th>
											<th><i class="icon-time"></i> ถึง</th>
											<th><i class="icon-time"></i> เริ่ม</th>
											<th><i class="icon-time"></i> ถึง</th>
											<th><i class="icon-time"></i> เริ่ม</th>
											<th><i class="icon-time"></i> ถึง</th>
											<th><i class="icon-time"></i> เริ่ม</th>
											<th><i class="icon-time"></i> ถึง</th>
											<th><i class="icon-time"></i> เริ่ม</th>
											<th><i class="icon-time"></i> ถึง</th>
											
										</tr>
									</thead>
									<tbody>
										<asp:Repeater ID="rptRound" OnItemCommand="rptRound_ItemCommand" OnItemDataBound="rptRound_ItemDataBound" runat="server">
										    <ItemTemplate>
										                <tr>
											                <td class="highlight" data-title="ประจำปี">
												                <div class="success"></div>
												                <a href="javascript;;"><asp:Label ID="lbl_R_Year" runat="server"></asp:Label></a>
											                </td>
											                <td data-title="ครั้งที่"><asp:Label ID="lbl_R_Round" runat="server"></asp:Label></td>
											                <td data-title="ช่วงการประเมินทั้งรอบ"><asp:Label ID="lbl_R_Start" runat="server"></asp:Label></td>
											                <td data-title="ช่วงการประเมินทั้งรอบ"><asp:Label ID="lbl_R_End" runat="server"></asp:Label></td>
											                <td data-title="กำหนด KPI"><asp:Label ID="lbl_KPI_CP_Start" runat="server"></asp:Label></td>											
											                <td data-title="กำหนด KPI"><asp:Label ID="lbl_KPI_CP_End" runat="server"></asp:Label></td>
											                <td data-title="ประเมิน KPI"><asp:Label ID="lbl_KPI_AP_Start" runat="server"></asp:Label></td>
											                <td data-title="ประเมิน KPI"><asp:Label ID="lbl_KPI_AP_End" runat="server"></asp:Label></td>
											                <td data-title="กำหนด Competency"><asp:Label ID="lbl_COMP_CP_Start" runat="server"></asp:Label></td>
											                <td data-title="กำหนด Competency"><asp:Label ID="lbl_COMP_CP_End" runat="server"></asp:Label></td>
											                <td data-title="ประเมิน Competency"><asp:Label ID="lbl_COMP_AP_Start" runat="server"></asp:Label></td>
											                <td data-title="ประเมิน Competency"><asp:Label ID="lbl_COMP_AP_End" runat="server"></asp:Label></td>
											                <td data-title="สถานะ/ดำเนินการ">											                
											                    <%--<a class="btn mini green" id="lnkComplete" runat="server" href="javascript:;"><i class="icon-check"></i> เสร็จสมบูรณ์</a>--%>
                                                                <asp:LinkButton id="lnkComplete" CommandName="Complete" runat="server" CssClass="btn mini green"><i class="icon-check"></i> เสร็จสมบูรณ์</asp:LinkButton>
                                                                <asp:LinkButton id="lnkDetail" CommandName="Edit" runat="server" CssClass="btn mini blue"><i class="icon-info-sign"></i> รายละเอียด</asp:LinkButton>
											                    <asp:LinkButton id="lnkCloseRound" CommandName="ToggleClose" runat="server" CssClass="btn mini"><i class="icon-check"></i> ปิดรอบ</asp:LinkButton>
											                    <asp:LinkButton id="lnkDeleteRound" CommandName="Delete" runat="server" CssClass="btn mini red"><i class="icon-check"></i> ลบรอบ</asp:LinkButton>
											                    <asp:ConfirmButtonExtender ID="cfm_Close" runat="server" ConfirmText="ยืนยันปิดรอบ" TargetControlID="lnkCloseRound"></asp:ConfirmButtonExtender>
											                    <asp:ConfirmButtonExtender ID="cfm_Delete" runat="server" ConfirmText="ยืนยันลบรอบ" TargetControlID="lnkDeleteRound"></asp:ConfirmButtonExtender>
											                </td>                											
										                </tr>
										    </ItemTemplate>
										</asp:Repeater>
									</tbody>
								</table>
							</div>
							
							<asp:LinkButton id="lnkAddRound" OnClick="lnkAddRound_Click" runat="server" CssClass="btn blue"><i class="icon-plus"></i> สร้างรอบใหม่</asp:LinkButton>
						    <uc2:PageNavigation ID="PageNavigation1" OnPageChanging="Pager_PageChanging" runat="server" />
						</div>
						<!-- END SAMPLE TABLE PORTLET-->
						
					</div>
             </div>
              
         </asp:Panel>
              <asp:Panel ID="pnlEdit" runat="server" Visible="False">
              
            <div class="row-fluid">
                <div class="span12">
                  <div class="portlet-body form">
                    <asp:Panel ID="pnlComplete" runat="server">

												<h3 class="form-section"><i class="icon-retweet"></i> <asp:Label ID="lblEditMode" runat="server" Text="สร้าง/แก้ไข"></asp:Label> รอบการประเมิน</h3>
												<div class="row-fluid form-horizontal">
													<div class="span6 ">
														<div class="control-group">
															<label class="control-label">ประจำปี</label>
															<div class="controls">
																<asp:DropDownList ID="ddlEditYear" runat="server" CssClass="small m-wrap" placeholder="เลือกปี" >															        
														        </asp:DropDownList>
															</div>
														</div>
													</div>													
												</div>
												<div class="row-fluid form-horizontal">
												    <div class="span6 ">
														<div class="control-group">
													        <label class="control-label">รอบที่</label>
													        <div class="controls">
														       <asp:TextBox ID="txtEditRound" runat="server" class="m-wrap small" placeholder="กรอกเลขรอบ"></asp:TextBox>
													        </div>
												        </div>
													</div>
												</div>
												<h3 class="form-section"><i class="icon-time"></i> กำหนดช่วงรอบวันที่</h3>
												<div class="row-fluid form-horizontal">
												    <div class="span6 ">
														<div class="control-group">
													        <label class="control-label">วันที่</label>
													        <div class="controls">
														        <asp:WUC_DateTimePicker ID="dtp_R_Start" runat="server" />
													        </div>
												        </div>
													</div>
												</div>
												<div class="row-fluid form-horizontal">
												    <div class="span6 ">
														<div class="control-group">
													        <label class="control-label">ถึงวันที่</label>
													        <div class="controls">
														        <asp:WUC_DateTimePicker ID="dtp_R_End" runat="server" />
													        </div>
												        </div>
													</div>
												</div>																								
												<h3 class="form-section"><i class="icon-time"></i> ช่วงการประเมินผลตัวชี้วัด</h3>
												<div class="row-fluid form-horizontal">
												    <div class="span4 ">
														<div class="control-group">
													        <label class="control-label">กำหนดแบบประเมินวันที่</label>
													        <div class="controls">														        
														        <asp:WUC_DateTimePicker ID="dtp_KPI_CP_Start" runat="server" />
													        </div>
												        </div>
													</div>
													<div class="span4 ">
														<div class="control-group">
													        <label class="control-label">ถึงวันที่</label>
													        <div class="controls">
														        <asp:WUC_DateTimePicker ID="dtp_KPI_CP_End" runat="server" />
													        </div>
												        </div>
													</div>
												</div>
												<div class="row-fluid form-horizontal">
												    <div class="span4 ">
														<div class="control-group">
													        <label class="control-label">พนักงานประเมินตนเองวันที่</label>
													        <div class="controls">
														        <asp:WUC_DateTimePicker ID="dtp_KPI_AP_Start" runat="server" />
													        </div>
												        </div>
													</div>
													<div class="span4">
														<div class="control-group">
													        <label class="control-label">ถึงวันที่</label>
													        <div class="controls">
														        <asp:WUC_DateTimePicker ID="dtp_KPI_AP_End" runat="server" />
													        </div>
												        </div>
													</div>
												</div>
												<div class="row-fluid form-horizontal">
												    <div class="span4 ">
														<div class="control-group">
													        <label class="control-label">ผู้บังคับบัญชาประเมินวันที่</label>
													        <div class="controls">
														        <asp:WUC_DateTimePicker ID="dtp_KPI_MGR_Start" runat="server" />
													        </div>
												        </div>
													</div>
													<div class="span4 ">
														<div class="control-group">
													        <label class="control-label">ถึงวันที่</label>
													        <div class="controls">
														        <asp:WUC_DateTimePicker ID="dtp_KPI_MGR_End" runat="server" />
													        </div>
												        </div>
													</div>
												</div>
												
												<h3 class="form-section"><i class="icon-time"></i> ช่วงการประเมินสมรรถนะ</h3>
												<div class="row-fluid form-horizontal">
												    <div class="span4 ">
														<div class="control-group">
													        <label class="control-label">กำหนดแบบประเมินวันที่</label>
													        <div class="controls">
														        <asp:WUC_DateTimePicker ID="dtp_COMP_CP_Start" runat="server" />
													        </div>
												        </div>
													</div>
													<div class="span4 ">
														<div class="control-group">
													        <label class="control-label">ถึงวันที่</label>
													        <div class="controls">
														        <asp:WUC_DateTimePicker ID="dtp_COMP_CP_End" runat="server" />
													        </div>
												        </div>
													</div>
												</div>
												<div class="row-fluid form-horizontal">
												    <div class="span4 ">
														<div class="control-group">
													        <label class="control-label">พนักงานประเมินตนเองวันที่</label>
													        <div class="controls">
														        <asp:WUC_DateTimePicker ID="dtp_COMP_AP_Start" runat="server" />
													        </div>
												        </div>
													</div>
													<div class="span4 ">
														<div class="control-group">
													        <label class="control-label">ถึงวันที่</label>
													        <div class="controls">
														        <asp:WUC_DateTimePicker ID="dtp_COMP_AP_End" runat="server" /> 
													        </div>
												        </div>
													</div>
												</div>
												<div class="row-fluid form-horizontal">
												    <div class="span4 ">
														<div class="control-group">
													        <label class="control-label">ผู้บังคับบัญชาประเมินวันที่</label>
													        <div class="controls">
														        <asp:WUC_DateTimePicker ID="dtp_COMP_MGR_Start" runat="server" />
													        </div>
												        </div>
													</div>
													<div class="span4 ">
														<div class="control-group">
													        <label class="control-label">ถึงวันที่</label>
													        <div class="controls">
														        <asp:WUC_DateTimePicker ID="dtp_COMP_MGR_End" runat="server" />
													        </div>
												        </div>
													</div>
												</div>
										
												    <h3 class="form-section"><i class="icon-bookmark"></i> ระดับพนักงานที่ประเมินในรอบนี้</h3>
												    <div class="row-fluid form-horizontal">
												        <div class="span6 ">
														    <div class="control-group">
													            <div class="controls">
													                <asp:Repeater ID="rptLevel" OnItemDataBound="rptLevel_ItemDataBound" runat="server">
													                <ItemTemplate>
													                    <asp:CheckBox ID="chkLevel" runat="server" /> <asp:Label ID ="lbl" runat="server"></asp:Label><br>
													                </ItemTemplate>													            
													                </asp:Repeater>			
														       </div>
												            </div>
													    </div>
												    </div>
												
												
												
												    <h3 class="form-section"><i class="icon-bookmark"></i> ประเภทพนักงานที่ประเมินในรอบนี้</h3>
												    <div class="row-fluid form-horizontal">
												        <div class="span6 ">
														    <div class="control-group">
													            <div class="controls">
													                <asp:Repeater ID="rptType" OnItemDataBound="rptType_ItemDataBound" runat="server">
													                <ItemTemplate>
													                     
													                       <asp:CheckBox ID="chkType" runat="server" /> <asp:Label ID ="lbl" runat="server"></asp:Label><br>
													                 
													                </ItemTemplate>													            
													                </asp:Repeater>			
														       </div>
												            </div>
													    </div>
												    </div>
												
												
												
												    <h3 class="form-section"><i class="icon-adjust"></i> กำหนดน้ำหนักการประเมิน</h3>
												    <div class="row-fluid form-horizontal">
												        <div class="span3 ">
														    <div class="control-group">
													            <label class="control-label">การประเมินตัวชี้วัดผลงาน</label>
													            <div class="controls">
														            <asp:TextBox CssClass="m-wrap small" style="text-align:center; width:50px !important; " ID="txtWeightKPI" runat="server" placeholder="น้ำหนัก"/></span>
														        </div>
												            </div>
													    </div>
													    <div class="span6 ">
														    <div class="control-group">
													            <label class="control-label">การประเมินสมรรถนะ</label>
													            <div class="controls">
														            <asp:TextBox CssClass="m-wrap small" style="text-align:center; width:50px !important;" ID="txtWeightCOMP" runat="server" placeholder="น้ำหนัก"/></span>
														       </div>
												            </div>
													    </div>
												    </div>
												
												
                                            </asp:Panel> 												
																								
											<div class="form-actions">
											    <asp:Button CssClass="btn blue" runat="server" ID="btnSave" OnClick="btnSave_Click" Text="ยืนยันบันทึก" />
											    <asp:Button CssClass="btn" runat="server" ID="btnCancel" OnClick="btnCancel_Click" Text="ย้อนกลับ" />
											    <asp:ConfirmButtonExtender ID="cfmbtnSave" ConfirmText="ยืนยันบันทึก" TargetControlID="btnSave" runat="server" ></asp:ConfirmButtonExtender>
										    </div>		
										</div>
						</div>
            </div>


</asp:Panel>		
						
</div>

</ContentTemplate>           
</asp:UpdatePanel>
</asp:Content>



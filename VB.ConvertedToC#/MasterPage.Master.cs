using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Xml.Linq;
using System.Diagnostics;
using System.Collections.Specialized;
using System.Configuration;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Caching;
using System.Web.SessionState;
using System.Web.Security;
using System.Web.Profile;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
namespace VB
{
	public partial class MasterPage : System.Web.UI.MasterPage
	{

        HRBL BL=new HRBL();

		protected void Page_Load(object sender, System.EventArgs e)
		{
			if ((Session["USER_PSNL_NO"] == null)) {
				ScriptManager.RegisterStartupScript(this.Page, typeof(string), "Alert", "showAlert('กรุณาลงชื่อเข้าใช้ระบบ');", true);
				Response.Redirect("Login.aspx");
				return;
			}

			SetThemeColor();
			if (!IsPostBack) {
				SetMenuActive();
				SetUserRole();
				lblUserName.Text = Session["USER_PSNL_NO"] + " : " + Session["USER_Full_Name"];
				if (Session["USER_Position"] != null) {
					lblUserName.Text += " (" + Session["USER_Position"].ToString() + ")";
				}

                lblVersion.Text = BL.AppVersion();

			} else {
				ScriptManager.RegisterStartupScript(this.Page, typeof(string), "freezeHeader", "freezeHeaderAfterPostback();", true);
			}

            ScriptManager.RegisterStartupScript(this.Page, typeof(string), "jQuery", "App.initUniform();", true);
  	}

		private void SetThemeColor()
		{
			if ((Request.Cookies["style_color"] != null)) {
				switch (Request.Cookies["style_color"].Value.ToString()) {
					case "blue":
						Theme_blue.Attributes["class"] += " current";
						style_color.Href = "assets/css/themes/blue.css";
						break;
					case "brown":
						Theme_brown.Attributes["class"] += " current";
						style_color.Href = "assets/css/themes/brown.css";
						break;
					case "purple":
						Theme_purple.Attributes["class"] += " current";
						style_color.Href = "assets/css/themes/purple.css";
						break;
					case "grey":
						Theme_grey.Attributes["class"] += " current";
						style_color.Href = "assets/css/themes/grey.css";
						break;
					case "light":
						Theme_light.Attributes["class"] += " current";
						style_color.Href = "assets/css/themes/light.css";
						break;
					default:
						Theme_default.Attributes["class"] += " current";
						style_color.Href = "assets/css/themes/default.css";
						break;
				}
			} else {
				Theme_default.Attributes["class"] += " current";
				style_color.Href = "assets/css/themes/default.css";
			}
		}


		private void SetMenuActive()
		{

			switch (Page.TemplateControl.ToString().ToUpper()) {

                case "ASP.ASSESSMENT_SELF_KPI_CREATE_ASPX":
                case "ASP.ASSESSMENT_SELF_COMP_CREATE_ASPX":
                case "ASP.ASSESSMENT_SELF_KPI_ASSESSMENT_ASPX":
                case "ASP.ASSESSMENT_SELF_COMP_ASSESSMENT_ASPX":
                case "ASP.ASSESSMENT_SELF_ASSESSMENT_HISTORY_ASPX":

					munAssSelf.Attributes["class"] += " active";

					break;

                case "ASP.ASSESSMENT_TEAM_KPI_CREATE_ASPX":
                case "ASP.ASSESSMENT_TEAM_KPI_ASSESSMENT_ASPX":
                case "ASP.ASSESSMENT_TEAM_COMP_ASSESSMENT_ASPX":
                case "ASP.ASSESSMENT_TEAM_COMP_CREATE_ASPX":
                case "ASP.ASSESSMENT_TEAM_SET_RANK_ASPX":
                case "ASP.ASSESSMENT_TEAM_ASSESSMENT_RANK_ASPX":
                case "ASP.RPT_M_YEARLY_RESULT_ASPX":

					munAssTeam.Attributes["class"] += " active";

					break;
                case "ASP.ASSESSMENTSETTING_ROUND_ASPX":
                case "ASP.ASSESSMENTSETTING_COMMITTEE_ASPX":
                case "ASP.ASSESSMENTSETTING_ASSESSOR_ASPX":
                case "ASP.ASSESSMENTSETTING_ASSESSOR_ASSIGNED_ASPX":
                case "ASP.ASSESSMENTSETTING_ORGANIZEPERIOD_ASPX":

					mnuAssessmentSetting.Attributes["class"] += " active";

					break;
                case "ASP.ASSESSMENTSETTING_COMP_CORE_ASPX":
                case "ASP.ASSESSMENTSETTING_COMP_CLASSGROUP_ASPX":
                case "ASP.ASSESSMENTSETTING_COMP_FN_ASPX":
                case "ASP.ASSESSMENTSETTING_COMP_MGR_ASPX":

					mnuAssessmentSetting.Attributes["class"] += " active";
					mnuAssessmentSettingCOMP.Attributes["class"] += " active";

					break;
                case "ASP.ASSESSMENTSETTING_KPI_PSN_ASPX":
                case "ASP.ASSESSMENTSETTING_COMP_PSN_ASPX":

					mnuAssessmentSetting.Attributes["class"] += " active";
					mnuAssessmentSettingPSN.Attributes["class"] += " active";

					break;

                case "ASP.ASSESSMENTSETTING_WKL_JOBMAPPING_ASPX":
                case "ASP.ASSESSMENTSETTING_WKL_WORKLOAD_ASPX":
                case "ASP.ASSESSMENTSETTING_WKL_WORKLOAD_SIM_ASPX":

					mnuAssessmentSetting.Attributes["class"] += " active";
					mnuAssessmentSettingWKL.Attributes["class"] += " active";

					break;
                case "ASP.ASSESSMENTSETTING_GAP_MASTER_ASPX":
                case "ASP.RPT_IDP_PSN_ASPX":
                case "ASP.RPT_C_IDP_PSN_BY_GAP_ASPX":
                case "ASP.RPT_C_IDP_QTY_BY_GAP_ASPX":
					mnuGAP.Attributes["class"] += " active";

					break;

                case "ASP.ASSESSMENTSETTING_GAP_CORE_ASPX":
                case "ASP.ASSESSMENTSETTING_GAP_FN_ASPX":
                case "ASP.ASSESSMENTSETTING_GAP_MGR_ASPX":
				
					//เพิ่มเมนูรายงานช่องว่างสมรรถนะ()
					mnuGAP.Attributes["class"] += " active";
					mnuGAPCOMP.Attributes["class"] += " active";

					break;
                case "ASP.ASSTEAM_WKL_CLASS_GROUP_ASPX":
                case "ASP.ASSTEAM_WKL_JOBMAPPING_ASPX":
                case "ASP.ASSTEAM_WKL_WORKLOAD_ASPX":
                case "ASP.ASSTEAM_WKL_WORKLOAD_SIM_ASPX":

					mnuWKL.Attributes["class"] += " active";

					break;
                case "ASP.SETTING_ORGANIZE_ASPX":
                case "ASP.SETTING_POSITION_ASPX":
                case "ASP.SETTING_PERSONAL_ASPX":
                case "ASP.SETTING_USER_ASPX":
                case "ASP.SETTING_TRANSACTION_ASPX":
                case "ASP.SETTING_TRANSFER_ORACLE_ASPX":
                case "ASP.ASSESSMENT_SETTING_HEADER_ASPX":
					mnuSetting.Attributes["class"] += " active";

					break;
                case "ASP.RPT_K_PSNNOTASS_ASPX":
                case "ASP.RPT_K_DEPTNOTASS_ASPX":
                case "ASP.RPT_K_MGRNOTASS_ASPX":
                case "ASP.RPT_K_PSNASSSTATUS_ASPX":
                case "ASP.RPT_K_PNSASS_ASPX":
                case "ASP.RPT_K_DEPTSORT_ASPX":
                case "ASP.RPT_K_DEPTRANK_ASPX":
                case "ASP.RPT_K_PSNASSRESULT_ASPX":

					mnuReport.Attributes["class"] += " active";
					mnuReportKPI.Attributes["class"] += " active";

					break;
                case "ASP.RPT_C_PSNNOTASS_ASPX":
                case "ASP.RPT_C_DEPTNOTASS_ASPX":
                case "ASP.RPT_C_MGRNOTASS_ASPX":
                case "ASP.RPT_C_PSNASSSTATUS_ASPX":
                case "ASP.RPT_C_PSNASS_ASPX":
                case "ASP.RPT_C_DEPTSORT_ASPX":
                case "ASP.RPT_C_DEPTRANK_ASPX":
                case "ASP.RPT_C_IDP_ASPX":
                
					mnuReport.Attributes["class"] += " active";
					mnuReportCOMP.Attributes["class"] += " active";

					break;
                case "ASP.RPT_A_DEPTASSSTATUS_ASPX":
                case "ASP.RPT_A_DEPTASSCOMPLETED_ASPX":
                case "ASP.RPT_A_LEAVE_ASPX":
                case "ASP.RPT_A_PSNASSSTATUS_ASPX":
                case "ASP.RPT_A_YEARLY_RESULT_ASPX":
                case "ASP.RPT_A_ASSESSOR_SET_RANK_ASPX":

					mnuReport.Attributes["class"] += " active";
					mnuReportAll.Attributes["class"] += " active";

					break;
                case "ASP.RPT_W_JOBMAPPING_ASPX":
                case "ASP.RPT_W_WORKLOAD_ASPX":
                case "ASP.RPT_W_FTE_ASPX":

					mnuReport.Attributes["class"] += " active";
					mnuWorkload.Attributes["class"] += " active";

					break;

				//------CareerPath-----

                case "ASP.CP_VIEW_PERSONAL_SELF_ASPX":
                case "ASP.CP_SETTING_CAREERPATH_SELF_ASPX":

					munCareerPathSelf.Attributes["class"] += " active";
					munHeaderCareerPath.Attributes["class"] += " active";

					break;
                case "ASP.CP_VIEW_PERSONAL_TEAM_ASPX":
                case "ASP.CP_SETTING_PROPERTY_TEAM_ASPX":
                case "ASP.CP_COMPARE_PERSONAL_TEAM_ASPX":

					munCareerPathTeam.Attributes["class"] += " active";
					munHeaderCareerPath.Attributes["class"] += " active";

					break;
                case "ASP.CP_VIEW_PERSONAL_ASPX":
                case "ASP.CP_SETTING_PROPERTY_ASPX":

					mnuCareerPath.Attributes["class"] += " active";
					break;
			}
		}

		private void SetUserRole()
		{
			munAssSelf.Visible = Convert.ToBoolean(Session["USER_Is_PSN"]);
			mnuAssessmentSetting.Visible = Convert.ToBoolean(Session["USER_Is_Admin"]);
			munAssTeam.Visible = Convert.ToBoolean(Session["USER_Is_Manager"]);
			mnuSetting.Visible = Convert.ToBoolean(Session["USER_Is_Admin"]);
			mnuReport.Visible = Convert.ToBoolean(Session["USER_Is_Admin"]);

           	mnuGAP.Visible = Convert.ToBoolean(Session["USER_Is_GAP_Editor"]);
			mnuAssessmentSettingWKL.Visible = false;


			//--------------เส้นทางสายอสชีพ ของ ADMIN------------------------
            munCareerPathSelf.Visible =Convert.ToBoolean(Session["USER_Is_Manager"]);
            munCareerPathSelf.Visible =Convert.ToBoolean(Session["USER_Is_PSN"]);
            munCareerPathTeam.Visible = Convert.ToBoolean(Session["USER_Is_Manager"]);
            if (Convert.ToBoolean(Session["USER_Is_View_PSN"]) | Convert.ToBoolean(Session["USER_Is_Setting_Property_POS"]))
            {
                mnuCareerPath.Visible = true;
                munSub_Admin_ViewPSN.Visible = Convert.ToBoolean(Session["USER_Is_View_PSN"]);
                //---------ดูข้อมูลพนักงาน-------------
                munSub_Admin_ProperPOS.Visible = Convert.ToBoolean(Session["USER_Is_Setting_Property_POS"]);
                //---------กำหนดเกณธ์ตำแหน่ง-------------
                munSub_Admin_ComparePOS.Visible = Convert.ToBoolean(Session["USER_Is_Admin_CareerPath"]);

            }
            else {
                mnuCareerPath.Visible = false ;
            }

            //-------------- WorkLoad ----------------
            bool Is_Workload_Admin = Convert.ToBoolean(Session["USER_Is_Workload_Admin"]);
            bool Is_Workload_User = Convert.ToBoolean(Session["USER_Is_Workload_User"]);
            bool Is_Manager = Convert.ToBoolean(Session["USER_Is_Manager"]);
                                 
            mnuWKLDeptGroup.Visible=Is_Workload_Admin;
            mnuWKLJobMapping.Visible = Is_Workload_Admin | Is_Workload_User | Is_Manager;
            mnuWKLTable.Visible = Is_Workload_Admin | Is_Workload_User | Is_Manager;
            mnuWKLSim.Visible = Is_Workload_Admin | Is_Workload_User | Is_Manager;
            mnuWKL.Visible = Is_Workload_Admin | Is_Workload_User | Is_Manager;
		}


		protected void btnCloseModal_Click(object sender, System.EventArgs e)
		{
		}

		protected void btnLogout_Click(object sender, System.EventArgs e)
		{
			Session.Abandon();
			Response.Redirect("Login.aspx");
		}
		public MasterPage()
		{
			Load += Page_Load;
		}

        public void GoToSelfKPI(object sender, System.EventArgs e)
        {
            Session["ddlRound_COMP"] = null;
            Session["ddlRound_KPI"] = null;
            Session["ddlRound"] = null;
            Session["R_Year"] = null;
            Session["R_Round"] = null;


            HRBL.HR_Round_Structure LastRound=BL.LastRound();

            int R_Year=LastRound.Year;
            int R_Round=LastRound.Round;
            int KPIStatus=BL.GetKPIStatus(R_Year,R_Round,Session["USER_PSNL_NO"].ToString());
            if(KPIStatus < HRBL.AssessmentStatus.WaitCreatingApproved)
            {
               Response.Redirect("Assessment_self_KPI_Create.aspx");
            }
            else
            {
               Response.Redirect("Assessment_self_KPI_Assessment.aspx");
            }
         }

        public void GoToSelfCOMP(object sender, System.EventArgs e)
        {
            Session["ddlRound_COMP"] = null;
            Session["ddlRound_KPI"] = null;
            Session["ddlRound"] = null;
            Session["R_Year"] = null;
            Session["R_Round"] = null;

            HRBL.HR_Round_Structure LastRound=BL.LastRound();

            int R_Year=LastRound.Year;
            int R_Round=LastRound.Round;
            int COMPStatus=BL.GetCOMPStatus(R_Year,R_Round,Session["USER_PSNL_NO"].ToString());
            if(COMPStatus < HRBL.AssessmentStatus.WaitCreatingApproved)
            {
               Response.Redirect("Assessment_self_COMP_Create.aspx");
            }
            else
            {
               Response.Redirect("Assessment_self_COMP_Assessment.aspx");
            }
         }

	}
}

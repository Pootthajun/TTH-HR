﻿<%@ Page  Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPage.Master" CodeBehind="CP_Setting_Property.aspx.cs" Inherits="VB.CP_Setting_Property" %>

<%@ Register src="WUC_DateReporter.ascx" tagname="WUC_DateReporter" tagprefix="uc1" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<%@ Register src="PageNavigation.ascx" tagname="PageNavigation" tagprefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <link href="css/flow_container.css" type="text/css" rel="stylesheet" />
    <link href="assets/css/pages/pricing-tables.css" rel="stylesheet" type="text/css">
    <link href="assets/plugins/glyphicons/css/glyphicons.css" rel="stylesheet">
    <link href="assets/plugins/fancybox/source/jquery.fancybox.css" rel="stylesheet">
    <script src="assets/plugins/fancybox/source/jquery.fancybox.pack.js"></script>   
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:ToolkitScriptManager ID="toolkit1" runat="server"></asp:ToolkitScriptManager>
<asp:UpdatePanel ID="UpdatePanel1" runat="server">
<ContentTemplate>
<div class="container-fluid">
                <!-- BEGIN PAGE HEADER-->
                <div class="row-fluid" id="fixedPageHeader">
					<div class="span12"> 
						<!-- BEGIN PAGE TITLE & BREADCRUMB-->
						<h3>กำหนดเกณฑ์การเข้ารับตำแหน่ง</h3>					
						
						<ul class="breadcrumb">
                            <li>
                            	<i class="icon-signal"></i> <a href="javascript:;">เส้นทางก้าวหน้าในสายอาชีพ</a><i class="icon-angle-right"></i>
                            </li>
                        	<li>
                        	    <i class="icon-trophy"></i> <a href="javascript:;">กำหนดเกณฑ์การเข้ารับตำแหน่ง</a>
                        	</li>                        	
                        	
                            <uc1:WUC_DateReporter ID="WUC_DateReporter1" runat="server" />
                        </ul>
						<!-- END PAGE TITLE & BREADCRUMB-->
									        
				     </div>			
			    </div>
		        <!-- END PAGE HEADER-->
		        
                <asp:Panel ID="PnlList" runat="server" DefaultButton="btnSearch"> 
                <div class="row-fluid">					
					<div class="span12">
						<!-- BEGIN SAMPLE TABLE PORTLET-->	
                                             <div id="boxAlertEdit" runat ="server"  class="alert" style =" margin-top :-10px;">
							<li>
								สำหรับ <b style ="">การคัดลอกเกณฑ์การเข้ารับตำแหน่งหลายตำแหน่งพร้อมกัน</b> 
							</li>
							<li>
								โดยสามารถ คลิกปุ่ม <b>" เลือกตำแหน่งตั้งต้นที่ต้องการคัดลอก " > เลือกตำแหน่งที่ต้องการ</b>
							</li>
							<li>
								<u>วิธีการ</u> ทำเครื่องหมาย <i class="icon-ok"></i> หน้า <b> เลขที่ตำแหน่ง (ในตาราง)</b> ที่ต้องการกำหนดให้เหมือนตำแหน่งตั้งต้น 
							</li>
                            <li>
								จากนั้น คลิกปุ่ม <b>" ยืนยันการคัดลอก "</b>
							</li>
					</div>

                                        <div class="row-fluid form-horizontal">
												        <div class="span4 ">
													        <div class="control-group">
													            <label class="control-label" ><i class="icon-sitemap"></i> ฝ่าย</label>
													            <div class="controls">
														            
				                                                    <asp:DropDownList ID="ddlSector" OnSelectedIndexChanged="btnSearch_Click" runat="server" AutoPostBack="true" CssClass="medium m-wrap">
								                                        </asp:DropDownList>

														        </div>
												            </div>
													    </div>
													     <div class="span6 ">
														    <div class="control-group">
													            <label class="control-label"><i class="icon-sitemap"></i> หน่วยงาน</label>
													            <div class="controls">
														            <asp:TextBox ID="txt_Search_Organize" OnTextChanged="btnSearch_Click" runat="server" AutoPostBack="true" CssClass="m-wrap medium" placeholder="ค้นหาจากชื่อหน่วยงาน"></asp:TextBox>
														        </div>
												            </div>
													    </div>	
                                                        													    
												    </div>
    				                    <div class="row-fluid form-horizontal">
												        <div class="span4 ">
													        <div class="control-group">
													            <label class="control-label" ><i class="icon-briefcase"></i> ตำแหน่ง</label>
													            <div class="controls">
                                                                    <asp:TextBox ID="lblPOS" OnTextChanged="btnSearch_Click" runat="server" AutoPostBack="true" CssClass="m-wrap medium" placeholder="ค้นหาจากรหัส/ชื่อตำแหน่ง"></asp:TextBox>
														        </div>
												            </div>
													    </div>
													     <div class="span6 ">
                                                            <div class="control-group">
													            <label class="control-label"><i class="icon-user"></i> ผู้ครองตำแหน่ง</label>
													            <div class="controls">
														            <asp:DropDownList ID="ddlChkPosEmpty" OnSelectedIndexChanged="btnSearch_Click" runat="server" AutoPostBack="true" CssClass="medium m-wrap">
				                                                        <asp:ListItem Value="0" Selected="true">ทั้งหมด</asp:ListItem>
				                                                        <asp:ListItem Value="1">ไม่มีผู้ครองตำแหน่ง</asp:ListItem>
				                                                        <asp:ListItem Value="2">มีผู้ครองตำแหน่ง</asp:ListItem>
			                                                        </asp:DropDownList>
														        </div>
												            </div>
														    
													    </div>													    
												    </div>
                                        <div class="row-fluid form-horizontal">
												        <div class="span4 ">
                                                            <div class="control-group">
													            <label class="control-label"><i class="icon-ok"></i> กำหนดเกณฑ์</label>
													            <div class="controls">
														            <asp:DropDownList ID="ddlSetting" OnSelectedIndexChanged="btnSearch_Click" runat="server" AutoPostBack="true" CssClass="medium m-wrap">
				                                                        <asp:ListItem Value="0" Selected="true">ทั้งหมด</asp:ListItem>
				                                                        <asp:ListItem Value="1">กำหนดแล้ว</asp:ListItem>
				                                                        <asp:ListItem Value="2">ยังไม่ได้กำหนด</asp:ListItem>		                                                        

			                                                        </asp:DropDownList>
														        </div>
												            </div>

													    </div>
													     <div class="span6 ">
														    <div class="control-group">
													            
												            </div>
													    </div>													    
												    </div>
                                                <div class="row-fluid form-horizontal">
												        <div class="span4 ">
                                                            <div class="control-group">
													            <label class="control-label"> กำหนดพร้อมกันหลายตำแหน่ง</label>
													            <div class="controls">
														                <asp:Panel ID="pnlCopy" runat="server" >
                                                                            <asp:Button ID="btnCopySource_POS"  runat="server" CssClass="btn purple " 
                                                                                Text="เลือกตำแหน่งตั้งต้นที่ต้องการคัดลอก" onclick="btnCopySource_POS_Click" />
                                                                        </asp:Panel>
                                               
														        </div>
												            </div>
<%--OnClick="BtnAddTest_Click"--%>
													    </div>
													     <div class="span6 ">
														    <div class="control-group">
								                                            <asp:Panel ID="pnlPaste" runat="server" >
                                                                                <asp:Button ID="btnPasteDestination_POS"  runat="server" CssClass="btn blue "  Visible ="false" 
                                                                                    Text="ยืนยันการคัดลอก" 
                                                                                    onclick="btnPasteDestination_POS_Click" />
                                                                            </asp:Panel>					            
												            </div>
													    </div>													    
												    </div>
                                                <div class="row-fluid form-horizontal">
												        <div class="span8 ">
                                                            <div class="control-group">
													            <label class="control-label"> คัดลอกจาก</label>
													            <div class="controls">
														                <asp:Panel ID="Panel1" runat="server" >
                                                                            <asp:Label ID="lblSource_POS" runat="server" Text ="-"  Font-Size="16px" Font-Bold="true"></asp:Label><br />
                                                                            <asp:Label ID="lblSource_POS_Sub" runat="server"  Font-Size="14px" Font-Bold="true"></asp:Label>
                                                                            <asp:Label ID="lblCOPY_POS_NO" runat="server" style="display:none;"></asp:Label>
                                                                            <%--<asp:Label ID="lblCurrent_SelectPOS" runat="server" style="display:none;"></asp:Label>--%>
                                                                <asp:Label ID="lblCurrent_SelectPOS"  style="display:none;" runat="server"   Font-Size="14px" Font-Bold="true"></asp:Label>

                                                                        </asp:Panel>
														        </div>
												            </div>
<%--OnClick="BtnAddTest_Click"--%>
													    </div>
													     <div class="span6 ">
														    <div class="control-group">
												            </div>
													    </div>													    
												    </div>



	                        <asp:Button ID="btnSearch"  OnClick="btnSearch_Click"   runat="server" Text="" style="display:none;" />    
						    <div class="portlet-body no-more-tables" style="width:auto  ">
						        <asp:Label ID="lblCountList" runat="server" Font-Size="14px" Font-Bold="true"></asp:Label>
								<table class="table table-bordered  table-full-width table-advance dataTable no-more-tables table-hover">
									<thead style="text-align:center;">                                        
										<tr>											
											<th style="text-align:center;"><i class="icon-sitemap"></i> ฝ่าย</th>
											<th style="text-align:center;"><i class="icon-sitemap"></i> หน่วยงาน</th>
											<th style="text-align:center;"><i class="icon-bookmark"></i> ระดับ</th>
                                            <th style="text-align:center;"><i class="icon-ok"></i> เลือกตำแหน่ง<br />ที่ต้องการ</th>
											<th style="text-align:center;"><i class="icon-briefcase"></i> เลขตำแหน่ง</th>
											<th style="text-align:center;"><i class="icon-briefcase"></i> ตำแหน่ง</th>
											<th style="text-align:center;"><i class="icon-briefcase"></i> ตำแหน่งการบริหาร</th>											
											<th style="text-align:center;"><i class="icon-user"></i> ผู้ครองตำแหน่ง</th>
                                            <th style="text-align:center;"><i class="icon-ok"></i> กำหนดเกณฑ์</th>
                                            <th style="text-align:center;"><i class="icon-bolt"></i> ดำเนินการ</th>

										</tr>
									</thead>
									<tbody style="text-align:center; background-color:White ;">
										<asp:Repeater ID="rptList" OnItemCommand="rptList_ItemCommand" OnItemDataBound="rptList_ItemDataBound" runat="server">
										    <ItemTemplate>
    										        <tr>
    										            <td data-title="ฝ่าย">
                                                            <asp:Label ID="lblSector" runat="server"></asp:Label>
                                                            <asp:Label ID="lblSector_No" runat="server" CssClass="text bold"   style=" display:none;"></asp:Label>
                                                        </td>
                                                        <td data-title="หน่วยงาน">
                                                            <asp:Label ID="lblDept" runat="server"></asp:Label>
                                                            <asp:Label ID="lblDept_Temp" runat="server"  style="display:none;" ></asp:Label>
                                                            <asp:Label ID="lblDept_No" runat="server" CssClass="text bold"   style=" display:none;"></asp:Label>
                                                        </td>
                                                        <td data-title="ระดับ" style =" text-align :center ;"><asp:Label ID="lblClass" runat="server"></asp:Label></td>
                                                        <td data-title="เลือกตำแหน่ง" style="text-align:center;">
                                                           <%-- <a ID="img_SelectCopy" runat="server" target="_blank"></a>--%>
                                                           <asp:CheckBox ID="chkSelect_Paste" runat="server" Text=""  CommandName="ckSelect"  Checked="false" />
                                                        </td>
    										            
                                                        <td data-title="เลขตำแหน่ง"  style =" text-align :center ;" ><b><asp:Label ID="lblPosNo" runat="server"></asp:Label></b></td>
    										            <td data-title="ตำแหน่ง"><asp:Label ID="lblFLD" runat="server"></asp:Label></td>
    										            <td data-title="ตำแหน่งทางการบริหาร"><asp:Label ID="lblMGR" runat="server"></asp:Label></td>                                                        
                                                        <td data-title="ผู้ครองตำแหน่ง"><asp:Label ID="lblPSN" runat="server"></asp:Label></td>                                                        
                                                        <td data-title="กำหนดเกณฑ์" style="text-align:center;">
                                                            <a ID="img_Status" runat="server" target="_blank"></a>
                                                        </td>
                                                        <td data-title="ดำเนินการ">
                                                            <asp:Button ID="btnSetting" runat="server" CssClass="btn mini blue" CommandName="Setting" Text="กำหนดเกณฑ์" />	
                                                        </td>
                                                    </tr>
										    </ItemTemplate>
										</asp:Repeater>
										
										

                                    </tbody>
                                </table>
                                <asp:PageNavigation ID="Pager" OnPageChanging="Pager_PageChanging" MaximunPageCount="5" PageSize="20" runat="server" />
							</div>
						
						<!-- END SAMPLE TABLE PORTLET-->
						
					</div>
                </div>
               </asp:Panel>

                <asp:Panel ID="pnlEdit" runat="server" Visible="True">
                			<div class="alert">
										<li>
											สามารถกรอกข้อมูลกำหนดเกณฑ์เข้ารับตำแหน่งข้อใดข้อหนึ่งได้
										</li>
										<li>
											ระบบจะบันทึกข้อมูลอัตโนมัติหลังจากที่ทำการกรอกข้อมูล หรือเปลี่ยนแปลงข้อมูล
										</li>
								</div>

                          <h3 class="form"  >
							  กำหนดเกณฑ์การเข้ารับตำแหน่งสำหรับ &nbsp;&nbsp;<asp:Label ID="lblPos_Head" runat="server"  Text ="XXX"></asp:Label>&nbsp;&nbsp;&nbsp; 
                              ระดับ  &nbsp;<asp:Label ID="lblClass_Head" runat="server"   Text ="7"></asp:Label>
						  </h3>
                          <h4 class="form" style =" color :Gray ;"  >
							  &nbsp;&nbsp;&nbsp;&nbsp; หน่วนงาน : <asp:Label ID="lblDept_Head" runat="server"  Text ="XXX"></asp:Label>&nbsp;&nbsp;&nbsp; 
                              ( ผู้ครองตำแหน่ง ณ ปัจจุบัน :  &nbsp;<asp:Label ID="lblPSN_Head" style =" color :Gray ;" runat="server"   Text ="XXX"></asp:Label> )
						  </h4>        
				         <div class="row-fluid" style="border-bottom:10px solid #EEEEEE; margin-bottom :-13px;"></div>
			                
												
				                <!-- BEGIN FORM-->
                			    <div class="row-fluid" id="div1" runat="server" Visible="True">
                                    <div class="span12"></div>
                                    <div class="span12">
                                    <div class="portlet">
				                    <div class="portlet-body form">
									    
											    <div class="row-fluid" style=" padding-left:6em; font-size:16px; font-weight:bold; padding-bottom:20px; padding-top:20px; border-bottom:2px solid #eeeeee;">
											            <%--<span class="badge badge-important glyphicons no-js play">1</span>--%>
                                                        <span style=" font-size :20px;" >&nbsp;1.&nbsp;  </span>
                                                        ผลการประเมิน KPI + Competency 3 รอบการประเมินย้อนหลัง เฉลี่ยไม่ต่ำกว่า 
											            <asp:TextBox runat="server" ID="txtAss_Min_Score" OnTextChanged="txtAss_Min_Score_TextChanged" AutoPostBack="true" CssClass="m-wrap " Width ="50px" Text ="" style=" text-align :center; margin-top:-10px; "   ></asp:TextBox>
											            &nbsp;<asp:Label ID="lblExeAssScore" style ="  font-size :16px;color :red ;" runat="server"   Text =" ( เต็ม 500 คะแนน ) "></asp:Label>
                                                </div>

                                                <div class="row-fluid" style=" padding-left:6em; font-size:16px; font-weight:bold; padding-bottom:20px; padding-top:20px; border-bottom:2px solid #eeeeee;">
											                <span style=" font-size :20px;" >&nbsp;2.&nbsp;  </span>
                                                            จำนวนวันลา(กิจ/ป่วย) 3 ปีงบประมาณย้อนหลังไม่เกิน <asp:TextBox runat="server" ID="txtLeave_Score"  OnTextChanged="txtAss_Min_Score_TextChanged" AutoPostBack="true" CssClass="m-wrap" Width ="50px" style=" text-align :center; margin-top:-10px; "  ></asp:TextBox> วัน 
											                &nbsp; &nbsp; &nbsp;
											                <div  id="Arrive_Late" runat ="server" visible ="false" >
                                                            มาสายไม่เกิน <asp:TextBox runat="server" ID="txtArrive_Late" OnTextChanged="txtAss_Min_Score_TextChanged" CssClass="m-wrap"  Width ="50px" style=" text-align :center; margin-top:-10px; " ></asp:TextBox> วัน
											                </div>
                                                </div>
											   
											   <div class="row-fluid" style=" padding-left:6em; font-size:16px; font-weight:bold; padding-bottom:20px; padding-top:20px; ">
											            <span style=" font-size :20px;" >&nbsp;3.&nbsp;  </span>
                                                        ต้องผ่านการฝึกอบรมหลักสูตรต่อไปนี้
											    </div>
											    
									           
                                             <div class="row-fluid" style="border-bottom:2px solid #eeeeee;">
												<div class="form-horizontal">	
                                                    <div class ="span10" >									        
												        <div class="control-group">
													        <label class="control-label"></label>
													        <div class="controls">
                                                    <asp:Panel ID="pnltable_Course" runat="server" >
                                                   <table class="table table-advance dataTable no-more-tables table-hover" width="50%">
									                    <thead>
										                    <tr>
											                    <th><i class="icon-book"></i> หลักสูตร</th>
											                    <th><i class="icon-bolt"></i> ดำเนินการ</th>
										                    </tr>
									                    </thead>
									                    <tbody>
                                                            <asp:Repeater ID="rptTraining" OnItemCommand="rptTraining_ItemCommand" OnItemDataBound="rptTraining_ItemDataBound" runat="server">
								                                    <ItemTemplate>	   
                                                                   <tr>
                                                                       <td data-title="หลักสูตร">
                                                                           <asp:Label ID="lblName" runat="server" style="text-decoration:none; width:80%; float:left;"></asp:Label>
                                                                       </td>
                                                                       <td data-title="เลือก">
                                                                           <%--<a ID="lblDelete"  runat="server" class="btn mini red" style="float:right;">ลบ</a> --%>
                                                                           <asp:Button ID="btnDelete" runat="server" CssClass="btn mini red" CommandName="Delete" Text="ลบ" /> 
                                                                       </td>
                                                                   </tr>
                                                              </ItemTemplate>
                                                               
								                         </asp:Repeater>

									                    </tbody>								                                    
								                    </table>
                                                    </asp:Panel>
                                                                    <asp:Panel ID="pnlBack1" runat="server" >
                                                                        <asp:Button ID="btnAddCourse" OnClick="btnAddCourse_Click" runat="server" CssClass="btn purple " Text="เพิ่มหลักสูตร" />
                                                                    </asp:Panel>
                                                                    <br /><br />
                                                             </div>
												        </div>
                                                    </div>		    											        												        
											   </div>
											</div>
											
											<div class="row-fluid" style=" padding-left:6em; font-size:16px; font-weight:bold; padding-bottom:20px; padding-top:20px;">
											            <span style=" font-size :20px;" >&nbsp;4.&nbsp;  </span>
                                                        ต้องผ่านการสอบวัดผลรายวิชาดังนี้
											</div>

                                               <div class="row-fluid" style="border-bottom:2px solid #eeeeee;">
												<div class="form-horizontal">	
                                                    <div class ="span10" >									        
												        <div class="control-group">
													        <label class="control-label"></label>
													        <div class="controls">
                                                    <asp:Panel ID="pnltable_Test" runat="server" >
                                                   <table class="table table-advance dataTable no-more-tables table-hover" width="50%">
									                    <thead>
										                    <tr>
											                    <th><i class="icon-book"></i> วิชา</th>
											                    <th  style="text-align:center;"><i class="icon-book"></i> ได้คะแนนไม่ต่ำกว่า</th>
										                        <th><i class="icon-bolt"></i>ดำเนินการ</th>
										                    </tr>
									                    </thead>
									                    <tbody>
                                                               <asp:Repeater ID="rptTest" OnItemCommand="rptTest_ItemCommand" OnItemDataBound="rptTest_ItemDataBound" runat="server" >
								                                    <ItemTemplate>
                                                                   <tr>
                                                                       <td data-title="หลักสูตร">
                                                                           <asp:Label ID="lblName" runat="server" style="text-decoration:none; width:80%; float:left;"></asp:Label>
                                                                       </td>
                                                                       <td id="cel_Score" runat ="server"   style="text-align:center;" data-title="ได้คะแนนไม่ต่ำกว่า">
                                                                           <asp:TextBox ID="txtScore" Enabled ="true"  runat="server" style="text-decoration:none; width:20%; float:Center; text-align :Right;"></asp:TextBox>
                                                                           <asp:Button ID="btnupdate" runat="server" style="display:none;" CommandName="Update" /> 
                                                                           </td>
                                                                       <td data-title="เลือก">
                                                                           <asp:Button ID="btnDelete" runat="server" CssClass="btn mini red" CommandName="Delete" Text="ลบ" /> 
                                                                       </td>
                                                                   </tr>
                                                                   </ItemTemplate> 
                                                                   </asp:Repeater> 
									                    </tbody>
								                    </table>
                                                    </asp:Panel>
                                                                        <asp:Panel ID="pnlbtnTest" runat="server" >
                                                                            <asp:Button ID="BtnAddTest" OnClick="BtnAddTest_Click" runat="server" CssClass="btn purple " Text="เพิ่มวิชา" />
                                                                        </asp:Panel>
                                                             </div>
												        </div>
                                                    </div>		    											        												        
											   </div>
											</div>
		
                                            <div class="row-fluid" style=" padding-left:6em; font-size:16px; font-weight:bold; padding-bottom:20px; padding-top:20px; border-bottom:2px solid #eeeeee;">
											             <span style=" font-size :20px;" >&nbsp;5.&nbsp;  </span>
                                                         ไม่มีประวัติมีโทษทางวินัยใน 1 ปี ย้อนหลัง &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp; <asp:CheckBox ID="chkPunishment" OnCheckedChanged="txtAss_Min_Score_TextChanged" Checked ="true" Visible ="false"   runat ="server" />
                                                        <asp:ImageButton ID="btnCheck_Punishment"  OnClick="btnCheck_Punishment_Click" runat="server" ImageUrl="images/checkCP.png" Height="30px" ToolTip="Click เพื่อเปลี่ยน" />
                                            </div>
                                            <br />
											<b><h3  style =" padding-left:25em; font-size :20px;" text-align :center ;" >กำหนดเส้นทางความก้าวหน้าของตำแหน่ง</h3></b>
											    
											    <div class="row-fluid" class="flow_container" style=" padding-left:-10em; font-size:16px; font-weight:bold; padding-bottom:20px; padding-top:20px; border-bottom:2px solid #eeeeee;">
											                
                                                        <table style =" margin-left :30px;" border="0" cellspacing="0" cellpadding="0" class="flow_container"  align="center">
                                                              <%--<tr>
                                                                 <th colspan="5" style="font-size:16px; font-weight:bold; padding-top:20px; text-align:center; text-decoration:underline;"><span style=" font-size :20px;" >&nbsp;6.&nbsp;  </span><span style=" font-size :16px;" >กำหนดเส้นทางความก้าวหน้าของตำแหน่ง</span> </th>
                                                              </tr>--%>
                                                              <tr>
                                                                  <th align="center" class="flow_header"> </th>
                                                                  <th align="center" class="flow_header"> </th>
                                                                  <th align="center" class="flow_header"> </th>
                                                                  <th align="center" class="flow_header"> </th>
                                                                  <th align="center" class="flow_header"> </th>
                                                              </tr>
                                                              <tr>
                                                                <td id="tdPosLeft_Header" runat ="server" align="center" valign="middle"  >
                                                               	    ตำแหน่งที่สามารถเข้ารับตำแหน่งนี้ได้ <a  ID="btnAddLeft" onserverclick="btnAddLeft_ServerClick" runat ="server" style =" top :-9px;color:Green; display :none ;"   class="  glyphicons no-js circle_plus " title="เพิ่มตำแหน่ง"><i></i></a>
                                                                </td>
                                                                <td id="tdimgPosLeft_Header" runat ="server" align="center" valign="middle" > </td>
                                                                <td align="center" valign="middle"> <asp:Label ID="lblModePos" runat="server" style="display:none;"></asp:Label> </td>
                                                                <td id="tdimgPosRight_Header" runat ="server" align="center" valign="middle"> </td>
                                                                <td id="tdPosRight_Header" runat ="server"  align="center" valign="middle">
                                                                    ตำแหน่งต่อไปที่ผู้ดำรงตำแหน่งนี้สามารถเข้ารับได้ &nbsp;&nbsp; <a  ID="btnAddRight" onserverclick="btnAddRight_ServerClick" runat ="server" style =" top :-9px;color:Green;"   class="  glyphicons no-js circle_plus " title="เพิ่มตำแหน่ง"><i></i></a>
                                                                </td>
                                                              </tr>
                                                              <tr>
                                                                  <td id="tdPosLeft_Detail" runat ="server" align="center" valign="middle"> </td>
                                                                  <td id="tdimgPosLeft_Detail" runat ="server" align="center" valign="middle"> </td>
                                                                  <td align="center" valign="middle"> </td>
                                                                  <td id="tdimgPosRight_Detail" runat ="server" align="center" valign="middle"> </td>
                                                                  <td id="tdPosRight_Detail" runat ="server" align="center" valign="middle"> </td>
                                                              </tr>
                                                              <tr>
                                                                  <td id="tdPosLeft_rpt" align="center" runat ="server" style =" vertical-align :middle ;" >
                                                                      <asp:Repeater  ID="rptPosLeft" OnItemDataBound="rptPosLeft_ItemDataBound" runat="server">
                                                                          <ItemTemplate>
                                                                              <table border="0"  cellspacing="0" style=" margin-bottom :0px;" >
                                                                                  
                                                                                  <tr style=" margin:-10px;">

                                                                                      <td style=" margin:0px;">
                                                                                            <div class="row-fluid">
					                                                                            <div class="span12">
						                                                                            <!-- BEGIN SAMPLE TABLE PORTLET-->
						                                                                            <div class="portlet box blue ">
							                                                                            <div class="portlet-title">
								                                                                            <div class="caption">
                                                                                                            <asp:LinkButton ID="L" runat ="server"  CommandName="lnkCurrentFromLeft" ></asp:LinkButton>                                                                                          
                                                                                                              <table border="0" cellpadding="0" cellspacing="0" style=" margin:0px;" class="flow_item" width="200">
                                                                                                                  <tr>
                                                                                                                      <td align="center" width="30">
                                                                                                                          <img src="images/pos.png" width="16" height="16" />
                                                                                                                      </td>
                                                                                                                      <td>
                                                                                                                          <asp:Label ID="lblPosLeft" runat="server"></asp:Label>
                                                                                                                      </td>
                                                                                                                  </tr>
                                                                                                              </table> 
                                                                                                            
                                                                                                            </div>
								                                                                            <div class="tools">
                                                                                                                <a href="javascript:;" title ="ลบตำแหน่ง"  class="remove" id="btnDeleteLeft_Close" runat ="server" style =" display :none;" ></a><br />
									                                                                            <a href="javascript:;" title ="คลิกดูเกณฑ์เข้ารับตำแหน่ง" style =" vertical-align : bottom ;" class="expand"></a>
								                                                                            </div>
							                                                                            </div>
                                                                                                        <div style="display: none;" class="portlet-body hide">
								                                                                            <table class="table table-hover">
									                                                                            
									                                                                            <tbody>
										                                                                            <tr>
											                                                                            <ul class="pricing-content_Property unstyled" style =" width :300px; border-bottom :0px;">
												                                                                            <li><i class="icon-check"></i> ผลการประเมิน KPI+Competency ย้อนหลัง 3 รอบย้อนหลัง <b><asp:Label ID="lblAss_Min_Score" ForeColor ="Black"  runat="server" Text ="XXX"></asp:Label></b></li>
												                                                                            <li><i class="icon-ambulance"></i> จำนวนวัน ป่วย สาย ลา ขาด ย้อนหลัง 3 ปี <br /> <b><asp:Label ID="lblLeave_Score" runat="server" ForeColor ="Black"  Text ="XXX"></asp:Label></b> </li>
												                                                                            <li><i class="icon-book"></i> 
												                                                                                ผ่านการฝึกอบรม <b><asp:Label ID="lblCount_Course" ForeColor ="Black"  runat="server" Text ="XXX"></asp:Label></b> หลักสูตร
												                                                                                <ul>
                                                                                                                                    <asp:Repeater ID="rptCourse" runat="server">
								                                                                                                        <ItemTemplate>	
												                                                                                            <li><asp:Label ID="lblCourse" runat="server" ForeColor ="Black"  Text ="XXX"></asp:Label></li>
												                                                                                        </ItemTemplate> 
                                                                                                                                    </asp:Repeater> 
												                                                                                </ul>    
												                                                                            </li>
												                                                                            <li><i class="icon-thumbs-up"></i> ผ่านการสอบ <b><asp:Label ID="lblCount_Test" ForeColor ="Black"  runat="server" Text ="XXX"></asp:Label></b> วิชา
												                                                                                <ul>
                                                                                                                                    <asp:Repeater ID="rptTest" runat="server" >
								                                                                                                        <ItemTemplate>
												                                                                                            <li><asp:Label ID="lblTest" ForeColor ="Black"  runat="server" Text ="XXX"></asp:Label> <i class ="icon-star-empty"></i><b><asp:Label ID="lblMin_Score" ForeColor ="Black"  runat="server" Text ="XXX"></asp:Label></b> <span ForeColor ="Black" >คะแนน</span></li>
												                                                                                        </ItemTemplate> 
                                                                                                                                    </asp:Repeater> 
												                                                                                </ul>  
												                                                                            </li>
												                                                                            <li>
                                                                                                                                <i class="icon-legal"></i> <asp:Label ID="lblPunishment" runat="server" Text ="XXX"></asp:Label>											
												                                                                            </li>												
											                                                                            </ul>
										                                                                            <%--</div>--%>
                                                                                                                    
                                                                                                                    </tr>
										                                                                            
									                                                                            </tbody>
								                                                                            </table>
                                                                                                            <div class="scroller-footer" style=" margin-top :-30px;">
										                                                                        <div class="pull-right">
                                                                                                                    <a  target ="_blank" id="lnkL" runat ="server"  > ดูรายละเอียดทั้งหมด &nbsp;&nbsp;<i class="m-icon-swapright m-icon-gray"></i> &nbsp;</a>
                                                                                                                 </div>
									                                                                        </div>
							                                                                            </div>
						                                                                            </div>
						                                                                            <!-- END SAMPLE TABLE PORTLET-->
					                                                                            </div>
				                                                                            </div>
                                                                                                
                                                                                            <%--<a title="ลบ" id="btnDeleteLeft_Close" runat ="server"  class="fancybox-item fancybox-close" ></a>--%>
                                                                                         
                                                                                       
                                                                                      </td>
                                                                                      <td  id="td" runat ="server"   style=" margin:0px; display :none ;" >
                                                                                          <asp:Button ID="btnDeleteLeft" runat="server" CommandName="Delete" 
                                                                                          CssClass="btn mini  white "     Text="ลบ"  />
                                                                                          <asp:ConfirmButtonExtender ID="btnDeleteLeft_Confirm" runat="server"  ConfirmText="ลบตำแหน่งที่เลือก ?" TargetControlID="btnDeleteLeft"></asp:ConfirmButtonExtender>
                                                                                      </td>
                                                                                  </tr>
                                                                                  
                                                                              </table>
                                                                          </ItemTemplate>
                                                                      </asp:Repeater>
                                                                  </td>
                                                                  <td  id="tdimgPosLeft_rpt" runat ="server" align="center"  style =" vertical-align :middle ; margin-bottom :5em;">
                                                                      <img src="images/Next.png" alt="" style="height:50px;" />
                                                                  </td>
                                                                  <td align="center" valign="middle">
                                                                      <asp:Repeater ID="rptPosLevel" OnItemCommand="rptPosLevel_ItemCommand" OnItemDataBound="rptPosLevel_ItemDataBound" runat="server" Visible ="false" >
                                                                          <ItemTemplate>
                                                                              <table border="0" cellpadding="0" cellspacing="0">
                                                                                  <tr style=" margin:0px;" >
                                                                                      <td  style=" margin:0px;" >
                                                                                      <asp:LinkButton ID="T" runat ="server"  CommandName="lnkCurrentFromTop" >
                                                                                          <table border="0" cellpadding="0" cellspacing="0" style=" margin:0px;" class="flow_item" width="200">
                                                                                              <tr>
                                                                                                  <td align="center" width="30">
                                                                                                      <img src="images/pos.png" width="16" height="16" />
                                                                                                  </td>
                                                                                                  <td>
                                                                                                      <asp:Label ID="lblPosLevel" runat="server"></asp:Label>
                                                                                                      <asp:Button ID="btnDeleteTop" runat="server"  style="display:none;" />
                                                                                                  </td>
                                                                                              </tr>
                                                                                          </table>
                                                                                      </asp:LinkButton>
                                                                                      </td>
                                                                                      <td  style=" margin:0px;" >
                                                                                          
                                                                                      </td>
                                                                                  </tr>
                                                                              </table>
                                                                          </ItemTemplate>
                                                                      </asp:Repeater>
                                                                      
                                                                      <table border="0" cellpadding="0" cellspacing="0" class="flow_item_disable" width="350" >
                                                                         <tr>
                                                                            <td align="center" width="30" colspan ="2">
                                                                            
                                                                            </td>
                                                                         </tr>
                                                                          <tr>
                                                                              <td align="center" width="30">
                                                                                  <img src="images/pos.png" alt="" width="20" height="20" />
                                                                              </td>
                                                                              <td>
                                                                                 <b><asp:Label ID="lblCurrentPos" runat="server" style=" font-size :14px;"></asp:Label></b> 
                                                                              </td>
                                                                          </tr>
                                                                          <tr>
                                                                            <td align="center" width="30" colspan ="2">
                                                                            
                                                                            </td>
                                                                         </tr>
                                                                      </table>
                                                                     
                                                                  </td>
                                                                  <td  id="tdimgPosRight_rpt" runat ="server" align="center" style=" vertical-align :middle ; margin-bottom :5em;">
                                                                      <img src="images/Next.png" alt="" style="height:50px;" />
                                                                  </td>
                                                                  <td  id="tdPosRight_rpt" runat ="server"  align="center" runat ="server" style =" vertical-align :middle ;">
                                                                      <asp:Repeater ID="rptPosRight" OnItemCommand="rptPosRight_ItemCommand" OnItemDataBound="rptPosRight_ItemDataBound" runat="server">
                                                                          <ItemTemplate>
                                                                              <table border="0"  cellspacing="0" style=" margin-bottom :0px;" >                                                                                  
                                                                                  <tr style=" margin:0px;">
                                                                                      <td style=" margin:0px;">
                                                                                            <div class="row-fluid">
					                                                                            <div class="span12">
						                                                                            <!-- BEGIN SAMPLE TABLE PORTLET-->
						                                                                            <div id="portletBox" runat ="server"  class="portlet box red ">
							                                                                            <div class="portlet-title">
								                                                                            <div class="caption">
                                                                                                            <asp:LinkButton ID="R" runat ="server"  CommandName="lnkCurrentFromRight" ></asp:LinkButton>                                                                                          
                                                                                                              <table border="0" cellpadding="0" cellspacing="0" style=" margin:0px;" class="flow_item" width="200">
                                                                                                                  <tr>
                                                                                                                      <td align="center" width="30">
                                                                                                                          <img src="images/pos.png" width="16" height="16" />
                                                                                                                      </td>
                                                                                                                      <td>
                                                                                                                          <asp:Label ID="lblPosRight" runat="server"></asp:Label>
                                                                                                                      </td>
                                                                                                                  </tr>
                                                                                                              </table> 
                                                                                                            
                                                                                                            </div>
								                                                                            <div class="tools">
                                                                                                                <a href="javascript:;" title ="ลบตำแหน่ง" class="remove" id="btnDeleteRight_Close" runat ="server" ></a><br />
									                                                                            <a href="javascript:;" title ="คลิกดูเกณฑ์เข้ารับตำแหน่ง" style =" vertical-align : bottom ;" class="expand"></a>
								                                                                            </div>
							                                                                            </div>
                                                                                                        <div style="display: none;" class="portlet-body hide">
								                                                                            <table class="table table-hover">
									                                                                            
									                                                                            <tbody>
										                                                                            <tr>
											                                                                            <ul class="pricing-content_Property unstyled" style =" width :300px; border-bottom :0px;">
												                                                                            <li><i class="icon-check"></i> ผลการประเมิน KPI+Competency ย้อนหลัง 3 รอบย้อนหลัง <b><asp:Label ID="lblAss_Min_Score" ForeColor ="Black"  runat="server" Text ="XXX"></asp:Label></b></li>
												                                                                            <li><i class="icon-ambulance"></i> จำนวนวัน ป่วย สาย ลา ขาด ย้อนหลัง 3 ปี  <br /><b><asp:Label ID="lblLeave_Score" runat="server" ForeColor ="Black"  Text ="XXX"></asp:Label></b> </li>
												                                                                            <li><i class="icon-book"></i> 
												                                                                                ผ่านการฝึกอบรม <b><asp:Label ID="lblCount_Course" ForeColor ="Black"  runat="server" Text ="XXX"></asp:Label></b> หลักสูตร
												                                                                                <ul>
                                                                                                                                    <asp:Repeater ID="rptCourse" runat="server">
								                                                                                                        <ItemTemplate>	
												                                                                                            <li><asp:Label ID="lblCourse" runat="server" ForeColor ="Black"  Text ="XXX"></asp:Label></li>
												                                                                                        </ItemTemplate> 
                                                                                                                                    </asp:Repeater> 
												                                                                                </ul>    
												                                                                            </li>
												                                                                            <li><i class="icon-thumbs-up"></i> ผ่านการสอบ <b><asp:Label ID="lblCount_Test" ForeColor ="Black"  runat="server" Text ="XXX"></asp:Label></b> วิชา
												                                                                                <ul>
                                                                                                                                    <asp:Repeater ID="rptTest" runat="server" >
								                                                                                                        <ItemTemplate>
												                                                                                            <li><asp:Label ID="lblTest" ForeColor ="Black"  runat="server" Text ="XXX"></asp:Label> <i class ="icon-star-empty"></i><b><asp:Label ID="lblMin_Score" ForeColor ="Black"  runat="server" Text ="XXX"></asp:Label></b> <span ForeColor ="Black" >คะแนน</span></li>
												                                                                                        </ItemTemplate> 
                                                                                                                                    </asp:Repeater> 
												                                                                                </ul>  
												                                                                            </li>
												                                                                            <li>
                                                                                                                                <i class="icon-legal"></i> <asp:Label ID="lblPunishment" runat="server" Text ="XXX"></asp:Label>											
												                                                                            </li>												
											                                                                            </ul>
										                                                                            <%--</div>--%>
                                                                                                                    
                                                                                                                    </tr>
										                                                                            
									                                                                            </tbody>
								                                                                            </table>
                                                                                                            <div class="scroller-footer" style=" margin-top :-30px;">
										                                                                        <div class="pull-right">
                                                                                                                    <a  target ="_blank" id="lnkR" runat ="server"  > ดูรายละเอียดทั้งหมด &nbsp;&nbsp;<i class="m-icon-swapright m-icon-gray"></i> &nbsp;</a>
                                                                                                                 </div>
									                                                                        </div>
							                                                                            </div>
						                                                                            </div>
						                                                                            <!-- END SAMPLE TABLE PORTLET-->
					                                                                            </div>
				                                                                            </div>
                                                                                      </td>
                                                                                      <td  id="td" runat ="server"   style=" margin:0px; display :none ;" >
                                                                                          <asp:Button ID="btnDeleteRight" runat="server" CommandName="Delete" 
                                                                                          CssClass="btn mini  white "     Text="ลบ"  />
                                                                                          <asp:ConfirmButtonExtender ID="btnDeleteLeft_Confirm" runat="server"  ConfirmText="ลบตำแหน่งที่เลือก ?" TargetControlID="btnDeleteRight"></asp:ConfirmButtonExtender>
                                                                                      </td>
                                                                                  </tr>
                                                                                  
                                                                              </table>            

                                                                          </ItemTemplate>
                                                                      </asp:Repeater>
                                                                  </td>

                                                              </tr>
                                                        </table>

                                                <br />
                                                </td>											    
                                                </div>
									            
                                            <div class="form-actions">
								                <asp:Button ID="btnOK" OnClick="btnOK_Click" Visible ="false"  runat="server" CssClass="btn blue" Text="บันทึก" />
								                <asp:Button ID="btnBack"  OnClick="btnBack_Click" runat="server" CssClass="btn" Text="ย้อนกลับ" />							            
						                    </div>
										    </div>
                                            													
									    </div>
                                        
								            
                                        												
							    </div>
						</div>
                               
			  </asp:Panel> 

                      <%-- เลือกหลักสูตรที่กำหนด --%>
		                <asp:Panel CssClass="modal" style="top:10%; position:fixed; width:800px;" id="ModalTraining" runat="server" DefaultButton="btnSearchTraining"  >
                        <div style="padding: 0px; width: auto;  margin-top :0px; margin-bottom :0px; top: 0px; left: 0px;" class="fancybox-skin">
                        <div class="fancybox-outer">
				            <div class="modal-header">										
					            <h3>หลักสูตรการพัฒนา </h3>
					            <asp:Label ID="lblTrainingNo" runat="server" style="display:none;"></asp:Label>
				            </div>
				            <div class="modal-body">					            
					            <div class="row-fluid form-horizontal">
					                 <div class="span12 ">
                                     <div class="control-group">
								            <label class="control-label" style="width:120px;"><i class="icon-book"></i> กลุ่มหลักสูตร</label>
								            <div class="controls">
									            <asp:DropDownList ID="BindGroup_Course" OnSelectedIndexChanged="SearchTraining" runat="server" AutoPostBack="true" CssClass="  m-wrap large header" style="font-size:14px;">
					                            </asp:DropDownList>
								            </div>
							            </div>

									    <div class="control-group">
								            <label class="control-label" style="width:120px;"><i class="icon-book"></i> ชื่อหลักสูตร</label>
								            <div class="controls">
									            <asp:TextBox ID="txtSearchTraining" OnTextChanged="SearchTraining" runat="server" AutoPostBack="true" CssClass="m-wrap large" placeholder="ค้นหาจากชื่อหลักสูตร"></asp:TextBox>
									            <asp:Button ID="btnSearchTraining" OnClick="SearchTraining" runat="server" Text="" style="display:none;" />
								            </div>
							            </div>
                                        
								    </div>								       
					            </div>
					            <div class="portlet-body no-more-tables">
    								
								    <asp:Label ID="lblCountTraining" runat="server" Font-Size="14px" Font-Bold="true"></asp:Label>
								    <table class="table table-full-width  no-more-tables table-hover">
									    <thead>
										    <tr>
                                                <th style="text-align:center;"><i class="icon-book"></i> กลุ่มหลักสูตร</th>
											    <th style="text-align:center;"><i class="icon-caret-right"></i> รหัส</th>
											    <th style="text-align:center;"><i class="icon-book"></i> หลักสูตร</th>											    
											    <th style="text-align:center;"><i class="icon-check"></i> เลือก</th>
										    </tr>										    
									    </thead>
									    <tbody>
										    <asp:Repeater ID="rptTrainingDialog" OnItemCommand="rptTrainingDialog_ItemCommand" OnItemDataBound="rptTrainingDialog_ItemDataBound" runat="server">
									            <ItemTemplate>
    									            <tr>   
                                                        <td data-title="กลุ่มหลักสูตร" style="text-align:Left;"><asp:Label ID="lblGroup_Course" runat="server"></asp:Label></td>                                 
											            <td data-title="รหัส" style="text-align:center;"><asp:Label ID="lblNo" runat="server"></asp:Label></td>
											            <td data-title="หลักสูตร"><asp:Label ID="lblName" runat="server"></asp:Label></td>											           
											            <td data-title="ดำเนินการ" style="text-align:center;">
											                <asp:Button ID="btnTrainingSelect" runat="server" CssClass="btn mini blue" CommandName="Select" Text="เลือก" /> 
										                </td>
										            </tr>	
									            </ItemTemplate>
									        </asp:Repeater>					
									    </tbody>
								    </table>
    								
								    <asp:PageNavigation ID="Pager_Training" OnPageChanging="Pager_Training_PageChanging" MaximunPageCount="10" PageSize="5" runat="server" />
							    </div>				            
				            </div>
				            <div class="modal-footer">
				                <%--<asp:Button ID="btnClose_Training" runat="server" CssClass="btn" Text="ปิด" />--%>								
				            </div>
                            </div>
                            <a title="ลบ" id="btnModalTraining_Close" onserverclick="btnModalTraining_Close_ServerClick" runat ="server"  class="fancybox-item fancybox-close" ></a>
                        </div>
			            </asp:Panel>
                    
                      <%-- เลือกการทดสอบที่กำหนด --%>
		                <asp:Panel CssClass="modal" style="top:10%; position:fixed; width:auto;" id="ModalTest" runat="server" DefaultButton="btnSearchTest"  >
                        <div style="padding: 0px; width: auto;  margin-top :0px; margin-bottom :0px; top: 0px; left: 0px;" class="fancybox-skin">
                        <div class="fancybox-outer">
				            <div class="modal-header">										
					            <h3>วิชาสอบวัดผล </h3>
					            <asp:Label ID="lblTestNo" runat="server" style="display:none;"></asp:Label>
				            </div>
				            <div class="modal-body">					            
					            <div class="row-fluid form-horizontal">
					                 <div class="span12 ">
									    <div class="control-group">
								            <label class="control-label" style="width:120px;"><i class="icon-book"></i> ชื่อวิชา</label>
								            <div class="controls">
									            <asp:TextBox ID="txtSearchTest" OnTextChanged="btnSearchTest_Click" runat="server" AutoPostBack="true" CssClass="m-wrap large" placeholder="ค้นหาจากชื่อวิชา"></asp:TextBox>
									            <asp:Button ID="btnSearchTest" OnClick="btnSearchTest_Click" runat="server" Text="" style="display:none;" />
								            </div>
							            </div>
								    </div>								       
					            </div>
					            <div class="portlet-body no-more-tables">
    								
								    <asp:Label ID="lblCountTest" runat="server" Font-Size="14px" Font-Bold="true"></asp:Label>
								    <table class="table table-full-width  no-more-tables table-hover">
									    <thead>
										    <tr>
											    <th style="text-align:center;"><i class="icon-caret-right"></i> รหัส</th>
											    <th style="text-align:center;"><i class="icon-book"></i> ชื่อวิชา</th>											    
											    <th style="text-align:center;"><i class="icon-check"></i> เลือก</th>
										    </tr>										    
									    </thead>
									    <tbody>
										    <asp:Repeater ID="rptTestDialog" OnItemCommand="rptTestDialog_ItemCommand" OnItemDataBound="rptTestDialog_ItemDataBound" runat="server">
									            <ItemTemplate>
    									            <tr>                                        
											            <td data-title="รหัส" style="text-align:center;"><asp:Label ID="lblNo" runat="server"></asp:Label></td>
											            <td data-title="ชื่อวิชา"><asp:Label ID="lblName" runat="server"></asp:Label></td>											           
											            <td data-title="ดำเนินการ" style="text-align:center;">
											                <asp:Button ID="btnTestSelect" runat="server" CssClass="btn mini blue" CommandName="Select" Text="เลือก" /> 
										                </td>
										            </tr>	
									            </ItemTemplate>
									        </asp:Repeater>					
									    </tbody>
								    </table>
    								
								    <asp:PageNavigation ID="Pager_Test" OnPageChanging="Pager_Test_PageChanging" MaximunPageCount="10" PageSize="5" runat="server" />
							    </div>				            
				            </div>
				            <div class="modal-footer">
				                <%--<asp:Button ID="btnClose_Test" runat="server" CssClass="btn" Text="ปิด" />--%>								
				            </div>
                                </div>
                            <a title="ลบ" id="btnModalTest_Close" onserverclick="btnModalTest_Close_ServerClick" runat ="server"  class="fancybox-item fancybox-close" ></a>
                        </div>
			            </asp:Panel>


                      <%-- เลือกตำแหน่งที่สามารถเข้าตำแหน่งนี้ได้   LEFT --%>
		                <asp:Panel CssClass="modal" style="top:10%;left:30%; position:fixed; width:1200px; "  id="ModalPost" runat="server" DefaultButton="btnSearchPos"  >
                        <div style="padding: 0px; width: auto;  margin-top :0px; margin-bottom :0px; top: 0px; left: 0px;" class="fancybox-skin">
                        <div class="fancybox-outer">
				            <div class="modal-header">										
					            <h3><asp:Label ID="lblHeader_ModalPost" runat="server"></asp:Label></h3>
					            <asp:Label ID="lblPosCurrent" runat="server" style="display:none;"></asp:Label>
                                <asp:Label ID="lblPosPath" runat="server" style="display:none;"></asp:Label>
				            </div>
				            <div class="modal-body"  >					            
					            <div class="row-fluid form-horizontal">
					                 
									<asp:Button ID="btnSearchPos" OnClick="btnSearchPos_Click" runat="server" Text="" style="display:none;" />
                                    <div class="span4 ">
											<div class="control-group">
												<label class="control-label"><i class="icon-sitemap"></i> ฝ่าย</label>
												<div class="controls">
													<asp:DropDownList ID="ddl_Search_Sector" OnSelectedIndexChanged="btnSearchPos_Click" runat="server" AutoPostBack="true" CssClass="medium m-wrap">
													</asp:DropDownList>
												</div>
											</div>
										</div>
									<div class="span4 ">
									<div class="control-group">
										<label class="control-label"><i class="icon-sitemap"></i> หน่วยงาน</label>
										<div class="controls">
											<asp:TextBox ID="txt_Search_Name" OnTextChanged="btnSearchPos_Click" runat="server" AutoPostBack="true" CssClass="m-wrap medium" placeholder="ค้นหาจากฝ่าย/หน่วยงาน"></asp:TextBox>
										</div>
									</div>
								</div>
													    
								<div class="span4 ">
									<div class="control-group">
										<label class="control-label"><i class="icon-briefcase"></i> ตำแหน่ง</label>
										<div class="controls">
											<asp:TextBox ID="txtSearchPos" OnTextChanged="btnSearchPos_Click" runat="server" AutoPostBack="true" CssClass="m-wrap medium" placeholder="ค้นหาจากเลขตำแหน่ง/ชื่อตำแหน่ง"></asp:TextBox>
										</div>
									</div>														        
								</div>	
                                <div class="span4 ">
									<div class="control-group">
										<label class="control-label"><i class="icon-briefcase"></i> เลือกทั้งหมด</label>
										<div class="controls">
											<asp:ImageButton ID="btnSelectAll" runat="server" ImageUrl="images/none.png" Height="24px" OnClick="btnOKAll_Click" ToolTip="Click เพื่อเปลี่ยน" />
                                            

                                            <asp:ConfirmButtonExtender ID="btnDeleteLeft_Confirm" runat="server"  ConfirmText="ลบตำแหน่งที่เลือก ?" TargetControlID="btnSelectAll"></asp:ConfirmButtonExtender>
										</div>
									</div>														        
								</div>  								       
					            </div>
					            <div class="portlet-body no-more-tables">
    								
								    <asp:Label ID="lblCountPosNo" runat="server" Font-Size="14px" Font-Bold="true"></asp:Label>
                                    
								    <table class="table table-full-width  no-more-tables table-hover">
									    <thead>
										    <tr>

                                                <th style="text-align:center;"><i class="icon-sitemap"></i> ฝ่าย</th>
											    <th style="text-align:center;"><i class="icon-sitemap"></i> หน่วยงาน</th>
											    <th style="text-align:center;"><i class="icon-bookmark"></i> ระดับ</th>
											    <th style="text-align:center;"><i class="icon-briefcase"></i> ตำแหน่ง</th>
											    <th style="text-align:center;"><i class="icon-bookmark"></i> ตำแหน่งการบริหาร</th>											
											    <%--<th style="text-align:center;"><i class="icon-user"></i> ผู้ครองตำแหน่ง</th>
											    <th style="text-align:center;"><i class="icon-caret-right"></i> รหัส</th>
											    <th style="text-align:center;"><i class="icon-book"></i> ชื่อวิชา</th>	--%>
											    <th style="text-align:center;"><i class="icon-check"></i> เลือก</th>
										    </tr>										    
									    </thead>
									    <tbody>
										    <asp:Repeater ID="rptPosDialog" OnItemCommand="rptPosDialog_ItemCommand" OnItemDataBound="rptPosDialog_ItemDataBound" runat="server">
									            <ItemTemplate>
    									            <tr>                                        
											            <td ><asp:Label ID="lblSector" runat="server"></asp:Label></td>
											            <td ><asp:Label ID="lblDept" runat="server"></asp:Label></td>		
                                                        <td style="text-align:center;"><asp:Label ID="lblClass" runat="server"></asp:Label></td>	
                                                        <td ><asp:Label ID="lblPosNo" runat="server"></asp:Label></td>	
                                                        <td ><asp:Label ID="lblMGR" runat="server"></asp:Label>
                                                            <asp:Label ID="lblPSN" runat="server" style="display:none;"></asp:Label>
                                                        </td>							           
											            <td data-title="ดำเนินการ" style="text-align:center; ">
                                                            <asp:ImageButton ID="btnSelectPos" runat="server" ImageUrl="images/none.png" Height="24px" CommandName="SelectPos" ToolTip="Click เพื่อเปลี่ยน" />

											                <asp:Button ID="btnPosSelect" runat="server" CssClass="btn mini blue" Visible ="false"  CommandName="Select" Text="เลือก" /> 
                                                            
										                </td>
										            </tr>	
									            </ItemTemplate>
									        </asp:Repeater>					
									    </tbody>
								    </table>
    								
								    <asp:PageNavigation ID="Pager_Pos" OnPageChanging="Pager_Pos_PageChanging" MaximunPageCount="10" PageSize="5" runat="server" />
							    </div>				            
				            </div>
                            </div>
                            <a title="ลบ" id="btnModalPost_Close" onserverclick="btnModalPost_Close_ServerClick" runat ="server"  class="fancybox-item fancybox-close" ></a>
                        </div>
			            </asp:Panel>
     			
                
                
                      <%-- เลือกตำแหน่งตั้งต้นเพื่อ COPY --%>
		                <asp:Panel CssClass="modal" style="top:10%;left:30%; position:fixed; width:1200px; "  id="Modal_CopyPOS" runat="server" DefaultButton="btnSearch_CopyPOS"  Visible ="false"  >
                        <div style="padding: 0px; width: auto;  margin-top :0px; margin-bottom :0px; top: 0px; left: 0px;" class="fancybox-skin">
                        <div class="fancybox-outer">
				            <div class="modal-header">										
					            <h3><asp:Label ID="lblTitle_CopyPOS" runat="server">เลือกตำแหน่งตั้งต้นที่ต้องการคัดลอก</asp:Label></h3>
					            <asp:Label ID="lblPOS_CopyPOS" runat="server" style="display:none;"></asp:Label>
                                <asp:Label ID="lblName_CopyPOS" runat="server" style="display:none;"></asp:Label>
				            </div>
				            <div class="modal-body"  >					            
					            <div class="row-fluid form-horizontal">
									<asp:Button ID="btnSearch_CopyPOS" OnClick="btnSearch_CopyPOS_Click" runat="server" Text="" style="display:none;" />
                                    <div class="span4 ">
											<div class="control-group">
												<label class="control-label"><i class="icon-sitemap"></i> ฝ่าย</label>
												<div class="controls">
													<asp:DropDownList ID="ddl_Search_Sector_CopyPOS" OnSelectedIndexChanged="btnSearch_CopyPOS_Click" runat="server" AutoPostBack="true" CssClass="medium m-wrap">
													</asp:DropDownList>
												</div>
											</div>
										</div>
									<div class="span4 ">
									<div class="control-group">
										<label class="control-label"><i class="icon-sitemap"></i> หน่วยงาน</label>
										<div class="controls">
											<asp:TextBox ID="txt_Search_Name_CopyPOS" OnTextChanged="btnSearch_CopyPOS_Click" runat="server" AutoPostBack="true" CssClass="m-wrap medium" placeholder="ค้นหาจากฝ่าย/หน่วยงาน"></asp:TextBox>
										</div>
									</div>
								</div>
													    
								<div class="span4 ">
									<div class="control-group">
										<label class="control-label"><i class="icon-briefcase"></i> ตำแหน่ง</label>
										<div class="controls">
											<asp:TextBox ID="txtSearch_CopyPOS" OnTextChanged="btnSearch_CopyPOS_Click" runat="server" AutoPostBack="true" CssClass="m-wrap medium" placeholder="ค้นหาจากเลขตำแหน่ง/ชื่อตำแหน่ง"></asp:TextBox>
										</div>
									</div>														        
								</div>  								       
					            </div>
					            <div class="portlet-body no-more-tables">
    								
								    <asp:Label ID="Label4" runat="server" Font-Size="14px" Font-Bold="true"></asp:Label>
                                    
								    <table class="table table-full-width  no-more-tables table-hover">
									    <thead>
										    <tr>

                                                <th style="text-align:center;"><i class="icon-sitemap"></i> ฝ่าย</th>
											    <th style="text-align:center;"><i class="icon-sitemap"></i> หน่วยงาน</th>
											    <th style="text-align:center;"><i class="icon-bookmark"></i> ระดับ</th>
											    <th style="text-align:center;"><i class="icon-briefcase"></i> ตำแหน่ง</th>
											    <th style="text-align:center;"><i class="icon-bookmark"></i> ตำแหน่งการบริหาร</th>
											    <th style="text-align:center;"><i class="icon-check"></i> เลือก</th>
										    </tr>										    
									    </thead>
									    <tbody>
										    <asp:Repeater ID="rptCopyPOSDialog" OnItemCommand="rptCopyPOSDialog_ItemCommand" OnItemDataBound="rptCopyPOSDialog_ItemDataBound" runat="server">
									            <ItemTemplate>
    									            <tr>                                        
											            <td ><asp:Label ID="lblSector" runat="server"></asp:Label></td>
											            <td ><asp:Label ID="lblDept" runat="server"></asp:Label></td>		
                                                        <td style="text-align:center;"><asp:Label ID="lblClass" runat="server"></asp:Label></td>	
                                                        <td ><asp:Label ID="lblPosNo" runat="server"></asp:Label></td>	
                                                        <td ><asp:Label ID="lblMGR" runat="server"></asp:Label>
                                                            <asp:Label ID="lblPSN" runat="server" style="display:none;"></asp:Label>
                                                        </td>							           
											            <td data-title="ดำเนินการ" style="text-align:center; ">
											                <asp:Button ID="btnPosSelect" runat="server" CssClass="btn mini blue" Visible ="True"  CommandName="Select" Text="เลือก" /> 
                                                            
										                </td>
										            </tr>	
									            </ItemTemplate>
									        </asp:Repeater>					
									    </tbody>
								    </table>
    								
								    <asp:PageNavigation ID="Pager_CopyPos" OnPageChanging="Pager_CopyPos_PageChanging" MaximunPageCount="10" PageSize="5" runat="server" />
							    </div>				            
				            </div>
                            </div>
                            <a title="ลบ" id="btnModalCopyPost_Close" onserverclick="btnModalCopyPost_Close_ServerClick" runat ="server"  class="fancybox-item fancybox-close" ></a>
                        </div>
			            </asp:Panel>
                
                
                 
</div>
</ContentTemplate>
</asp:UpdatePanel>
<script>
    jQuery(document).ready(function () {
        // initiate layout and plugins
        App.init();
        Gallery.init();
        $('.fancybox-video').fancybox({ type: 'iframe' });
    });
	</script>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ScriptPlaceHolder" Runat="Server">
</asp:Content>





using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Xml.Linq;
using System.Diagnostics;
using System.Collections.Specialized;
using System.Configuration;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Caching;
using System.Web.SessionState;
using System.Web.Security;
using System.Web.Profile;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
namespace VB
{


	public partial class Assessment_Team_COMP_Assessment : System.Web.UI.Page
	{


		HRBL BL = new HRBL();
		GenericLib GL = new GenericLib();

		textControlLib TC = new textControlLib();
		public int R_Year {
			get {
				try {
					return GL.CINT( GL.SplitString(ddlRound.Items[ddlRound.SelectedIndex].Value, "-")[0]);
				} catch (Exception ex) {
					return 0;
				}
			}
		}

		public int R_Round {
			get {
				try {
					return GL.CINT(GL.SplitString(ddlRound.Items[ddlRound.SelectedIndex].Value, "-")[1]);
				} catch (Exception ex) {
					return 0;
				}
			}
		}

		public string PSNL_NO {
			get { return lblPSNName.Attributes["PSNL_NO"]; }
			set { lblPSNName.Attributes["PSNL_NO"] = value; }
		}

		public string ASSESSOR_BY {
			get {
				try {
					string[] tmp = Strings.Split(ddlASSName.Items[ddlASSName.SelectedIndex].Value, ":::");
					return tmp[0];
				} catch (Exception ex) {
					return "";
				}
			}
		}

		public string ASSESSOR_NAME {
			get {
				try {
					return ddlASSName.Items[ddlASSName.SelectedIndex].Text;
				} catch (Exception ex) {
					return "";
				}
			}
		}

		public string ASSESSOR_POS {
			get {
				try {
					string[] tmp = Strings.Split(ddlASSName.Items[ddlASSName.SelectedIndex].Value, ":::");
					return tmp[1];
				} catch (Exception ex) {
					return "";
				}
			}
		}

		public string ASSESSOR_DEPT {
			get {
				try {
					string[] tmp = Strings.Split(ddlASSName.Items[ddlASSName.SelectedIndex].Value, ":::");
					return tmp[2];
				} catch (Exception ex) {
					return "";
				}
			}
		}


		public int COMP_Status {
            get { return GL.CINT(lblCOMPStatus.Attributes["COMP_Status"]); }
            set { lblCOMPStatus.Attributes["COMP_Status"] = value.ToString() ; }
		}

		public int PSNL_CLASS_GROUP {
			get {
				try {
					return Convert.ToInt32(Conversion.Val(lblPSNName.Attributes["CLSGP_ID"]));
				} catch (Exception ex) {
					return 0;
				}
			}
			set { lblPSNName.Attributes["CLSGP_ID"] = value.ToString(); }
		}

		public int PSNL_TYPE {
			get {
				try {
					return Convert.ToInt32(Conversion.Val(lblPSNType.Attributes["PSNL_Type_Code"]));
				} catch (Exception ex) {
					return -1;
				}
			}
            set { lblPSNType.Attributes["PSNL_Type_Code"] = value.ToString(); }
		}

		public string FN_CODE {
			get {
				try {
					return lblPSNPos.Attributes["FN_CODE"];
				} catch (Exception ex) {
					return "";
				}
			}
            set { lblPSNPos.Attributes["FN_CODE"] = value.ToString(); }
		}

		public string FN_Type {
			get {
				try {
					return lblPSNType.Attributes["FN_Type"];
				} catch (Exception ex) {
					return "";
				}
			}
            set { lblPSNType.Attributes["FN_Type"] = value.ToString(); }
		}

		public string FN_ID {
			get {
				try {
					return FN_CODE;
				} catch (Exception ex) {
					return "";
				}
			}
		}

		//------------------- For Behavior Setting-------------
		public int COMP_Type_Id {
            get { return GL.CINT(txtCOMPTypeID.Text); }
			set { txtCOMPTypeID.Text = value.ToString(); }
		}

		public int Master_No {
			get { return Convert.ToInt32(Conversion.Val(txtMasterNo.Text)); }
			set { txtMasterNo.Text = value.ToString(); }
		}

		public int BHV_No {
			get { return GL.CINT (txtBehaviorNo.Text); }
			set { txtBehaviorNo.Text = value.ToString(); }
		}

        public string GAP_ID
        {
            get { return txtGAPID.Text; }
            set { txtGAPID.Text = value.ToString(); }
        }
		public string GAP_Name {
			get { return txtGAPName.Text; }
			set { txtGAPName.Text = value.ToString (); }
		}

        public string DEPT_CODE
        {
            get
            {
                try
                {
                    return lblPSNType.Attributes["DEPT_CODE"];
                }
                catch (Exception ex)
                {
                    return "";
                }
            }
            set { lblPSNType.Attributes["DEPT_CODE"] = value.ToString(); }
        }

        public bool chkIsEditable
        {    
             get
            {                
                 return Convert.ToBoolean(lblPSNType.Attributes["chkIsEditable"]);           
            }
        }

		protected void Page_Load(object sender, System.EventArgs e)
		{
			if ((Session["USER_PSNL_NO"] == null)) {
				ScriptManager.RegisterStartupScript(this.Page, typeof(string), "Alert", "showAlert('กรุณาลงชื่อเข้าใช้ระบบ');", true);
				Response.Redirect("Login.aspx");
				return;
			}

			if (!IsPostBack) {
                // Keep Start Time
                DateTime StartTime = DateTime.Now;

                BL.BindDDlYearRound(ddlRound);
                
				BindDDlAssessmentStatus();
                chk_CreateHR_Actual_Assessor();
                //----------------- ทำสองที่ Page Load กับ Update Status ---------------
                Update_COMP_Status_All_Team(); //ถ้ายังช้า ลบบรรทัดนี้ออก
				BindPersonalList();

				pnlList.Visible = true;
				pnlEdit.Visible = false;

                //Report Time To Entry This Page
                DateTime EndTime = DateTime.Now;
                lblReportTime.Text = "เริ่มเข้าสู่หน้านี้ " + StartTime.ToString("HH:mm:ss.ff") + " ถึง " + EndTime.ToString("HH:mm:ss.ff") + " ใช้เวลา " + Math.Round( GL.DiffTimeDecimalSeconds(StartTime,EndTime),2) + " วินาที";
			}
		}
        
        private void Update_COMP_Status_All_Team()
        {
            DataTable dt = GetData();
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                string _psnlNo = dt.Rows[i]["PSNL_NO"].ToString();
                BL.Update_COMP_Status_To_Assessment_Period(_psnlNo, R_Year, R_Round);
            }
        }

		private void BindDDlAssessmentStatus()
		{
			ddlStatus.Items.Clear();
			ddlStatus.Items.Add(new ListItem("ทั้งหมด", "0"));
			ddlStatus.Items.Add(new ListItem("ยังไม่ส่งผลการประเมิน", "1"));
			ddlStatus.Items.Add(new ListItem("อนุมัติผลการประเมินแล้ว", "2"));
		}

        private void chk_CreateHR_Actual_Assessor()
        {

            Boolean IsCompleted = BL.Is_Round_Completed(R_Year, R_Round);
            if (!IsCompleted)
            {
                SqlDataAdapter DA;
                DataTable PSN = new DataTable();
                string SQL = "";
                SQL += " select PSNL_NO from tb_HR_Actual_Assessor where MGR_PSNL_NO ='" + Session["USER_PSNL_NO"].ToString().Replace("'", "''") + "' AND R_Year='" + R_Year + "' AND R_Round ='" + R_Round + "' \n";
                SQL += " AND  PSNL_NO NOT IN (select PSN_PSNL_NO from vw_HR_ASSESSOR_PSN where MGR_PSNL_NO ='" + Session["USER_PSNL_NO"].ToString().Replace("'", "''") + "'  AND R_Year='" + R_Year + "'  AND R_Round ='" + R_Round + "') \n";
                DA = new SqlDataAdapter(SQL, BL.ConnectionString());
                DA.Fill(PSN);
                if (PSN.Rows.Count > 0)
                {
                    for (int i = 0; i < PSN.Rows.Count; i++)
                    {
                        BL.CreateHR_Actual_Assessor(R_Year, R_Round, PSN.Rows[i]["PSNL_NO"].ToString(), Session["USER_PSNL_NO"].ToString().Replace("'", "''"));
                    }
                }
            }

        }

		protected void Search(object sender, System.EventArgs e)
		{
            BindPersonalList();
            Update_COMP_Status_All_Team();
            BindPersonalList();

			if (pnlEdit.Visible) {
				BindPersonal();
				BindAssessor();
				BindMasterCOMP();

				SetPrintButton();
			}
		}

		private void SetPrintButton()
		{
			btnCOMPFormPDF.HRef = "Print/RPT_C_PNSAss.aspx?MODE=PDF&R_Year=" + R_Year + "&R_Round=" + R_Round + "&PSNL_No=" + PSNL_NO + "&Status=" + GL.CINT(COMP_Status);
            //btnCOMPFormExcel.HRef = "Print/RPT_C_PNSAss.aspx?MODE=EXCEL&R_Year=" + R_Year + "&R_Round=" + R_Round + "&PSNL_No=" + PSNL_NO + "&Status=" + GL.CINT(COMP_Status);
            btnCOMPFormExcel.HRef = "Print/RPT_C_PNSAss.aspx?MODE=EXCEL&R_Year=" + R_Year + "&R_Round=" + R_Round + "&PSNL_No=" + PSNL_NO + "&Status=" + COMP_Status;


			btnIDPFormPDF.HRef = "Print/RPT_IDP_PSN.aspx?MODE=PDF&R_Year=" + R_Year + "&R_Round=" + R_Round + "&PSNL_No=" + PSNL_NO + "&Status=" + COMP_Status;
			btnIDPFormExcel.HRef = "Print/RPT_IDP_PSN.aspx?MODE=EXCEL&R_Year=" + R_Year + "&R_Round=" + R_Round + "&PSNL_No=" + PSNL_NO + "&Status=" + COMP_Status;

            btnIDPResultFormPDF.HRef = "Print/RPT_IDP_PSN_Result.aspx?MODE=PDF&R_Year=" + R_Year + "&R_Round=" + R_Round + "&PSNL_No=" + PSNL_NO + "&Status=" + COMP_Status;
            btnIDPResultFormExcel.HRef = "Print/RPT_IDP_PSN_Result.aspx?MODE=EXCEL&R_Year=" + R_Year + "&R_Round=" + R_Round + "&PSNL_No=" + PSNL_NO + "&Status=" + COMP_Status;

		}

        public DataTable GetData()
        {
            string SQL = "";
            SQL += " DECLARE @R_Year As Int=" + R_Year + "\n";
            SQL += " DECLARE @R_Round As Int=" + R_Round + "\n";
            SQL += " DECLARE @ASSESSOR_CODE AS nvarchar(50)='" + Session["USER_PSNL_NO"].ToString().Replace("'", "''") + "'\n";
            SQL += " \n\n";
            SQL += " SELECT DISTINCT COMP.* FROM\n";
            SQL += " (";
            SQL += " SELECT DISTINCT PSN_DEPT_CODE,PSN.PSN_DEPT_Name DEPT_Name,PSN.PSN_PSNL_NO PSNL_NO,PSN.PSN_PSNL_Fullname PSNL_Fullname,PSN.PSN_MGR_NAME POS_Name,PSN.PSN_PNPS_CLASS PSNL_CLASS,Result.POS_No,\n";
            SQL += "  ISNULL(Header.COMP_Status,0) COMP_Status,dbo.udf_GetAssessmentStatusName(Header.COMP_Status) COMP_Status_Name,Result.RESULT,Result .ASS_Status\n";
            SQL += "  FROM vw_HR_ASSESSOR_PSN PSN\n";
            SQL += "  INNER JOIN vw_RPT_COMP_Result Result ON PSN.PSN_PSNL_NO=Result.PSNL_NO \n";
            SQL += "   \tAND Result.R_Year=PSN.R_Year AND Result.R_Round=PSN.R_Round\n";
            SQL += " \tAND PSN.MGR_PSNL_NO=@ASSESSOR_CODE\n";
            SQL += "  INNER JOIN tb_HR_COMP_Header Header ON Header.R_Year=Result.R_Year AND Header.R_Round=Result.R_Round AND Header.PSNL_NO=Result.PSNL_NO \n";
            SQL += "  WHERE PSN.R_Year=@R_Year AND PSN.R_Round=@R_Round\n";
            SQL += " \n";

            SQL += " ) COMP\n";
            SQL += "  LEFT JOIN vw_PN_PSNL_ALL PSN_ALL ON COMP.PSNL_NO=PSN_ALL.PSNL_NO \n";
            SQL += " WHERE 1=1 \n";
            SQL += "  AND (ISNULL(PSN_ALL.PNPS_RESIGN_DATE,PSN_ALL.PNPS_RETIRE_DATE) > (SELECT MAX(R_End) FROM tb_HR_Round WHERE R_Year=@R_Year AND R_Round=@R_Round)) \n";
            if (!string.IsNullOrEmpty(txtName.Text))
            {
                SQL += " AND (COMP.PSNL_NO LIKE '%" + txtName.Text.Replace("'", "''") + "%' OR COMP.PSNL_Fullname LIKE '%" + txtName.Text.Replace("'", "''") + "%')";
            }
            switch (ddlStatus.SelectedIndex)
            {
                case 1:
                    SQL += " AND ( ASS_Status IS NULL OR ASS_Status<5)";
                    break;
                case 2:
                    SQL += " AND  ASS_Status =5";
                    break;
            }

            SQL += " ORDER BY PSN_DEPT_CODE,PSNL_CLASS DESC,COMP.POS_No\n";


            SqlDataAdapter DA = new SqlDataAdapter(SQL, BL.ConnectionString());
            DataTable DT = new DataTable();
            DA.SelectCommand.CommandTimeout = 200;
            LastOrgranizeName = "";

            if (ddlRound.SelectedIndex > 0)
            {
                DA.Fill(DT);


            }
            Session["Assessment_Team_COMP_Assessment"] = DT;
            return DT;
        }

		private void BindPersonalList()
		{
		
			Pager.SesssionSourceName = "Assessment_Team_COMP_Assessment";
			Pager.RenderLayout();
            DataTable DT = new DataTable();
            DT = GetData();
			if (DT.Rows.Count == 0) {
				lblCountPSN.Text = "ไม่พบรายการดังกล่าว";
			} else {
				lblCountPSN.Text = "พบ " + GL.StringFormatNumber(DT.Rows.Count.ToString(), 0).ToString() + " รายการ";
			}
		}

		protected void Pager_PageChanging(PageNavigation Sender)
		{
			Pager.TheRepeater = rptCOMP;
		}

		protected void rptCOMP_ItemCommand(object source, System.Web.UI.WebControls.RepeaterCommandEventArgs e)
		{
			switch (e.CommandName) {
				case "Edit":
                    Button btnPSNEdit = (Button)e.Item.FindControl("btnPSNEdit");
                    Label lblPSNCOMPStatus = (Label)e.Item.FindControl("lblPSNCOMPStatus");
                    COMP_Status =GL.CINT(lblPSNCOMPStatus.Attributes["COMP_Status"]);
					pnlList.Visible = false;
					pnlEdit.Visible = true;
					//-------------- ดึงข้อมูลบุคคล ------------
					PSNL_NO = btnPSNEdit.CommandArgument;
					BindPersonal();
                    First_AVG_Weight();
					BindAssessor();
					BindMasterCOMP();
					SetPrintButton();

					break;
			}
		}

		string LastOrgranizeName = "";

		protected void rptCOMP_ItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
		{
			if (e.Item.ItemType != ListItemType.AlternatingItem & e.Item.ItemType != ListItemType.Item)
				return;

            DataRowView drv = (DataRowView)e.Item.DataItem;


            Label lnkPSNOrganize = (Label)e.Item.FindControl("lnkPSNOrganize");
            Label lblPSNNo = (Label)e.Item.FindControl("lblPSNNo");
            Label lblPSNName = (Label)e.Item.FindControl("lblPSNName");
            Label lblPSNPos = (Label)e.Item.FindControl("lblPSNPos");
            Label lblPSNClass = (Label)e.Item.FindControl("lblPSNClass");
            Label lblPSNCOMPStatus = (Label)e.Item.FindControl("lblPSNCOMPStatus");
            Label lblResult = (Label)e.Item.FindControl("lblResult");
            Label lblPercent = (Label)e.Item.FindControl("lblPercent");
            Button btnPSNEdit = (Button)e.Item.FindControl("btnPSNEdit");

            if (Convert.ToString(drv["DEPT_NAME"]) != LastOrgranizeName)
            {
                lnkPSNOrganize.Text = drv["DEPT_NAME"].ToString();
                LastOrgranizeName = drv["DEPT_NAME"].ToString();
			}

			lblPSNNo.Text =drv["PSNL_NO"].ToString();
			lblPSNName.Text = drv["PSNL_Fullname"].ToString();
			if (!GL.IsEqualNull(drv["POS_NAME"])) {
                lblPSNPos.Text = drv["POS_NAME"].ToString();
			}

			lblPSNClass.Text = GL.CINT (drv["PSNL_CLASS"]).ToString ();
            lblPSNCOMPStatus.Text = drv["COMP_Status_Name"].ToString();

			//If GL.IsEqualNull(drv["COMP_Status"]) OrElse drv["COMP_Status"] < HRBL.AssessmentStatus.AssessmentCompleted Then
			//    lblPSNCOMPStatus.ForeColor = Drawing.Color.Red
			//Else
			//    lblPSNCOMPStatus.ForeColor = Drawing.Color.Green
			//End If

            lblPSNCOMPStatus.Attributes["COMP_Status"] = GL.CINT(drv["COMP_Status"]).ToString();

            //------ปรับสีสถานะ-----------
            //----<0
            if (GL.IsEqualNull(drv["COMP_Status"]) || GL.CINT(drv["COMP_Status"]) == HRBL.AssessmentStatus.Creating)
            {
                lblPSNCOMPStatus.ForeColor = System.Drawing.Color.Red;
                //----1
            }
            else if (GL.IsEqualNull(drv["COMP_Status"]) || GL.CINT(drv["COMP_Status"]) == HRBL.AssessmentStatus.WaitCreatingApproved)
            {
                lblPSNCOMPStatus.ForeColor = System.Drawing.Color.DarkRed;
                //----2
            }
            else if (GL.IsEqualNull(drv["COMP_Status"]) || GL.CINT(drv["COMP_Status"]) == HRBL.AssessmentStatus.CreatedApproved)
            {
                lblPSNCOMPStatus.ForeColor = System.Drawing.Color.Orange;
                //----3
            }
            else if (GL.IsEqualNull(drv["COMP_Status"]) || GL.CINT(drv["COMP_Status"]) == HRBL.AssessmentStatus.WaitAssessment)
            {
                lblPSNCOMPStatus.ForeColor = System.Drawing.Color.DarkBlue;
                //----4
            }
            else if (GL.IsEqualNull(drv["COMP_Status"]) || GL.CINT(drv["COMP_Status"]) == HRBL.AssessmentStatus.WaitConfirmAssessment)
            {
                lblPSNCOMPStatus.ForeColor = System.Drawing.Color.BlueViolet;
                //----5
            }
            else if (GL.IsEqualNull(drv["COMP_Status"]) || GL.CINT(drv["COMP_Status"]) == HRBL.AssessmentStatus.AssessmentCompleted)
            {
                lblPSNCOMPStatus.ForeColor = System.Drawing.Color.Green;
            }

			if (!GL.IsEqualNull(drv["RESULT"])) {
				lblResult.Text = GL.StringFormatNumber(drv["RESULT"].ToString());
				lblPercent.Text = GL.StringFormatNumber((GL.CDBL(drv["RESULT"].ToString()) / 5).ToString()) + " %";
				//--------------------ปรับสีคะแนนที่หน้าจอประเมินของผู้ประเมิน ให้แสดงเหมือนกับสีของสถานะ --------
				lblResult.ForeColor = lblPSNCOMPStatus.ForeColor;
				lblPercent.ForeColor = lblPSNCOMPStatus.ForeColor;
			} else {
				lblResult.Text = "-";
				lblPercent.Text = "-";
			}

            btnPSNEdit.CommandArgument = drv["PSNL_NO"].ToString();

		}

        private void First_AVG_Weight()  //เข้ามาครั้งแรก ระบบเฉลี่ยให้ Auto
        {
            if (COMP_Status == 0 | GL.CINT(COMP_Status) == -1)
            {
                string SQL = "  SELECT COMP_Status \n";
                SQL += " FROM tb_HR_COMP_Header \n";
                SQL += " WHERE R_Year =" + R_Year + " And R_Round =" + R_Round + " AND PSNL_NO='" + PSNL_NO.Replace("'", "''") + "'\n";
                SqlDataAdapter DA = new SqlDataAdapter(SQL, BL.ConnectionString());
                DataTable DT = new DataTable();
                DA.Fill(DT);
                if ((DT.Rows.Count > 0) && GL.IsEqualNull(DT.Rows[0]["COMP_Status"]))
                {
                    AVG_Weight();  //เฉลี่ย นน.
                }

            }

        }
        private void AVG_Weight()
        {
            DataTable DT = BL.GetCOMPBundle(PSNL_NO, R_Year, R_Round, PSNL_CLASS_GROUP, PSNL_TYPE, FN_ID, FN_Type, HRBL.CompetencyType.All);
            DT.DefaultView.Sort = " COMP_Type_Sort DESC ,Master_No DESC";
            DT = DT.DefaultView.ToTable();
            if (DT.Rows.Count > 0)
            {
                double AVG_Weight = (GL.CDBL(100) / GL.CDBL(DT.Rows.Count));
                Double SUM_Weight = 0;
                Double CDBL_MOD = (GL.CDBL(GL.StringFormatNumber(GL.CDBL(AVG_Weight), 2)) % GL.CDBL(0.50));

                for (int i = 0; i <= DT.Rows.Count - 1; i++)
                {
                    //------Save Detail (AVG Weight)-------
                    string SQL = "SELECT * FROM tb_HR_COMP_Detail ";
                    SQL += " WHERE PSNL_NO='" + PSNL_NO.Replace("'", "''") + "'\n";
                    SQL += " AND R_Year=" + R_Year + "\n";
                    SQL += " AND R_Round=" + R_Round + "\n";
                    SQL += " AND COMP_Type_Id=" + DT.Rows[i]["COMP_Type_Id"] + "\n";
                    SQL += " AND Master_No=" + DT.Rows[i]["Master_No"] + "\n";

                    SqlDataAdapter DA = new SqlDataAdapter(SQL, BL.ConnectionString());
                    DataTable DTAVG = new DataTable();
                    DA.Fill(DTAVG);

                    DataRow DR = null;
                    if (DTAVG.Rows.Count == 0)
                    {
                        DR = DTAVG.NewRow();
                        DR["PSNL_NO"] = PSNL_NO;
                        DR["R_Year"] = R_Year;
                        DR["R_Round"] = R_Round;
                        DR["COMP_Type_Id"] = DT.Rows[i]["COMP_Type_Id"];
                        DR["Master_No"] = DT.Rows[i]["Master_No"];
                        DR["Create_By"] = Session["USER_PSNL_NO"];
                        DR["Create_Time"] = DateAndTime.Now;
                    }
                    else
                    {
                        DR = DTAVG.Rows[0];
                    }
                    DR["COMP_Comp"] = DT.Rows[i]["COMP_Comp"].ToString();
                    DR["COMP_Std"] = DT.Rows[i]["COMP_Std"].ToString();
                    for (int j = 1; j <= 5; j++)
                    {
                        DR["COMP_Choice_" + j] = DT.Rows[i]["COMP_Choice_" + j];
                    }
                    if (GL.CINT(DT.Rows[i]["COMP_No"]) != 1)
                    {
                        if (GL.CDBL(CDBL_MOD) == 0)
                        {
                            DR["COMP_Weight"] = GL.StringFormatNumber(AVG_Weight);
                            SUM_Weight += GL.CDBL(GL.StringFormatNumber(AVG_Weight));
                        }
                        else
                        {
                            DR["COMP_Weight"] = Math.Floor(AVG_Weight);
                            SUM_Weight += Math.Floor(AVG_Weight);
                        }
                    }
                    else
                    {
                        DR["COMP_Weight"] = (GL.CDBL(100) - GL.CDBL(SUM_Weight));
                    }
                    DR["Update_By"] = Session["USER_PSNL_NO"];
                    DR["Update_Time"] = DateAndTime.Now;
                    if (DTAVG.Rows.Count == 0)
                        DTAVG.Rows.Add(DR);
                    SqlCommandBuilder cmd = new SqlCommandBuilder(DA);
                    DA.Update(DTAVG);

                }

            }
            else
            {
                return;
            }
            BindMasterCOMP();
        }


		private void BindPersonal()
		{
			//----------Personal Info ----------
			HRBL.PersonalInfo PSNInfo = BL.GetAssessmentPersonalInfo(R_Year, R_Round, PSNL_NO, COMP_Status);
			lblPSNName.Text = PSNInfo.PSNL_Fullname;
			lblPSNDept.Text = PSNInfo.DEPT_NAME;
			if (!string.IsNullOrEmpty(PSNInfo.MGR_NAME)) {
				lblPSNPos.Text = PSNInfo.MGR_NAME;
			} else {
				lblPSNPos.Text = PSNInfo.FN_NAME;
			}
			lblPSNType.Text = "พนักงาน " + PSNInfo.WAGE_NAME + " ระดับ " + Convert.ToInt32(PSNInfo.PNPS_CLASS);

			PSNL_CLASS_GROUP = GL.CINT ( PSNInfo.CLSGP_ID);
			PSNL_TYPE = GL.CINT (PSNInfo.PSNL_TYPE);
			FN_CODE = PSNInfo.FN_ID;
			//FN_CODE = PSNInfo.FN_CODE
			FN_Type = PSNInfo.FN_Type;
            DEPT_CODE = PSNInfo.DEPT_CODE;
		}

		private void BindAssessor()
		{
			BL.BindDDLAssessor(ddlASSName, R_Year, R_Round, PSNL_NO);
			ddlASSName_SelectedIndexChanged(null, null);
		}

		protected void ddlASSName_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			lblASSPos.Text = ASSESSOR_POS;
			lblASSDept.Text = ASSESSOR_DEPT;
			SaveHeader();
		}

		private void BindMasterCOMP()
		{
			COMP_Status = BL.GetCOMPStatus(R_Year, R_Round, PSNL_NO);
			lblCOMPStatus.Text = BL.GetAssessmentStatusName(COMP_Status);

            Boolean IsInPeriod = BL.IsTimeInAssessmentPeriod(DateTime.Now, R_Year, R_Round, HRBL.AssessmentType.Competency , COMP_Status,DEPT_CODE.ToString () );
            
            // หาว่ารอบปิดหรือยัง  
            Boolean IsRoundCompleted = BL.Is_Round_Completed(R_Year, R_Round);
            Boolean IsEditable = (COMP_Status > HRBL.AssessmentStatus.CreatedApproved) & (COMP_Status < HRBL.AssessmentStatus.AssessmentCompleted) & IsInPeriod & (!IsRoundCompleted);
            Boolean chkIsEditable = IsEditable;

            pnlEdit_Ass.Enabled = chkIsEditable;
            btnPreSend.Visible = chkIsEditable;
            btnSaveRemark.Visible = chkIsEditable;
            if (btnSaveRemark.Visible)
            {
                divFooter.Attributes["class"] = "Buttonfooter";
            }
            else {
                divFooter.Attributes["class"] = "";
            }

            ddlASSName.Enabled = chkIsEditable;
            
			//--------------- Bind Detail----------------------
			DataTable DT = BL.GetCOMPBundle(PSNL_NO, R_Year, R_Round, PSNL_CLASS_GROUP, PSNL_TYPE, FN_ID, FN_Type, HRBL.CompetencyType.All);
			Behavior = BL.GetCOMPBehavior(PSNL_NO, R_Year, R_Round).Table;

			//--------------- Calculate Final Score -------------
			DT.Columns.Add("FinalScore", typeof(Int32));
			for (int i = 0; i <= DT.Rows.Count - 1; i++) {
				int TotalBehavior = GL.CINT ( Behavior.Compute("COUNT(Master_No)", "COMP_Type_Id=" + DT.Rows[i]["COMP_Type_Id"].ToString() + " AND Master_No=" + DT.Rows[i]["Master_No"].ToString()));
				int SelectedChoice = 0;
				if (!GL.IsEqualNull(DT.Rows[i]["COMP_Answer_MGR"]))
					SelectedChoice =GL.CINT( DT.Rows[i]["COMP_Answer_MGR"].ToString());
				if (SelectedChoice >= 3) {
					DT.Rows[i]["FinalScore"] = (TotalBehavior >= SelectedChoice ? SelectedChoice : TotalBehavior);
				} else {
					DT.Rows[i]["FinalScore"] = SelectedChoice;
				}
			}

			//---------- Get GAP Data------------
			GAPData = BL.GetGAPDetailPSN(PSNL_NO, HRBL.CompetencyType.All, R_Year, R_Round);

			rptAss.DataSource = DT;
			rptAss.DataBind();

		}

		protected void rptAss_ItemCommand(object source, System.Web.UI.WebControls.RepeaterCommandEventArgs e)
		{

            HtmlTableCell ass_no = (HtmlTableCell)e.Item.FindControl("ass_no");

            DataTable DT;
            SqlCommandBuilder cmd;
            SqlDataAdapter DA;
            string SQL = "";



            TextBox txtMGR = (TextBox)e.Item.FindControl("txtMGR");

            switch (e.CommandName) {
				case "Select":                  
                    Button btn = (Button)e.CommandSource;
					int SelectedChoice = Convert.ToInt32(btn.ID.Replace("btn", ""));
                    int _COMP_Type_Id = GL.CINT(ass_no.Attributes["COMP_Type_Id"]);
                    int _Master_No = GL.CINT(ass_no.InnerHtml);
					 SQL = "SELECT * FROM tb_HR_COMP_Detail \n";
					SQL += " WHERE PSNL_NO='" + PSNL_NO.Replace("'", "''") + "' ";
					SQL += " AND R_Year=" + R_Year + "\n";
					SQL += " AND R_Round=" + R_Round + "\n";
                    SQL += " AND COMP_Type_Id=" + _COMP_Type_Id + "\n";
                    SQL += " AND Master_No=" + _Master_No + "\n";

					 DA = new SqlDataAdapter(SQL, BL.ConnectionString());
					 DT = new DataTable();
					DA.Fill(DT);

					if (DT.Rows.Count == 0)
						return;

					DT.Rows[0]["COMP_Answer_MGR"] = SelectedChoice;
					DT.Rows[0]["Update_By"] = Session["USER_PSNL_NO"];
					DT.Rows[0]["Update_Time"] = DateAndTime.Now;

					 cmd = new SqlCommandBuilder(DA);
					try {
						DA.Update(DT);
					} catch {
					}

                    //----------Show/Hide Behavior Auto Add Behavior--------------
                    int YearRound= (R_Year*10) + R_Round;
                    if (YearRound >= BL.Start_Skip_Behavior_Round())
                    {
                        BL.AutoAddBehavior(PSNL_NO, R_Year, R_Round, _COMP_Type_Id, _Master_No, SelectedChoice);
                    }

                    //Remark
                    SaveRemark();
					//------------- Refresh-----------
					BindMasterCOMP();

					break;
				case "AddBehavior":
                    Button btnAddBehavior = (Button)e.Item.FindControl("btnAddBehavior");
                    COMP_Type_Id = GL.CINT (ass_no.Attributes["COMP_Type_Id"]);
					Master_No = Convert.ToInt32(ass_no.InnerHtml);
					BHV_No = Convert.ToInt32(btnAddBehavior.CommandArgument) + 1;
					btnBehaviorDialog_Click(null, null);

					break;
				case "Remark":

                    if (txtMGR.Text != "")
                    {
                        if (txtMGR.Text.Length > 3990)
                        {
                            txtMGR.Text = txtMGR.Text.Substring(0, 3990);
                        }

                    }
                     SQL = "SELECT * FROM tb_HR_COMP_Detail \n";
                    SQL += " WHERE PSNL_NO='" + PSNL_NO.Replace("'", "''") + "' ";
                    SQL += " AND R_Year=" + R_Year + "\n";
                    SQL += " AND R_Round=" + R_Round + "\n";
                    SQL += " AND COMP_Type_Id=" + ass_no.Attributes["COMP_Type_Id"] + "\n";
                    SQL += " AND Master_No=" + ass_no.InnerHtml + "\n";

                     DA = new SqlDataAdapter(SQL, BL.ConnectionString());
                     DT = new DataTable();
                    DA.Fill(DT);

                    if (DT.Rows.Count == 0)
                        return;

                    DT.Rows[0]["COMP_Remark_MGR"] = txtMGR.Text;
                    DT.Rows[0]["Update_By"] = Session["USER_PSNL_NO"];
                    DT.Rows[0]["Update_Time"] = DateAndTime.Now;

                     cmd = new SqlCommandBuilder(DA);
                    try {
                        DA.Update(DT);
                    } catch {
                    }

                    //------------- Refresh-----------
                    BindMasterCOMP();
					break;

                case "AddGAP":

                    int GAP_COMP_Type = GL.CINT(ass_no.Attributes["COMP_Type_Id"]);
                    int GAP_Master_No = GL.CINT(ass_no.InnerHtml);

                    BindGAPList(GAP_COMP_Type, GAP_Master_No);
                    ModalGAP.Visible = true;
                    break;
			}
		}

		int LastType = 0;
			//----------------- To Bind Behavior ---------------
		DataTable Behavior = null;
		protected void rptAss_ItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
		{
			switch (e.Item.ItemType) {

				case ListItemType.AlternatingItem:
				case ListItemType.Item:

					HtmlTableRow row_COMP_type =(HtmlTableRow) e.Item.FindControl("row_COMP_type");
					HtmlTableCell cell_COMP_type =(HtmlTableCell) e.Item.FindControl("cell_COMP_type");

					HtmlTableCell ass_no =(HtmlTableCell)  e.Item.FindControl("ass_no");
					HtmlTableCell COMP_Name =(HtmlTableCell)  e.Item.FindControl("COMP_Name");
					HtmlTableCell target =(HtmlTableCell) e.Item.FindControl("target");
					HtmlTableCell weight =(HtmlTableCell) e.Item.FindControl("weight");
					HtmlTableCell cel_officer =(HtmlTableCell) e.Item.FindControl("cel_officer");
					HtmlTableCell cel_header =(HtmlTableCell) e.Item.FindControl("cel_header");
					HtmlTableCell mark =(HtmlTableCell) e.Item.FindControl("mark");
					//--------------- Textbox -------------
					TextBox txtPSN =(TextBox) e.Item.FindControl("txtPSN");
					TextBox txtMGR =(TextBox) e.Item.FindControl("txtMGR");
					Button btnRemark =(Button) e.Item.FindControl("btnRemark");

					//-------------------- Notification Handler ------------------
					Button btn_Cal_Bhv = (Button)e.Item.FindControl("btn_Cal_Bhv");
					Image imgAlert =(Image) e.Item.FindControl("imgAlert");
					Label lblAlert =(Label) e.Item.FindControl("lblAlert");

                    Image imgAlertGAP = (Image)e.Item.FindControl("imgAlertGAP");
                    Label lblAlertGAP = (Label)e.Item.FindControl("lblAlertGAP");

					//------------------ GAP -------------------
                    //Label lblGAPGuide =(Label) e.Item.FindControl("lblGAPGuide");

                    DataRowView drv = (DataRowView)e.Item.DataItem;
					ass_no.Attributes["COMP_Type_Id"] =GL.CINT ( drv["COMP_Type_Id"]).ToString ();
					if (LastType != GL.CINT ( drv["COMP_Type_Id"])) {
						switch (GL.CINT (drv["COMP_Type_Id"])) {
							case 1:
								cell_COMP_type.InnerHtml = "สมรรถนะหลัก (Core Competency)";
								break;
							case 2:
								cell_COMP_type.InnerHtml = "สมรรถนะตามสายงาน (Functional Competency)";
								break;
							case 3:
								cell_COMP_type.InnerHtml = "สมรรถนะตามสายระดับ (Managerial Competency)";
								break;
						}
						row_COMP_type.Visible = true;
						LastType = GL.CINT (drv["COMP_Type_Id"]);
					} else {
						row_COMP_type.Visible = false;
					}

					ass_no.InnerHtml = drv["Master_No"].ToString ();
					//.ToString().PadLeft(2, GL.chr0)
					COMP_Name.InnerHtml = drv["COMP_Comp"].ToString().Replace("\n", "<br>");
					target.InnerHtml = drv["COMP_Std"].ToString().Replace("\n", "<br>");
					weight.InnerHtml = drv["COMP_Weight"].ToString();

					//---------------------- Remark --------------------------
					txtPSN.Text = drv["COMP_Remark_PSN"].ToString();
					txtMGR.Text = drv["COMP_Remark_MGR"].ToString();

                    //txtMGR.Attributes["onchange"] = "document.getElementById('" + btnRemark.ClientID + "').click();";
                    txtMGR.ReadOnly = chkIsEditable;
					for (int i = 1; i <= 5; i++) {

                        HtmlTableCell choice = (HtmlTableCell)e.Item.FindControl("choice" + i.ToString());
                        HtmlTableCell selHeaderColor = (HtmlTableCell)e.Item.FindControl("selHeaderColor" + i.ToString());
                        Button btn = (Button)e.Item.FindControl("btn" + i.ToString());
						choice.InnerHtml = drv["COMP_Choice_" + i.ToString()].ToString().Replace("\n", "<br>");
						if (COMP_Status >= HRBL.AssessmentStatus.WaitAssessment & COMP_Status < HRBL.AssessmentStatus.AssessmentCompleted) {
							selHeaderColor.Attributes["onclick"] = "document.getElementById('" + btn.ClientID + "').click();";
							choice.Attributes["onclick"] = "document.getElementById('" + btn.ClientID + "').click();";
						} else {
							selHeaderColor.Attributes["title"] = "";
							choice.Attributes["title"] = "";
							selHeaderColor.Style["cursor"] = "Default";
							choice.Style["cursor"] = "Default";
						}
                        //btn.Enabled = chkIsEditable;

					}


					//---------------------- Display PSN Answer For Suggession ------------------------
					if (!GL.IsEqualNull(drv["COMP_Answer_PSN"])  && GL.CINT(drv["COMP_Answer_PSN"]) != 0) {
						cel_officer.Attributes["class"] = "AssLevel" + drv["COMP_Answer_PSN"];
						for (int j = 1; j <= GL.CINT ( drv["COMP_Answer_PSN"]); j++) {
                            HtmlTableCell _selOfficialColor = (HtmlTableCell)e.Item.FindControl("selOfficialColor" + j.ToString());
							_selOfficialColor.Attributes["class"] = "AssLevel" + drv["COMP_Answer_PSN"];
						}
						cel_officer.Attributes["COMP_Answer_PSN"] = GL.CINT (drv["COMP_Answer_PSN"]).ToString ();
					}

                    //------------------ Show/Hide Behavior-------------------
                    HtmlTableCell tdBehavior1 = (HtmlTableCell)e.Item.FindControl("tdBehavior1");
                    HtmlTableCell tdBehavior2 = (HtmlTableCell)e.Item.FindControl("tdBehavior2");
                    HtmlTableCell tdGAP1 = (HtmlTableCell)e.Item.FindControl("tdGAP1");
                    HtmlTableCell tdGAP2 = (HtmlTableCell)e.Item.FindControl("tdGAP2");
                    int YearRound = (R_Year * 10) + R_Round;
                    if (YearRound >= BL.Start_Skip_Behavior_Round())
                    {
                        tdBehavior1.Visible = false;
                        tdBehavior2.Visible = false;
                        tdGAP1.ColSpan = 2;
                        tdGAP2.ColSpan = 5;
                    }
                    else
                    {
                        tdBehavior1.Visible = true;
                        tdBehavior2.Visible = true;
                        tdGAP1.ColSpan = 1;
                        tdGAP2.ColSpan = 2;
                    }

					//---------------------- Actual Mark PSN Answer For Suggession ------------------------
					if (!GL.IsEqualNull(drv["COMP_Answer_MGR"])  && GL.CINT(drv["COMP_Answer_MGR"]) != 0)  {
						cel_header.Attributes["class"] = "AssLevel" + drv["COMP_Answer_MGR"];
						weight.Attributes["class"] = "AssLevel" + drv["COMP_Answer_MGR"];
						COMP_Name.Attributes["class"] = "TextLevel" + drv["COMP_Answer_MGR"];
						target.Attributes["class"] = "TextLevel" + drv["COMP_Answer_MGR"];
						mark.Attributes["class"] = "AssLevel" + drv["COMP_Answer_MGR"];

						HtmlTableCell _choice =(HtmlTableCell) e.Item.FindControl("choice" + drv["COMP_Answer_MGR"]);
						_choice.Attributes["class"] = "TextLevel" + drv["COMP_Answer_MGR"];

						for (int j = 1; j <= GL.CINT (drv["COMP_Answer_MGR"]); j++) {
                            HtmlTableCell _selHeaderColor = (HtmlTableCell)e.Item.FindControl("selHeaderColor" + j.ToString());
							_selHeaderColor.Attributes["class"] = "AssLevel" + drv["COMP_Answer_MGR"];
						}
						cel_officer.Attributes["COMP_Answer_MGR"] = GL.CINT (drv["COMP_Answer_MGR"]).ToString ();

						//------------- Add Wink Feature--------------
                        if (GL.CDBL(drv["FinalScore"]) < GL.CDBL(drv["COMP_Answer_MGR"]) & YearRound < BL.Start_Skip_Behavior_Round())
                        {
							mark.Attributes["class"] += " winkLevel" + drv["COMP_Answer_MGR"];
							weight.Attributes["class"] += " winkLevel" + drv["COMP_Answer_MGR"];
							mark.Attributes["title"] = "พฤติกรรมบ่งชี้น้อยกว่าคะแนนประเมิน";
							weight.Attributes["title"] = mark.Attributes["title"];
							imgAlert.Visible = true;
							lblAlert.Text = "ต้องระบุพฤติกรรมบ่งชี้ " + drv["COMP_Answer_MGR"] + " ข้อ";
						}
						//------------- End Wink Feature--------------



					}

					//-------------- Report Score----------
					mark.InnerHtml = GL.CDBL ( drv["FinalScore"]).ToString ();

					//--------------- Behavior ------------
					HtmlTableCell cell_Add_Behavior =(HtmlTableCell) e.Item.FindControl("cell_Add_Behavior");
					Button btnAddBehavior =(Button) e.Item.FindControl("btnAddBehavior");
					Repeater rptBehavior =(Repeater) e.Item.FindControl("rptBehavior");

					rptBehavior.ItemDataBound += rptBehavior_ItemDataBound;
					Behavior.DefaultView.RowFilter = "COMP_Type_Id=" + drv["COMP_Type_Id"] + " AND Master_No=" + drv["Master_No"];
					rptBehavior.DataSource = Behavior.DefaultView.ToTable().Copy();
					rptBehavior.DataBind();

					btnAddBehavior.CommandArgument = GL.CINT( Behavior.DefaultView.Count).ToString ();
					//-----  จำนวนพฤติกรรมบ่งชี้ที่มี -----
					btnAddBehavior.Visible = !txtMGR.ReadOnly | !txtPSN.ReadOnly;
                    
					//--------------- GAP -------------------------
					GAPData.DefaultView.RowFilter = "COMP_Type_Id=" + drv["COMP_Type_Id"] + " AND Master_No=" + drv["Master_No"] + " AND Selected=1"  ;
					DataTable GAP = GAPData.DefaultView.ToTable();
					Repeater rptGAP =(Repeater) e.Item.FindControl("rptGAP");
					rptGAP.ItemDataBound += rptGAP_ItemDataBound;
					rptGAP.DataSource = GAP;
					rptGAP.DataBind();
                    //------------- Add Wink Feature GAP--------------
                    if (!GL.IsEqualNull(drv["COMP_Answer_MGR"]) && GL.CINT(drv["COMP_Answer_MGR"]) != 0) {

                        if ((GL.CDBL(drv["COMP_Answer_MGR"]) < 3) && (GAP.Rows.Count == 0))
                        {
                            imgAlertGAP.Visible = true;
                            lblAlertGAP.Text = "ต้องเลือกอย่างน้อย 1 วิธี ";
                        }                    
                    
                    }

                    //------------- Add Wink Feature GAP--------------

					break;
				case ListItemType.Footer:
					//----------------Report Summary ------------

					//----------- Control Difinition ------------
					HtmlTableCell cell_sum_mark =(HtmlTableCell) e.Item.FindControl("cell_sum_mark");
					HtmlTableCell cell_sum_weight =(HtmlTableCell) e.Item.FindControl("cell_sum_weight");
					HtmlTableCell cell_total =(HtmlTableCell) e.Item.FindControl("cell_total");
					Label lblDone =(Label) e.Item.FindControl("lblDone");
					Label lblTotalItem =(Label) e.Item.FindControl("lblTotalItem");
					HtmlTableCell cell_sum_raw =(HtmlTableCell) e.Item.FindControl("cell_sum_raw");

					//------------ Report -------------
					DataTable DT =(DataTable) rptAss.DataSource;

					int TotalItem = DT.Rows.Count;
					int AssCount = GL.CINT ( DT.Compute("COUNT(COMP_No)", "COMP_Answer_MGR>0"));

					if (TotalItem == 0) {
						cell_total.InnerHtml = "ไม่พบหัวข้อในแบบประเมิน";
					} else if (AssCount == 0) {
						cell_total.InnerHtml = "ยังไม่ได้ประเมิน";
					} else if (AssCount == TotalItem) {
						cell_total.InnerHtml = "ประเมินครบ " + TotalItem + " ข้อแล้ว";
					} else {
						cell_total.InnerHtml = "ประเมินแล้ว " + AssCount + " ข้อจากทั้งหมด " + TotalItem + " ข้อ";
					}

					for (int i = 1; i <= 5; i++) {
						HtmlTableCell cell_sum =(HtmlTableCell) e.Item.FindControl("cell_sum_" + i);
						int cnt =GL.CINT ( DT.Compute("COUNT(COMP_Answer_MGR)", "COMP_Answer_MGR=" + i).ToString());
						if (cnt == 0) {
							cell_sum.InnerHtml = "-";
						} else {
							cell_sum.InnerHtml = "ได้ " + DT.Compute("COUNT(COMP_Answer_MGR)", "COMP_Answer_MGR=" + i).ToString() + " ข้อ";
							cell_sum.Attributes["class"] = "AssLevel" + i;
						}
					}


					if (TotalItem != 0) {
						DT.Columns.Add("WEIGHT", typeof(double));

						for (int i = 0; i <= DT.Rows.Count - 1; i++) {
							if (!GL.IsEqualNull(DT.Rows[i]["COMP_Weight"]) && Information.IsNumeric(DT.Rows[i]["COMP_Weight"].ToString().Replace("%", ""))) {
								DT.Rows[i]["WEIGHT"] = DT.Rows[i]["COMP_Weight"].ToString().Replace("%", "");
							}
						}
						DT.Columns.Add("MARKxWEIGHT", typeof(double), "FinalScore*WEIGHT");
						DT.Columns.Add("WEIGHTx5", typeof(double), "WEIGHT*5");

						object _SUM = DT.Compute("SUM(MARKxWEIGHT)", "");
						object _TOTAL = DT.Compute("SUM(WEIGHTx5)", "");

						if (!GL.IsEqualNull(_SUM) & !GL.IsEqualNull(_TOTAL)) {
							double Result = Convert.ToDouble(_SUM) * 100 / Convert.ToDouble(_TOTAL);
							if (Strings.Len(Result) > 5)
								Result = GL.CDBL ( GL.StringFormatNumber(Result));
							cell_sum_mark.InnerHtml = Result + "%";
							cell_sum_raw.InnerHtml = (Result * 5) + "/500";

							if (Result <= 20) {
								cell_sum_mark.Attributes["class"] = "AssLevel1";
								cell_sum_raw.Attributes["class"] = "AssLevel1";
							} else if (Result <= 40) {
								cell_sum_mark.Attributes["class"] = "AssLevel2";
								cell_sum_raw.Attributes["class"] = "AssLevel2";
							} else if (Result <= 60) {
								cell_sum_mark.Attributes["class"] = "AssLevel3";
								cell_sum_raw.Attributes["class"] = "AssLevel3";
							} else if (Result <= 80) {
								cell_sum_mark.Attributes["class"] = "AssLevel4";
								cell_sum_raw.Attributes["class"] = "AssLevel4";
							} else {
								cell_sum_mark.Attributes["class"] = "AssLevel5";
								cell_sum_raw.Attributes["class"] = "AssLevel5";
							}
						} else {
							cell_sum_mark.Attributes["class"] = "";
							cell_sum_mark.InnerHtml = "0%";
							cell_sum_raw.InnerHtml = "0/500";
						}
					}
					break;
			}


		}

		DataTable GAPData = null;
		protected void rptGAP_ItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
		{
			if (e.Item.ItemType != ListItemType.AlternatingItem & e.Item.ItemType != ListItemType.Item)
				return;

			HtmlAnchor aGAP =(HtmlAnchor) e.Item.FindControl("aGAP");
			HtmlImage imgSelect =(HtmlImage) e.Item.FindControl("imgSelect");
			Label lblGAP =(Label) e.Item.FindControl("lblGAP");
            DataRowView drv = (DataRowView)e.Item.DataItem;
			switch ( GL.CINT ( drv["Selected"].ToString ())) {
				case 1:
					imgSelect.Src = "images/check.png";
					break;
				case 0:
					imgSelect.Src = "images/nocheck.png";
					break;
			}
			lblGAP.Text = drv["GAP_Name"].ToString ();

			

		}


		protected void btnToggleGAP_Click(object sender, System.EventArgs e)
		{
			string SQL = " SELECT * FROM tb_HR_COMP_GAP \n";
			SQL += " WHERE R_Year = " + R_Year + " And R_Round =" + R_Round + "\n";
			SQL += " AND PSNL_NO='" + PSNL_NO.Replace("'", "''") + "' AND COMP_Type_Id=" + COMP_Type_Id + "\n";
			SQL += " AND Master_No=" + Master_No + " AND GAP_ID=" + GAP_ID + "\n";
			SqlDataAdapter DA = new SqlDataAdapter(SQL, BL.ConnectionString());
			DataTable DT = new DataTable();
			DA.Fill(DT);

			if (DT.Rows.Count == 0) {
				DataRow DR = DT.NewRow();
				DR["PSNL_NO"] = PSNL_NO;
				DR["R_Year"] = R_Year;
				DR["R_Round"] = R_Round;
				DR["COMP_Type_Id"] = COMP_Type_Id;
				DR["Master_No"] = Master_No;
				DR["GAP_ID"] = GAP_ID;
				DR["PSNL_TYPE"] = PSNL_TYPE;
				DR["CLSGP_ID"] = PSNL_CLASS_GROUP;
				DR["FN_ID"] = FN_ID;
				DR["FN_TYPE"] = FN_Type;
				DR["GAP_Name"] = GAP_Name;
				DR["Update_By"] = Session["USER_PSNL_NO"];
				DR["Update_Time"] = DateAndTime.Now;
				DT.Rows.Add(DR);
				SqlCommandBuilder cmd = new SqlCommandBuilder(DA);
				try {
					DA.Update(DT);
				} catch (Exception ex) {
				}
			} else {
				DT.Rows[0].Delete();
				SqlCommandBuilder cmd = new SqlCommandBuilder(DA);
				try {
					DA.Update(DT);
				} catch (Exception ex) {
				}
			}

			ModalBehavior.Visible = false;
			BindMasterCOMP();

		}

		protected void rptBehavior_ItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
		{
			if (e.Item.ItemType != ListItemType.AlternatingItem & e.Item.ItemType != ListItemType.Item)
				return;

			HtmlAnchor aBehavior =(HtmlAnchor) e.Item.FindControl("aBehavior");
			HtmlAnchor aDelete =(HtmlAnchor) e.Item.FindControl("aDelete");
            DataRowView drv = (DataRowView)e.Item.DataItem;
			aBehavior.InnerHtml = drv["BHV_No"].ToString() + " . " + drv["BHV_Content"].ToString();
			if (COMP_Status >= HRBL.AssessmentStatus.WaitAssessment & COMP_Status < HRBL.AssessmentStatus.AssessmentCompleted) {
				//-------------------------------- Add Click Event---------------------------------------
				string Script = "document.getElementById('" + txtCOMPTypeID.ClientID + "').value=" + drv["COMP_Type_Id"] + ";";
				Script += "document.getElementById('" + txtMasterNo.ClientID + "').value=" + drv["Master_No"] + ";";
				Script += "document.getElementById('" + txtBehaviorNo.ClientID + "').value='" + drv["BHV_No"] + "';";
				Script += "document.getElementById('" + btnBehaviorDialog.ClientID + "').click();";
				aBehavior.Attributes["onclick"] = Script;

				Script = "document.getElementById('" + txtCOMPTypeID.ClientID + "').value=" + drv["COMP_Type_Id"] + ";";
				Script += "document.getElementById('" + txtMasterNo.ClientID + "').value=" + drv["Master_No"] + ";";
				Script += "document.getElementById('" + txtBehaviorNo.ClientID + "').value=" + drv["BHV_No"] + ";";
				Script += "document.getElementById('" + btnBehaviorDelete.ClientID + "').click();";
				aDelete.Attributes["onclick"] = Script;
			} else {
				aDelete.Visible = false;
				aBehavior.Style["width"] = "";
				aBehavior.Style["cursor"] = "Default";
			}

		}

		protected void btnBehaviorDialog_Click(object sender, System.EventArgs e)
		{
			lblBehaviorNo.Text = txtBehaviorNo.Text;

			string SQL = "SELECT BHV_Content FROM tb_HR_COMP_Behavior \n";
			SQL += " WHERE PSNL_NO='" + PSNL_NO.Replace("'", "''") + "' ";
			SQL += " AND R_Year=" + R_Year + "\n";
			SQL += " AND R_Round=" + R_Round + "\n";
			SQL += " AND COMP_Type_Id=" + COMP_Type_Id + "\n";
			SQL += " AND Master_No=" + Master_No + "\n";
			SQL += " AND BHV_No=" + BHV_No + "\n";

			SqlDataAdapter DA = new SqlDataAdapter(SQL, BL.ConnectionString());
			DataTable DT = new DataTable();
			DA.Fill(DT);

			if (DT.Rows.Count == 0) {
				txtBehavior.Text = "";
			} else {
				txtBehavior.Text = DT.Rows[0]["BHV_Content"].ToString ();
			}
			ModalBehavior.Visible = true;
			ScriptManager.RegisterStartupScript(this.Page, typeof(string), "focus", "document.getElementById('" + txtBehavior.ClientID + "').focus();", true);
		}

		protected void btnBehaviorDelete_Click(object sender, System.EventArgs e)
		{
			string SQL = "SELECT * FROM tb_HR_COMP_Behavior \n";
			SQL += " WHERE PSNL_NO='" + PSNL_NO.Replace("'", "''") + "' ";
			SQL += " AND R_Year=" + R_Year + "\n";
			SQL += " AND R_Round=" + R_Round + "\n";
			SQL += " AND COMP_Type_Id=" + COMP_Type_Id + "\n";
			SQL += " AND Master_No=" + Master_No + "\n";

			SqlDataAdapter DA = new SqlDataAdapter(SQL, BL.ConnectionString());
			DataTable DT = new DataTable();
			DA.Fill(DT);
			//--------------- Delete  Target-----
			DT.DefaultView.RowFilter = "BHV_No=" + BHV_No;
			if (DT.DefaultView.Count > 0) {
				DT.DefaultView[0].Row.Delete();
			}
			SqlCommandBuilder cmd = new SqlCommandBuilder(DA);
			try {
				DA.Update(DT);
			} catch {
			}
			//--------------- ReOrder -----------
			for (int i = 0; i <= DT.Rows.Count - 1; i++) {
				DT.Rows[i]["BHV_No"] = i + 1;
			}
			cmd = new SqlCommandBuilder(DA);
			try {
				DA.Update(DT);
			} catch {
			}

			ModalBehavior.Visible = false;
			BindMasterCOMP();

		}

		protected void btnClose_Click(object sender, System.EventArgs e)
		{
			ModalBehavior.Visible = false;
		}

		protected void btnOK_Click(object sender, System.EventArgs e)
		{
			if (string.IsNullOrEmpty(Strings.Trim(txtBehavior.Text))) {
				ScriptManager.RegisterStartupScript(this.Page, typeof(string), "Alert", "showAlert('กรอกรายละเอียด');", true);
				return;
			}

			string SQL = "SELECT * FROM tb_HR_COMP_Behavior \n";
			SQL += " WHERE PSNL_NO='" + PSNL_NO.Replace("'", "''") + "' ";
			SQL += " AND R_Year=" + R_Year + "\n";
			SQL += " AND R_Round=" + R_Round + "\n";
			SQL += " AND COMP_Type_Id=" + COMP_Type_Id + "\n";
			SQL += " AND Master_No=" + Master_No + "\n";
			SQL += " AND BHV_No=" + BHV_No + "\n";

			SqlDataAdapter DA = new SqlDataAdapter(SQL, BL.ConnectionString());
			DataTable DT = new DataTable();
			DA.Fill(DT);

			DataRow DR = null;
			if (DT.Rows.Count == 0) {
				DR = DT.NewRow();
				DR["PSNL_NO"] = PSNL_NO;
				DR["R_Year"] = R_Year;
				DR["R_Round"] = R_Round;
				DR["COMP_Type_Id"] = COMP_Type_Id;
				DR["Master_No"] = Master_No;
				DR["BHV_No"] = BHV_No;
			} else {
				DR = DT.Rows[0];
			}
			DR["BHV_Content"] = txtBehavior.Text;

			if (DT.Rows.Count == 0)
				DT.Rows.Add(DR);
			SqlCommandBuilder cmd = new SqlCommandBuilder(DA);
			try {
				DA.Update(DT);
			} catch (Exception ex) {
			}

			ModalBehavior.Visible = false;
			BindMasterCOMP();

		}

		protected void btnBack_Click(object sender, System.EventArgs e)
		{
			BindPersonalList();
			pnlList.Visible = true;
			pnlEdit.Visible = false;
		}


		protected void btnBack_TOP_Click(object sender, System.EventArgs e)
		{
			BindPersonalList();
			pnlList.Visible = true;
			pnlEdit.Visible = false;
		}

		protected void btnPreSend_Click(object sender, System.EventArgs e)
		{
			DataTable DT = BL.GetCOMPBundle(PSNL_NO, R_Year, R_Round, PSNL_CLASS_GROUP, PSNL_TYPE, FN_ID, FN_Type, HRBL.CompetencyType.All);


			DT.DefaultView.RowFilter = "COMP_Answer_MGR IS NULL OR COMP_Answer_MGR=0";
			if (DT.DefaultView.Count > 0) {
				ScriptManager.RegisterStartupScript(this.Page, typeof(string), "Alert", "showAlert('คลิกเพื่อประเมินให้ครบทุกข้อ');", true);
				return;
			}

			//--------------- Calculate Final Score -------------
			Behavior = BL.GetCOMPBehavior(PSNL_NO, R_Year, R_Round).Table;
			DT.Columns.Add("FinalScore", typeof(Int32));
			for (int i = 0; i <= DT.Rows.Count - 1; i++) {
				int TotalBehavior = GL.CINT ( Behavior.Compute("COUNT(Master_No)", "COMP_Type_Id=" + DT.Rows[i]["COMP_Type_Id"] + " AND Master_No=" + DT.Rows[i]["Master_No"]));
				int SelectedChoice = 0;
				if (!GL.IsEqualNull(DT.Rows[i]["COMP_Answer_MGR"]))
					SelectedChoice =GL.CINT ( DT.Rows[i]["COMP_Answer_MGR"]);
				if (SelectedChoice >= 3) {
					DT.Rows[i]["FinalScore"] = (TotalBehavior >= SelectedChoice ? SelectedChoice : TotalBehavior);
				} else {
					DT.Rows[i]["FinalScore"] = SelectedChoice;
				}
			}
			DT.DefaultView.RowFilter = "FinalScore < COMP_Answer_MGR";
			if (DT.DefaultView.Count > 0) {
				ScriptManager.RegisterStartupScript(this.Page, typeof(string), "Alert", "showAlert('มีบางข้อพฤติกรรมบ่งชี้ น้อยกว่าคะแนนประเมิน');", true);
				return;
			}
            DT.DefaultView.RowFilter = "FinalScore < 3";
            DataTable GAP_FinalScore = DT.DefaultView.ToTable();

            //---------- Get GAP Data------------
            GAPData = BL.GetGAPDetailPSN(PSNL_NO, HRBL.CompetencyType.All, R_Year, R_Round);
            if (DT.DefaultView.Count > 0)
            {
                for (int i = 0; i <= GAP_FinalScore.Rows.Count - 1; i++)
                {
                    GAPData.DefaultView.RowFilter = "COMP_Type_Id=" + GAP_FinalScore.Rows[i]["COMP_Type_Id"] + " AND Master_No=" + GAP_FinalScore.Rows[i]["Master_No"] + " AND Selected=1";
                    if (GAPData.DefaultView.Count == 0)
                    { 
                        ScriptManager.RegisterStartupScript(this.Page, typeof(string), "Alert", "showAlert('มีบางข้อคะแนนประเมินน้อยกว่า 3 เลือกวิธีการพัฒนาอย่างน้อย 1 วิธี');", true);
                        return;      
                    }
                }

            }

			ScriptManager.RegisterStartupScript(this.Page, typeof(string), "Alert", "if(confirm('ยืนยันอนุมัติผลการประเมิน ??\\nหลังจากอนุมัติผลการประเมินแล้วคุณไม่สามารถแก้ไขได้')) document.getElementById('" + btnSend.ClientID + "').click();", true);
		}


		protected void btnSend_Click(object sender, System.EventArgs e)
		{
			DataTable DT = BL.GetCOMPBundle(PSNL_NO, R_Year, R_Round, PSNL_CLASS_GROUP, PSNL_TYPE, FN_ID, FN_Type, HRBL.CompetencyType.All);

			//--------------------Saving--------------------
			string SQL = "";
			SQL = " SELECT * \n";
			SQL += "  FROM tb_HR_COMP_Header\n";
			SQL += " WHERE PSNL_NO='" + PSNL_NO.Replace("'", "''") + "' AND R_Year=" + R_Year + " AND R_Round=" + R_Round;
			SqlDataAdapter DA = new SqlDataAdapter(SQL, BL.ConnectionString());
			DT = new DataTable();
			DA.Fill(DT);

			DataRow DR = DT.Rows[0];
            HRBL.PersonalInfo PSNInfo = BL.GetAssessmentPersonalInfo(R_Year, R_Round, Session["USER_PSNL_NO"].ToString (), COMP_Status);
			DR["COMP_Status"] = HRBL.AssessmentStatus.AssessmentCompleted;
			DR["Update_By"] = Session["USER_PSNL_NO"];
			DR["Update_Time"] = DateAndTime.Now;
			//------------- Set Manager ----------------
			DR["Assessment_Commit_MGR"] = true;
			DR["Assessment_Commit_Name"] = PSNInfo.PSNL_Fullname;
			DR["Assessment_Commit_DEPT"] = PSNInfo.DEPT_NAME;
			if (!string.IsNullOrEmpty(PSNInfo.MGR_NAME)) {
				DR["Assessment_Commit_POS"] = PSNInfo.MGR_NAME;
			} else {
				DR["Assessment_Commit_POS"] = PSNInfo.FN_NAME;
			}
			DR["Assessment_Commit_By_MGR"] = PSNInfo.PSNL_No;
			DR["Assessment_Commit_Time_MGR"] = DateAndTime.Now;

			if (DT.Rows.Count == 0)
				DT.Rows.Add(DR);
			SqlCommandBuilder cmd = new SqlCommandBuilder(DA);
			DA.Update(DT);

			//----------------- ทำสองที่ Page Load กับ Update Status ---------------
			BL.Update_COMP_Status_To_Assessment_Period(PSNL_NO, R_Year, R_Round);

			ScriptManager.RegisterStartupScript(this.Page, typeof(string), "Alert", "showAlert('การประเมินเสร็จสมบูรณ์');", true);
			BindMasterCOMP();

		}


		private void SaveHeader()
		{
			//------------- Check Round ---------------
			if (R_Year == 0 | R_Round == 0)
				return;
			//------------- Check Progress-------------

			string SQL = "";
			SQL = " SELECT * \n";
			SQL += "  FROM tb_HR_COMP_Header\n";
			SQL += " WHERE PSNL_NO='" + PSNL_NO.Replace("'", "''") + "' AND R_Year=" + R_Year + " AND R_Round=" + R_Round;
			SqlDataAdapter DA = new SqlDataAdapter(SQL, BL.ConnectionString());
			DataTable DT = new DataTable();
			DA.Fill(DT);

			DataRow DR = null;
			if (DT.Rows.Count == 0) {
				DR = DT.NewRow();
				DR["PSNL_NO"] = PSNL_NO;
				DR["R_Year"] = R_Year;
				DR["R_Round"] = R_Round;
				DR["Create_By"] = Session["USER_PSNL_NO"];
				DR["Create_Time"] = DateAndTime.Now;
				DR["COMP_Status"] = 0;
			} else {
				DR = DT.Rows[0];
			}

			//------------- Round Detail------------- 
			SQL = "SELECT * FROM tb_HR_Round ";
			SQL += " WHERE R_Year=" + R_Year + " AND R_Round=" + R_Round;
			DataTable PN = new DataTable();
			SqlDataAdapter PA = new SqlDataAdapter(SQL, BL.ConnectionString());
			PA.Fill(PN);
			DR["R_Start"] = PN.Rows[0]["R_Start"];
			DR["R_End"] = PN.Rows[0]["R_End"];
			DR["R_Remark"] = "";

            			//============ป้องกันการ update ตำแหน่ง ณ สิ้นรอบประเมินและปิดรอบประเมิน======

            Boolean IsInPeriod = BL.IsTimeInPeriod(DateTime.Now, R_Year, R_Round);
            Boolean IsRoundCompleted = BL.Is_Round_Completed(R_Year, R_Round);
            Boolean ckUpdate =  IsInPeriod & !IsRoundCompleted;

            //if (ckUpdate)

            if (ckUpdate | DT.Rows.Count == 0 )
            {
                //------------- Personal Detail---------
                HRBL.PersonalInfo PSNInfo = BL.GetAssessmentPersonalInfo(R_Year, R_Round, PSNL_NO, COMP_Status);
                // Replace With New Updated Data If Exists
                if (!string.IsNullOrEmpty(PSNInfo.PSNL_No))
                {
                    if (!string.IsNullOrEmpty(PSNInfo.PSNL_No))
                        DR["PSNL_No"] = PSNInfo.PSNL_No;
                    else
                        DR["PSNL_No"] = DBNull.Value;
                    if (!string.IsNullOrEmpty(PSNInfo.PSNL_Fullname))
                        DR["PSNL_Fullname"] = PSNInfo.PSNL_Fullname;
                    else
                        DR["PSNL_Fullname"] = DBNull.Value;
                    if (!string.IsNullOrEmpty(PSNInfo.PNPS_CLASS))
                        DR["PNPS_CLASS"] = PSNInfo.PNPS_CLASS;
                    else
                        DR["PNPS_CLASS"] = DBNull.Value;
                    if (!string.IsNullOrEmpty(PSNInfo.PSNL_TYPE))
                        DR["PSNL_TYPE"] = PSNInfo.PSNL_TYPE;
                    else
                        DR["PSNL_TYPE"] = DBNull.Value;
                    if (!string.IsNullOrEmpty(PSNInfo.POS_NO))
                        DR["POS_NO"] = PSNInfo.POS_NO;
                    else
                        DR["POS_NO"] = DBNull.Value;
                    if (!string.IsNullOrEmpty(PSNInfo.WAGE_TYPE))
                        DR["WAGE_TYPE"] = PSNInfo.WAGE_TYPE;
                    else
                        DR["WAGE_TYPE"] = DBNull.Value;
                    if (!string.IsNullOrEmpty(PSNInfo.WAGE_NAME))
                        DR["WAGE_NAME"] = PSNInfo.WAGE_NAME;
                    else
                        DR["WAGE_NAME"] = DBNull.Value;
                    if (!string.IsNullOrEmpty(PSNInfo.SECTOR_CODE))
                        DR["SECTOR_CODE"] = Strings.Left(PSNInfo.SECTOR_CODE, 2);
                    else
                        DR["SECTOR_CODE"] = DBNull.Value;
                    if (!string.IsNullOrEmpty(PSNInfo.DEPT_CODE))
                        DR["DEPT_CODE"] = PSNInfo.DEPT_CODE;
                    else
                        DR["DEPT_CODE"] = DBNull.Value;
                    if (!string.IsNullOrEmpty(PSNInfo.MINOR_CODE))
                        DR["MINOR_CODE"] = PSNInfo.MINOR_CODE;
                    else
                        DR["MINOR_CODE"] = DBNull.Value;
                    if (!string.IsNullOrEmpty(PSNInfo.SECTOR_NAME))
                        DR["SECTOR_NAME"] = PSNInfo.SECTOR_NAME;
                    else
                        DR["SECTOR_NAME"] = DBNull.Value;
                    if (!string.IsNullOrEmpty(PSNInfo.DEPT_NAME))
                        DR["DEPT_NAME"] = PSNInfo.DEPT_NAME;
                    else
                        DR["DEPT_NAME"] = DBNull.Value;
                    if (!string.IsNullOrEmpty(PSNInfo.FN_ID))
                        DR["FN_ID"] = PSNInfo.FN_ID;
                    else
                        DR["FN_ID"] = DBNull.Value;
                    if (!string.IsNullOrEmpty(PSNInfo.FLD_Name))
                        DR["FLD_Name"] = PSNInfo.FLD_Name;
                    else
                        DR["FLD_Name"] = DBNull.Value;
                    if (!string.IsNullOrEmpty(PSNInfo.FN_CODE))
                        DR["FN_CODE"] = PSNInfo.FN_CODE;
                    else
                        DR["FN_CODE"] = DBNull.Value;
                    if (!string.IsNullOrEmpty(PSNInfo.FN_NAME))
                        DR["FN_NAME"] = PSNInfo.FN_NAME;
                    else
                        DR["FN_NAME"] = DBNull.Value;
                    if (!string.IsNullOrEmpty(PSNInfo.MGR_CODE))
                        DR["MGR_CODE"] = PSNInfo.MGR_CODE;
                    else
                        DR["MGR_CODE"] = DBNull.Value;
                    if (!string.IsNullOrEmpty(PSNInfo.MGR_NAME))
                        DR["MGR_NAME"] = PSNInfo.MGR_NAME;
                    else
                        DR["MGR_NAME"] = DBNull.Value;
                }
                //------------- Assign Assessor Only-----------------------------
                DR["Create_Commit_By"] = ASSESSOR_BY;
                DR["Create_Commit_Name"] = ASSESSOR_NAME;
                DR["Create_Commit_DEPT"] = ASSESSOR_DEPT;
                DR["Create_Commit_POS"] = ASSESSOR_POS;
                DR["Assessment_Commit_By_MGR"] = ASSESSOR_BY;
                DR["Assessment_Commit_Name"] = ASSESSOR_NAME;
                DR["Assessment_Commit_DEPT"] = ASSESSOR_DEPT;
                DR["Assessment_Commit_POS"] = ASSESSOR_POS;
            }
			DR["Update_By"] = Session["USER_PSNL_NO"];
			DR["Update_Time"] = DateAndTime.Now;

			if (DT.Rows.Count == 0)
				DT.Rows.Add(DR);
			SqlCommandBuilder cmd = new SqlCommandBuilder(DA);
			DA.Update(DT);

			//----------- Prevent Save----------
			if (COMP_Status > HRBL.AssessmentStatus.WaitConfirmAssessment | BL.Is_Round_Completed(R_Year, R_Round)) {
				return;
			}

			DataTable TMP = BL.GetAssessorList(R_Year, R_Round, PSNL_NO);
			TMP.DefaultView.RowFilter = "MGR_PSNL_NO='" + ASSESSOR_BY.Replace("'", "''") + "'";

			//----------------- Update Information For History Assessor Structure ------------------
			SQL = "SELECT * ";
			SQL += " FROM tb_HR_Actual_Assessor ";
			SQL += " WHERE PSNL_NO='" + PSNL_NO + "' AND R_Year=" + R_Year + " AND R_Round=" + R_Round;
			DA = new SqlDataAdapter(SQL, BL.ConnectionString());
			DT = new DataTable();
			DA.Fill(DT);

			if (DT.Rows.Count > 0) {
				DT.Rows[0].Delete();
				cmd = new SqlCommandBuilder(DA);
				DA.Update(DT);
			}

			if (TMP.DefaultView.Count > 0) {
				DR = DT.NewRow();
				DR["PSNL_NO"] = PSNL_NO;
				DR["R_Year"] = R_Year;
				DR["R_Round"] = R_Round;
				DR["MGR_ASSESSOR_POS"] = TMP.DefaultView[0]["ASSESSOR_POS"];
				DR["MGR_PSNL_NO"] = TMP.DefaultView[0]["MGR_PSNL_NO"];
				DR["MGR_PSNL_Fullname"] = TMP.DefaultView[0]["MGR_PSNL_Fullname"];
				DR["MGR_PNPS_CLASS"] = TMP.DefaultView[0]["MGR_CLASS"];
				DR["MGR_PSNL_TYPE"] = TMP.DefaultView[0]["MGR_WAGE_TYPE"];
				DR["MGR_POS_NO"] = TMP.DefaultView[0]["MGR_POS_NO"];
				DR["MGR_WAGE_TYPE"] = TMP.DefaultView[0]["MGR_WAGE_TYPE"];
				DR["MGR_WAGE_NAME"] = TMP.DefaultView[0]["MGR_WAGE_NAME"];
				DR["MGR_DEPT_CODE"] = TMP.DefaultView[0]["MGR_DEPT_CODE"];
				//DR("MGR_MINOR_CODE") = TMP.DefaultView(0).Item("MGR_MINOR_CODE")
				DR["MGR_SECTOR_CODE"] = TMP.DefaultView[0]["MGR_SECTOR_CODE"].ToString().Substring(0, 2);
				DR["MGR_SECTOR_NAME"] = TMP.DefaultView[0]["MGR_SECTOR_NAME"];
				DR["MGR_DEPT_NAME"] = TMP.DefaultView[0]["MGR_DEPT_NAME"];
				//DR("MGR_FN_ID") = TMP.DefaultView(0).Item("MGR_FN_ID")
				//DR("MGR_FLD_Name") = TMP.DefaultView(0).Item("MGR_FLD_Name")
				DR["MGR_FN_CODE"] = TMP.DefaultView[0]["MGR_FN_CODE"];
				//DR("MGR_FN_TYPE") = TMP.DefaultView(0).Item("MGR_FN_TYPE")
				DR["MGR_FN_NAME"] = TMP.DefaultView[0]["MGR_FN_NAME"];
				if (!GL.IsEqualNull(TMP.DefaultView[0]["MGR_MGR_CODE"])) {
					DR["MGR_MGR_CODE"] = TMP.DefaultView[0]["MGR_MGR_CODE"];
				}
				if (!GL.IsEqualNull(TMP.DefaultView[0]["MGR_MGR_NAME"])) {
					DR["MGR_MGR_NAME"] = TMP.DefaultView[0]["MGR_MGR_NAME"];
				}
				try {
					DR["MGR_PNPS_RETIRE_DATE"] = BL.GetPSNRetireDate(TMP.DefaultView[0]["MGR_PSNL_NO"].ToString());
				} catch {
				}
				DR["Update_By"] = Session["USER_PSNL_NO"];
				DR["Update_Time"] = DateAndTime.Now;
				DT.Rows.Add(DR);
				cmd = new SqlCommandBuilder(DA);
				DA.Update(DT);
			}

			//----------------- Update Information For History PSN Structure ------------------
			DataTable PSN = new DataTable();
			SQL = "SELECT * FROM vw_PN_PSNL WHERE PSNL_NO='" + PSNL_NO.Replace("'", "''") + "'";
			DA = new SqlDataAdapter(SQL, BL.ConnectionString());
			DA.Fill(PSN);

			SQL = "SELECT * ";
			SQL += " FROM tb_HR_Assessment_PSN ";
			SQL += " WHERE PSNL_NO='" + PSNL_NO.Replace("'", "''") + "' AND R_Year=" + R_Year + " AND R_Round=" + R_Round;
			DA = new SqlDataAdapter(SQL, BL.ConnectionString());
			DT = new DataTable();
			DA.Fill(DT);


			if (PSN.Rows.Count > 0) {
				if (DT.Rows.Count == 0) {
					DR = DT.NewRow();
					DR["PSNL_NO"] = PSNL_NO;
					DR["R_Year"] = R_Year;
					DR["R_Round"] = R_Round;
					if (TMP.DefaultView.Count > 0) {
						try {
							DR["PSN_ASSESSOR_POS"] = TMP.DefaultView[0]["ASSESSOR_POS"];
						} catch {
						}
					}
					DR["PSN_PSNL_NO"] = PSNL_NO;
					DR["PSN_PSNL_Fullname"] = PSN.Rows[0]["PSNL_Fullname"].ToString();
					DT.Rows.Add(DR);
				} else {
					DR = DT.Rows[0];
				}

                //============ป้องกันการ update ตำแหน่ง ณ สิ้นรอบประเมินและปิดรอบประเมิน======
                if (ckUpdate | DT.Rows.Count == 0 | string.IsNullOrEmpty(DT.Rows[0]["PSN_PNPS_CLASS"].ToString()))
                {
                    DR["PSN_PNPS_CLASS"] = PSN.Rows[0]["PNPS_CLASS"].ToString();
                    DR["PSN_PSNL_TYPE"] = PSN.Rows[0]["PSNL_TYPE"].ToString();
                    DR["PSN_POS_NO"] = PSN.Rows[0]["POS_NO"].ToString();
                    DR["PSN_WAGE_TYPE"] = PSN.Rows[0]["WAGE_TYPE"].ToString();
                    DR["PSN_WAGE_NAME"] = PSN.Rows[0]["WAGE_NAME"].ToString();
                    DR["PSN_DEPT_CODE"] = PSN.Rows[0]["DEPT_CODE"].ToString();
                    DR["PSN_MINOR_CODE"] = PSN.Rows[0]["MINOR_CODE"].ToString();
                    DR["PSN_SECTOR_CODE"] = PSN.Rows[0]["SECTOR_CODE"].ToString().Substring(0, 2);
                    DR["PSN_SECTOR_NAME"] = PSN.Rows[0]["SECTOR_NAME"].ToString();
                    DR["PSN_DEPT_NAME"] = PSN.Rows[0]["DEPT_NAME"].ToString();
                    DR["PSN_FN_ID"] = PSN.Rows[0]["FN_ID"].ToString();
                    DR["PSN_FLD_Name"] = PSN.Rows[0]["FLD_Name"].ToString();
                    DR["PSN_FN_CODE"] = PSN.Rows[0]["FN_CODE"].ToString();
                    DR["PSN_FN_TYPE"] = PSN.Rows[0]["FN_TYPE"].ToString();
                    DR["PSN_FN_NAME"] = PSN.Rows[0]["FN_NAME"].ToString();
                    if (!GL.IsEqualNull(PSN.Rows[0]["MGR_CODE"]))
                    {
                        DR["PSN_MGR_CODE"] = PSN.Rows[0]["MGR_CODE"];
                    }
                    if (!GL.IsEqualNull(PSN.Rows[0]["MGR_NAME"]))
                    {
                        DR["PSN_MGR_NAME"] = PSN.Rows[0]["MGR_NAME"];
                    }
                }
				try {
					DR["PSN_PNPS_RETIRE_DATE"] = BL.GetPSNRetireDate(PSNL_NO);
				} catch {
				}
				DR["Update_By"] = Session["USER_PSNL_NO"];
				DR["Update_Time"] = DateAndTime.Now;
				cmd = new SqlCommandBuilder(DA);
				DA.Update(DT);

			}
		}

		protected void ddlForm_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if (ddlForm.SelectedIndex != 0) {
				Response.Redirect("Assessment_Team_COMP_Create.aspx");
			}
		}






        //----------------------------Selece GAP--------------------------------------
        private void BindGAPList(int GAP_COMP_Type_Id, int GAP_Master_No)
        {

            //--------------- GAP -------------------------
            COMP_Type_Id = GL.CINT(GAP_COMP_Type_Id);
            Master_No = GL.CINT(GAP_Master_No);
            //---------- Get GAP Data------------
            GAPData = BL.GetGAPDetailPSN(PSNL_NO, HRBL.CompetencyType.All, R_Year, R_Round);
            GAPData.DefaultView.RowFilter = "COMP_Type_Id=" + GAP_COMP_Type_Id + " AND Master_No=" + GAP_Master_No;
            DataTable GAP = GAPData.DefaultView.ToTable();

            Session["AssessmentSetting_COMP_PSN_GAP"] = GAP;
            PagerGAP.SesssionSourceName = "AssessmentSetting_COMP_PSN_GAP";
            PagerGAP.RenderLayout();
            if (GAP.Rows.Count == 0)
            {
                lblCountGAP.Text = "ไม่พบรายการดังกล่าว";
            }
            else
            {
                lblCountGAP.Text = "พบ " + GL.StringFormatNumber(GAP.Rows.Count.ToString(), 0) + " รายการ";
            }

            GAP.DefaultView.RowFilter = "Selected=1";
            DataTable GAP_Selected = GAP.DefaultView.ToTable();
            if (GAP.Rows.Count == GAP_Selected.DefaultView.Count)
            {
                btnGAPAll.ImageUrl = "images/check.png";
            }
            else
            {
                btnGAPAll.ImageUrl = "images/none.png";
            }


        }

        protected void PagerGAP_PageChanging(PageNavigation Sender)
        {
            PagerGAP.TheRepeater = rptGAPDialog;
        }

        protected void rptGAPDialog_ItemCommand(object source, System.Web.UI.WebControls.RepeaterCommandEventArgs e)
        {
            Label lblNo = (Label)e.Item.FindControl("lblNo");
            Label lblName = (Label)e.Item.FindControl("lblName");


            switch (e.CommandName)
            {
                case "Select":
                    ImageButton btn = (ImageButton)e.Item.FindControl("btnEditGAP");

                    switch (btn.ImageUrl)
                    {
                        case "images/check.png":
                            btn.ImageUrl = "images/none.png";
                            btnGAPAll.ImageUrl = "images/none.png";
                            break;
                        default:
                            btn.ImageUrl = "images/check.png";
                            break;
                    }

                    break;

            }

        }

        protected void rptGAPDialog_ItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType != ListItemType.AlternatingItem & e.Item.ItemType != ListItemType.Item)
                return;
            Label lblNo = (Label)e.Item.FindControl("lblNo");
            Label lblName = (Label)e.Item.FindControl("lblName");
            ImageButton btnEditGAP = (ImageButton)e.Item.FindControl("btnEditGAP");

            DataRowView drv = (DataRowView)e.Item.DataItem;
            lblNo.Text = drv["GAP_ID"].ToString().PadLeft(3, GL.chr0);
            lblName.Text = drv["GAP_Name"].ToString();

            if ((GL.CINT(drv["Selected"])) == 1)
            {
                btnEditGAP.ImageUrl = "images/check.png";
            }
            else
            {
                btnEditGAP.ImageUrl = "images/none.png";
            }
        }

        protected void btnModalGAP_Close_ServerClick(object sender, System.EventArgs e)
        {
            ModalGAP.Visible = false;
        }

        protected void btnGAPAll_Click(object sender, ImageClickEventArgs e)
        {
            ImageButton btn = (ImageButton)sender;
            switch (btn.ImageUrl)
            {
                case "images/check.png":
                    btn.ImageUrl = "images/none.png";
                    SETALL_SelectGAP();
                    break;
                default:
                    btn.ImageUrl = "images/check.png";
                    SETALL_SelectGAP();
                    break;
            }


        }

        protected void btnOKGAP_Click(object sender, EventArgs e)
        {
            DataTable DTGAP = new DataTable();
            DTGAP = Current_SelectGAP();
            // ลบรายการก่อนบันทึกใหม่
            string SQL = " DELETE  FROM tb_HR_COMP_GAP \n";
            SQL += " WHERE R_Year = " + R_Year + " And R_Round =" + R_Round + "\n";
            SQL += " AND PSNL_NO='" + PSNL_NO.Replace("'", "''") + "' AND COMP_Type_Id=" + Convert.ToInt32(COMP_Type_Id) + "\n";
            SQL += " AND Master_No=" + Convert.ToInt32(Master_No) + "\n"; 
            SqlDataAdapter DA = new SqlDataAdapter(SQL, BL.ConnectionString());
            DataTable DT = new DataTable();
            DA.Fill(DT);
            if (DTGAP.Rows.Count > 0)
            {
                SQL = " SELECT *  FROM tb_HR_COMP_GAP WHERE 0=1 \n";
                DA = new SqlDataAdapter(SQL, BL.ConnectionString());
                DT = new DataTable();
                DA.Fill(DT);
                for (int i = 0; i <= DTGAP.Rows.Count - 1; i++)
                {
                    DataRow DR = DT.NewRow();
                    DR["PSNL_NO"] = PSNL_NO;
                    DR["R_Year"] = R_Year;
                    DR["R_Round"] = R_Round;
                    DR["COMP_Type_Id"] = Convert.ToInt32(COMP_Type_Id);
                    DR["Master_No"] = Master_No;
                    DR["GAP_ID"] = DTGAP.Rows[i]["GAPNo"].ToString();
                    DR["PSNL_TYPE"] = PSNL_TYPE;
                    DR["CLSGP_ID"] = PSNL_CLASS_GROUP;
                    DR["FN_ID"] = FN_ID;
                    DR["FN_TYPE"] = FN_Type;
                    DR["GAP_Name"] = DTGAP.Rows[i]["GAPName"].ToString();
                    DR["Update_By"] = Session["USER_PSNL_NO"];
                    DR["Update_Time"] = DateAndTime.Now;
                    DT.Rows.Add(DR);
                }
                SqlCommandBuilder cmd = new SqlCommandBuilder(DA);
                DA.Update(DT);
            }
            ModalGAP.Visible = false;
            BindMasterCOMP();
        }

        private DataTable Current_SelectGAP()
        {
            DataTable DT = new DataTable();
            DataRow DR = null;
            DT.Columns.Add("GAPNo", typeof(string));
            DT.Columns.Add("GAPName", typeof(string));
            DT.Columns.Add("Select", typeof(Boolean));

            foreach (RepeaterItem Item in rptGAPDialog.Items)
            {
                ImageButton btnEditGAP = (ImageButton)Item.FindControl("btnEditGAP");
                Label lblNo = (Label)Item.FindControl("lblNo");
                Label lblName = (Label)Item.FindControl("lblName");

                if (btnEditGAP.ImageUrl == "images/check.png")
                {
                    DR = DT.NewRow();
                    DR["GAPNo"] = lblNo.Text;
                    DR["GAPName"] = lblName.Text;
                    DR["Select"] = true;
                    DT.Rows.Add(DR);
                }

            }
            return DT;
        }

        private DataTable SETALL_SelectGAP()
        {
            DataTable DT = new DataTable();
            foreach (RepeaterItem Item in rptGAPDialog.Items)
            {
                ImageButton btnEditGAP = (ImageButton)Item.FindControl("btnEditGAP");
                Label lblNo = (Label)Item.FindControl("lblNo");
                Label lblName = (Label)Item.FindControl("lblName");

                switch (btnGAPAll.ImageUrl)
                {
                    case "images/check.png":
                        btnEditGAP.ImageUrl = "images/check.png";
                        break;
                    default:
                        btnEditGAP.ImageUrl = "images/none.png";
                        break;
                }
            }
            return DT;
        }

        protected void btnCloseGAP_Click(object sender, EventArgs e)
        {
            ModalGAP.Visible = false;

        }        

		public Assessment_Team_COMP_Assessment()
		{
			Load += Page_Load;
		}




        private DataTable Current_Remark()
        {
            DataTable DT = new DataTable();
            DataRow DR = null;
            DT.Columns.Add("COMP_Type_Id");
            DT.Columns.Add("Master_No");
            DT.Columns.Add("COMP_Remark_MGR", typeof(string));
            DT.Columns.Add("COMP_Type_Name", typeof(string));

            foreach (RepeaterItem Item in rptAss.Items)
            {
                HtmlTableCell cell_COMP_type = (HtmlTableCell)Item.FindControl("cell_COMP_type");
                HtmlTableCell ass_no = (HtmlTableCell)Item.FindControl("ass_no");
                TextBox txtMGR = (TextBox) Item.FindControl("txtMGR");

                if (txtMGR.Text != "")
                {
                    if (txtMGR.Text.Length > 3990)
                    {
                        txtMGR.Text = txtMGR.Text.Substring(0, 3990);
                    }

                }

                DR = DT.NewRow();
                DR["COMP_Type_Id"] = ass_no.Attributes["COMP_Type_Id"];
                DR["Master_No"] = ass_no.InnerHtml;
                DR["COMP_Remark_MGR"] = txtMGR.Text;
                DR["COMP_Type_Name"] = cell_COMP_type.InnerHtml;
                DT.Rows.Add(DR);
            }
            return DT;
        }
        protected void btnSaveRemark_Click(object sender, System.EventArgs e)
        {


            try
            {
                SaveRemark();
            }
            catch
            {
                ScriptManager.RegisterStartupScript(this.Page, typeof(string), "Alert", "showAlert('ไม่สามารถบันทึกได้');", true);
            }

            ScriptManager.RegisterStartupScript(this.Page, typeof(string), "Alert", "showAlert('บันทึกแล้ว');", true);
            BindMasterCOMP();

        }


        private void SaveRemark()
        {
            DataTable DT = Current_Remark();
            for (int i = 0; i <= DT.Rows.Count - 1; i++)
            {

                string SQL_Remark = "SELECT * FROM tb_HR_COMP_Detail \n";
                SQL_Remark += " WHERE PSNL_NO='" + PSNL_NO.Replace("'", "''") + "' ";
                SQL_Remark += " AND R_Year=" + R_Year + "\n";
                SQL_Remark += " AND R_Round=" + R_Round + "\n";
                SQL_Remark += " AND COMP_Type_Id=" + DT.Rows[i]["COMP_Type_Id"] + "\n";
                SQL_Remark += " AND Master_No=" + DT.Rows[i]["Master_No"] + "\n";

                SqlDataAdapter DA = new SqlDataAdapter(SQL_Remark, BL.ConnectionString());
                DataTable DT_Remark = new DataTable();
                DA.Fill(DT_Remark);

                if (DT_Remark.Rows.Count == 0)
                    return;

                DT_Remark.Rows[0]["COMP_Remark_MGR"] = DT.Rows[i]["COMP_Remark_MGR"].ToString();
                //DT_Remark.Rows[0]["Update_By"] = Session["USER_PSNL_NO"];
                //DT_Remark.Rows[0]["Update_Time"] = DateAndTime.Now;

                SqlCommandBuilder cmd = new SqlCommandBuilder(DA);
DA.Update(DT_Remark);
              

            }

        }


	}
}

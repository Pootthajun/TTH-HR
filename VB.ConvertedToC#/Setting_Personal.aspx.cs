using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Xml.Linq;
using System.Diagnostics;
using System.Collections.Specialized;
using System.Configuration;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Caching;
using System.Web.SessionState;
using System.Web.Security;
using System.Web.Profile;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;

namespace VB
{

	public partial class Setting_Personal : System.Web.UI.Page
	{


		HRBL BL = new HRBL();
		GenericLib GL = new GenericLib();

		textControlLib TC = new textControlLib();
		protected void Page_Load(object sender, System.EventArgs e)
		{
			if ((Session["USER_PSNL_NO"] == null)) {
				ScriptManager.RegisterStartupScript(this.Page, typeof(string), "Alert", "showAlert('กรุณาลงชื่อเข้าใช้ระบบ');", true);
				Response.Redirect("Login.aspx");
				return;
			}

			if (!IsPostBack) {
				BindPersonal();
				BL.BindDDlSector(ddlSector);
			}
		}

		private void BindPersonal()
		{
			string SQL = " SELECT DEPT_CODE,SECTOR_CODE,SECTOR_NAME,DEPT_NAME,PSNL_NO,PSNL_Fullname,vw.PNPS_CLASS,WAGE_NAME,ISNULL(MGR_NAME,FN_NAME) POS_Name,Update_Time" + "\n";
			SQL += " FROM vw_PN_PSNL vw " + "\n";
			string Filter = "";
			//If ddlSector.SelectedIndex > 0 Then
			//    Filter &= " SECTOR_CODE='" & ddlSector.Items(ddlSector.SelectedIndex).Value & "' AND "
			//End If

			if (ddlSector.SelectedIndex > 0) {
				Filter += " ( LEFT(DEPT_CODE,2)='" + ddlSector.Items[ddlSector.SelectedIndex].Value + "' " + "\n";
				Filter += " OR LEFT(DEPT_CODE,4)='" + ddlSector.Items[ddlSector.SelectedIndex].Value + "'   " + "\n";

				if (ddlSector.Items[ddlSector.SelectedIndex].Value == "3200") {
					Filter += " AND DEPT_CODE NOT IN ('32000300'))    AND" + "\n";
				} else if (ddlSector.Items[ddlSector.SelectedIndex].Value == "3203") {
					Filter += " OR DEPT_CODE IN ('32000300','32030000','32030100','32030200','32030300','32030500'))  AND" + "\n";
				} else if (ddlSector.Items[ddlSector.SelectedIndex].Value == "3204") {
					Filter += " OR DEPT_CODE IN ('32040000','32040100','32040200','32040300','32040500','32040300'))  AND" + "\n";
				} else if (ddlSector.Items[ddlSector.SelectedIndex].Value == "3205") {
					Filter += " OR DEPT_CODE IN ('32050000','32050100','32050200','32050300','32050500'))  AND" + "\n";
				} else {
					Filter += ")   AND" + "\n";
				}

			}


			if (!string.IsNullOrEmpty(txtFilter.Text)) {
				Filter += " (" + "\n";
				Filter += " SECTOR_NAME LIKE '%" + txtFilter.Text.Replace("'", "''") + "%' OR " + "\n";
				Filter += " DEPT_NAME LIKE '%" + txtFilter.Text.Replace("'", "''") + "%' OR " + "\n";
				Filter += " PSNL_NO LIKE '%" + txtFilter.Text.Replace("'", "''") + "%' OR " + "\n";
				Filter += " PSNL_Fullname LIKE '%" + txtFilter.Text.Replace("'", "''") + "%' OR " + "\n";
				Filter += " ISNULL(MGR_NAME,FN_NAME) LIKE '%" + txtFilter.Text.Replace("'", "''") + "%'" + "\n";
				Filter += " ) AND ";
			}
			if (!string.IsNullOrEmpty(Filter)) {
				SQL += " WHERE " + Filter.Substring(0, Filter.Length - 4) + "\n";
			}
			SQL += " ORDER BY SECTOR_CODE,DEPT_CODE,vw.PNPS_CLASS DESC,POS_NO" + "\n";

			SqlDataAdapter DA = new SqlDataAdapter(SQL, BL.ConnectionString());
			DataTable DT = new DataTable();
			DA.Fill(DT);

			Session["Setting_Personal"] = DT;
			Pager.SesssionSourceName = "Setting_Personal";
			Pager.RenderLayout();

			if (DT.Rows.Count == 0) {
				lblCountPSN.Text = "ไม่พบรายการดังกล่าว";
			} else {
				lblCountPSN.Text = "พบ " + GL.StringFormatNumber(DT.Rows.Count.ToString(), 0) + " รายการ";
			}

		}

		protected void Pager_PageChanging(PageNavigation Sender)
		{
			Pager.TheRepeater = rptPSN;
		}

		string LastSECTOR = "";
		string LastDEPT = "";
		protected void rptPSN_ItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
		{
			if (e.Item.ItemType != ListItemType.AlternatingItem & e.Item.ItemType != ListItemType.Item)
				return;

            Label lblSector = (Label)e.Item.FindControl("lblSector");
            Label lblDept = (Label)e.Item.FindControl("lblDept");
            Label lblName = (Label)e.Item.FindControl("lblName");
            Label lblClass = (Label)e.Item.FindControl("lblClass");
            Label lblPOS = (Label)e.Item.FindControl("lblPOS");
            Label lblType = (Label)e.Item.FindControl("lblType");
            Label lblUpdate = (Label)e.Item.FindControl("lblUpdate");

            DataRowView drv = (DataRowView)e.Item.DataItem;

			if (LastSECTOR != drv["SECTOR_NAME"].ToString()) {
				lblSector.Text = drv["SECTOR_NAME"].ToString();
				LastSECTOR = drv["SECTOR_NAME"].ToString();
				lblDept.Text = drv["DEPT_NAME"].ToString();
				LastDEPT = drv["DEPT_NAME"].ToString();
			} else if (LastDEPT != drv["DEPT_NAME"].ToString()) {
				lblDept.Text = drv["DEPT_NAME"].ToString();
				LastDEPT = drv["DEPT_NAME"].ToString();
			}

			lblName.Text = drv["PSNL_NO"].ToString() + " : " + drv["PSNL_Fullname"].ToString();
			lblPOS.Text = drv["POS_Name"].ToString();
			lblType.Text =drv["WAGE_NAME"].ToString();
			if (Information.IsNumeric(drv["PNPS_CLASS"])) {
				lblClass.Text = GL.CINT ( drv["PNPS_CLASS"]).ToString();
			}

			if (!GL.IsEqualNull(drv["Update_Time"])) {
                lblUpdate.Text = BL.DateTimeToThaiDateTime((DateTime)drv["Update_Time"]);
			} else {
				lblUpdate.Text = "-";
			}

		}

		protected void Search_Changed(object sender, System.EventArgs e)
		{
			BindPersonal();
		}
		public Setting_Personal()
		{
			Load += Page_Load;
		}

	}
}

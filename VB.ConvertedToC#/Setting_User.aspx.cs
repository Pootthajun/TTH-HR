using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Xml.Linq;
using System.Diagnostics;
using System.Collections.Specialized;
using System.Configuration;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Caching;
using System.Web.SessionState;
using System.Web.Security;
using System.Web.Profile;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
namespace VB
{

    public partial class Setting_User : System.Web.UI.Page
    {


        HRBL BL = new HRBL();
        GenericLib GL = new GenericLib();

        textControlLib TC = new textControlLib();
        public string PSNL_NO
        {
            get { return lblUserCode.Text; }
            set { lblUserCode.Text = value.ToString(); }
        }

        protected void Page_Load(object sender, System.EventArgs e)
        {
            if ((Session["USER_PSNL_NO"] == null))
            {
                ScriptManager.RegisterStartupScript(this.Page, typeof(string), "Alert", "showAlert('กรุณาลงชื่อเข้าใช้ระบบ');", true);
                Response.Redirect("Login.aspx");
                return;
            }

            if (!IsPostBack)
            {
                BindUserList();
            }
        }

        protected void Search_Changed(object sender, System.EventArgs e)
        {
            BindUserList();
        }

        private void BindUserList()
        {
            string SQL = "";
            SQL += " SELECT PSN.SECTOR_NAME,PSN.DEPT_NAME,U.PSNL_NO,U.HR_User_Name,U.HR_Password," + "\n";
            SQL += " PSN.PSNL_Fullname,PSN.WAGE_NAME,ISNULL(PSN.MGR_NAME,PSN.FN_NAME) POS_Name," + "\n";
            SQL += " PNPS_CLASS," + "\n";
            SQL += " CAST(ISNULL(U.Is_Admin,0) AS bit) Is_Admin," + "\n";
            SQL += " CAST(ISNULL(U.Is_GAP_Editor,0) AS bit) Is_GAP_Editor," + "\n";
            //--------------เพิ่ม Career Path-------------
            SQL += " CAST(ISNULL(U.Is_Property_Pos,0) AS bit) Is_Property_Pos," + "\n";
            SQL += " CAST(ISNULL(U.Is_View_PSN,0) AS bit) Is_View_PSN," + "\n";
            //--------------เพิ่ม Workload -------------
            SQL += " CAST(ISNULL(U.Is_WKL_Admin,0) AS bit) Is_WKL_Admin," + "\n";
            SQL += " CAST(ISNULL(U.Is_WKL_User,0) AS bit) Is_WKL_User," + "\n";

            SQL += " CAST(ISNULL(U.Is_Allow,0) AS bit) Is_Allow" + "\n";
            SQL += " FROM tb_HR_User U" + "\n";
            SQL += " INNER JOIN vw_PN_PSNL PSN ON U.PSNL_NO=PSN.PSNL_NO" + "\n";
            string Filter = "";
            if (!string.IsNullOrEmpty(txtSearchDept.Text))
            {
                Filter += "(DEPT_NAME LIKE '%" + txtSearchDept.Text.Replace("'", "''") + "%' OR " + "\n";
                Filter += "DEPT_CODE LIKE '%" + txtSearchDept.Text.Replace("'", "''") + "%' OR " + "\n";
                Filter += "PSN.WAGE_NAME LIKE '%" + txtSearchDept.Text.Replace("'", "''") + "%' OR " + "\n";
                Filter += "ISNULL(PSN.MGR_NAME,PSN.FN_NAME) LIKE '%" + txtSearchDept.Text.Replace("'", "''") + "%') AND ";
            }
            if (!string.IsNullOrEmpty(txtSearchName.Text))
            {
                Filter += "(U.HR_User_Name LIKE '%" + txtSearchName.Text.Replace("'", "''") + "%' OR PSN.PSNL_Fullname LIKE '%" + txtSearchName.Text.Replace("'", "''") + "%') AND ";
            }
            switch (ddlSearchAdmin.SelectedIndex)
            {
                case 1:
                    Filter += "ISNULL(U.Is_Admin,0)=1 AND ";
                    break;
                case 2:
                    Filter += "ISNULL(U.Is_Admin,0)=0 AND ";
                    break;
            }
            switch (ddlSearchGAP.SelectedIndex)
            {
                case 1:
                    Filter += "ISNULL(U.Is_GAP_Editor,0)=1 AND ";
                    break;
                case 2:
                    Filter += "ISNULL(U.Is_GAP_Editor,0)=0 AND ";
                    break;
            }

            switch (ddlSearchPropertyPos.SelectedIndex)
            {
                case 1:
                    Filter += "ISNULL(U.Is_Property_Pos,0)=1 AND ";
                    break;
                case 2:
                    Filter += "ISNULL(U.Is_Property_Pos,0)=0 AND ";
                    break;
            }

            //--------------เพิ่ม Career Path-------------
            switch (ddlSearchViewPSN.SelectedIndex)
            {
                case 1:
                    Filter += "ISNULL(U.Is_View_PSN,0)=1 AND ";
                    break;
                case 2:
                    Filter += "ISNULL(U.Is_View_PSN,0)=0 AND ";
                    break;
            }

            switch (ddlSearchAllow.SelectedIndex)
            {
                case 1:
                    Filter += "ISNULL(U.Is_Allow,0)=1 AND ";
                    break;
                case 2:
                    Filter += "ISNULL(U.Is_Allow,0)=0 AND ";
                    break;
            }

            if (!string.IsNullOrEmpty(Filter))
            {
                SQL += " WHERE " + Filter.Substring(0, Filter.Length - 4) + "\n";
            }

            SQL += "  " + "\n";
            SQL += " ORDER BY SECTOR_CODE,DEPT_CODE,POS_NO" + "\n";


            SqlDataAdapter DA = new SqlDataAdapter(SQL, BL.ConnectionString());
            DataTable DT = new DataTable();
            DA.Fill(DT);

            Session["Setting_User"] = DT;
            Pager.SesssionSourceName = "Setting_User";
            Pager.RenderLayout();

            if (DT.Rows.Count == 0)
            {
                lblCountUser.Text = "ไม่พบรายการดังกล่าว";
            }
            else
            {
                // Declare an object variable. 
                object sumObject = null;
                sumObject = DT.Compute("COUNT(PSNL_No)", "");
                lblCountUser.Text = "พบ " + GL.StringFormatNumber(DT.Rows.Count, 0) + " รายการ";
            }

        }

        protected void Pager_PageChanging(PageNavigation Sender)
        {
            Pager.TheRepeater = rptUser;
        }


        string LastDEPT = "";
        protected void rptUser_ItemCommand(object source, System.Web.UI.WebControls.RepeaterCommandEventArgs e)
        {

            Label lblCode = (Label)e.Item.FindControl("lblCode");
            string HR_User_Name = lblCode.Text;
            string SQL = "SELECT * FROM tb_HR_User WHERE HR_User_Name='" + HR_User_Name.Replace("'", "''") + "'";
            SqlDataAdapter DA;
            DataTable DT;
            SqlCommandBuilder cmd;

            switch (e.CommandName)
            {
                case "View":

                    lblCode = (Label)e.Item.FindControl("lblCode");
                    Label lblDept = (Label)e.Item.FindControl("lblDept");
                    Label lblName = (Label)e.Item.FindControl("lblName");
                    Label lblPos = (Label)e.Item.FindControl("lblPos");
                    ImageButton btnAdmin = (ImageButton)e.Item.FindControl("btnAdmin");
                    ImageButton btnGAP = (ImageButton)e.Item.FindControl("btnGAP");
                    //--------------เพิ่ม Career Path-------------
                    ImageButton btnProperty = (ImageButton)e.Item.FindControl("btnProperty");
                    ImageButton btnViewPSN = (ImageButton)e.Item.FindControl("btnViewPSN");
                    //--------------เพิ่ม Workload-------------
                    ImageButton btnWorkloadAdmin = (ImageButton)e.Item.FindControl("btnWorkloadAdmin");
                    ImageButton btnWorkloadUser = (ImageButton)e.Item.FindControl("btnWorkloadUser");

                    ImageButton btnAllow = (ImageButton)e.Item.FindControl("btnAllow");

                    PSNL_NO = lblCode.Text;
                    lblUserName.Text = lblName.Text;
                    lblPOSName.Text = lblPos.Text;
                    lblDEPTName.Text = lblDept.Attributes["DEPT_NAME"];
                    txtPassword.Text = lblName.Attributes["PW"];

                    btnEditAdmin.ImageUrl = btnAdmin.ImageUrl;
                    btnEditGAP.ImageUrl = btnGAP.ImageUrl;
                    //--------------เพิ่ม Career Path-------------
                    btnEditPropertyPos.ImageUrl = btnProperty.ImageUrl;
                    btnEditViewPSN.ImageUrl = btnViewPSN.ImageUrl;
                    //--------------เพิ่ม Workload -------------
                    btnEditWorkloadAdmin.ImageUrl = btnWorkloadAdmin.ImageUrl;
                    btnEditWorkloadUser.ImageUrl = btnWorkloadUser.ImageUrl;

                    btnEditAllow.ImageUrl = btnAllow.ImageUrl;

                    divModal.Visible = true;

                    break;

                case "Admin":

                    DA = new SqlDataAdapter(SQL, BL.ConnectionString());
                    DT = new DataTable();
                    DA.Fill(DT);
                    if (DT.Rows.Count == 0)
                    {
                        BindUserList();
                        return;
                    }
                    if (GL.IsEqualNull(DT.Rows[0]["Is_Admin"]) || !GL.CBOOL(DT.Rows[0]["Is_Admin"]))
                    {
                        DT.Rows[0]["Is_Admin"] = true;
                    }
                    else
                    {
                        DT.Rows[0]["Is_Admin"] = false;
                    }
                    cmd = new SqlCommandBuilder(DA);
                    DA.Update(DT);
                    DT.AcceptChanges();
                    BindUserList();

                    break;
                case "GAP":

                    DA = new SqlDataAdapter(SQL, BL.ConnectionString());
                    DT = new DataTable();
                    DA.Fill(DT);
                    if (DT.Rows.Count == 0)
                    {
                        BindUserList();
                        return;
                    }
                    if (GL.IsEqualNull(DT.Rows[0]["Is_GAP_Editor"]) || !GL.CBOOL(DT.Rows[0]["Is_GAP_Editor"]))
                    {
                        DT.Rows[0]["Is_GAP_Editor"] = true;
                    }
                    else
                    {
                        DT.Rows[0]["Is_GAP_Editor"] = false;
                    }
                    cmd = new SqlCommandBuilder(DA);
                    DA.Update(DT);
                    DT.AcceptChanges();
                    BindUserList();

                    break;

                //------Career path------
                case "SettingProperty":

                    DA = new SqlDataAdapter(SQL, BL.ConnectionString());
                    DT = new DataTable();
                    DA.Fill(DT);
                    if (DT.Rows.Count == 0)
                    {
                        BindUserList();
                        return;
                    }
                    if (GL.IsEqualNull(DT.Rows[0]["Is_Property_Pos"]) || !GL.CBOOL(DT.Rows[0]["Is_Property_Pos"]))
                    {
                        DT.Rows[0]["Is_Property_Pos"] = true;
                    }
                    else
                    {
                        DT.Rows[0]["Is_Property_Pos"] = false;
                    }
                    cmd = new SqlCommandBuilder(DA);
                    DA.Update(DT);
                    DT.AcceptChanges();
                    BindUserList();

                    break;

                //------Career path------
                case "ViewPSN":

                    DA = new SqlDataAdapter(SQL, BL.ConnectionString());
                    DT = new DataTable();
                    DA.Fill(DT);
                    if (DT.Rows.Count == 0)
                    {
                        BindUserList();
                        return;
                    }
                    if (GL.IsEqualNull(DT.Rows[0]["Is_View_PSN"]) || !GL.CBOOL(DT.Rows[0]["Is_View_PSN"]))
                    {
                        DT.Rows[0]["Is_View_PSN"] = true;
                    }
                    else
                    {
                        DT.Rows[0]["Is_View_PSN"] = false;
                    }
                    cmd = new SqlCommandBuilder(DA);
                    DA.Update(DT);
                    DT.AcceptChanges();
                    BindUserList();

                    break;

                case "Allow":

                    DA = new SqlDataAdapter(SQL, BL.ConnectionString());
                    DT = new DataTable();
                    DA.Fill(DT);
                    if (DT.Rows.Count == 0)
                    {
                        BindUserList();
                        return;
                    }
                    if (GL.IsEqualNull(DT.Rows[0]["Is_Allow"]) || !GL.CBOOL(DT.Rows[0]["Is_Allow"]))
                    {
                        DT.Rows[0]["Is_Allow"] = true;
                    }
                    else
                    {
                        DT.Rows[0]["Is_Allow"] = false;
                    }
                    cmd = new SqlCommandBuilder(DA);
                    DA.Update(DT);
                    DT.AcceptChanges();
                    BindUserList();

                    break;

                case "WorkloadAdmin":

                    DA = new SqlDataAdapter(SQL, BL.ConnectionString());
                    DT = new DataTable();
                    DA.Fill(DT);
                    if (DT.Rows.Count == 0)
                    {
                        BindUserList();
                        return;
                    }
                    if (GL.IsEqualNull(DT.Rows[0]["Is_WKL_Admin"]) || !GL.CBOOL(DT.Rows[0]["Is_WKL_Admin"]))
                    {
                        DT.Rows[0]["Is_WKL_Admin"] = true;
                    }
                    else
                    {
                        DT.Rows[0]["Is_WKL_Admin"] = false;
                    }
                    cmd = new SqlCommandBuilder(DA);
                    DA.Update(DT);
                    DT.AcceptChanges();
                    BindUserList();
                    break;

                case "WorkloadUser":

                    DA = new SqlDataAdapter(SQL, BL.ConnectionString());
                    DT = new DataTable();
                    DA.Fill(DT);
                    if (DT.Rows.Count == 0)
                    {
                        BindUserList();
                        return;
                    }
                    if (GL.IsEqualNull(DT.Rows[0]["Is_WKL_User"]) || !GL.CBOOL(DT.Rows[0]["Is_WKL_User"]))
                    {
                        DT.Rows[0]["Is_WKL_User"] = true;
                    }
                    else
                    {
                        DT.Rows[0]["Is_WKL_User"] = false;
                    }
                    cmd = new SqlCommandBuilder(DA);
                    DA.Update(DT);
                    DT.AcceptChanges();
                    BindUserList();
                    break;


                //Case "Edit"
                //    Dim lblCode As Label = e.Item.FindControl("lblCode")
                //    Dim chkAdmin As CheckBox = e.Item.FindControl("chkAdmin")
                //    Dim chkGAP As CheckBox = e.Item.FindControl("chkGAP")
                //    Dim chkAllow As CheckBox = e.Item.FindControl("chkAllow")

                //    Dim HR_User_Name As String = lblCode.Text
                //    Dim SQL As String = "SELECT * FROM tb_HR_User WHERE HR_User_Name='" & HR_User_Name.Replace("'", "''") & "'"
                //    Dim DA As New SqlDataAdapter(SQL, BL.ConnectionString())
                //    Dim DT As New DataTable
                //    DA.Fill(DT)
                //    If DT.Rows.Count = 0 Then
                //        BindUserList()
                //        Exit Sub
                //    End If

                //    DT.Rows(0).Item("Is_Admin") = chkAdmin.Checked
                //    DT.Rows(0).Item("Is_GAP_Editor") = chkGAP.Checked
                //    DT.Rows(0).Item("Is_Allow") = chkAllow.Checked

                //    DT.Rows(0).Item("Update_By") = Session("USER_PSNL_NO")
                //    DT.Rows(0).Item("Update_Time") = Now

                //    Dim cmd As New SqlCommandBuilder(DA)
                //    DA.Update(DT)
                //    DT.AcceptChanges()

                //    BindUserList()
            }
        }
        protected void rptUser_ItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType != ListItemType.AlternatingItem & e.Item.ItemType != ListItemType.Item)
                return;

            Label lblDept = (Label)e.Item.FindControl("lblDept");
            Label lblCode = (Label)e.Item.FindControl("lblCode");
            Label lblName = (Label)e.Item.FindControl("lblName");
            Label lblPos = (Label)e.Item.FindControl("lblPos");
            ImageButton btnAdmin = (ImageButton)e.Item.FindControl("btnAdmin");
            ImageButton btnGAP = (ImageButton)e.Item.FindControl("btnGAP");
            //--------------เพิ่ม Career Path-------------
            ImageButton btnProperty = (ImageButton)e.Item.FindControl("btnProperty");
            ImageButton btnViewPSN = (ImageButton)e.Item.FindControl("btnViewPSN");
            //-------------Workload--------------------
            ImageButton btnWorkloadAdmin = (ImageButton)e.Item.FindControl("btnWorkloadAdmin");
            ImageButton btnWorkloadUser = (ImageButton)e.Item.FindControl("btnWorkloadUser");

            ImageButton btnAllow = (ImageButton)e.Item.FindControl("btnAllow");
            Button btnView = (Button)e.Item.FindControl("btnView");
            Button btnEdit = (Button)e.Item.FindControl("btnEdit");

            DataRowView drv = (DataRowView)e.Item.DataItem;

            if (LastDEPT != drv["DEPT_NAME"].ToString())
            {
                lblDept.Text = drv["DEPT_NAME"].ToString();
                LastDEPT = drv["DEPT_NAME"].ToString();
            }
            lblDept.Attributes["DEPT_NAME"] = drv["DEPT_NAME"].ToString();
            lblCode.Text = drv["PSNL_NO"].ToString();
            lblName.Text = drv["PSNL_Fullname"].ToString();

            lblName.Attributes["PNPS_CLASS"] = GL.CINT(drv["PNPS_CLASS"]).ToString();
            lblName.Attributes["POS_Name"] = drv["POS_Name"].ToString();
            lblName.Attributes["PW"] = drv["HR_Password"].ToString();

            lblPos.Text = drv["POS_Name"].ToString();
            if (GL.CBOOL(drv["Is_Admin"]))
            {
                btnAdmin.ImageUrl = "images/check.png";
            }
            else
            {
                btnAdmin.ImageUrl = "images/none.png";
            }

            if (GL.CBOOL(drv["Is_GAP_Editor"]))
            {
                btnGAP.ImageUrl = "images/check.png";
            }
            else
            {
                btnGAP.ImageUrl = "images/none.png";
            }

            //--------------เพิ่ม Career Path-------------
            if (GL.CBOOL(drv["Is_Property_Pos"]))
            {
                btnProperty.ImageUrl = "images/check.png";
            }
            else
            {
                btnProperty.ImageUrl = "images/none.png";
            }

            if (GL.CBOOL(drv["Is_View_PSN"]))
            {
                btnViewPSN.ImageUrl = "images/check.png";
            }
            else
            {
                btnViewPSN.ImageUrl = "images/none.png";
            }

            if (GL.CBOOL(drv["Is_WKL_Admin"]))
            {
                btnWorkloadAdmin.ImageUrl = "images/check.png";
            }
            else
            {
                btnWorkloadAdmin.ImageUrl = "images/none.png";
            }

            if (GL.CBOOL(drv["Is_WKL_User"]))
            {
                btnWorkloadUser.ImageUrl = "images/check.png";
            }
            else
            {
                btnWorkloadUser.ImageUrl = "images/none.png";
            }


            if (GL.CBOOL(drv["Is_Allow"]))
            {
                btnAllow.ImageUrl = "images/check.png";
            }
            else
            {
                btnAllow.ImageUrl = "images/none.png";
            }
            btnAllow.Attributes["Is_Allow"] = "True";


        }

        protected void btnClose_Click(object sender, System.EventArgs e)
        {
            divModal.Visible = false;
        }


        protected void btnOK_Click(object sender, System.EventArgs e)
        {
            if (txtPassword.Text.Length < 4)
            {
                ScriptManager.RegisterStartupScript(this.Page, typeof(string), "Alert", "showAlert('รหัสผ่านไม่น้อยกว่า 4 ตัวอักษร');", true);
                return;
            }

            string SQL = "SELECT * FROM tb_HR_User WHERE HR_User_Name='" + PSNL_NO.Replace("'", "''") + "'";
            SqlDataAdapter DA = new SqlDataAdapter(SQL, BL.ConnectionString());
            DataTable DT = new DataTable();
            DA.Fill(DT);
            if (DT.Rows.Count == 0)
            {
                BindUserList();
                divModal.Visible = false;
                return;
            }

            DT.Rows[0]["Is_Admin"] = btnEditAdmin.ImageUrl == "images/check.png";
            DT.Rows[0]["Is_GAP_Editor"] = btnEditGAP.ImageUrl == "images/check.png";
            //-------------เพิ่ม Career Path------------
            DT.Rows[0]["Is_Property_Pos"] = btnEditPropertyPos.ImageUrl == "images/check.png";
            DT.Rows[0]["Is_View_PSN"] = btnEditViewPSN.ImageUrl == "images/check.png";
            //-------------เพิ่ม Career Path------------
            DT.Rows[0]["Is_WKL_Admin"] = btnEditWorkloadAdmin.ImageUrl == "images/check.png";
            DT.Rows[0]["Is_WKL_User"] = btnEditWorkloadUser.ImageUrl == "images/check.png";

            DT.Rows[0]["Is_Allow"] = btnEditAllow.ImageUrl == "images/check.png";
            DT.Rows[0]["Update_By"] = Session["USER_PSNL_NO"];
            DT.Rows[0]["Update_Time"] = DateTime.Now;

            SqlCommandBuilder cmd = new SqlCommandBuilder(DA);
            DA.Update(DT);

            BindUserList();
            divModal.Visible = false;

        }

        protected void btnEdit_Click(object sender, System.Web.UI.ImageClickEventArgs e)
        {
            ImageButton btn = (ImageButton)sender;
            switch (btn.ImageUrl)
            {
                case "images/check.png":
                    btn.ImageUrl = "images/none.png";
                    break;
                default:
                    btn.ImageUrl = "images/check.png";
                    break;
            }
        }
        public Setting_User()
        {
            Load += Page_Load;
        }


    }
}

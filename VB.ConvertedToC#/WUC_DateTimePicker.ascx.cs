using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Xml.Linq;
using System.Diagnostics;
using System.Collections.Specialized;
using System.Configuration;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Caching;
using System.Web.SessionState;
using System.Web.Security;
using System.Web.Profile;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
namespace VB
{
	public partial class WUC_DateTimePicker : System.Web.UI.UserControl
	{


		HRBL BL = new HRBL();
		public string ThaiDateString {
			get { return txtDisplay.Text; }
		}

		public bool IsDateSelected {
			get { return Value > DateTime.FromOADate(100); }
		}

		public string EngDateString {
			get { return txtValue.Text; }
		}

		public DateTime Value {
			get {
				if (!BL.IsEngDate(txtValue.Text)) {
					return DateTime.FromOADate(0);
				} else {
					return BL.EngDateToDateTime(txtValue.Text);
				}
			}
			set {
				if (Conversion.Int(value.ToOADate()) == 0) {
					txtValue.Text = "";
					txtDisplay.Text = "";
					return;
				}
				txtValue.Text = BL.DateTimeToEngDate(value);
				txtDisplay.Text = BL.DateTimeToThaiDate(value);
			}
		}

		protected void Page_Load(object sender, System.EventArgs e)
		{
			if (!IsPostBack) {
				lnkPopup.Attributes["onclick"] = "document.getElementById('" + txtValue.ClientID + "').focus();";
				txtDisplay.Attributes["onfocus"] = "document.getElementById('" + txtValue.ClientID + "').focus();";

				txtValue.Attributes["onchange"] = "EngToThaiDate(document.getElementById('" + txtDisplay.ClientID + "'),this);";

				AjaxCalendar.TargetControlID = txtValue.ClientID;
				AjaxCalendar.BehaviorID = txtValue.ClientID;
			}
		}

	}
}

﻿<%@ Page  Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPage.Master" CodeBehind="Assessment_Team_COMP_Create.aspx.cs" Inherits="VB.Assessment_Team_COMP_Create" %>

<%@ Register src="WUC_DateReporter.ascx" tagname="WUC_DateReporter" tagprefix="uc1" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<%@ Register src="PageNavigation.ascx" tagname="PageNavigation" tagprefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<asp:ToolkitScriptManager ID="toolkit1" runat="server"></asp:ToolkitScriptManager>
<asp:UpdatePanel ID="UpdatePanel1" runat="server">      
<ContentTemplate>

<div class="container-fluid">
				<!-- BEGIN PAGE HEADER-->
				<div class="row-fluid" id="fixedPageHeader">
					<div class="span12"> 
						<!-- BEGIN PAGE TITLE & BREADCRUMB-->
						<h3>กำหนดแบบประเมินสมรรถนะพนักงาน (Competency) <font color="blue">(ScreenID : S-MGR-03)</font>
                        <asp:Label ID="lblReportTime" Font-Size="14px" ForeColor="Green" runat="Server"></asp:Label>
                        </h3>

						<h3 id="pnlFilterYear" runat="server" visible="True">
						     สำหรับรอบการประเมิน
                            <asp:DropDownList ID="ddlRound" OnSelectedIndexChanged="Search" runat="server" AutoPostBack="true" CssClass="medium m-wrap" style="font-size:20px;">
							</asp:DropDownList>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;
                            <asp:DropDownList ID="ddlForm" OnSelectedIndexChanged="ddlForm_SelectedIndexChanged" runat="server"  Visible ="false"  style="font-size:20px;" AutoPostBack="true" CssClass="medium m-wrap">
				                    <asp:ListItem Value="0">ประเมินผลสมรรถนะ  </asp:ListItem>
				                    <asp:ListItem Value="1"  Selected="true">กำหนดแบบประเมิน </asp:ListItem>
			                </asp:DropDownList>
						</h3>						
									
						<ul class="breadcrumb">
                            <li>
                            	<i class="icon-user"></i> <a href="javascript:;">การประเมินพนักงาน</a><i class="icon-angle-right"></i>
                            </li>
                        	<li>
                        	    <i class="icon-th-list"></i> <a href="javascript:;">กำหนดแบบประเมินสมรรถนะ (Competency)</a>
                        	</li>                        	
                            <uc1:WUC_DateReporter ID="WUC_DateReporter1" runat="server" />
                        </ul>
						<!-- END PAGE TITLE & BREADCRUMB-->
									        
				     </div>

				
			    </div>
		        <!-- END PAGE HEADER-->
				
				<asp:Panel ID="pnlList" runat="server" Visible="True" DefaultButton="btnSearch">
				<div class="row-fluid">
					
					<div class="span12">
						<!-- BEGIN SAMPLE TABLE PORTLET-->
						            
						            <div class="row-fluid form-horizontal">
									    <div class="span4 ">
											<div class="control-group">
										        <label class="control-label"><i class="icon-user"></i> ชื่อ</label>
										        <div class="controls">
											        <asp:TextBox ID="txtName" OnTextChanged="Search" runat="server" AutoPostBack="true" CssClass="m-wrap medium" placeholder="ค้นหาจากชื่อ/เลขประจำตัว"></asp:TextBox>
										        </div>
									        </div>
										</div>
										<div class="span3 ">
											<div class="control-group">
										        <label class="control-label"><i class="icon-info-sign"></i> สถานะ</label>
										        <div class="controls">
											        <asp:DropDownList ID="ddlStatus" OnSelectedIndexChanged="Search" runat="server" AutoPostBack="true" CssClass="medium m-wrap">
											        </asp:DropDownList>
										        </div>
									        </div>
										</div>
									</div>
								    <asp:Button ID="btnSearch" OnClick="Search" runat="server" Text="" style="display:none;" />
								            
							<div class="portlet-body no-more-tables">
								<asp:Label ID="lblCountPSN" runat="server" Font-Size="14px" Font-Bold="true"></asp:Label>
								<table class="table table-full-width table-advance dataTable no-more-tables table-hover">
									<thead>
										<tr>
											<th><i class="icon-sitemap"></i> หน่วยงาน</th>
											<th><i class="icon-user"></i> เลขประจำตัว</th>
											<th><i class="icon-user"></i> ชื่อ</th>
											<th><i class="icon-briefcase"></i> ตำแหน่ง</th>
											<th><i class="icon-bookmark"></i> ระดับ</th>
											<th><i class="icon-info-sign"></i> สถานะ</th>
											<th><i class="icon-bolt"></i> ดำเนินการ</th>
										</tr>
									</thead>
									<tbody>
									<asp:Repeater ID="rptCOMP" OnItemCommand="rptCOMP_ItemCommand" OnItemDataBound="rptCOMP_ItemDataBound" runat="server">
									        <ItemTemplate>
									            <tr>
											    <td data-title="หน่วยงาน"><asp:Label ID="lnkPSNOrganize" runat="server"></asp:Label></td>
										        <td data-title="เลขประจำตัว"><asp:Label ID="lblPSNNo" runat="server"></asp:Label></td>
										        <td data-title="ชื่อ"><asp:Label ID="lblPSNName" runat="server"></asp:Label></td>
											    <td data-title="ตำแหน่ง"><asp:Label ID="lblPSNPos" runat="server"></asp:Label></td>
											    <td data-title="ระดับ"><asp:Label ID="lblPSNClass" runat="server"></asp:Label></td>
											    <td data-title="สถานะ"><asp:Label ID="lblPSNCOMPStatus" runat="server"></asp:Label></td>
											        <td data-title="ดำเนินการ">
											            <asp:Button ID="btnPSNEdit" runat="server" CssClass="btn mini blue" CommandName="Edit" Text="การกำหนดแบบประเมิน" /> 
											            <asp:Button ID="btnPSNDelete" runat="server" CssClass="btn mini red" CommandName="Delete" Text="ลบแบบประเมิน" />
											            <asp:ConfirmButtonExtender TargetControlID="btnPSNDelete" ID="cfm_Delete" runat="server"></asp:ConfirmButtonExtender>
										            </td>
										        </tr>
									        </ItemTemplate>
									</asp:Repeater>
									        	
									</tbody>
								</table>
								
								<asp:PageNavigation ID="Pager" OnPageChanging="Pager_PageChanging" MaximunPageCount="10" PageSize="20" runat="server" />
							</div>						
						<!-- END SAMPLE TABLE PORTLET-->						
					</div>
                </div>  
                </asp:Panel>  
                
                <asp:Panel ID="pnlEdit" runat="server" Visible="False">
                <div class="portlet-body form">										
			                    <div class="form-horizontal form-view">
    			                        <div class="btn-group pull-left">
                                                <asp:Button ID="btnBack_TOP" OnClick="btnBack_TOP_Click" runat="server" CssClass="btn" Text="ย้อนกลับ" />
                                          </div>
    			                        <div class="btn-group pull-right">                                    
			                                <a data-toggle="dropdown" class="btn dropdown-toggle">พิมพ์ <i class="icon-angle-down"></i></a>										
			                                <ul class="dropdown-menu pull-right">                                                       
                                                <li><a id="btnCOMPFormPDF" runat="server" target="_blank">ใบประเมิน รูปแบบ PDF</a></li>
                                                <li><a id="btnCOMPFormExcel" runat="server" target="_blank">ใบประเมิน รูปแบบ Excel</a></li>	
                                                <li><a id="btnIDPFormPDF" runat="server" target="_blank">แผนพัฒนารายบุคคล รูปแบบ PDF</a></li>
                                                <li><a id="btnIDPFormExcel" runat="server" target="_blank">แผนพัฒนารายบุคคล รูปแบบ Excel</a></li>	
                                                <li><a id="btnIDPResultFormPDF" runat="server" target="_blank">ผลการพัฒนาตามแผนพัฒนารายบุคคล รูปแบบ PDF</a></li>
                                                <li><a id="btnIDPResultFormExcel" runat="server" target="_blank">ผลการพัฒนาตามแผนพัฒนารายบุคคล รูปแบบ Excel</a></li>
                                                <li><a id="btnIDPRemark_Plan" href="Doc/RemarkIDP/Remark_PlanIDP.pdf" runat="server" target="_blank">Download คำชี้แจง แผนการพัฒนารายบุคคล</a></li>
                                                <li><a id="btnIDPRemark_Result" href="Doc/RemarkIDP/Remark_ResultIDP.pdf" runat="server" target="_blank">Download คำชี้แจง ผลการพัฒนาตามแผนพัฒนารายบุคคล</a></li>				                                </ul>
			                                </ul>
		                                </div>
    			                           			                               
		                                <h3 style="text-align:center; margin-top:0px;">
		                                    <asp:Label ID="lblCOMPStatus" runat="server" KPI_Status="0"></asp:Label> (Competency)
		                                </h3>   
    			                        
    			                        <div class="row-fluid" style="border-bottom:1px solid #EEEEEE;">
					                        <div class="span6 ">
					                            <div class="control-group">
								                    <label class="control-label" style="width:100px;">ประเภท :</label>
								                    <div class="controls" style="margin-left: 120px;">
									                    <asp:Label ID="lblPSNType" runat="server" CssClass="text bold"></asp:Label>
								                    </div>
							                    </div>
					                        </div>					        
					                     </div>
    			                        <div class="row-fluid" style="border-bottom:1px solid #EEEEEE;">
					                        <div class="span4 ">
					                            <div class="control-group">
								                    <label class="control-label" style="width:100px;">ผู้รับการประเมิน :</label>
								                    <div class="controls" style="margin-left: 120px;">
									                    <asp:Label ID="lblPSNName" runat="server" CssClass="text bold"></asp:Label>
								                    </div>
							                    </div>
					                        </div>
					                        <div class="span3 ">
					                            <div class="control-group">
								                    <label class="control-label" style="width:100px;">ตำแหน่ง :</label>
								                    <div class="controls" style="margin-left: 120px;">
									                    <asp:Label ID="lblPSNPos" runat="server" CssClass="text bold"></asp:Label>
								                    </div>
							                    </div>
					                        </div>
					                        <div class="span5 ">
					                            <div class="control-group">
								                    <label class="control-label" style="width:100px;">สังกัด :</label>
								                    <div class="controls" style="margin-left: 120px;">
									                    <asp:Label ID="lblPSNDept" runat="server" CssClass="text bold"></asp:Label>
								                    </div>
							                    </div>
					                        </div>
					                     </div>
                					     
					                      <div class="row-fluid" style="margin-top:10px;">
					                        <div class="span4 ">
					                            <div class="control-group">
								                    <label class="control-label" style="width:100px;">ผู้ประเมิน :</label>
								                    <div class="controls" style="margin-left: 120px;">
									                    <asp:DropDownList ID="ddlASSName" OnSelectedIndexChanged="ddlASSName_SelectedIndexChanged" runat="Server" AutoPostBack="True" style="font-weight:bold; color:Black; border:none;">									        
									                    </asp:DropDownList>
									                </div>
							                    </div>
					                        </div>
					                        <div class="span3 ">
					                            <div class="control-group">
								                    <label class="control-label" style="width:100px;">ตำแหน่ง :</label>
								                    <div class="controls" style="margin-left: 120px;">
									                    <asp:Label ID="lblASSPos" runat="server" CssClass="text bold"></asp:Label>
								                    </div>
							                    </div>
					                        </div>
					                        <div class="span5 ">
					                            <div class="control-group">
								                    <label class="control-label" style="width:100px;">สังกัด :</label>
								                    <div class="controls" style="margin-left: 120px;">
									                    <asp:Label ID="lblASSDept" runat="server" CssClass="text bold"></asp:Label>
								                    </div>
							                    </div>
					                        </div>
					                     </div>
					             </div>
					
												
				    <!-- BEGIN FORM-->
				    <div class="portlet-body no-more-tables">
				
				                         <table class="table table-bordered no-more-tables" style="background-color:White;">
									        <thead>
										        <tr>
											        <th rowspan="2" style="text-align:center; background-color:Silver;"> ลำดับ</th>
											        <th rowspan="2" style="text-align:center; background-color:Silver;"> สมรรถนะ</th>
											        <th rowspan="2" style="text-align:center; background-color:Silver;"> มาตรฐานพฤติกรรม</th>
											        <th colspan="5" style="text-align:center; background-color:Silver;"> คะแนนตามระดับค่าเป้าหมาย</th>
											        <th rowspan="2" style="text-align:center; background-color:Silver;"> น้ำหนัก(%)<br><asp:Button ID="btnAVG_Weight" OnClick="btnAVG_Weight_Click" runat="server" CssClass=" btn mini  blue " Text="เฉลี่ยน้ำหนัก" />	</th>											        
										        </tr>
                                                <tr>
										          <th class="AssLevel1" style="text-align:center; font-weight:bold; vertical-align:middle;">1</th>
										          <th class="AssLevel2" style="text-align:center; font-weight:bold; vertical-align:middle;">2</th>
										          <th class="AssLevel3" style="text-align:center; font-weight:bold; vertical-align:middle;">3</th>
										          <th class="AssLevel4" style="text-align:center; font-weight:bold; vertical-align:middle;">4</th>
										          <th class="AssLevel5" style="text-align:center; font-weight:bold; vertical-align:middle;">5</th>
								              </tr>
									        </thead>
									        <tbody>
									             <asp:Panel ID="pnlCOMP" runat="server">
									             
									             
									                    <asp:Repeater ID="rptAss" OnItemCommand="rptAss_ItemCommand" OnItemDataBound="rptAss_ItemDataBound" runat="server">
									                        <ItemTemplate>
									                        <tr id="row_COMP_type" runat="server">
									                            <td colspan="9" style="font-weight:bold; font-size:18px; background-color:#AAAAAA;" id="cell_COMP_type" runat="server">สมรรถนะหลัก Core Competency, สมรรถนะตามสายงาน Functional Competency, สมรรถนะตามสายระดับ Managerial Competency</td>
									                        </tr>
        									                
									                        <tr>
										                      <td data-title="ลำดับ" id="ass_no" runat="server" style="text-align:center; font-weight:bold; vertical-align:top;">001</td>
										                      <td data-title="สมรรถนะ" id="COMP_Name" class="TextLevel3" runat="server" style="background-color:#f8f8f8; ">xxxxxxxxxxxx xxxxxxxxxxxx</td>
										                      <td data-title="มาตรฐานพฤติกรรม" id="target" runat="server" class="TextLevel3" style="background-color:#f8f8f8; ">xxxxxxxxxxxx xxxxxxxxxxxx</td>
										                      <td data-title="ระดับที่ 1" id="choice1" class="TextLevel3" runat="server" style="background-color:White;">xxxxxxxxxxxx xxxxxxxxxxxx xxxxxxxxxxxx xxxxxxxxxxxx xxxxxxxxxxxx xxxxxxxxxxxx</td>
                                                              <td data-title="ระดับที่ 2" id="choice2" class="TextLevel3" runat="server" style="background-color:White;">xxxxxxxxxxxx xxxxxxxxxxxx xxxxxxxxxxxx xxxxxxxxxxxx xxxxxxxxxxxx xxxxxxxxxxxx</td>
                                                              <td data-title="ระดับที่ 3" id="choice3" class="TextLevel3" runat="server" style="background-color:White;">xxxxxxxxxxxx xxxxxxxxxxxx xxxxxxxxxxxx xxxxxxxxxxxx xxxxxxxxxxxx xxxxxxxxxxxx</td>
                                                              <td data-title="ระดับที่ 4" id="choice4" class="TextLevel3" runat="server" style="background-color:White;">xxxxxxxxxxxx xxxxxxxxxxxx xxxxxxxxxxxx xxxxxxxxxxxx xxxxxxxxxxxx xxxxxxxxxxxx</td>
                                                              <td data-title="ระดับที่ 5" id="choice5" class="TextLevel3" runat="server" style="background-color:White;">xxxxxxxxxxxx xxxxxxxxxxxx xxxxxxxxxxxx xxxxxxxxxxxx xxxxxxxxxxxx xxxxxxxxxxxx</td>
										                      <td data-title="น้ำหนัก" title="น้ำหนัก" id="cel_weight" runat="server" style="text-align:center; font-size:14px; font-weight:bold; vertical-align:middle;">
        										              <asp:Panel ID="pnlWeight" runat="server" DefaultButton="btnUpdate">
        										                  <asp:Textbox ID="txtWeight" MaxLength="5" runat="server"  Enabled ="false"  style="width:90%; padding:5px; font-size:14px; 
										                              font-weight:bold; text-align:center; float:left; background-color:Transparent; 
										                              border:0px none transparent; color:black; text-align:center" Text=""  placeholder="น้ำหนัก ??"></asp:Textbox>    										                      
										                          <asp:Button ID="btnUpdate" runat="server" style="display:none;" CommandName="Update" />
        										              </asp:Panel>
        										              
        										              
										                      </td>            										          
									                        </tr>
        									               
									                        </ItemTemplate>
									                        <FooterTemplate>        									                
									                            <tfoot>
									                                <tr>
									                                    <th colspan="8" style="text-align:center; background-color:#EEEEEE; font-size:14px; font-weight:bold;">
									                                    ผลรวมน้ำหนัก
									                                    </th>
									                                    <th id="cell_sum_weight" runat="server" style="text-align:center; color:White; font-size:16px; font-weight:bold;" colspan="2">-</th>
									                                </tr>									                        
									                            </tfoot>
        									                
									                        </FooterTemplate>
									                    </asp:Repeater>	
									              </asp:Panel>
									        </tbody>									        
								        </table>
								        
								                <div class="form-actions">
												    <asp:Button ID="btnPreSend" OnClick="btnPreSend_Click" runat="server" CssClass="btn green" Text="อนุมัติแบบประเมิน (หลังจากอนุมัติแบบประเมินแล้วคุณไม่สามารถแก้ไขได้)" />
							                        <asp:Button ID="btnSend" OnClick="btnSend_Click" runat="server" Text="" style="display:none;" />
												    <asp:Button ID="btnBack" OnClick="btnBack_Click" runat="server" CssClass="btn" Text="ย้อนกลับ" />
												</div>
							</div>						
				    <!-- END FORM-->  				   
			    </div>
                </asp:Panel>
					
</div>

</ContentTemplate>           
</asp:UpdatePanel>

</asp:Content>


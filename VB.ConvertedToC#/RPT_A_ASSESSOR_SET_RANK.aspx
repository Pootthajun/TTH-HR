﻿<%@ Page  Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPage.Master" CodeBehind="RPT_A_ASSESSOR_SET_RANK.aspx.cs" Inherits="VB.RPT_A_ASSESSOR_SET_RANK" %>
<%@ Register src="WUC_DateReporter.ascx" tagname="WUC_DateReporter" tagprefix="uc1" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<%@ Register src="PageNavigation.ascx" tagname="PageNavigation" tagprefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<asp:ToolkitScriptManager ID="toolkit1" runat="server"></asp:ToolkitScriptManager>
<asp:UpdatePanel ID="UpdatePanel1" runat="server">				           
<ContentTemplate>

<div class="container-fluid">
				<!-- BEGIN PAGE HEADER-->
				<div class="row-fluid" id="fixedPageHeader">
					<div class="span12"> 
						<!-- BEGIN PAGE TITLE & BREADCRUMB-->
						<h3 class="page-user-md">สถานะการกำหนดเกณฑ์สรุปผลการประเมิน (KPI+Competency)<font color="blue">(ScreenID : R-ALL-03)</font></h3>					
									
						<ul class="breadcrumb">
                            <li>
                            	<i class="icon-copy"></i> <a href="javascript:;">รายงาน</a><i class="icon-angle-right"></i>
                            </li>
                            <li>
                            	<i class="icon-copy"></i> <a href="javascript:;">การประเมินผลรวมตัวชี้วัดและสมรรถนะ</a><i class="icon-angle-right"></i>
                            </li>
                        	<li>
                        	    <i class="icon-check"></i> <a href="javascript:;">สถานะการกำหนดเกณฑ์สรุปผลการประเมิน</a>
                        	</li>      
                            <uc1:WUC_DateReporter ID="WUC_DateReporter1" runat="server" />
                        </ul>
						<!-- END PAGE TITLE & BREADCRUMB-->
									        
				     </div>

				
			    </div>
		        <!-- END PAGE HEADER-->
				
				<asp:Panel ID="pnlList" runat="server" Visible="True" DefaultButton="btnSearch">
                        <div class="row-fluid">        					
					        <div class="span12">
						        <!-- BEGIN SAMPLE TABLE PORTLET-->
                                        <div class="row-fluid form-horizontal">
                			                  <div class="btn-group pull-left">
                                                    <asp:Button ID="btnSearch" OnClick="btnSearch_Click"  runat="server" CssClass="btn green" Text="ค้นหา" />
                                              </div>                                        
                                        </div>        								                           
							            <div class="row-fluid form-horizontal">

                                          
					                             <div class="span8" style="font-size:14px;">
													<div class="control-group">
												        <label class="control-label"> ปี</label>
												        <div class="controls">
													        <asp:DropDownList ID="ddlYear" runat="server" CssClass="medium m-wrap">
						                                    </asp:DropDownList>
						                                    &nbsp;&nbsp;
						                                    <asp:CheckBox ID="chk" runat="server" Text=""/>&nbsp;แสดงเฉพาะผู้ประเมินที่ยังไม่ได้กำหนดเกณฑ์
													    </div>
											        </div>
												</div>   
										</div>
							           	<div class="row-fluid form-horizontal">
											    <div class="span12">
											        <div class="control-group">
											                <label class="control-label">ระดับ</label>
											                <div class="controls">											                   
											                    <label class="checkbox">
											                        <asp:CheckBox ID="chkClass8" runat="server" Text=""/>&nbsp;8
											                    </label>
											                    <label class="checkbox">
											                        <asp:CheckBox ID="chkClass9" runat="server" Text=""/>&nbsp;9
											                    </label>
											                    <label class="checkbox">
											                        <asp:CheckBox ID="chkClass10" runat="server" Text=""/>&nbsp;10
											                    </label>
											                    <label class="checkbox">
											                        <asp:CheckBox ID="chkClass11" runat="server" Text=""/>&nbsp;11
											                    </label>
											                    <label class="checkbox">
											                        <asp:CheckBox ID="chkClass12" runat="server" Text=""/>&nbsp;12
											                    </label>
											                    <label class="checkbox">
											                        <asp:CheckBox ID="chkClass13" runat="server" Text=""/>&nbsp;13
											                    </label>
										                   </div>
											        </div>
											    </div>
											</div>

												           								        
						            <div class="portlet-body no-more-tables">
						            <asp:Label ID="lblCountList" runat="server" Font-Size="14px" Font-Bold="true"></asp:Label>
								        <table class="table table-full-width dataTable no-more-tables table-hover">
									        <thead>
										        <tr>
											        <th><i class="icon-briefcase"></i> ตำแหน่งผู้ประเมิน</th>
											        <th><i class="icon-bookmark"></i> ระดับ</th>
											        <th><i class="icon-sitemap"></i> หน่วยงาน</th>
											        <th><i class="icon-user-md"></i> ผู้ครองตำแหน่ง</th>
											        <th><i class="icon-group"></i> สถานะการกำหนดเกณฑ์</th>
											        <th></th>
										        </tr>
									        </thead>
									        <tbody>
										        <asp:Repeater ID="rptList" OnItemDataBound="rptList_ItemDataBound" runat="server"> 
										            <ItemTemplate>
										                    <tr>
											                    <td data-title="ตำแหน่งผู้ประเมิน">
											                        <asp:Label ID="lblPos" runat="server"></asp:Label>
											                    </td>											    
											                    <td data-title="ระดับ">
											                        <asp:Label ID="lblClass" runat="server"></asp:Label>
						                                        </td>
											                    <td data-title="หน่วยงาน">
											                        <asp:Label ID="lblDept" runat="server"></asp:Label>
						                                        </td>
											                    <td data-title="ผู้ครองตำแหน่ง">
											                        <asp:Label ID="lblName" runat="server"></asp:Label>
						                                        </td>
											                    <td data-title="สถานะการกำหนดเกณฑ์">
									                                <asp:Label ID="lblRank" runat="server"></asp:Label>
						                                        </td>
										                    </tr>	
										            </ItemTemplate>
										        </asp:Repeater>        															
									        </tbody>
								        </table>
								        
								        <asp:PageNavigation ID="Pager" OnPageChanging="Pager_PageChanging" MaximunPageCount="20" PageSize="20" runat="server" />
							        </div>
        						<!-- END SAMPLE TABLE PORTLET-->       						
					        </div>
                        </div>
               </asp:Panel>
               
              <asp:Panel ID="pnlEdit" runat="server" Visible="False">
				    <div class="portlet-body form">
							<h4 class="form-section"><i class="icon-retweet"></i> 							
							    กำหนดผู้ประเมินรักษาการแทนสำหรับ <asp:Label ID="lblEditPosNo" runat="server"></asp:Label> : <asp:Label ID="lblEditPosName" runat="server"></asp:Label> <asp:Label ID="lblEditDept" runat="server"></asp:Label> 
							    <asp:Label ID="lblEditRound" runat="server"></asp:Label>
							</h4>
														
								<!-- BEGIN FORM-->
								
							    			    
								<div class="form-horizontal form-view">									
									<h4 class="form-section"><i class="icon-user-md"></i> ผู้ประเมินหลัก</h4>
									
									<div class="row-fluid">													
										<div class="span6 ">
											<div class="control-group">
												<label class="control-label">ตำแหน่ง:</label>
												<div class="controls">
													<asp:Label ID="lblMainPos" runat="server" CssClass="text bold"></asp:Label>
												</div>
											</div>
										</div>
										<!--/span-->
										<div class="span6 ">
											<div class="control-group">
												<label class="control-label">ระดับ:</label>
												<div class="controls">
													<asp:Label ID="lblMainClass" runat="server" CssClass="text bold"></asp:Label>
												</div>
											</div>
										</div>
										<!--/span-->
									</div>
									<!--/row-->
									<div class="row-fluid">
									    <div class="span6 ">
											<div class="control-group">
												<label class="control-label">หน่วยงาน:</label>
												<div class="controls">
													<asp:Label ID="lblMainDept" runat="server" CssClass="text bold"></asp:Label>
												</div>
											</div>
										</div>
										<!--/span-->
										<div class="span6 ">
										    <div class="control-group">
												<label class="control-label">ผู้ครองตำแหน่ง(ปัจจุบัน):</label>
												<div class="controls" style="width:auto;">
													<asp:Label ID="lblMainName" runat="server" CssClass="text bold" ForeColor="#cccccc"></asp:Label>
												</div>
											</div>
										</div>										
									</div>
									
									<!--/row-->        
									           
									<h4 class="form-section"><i class="icon-check"></i> <asp:Label ID="lblHeaderAssigned" runat="server" Text="ผู้ได้รับมอบหมายให้ประเมินเพิ่มเติม"></asp:Label></h4>
									<div class="row-fluid" id="pnlAssignedList" runat="server">
										<div class="span12 ">
											<div class="control-group">
												<label class="control-label">&nbsp;</label>
												
												    <div class="controls">
												    <div class="span10">
													    <div class="portlet-body no-more-tables">
					                                        <table class="table  table-hover">
						                                        <thead>
							                                        <tr>
								                                        <th>#</th>
								                                        <th><i class="icon-briefcase"></i> ตำแหน่ง</th>
								                                        <th><i class="icon-bookmark"></i> ระดับ</th>
								                                        <th><i class="icon-sitemap"></i> หน่วยงาน</th>
								                                        <th><i class="icon-user"></i> ผู้ครองตำแหน่ง (ปัจจุบัน)</th>
								                                        <th><i class="icon-bolt"></i> ดำเนินการ</th>
							                                        </tr>
						                                        </thead>
						                                        <tbody>
							                                        <asp:Repeater ID="rptAssigned" runat="server">
							                                            <ItemTemplate>
							                                                    <tr>
								                                                    <td data-title="#"><asp:Label ID="lblNo" runat="server"></asp:Label></td>
								                                                    <td data-title="ตำแหน่ง"><asp:Label ID="lblPSNPos" runat="server"></asp:Label></td>
								                                                    <td data-title="ระดับ"><asp:Label ID="lblPSNClass" runat="server"></asp:Label></td>
								                                                    <td data-title="หน่วยงาน"><asp:Label ID="lblPSNDept" runat="server"></asp:Label></td>
								                                                    <td data-title="ผู้ครองตำแหน่ง(ปัจจุบัน)"><asp:Label ID="lblPSNNow" runat="server" ForeColor="#cccccc"></asp:Label></td>
								                                                    <td data-title="ดำเนินการ">
								                                                        <asp:Button CssClass="btn red mini" Text="ยกเลิก" ID="btnPSNDelete" runat="server" CommandName="Delete"/>
								                                                        <asp:ConfirmButtonExtender TargetControlID="btnPSNDelete" ID="cfm_PSN_Delete" runat="server"></asp:ConfirmButtonExtender>
								                                                    </td>
							                                                    </tr>
							                                            </ItemTemplate>
							                                        </asp:Repeater>							                                        									                    
						                                        </tbody>
					                                        </table>
				                                        </div>
												    </div>
												</div>
											</div>
										</div>
									</div>
									
									<div class="form-actions">
									    <asp:Button CssClass="btn" runat="server" ID="btnCancel" Text="ย้อนกลับ" />
										<asp:Button runat="server" ID="btnAdd" CssClass="btn purple" Text="เพิ่มผู้ประเมิน" />
									</div>
								</div>
								
							
                <asp:Panel ID="pnlAdd" DefaultButton="btn_Search_Add_List" runat="server" Visible="False">
                    <div style="z-index: 1049;" class="modal-backdrop fade in"></div>
                    <div style="z-index: 1050; max-width:90%; min-width:70%; left:30%; max-height:95%; top:5%;" class="modal">
                        
				            <div class="modal-header">
	                            <h3><i class="icon-search"></i> เพิ่มผู้ประเมิน โดยตำแหน่ง</h3>
                            </div>
                            <div class="modal-body" style="max-height:85%;">
	                                  	<div >
											<div class="row-fluid">
																			
												    <div class="row-fluid">
												           					
												            <div class="span4 ">
													            <div class="control-group">
													                <label class="control-label"><i class="icon-sitemap"></i> ฝ่าย</label>
													                <div class="controls">
														                 <asp:DropDownList ID="ddl_Search_Sector" runat="server" AutoPostBack="true" CssClass="medium m-wrap">
														                </asp:DropDownList>
														            </div>
												                </div>
													        </div>
													         <div class="span4 ">
														        <div class="control-group">
													                <label class="control-label"><i class="icon-sitemap"></i> หน่วยงาน/ตำแหน่ง</label>
													                <div class="controls">
														                <asp:TextBox ID="txt_Search_Organize" runat="server" AutoPostBack="true" CssClass="m-wrap medium" placeholder="ค้นหาจากหน่วยงาน/ตำแหน่ง"></asp:TextBox>
														            </div>
												                </div>
													        </div>
													    
													       <div class="span4 ">
												                <div class="control-group">
													                <label class="control-label"><i class="icon-user"></i> ผู้ครองตำแหน่ง(ปัจจุบัน)</label>
													                <div class="controls">
														                <asp:TextBox ID="txt_Search_Name" runat="server" AutoPostBack="true" CssClass="m-wrap medium" placeholder="ค้นหาจากชื่อ/เลขประจำตัว"></asp:TextBox>
														                <asp:Button ID="btn_Search_Add_List" runat="server" Text="" style="display:none;" />
													                </div>
												                </div>														        
													        </div>			
													        
												    </div>
												    	
												     <asp:Label ID="lblCountPSN" runat="server" Font-Size="14px" Font-Bold="true"></asp:Label>
								                    <table class="table table-full-width table-advance dataTable no-more-tables table-hover">
									                    <thead>
										                    <tr>
											                    <th><i class="icon-sitemap"></i> หน่วยงาน</th>
											                    <th><i class="icon-briefcase"></i> ตำแหน่ง</th>
											                    <th><i class="icon-bookmark"></i> ระดับ</th>
											                    <th><i class="icon-user"></i>ผู้ครองตำแหน่ง(ปัจจุบัน)</th>
											                    <th><i class="icon-bolt"></i> เลือก</th>
										                    </tr>
									                    </thead>
									                    <tbody>
									                        <asp:Repeater ID="rptPSN" runat="server">
									                            <ItemTemplate>
    									                            <tr>                                        
											                            <td data-title="หน่วยงาน"><asp:Label ID="lblPSNDept" runat="server"></asp:Label></td>
											                            <td data-title="ตำแหน่ง"><asp:Label ID="lblPSNPos" runat="server"></asp:Label></td>
											                            <td data-title="ระดับ"><asp:Label ID="lblPSNClass" runat="server"></asp:Label></td>
											                            <td data-title="ผู้ครองตำแหน่ง(ปัจจุบัน)"><asp:Label ID="lblPSNName" runat="server"></asp:Label></td>
											                            <td data-title="เลือก">
											                                <asp:Button ID="btnSelect" runat="server" CssClass="btn mini blue" CommandName="Select" Text="เลือก" />
											                                <asp:ConfirmButtonExtender ID="cfmbtnSelect" TargetControlID="btnSelect" runat="server" ></asp:ConfirmButtonExtender>
										                                </td>
										                            </tr>	
									                            </ItemTemplate>
									                        </asp:Repeater>
									                    </tbody>
								                    </table>
                    								
								                    <asp:PageNavigation ID="PagerPSN" MaximunPageCount="10" PageSize="7" runat="server" />
											  
											</div>
										</div>
                            </div>                            
                            
                            <asp:LinkButton ID="btnClosePNLAdd" runat="server" CssClass="fancybox-item fancybox-close" ToolTip="Close" />
                     
                    </div>
               </asp:Panel>
							
				</div>
					    
		</asp:Panel>

							 
</ContentTemplate>				                
</asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ScriptPlaceHolder" Runat="Server">
</asp:Content>


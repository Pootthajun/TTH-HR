using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Xml.Linq;
using System.Diagnostics;
using System.Collections.Specialized;
using System.Configuration;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Caching;
using System.Web.SessionState;
using System.Web.Security;
using System.Web.Profile;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
namespace VB
{

	public partial class AssessmentSetting_Committee : System.Web.UI.Page
	{

		HRBL BL = new HRBL();
		GenericLib GL = new GenericLib();

		textControlLib TC = new textControlLib();
		public int R_Year {
			get {
				try {
					return GL.CINT( GL.SplitString(ddlRound.Items[ddlRound.SelectedIndex].Value, "-")[0]);
				} catch (Exception ex) {
					return 0;
				}
			}
		}

		public int R_Round {
			get {
				try {
					return GL.CINT(GL.SplitString(ddlRound.Items[ddlRound.SelectedIndex].Value, "-")[1]);
				} catch (Exception ex) {
					return 0;
				}
			}
		}

		public string DEPT_CODE {
			get {
				try {
					return lblEditDept.Attributes["DEPT_CODE"];
				} catch (Exception ex) {
					return "";
				}
			}
			set { lblEditDept.Attributes["DEPT_CODE"] = value; }
		}

		public string MINOR_CODE {
			get {
				try {
					return lblEditDept.Attributes["MINOR_CODE"];
				} catch (Exception ex) {
					return "";
				}
			}
			set { lblEditDept.Attributes["MINOR_CODE"] = value; }
		}

		public string DefaultManager {
			get { return lblMainCode.Attributes["DefaultManager"]; }
			set { lblMainCode.Attributes["DefaultManager"] = value; }
		}

		public DataTable Department {
			get { return (DataTable)Session["AssessmentSetting_Committee_DEPT"]; }
			set { Session["AssessmentSetting_Committee_DEPT"] = value; }
		}

		public DataTable Manager {
			get { return (DataTable)Session["AssessmentSetting_Committee_MGR"]; }
			set { Session["AssessmentSetting_Committee_MGR"] = value; }
		}

		protected void Page_Load(object sender, System.EventArgs e)
		{
			if ((Session["USER_PSNL_NO"] == null)) {
				ScriptManager.RegisterStartupScript(this.Page, typeof(string), "Alert", "showAlert('กรุณาลงชื่อเข้าใช้ระบบ');", true);
				Response.Redirect("Login.aspx");
				return;
			}

			if (!IsPostBack) {
				BindSearchForm();
				BindList();
				pnlList.Visible = true;
				pnlEdit.Visible = false;
				pnlAdd.Style["visibility"] = "hidden";

			}
		}

		private void BindSearchForm()
		{
			BL.BindDDlYearRound(ddlRound);
			txtFilter.Text = "";
		}

		protected void btnSearch_Click(object sender, System.EventArgs e)
		{
			BindList();
		}


		private void BindList()
		{
			string SQL = "";

			SQL = " SELECT DISTINCT SECTOR_CODE,\n";
			SQL += " DEPT_CODE,MINOR_CODE,SECTOR_NAME,DEPT_NAME,\n";
			SQL += " ASSESSOR_CODE Manager_Code,\n";
			SQL += " ASS_TYPE Manager_Type,\n";
			SQL += " ASSESSOR_NAME Manager_Name,\n";
			SQL += " MGR_NAME MGR_NAME,\n";
			SQL += " Origin_DEPT_CODE, Origin_MINOR_CODE, Origin_DEPT_NAME\n";

			SQL += " FROM  vw_HR_ASSESSOR\n";
			SQL += " WHERE R_Year =" + R_Year + " And R_Round =" + R_Round + "\n";
			SQL += " ORDER BY SECTOR_CODE,DEPT_CODE";


			SqlDataAdapter DA = new SqlDataAdapter(SQL, BL.ConnectionString());
			DataTable DT = new DataTable();
			DA.Fill(DT);

			string Filter = "";
			if (!string.IsNullOrEmpty(txtFilter.Text)) {
				Filter += " (SECTOR_NAME LIKE '%" + txtFilter.Text.Replace("'", "''") + "%' OR ";
				Filter += " DEPT_NAME LIKE '%" + txtFilter.Text.Replace("'", "''") + "%' OR ";
				Filter += " Manager_Name LIKE '%" + txtFilter.Text.Replace("'", "''") + "%' OR ";
				Filter += " Manager_Code LIKE '%" + txtFilter.Text.Replace("'", "''") + "%') AND ";
			}
			if (chkBlank.Checked) {
				string BlankList = "";
				for (int i = 0; i <= DT.Rows.Count - 1; i++) {
					if (GL.CINT (DT.Compute("COUNT(Manager_Code)", "DEPT_CODE='" + DT.Rows[i]["DEPT_CODE"] + "' AND MINOR_CODE='" + DT.Rows[i]["MINOR_CODE"] + "' AND Manager_Code IS NOT NULL")) == 0) {
						BlankList += "'" + DT.Rows[i]["DEPT_CODE"] + DT.Rows[i]["MINOR_CODE"] + "',";
					}
				}
				if (!string.IsNullOrEmpty(BlankList)) {
					Filter += "DM IN (" + BlankList.Substring(0, BlankList.Length - 1) + ") AND ";
				} else {
					Filter += "1=0 AND ";
				}
			}

			string[] Col = {
				"DEPT_CODE",
				"DEPT_NAME",
				"MINOR_CODE",
				"SECTOR_CODE",
				"SECTOR_NAME"
			};

			if (!string.IsNullOrEmpty(Filter)) {
				DT.Columns.Add("DM", typeof(string), "DEPT_CODE+MINOR_CODE");
				DataTable DEPT = DT.Copy();
				DEPT.DefaultView.RowFilter = Filter.Substring(0, Filter.Length - 4) + "\n";
				DEPT = DEPT.DefaultView.ToTable(true, Col);
				Filter = "";
				for (int i = 0; i <= DEPT.Rows.Count - 1; i++) {
					Filter += "'" + DEPT.Rows[i]["DEPT_CODE"] + DEPT.Rows[i]["MINOR_CODE"] + "',";
				}
				if (!string.IsNullOrEmpty(Filter)) {
					DT.DefaultView.RowFilter = "DM IN (" + Filter.Substring(0, Filter.Length - 1) + ")";
				} else {
					DT.DefaultView.RowFilter = "1=0";
				}
				DT = DT.DefaultView.ToTable().Copy();
				DT.DefaultView.RowFilter = "";
				DT.Columns.Remove("DM");
			}

			Department = DT.DefaultView.ToTable(true, Col).Copy();
			Manager = DT;
			Pager.SesssionSourceName = "AssessmentSetting_Committee_DEPT";
			Pager.RenderLayout();

			if (Department.Rows.Count == 0) {
				lblCountList.Text = "ไม่พบรายการดังกล่าว";
			} else {
				lblCountList.Text = "พบ " + GL.StringFormatNumber(DT.Rows.Count, 0) + " รายการ";
			}

		}

		protected void Pager_PageChanging(PageNavigation Sender)
		{
			Pager.TheRepeater = rptList;
		}

		//---------- Datasource = Department ---------

		string LastSector = "";
		protected void rptList_ItemCommand(object source, System.Web.UI.WebControls.RepeaterCommandEventArgs e)
		{
			switch (e.CommandName) {
				case "Edit":

					LinkButton btnEdit =(LinkButton) e.Item.FindControl("btnEdit");
					DEPT_CODE = btnEdit.Attributes["DEPT_CODE"];
					//----------- Key -------------
					MINOR_CODE = btnEdit.Attributes["MINOR_CODE"];
					BindDepartment();

					break;
			}
		}


		private void BindDepartment()
		{
			//------------- Bind Header ----------------------
			string SQL = "SELECT DISTINCT SECTOR_CODE,\n";
			SQL += " \tDEPT_CODE,MINOR_CODE,SECTOR_NAME,DEPT_NAME,\n";
			SQL += " \tASSESSOR_CODE Manager_Code,\n";
			SQL += " \tASS_TYPE Manager_Type,\n";
			SQL += " \tASSESSOR_NAME Manager_Name,\n";
			SQL += " \tMGR_NAME MGR_NAME,\n";
			SQL += " \tOrigin_DEPT_CODE,Origin_MINOR_CODE,Origin_DEPT_NAME\n";

			SQL += " FROM vw_HR_ASSESSOR WHERE R_Year=" + R_Year + " AND R_Round=" + R_Round + "\n";
			SQL += " AND DEPT_CODE='" + DEPT_CODE.Replace("'", "''") + "' AND MINOR_CODE='" + MINOR_CODE.Replace("'", "''") + "'\n";

			SqlDataAdapter DA = new SqlDataAdapter(SQL, BL.ConnectionString());
			DataTable DT = new DataTable();
			DA.Fill(DT);
			if (DT.Rows.Count == 0) {
				ScriptManager.RegisterStartupScript(this.Page, typeof(string), "Alert", "showAlert('กำลังปรับปรุงข้อมูล');", true);
				BindList();
				return;
			}

			ClearEditForm();
			//------------------------ Collect DefaultManager Code-----------------
			DefaultManager = "";
			DT.DefaultView.RowFilter = "Manager_Type IN (0,1) AND Manager_Code IS NOT NULL";
			for (int i = 0; i <= DT.DefaultView.Count - 1; i++) {
				DefaultManager += DT.DefaultView[i]["Manager_Code"] + ",";
			}
			if (!string.IsNullOrEmpty(DefaultManager))
				DefaultManager = DefaultManager.Substring(0, DefaultManager.Length - 1);

			lblEditRound.Text = ddlRound.Items[ddlRound.SelectedIndex].Text;
			lblEditDept.Text = DT.Rows[0]["DEPT_NAME"].ToString ();
			//------------- Bind Default Manager ------------- Display Only First Manager---------
			if (DT.DefaultView.Count > 0) {
				string Manager_Code = DT.DefaultView[0]["Manager_Code"].ToString ();
				SQL = "SELECT PSNL_Fullname,PNPS_CLASS,MGR_NAME\n";
				SQL += " FROM vw_PN_PSNL\n";
				SQL += " WHERE PSNL_NO='" + Manager_Code.Replace("'", "''") + "'\n";
				DA = new SqlDataAdapter(SQL, BL.ConnectionString());
				DataTable PT = new DataTable();
				DA.Fill(PT);
				if (PT.Rows.Count > 0) {
					lblMainName.Text = PT.Rows[0]["PSNL_Fullname"].ToString ();
					lblMainCode.Text = Manager_Code;
					lblMainPos.Text = PT.Rows[0]["MGR_NAME"].ToString();
					lblMainClass.Text = GL.CINT (PT.Rows[0]["PNPS_CLASS"]).ToString ();
				}

				if (DT.DefaultView.Count == 1) {
					lblHeaderDefault.Text = "ผู้ประเมินหลัก";
				} else {
					lblHeaderDefault.Text = "ผู้ประเมินหลัก " + DT.DefaultView.Count + " คน หนึ่งในนั้นคือ ...";
				}
			} else {
				lblHeaderDefault.Text = "ไม่มีผู้ประเมินหลัก";
			}
			DT.DefaultView.RowFilter = "Manager_Type=2 AND Manager_Code IS NOT NULL";
			DT = DT.DefaultView.ToTable().Copy();
			DT.DefaultView.RowFilter = "";

			if (DT.Rows.Count == 0) {
				lblHeaderAssigned.Text = "ไม่มีผู้ได้รับมอบหมายเพิ่มเติม";
				pnlAssignedList.Visible = false;
			} else {
				lblHeaderAssigned.Text = "ผู้ได้รับมอบหมายเพิ่มเติม " + DT.Rows.Count + " คน";
				pnlAssignedList.Visible = true;
				//------------- Bind Assigned List ---------------
				rptAssigned.DataSource = DT;
				rptAssigned.DataBind();
			}

			pnlEdit.Visible = true;
			pnlList.Visible = false;
			divView.Style["visibility"] = "visible";
			pnlAdd.Style["visibility"] = "hidden";
		}

		protected void rptList_ItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
		{
			if (e.Item.ItemType != ListItemType.Item & e.Item.ItemType != ListItemType.AlternatingItem)
				return;

			Label lblSector =(Label) e.Item.FindControl("lblSector");
			Label lblDept =(Label) e.Item.FindControl("lblDept");
			Label lblDefault =(Label) e.Item.FindControl("lblDefault");
			Label lblAssigned =(Label) e.Item.FindControl("lblAssigned");
			LinkButton btnEdit =(LinkButton) e.Item.FindControl("btnEdit");

            DataRowView drv = (DataRowView)e.Item.DataItem;
			if (LastSector != drv["SECTOR_NAME"].ToString()) {
				lblSector.Text = drv["SECTOR_NAME"].ToString();
				LastSector = drv["SECTOR_NAME"].ToString();
			}
			lblDept.Text = drv["DEPT_NAME"].ToString();

			//----------------- Check Duplicated UI Department Name ------
			Department.DefaultView.RowFilter = "DEPT_CODE='" + drv["DEPT_CODE"].ToString () + "' AND DEPT_NAME='" + drv["DEPT_NAME"].ToString().Replace("'", "''") + "'";
			if (Department.DefaultView.Count > 1) {
                lblDept.Text += " (" + drv["MINOR_CODE"].ToString() + ")";
			}
			Department.DefaultView.RowFilter = "";

			DataTable DT = Manager.Copy();
			//---------- Classified Manager Type Default--------------
            string Filter_Default = "DEPT_CODE='" + drv["DEPT_CODE"].ToString() + "' AND MINOR_CODE='" + drv["MINOR_CODE"].ToString() + "' AND Manager_Type IN (0,1) AND Manager_Code IS NOT NULL AND Manager_Name IS NOT NULL";
			DT.DefaultView.RowFilter = Filter_Default;
			if (DT.DefaultView.Count == 0) {
				lblDefault.Text = "-";
			} else {
				for (int i = 0; i <= DT.DefaultView.Count - 1; i++) {
                    lblDefault.Text += "<i class='icon-user-md'></i> " + DT.DefaultView[i]["Manager_Code"].ToString() + " : " + DT.DefaultView[i]["Manager_Name"].ToString() + "<br>";
				}
				lblDefault.Text = lblDefault.Text.Substring(0, lblDefault.Text.Length - 4);
			}
			//---------- Classified Manager Type Assigned--------------
            string Filter_Assigned = "DEPT_CODE='" + drv["DEPT_CODE"] + "' AND MINOR_CODE='" + drv["MINOR_CODE"].ToString() + "' AND Manager_Type=2 AND Manager_Code IS NOT NULL AND Manager_Name IS NOT NULL";
			DT.DefaultView.RowFilter = Filter_Assigned;
			if (DT.DefaultView.Count == 0) {
				lblAssigned.Text = "-";
			} else {
				for (int i = 0; i <= DT.DefaultView.Count - 1; i++) {
                    lblAssigned.Text += "<i class='icon-check' style='color:red;'></i> <font style='color:red;'>" + DT.DefaultView[i]["Manager_Code"].ToString() + " : " + DT.DefaultView[i]["Manager_Name"].ToString() + "</font><br>";
				}
				lblAssigned.Text = lblAssigned.Text.Substring(0, lblAssigned.Text.Length - 1);
			}

			btnEdit.Attributes["DEPT_CODE"] = drv["DEPT_CODE"].ToString ();
			//----------- Key -------------
			btnEdit.Attributes["MINOR_CODE"] = drv["MINOR_CODE"].ToString ();
			//----------- Key -------------
		}

		private void ClearEditForm()
		{
			lblEditDept.Text = "";
			lblEditRound.Text = "";

			lblMainName.Text = "-";
			lblMainCode.Text = "-";
			lblMainPos.Text = "-";
			lblMainClass.Text = "-";

			rptAssigned.DataSource = null;
			rptAssigned.DataBind();
		}

		protected void rptAssigned_ItemCommand(object source, System.Web.UI.WebControls.RepeaterCommandEventArgs e)
		{
			switch (e.CommandName) {
				case "Delete":
					string Manager_Code = e.CommandArgument.ToString ();
					string SQL = "DELETE FROM tb_HR_Committee ";
					SQL += " WHERE PNDP_DEPT_CODE ='" + DEPT_CODE.Replace("'", "''") + "' AND PNDP_MINOR_CODE='" + MINOR_CODE.Replace("'", "''") + "' AND R_Year=" + R_Year + " AND R_Round=" + R_Round + " AND Manager_Code='" + Manager_Code.Replace("'", "''") + "'\n";

					SqlConnection Conn = new SqlConnection(BL.ConnectionString());
					Conn.Open();
					SqlCommand Comm = new SqlCommand();
					var _with1 = Comm;
					_with1.Connection = Conn;
					_with1.CommandType = CommandType.Text;
					_with1.CommandText = SQL;
					_with1.ExecuteNonQuery();
					_with1.Dispose();
					Conn.Close();
					Conn.Dispose();

					BindDepartment();
					break;
			}
		}

		protected void rptAssigned_ItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
		{
			if (e.Item.ItemType != ListItemType.Item & e.Item.ItemType != ListItemType.AlternatingItem)
				return;

			Label lblNo =(Label) e.Item.FindControl("lblNo");
			Label lblCode =(Label) e.Item.FindControl("lblCode");
			Label lblName =(Label) e.Item.FindControl("lblName");
			Label lblPos =(Label) e.Item.FindControl("lblPos");
			Label lblDept =(Label) e.Item.FindControl("lblDept");
			Button btnDelete =(Button) e.Item.FindControl("btnDelete");
			AjaxControlToolkit.ConfirmButtonExtender cfm_Delete =(AjaxControlToolkit.ConfirmButtonExtender) e.Item.FindControl("cfm_Delete");

            DataRowView drv = (DataRowView)e.Item.DataItem;
			lblNo.Text = GL.CINT ( e.Item.ItemIndex+ 1).ToString () ;
			lblCode.Text = drv["Manager_Code"].ToString ();
			lblName.Text = drv["Manager_Name"].ToString ();
			lblPos.Text = drv["MGR_NAME"].ToString ();
			//& " (" & drv["FN_Name"] & ")"
			lblDept.Text = drv["DEPT_NAME"].ToString ();

			btnDelete.CommandArgument = drv["Manager_Code"].ToString ();;
			cfm_Delete.ConfirmText = "ยืนยันยกเลิก " + drv["Manager_Name"].ToString ();
		}

		protected void btnAdd_Click(object sender, System.EventArgs e)
		{
			BL.BindDDlSector(ddl_Search_Sector, DEPT_CODE.Substring(0, 2));
			txt_Search_Name.Text = "";
			txt_Search_Organize.Text = "";
			BindAddPanel();

			//divView.Style("visibility") = "visible"
			pnlAdd.Style["visibility"] = "visible";
		}

		private string[] CurrentManager()
		{
			string[] Spliter = { "," };
			string[] Result = DefaultManager.Split(Spliter, System.StringSplitOptions.RemoveEmptyEntries);
			foreach (RepeaterItem Item in rptAssigned.Items) {
				if (Item.ItemType != ListItemType.Item & Item.ItemType != ListItemType.AlternatingItem)
					continue;
				Button btnDelete =(Button) Item.FindControl("btnDelete");
				GL.PushArray_String(Result, btnDelete.CommandArgument);
			}
			return Result;
		}

		private void BindAddPanel()
		{
			string SQL = " SELECT PSN.PSNL_NO,PSN.PSNL_Fullname,DEPT_NAME,ISNULL(MGR_NAME,FN_NAME) MGR_NAME,PNPS_CLASS,DEPT_CODE,MINOR_CODE\n";
			SQL += " FROM vw_PN_PSNL PSN \n";
			SQL += " WHERE PSNL_STAT<>'03' \n";


			if (ddl_Search_Sector.SelectedIndex > 0) {
				SQL += " AND PSN.SECTOR_CODE = '" + ddl_Search_Sector.Items[ddl_Search_Sector.SelectedIndex].Value + "' \n";
			}
			if (!string.IsNullOrEmpty(txt_Search_Organize.Text)) {
				SQL += " AND (DEPT_CODE LIKE '%" + txt_Search_Organize.Text.Replace("'", "''") + "%' OR SECTOR_NAME LIKE '%" + txt_Search_Organize.Text.Replace("'", "''") + "%' OR DEPT_NAME LIKE '%" + txt_Search_Organize.Text.Replace("'", "''") + "%' OR FN_NAME LIKE '%" + txt_Search_Organize.Text.Replace("'", "''") + "%' OR MGR_NAME LIKE '%" + txt_Search_Organize.Text.Replace("'", "''") + "%' ) \n";
			}
			if (!string.IsNullOrEmpty(txt_Search_Name.Text)) {
				SQL += " AND (PSN.PSNL_Fullname LIKE '%" + txt_Search_Name.Text.Replace("'", "''") + "%' OR PSN.PSNL_NO LIKE '%" + txt_Search_Name.Text.Replace("'", "''") + "%' ) \n";
			}

			string[] MGR = CurrentManager();
			string Filter = "";
			if (MGR.Length > 0) {
				for (int i = 0; i <= MGR.Length - 1; i++) {
					Filter += "'" + MGR[i] + "',";
				}
				SQL += " AND PSN.PSNL_NO NOT IN (" + Filter.Substring(0, Filter.Length - 1) + ")\n";
			}

			SQL += " ORDER BY SECTOR_CODE,DEPT_CODE,PNPS_CLASS DESC,PSN.POS_NO\n";
			SqlDataAdapter DA = new SqlDataAdapter(SQL, BL.ConnectionString());
			DataTable DT = new DataTable();
			DA.Fill(DT);

			Session["AssessmentSetting_Committee_MGR"] = DT;
			PagerPSN.SesssionSourceName = "AssessmentSetting_Committee_MGR";
			PagerPSN.RenderLayout();
			lblCountPSN.Text = "พบ " + GL.StringFormatNumber(DT.Rows.Count, 0) + " รายการ";
		}

		protected void PagerPSN_PageChanging(PageNavigation Sender)
		{
			PagerPSN.TheRepeater = rptPSN;
		}

		protected void Search_Changed(object sender, System.EventArgs e)
		{
			BindAddPanel();
		}

		protected void btnCancel_Click(object sender, System.EventArgs e)
		{
			BindList();
			pnlEdit.Visible = false;
			pnlList.Visible = true;
		}

		//Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
		//    divAdd.Visible = False
		//    divView.Visible = True
		//End Sub


		protected void rptPSN_ItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
		{
			if (e.Item.ItemType != ListItemType.Item & e.Item.ItemType != ListItemType.AlternatingItem)
				return;

			Label lnkPSNOrganize =(Label) e.Item.FindControl("lnkPSNOrganize");
			Label lblPSNNo = (Label)e.Item.FindControl("lblPSNNo");
			Label lblPSNName = (Label)e.Item.FindControl("lblPSNName");
			Label lblPSNMGR =(Label) e.Item.FindControl("lblPSNMGR");
			 Label lblPSNClass = (Label)e.Item.FindControl("lblPSNClass");
			Button btnSelect =(Button) e.Item.FindControl("btnSelect");
			AjaxControlToolkit.ConfirmButtonExtender cfmbtnSelect =(AjaxControlToolkit.ConfirmButtonExtender) e.Item.FindControl("cfmbtnSelect");

            DataRowView drv = (DataRowView)e.Item.DataItem;
			if (Convert.ToString(drv["DEPT_NAME"]) != LastOrgranizeName) {
				lnkPSNOrganize.Text = drv["DEPT_NAME"].ToString ();
				LastOrgranizeName = drv["DEPT_NAME"].ToString ();
			}
			lnkPSNOrganize.Attributes["DEPT_CODE"] = drv["DEPT_CODE"].ToString ();
			lnkPSNOrganize.Attributes["MINOR_CODE"] = drv["MINOR_CODE"].ToString ();
			lnkPSNOrganize.Attributes["DEPT_NAME"] = drv["DEPT_NAME"].ToString ();


			lblPSNNo.Text = drv["PSNL_NO"].ToString ();
			lblPSNName.Text = drv["PSNL_Fullname"].ToString ();
			lblPSNMGR.Text = drv["MGR_NAME"].ToString ();
			lblPSNClass.Text = GL.CINT (drv["PNPS_CLASS"]).ToString ();
			btnSelect.CommandArgument = drv["PSNL_NO"].ToString ();

			cfmbtnSelect.ConfirmText = "ยืนยันแต่งตั้ง " + drv["PSNL_Fullname"];
		}

		string LastOrgranizeName = "";
		protected void rptPSN_ItemCommand(object source, System.Web.UI.WebControls.RepeaterCommandEventArgs e)
		{
			switch (e.CommandName) {
				case "Select":
					Button btnSelect =(Button) e.Item.FindControl("btnSelect");
					string Manager_Code = btnSelect.CommandArgument;
					Label lnkPSNOrganize =(Label) e.Item.FindControl("lnkPSNOrganize");

					string SQL = "SELECT * FROM tb_HR_Committee \n";
					SQL += " WHERE PNDP_DEPT_CODE ='" + DEPT_CODE.Replace("'", "''") + "' AND PNDP_MINOR_CODE='" + MINOR_CODE.Replace("'", "''") + "' AND R_Year=" + R_Year + " AND R_Round=" + R_Round + " AND Manager_Code='" + Manager_Code.Replace("'", "''") + "'\n";

					SqlDataAdapter DA = new SqlDataAdapter(SQL, BL.ConnectionString());
					DataTable DT = new DataTable();
					DA.Fill(DT);

					DataRow DR = null;
					if (DT.Rows.Count == 0) {
						DR = DT.NewRow();
						DR["PNDP_DEPT_CODE"] = DEPT_CODE;
						DR["PNDP_MINOR_CODE"] = MINOR_CODE;
						DR["R_Year"] = R_Year;
						DR["R_Round"] = R_Round;
						DR["Manager_Code"] = Manager_Code;
						DR["Origin_DEPT_CODE"] = lnkPSNOrganize.Attributes["DEPT_CODE"];
						DR["Origin_MINOR_CODE"] = lnkPSNOrganize.Attributes["MINOR_CODE"];
						DR["Origin_DEPT_NAME"] = lnkPSNOrganize.Attributes["DEPT_NAME"];
					} else {
						DR = DT.Rows[0];
					}
					DR["Update_By"] = Session["USER_PSNL_NO"];
					DR["Update_Time"] = DateAndTime.Now;

					if (DT.Rows.Count == 0)
						DT.Rows.Add(DR);
					SqlCommandBuilder cmd = new SqlCommandBuilder(DA);
					DA.Update(DT);

					BindDepartment();
					pnlAdd.Style["visibility"] = "hidden";

					break;
			}
		}

		protected void btnClosePNLAdd_Click(object sender, System.EventArgs e)
		{
			pnlAdd.Style["visibility"] = "hidden";
		}
		public AssessmentSetting_Committee()
		{
			Load += Page_Load;
		}
	}
}

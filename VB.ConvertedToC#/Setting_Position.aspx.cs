using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Xml.Linq;
using System.Diagnostics;
using System.Collections.Specialized;
using System.Configuration;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Caching;
using System.Web.SessionState;
using System.Web.Security;
using System.Web.Profile;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using SyncData;
namespace VB
{

	public partial class Setting_Position : System.Web.UI.Page
	{


		HRBL BL = new HRBL();
		GenericLib GL = new GenericLib();

		textControlLib TC = new textControlLib();
		protected void Page_Load(object sender, System.EventArgs e)
		{
			if ((Session["USER_PSNL_NO"] == null)) {
				ScriptManager.RegisterStartupScript(this.Page, typeof(string), "Alert", "showAlert('กรุณาลงชื่อเข้าใช้ระบบ');", true);
				Response.Redirect("Login.aspx");
				return;
			}

			if (!IsPostBack) {
				BindPosition();
			}
		}

		protected void Search_Changed(object sender, System.EventArgs e)
		{
			BindPosition();
		}


		private void BindPosition()
		{
			string SQL = "";
            SQL += " SELECT DISTINCT   DEPT.SECTOR_CODE,DEPT.DEPT_CODE,DEPT.SECTOR_NAME,DEPT.DEPT_NAME,POS.POS_NO,ISNULL(POS.MGR_NAME,'-') MGR_NAME,ISNULL(POS.FLD_NAME,'-') FLD_NAME," + "\n";
			SQL += "  POS.PNPO_CLASS,PSN.PSNL_NO,PSN.PSNL_Fullname,POS.Update_Time " + "\n";
			SQL += "  FROM vw_PN_Position POS " + "\n";
			SQL += "  INNER JOIN vw_HR_Department DEPT ON POS.DEPT_CODE=DEPT.DEPT_CODE AND POS.MINOR_CODE=DEPT.MINOR_CODE" + "\n";
            SQL += "  LEFT JOIN vw_PN_PSNL PSN ON POS.POS_NO=PSN.POS_NO" + "  AND  POS.PSNL_NO=PSN.PSNL_NO    \n";

			SQL += "  WHERE POS.PNPO_CLASS IS NOT NULL" + "\n";
			if (!string.IsNullOrEmpty(txtFilter.Text)) {
				SQL += " AND (POS.FLD_NAME LIKE '%" + txtFilter.Text.Replace("'", "''") + "%' OR " + "\n";
				SQL += " POS.POS_NO LIKE '%" + txtFilter.Text.Replace("'", "''") + "%' OR " + "\n";
				SQL += " POS.MGR_NAME LIKE '%" + txtFilter.Text.Replace("'", "''") + "%' OR " + "\n";
				SQL += " POS.SECTOR_NAME LIKE '%" + txtFilter.Text.Replace("'", "''") + "%' OR " + "\n";
				SQL += " POS.DEPT_NAME LIKE '%" + txtFilter.Text.Replace("'", "''") + "%' OR " + "\n";
				SQL += " PSN.PSNL_NO LIKE '%" + txtFilter.Text.Replace("'", "''") + "%' OR " + "\n";
				SQL += " PSN.PSNL_Fullname LIKE '%" + txtFilter.Text.Replace("'", "''") + "%')" + "\n";
			}
			SQL += "  ORDER BY DEPT.SECTOR_CODE,DEPT.DEPT_CODE,POS.PNPO_CLASS DESC,POS.POS_NO" + "\n";


			SqlDataAdapter DA = new SqlDataAdapter(SQL, BL.ConnectionString());
			DataTable DT = new DataTable();
			DA.Fill(DT);

			Session["Setting_Position"] = DT;
			Pager.SesssionSourceName = "Setting_Position";
			Pager.RenderLayout();

			if (DT.Rows.Count == 0) {
				lblCountList.Text = "ไม่พบรายการดังกล่าว";
			} else {
				lblCountList.Text = "พบ " + GL.StringFormatNumber(DT.Rows.Count.ToString(), 0) + " รายการ";
			}

		}

		protected void Pager_PageChanging(PageNavigation Sender)
		{
			Pager.TheRepeater = rptList;
		}


		string LastSector = "";
		string LastDept = "";
		protected void rptList_ItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
		{
			if (e.Item.ItemType != ListItemType.Item & e.Item.ItemType != ListItemType.AlternatingItem)
				return;

            Label lblClass = (Label)e.Item.FindControl("lblClass");
            Label lblPosNo = (Label)e.Item.FindControl("lblPosNo");
            Label lblFLD = (Label)e.Item.FindControl("lblFLD");
            Label lblMGR = (Label)e.Item.FindControl("lblMGR");
            Label lblSector = (Label)e.Item.FindControl("lblSector");
            Label lblDept = (Label)e.Item.FindControl("lblDept");
            Label lblPSN = (Label)e.Item.FindControl("lblPSN");
            Label lblUpdate = (Label)e.Item.FindControl("lblUpdate");
            
            DataRowView drv = (DataRowView)e.Item.DataItem;

			if (LastSector != drv["SECTOR_NAME"].ToString()) {
				lblSector.Text = drv["SECTOR_NAME"].ToString();
				LastSector = drv["SECTOR_NAME"].ToString();
				lblDept.Text = drv["DEPT_NAME"].ToString();
				LastDept = drv["DEPT_NAME"].ToString();
			} else if (LastDept != drv["DEPT_NAME"].ToString()) {
				lblDept.Text = drv["DEPT_NAME"].ToString();
				LastDept = drv["DEPT_NAME"].ToString();
			}

			lblClass.Text = GL.CINT (drv["PNPO_CLASS"]).ToString ();

			lblPosNo.Text = drv["POS_NO"].ToString();
			lblFLD.Text = drv["FLD_NAME"].ToString();
			lblMGR.Text = drv["MGR_NAME"].ToString();
			if (!GL.IsEqualNull(drv["PSNL_Fullname"])) {
				lblPSN.Text = drv["PSNL_NO"] + " : " + drv["PSNL_Fullname"];
			} else {
				lblPSN.Text = "-";
			}

			if (!GL.IsEqualNull(drv["Update_Time"])) {
                lblUpdate.Text = BL.DateTimeToThaiDateTime((DateTime)drv["Update_Time"]);
			} else {
				lblUpdate.Text = "-";
			}
		}
		public Setting_Position()
		{
			Load += Page_Load;
		}



	}
}

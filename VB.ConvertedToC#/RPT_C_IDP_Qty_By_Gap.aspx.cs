﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using System.Web.UI.HtmlControls;

namespace VB
{
    public partial class RPT_C_IDP_Qty_By_Gap : System.Web.UI.Page
    {
         HRBL BL = new HRBL();
        GenericLib GL = new GenericLib();

        public int R_Year
        {
            get
            {
                try
                {
                    return GL.CINT(GL.SplitString(ddlRound.Items[ddlRound.SelectedIndex].Value, "-")[0]);
                }
                catch (Exception ex)
                {
                    return 0;
                }
            }
        }

        public int R_Round
        {
            get
            {
                try
                {
                    return GL.CINT(GL.SplitString(ddlRound.Items[ddlRound.SelectedIndex].Value, "-")[1]);
                }
                catch (Exception ex)
                {
                    return 0;
                }
            }
        }

        protected void Page_Load(object sender, System.EventArgs e)
        {
            if ((Session["USER_PSNL_NO"] == null))
            {
                ScriptManager.RegisterStartupScript(this.Page, typeof(string), "Alert", "showAlert('กรุณาลงชื่อเข้าใช้ระบบ');", true);
                Response.Redirect("Login.aspx");
                return;
            }

            if (!IsPostBack)
            {
                BL.BindDDlYearRound(ddlRound);
                ddlRound.Items.RemoveAt(0);
                BL.BindDDlPSNType(ddlPSNType);
                BL.BindDDlFLDCode(ddlFN);
                //BL.BindDDlCOMPClassGroup(ddlClassGroup, R_Year, R_Round);

                

                BL.BindDDlClassAll(ddlClassGroupFrom);
                BL.BindDDlClassAll(ddlClassGroupTo);
                //DataTable LT = BL.PSNClassLevel();

                BindSummary();

              
            }
            
        }

        private void BindSummary()
        {

            if (!chkCOMP1.Checked & !chkCOMP2.Checked & !chkCOMP3.Checked)
            {
                rptIDP.DataSource = null;
                rptIDP.DataBind();
                return;
            }
            string StartTitle = " ปี " + R_Year + " รอบ " + R_Round;
            string Title = "";

            string Filter = "R_Year=" + R_Year + " AND R_Round=" + R_Round + " AND Selected=1 AND \n";
            if (txtDept.Text != "")
            {
                Filter += " (DEPT_CODE LIKE '%" + txtDept.Text.Replace("'", "''") + "%' OR DEPT_NAME LIKE '%" + txtDept.Text.Replace("'", "''") + "%')  AND \n";
            }
            if (ddlPSNType.SelectedIndex > 0)
            {
                Filter += " WAGE_TYPE='" + ddlPSNType.SelectedValue.Replace("'","''") + "' AND \n";
                Title += " พนักงาน" + ddlPSNType.SelectedItem;
            }
            if (ddlClassGroupFrom.SelectedIndex > 0)
            {
                Filter += "PNPS_CLASS>='" + ddlClassGroupFrom.SelectedValue.PadLeft(2, '0') + "' AND \n";
            }
            if ( ddlClassGroupTo.SelectedIndex > 0)
            {
                Filter += "PNPS_CLASS<='" + ddlClassGroupTo.SelectedValue.PadLeft(2, '0') + "' AND \n";
            }
            if ((ddlClassGroupFrom.SelectedIndex > 0) && (ddlClassGroupTo.SelectedIndex > 0))
            {
                Title += " ระดับ " + ddlClassGroupFrom.Items[ddlClassGroupFrom.SelectedIndex].Text + " ถึง " + ddlClassGroupTo.Items[ddlClassGroupTo.SelectedIndex].Text;
            }

            if (ddlFN.SelectedIndex > 0)
            {
                Filter += " FN_ID='" + ddlFN.SelectedValue.Replace("'","''") + "' AND \n";
            }
            if (txtCOMP_COMP.Text != "")
            {
                Filter += " COMP_Comp LIKE '%" + txtCOMP_COMP.Text.Replace("'", "''") + "%' AND \n";
            }

            if ((chkGAP0.Checked & chkGAP1.Checked & chkGAP2.Checked & chkGAP3.Checked & chkGAP4.Checked) | (!chkGAP0.Checked & !chkGAP1.Checked & !chkGAP2.Checked & !chkGAP3.Checked & !chkGAP4.Checked))
            {
                //-----------Do Nothing--------------
            }
            else
            {
                string sel = "";
                string t = "";
                if (chkGAP0.Checked)
                {
                    sel += "5,";
                    t += "0,";
                }
                if (chkGAP1.Checked)
                {
                    sel += "4,";
                    t += "-1,";
                }
                if (chkGAP2.Checked)
                {
                    sel += "3,";
                    t += "-2,";
                }
                if (chkGAP3.Checked)
                {
                    sel += "2,";
                    t += "-3,";
                }
                if (chkGAP4.Checked)
                {
                    sel += "1,";
                    t += "-4,";
                }
                Filter += " Final_Score IN (" + sel.Substring(0, sel.Length - 1) + ") AND \n";
                if (chkGAP1.Checked & chkGAP2.Checked & chkGAP3.Checked & chkGAP4.Checked)
                {
                    Title += " ที่มีช่องว่างสมรรถนะ";
                }
                else if (chkGAP0.Checked & !chkGAP1.Checked & !chkGAP2.Checked & !chkGAP3.Checked & !chkGAP4.Checked)
                {
                    Title += " ที่ไม่มีช่องว่างสมรรถนะ";
                }
                else
                {
                    Title += " ที่มีช่องว่าง " + t.Substring(0, t.Length - 1);
                }    

            }

            string template="SELECT \n";
                    template += " COMP_Type_Id,Master_No,COMP_Comp\n";
                    template += " ,SECTOR_CODE,SECTOR_NAME\n";
                    template += " ,DEPT_CODE,DEPT_NAME \n";
                    template += " ,PNPS_CLASS\n";
                    template += " ,COUNT(PSNL_NO) Total\n";
                    template += " FROM /--FromClause--/ \n";
                    template += " /--WhereClause--/ \n";
                    template += " GROUP BY \n";
                    template += " COMP_Type_Id,Master_No,COMP_Comp\n";
                    template += " ,SECTOR_CODE,SECTOR_NAME\n";
                    template += " ,DEPT_CODE,DEPT_NAME \n";
                    template += " ,PNPS_CLASS\n";

            string SQL = "SELECT * FROM \n(\n";
            if (Filter != "") Filter = " WHERE " + Filter.Substring(0, Filter.Length - 5) + "\n";
            String TitleCOMP = " ";
            if (chkCOMP1.Checked)
            {
                SQL += template.Replace("/--FromClause--/", "vw_COMP_CORE_GAP").Replace("/--WhereClause--/", Filter) + "\nUNION ALL\n";
                TitleCOMP += "สมรรถนะหลัก(Core),";
            }
            if (chkCOMP3.Checked)
            {
                SQL += template.Replace("/--FromClause--/", "vw_COMP_MGR_GAP").Replace("/--WhereClause--/", Filter) + "\nUNION ALL\n";
                TitleCOMP += "สมรรถนะตามสายระดับ/การจัดการทั่วไป(Managerial),";
            }
            if (chkCOMP2.Checked)
            {
                SQL += template.Replace("/--FromClause--/", "vw_COMP_FN_GAP").Replace("/--WhereClause--/", Filter) + "\nUNION ALL\n";
                TitleCOMP += "สมรรถนะตามสายงาน(Functional),";
            }


            Title += TitleCOMP.Substring(0, TitleCOMP.Length - 1);

            SQL = SQL.Substring(0, SQL.Length - ("\nUNION ALL").Length) + ") IDP\n";
            SQL += " ORDER BY COMP_Type_Id,Master_No,SECTOR_CODE,DEPT_CODE,PNPS_CLASS DESC";

            SqlDataAdapter DA = new SqlDataAdapter(SQL, BL.ConnectionString());
            DA.SelectCommand.CommandTimeout = 400;
            DataTable DT = new DataTable();
            DA.Fill(DT);
            rptIDP.DataSource = DT;
            rptIDP.DataBind();

            if (!string.IsNullOrEmpty(Title))
            {
                Title = StartTitle + " " + Title;
            }
            else
            {
                Title = StartTitle;
            }

            lblTitle.Text = Title;
            Session["RPT_C_IDP_Qty_By_Gap"] = DT;
            //--------- Title --------------
            Session["RPT_C_IDP_Qty_By_Gap_Title"] = Title;
            //--------- Sub Title ----------
            Session["RPT_C_IDP_Qty_By_Gap_SubTitle"] = "ปี " + R_Year + " รอบ " + R_Round;
            Session["RPT_C_IDP_Qty_By_Gap_Round"]=R_Round;
            Session["RPT_C_IDP_Qty_By_Gap_Year"]=R_Year;

            if (DT.Rows.Count == 0)
            {
                lblTitle.Text += "\n" + "<br>ไม่พบรายการดังกล่าว";
            }
            else
            {
                lblTitle.Text += "\n" + "<br>พบ " + GL.StringFormatNumber(DT.Rows.Count, 0) + " รายการ";

            }


        }

        string LastCOMP = "";
        //string LastCOMPTypeID = "";
        string LastCOMPTypeName = "";
        //int TotalCOMPType = 0;
        string LastDept = "";
        string LastSector = "";
        int TotalSector = 0;
        int TotalInComp = 0;
        //int totalGap = 0;
        protected void rptIDP_ItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
        {
            switch (e.Item.ItemType)
            { 
                case ListItemType.AlternatingItem:
                case ListItemType.Item:
                    Label lblComp = (Label)e.Item.FindControl("lblComp");
                    Label lblDept = (Label)e.Item.FindControl("lblDept");
                    Label lblClass = (Label)e.Item.FindControl("lblClass");
                    Label lblTotal = (Label)e.Item.FindControl("lblTotal");
                                                            
                    DataRowView drv = (DataRowView)e.Item.DataItem;

                    //if(LastCOMPTypeID != drv["COMP_Type_Id"].ToString())
                    //{
                    //    LastCOMPTypeID=drv["COMP_Type_Id"].ToString();
                    //    TotalCOMPType=0;
                        //switch(drv["COMP_Type_Id"].ToString())
                        //{
                        //    case "1":LastCOMPTypeName="สมรรถนะหลัก";
                        //        break;
                        //    case "2":LastCOMPTypeName="สมรรถนะตามสายงาน";
                        //        break;
                        //    case "3":LastCOMPTypeName="สมรรถนะตามสายระดับ";
                        //        break;
                        //}
                    //}
                    //TotalCOMPType += (int)drv["Total"];

                    if (LastCOMP != drv["COMP_Comp"].ToString())
                    {
                        LastCOMP = drv["COMP_Comp"].ToString();                        
                        lblComp.Text = LastCOMP;
                        // Reset All 
                        LastDept = "";
                        LastSector = "";
                        TotalSector = 0;
                        TotalInComp = 0;

                        switch (drv["COMP_Type_Id"].ToString())
                        {
                            case "1": LastCOMPTypeName = "สมรรถนะหลัก";
                                break;
                            case "2": LastCOMPTypeName = "สมรรถนะตามสายงาน";
                                break;
                            case "3": LastCOMPTypeName = "สมรรถนะตามสายระดับ";
                                break;
                        }
                    }
                    TotalInComp += (int)drv["Total"];

                    if (LastDept != drv["DEPT_NAME"].ToString())
                    {
                        LastDept = drv["DEPT_NAME"].ToString();
                        lblDept.Text = LastDept;
                    }                    
                    lblClass.Text= GL.CINT(drv["PNPS_CLASS"]).ToString();
                    lblTotal.Text = drv["Total"].ToString();
                   
                    // Count คนทั้งหมดใน Sector
                    if (LastSector != drv["SECTOR_NAME"].ToString())
                    {
                        LastSector = drv["SECTOR_NAME"].ToString();
                        TotalSector = 0;
                    }
                    TotalSector += (int)drv["Total"];

                    DataTable dt = (DataTable)rptIDP.DataSource;

                    // สรุปยอดรวมใน Sector
                    HtmlTableRow trFooter = (HtmlTableRow)e.Item.FindControl("trFooter");
                    Label lblSector = (Label)e.Item.FindControl("lblSector");
                    Label lblSumSector = (Label)e.Item.FindControl("lblSumSector");
                    if (e.Item.ItemIndex == dt.Rows.Count - 1)
                    {
                        lblSector.Text = "รวม" + LastSector;
                        lblSumSector.Text = TotalSector.ToString();
                    }
                    else if (dt.Rows[e.Item.ItemIndex + 1]["SECTOR_NAME"].ToString() != LastSector)
                    {
                        lblSector.Text = "รวม" + LastSector;
                        lblSumSector.Text = TotalSector.ToString();
                    }
                    else
                    {
                        trFooter.Visible = false;
                    }

                    //  รวมตามสมรรถนะ
                    HtmlTableRow trCompSum = (HtmlTableRow)e.Item.FindControl("trCompSum");
                    Label lblCompFooter = (Label)e.Item.FindControl("lblCompFooter");
                    Label lblCompSum = (Label)e.Item.FindControl("lblCompSum");
                    if (e.Item.ItemIndex == dt.Rows.Count-1)
                    {
                        lblCompFooter.Text = "รวม" + LastCOMPTypeName + " " + LastCOMP;
                        lblCompSum.Text = TotalInComp.ToString();
                    }
                    else if (dt.Rows[e.Item.ItemIndex + 1]["COMP_Comp"].ToString() != LastCOMP)
                    {
                        lblCompFooter.Text = "รวม" + LastCOMPTypeName + " " + LastCOMP;
                        lblCompSum.Text = TotalInComp.ToString();
                    }
                    else
                    {
                        trCompSum.Visible = false;
                    }
                    break;

                case ListItemType.Footer:

                    //HtmlTableRow trSum = (HtmlTableRow)e.Item.FindControl("trSum");
                    //Label lblSum = (Label)e.Item.FindControl("lblSum");

                    //lblSum.Text = "รวม " + GL.StringFormatNumber(totalGap, 0) + " หลักสูตร";
                    break;
            }            
        }

        protected void btnSearch_Click(object sender, System.EventArgs e)
		{
            BindSummary();
		}

        public RPT_C_IDP_Qty_By_Gap()
		{
			Load += Page_Load;
		}
    }
}
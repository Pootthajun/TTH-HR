﻿<%@ Page  Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPage.Master" CodeBehind="AssessmentSetting_Assessor_Assigned.aspx.cs" Inherits="VB.AssessmentSetting_Assessor_Assigned" %>

<%@ Register src="WUC_DateReporter.ascx" tagname="WUC_DateReporter" tagprefix="uc1" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<%@ Register src="PageNavigation.ascx" tagname="PageNavigation" tagprefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<asp:ToolkitScriptManager ID="toolkit1" runat="server"></asp:ToolkitScriptManager>

<asp:UpdatePanel ID="UpdatePanel1" runat="server">				           
<ContentTemplate>

<div class="container-fluid">
				<!-- BEGIN PAGE HEADER-->
				<div class="row-fluid" id="fixedPageHeader">
					<div class="span12"> 
						<!-- BEGIN PAGE TITLE & BREADCRUMB-->
						<h3 class="page-user-md">กำหนดผู้ประเมินรักษาการแทน <font color="blue">(ScreenID : S-HDR-11)</font></h3>					
									
						<ul class="breadcrumb">
                            <li>
                            	<i class="icon-th-list"></i><a href="javascript:;">การกำหนดข้อมูลการประเมิน</a><i class="icon-angle-right"></i>
                            </li>
                        	<li><i class="icon-user-md"></i> <a href="javascript:;">กำหนดผู้ประเมินรักษาการแทน</a></li>                        	
                            <uc1:WUC_DateReporter ID="WUC_DateReporter1" runat="server" />
                        </ul>
						<!-- END PAGE TITLE & BREADCRUMB-->
									        
				     </div>

				
			    </div>
		        <!-- END PAGE HEADER-->
				
				<asp:Panel ID="pnlList" runat="server" Visible="True">
                        <div class="row-fluid">        					
					        <div class="span12">
						        <!-- BEGIN SAMPLE TABLE PORTLET-->
                                        <div class="btn-group pull-right">                                    
								            <button data-toggle="dropdown" class="btn dropdown-toggle">พิมพ์ <i class="icon-angle-down"></i></button>										
								            <%--<ul class="dropdown-menu pull-right">
                                               
                                                <li><a href="Print/RPT_A_Assessor_Assigned.aspx?Mode=PDF" target="_blank">รูปแบบ PDF</a></li>
                                                <li><a href="Print/RPT_A_Assessor_Assigned.aspx?Mode=EXCEL" target="_blank">รูปแบบ Excel</a></li>	
                                                											
								            </ul>--%>
                                            <ul class="dropdown-menu pull-right">                                                       
                                                <li><a id="btnFormPDF" runat="server" target="_blank">ใบประเมิน รูปแบบ PDF</a></li>
                                                <li><a id="btnFormExcel" runat="server" target="_blank">ใบประเมิน รูปแบบ Excel</a></li>									
			                                </ul>
							            </div>        								                           
							            <h4 class="control-label"> <i class="icon-retweet"></i> สำหรับรอบการประเมิน        							    
								            <asp:DropDownList ID="ddlRound" OnSelectedIndexChanged="btnSearch_Click" runat="server" AutoPostBack="true" CssClass="medium m-wrap">
									        </asp:DropDownList>
						                    &nbsp; &nbsp;
						                    ค้นหา
						                    <asp:TextBox  ID="txtFilter" OnTextChanged="btnSearch_Click" runat="server" AutoPostBack="true" CssClass="m-wrap large" style=" background-color:White; margin-top:5px;margin-left:10px;" placeholder="หน่วยงาน/ผู้ประเมิน/ผู้ที่ได้รับมอบหมายให้ประเมิน"></asp:TextBox>
						                    &nbsp; &nbsp;
						                   <asp:CheckBox ID="chkBlank" OnCheckedChanged="btnSearch_Click" runat="server" Text="" AutoPostBack="true" /> แสดงเฉพาะที่ยังไม่มีผู้ประเมิน
						                   <asp:Button ID="btnSearch" OnClick="btnSearch_Click" runat="server" Style="display:none" />
							           </h4>
							           
												           								        
						            <div class="portlet-body no-more-tables">
						            <asp:Label ID="lblCountList" runat="server" Font-Size="14px" Font-Bold="true"></asp:Label>
								        <table class="table table-full-width dataTable no-more-tables table-hover">
									        <thead>
										        <tr>
											        <th><i class="icon-briefcase"></i> ตำแหน่งผู้ประเมิน</th>											        
											        <th style="text-align:center;"><i class="icon-bookmark"></i> ระดับ</th>
											        <th><i class="icon-sitemap"></i> หน่วยงาน</th>
											        <th><i class="icon-user-md"></i> ผู้ครองตำแหน่ง</th>
											        <th style="text-align:center;"><i class="icon-group"></i> ต้องประเมิน(คน)</th>
											        <th><i class="icon-user"></i> ผู้รักษาการแทน</th>
											        <th style="text-align:center;"><i class="icon-bolt"></i> ดำเนินการ</th>
											        <th></th>
										        </tr>
									        </thead>
									        <tbody>
										        <asp:Repeater ID="rptList" OnItemCommand="rptList_ItemCommand" OnItemDataBound="rptList_ItemDataBound" runat="server"> 
										            <ItemTemplate>
										                    <tr>
											                    <td data-title="ตำแหน่งผู้ประเมิน"><asp:Label ID="lblPos" runat="server"></asp:Label></td>											                    
											                    <td data-title="ระดับ" style="text-align:center;"><asp:Label ID="lblClass" runat="server"></asp:Label></td>
											                    <td data-title="หน่วยงาน"><asp:Label ID="lblDept" runat="server"></asp:Label></td>
											                    <td data-title="ผู้ครองตำแหน่ง"><asp:Label ID="lblDefault" runat="server"></asp:Label></td>
											                    <td data-title="ต้องประเมิน(คน)" style="text-align:center;"><asp:Label ID="lblPSN" runat="server"></asp:Label></td>
											                    <td data-title="ผู้รักษาการแทน"><asp:Label ID="lblAssigned" runat="server"></asp:Label></td>
											                    <td data-title="ดำเนินการ" style="text-align:center;">
											                        <asp:LinkButton ID="btnEdit" runat="server" CssClass="btn mini blue" CommandName="Edit"><i class="icon-bolt"></i> ผู้รักษาการแทน</asp:LinkButton></td>
										                    </tr>	
										            </ItemTemplate>
										        </asp:Repeater>        															
									        </tbody>
								        </table>
								        
								        <asp:PageNavigation ID="Pager" OnPageChanging="Pager_PageChanging" MaximunPageCount="20" PageSize="20" runat="server" />
							        </div>
        						<!-- END SAMPLE TABLE PORTLET-->       						
					        </div>
                        </div>
               </asp:Panel>
               
              <asp:Panel ID="pnlEdit" runat="server" Visible="False">
				    <div class="portlet-body form">
							<h4 class="form-section"><i class="icon-retweet"></i> 							
							    กำหนดผู้ประเมินรักษาการแทนสำหรับ <asp:Label ID="lblEditPosNo" runat="server"></asp:Label> : <asp:Label ID="lblEditPosName" runat="server"></asp:Label> <asp:Label ID="lblEditDept" runat="server"></asp:Label> 
							    <asp:Label ID="lblEditRound" runat="server"></asp:Label>
							</h4>
														
								<!-- BEGIN FORM-->
								
							    			    
								<div class="form-horizontal form-view">									
									<h4 class="form-section"><i class="icon-user-md"></i> ผู้ประเมินหลัก</h4>
									
									<div class="row-fluid">													
										<div class="span6 ">
											<div class="control-group">
												<label class="control-label">ตำแหน่ง:</label>
												<div class="controls">
													<asp:Label ID="lblMainPos" runat="server" CssClass="text bold"></asp:Label>
												</div>
											</div>
										</div>
										<!--/span-->
										<div class="span6 ">
											<div class="control-group">
												<label class="control-label">ระดับ:</label>
												<div class="controls">
													<asp:Label ID="lblMainClass" runat="server" CssClass="text bold"></asp:Label>
												</div>
											</div>
										</div>
										<!--/span-->
									</div>
									<!--/row-->
									<div class="row-fluid">
									    <div class="span6 ">
											<div class="control-group">
												<label class="control-label">หน่วยงาน:</label>
												<div class="controls">
													<asp:Label ID="lblMainDept" runat="server" CssClass="text bold"></asp:Label>
												</div>
											</div>
										</div>
										<!--/span-->
										<div class="span6 ">
										    <div class="control-group">
												<label class="control-label">ผู้ครองตำแหน่ง(ปัจจุบัน):</label>
												<div class="controls" style="width:auto;">
													<asp:Label ID="lblMainName" runat="server" CssClass="text bold" ForeColor="#cccccc"></asp:Label>
												</div>
											</div>
										</div>										
									</div>
									
									<!--/row-->        
									           
									<h4 class="form-section"><i class="icon-check"></i> <asp:Label ID="lblHeaderAssigned" runat="server" Text="ผู้ได้รับมอบหมายให้ประเมินเพิ่มเติม"></asp:Label></h4>
									<div class="row-fluid" id="pnlAssignedList" runat="server">
										<div class="span12 ">
											<div class="control-group">
												<label class="control-label">&nbsp;</label>
												
												    <div class="controls">
												    <div class="span10">
													    <div class="portlet-body no-more-tables">
					                                        <table class="table  table-hover">
						                                        <thead>
							                                        <tr>
								                                        <th>#</th>
								                                        <th><i class="icon-briefcase"></i> ตำแหน่ง</th>
								                                        <th><i class="icon-bookmark"></i> ระดับ</th>
								                                        <th><i class="icon-sitemap"></i> หน่วยงาน</th>
								                                        <th><i class="icon-user"></i> ผู้ครองตำแหน่ง (ปัจจุบัน)</th>
								                                        <th><i class="icon-bolt"></i> ดำเนินการ</th>
							                                        </tr>
						                                        </thead>
						                                        <tbody>
							                                        <asp:Repeater ID="rptAssigned" OnItemCommand="rptAssigned_ItemCommand" OnItemDataBound="rptAssigned_ItemDataBound" runat="server">
							                                            <ItemTemplate>
							                                                    <tr>
								                                                    <td data-title="#"><asp:Label ID="lblNo" runat="server"></asp:Label></td>
								                                                    <td data-title="ตำแหน่ง"><asp:Label ID="lblPSNPos" runat="server"></asp:Label></td>
								                                                    <td data-title="ระดับ"><asp:Label ID="lblPSNClass" runat="server"></asp:Label></td>
								                                                    <td data-title="หน่วยงาน"><asp:Label ID="lblPSNDept" runat="server"></asp:Label></td>
								                                                    <td data-title="ผู้ครองตำแหน่ง(ปัจจุบัน)"><asp:Label ID="lblPSNNow" runat="server" ForeColor="#cccccc"></asp:Label></td>
								                                                    <td data-title="ดำเนินการ">
								                                                        <asp:Button CssClass="btn red mini" Text="ยกเลิก" ID="btnPSNDelete" runat="server" CommandName="Delete"/>
								                                                        <asp:ConfirmButtonExtender TargetControlID="btnPSNDelete" ID="cfm_PSN_Delete" runat="server"></asp:ConfirmButtonExtender>
								                                                    </td>
							                                                    </tr>
							                                            </ItemTemplate>
							                                        </asp:Repeater>							                                        									                    
						                                        </tbody>
					                                        </table>
				                                        </div>
												    </div>
												</div>
											</div>
										</div>
									</div>
									
									<div class="form-actions">
									    <asp:Button ID="btnCancel" OnClick="btnCancel_Click" CssClass="btn" runat="server" Text="ย้อนกลับ" />
										<asp:Button ID="btnAdd" OnClick="btnAdd_Click" runat="server" CssClass="btn purple" Text="เพิ่มผู้ประเมิน" />
									</div>
								</div>
								
							
                <asp:Panel ID="pnlAdd" DefaultButton="btn_Search_Add_List" runat="server" Visible="False">
                    <div style="z-index: 1049;" class="modal-backdrop fade in"></div>
                    <div style="z-index: 1050; max-width:90%; min-width:70%; left:30%; max-height:95%; top:5%;" class="modal">
                        
				            <div class="modal-header">
	                            <h3><i class="icon-search"></i> เพิ่มผู้ประเมิน โดยตำแหน่ง</h3>
                            </div>
                            <div class="modal-body" style="max-height:85%;">
	                                  	<div >
											<div class="row-fluid">
																			
												    <div class="row-fluid">
												           					
												            <div class="span4 ">
													            <div class="control-group">
													                <label class="control-label"><i class="icon-sitemap"></i> ฝ่าย</label>
													                <div class="controls">
														                 <asp:DropDownList ID="ddl_Search_Sector" OnSelectedIndexChanged="Search_Changed" runat="server" AutoPostBack="true" CssClass="medium m-wrap">
														                </asp:DropDownList>
														            </div>
												                </div>
													        </div>
													         <div class="span4 ">
														        <div class="control-group">
													                <label class="control-label"><i class="icon-sitemap"></i> หน่วยงาน/ตำแหน่ง</label>
													                <div class="controls">
														                <asp:TextBox ID="txt_Search_Organize" OnTextChanged="Search_Changed" runat="server" AutoPostBack="true" CssClass="m-wrap medium" placeholder="ค้นหาจากหน่วยงาน/ตำแหน่ง"></asp:TextBox>
														            </div>
												                </div>
													        </div>
													    
													       <div class="span4 ">
												                <div class="control-group">
													                <label class="control-label"><i class="icon-user"></i> ผู้ครองตำแหน่ง(ปัจจุบัน)</label>
													                <div class="controls">
														                <asp:TextBox ID="txt_Search_Name" OnTextChanged="Search_Changed" runat="server" AutoPostBack="true" CssClass="m-wrap medium" placeholder="ค้นหาจากชื่อ/เลขประจำตัว"></asp:TextBox>
														                <asp:Button ID="btn_Search_Add_List"  OnClick="Search_Changed" runat="server" Text="" style="display:none;" />
													                </div>
												                </div>														        
													        </div>			
													        
												    </div>
												    	
												     <asp:Label ID="lblCountPSN" runat="server" Font-Size="14px" Font-Bold="true"></asp:Label>
								                    <table class="table table-full-width table-advance dataTable no-more-tables table-hover">
									                    <thead>
										                    <tr>
											                    <th><i class="icon-sitemap"></i> หน่วยงาน</th>
											                    <th><i class="icon-briefcase"></i> ตำแหน่ง</th>
											                    <th><i class="icon-bookmark"></i> ระดับ</th>
											                    <th><i class="icon-user"></i>ผู้ครองตำแหน่ง(ปัจจุบัน)</th>
											                    <th><i class="icon-bolt"></i> เลือก</th>
										                    </tr>
									                    </thead>
									                    <tbody>
									                        <asp:Repeater ID="rptPSN" OnItemCommand="rptPSN_ItemCommand" OnItemDataBound="rptPSN_ItemDataBound" runat="server">
									                            <ItemTemplate>
    									                            <tr>                                        
											                            <td data-title="หน่วยงาน"><asp:Label ID="lblPSNDept" runat="server"></asp:Label></td>
											                            <td data-title="ตำแหน่ง"><asp:Label ID="lblPSNPos" runat="server"></asp:Label></td>
											                            <td data-title="ระดับ"><asp:Label ID="lblPSNClass" runat="server"></asp:Label></td>
											                            <td data-title="ผู้ครองตำแหน่ง(ปัจจุบัน)"><asp:Label ID="lblPSNName" runat="server"></asp:Label></td>
											                            <td data-title="เลือก">
											                                <asp:Button ID="btnSelect" runat="server" CssClass="btn mini blue" CommandName="Select" Text="เลือก" />
											                                <asp:ConfirmButtonExtender ID="cfmbtnSelect" TargetControlID="btnSelect" runat="server" ></asp:ConfirmButtonExtender>
										                                </td>
										                            </tr>	
									                            </ItemTemplate>
									                        </asp:Repeater>
									                    </tbody>
								                    </table>
                    								
								                    <asp:PageNavigation ID="PagerPSN" OnPageChanging="PagerPSN_PageChanging" MaximunPageCount="10" PageSize="7" runat="server" />
											  
											</div>
										</div>
                            </div>                            
                            
                            <asp:LinkButton ID="btnClosePNLAdd" OnClick="btnClosePNLAdd_Click" runat="server" CssClass="fancybox-item fancybox-close" ToolTip="Close" />
                     
                    </div>
               </asp:Panel>
							
				</div>
					    
		</asp:Panel>

							 
</ContentTemplate>				                
</asp:UpdatePanel>		 

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ScriptPlaceHolder" Runat="Server">
</asp:Content>
using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Xml.Linq;
using System.Diagnostics;
using System.Collections.Specialized;
using System.Configuration;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Caching;
using System.Web.SessionState;
using System.Web.Security;
using System.Web.Profile;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using System.Data.SqlClient;
namespace VB
{

	public partial class Assessment_self_COMP_Assessment : System.Web.UI.Page
	{

		HRBL BL = new HRBL();
		GenericLib GL = new GenericLib();

		textControlLib TC = new textControlLib();
		public int R_Year {
			get {
				try {
					return GL.CINT( GL.SplitString(ddlRound.Items[ddlRound.SelectedIndex].Value, "-")[0]);
				} catch (Exception ex) {
					return 0;
				}
			}
		}

		public int R_Round {
			get {
				try {
					return GL.CINT(GL.SplitString(ddlRound.Items[ddlRound.SelectedIndex].Value, "-")[1]);
				} catch  {
					return 0;
				}
			}
		}

		public string PSNL_NO {
			get {
				try {
					return Session["USER_PSNL_NO"].ToString ();
				} catch (Exception ex) {
					return "";
				}
			}
		}

        public int WAGE_TYPE
        {
            get { return (GL.CINT(lblCOMPStatus.Attributes["WAGE_TYPE"])); }
            set { lblCOMPStatus.Attributes["WAGE_TYPE"] = Convert.ToString(value); }
        }
        public int PNPS_CLASS
        {
            get { return (GL.CINT(lblCOMPStatus.Attributes["PNPS_CLASS"])); }
            set { lblCOMPStatus.Attributes["PNPS_CLASS"] = Convert.ToString(value); }
        }

		public string ASSESSOR_BY {
			get {
				try {
					string[] tmp = Strings.Split(ddlASSName.Items[ddlASSName.SelectedIndex].Value, ":::");
					return tmp[0];
				} catch (Exception ex) {
					return "";
				}
			}
		}

		public string ASSESSOR_NAME {
			get {
				try {
					return ddlASSName.Items[ddlASSName.SelectedIndex].Text;
				} catch (Exception ex) {
					return "";
				}
			}
		}

		public string ASSESSOR_POS {
			get {
				try {
					string[] tmp = Strings.Split(ddlASSName.Items[ddlASSName.SelectedIndex].Value, ":::");
					return tmp[1];
				} catch (Exception ex) {
					return "";
				}
			}
		}

		public string ASSESSOR_DEPT {
			get {
				try {
					string[] tmp = Strings.Split(ddlASSName.Items[ddlASSName.SelectedIndex].Value, ":::");
					return tmp[2];
				} catch (Exception ex) {
					return "";
				}
			}
		}
		public int COMP_Status {
            get { return (GL.CINT(lblCOMPStatus.Attributes["COMP_Status"])); }
            set { lblCOMPStatus.Attributes["COMP_Status"] = Convert.ToString(value); }
		}

		public int PSNL_CLASS_GROUP {
			get {
				try {
					return Convert.ToInt32(Conversion.Val(lblName.Attributes["CLSGP_ID"]));
				} catch (Exception ex) {
					return 0;
				}
			}
			set { lblName.Attributes["CLSGP_ID"] = value.ToString (); }
		}

		public int PSNL_TYPE {
			get {
				try {
					return Convert.ToInt32(Conversion.Val(lblPSNType.Attributes["PSNL_Type_Code"]));
				} catch (Exception ex) {
					return -1;
				}
			}
            set { lblPSNType.Attributes["PSNL_Type_Code"] = value.ToString(); }
		}

		public string FN_CODE {
			get {
				try {
					return lblPSNPos.Attributes["FN_CODE"];
				} catch (Exception ex) {
					return "";
				}
			}
            set { lblPSNPos.Attributes["FN_CODE"] = value.ToString(); }
		}

		public string FN_Type {
			get {
				try {
					return lblPSNType.Attributes["FN_Type"];
				} catch (Exception ex) {
					return "";
				}
			}
            set { lblPSNType.Attributes["FN_Type"] = value.ToString(); }
		}

		public string FN_ID {
			get {
				try {
					return FN_CODE;
				} catch (Exception ex) {
					return "";
				}
			}
		}

        public string DEPT_CODE
        {
            get
            {
                try
                {
                    return lblPSNType.Attributes["DEPT_CODE"];
                }
                catch (Exception ex)
                {
                    return "";
                }
            }
            set { lblPSNType.Attributes["DEPT_CODE"] = value.ToString(); }
        }

		//------------------- For Behavior Setting-------------
		public int COMP_Type_Id {
            get { return Convert.ToInt32(txtCOMPTypeID.Text); }
			set { txtCOMPTypeID.Text = value.ToString(); }
		}

		public int Master_No {
			get { return Convert.ToInt32(Conversion.Val(txtMasterNo.Text)); }
			set { txtMasterNo.Text = value.ToString(); }
		}

		public int BHV_No {
			get { return GL.CINT(txtBehaviorNo.Text); }
			set { txtBehaviorNo.Text = value.ToString(); }
		}



		protected void Page_Load(object sender, System.EventArgs e)
		{
			if ((Session["USER_PSNL_NO"] == null)) {
				ScriptManager.RegisterStartupScript(this.Page, typeof(string), "Alert", "showAlert('กรุณาลงชื่อเข้าใช้ระบบ');", true);
				Response.Redirect("Login.aspx");
				return;
			}

			if (!IsPostBack) {
                //BL.BindDDlYearRound(ddlRound);
                // Keep Start Time
                DateTime StartTime = DateTime.Now;

                BL.BindDDlYearRound_ForTYPE(ddlRound, R_Year, R_Round,PSNL_NO );     //------------แสดงเฉพาะรอบที่มีสิทธิ์การเข้าประเมิน
                //----------------- ทำสองที่ Page Load กับ Update Status ---------------

                if (Session["ddlRound"] != null)
                {
                    ddlRound.SelectedIndex = GL.CINT(Session["ddlRound"]);
                }
                BL.Update_COMP_Status_To_Assessment_Period(PSNL_NO, R_Year, R_Round);	
                BindPersonal();
				BindAssessor();
				BindMasterCOMP();
				SetPrintButton();

                //Report Time To Entry This Page
                DateTime EndTime = DateTime.Now;
                lblReportTime.Text = "เริ่มเข้าสู่หน้านี้ " + StartTime.ToString("HH:mm:ss.ff") + " ถึง " + EndTime.ToString("HH:mm:ss.ff") + " ใช้เวลา " + Math.Round( GL.DiffTimeDecimalSeconds(StartTime,EndTime),2) + " วินาที";
			}
		}



        private void BindPersonal()
		{
			HRBL.PersonalInfo PSNInfo = BL.GetAssessmentPersonalInfo(R_Year, R_Round, PSNL_NO, COMP_Status);
			lblPSNName.Text = PSNInfo.PSNL_Fullname;
			lblPSNDept.Text = PSNInfo.DEPT_NAME;
            
			if (!string.IsNullOrEmpty(PSNInfo.MGR_NAME)) {
				lblPSNPos.Text = PSNInfo.MGR_NAME;
			} else {
				lblPSNPos.Text = PSNInfo.FN_NAME;
			}
			lblPSNType.Text = "พนักงาน " + PSNInfo.WAGE_NAME + " ระดับ " + Convert.ToInt32(PSNInfo.PNPS_CLASS);

			PSNL_CLASS_GROUP = GL.CINT( PSNInfo.CLSGP_ID);
			PSNL_TYPE =GL.CINT( PSNInfo.PSNL_TYPE);
			FN_CODE = PSNInfo.FN_ID;
			FN_Type = PSNInfo.FN_Type;
            DEPT_CODE = PSNInfo.DEPT_CODE;
            WAGE_TYPE = GL.CINT(PSNInfo.WAGE_TYPE);
            PNPS_CLASS = GL.CINT(PSNInfo.PNPS_CLASS);

		}


		private void BindAssessor()
		{
			BL.BindDDLAssessor(ddlASSName, R_Year, R_Round, PSNL_NO);
			ddlASSName_SelectedIndexChanged(null, null);
		}

		protected void ddlASSName_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			lblASSPos.Text = ASSESSOR_POS;
			lblASSDept.Text = ASSESSOR_DEPT;
			SaveHeader();
		}


		private void BindMasterCOMP()
		{
            //if (Session["ddlRound"] != null)
            //{
            //    ddlRound.SelectedIndex = GL.CINT(Session["ddlRound"]);
            //}

			COMP_Status = BL.GetCOMPStatus(R_Year, R_Round, PSNL_NO);
			lblCOMPStatus.Text = BL.GetAssessmentStatusName(COMP_Status);
           
            // มีเอาไว้คิดรอบที่กำลังทำ อยู่ในช่วงที่กำหนดหรือไม่  
            Boolean IsInPeriod = BL.IsTimeInAssessmentPeriod(DateTime.Now, R_Year, R_Round, HRBL.AssessmentType.Competency, HRBL.AssessmentStatus.WaitAssessment,DEPT_CODE.ToString ());
            // หาว่ารอบปิดหรือยัง  
            Boolean IsRoundCompleted = BL.Is_Round_Completed(R_Year, R_Round);

            Boolean IsEditable = COMP_Status == HRBL.AssessmentStatus.WaitAssessment & IsInPeriod & !IsRoundCompleted;

            pnlActivity.Visible = IsEditable;
            pnlCOMP.Enabled = IsEditable;
            ddlASSName.Enabled = IsEditable;
            btnSaveRemark.Visible = IsEditable;
            if (btnSaveRemark.Visible)
            {
                divFooter.Attributes["class"] = "Buttonfooter";
            }
            else
            {
                divFooter.Attributes["class"] = "";
            }
            
			DataTable DT = BL.GetCOMPBundle(PSNL_NO, R_Year, R_Round, PSNL_CLASS_GROUP, PSNL_TYPE, FN_ID, FN_Type, HRBL.CompetencyType.All);
			Behavior = BL.GetCOMPBehavior(PSNL_NO, R_Year, R_Round).Table;

			//--------------- Calculate Final Score -------------
			DT.Columns.Add("Score", typeof(Int32));
			DT.Columns.Add("FinalScore", typeof(Int32));

			for (int i = 0; i <= DT.Rows.Count - 1; i++) {
				int TotalBehavior =GL.CINT( Behavior.Compute("COUNT(Master_No)", "COMP_Type_Id=" + DT.Rows[i]["COMP_Type_Id"].ToString() + " AND Master_No=" + DT.Rows[i]["Master_No"].ToString()).ToString());
				int SelectedChoice = 0;
				if (!GL.IsEqualNull(DT.Rows[i]["COMP_Answer_MGR"])) {
					SelectedChoice = GL.CINT(DT.Rows[i]["COMP_Answer_MGR"]);
				} else if (!GL.IsEqualNull(DT.Rows[i]["COMP_Answer_PSN"])) {
					SelectedChoice = GL.CINT(DT.Rows[i]["COMP_Answer_PSN"]);
				}
				DT.Rows[i]["Score"] = SelectedChoice;

				if (SelectedChoice >= 3) {
					DT.Rows[i]["FinalScore"] = (TotalBehavior >= SelectedChoice ? SelectedChoice : TotalBehavior);
				} else {
					DT.Rows[i]["FinalScore"] = SelectedChoice;
				}
			}

			rptAss.DataSource = DT;
			rptAss.DataBind();

		}

		protected void rptAss_ItemCommand(object source, System.Web.UI.WebControls.RepeaterCommandEventArgs e)
		{

            HtmlTableCell ass_no = (HtmlTableCell)e.Item.FindControl("ass_no");
            DataTable DT;
            SqlCommandBuilder cmd;
            SqlDataAdapter DA;
            string SQL_Remark = "";
            TextBox txtPSN = (TextBox)e.Item.FindControl("txtPSN");
            switch (e.CommandName) {
				case "Select":

                    Button btn = (Button)e.CommandSource;
					int SelectedChoice = Convert.ToInt32(btn.ID.Replace("btn", ""));
                    int _COMP_Type_Id = GL.CINT(ass_no.Attributes["COMP_Type_Id"]);
                    int _Master_No = GL.CINT(ass_no.InnerHtml);
					string SQL = "SELECT * FROM tb_HR_COMP_Detail \n";
					SQL += " WHERE PSNL_NO='" + PSNL_NO.Replace("'", "''") + "' ";
					SQL += " AND R_Year=" + R_Year + "\n";
					SQL += " AND R_Round=" + R_Round + "\n";
                    SQL += " AND COMP_Type_Id=" + _COMP_Type_Id + "\n";
                    SQL += " AND Master_No=" + _Master_No + "\n";

					DA = new SqlDataAdapter(SQL, BL.ConnectionString());
					DT = new DataTable();
					DA.Fill(DT);

					if (DT.Rows.Count == 0)
						return;

					DT.Rows[0]["COMP_Answer_PSN"] = SelectedChoice;
					DT.Rows[0]["Update_By"] = Session["USER_PSNL_NO"];
					DT.Rows[0]["Update_Time"] = DateAndTime.Now;

					cmd = new SqlCommandBuilder(DA);
					try {
						DA.Update(DT);
					} catch {
					}
                    //----------Show/Hide Behavior Auto Add Behavior--------------
                    int YearRound= (R_Year*10) + R_Round;
                    if (YearRound >= BL.Start_Skip_Behavior_Round())
                    {
                        BL.AutoAddBehavior(PSNL_NO, R_Year, R_Round, _COMP_Type_Id, _Master_No,SelectedChoice);
                    }
                    
                    //'-----------------------------

                    SaveRemark();

					//------------- Refresh-----------
					BindMasterCOMP();

					break;
				case "AddBehavior":
                    Button btnAddBehavior = (Button)e.Item.FindControl("btnAddBehavior");

                    COMP_Type_Id =   GL.CINT (ass_no.Attributes["COMP_Type_Id"]);
                    Master_No = Convert.ToInt32(ass_no.InnerHtml);
					BHV_No = Convert.ToInt32(btnAddBehavior.CommandArgument) + 1;
					btnBehaviorDialog_Click(null, null);

					break;
				case "Remark":
					
                    //TextBox txtPSN = (TextBox)e.Item.FindControl("txtPSN");
                    if (txtPSN.Text != "")
                    {
                        if (txtPSN.Text.Length > 3990)
                        {
                            txtPSN.Text = txtPSN.Text.Substring(0, 3990);
                        }

                    }

                      SQL_Remark = "SELECT * FROM tb_HR_COMP_Detail \n";
                    SQL_Remark += " WHERE PSNL_NO='" + PSNL_NO.Replace("'", "''") + "' ";
                    SQL_Remark += " AND R_Year=" + R_Year + "\n";
                    SQL_Remark += " AND R_Round=" + R_Round + "\n";
                    SQL_Remark += " AND COMP_Type_Id=" + ass_no.Attributes["COMP_Type_Id"] + "\n";
                    SQL_Remark += " AND Master_No=" + ass_no.InnerHtml + "\n";

                    DA = new SqlDataAdapter(SQL_Remark, BL.ConnectionString());
                    DT = new DataTable();
                    DA.Fill(DT);

                    if (DT.Rows.Count == 0)
                        return;

                    DT.Rows[0]["COMP_Remark_PSN"] = txtPSN.Text;
                    DT.Rows[0]["Update_By"] = Session["USER_PSNL_NO"];
                    DT.Rows[0]["Update_Time"] = DateAndTime.Now;

                    cmd = new SqlCommandBuilder(DA);
                    try {
                        DA.Update(DT);
                    } catch {
                    }

                    //------------- Refresh-----------
                    BindMasterCOMP();

                    break;
			}
		}

		int LastType = 0;
			//----------------- To Bind Behavior ---------------
		DataTable Behavior = null;
		protected void rptAss_ItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
		{
			switch (e.Item.ItemType) {

				case ListItemType.AlternatingItem:
				case ListItemType.Item:

                    DataRowView drv = (DataRowView)e.Item.DataItem;

                    HtmlTableRow row_COMP_type = (HtmlTableRow)e.Item.FindControl("row_COMP_type");
                    HtmlTableCell cell_COMP_type = (HtmlTableCell)e.Item.FindControl("cell_COMP_type");

                    HtmlTableCell ass_no = (HtmlTableCell)e.Item.FindControl("ass_no");
                    HtmlTableCell COMP_Name = (HtmlTableCell)e.Item.FindControl("COMP_Name");
                    HtmlTableCell target = (HtmlTableCell)e.Item.FindControl("target");
                    Label weight = (Label)e.Item.FindControl("weight");
                    HtmlTableCell cel_officer = (HtmlTableCell)e.Item.FindControl("cel_officer");
                    HtmlTableCell mark = (HtmlTableCell)e.Item.FindControl("mark");
                    HtmlTableCell cell_weight = (HtmlTableCell)e.Item.FindControl("cell_weight");
					//--------------- Textbox -------------
                    TextBox txtPSN = (TextBox)e.Item.FindControl("txtPSN");
                    TextBox txtMGR = (TextBox)e.Item.FindControl("txtMGR");
                    Button btnRemark = (Button)e.Item.FindControl("btnRemark");
					//-------------------- Notification Handler ------------------
                    Button btn_Cal_Bhv = (Button)e.Item.FindControl("btn_Cal_Bhv");
                    Image imgAlert = (Image)e.Item.FindControl("imgAlert");
                    Label lblAlert = (Label)e.Item.FindControl("lblAlert");

                    ass_no.Attributes["COMP_Type_Id"] = drv["COMP_Type_Id"].ToString();
                    if (LastType !=GL.CINT ( drv["COMP_Type_Id"]))
                    {
                        switch (GL.CINT(drv["COMP_Type_Id"].ToString()))
                        {
							case 1:
								cell_COMP_type.InnerHtml = "สมรรถนะหลัก (Core Competency)";
								break;
							case 2:
								cell_COMP_type.InnerHtml = "สมรรถนะตามสายงาน (Functional Competency)";
								break;
							case 3:
								cell_COMP_type.InnerHtml = "สมรรถนะตามสายระดับ (Managerial Competency)";
								break;
						}
						row_COMP_type.Visible = true;
                        LastType =GL.CINT( drv["COMP_Type_Id"].ToString());
					} else {
						row_COMP_type.Visible = false;
					}

                    ass_no.InnerHtml = drv["Master_No"].ToString();
					//.ToString().PadLeft(2, GL.chr0)
					COMP_Name.InnerHtml = drv["COMP_Comp"].ToString().Replace("\n", "<br>");
					target.InnerHtml = drv["COMP_Std"].ToString().Replace("\n", "<br>");
					weight.Text = drv["COMP_Weight"].ToString();

					//---------------------- Remark --------------------------
					txtPSN.Text = drv["COMP_Remark_PSN"].ToString();
					txtMGR.Text = drv["COMP_Remark_MGR"].ToString();

					if (COMP_Status == HRBL.AssessmentStatus.WaitAssessment) {
                        //txtPSN.Attributes["onchange"] = "document.getElementById('" + btnRemark.ClientID + "').click();";
					} else {
						txtPSN.ReadOnly = true;
					}

					for (int i = 1; i <= 5; i++) {
                        HtmlTableCell choice = (HtmlTableCell)e.Item.FindControl("choice" + i);
                        HtmlTableCell selOfficialColor = (HtmlTableCell)e.Item.FindControl("selOfficialColor" + i);
                        Button btn = (Button)e.Item.FindControl("btn" + i);
						choice.InnerHtml = drv["COMP_Choice_" + i.ToString()].ToString().Replace("\n", "<br>");
						//------------------------- Set Editable ---------------------
						if (COMP_Status == HRBL.AssessmentStatus.WaitAssessment) {
							selOfficialColor.Attributes["onclick"] = "document.getElementById('" + btn.ClientID + "').click();";
							choice.Attributes["onclick"] = "document.getElementById('" + btn.ClientID + "').click();";
						} else {
							selOfficialColor.Style["cursor"] = "Default";
							choice.Style["cursor"] = "Default";
						}
					}


					//------------------- Get All Mark -----------------------
					int Officer = 0;
					int Manager = 0;
					if (!GL.IsEqualNull(drv["COMP_Answer_PSN"]))
						Officer = GL.CINT ( drv["COMP_Answer_PSN"]);
					if (!GL.IsEqualNull(drv["COMP_Answer_MGR"]))
						Manager = GL.CINT (drv["COMP_Answer_MGR"]);

					//------------------- Officer Mark -----------------------
					if (Officer > 0) {
						cel_officer.Attributes["class"] = "AssLevel" + Officer;
						for (int j = 1; j <= Officer; j++) {
                            HtmlTableCell _selOfficialColor = (HtmlTableCell)e.Item.FindControl("selOfficialColor" + j);
							_selOfficialColor.Attributes["class"] = "AssLevel" + Officer;
						}
					}

					//------------------- Officer Mark -----------------------
					if (Officer > 0) {
						cel_officer.Attributes["class"] = "AssLevel" + Officer;
					}

					//-----------Manager Mark-------------
                    HtmlTableRow rowHeader = (HtmlTableRow)e.Item.FindControl("rowHeader");
                    HtmlTableCell cel_header = (HtmlTableCell)e.Item.FindControl("cel_header");
					if (Manager > 0) {
						rowHeader.Visible = true;
						cel_header.Attributes["class"] = "AssLevel" + Manager;
						for (int j = 1; j <= Manager; j++) {
							HtmlTableCell selHeaderColor =(HtmlTableCell) e.Item.FindControl("selHeaderColor" + j);
							selHeaderColor.Attributes["class"] = "AssLevel" + Manager;
						}
					} else {
						rowHeader.Visible = false;
					}

					int Score = GL.CINT ( drv["Score"]);
					//-----------คะแนนสูงสุดที่เลือก ----------

                    //------------------ Show/Hide Behavior-------------------
                    HtmlTableRow trBehavior = (HtmlTableRow)e.Item.FindControl("trBehavior");
                    int YearRound = (R_Year * 10) + R_Round;
                    if (YearRound >= BL.Start_Skip_Behavior_Round())
                    {
                        trBehavior.Visible = false;
                        if (Manager == 0)
                        {
                            mark.RowSpan = 5;
                            cell_weight.RowSpan = 5;
                            ass_no.RowSpan = 5;
                        }
                        else{
                            mark.RowSpan = 6;
                            cell_weight.RowSpan = 6;
                            ass_no.RowSpan = 6;
                        }                        
                    }
                    else
                    {
                        trBehavior.Visible = true;
                        if (Manager == 0)
                        {
                            mark.RowSpan = 6;
                            cell_weight.RowSpan = 6;
                            ass_no.RowSpan = 6;

                        }
                        else {
                            mark.RowSpan = 7;
                            cell_weight.RowSpan = 7;
                            ass_no.RowSpan = 7;
                        }                        
                    }



					if (Score > 0) {
						cell_weight.Attributes["class"] = "AssLevel" + Score;
						COMP_Name.Attributes["class"] = "TextLevel" + Score;
						target.Attributes["class"] = "TextLevel" + Score;
						mark.Attributes["class"] = "AssLevel" + Score;

						HtmlTableCell _choice = (HtmlTableCell)e.Item.FindControl("choice" + Score);
						_choice.Attributes["class"] = "TextLevel" + Score;

						//------------- Add Wink Feature--------------
                        if (GL.CINT(drv["FinalScore"]) < Score & YearRound < BL.Start_Skip_Behavior_Round()) //------ Show/Hide Behavior
                        {
							mark.Attributes["class"] += " winkLevel" + Score;
							cell_weight.Attributes["class"] += " winkLevel" + Score;
							mark.Attributes["title"] = "พฤติกรรมบ่งชี้น้อยกว่าคะแนนประเมิน";
							cell_weight.Attributes["title"] = mark.Attributes["title"];
							imgAlert.Visible = true;
							lblAlert.Text = "ต้องระบุพฤติกรรมบ่งชี้ " + Score + " ข้อ";
						}
						//------------- End Wink Feature--------------
					}

					//-------------- Report Score----------'-----------คะแนนที่ได้----------
					mark.InnerHtml = drv["FinalScore"].ToString ();

					//--------------- Behavior ------------
					HtmlTableCell cell_Add_Behavior =(HtmlTableCell) e.Item.FindControl("cell_Add_Behavior");
					Button btnAddBehavior =(Button) e.Item.FindControl("btnAddBehavior");
					Repeater rptBehavior =(Repeater) e.Item.FindControl("rptBehavior");

					rptBehavior.ItemDataBound += rptBehavior_ItemDataBound;
					Behavior.DefaultView.RowFilter = "COMP_Type_Id=" + drv["COMP_Type_Id"] + " AND Master_No=" + drv["Master_No"];
					rptBehavior.DataSource = Behavior.DefaultView.ToTable().Copy();
					rptBehavior.DataBind();

					btnAddBehavior.CommandArgument = ( Behavior.DefaultView.Count).ToString ();
					//-----  จำนวนพฤติกรรมบ่งชี้ที่มี -----
					btnAddBehavior.Visible = COMP_Status == HRBL.AssessmentStatus.WaitAssessment;

					break;
				case ListItemType.Footer:
					//----------------Report Summary ------------

					//----------- Control Difinition ------------
					HtmlTableCell cell_sum_mark =(HtmlTableCell) e.Item.FindControl("cell_sum_mark");
					HtmlTableCell cell_sum_weight =(HtmlTableCell) e.Item.FindControl("cell_sum_weight");
					HtmlTableCell cell_total =(HtmlTableCell) e.Item.FindControl("cell_total");
					Label lblDone =(Label) e.Item.FindControl("lblDone");
					Label lblTotalItem =(Label) e.Item.FindControl("lblTotalItem");
					HtmlTableCell cell_sum_raw =(HtmlTableCell) e.Item.FindControl("cell_sum_raw");

					//------------ Report -------------
					DataTable DT =(DataTable) rptAss.DataSource;

					int TotalItem = DT.Rows.Count;

					int AssCount = GL.CINT ( DT.Compute("COUNT(Master_No)", "Score>0"));

					if (TotalItem == 0) {
						cell_total.InnerHtml = "ไม่พบหัวข้อในแบบประเมิน";
					} else if (AssCount == 0) {
						cell_total.InnerHtml = "ยังไม่ได้ประเมิน";
					} else if (AssCount == TotalItem) {
						cell_total.InnerHtml = "ประเมินครบ " + TotalItem + " ข้อแล้ว";
					} else {
						cell_total.InnerHtml = "ประเมินแล้ว " + AssCount + " ข้อจากทั้งหมด " + TotalItem + " ข้อ";
					}

					for (int i = 1; i <= 5; i++) {
						HtmlTableCell cell_sum =(HtmlTableCell) e.Item.FindControl("cell_sum_" + i);
						int cnt =GL.CINT ( DT.Compute("COUNT(Master_No)", "Score=" + i));
						if (cnt == 0) {
							cell_sum.InnerHtml = "-";
						} else {
							cell_sum.InnerHtml = "ได้ " + DT.Compute("COUNT(Master_No)", "Score=" + i).ToString() + " ข้อ";
							cell_sum.Attributes["class"] = "AssLevel" + i;
						}
					}


					if (TotalItem != 0) {
						DT.Columns.Add("MARK", typeof(Int32), "FinalScore");
						DT.Columns.Add("WEIGHT", typeof(double));

						for (int i = 0; i <= DT.Rows.Count - 1; i++) {
							if (!GL.IsEqualNull(DT.Rows[i]["COMP_Weight"]) && Information.IsNumeric(DT.Rows[i]["COMP_Weight"].ToString().Replace("%", ""))) {
								DT.Rows[i]["WEIGHT"] = DT.Rows[i]["COMP_Weight"].ToString().Replace("%", "");
							}
						}
						DT.Columns.Add("MARKxWEIGHT", typeof(double), "MARK*WEIGHT");
						DT.Columns.Add("WEIGHTx5", typeof(double), "WEIGHT*5");

						object _SUM = DT.Compute("SUM(MARKxWEIGHT)", "");
						object _TOTAL = DT.Compute("SUM(WEIGHTx5)", "");

						if (!GL.IsEqualNull(_SUM) & !GL.IsEqualNull(_TOTAL)) {
							double Result = Convert.ToDouble(_SUM) * 100 / Convert.ToDouble(_TOTAL);
							if (Strings.Len(Result) > 5)
								Result = GL.CDBL(Result.ToString());
							cell_sum_mark.InnerHtml = Result + "%";
							cell_sum_raw.InnerHtml = (Result * 5) + "/500";

							if (Result <= 20) {
								cell_sum_mark.Attributes["class"] = "AssLevel1";
								cell_sum_raw.Attributes["class"] = "AssLevel1";
							} else if (Result <= 40) {
								cell_sum_mark.Attributes["class"] = "AssLevel2";
								cell_sum_raw.Attributes["class"] = "AssLevel2";
							} else if (Result <= 60) {
								cell_sum_mark.Attributes["class"] = "AssLevel3";
								cell_sum_raw.Attributes["class"] = "AssLevel3";
							} else if (Result <= 80) {
								cell_sum_mark.Attributes["class"] = "AssLevel4";
								cell_sum_raw.Attributes["class"] = "AssLevel4";
							} else {
								cell_sum_mark.Attributes["class"] = "AssLevel5";
								cell_sum_raw.Attributes["class"] = "AssLevel5";
							}
						} else {
							cell_sum_mark.Attributes["class"] = "";
							cell_sum_mark.InnerHtml = "0%";
							cell_sum_raw.InnerHtml = "0/500";
						}
					}
					break;
			}


		}

		protected void rptBehavior_ItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
		{
			if (e.Item.ItemType != ListItemType.AlternatingItem & e.Item.ItemType != ListItemType.Item)
				return;

            DataRowView drv = (DataRowView)e.Item.DataItem;
			HtmlAnchor aBehavior =(HtmlAnchor) e.Item.FindControl("aBehavior");
			HtmlAnchor aDelete =(HtmlAnchor) e.Item.FindControl("aDelete");
			aBehavior.InnerHtml = drv["BHV_No"].ToString() + " . " + drv["BHV_Content"].ToString();
			if (COMP_Status == HRBL.AssessmentStatus.WaitAssessment) {
				//-------------------------------- Add Click Event---------------------------------------
				string Script = "document.getElementById('" + txtCOMPTypeID.ClientID + "').value=" + drv["COMP_Type_Id"] + ";";
				Script += "document.getElementById('" + txtMasterNo.ClientID + "').value=" + drv["Master_No"] + ";";
				Script += "document.getElementById('" + txtBehaviorNo.ClientID + "').value='" + drv["BHV_No"] + "';";
				Script += "document.getElementById('" + btnBehaviorDialog.ClientID + "').click();";
				aBehavior.Attributes["onclick"] = Script;


				Script = "document.getElementById('" + txtCOMPTypeID.ClientID + "').value=" + drv["COMP_Type_Id"] + ";";
				Script += "document.getElementById('" + txtMasterNo.ClientID + "').value=" + drv["Master_No"] + ";";
				Script += "document.getElementById('" + txtBehaviorNo.ClientID + "').value=" + drv["BHV_No"] + ";";
				Script += "document.getElementById('" + btnBehaviorDelete.ClientID + "').click();";
				aDelete.Attributes["onclick"] = Script;
			} else {
				aDelete.Visible = false;
				aBehavior.Style["width"] = "";
				aBehavior.Style["cursor"] = "Default";
			}

		}

		protected void btnBehaviorDialog_Click(object sender, System.EventArgs e)
		{
			lblBehaviorNo.Text = txtBehaviorNo.Text;

			string SQL = "SELECT BHV_Content FROM tb_HR_COMP_Behavior \n";
			SQL += " WHERE PSNL_NO='" + PSNL_NO.Replace("'", "''") + "' ";
			SQL += " AND R_Year=" + R_Year + "\n";
			SQL += " AND R_Round=" + R_Round + "\n";
			SQL += " AND COMP_Type_Id=" + COMP_Type_Id + "\n";
			SQL += " AND Master_No=" + Master_No + "\n";
			SQL += " AND BHV_No=" + BHV_No + "\n";

			SqlDataAdapter DA = new SqlDataAdapter(SQL, BL.ConnectionString());
			DataTable DT = new DataTable();
			DA.Fill(DT);

			if (DT.Rows.Count == 0) {
				txtBehavior.Text = "";
			} else {
				txtBehavior.Text = DT.Rows[0]["BHV_Content"].ToString ();
			}
			ModalBehavior.Visible = true;
			ScriptManager.RegisterStartupScript(this.Page, typeof(string), "focus", "document.getElementById('" + txtBehavior.ClientID + "').focus();", true);
		}


		protected void btnBehaviorDelete_Click(object sender, System.EventArgs e)
		{
			string SQL = "SELECT * FROM tb_HR_COMP_Behavior \n";
			SQL += " WHERE PSNL_NO='" + PSNL_NO.Replace("'", "''") + "' ";
			SQL += " AND R_Year=" + R_Year + "\n";
			SQL += " AND R_Round=" + R_Round + "\n";
			SQL += " AND COMP_Type_Id=" + COMP_Type_Id + "\n";
			SQL += " AND Master_No=" + Master_No + "\n";

			SqlDataAdapter DA = new SqlDataAdapter(SQL, BL.ConnectionString());
			DataTable DT = new DataTable();
			DA.Fill(DT);
			//--------------- Delete  Target-----
			DT.DefaultView.RowFilter = "BHV_No=" + BHV_No;
			if (DT.DefaultView.Count > 0) {
				DT.DefaultView[0].Row.Delete();
			}
			SqlCommandBuilder cmd = new SqlCommandBuilder(DA);
			try {
				DA.Update(DT);
			} catch {
			}
			//--------------- ReOrder -----------
			for (int i = 0; i <= DT.Rows.Count - 1; i++) {
				DT.Rows[i]["BHV_No"] = i + 1;
			}
			cmd = new SqlCommandBuilder(DA);
			try {
				DA.Update(DT);
			} catch {
			}

			ModalBehavior.Visible = false;
			BindMasterCOMP();

		}

		protected void btnClose_Click(object sender, System.EventArgs e)
		{
			ModalBehavior.Visible = false;
		}

		protected void btnOK_Click(object sender, System.EventArgs e)
		{
			if (string.IsNullOrEmpty(Strings.Trim(txtBehavior.Text))) {
				ScriptManager.RegisterStartupScript(this.Page, typeof(string), "Alert", "showAlert('กรอกรายละเอียด');", true);
				return;
			}

			string SQL = "SELECT * FROM tb_HR_COMP_Behavior \n";
			SQL += " WHERE PSNL_NO='" + PSNL_NO.Replace("'", "''") + "' ";
			SQL += " AND R_Year=" + R_Year + "\n";
			SQL += " AND R_Round=" + R_Round + "\n";
			SQL += " AND COMP_Type_Id=" + COMP_Type_Id + "\n";
			SQL += " AND Master_No=" + Master_No + "\n";
			SQL += " AND BHV_No=" + BHV_No + "\n";

			SqlDataAdapter DA = new SqlDataAdapter(SQL, BL.ConnectionString());
			DataTable DT = new DataTable();
			DA.Fill(DT);

			DataRow DR = null;
			if (DT.Rows.Count == 0) {
				DR = DT.NewRow();
				DR["PSNL_NO"] = PSNL_NO;
				DR["R_Year"] = R_Year;
				DR["R_Round"] = R_Round;
				DR["COMP_Type_Id"] = COMP_Type_Id;
				DR["Master_No"] = Master_No;
				DR["BHV_No"] = BHV_No;
			} else {
				DR = DT.Rows[0];
			}
			DR["BHV_Content"] = txtBehavior.Text;

			if (DT.Rows.Count == 0)
				DT.Rows.Add(DR);
			SqlCommandBuilder cmd = new SqlCommandBuilder(DA);
			try {
				DA.Update(DT);
			} catch (Exception ex) {
			}

			ModalBehavior.Visible = false;
			BindMasterCOMP();

		}

		protected void btnPreSend_Click(object sender, System.EventArgs e)
		{
			DataTable DT = BL.GetCOMPBundle(PSNL_NO, R_Year, R_Round, PSNL_CLASS_GROUP, PSNL_TYPE, FN_ID, FN_Type, HRBL.CompetencyType.All);


			DT.DefaultView.RowFilter = "COMP_Answer_PSN IS NULL OR COMP_Answer_PSN=0";
			if (DT.DefaultView.Count > 0) {
				ScriptManager.RegisterStartupScript(this.Page, typeof(string), "Alert", "showAlert('คลิกเพื่อประเมินให้ครบทุกข้อ');", true);
				return;
			}

			//--------------- Calculate Final Score -------------
			Behavior = BL.GetCOMPBehavior(PSNL_NO, R_Year, R_Round).Table;
			DT.Columns.Add("FinalScore", typeof(Int32));
			for (int i = 0; i <= DT.Rows.Count - 1; i++) {
				int TotalBehavior = GL.CINT(Behavior.Compute("COUNT(Master_No)", "COMP_Type_Id=" + DT.Rows[i]["COMP_Type_Id"].ToString() + " AND Master_No=" + DT.Rows[i]["Master_No"].ToString()));
				int SelectedChoice = 0;
				if (!GL.IsEqualNull(DT.Rows[i]["COMP_Answer_PSN"]))
					SelectedChoice = GL.CINT(DT.Rows[i]["COMP_Answer_PSN"]);
				if (SelectedChoice >= 3) {
					DT.Rows[i]["FinalScore"] = (TotalBehavior >= SelectedChoice ? SelectedChoice : TotalBehavior);
				} else {
					DT.Rows[i]["FinalScore"] = SelectedChoice;
				}
			}
			DT.DefaultView.RowFilter = "FinalScore < COMP_Answer_PSN";
			if (DT.DefaultView.Count > 0) {
				ScriptManager.RegisterStartupScript(this.Page, typeof(string), "Alert", "showAlert('มีบางข้อพฤติกรรมบ่งชี้ น้อยกว่าคะแนนประเมิน');", true);
				return;
			}

			ScriptManager.RegisterStartupScript(this.Page, typeof(string), "Alert", "if(confirm('ยืนยันส่งผลการประเมิน ??\\nหลังจากส่งผลการประเมินแล้วคุณไม่สามารถแก้ไขได้')) document.getElementById('" + btnSend.ClientID + "').click();", true);
		}

		protected void btnSend_Click(object sender, System.EventArgs e)
		{
			DataTable DT = BL.GetCOMPBundle(PSNL_NO, R_Year, R_Round, PSNL_CLASS_GROUP, PSNL_TYPE, FN_ID, FN_Type, HRBL.CompetencyType.All);

			//--------------------Saving--------------------
			string SQL = "";
			SQL = " SELECT * \n";
			SQL += "  FROM tb_HR_COMP_Header\n";
			SQL += " WHERE PSNL_NO='" + PSNL_NO.Replace("'", "''") + "' AND R_Year=" + R_Year + " AND R_Round=" + R_Round;
			SqlDataAdapter DA = new SqlDataAdapter(SQL, BL.ConnectionString());
			DT = new DataTable();
			DA.Fill(DT);

			DataRow DR = DT.Rows[0];
			DR["COMP_Status"] = Convert.ToString(GL.CINT(HRBL.AssessmentStatus.WaitConfirmAssessment));
			DR["Update_By"] = Session["USER_PSNL_NO"];
			DR["Update_Time"] = DateAndTime.Now;

			DR["Assessment_Commit_PSN"] = true;
			DR["Assessment_Commit_By_PSN"] = PSNL_NO;
			DR["Assessment_Commit_Time_PSN"] = DateAndTime.Now;
			//------------- Set Manager ----------------
			DR["Assessment_Commit_Name"] = ASSESSOR_NAME;
			DR["Assessment_Commit_DEPT"] = ASSESSOR_DEPT;
			DR["Assessment_Commit_POS"] = ASSESSOR_POS;
			DR["Assessment_Commit_By_MGR"] = ASSESSOR_BY;

			if (DT.Rows.Count == 0)
				DT.Rows.Add(DR);
			SqlCommandBuilder cmd = new SqlCommandBuilder(DA);
			DA.Update(DT);

			//----------------- ทำสองที่ Page Load กับ Update Status ---------------
			BL.Update_COMP_Status_To_Assessment_Period(PSNL_NO, R_Year, R_Round);

			ScriptManager.RegisterStartupScript(this.Page, typeof(string), "Alert", "showAlert('ส่งผลการประเมินไปยังผู้ประเมินแล้ว');", true);
			BindMasterCOMP();

		}

		protected void ddlRound_SelectedIndexChanged(object sender, System.EventArgs e)
		{
            //COMP_Status = BL.GetCOMPStatus(R_Year, R_Round, PSNL_NO);
            //BindPersonal();
            //BindAssessor();
            //BindMasterCOMP();

            //SetPrintButton();
            GoToSelfCOMP();
		}

        private void GoToSelfCOMP()
        {
            Session["ddlRound"] = ddlRound.SelectedIndex;
            Session["R_Year"] = R_Year;
            Session["R_Round"] = R_Round;

            int COMP_Status = BL.GetCOMPStatus(R_Year, R_Round, Session["USER_PSNL_NO"].ToString());
            if (COMP_Status < HRBL.AssessmentStatus.WaitCreatingApproved)
            {
                Response.Redirect("Assessment_self_COMP_Create.aspx");
            }
            else
            {
                Response.Redirect("Assessment_self_COMP_Assessment.aspx");
            }

        }

		private void SetPrintButton()
		{
			btnCOMPFormPDF.HRef = "Print/RPT_C_PNSAss.aspx?MODE=PDF&R_Year=" + R_Year + "&R_Round=" + R_Round + "&PSNL_No=" + PSNL_NO + "&Status=" + GL.CINT(COMP_Status);
            //btnCOMPFormExcel.HRef = "Print/RPT_C_PNSAss.aspx?MODE=EXCEL&R_Year=" + R_Year + "&R_Round=" + R_Round + "&PSNL_No=" + PSNL_NO + "&Status=" + GL.CINT(COMP_Status);
            btnCOMPFormExcel.HRef = "Print/RPT_C_PNSAss.aspx?MODE=EXCEL&R_Year=" + R_Year + "&R_Round=" + R_Round + "&PSNL_No=" + PSNL_NO + "&Status=" + COMP_Status;


			btnIDPFormPDF.HRef = "Print/RPT_IDP_PSN.aspx?MODE=PDF&R_Year=" + R_Year + "&R_Round=" + R_Round + "&PSNL_No=" + PSNL_NO + "&Status=" + GL.CINT(COMP_Status);
			btnIDPFormExcel.HRef = "Print/RPT_IDP_PSN.aspx?MODE=EXCEL&R_Year=" + R_Year + "&R_Round=" + R_Round + "&PSNL_No=" + PSNL_NO + "&Status=" + (COMP_Status);

            btnIDPResultFormPDF.HRef = "Print/RPT_IDP_PSN_Result.aspx?MODE=PDF&R_Year=" + R_Year + "&R_Round=" + R_Round + "&PSNL_No=" + PSNL_NO + "&Status=" + GL.CINT(COMP_Status);
            btnIDPResultFormExcel.HRef = "Print/RPT_IDP_PSN_Result.aspx?MODE=EXCEL&R_Year=" + R_Year + "&R_Round=" + R_Round + "&PSNL_No=" + PSNL_NO + "&Status=" + GL.CINT(COMP_Status);

        }


		private void SaveHeader()
		{
			//------------- Check Round ---------------
			if (R_Year == 0 | R_Round == 0)
				return;
			//------------- Check Progress-------------
            //----------- Prevent Save----------
            if (COMP_Status != HRBL.AssessmentStatus.WaitAssessment | BL.Is_Round_Completed(R_Year, R_Round))
                return;

            
			string SQL = "";
			SQL = " SELECT * \n";
			SQL += "  FROM tb_HR_COMP_Header\n";
			SQL += " WHERE PSNL_NO='" + PSNL_NO.Replace("'", "''") + "' AND R_Year=" + R_Year + " AND R_Round=" + R_Round;
			SqlDataAdapter DA = new SqlDataAdapter(SQL, BL.ConnectionString());
			DataTable DT = new DataTable();
			DA.Fill(DT);

			DataRow DR = null;
			if (DT.Rows.Count == 0) {
				DR = DT.NewRow();
				DR["PSNL_NO"] = PSNL_NO;
				DR["R_Year"] = R_Year;
				DR["R_Round"] = R_Round;
				DR["Create_By"] = Session["USER_PSNL_NO"];
				DR["Create_Time"] = DateAndTime.Now;
				DR["COMP_Status"] = 0;
			} else {
				DR = DT.Rows[0];
			}

			//------------- Round Detail------------- 
			SQL = "SELECT * FROM tb_HR_Round ";
			SQL += " WHERE R_Year=" + R_Year + " AND R_Round=" + R_Round;
			DataTable PN = new DataTable();
			SqlDataAdapter PA = new SqlDataAdapter(SQL, BL.ConnectionString());
			PA.Fill(PN);
			DR["R_Start"] = PN.Rows[0]["R_Start"];
			DR["R_End"] = PN.Rows[0]["R_End"];
			DR["R_Remark"] = "";

			//============ป้องกันการ update ตำแหน่ง ณ สิ้นรอบประเมินและปิดรอบประเมิน======

            Boolean IsInPeriod = BL.IsTimeInPeriod(DateTime.Now, R_Year, R_Round);
            Boolean IsRoundCompleted = BL.Is_Round_Completed(R_Year, R_Round);
            Boolean ckUpdate =  IsInPeriod & !IsRoundCompleted;
            if (ckUpdate | DT.Rows.Count == 0)

            {
                //------------- Personal Detail---------
                HRBL.PersonalInfo PSNInfo = BL.GetAssessmentPersonalInfo(R_Year, R_Round, PSNL_NO, COMP_Status);
                // Replace With New Updated Data If Exists
                if (!string.IsNullOrEmpty(PSNInfo.PSNL_No))
                {
                    if (!string.IsNullOrEmpty(PSNInfo.PSNL_No))
                        DR["PSNL_No"] = PSNInfo.PSNL_No;
                    else
                        DR["PSNL_No"] = DBNull.Value;
                    if (!string.IsNullOrEmpty(PSNInfo.PSNL_Fullname))
                        DR["PSNL_Fullname"] = PSNInfo.PSNL_Fullname;
                    else
                        DR["PSNL_Fullname"] = DBNull.Value;
                    if (!string.IsNullOrEmpty(PSNInfo.PNPS_CLASS))
                        DR["PNPS_CLASS"] = PSNInfo.PNPS_CLASS;
                    else
                        DR["PNPS_CLASS"] = DBNull.Value;
                    if (!string.IsNullOrEmpty(PSNInfo.PSNL_TYPE))
                        DR["PSNL_TYPE"] = PSNInfo.PSNL_TYPE;
                    else
                        DR["PSNL_TYPE"] = DBNull.Value;
                    if (!string.IsNullOrEmpty(PSNInfo.POS_NO))
                        DR["POS_NO"] = PSNInfo.POS_NO;
                    else
                        DR["POS_NO"] = DBNull.Value;
                    if (!string.IsNullOrEmpty(PSNInfo.WAGE_TYPE))
                        DR["WAGE_TYPE"] = PSNInfo.WAGE_TYPE;
                    else
                        DR["WAGE_TYPE"] = DBNull.Value;
                    if (!string.IsNullOrEmpty(PSNInfo.WAGE_NAME))
                        DR["WAGE_NAME"] = PSNInfo.WAGE_NAME;
                    else
                        DR["WAGE_NAME"] = DBNull.Value;
                    if (!string.IsNullOrEmpty(PSNInfo.SECTOR_CODE))
                        DR["SECTOR_CODE"] = Strings.Left(PSNInfo.SECTOR_CODE, 2);
                    else
                        DR["SECTOR_CODE"] = DBNull.Value;
                    if (!string.IsNullOrEmpty(PSNInfo.DEPT_CODE))
                        DR["DEPT_CODE"] = PSNInfo.DEPT_CODE;
                    else
                        DR["DEPT_CODE"] = DBNull.Value;
                    if (!string.IsNullOrEmpty(PSNInfo.MINOR_CODE))
                        DR["MINOR_CODE"] = PSNInfo.MINOR_CODE;
                    else
                        DR["MINOR_CODE"] = DBNull.Value;
                    if (!string.IsNullOrEmpty(PSNInfo.SECTOR_NAME))
                        DR["SECTOR_NAME"] = PSNInfo.SECTOR_NAME;
                    else
                        DR["SECTOR_NAME"] = DBNull.Value;
                    if (!string.IsNullOrEmpty(PSNInfo.DEPT_NAME))
                        DR["DEPT_NAME"] = PSNInfo.DEPT_NAME;
                    else
                        DR["DEPT_NAME"] = DBNull.Value;
                    if (!string.IsNullOrEmpty(PSNInfo.FN_ID))
                        DR["FN_ID"] = PSNInfo.FN_ID;
                    else
                        DR["FN_ID"] = DBNull.Value;
                    if (!string.IsNullOrEmpty(PSNInfo.FLD_Name))
                        DR["FLD_Name"] = PSNInfo.FLD_Name;
                    else
                        DR["FLD_Name"] = DBNull.Value;
                    if (!string.IsNullOrEmpty(PSNInfo.FN_CODE))
                        DR["FN_CODE"] = PSNInfo.FN_CODE;
                    else
                        DR["FN_CODE"] = DBNull.Value;
                    if (!string.IsNullOrEmpty(PSNInfo.FN_NAME))
                        DR["FN_NAME"] = PSNInfo.FN_NAME;
                    else
                        DR["FN_NAME"] = DBNull.Value;
                    if (!string.IsNullOrEmpty(PSNInfo.MGR_CODE))
                        DR["MGR_CODE"] = PSNInfo.MGR_CODE;
                    else
                        DR["MGR_CODE"] = DBNull.Value;
                    if (!string.IsNullOrEmpty(PSNInfo.MGR_NAME))
                        DR["MGR_NAME"] = PSNInfo.MGR_NAME;
                    else
                        DR["MGR_NAME"] = DBNull.Value;
                }


                //------------- Assign Assessor Only-----------------------------
                DR["Create_Commit_By"] = ASSESSOR_BY;
                DR["Create_Commit_Name"] = ASSESSOR_NAME;
                DR["Create_Commit_DEPT"] = ASSESSOR_DEPT;
                DR["Create_Commit_POS"] = ASSESSOR_POS;
                DR["Assessment_Commit_By_MGR"] = ASSESSOR_BY;
                DR["Assessment_Commit_Name"] = ASSESSOR_NAME;
                DR["Assessment_Commit_DEPT"] = ASSESSOR_DEPT;
                DR["Assessment_Commit_POS"] = ASSESSOR_POS;
            }
			DR["Update_By"] = Session["USER_PSNL_NO"];
			DR["Update_Time"] = DateAndTime.Now;

			if (DT.Rows.Count == 0)
				DT.Rows.Add(DR);
			SqlCommandBuilder cmd = new SqlCommandBuilder(DA);
			DA.Update(DT);

			DataTable TMP = BL.GetAssessorList(R_Year, R_Round, PSNL_NO);
			TMP.DefaultView.RowFilter = "MGR_PSNL_NO='" + ASSESSOR_BY.Replace("'", "''") + "'";

			//----------------- Update Information For History Assessor Structure ------------------
			SQL = "SELECT * ";
			SQL += " FROM tb_HR_Actual_Assessor ";
			SQL += " WHERE PSNL_NO='" + PSNL_NO + "' AND R_Year=" + R_Year + " AND R_Round=" + R_Round;
			DA = new SqlDataAdapter(SQL, BL.ConnectionString());
			DT = new DataTable();
			DA.Fill(DT);

			if (DT.Rows.Count > 0) {
				DT.Rows[0].Delete();
				cmd = new SqlCommandBuilder(DA);
				DA.Update(DT);
			}

            //--เพิ่ม Check การอัพเดท--//
            if (ckUpdate | DT.Rows.Count == 0)
            {

                if (TMP.DefaultView.Count > 0)
                {
                    DR = DT.NewRow();
                    DR["PSNL_NO"] = PSNL_NO;
                    DR["R_Year"] = R_Year;
                    DR["R_Round"] = R_Round;
                    DR["MGR_ASSESSOR_POS"] = TMP.DefaultView[0]["ASSESSOR_POS"];
                    DR["MGR_PSNL_NO"] = TMP.DefaultView[0]["MGR_PSNL_NO"];
                    DR["MGR_PSNL_Fullname"] = TMP.DefaultView[0]["MGR_PSNL_Fullname"];
                    DR["MGR_PNPS_CLASS"] = TMP.DefaultView[0]["MGR_CLASS"];
                    DR["MGR_PSNL_TYPE"] = TMP.DefaultView[0]["MGR_WAGE_TYPE"];
                    DR["MGR_POS_NO"] = TMP.DefaultView[0]["MGR_POS_NO"];
                    DR["MGR_WAGE_TYPE"] = TMP.DefaultView[0]["MGR_WAGE_TYPE"];
                    DR["MGR_WAGE_NAME"] = TMP.DefaultView[0]["MGR_WAGE_NAME"];
                    DR["MGR_DEPT_CODE"] = TMP.DefaultView[0]["MGR_DEPT_CODE"];
                    //DR("MGR_MINOR_CODE") = TMP.DefaultView(0).Item("MGR_MINOR_CODE")
                    DR["MGR_SECTOR_CODE"] = TMP.DefaultView[0]["MGR_SECTOR_CODE"].ToString().Substring(0, 2);
                    DR["MGR_SECTOR_NAME"] = TMP.DefaultView[0]["MGR_SECTOR_NAME"];
                    DR["MGR_DEPT_NAME"] = TMP.DefaultView[0]["MGR_DEPT_NAME"];
                    //DR("MGR_FN_ID") = TMP.DefaultView(0).Item("MGR_FN_ID")
                    //DR("MGR_FLD_Name") = TMP.DefaultView(0).Item("MGR_FLD_Name")
                    DR["MGR_FN_CODE"] = TMP.DefaultView[0]["MGR_FN_CODE"];
                    //DR("MGR_FN_TYPE") = TMP.DefaultView(0).Item("MGR_FN_TYPE")
                    DR["MGR_FN_NAME"] = TMP.DefaultView[0]["MGR_FN_NAME"];
                    if (!GL.IsEqualNull(TMP.DefaultView[0]["MGR_MGR_CODE"]))
                    {
                        DR["MGR_MGR_CODE"] = TMP.DefaultView[0]["MGR_MGR_CODE"];
                    }
                    if (!GL.IsEqualNull(TMP.DefaultView[0]["MGR_MGR_NAME"]))
                    {
                        DR["MGR_MGR_NAME"] = TMP.DefaultView[0]["MGR_MGR_NAME"];
                    }
                    try
                    {
                        DR["MGR_PNPS_RETIRE_DATE"] = (BL.GetPSNRetireDate(TMP.DefaultView[0]["MGR_PSNL_NO"].ToString()));
                    }
                    catch
                    {
                    }
                    DR["Update_By"] = Session["USER_PSNL_NO"];
                    DR["Update_Time"] = DateAndTime.Now;
                    DT.Rows.Add(DR);
                    cmd = new SqlCommandBuilder(DA);
                    DA.Update(DT);
                }
            }

			//----------------- Update Information For History PSN Structure ------------------
			DataTable PSN = new DataTable();
			SQL = "SELECT * FROM vw_PN_PSNL WHERE PSNL_NO='" + PSNL_NO.Replace("'", "''") + "'";
			DA = new SqlDataAdapter(SQL, BL.ConnectionString());
			DA.Fill(PSN);

			SQL = "SELECT * ";
			SQL += " FROM tb_HR_Assessment_PSN ";
			SQL += " WHERE PSNL_NO='" + PSNL_NO.Replace("'", "''") + "' AND R_Year=" + R_Year + " AND R_Round=" + R_Round;
			DA = new SqlDataAdapter(SQL, BL.ConnectionString());
			DT = new DataTable();
			DA.Fill(DT);


			if (PSN.Rows.Count > 0) {
				if (DT.Rows.Count == 0) {
					DR = DT.NewRow();
					DR["PSNL_NO"] = PSNL_NO;
					DR["R_Year"] = R_Year;
					DR["R_Round"] = R_Round;
					
					DR["PSN_PSNL_NO"] = PSNL_NO;
					DR["PSN_PSNL_Fullname"] = PSN.Rows[0]["PSNL_Fullname"].ToString();
					DT.Rows.Add(DR);
				} else {
					DR = DT.Rows[0];
				}
               	//============ป้องกันการ update ตำแหน่ง ณ สิ้นรอบประเมินและปิดรอบประเมิน======
                if (ckUpdate | DT.Rows.Count == 0 | string.IsNullOrEmpty(DT.Rows[0]["PSN_PNPS_CLASS"].ToString()))

                {

                    //--เพิ่ม Check การอัพเดท--//
                    if (TMP.DefaultView.Count > 0) {
						try {
							DR["PSN_ASSESSOR_POS"] = TMP.DefaultView[0]["ASSESSOR_POS"];
						} catch {
						}
					}


                    DR["PSN_PNPS_CLASS"] = PSN.Rows[0]["PNPS_CLASS"].ToString();
                    DR["PSN_PSNL_TYPE"] = PSN.Rows[0]["PSNL_TYPE"].ToString();
                    DR["PSN_POS_NO"] = PSN.Rows[0]["POS_NO"].ToString();
                    DR["PSN_WAGE_TYPE"] = PSN.Rows[0]["WAGE_TYPE"].ToString();
                    DR["PSN_WAGE_NAME"] = PSN.Rows[0]["WAGE_NAME"].ToString();
                    DR["PSN_DEPT_CODE"] = PSN.Rows[0]["DEPT_CODE"].ToString();
                    DR["PSN_MINOR_CODE"] = PSN.Rows[0]["MINOR_CODE"].ToString();
                    DR["PSN_SECTOR_CODE"] = Strings.Left(PSN.Rows[0]["SECTOR_CODE"].ToString(), 2);
                    DR["PSN_SECTOR_NAME"] = PSN.Rows[0]["SECTOR_NAME"].ToString();
                    DR["PSN_DEPT_NAME"] = PSN.Rows[0]["DEPT_NAME"].ToString();
                    DR["PSN_FN_ID"] = PSN.Rows[0]["FN_ID"].ToString();
                    DR["PSN_FLD_Name"] = PSN.Rows[0]["FLD_Name"].ToString();
                    DR["PSN_FN_CODE"] = PSN.Rows[0]["FN_CODE"].ToString();
                    DR["PSN_FN_TYPE"] = PSN.Rows[0]["FN_TYPE"].ToString();
                    DR["PSN_FN_NAME"] = PSN.Rows[0]["FN_NAME"].ToString();
                    if (!GL.IsEqualNull(PSN.Rows[0]["MGR_CODE"]))
                    {
                        DR["PSN_MGR_CODE"] = PSN.Rows[0]["MGR_CODE"];
                    }
                    if (!GL.IsEqualNull(PSN.Rows[0]["MGR_NAME"]))
                    {
                        DR["PSN_MGR_NAME"] = PSN.Rows[0]["MGR_NAME"];
                    }
                }
				try {
					DR["PSN_PNPS_RETIRE_DATE"] = BL.GetPSNRetireDate(PSNL_NO);
				} catch {
				}
				DR["Update_By"] = Session["USER_PSNL_NO"];
				DR["Update_Time"] = DateAndTime.Now;
				cmd = new SqlCommandBuilder(DA);
				DA.Update(DT);
				}		

		}
		public Assessment_self_COMP_Assessment()
		{
			Load += Page_Load;
		}


        private DataTable Current_Remark()
        {
            DataTable DT = new DataTable();
            DataRow DR = null;
            DT.Columns.Add("COMP_Type_Id");
            DT.Columns.Add("Master_No");
            DT.Columns.Add("COMP_Remark_PSN", typeof(string));
            DT.Columns.Add("COMP_Type_Name", typeof(string));

            foreach (RepeaterItem Item in rptAss .Items)
            {
                HtmlTableCell cell_COMP_type = (HtmlTableCell) Item.FindControl("cell_COMP_type");
                HtmlTableCell ass_no = (HtmlTableCell) Item.FindControl("ass_no");
                	TextBox txtPSN = (TextBox) Item.FindControl("txtPSN");

                    if (txtPSN.Text != "")
                    {
                        if (txtPSN.Text.Length > 3990)
                        {
                            txtPSN.Text = txtPSN.Text.Substring(0, 3990);
                        }

                    }
                
                    DR = DT.NewRow();
                    DR["COMP_Type_Id"] = ass_no.Attributes["COMP_Type_Id"];
                    DR["Master_No"] = ass_no.InnerHtml;
                    DR["COMP_Remark_PSN"] = txtPSN.Text;
                    DR["COMP_Type_Name"] = cell_COMP_type.InnerHtml;
                    DT.Rows.Add(DR);                 
            }
            return DT;
        }
        protected void btnSaveRemark_Click(object sender, System.EventArgs e)
        {

            try
            {
                SaveRemark();
            }
            catch
            {
                ScriptManager.RegisterStartupScript(this.Page, typeof(string), "Alert", "showAlert('ไม่สามารถบันทึกได้');", true);
            }         
             

            ScriptManager.RegisterStartupScript(this.Page, typeof(string), "Alert", "showAlert('บันทึกแล้ว');", true);
            BindMasterCOMP();

        }



        private void SaveRemark()
        {
            DataTable DT = Current_Remark();
            for (int i = 0; i <= DT.Rows.Count - 1; i++)
            {

                string SQL_Remark = "SELECT * FROM tb_HR_COMP_Detail \n";
                SQL_Remark += " WHERE PSNL_NO='" + PSNL_NO.Replace("'", "''") + "' ";
                SQL_Remark += " AND R_Year=" + R_Year + "\n";
                SQL_Remark += " AND R_Round=" + R_Round + "\n";
                SQL_Remark += " AND COMP_Type_Id=" + DT.Rows[i]["COMP_Type_Id"] + "\n";
                SQL_Remark += " AND Master_No=" + DT.Rows[i]["Master_No"] + "\n";

                SqlDataAdapter DA = new SqlDataAdapter(SQL_Remark, BL.ConnectionString());
                DataTable DT_Remark = new DataTable();
                DA.Fill(DT_Remark);

                if (DT_Remark.Rows.Count == 0)
                    return;

                DT_Remark.Rows[0]["COMP_Remark_PSN"] = DT.Rows[i]["COMP_Remark_PSN"].ToString();
                DT_Remark.Rows[0]["Update_By"] = Session["USER_PSNL_NO"];
                DT_Remark.Rows[0]["Update_Time"] = DateAndTime.Now;

                SqlCommandBuilder cmd = new SqlCommandBuilder(DA);
DA.Update(DT_Remark);
               

            }
        
        }

	}
}

﻿<%@ Page  Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPage.Master" CodeBehind="AssessmentSetting_OrganizePeriod.aspx.cs" Inherits="VB.AssessmentSetting_OrganizePeriod" %>

<%@ Register src="WUC_DateReporter.ascx" tagname="WUC_DateReporter" tagprefix="uc1" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<%@ Register src="WUC_DateTimePicker.ascx" tagname="WUC_DateTimePicker" tagprefix="asp" %>
<%@ Register src="PageNavigation.ascx" tagname="PageNavigation" tagprefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<asp:UpdatePanel ID="UpdatePanel1" runat="server">      
<ContentTemplate>
<asp:ToolkitScriptManager ID="toolkit1" runat="server"></asp:ToolkitScriptManager>

<div class="container-fluid">
				<!-- BEGIN PAGE HEADER-->
				<div class="row-fluid" id="fixedPageHeader">
					<div class="span12"> 
						<!-- BEGIN PAGE TITLE & BREADCRUMB-->
						<h3 class="page-title">ระยะเวลาการประเมินแต่ละหน่วยงาน <font color="blue">(ScreenID : S-HDR-02)</font></h3>
						
									
						<ul class="breadcrumb">
                            <li>
                            	<i class="icon-th-list"></i><a href="javascript:;">การกำหนดข้อมูลการประเมิน</a><i class="icon-angle-right"></i>
                            </li>
                        	<li><i class="icon-time"></i> <a href="javascript:;">ระยะเวลาการประเมินแต่ละหน่วยงาน</a></li>
                        	
                            <uc1:WUC_DateReporter ID="WUC_DateReporter1" runat="server" />
                        </ul>
						<!-- END PAGE TITLE & BREADCRUMB-->
									        
				     </div>

				
			    </div>
		        <!-- END PAGE HEADER-->
		        
		    <asp:Panel ID="pnlList" runat="server" Visible="True" DefaultButton="btnSearch">
			<div class="row-fluid">
					
					<div class="span12">
						<!-- BEGIN SAMPLE TABLE PORTLET-->
						<div class="portlet box">
							
							
							<h4 class="control-label"> <i class="icon-retweet"></i> สำหรับรอบการประเมิน
							        <asp:DropDownList ID="ddlSearchRound" OnSelectedIndexChanged="btnSearch_Click" runat="server" AutoPostBack="true" CssClass="small m-wrap" style="margin-top:5px;margin-left:10px;">
							        </asp:DropDownList>
						            &nbsp; &nbsp;
						            ชื่อหน่วยงาน
						            <asp:TextBox ID="txtSearchDept" OnTextChanged="btnSearch_Click" AutoPostBack="true" runat="server" CssClass="m-wrap large" style=" background-color:White; margin-top:5px;margin-left:10px;" placeholder="กรอกคำค้นชื่อหน่วยงาน"></asp:TextBox>
						            
						            <asp:CheckBox ID="chkAssigned" OnCheckedChanged="btnSearch_Click" runat="server" Text="" AutoPostBack="true" /> แสดงเฉพาะที่มีการปรับระยะเวลา
						            <asp:Button ID="btnSearch" OnClick="btnSearch_Click" runat="server" Style="display:none" />
							 </h4>
							   
							<div class="portlet-body no-more-tables">
								<table class="table table-full-width dataTable no-more-tables table-hover">
									<thead>
										
										
										<tr>
											<th rowspan="3"><i class="icon-sitemap"></i> ฝ่าย</th>
											<th rowspan="3"><i class="icon-sitemap"></i> หน่วยงาน</th>
											<th colspan="4" style="text-align:center;"><i class="icon-th-list"></i> การประเมินผลตัวชี้วัด</th>
											<th colspan="4" style="text-align:center;"><i class="icon-th-list"></i> การประเมินผลสมรรถนะ</th>
											<th rowspan="3"><i class="icon-bolt"></i> สถานะ/ดำเนินการ</th>
											
										</tr>
										
										<tr>
										 	<th colspan="2" style="text-align:center;"><i class="icon-edit"></i> กำหนดแบบประเมิน</th>
											<th colspan="2" style="text-align:center;"><i class="icon-check"></i> ประเมินผล</th>
											<th colspan="2" style="text-align:center;"><i class="icon-edit"></i> กำหนดแบบประเมิน</th>
											<th colspan="2" style="text-align:center;"><i class="icon-check"></i> ประเมินผล</th>										
										</tr>
										
										<tr>											
											<th><i class="icon-time"></i> เริ่ม</th>
											<th><i class="icon-time"></i> ถึง</th>
											<th><i class="icon-time"></i> เริ่ม</th>
											<th><i class="icon-time"></i> ถึง</th>
											<th><i class="icon-time"></i> เริ่ม</th>
											<th><i class="icon-time"></i> ถึง</th>
											<th><i class="icon-time"></i> เริ่ม</th>
											<th><i class="icon-time"></i> ถึง</th>
										</tr>
									</thead>
									<tbody>
										<asp:Label ID="lblCountList" runat="server" Font-Size="14px" Font-Bold="true"></asp:Label>
								        <asp:Repeater ID="rptPeriod" OnItemCommand="rptPeriod_ItemCommand" OnItemDataBound="rptPeriod_ItemDataBound" runat="server">
										    <ItemTemplate>
    										        <tr>
											            <td class="highlight" data-title="ฝ่าย"><asp:Label ID="lbl_Sector_Name" runat="server"></asp:Label></td>
											            <td class="highlight" data-title="หน่วยงาน"><asp:Label ID="lbl_Dept_Name" runat="server"></asp:Label></td>
											            <td data-title="เริ่ม"><asp:Label ID="lbl_KPI_CP_Start" runat="server"></asp:Label></td>
											            <td data-title="ถึง"><asp:Label ID="lbl_KPI_CP_End" runat="server"></asp:Label></td>
											            <td data-title="เริ่ม"><asp:Label ID="lbl_KPI_AP_Start" runat="server"></asp:Label></td>
											            <td data-title="ถึง"><asp:Label ID="lbl_KPI_AP_End" runat="server"></asp:Label></td>
											            <td data-title="เริ่ม"><asp:Label ID="lbl_COMP_CP_Start" runat="server"></asp:Label></td>
											            <td data-title="ถึง"><asp:Label ID="lbl_COMP_CP_End" runat="server"></asp:Label></td>
											            <td data-title="เริ่ม"><asp:Label ID="lbl_COMP_AP_Start" runat="server"></asp:Label></td>
											            <td data-title="ถึง"><asp:Label ID="lbl_COMP_AP_End" runat="server"></asp:Label></td>
											            <td data-title="ดำเนินการ"><asp:LinkButton ID="lnkAdjust" runat="server" CssClass="btn mini blue"><i class="icon-bolt"></i> ปรับเปลี่ยนเวลา</asp:LinkButton></td>
										             </tr>
										    </ItemTemplate>
										</asp:Repeater>										
									</tbody>
								</table>
								
								<asp:PageNavigation ID="Pager" OnPageChanging="Pager_PageChanging" MaximunPageCount="5" PageSize="20" runat="server" />
							</div>
						</div>
						<!-- END SAMPLE TABLE PORTLET-->
						
					</div>
             </div>              
            </asp:Panel>
            <asp:Panel ID="pnlEdit" runat="server" Visible="False">
            <div class="row-fluid">
                <div class="span12">
                  						<div class="portlet-body form">
												<h3 class="form-section"><i class="icon-retweet"></i> ระยะเวลาการประเมินสำหรับ : 
												    <asp:Label ID="lblEditDept" runat="server"></asp:Label>
												    											    
												    <asp:Label ID="lblEditRound" runat="server"></asp:Label>
												</h3>
												<h4 class="form-section"><i class="icon-time"></i> การประเมินผลตัวชี้วัด (KPI)</h4>
												<div class="row-fluid form-horizontal">
												    <div class="span4 ">
														<div class="control-group">
													        <label class="control-label">กำหนดแบบประเมิน</label>
													        <div class="controls">
														        <asp:WUC_DateTimePicker ID="dtp_KPI_CP_Start" runat="server" />												                   
													        </div>
												        </div>
													</div>
													<div class="span4 ">
														<div class="control-group">
													        <label class="control-label">ถึงวันที่</label>
													        <div class="controls">
														        <asp:WUC_DateTimePicker ID="dtp_KPI_CP_End" runat="server" />
												                   
													        </div>
												        </div>
													</div>
												</div>
												<div class="row-fluid form-horizontal">
												    <div class="span4 ">
														<div class="control-group">
													        <label class="control-label">พนักงานประเมินตนเอง</label>
													        <div class="controls">
														        <asp:WUC_DateTimePicker ID="dtp_KPI_AP_Start" runat="server" />												                   
													        </div>
												        </div>
													</div>
													<div class="span4 ">
														<div class="control-group">
													        <label class="control-label">ถึงวันที่</label>
													        <div class="controls">
														        <asp:WUC_DateTimePicker ID="dtp_KPI_AP_End" runat="server" />												                   
													        </div>
												        </div>
													</div>
												</div>
												<div class="row-fluid form-horizontal">
												    <div class="span4 ">
														<div class="control-group">
													        <label class="control-label">ผู้บังคับบัญชาประเมิน</label>
													        <div class="controls">
														        <asp:WUC_DateTimePicker ID="dtp_KPI_MGR_Start" runat="server" />												                   
													        </div>
												        </div>
													</div>
													<div class="span4 ">
														<div class="control-group">
													        <label class="control-label">ถึงวันที่</label>
													        <div class="controls">
														        <asp:WUC_DateTimePicker ID="dtp_KPI_MGR_End" runat="server" />												                   
													        </div>
												        </div>
													</div>
												</div>
												<h4 class="form-section"><i class="icon-time"></i> การประเมินผลสมรรถนะ (Competency)</h4>
												<div class="row-fluid form-horizontal">
												    <div class="span4 ">
														<div class="control-group">
													        <label class="control-label">กำหนดแบบประเมิน</label>
													        <div class="controls">
														        <asp:WUC_DateTimePicker ID="dtp_COMP_CP_Start" runat="server" />
													        </div>
												        </div>
													</div>
													<div class="span4 ">
														<div class="control-group">
													        <label class="control-label">ถึงวันที่</label>
													        <div class="controls">
														        <asp:WUC_DateTimePicker ID="dtp_COMP_CP_End" runat="server" />
													        </div>
												        </div>
													</div>
												</div>
												<div class="row-fluid form-horizontal">
												    <div class="span4 ">
														<div class="control-group">
													        <label class="control-label">พนักงานประเมินตนเอง</label>
													        <div class="controls">
														        <asp:WUC_DateTimePicker ID="dtp_COMP_AP_Start" runat="server" />
													        </div>
												        </div>
													</div>
													<div class="span4 ">
														<div class="control-group">
													        <label class="control-label">ถึงวันที่</label>
													        <div class="controls">
														        <asp:WUC_DateTimePicker ID="dtp_COMP_AP_End" runat="server" /> 
													        </div>
												        </div>
													</div>
												</div>
												<div class="row-fluid form-horizontal">
												    <div class="span4 ">
														<div class="control-group">
													        <label class="control-label">ผู้บังคับบัญชาประเมิน</label>
													        <div class="controls">
														        <asp:WUC_DateTimePicker ID="dtp_COMP_MGR_Start" runat="server" />
													        </div>
												        </div>
													</div>
													<div class="span4 ">
														<div class="control-group">
													        <label class="control-label">ถึงวันที่</label>
													        <div class="controls">
														        <asp:WUC_DateTimePicker ID="dtp_COMP_MGR_End" runat="server" />
													        </div>
												        </div>
													</div>
												</div>
											<div class="form-actions">
											    <asp:Button CssClass="btn red" runat="server" ID="btnSave" OnClick="btnSave_Click" Text="ยืนยันปรับเวลา" />
											    <asp:Button CssClass="btn blue" runat="server" ID="btnDefault" OnClick="btnDefault_Click" Text="ปรับเป็นเวลาตามปกติ" />
											    <asp:Button CssClass="btn" runat="server" ID="btnCancel" OnClick="btnCancel_Click" Text="ย้อนกลับ" />
											    <asp:ConfirmButtonExtender ID="cfmbtnSave" ConfirmText="ยืนยันปรับเวลา" TargetControlID="btnSave" runat="server" ></asp:ConfirmButtonExtender>
											    <asp:ConfirmButtonExtender ID="cfb_Default" runat="server" TargetControlID="btnDefault" ConfirmText="ยืนยันปรับเวลาตามรอบปกติ"></asp:ConfirmButtonExtender>								    
									        </div>		
										</div>
						</div>               
            </div>
            </asp:Panel>           
</div>
</ContentTemplate>           
</asp:UpdatePanel>
</asp:Content>



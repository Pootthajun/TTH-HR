﻿<%@ Page  Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPage.Master" CodeBehind="AssessmentSetting_KPI_PSN.aspx.cs" Inherits="VB.AssessmentSetting_KPI_PSN" %>

<%@ Register src="WUC_DateReporter.ascx" tagname="WUC_DateReporter" tagprefix="uc1" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<%@ Register src="PageNavigation.ascx" tagname="PageNavigation" tagprefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<asp:ToolkitScriptManager ID="toolkit1" runat="server"></asp:ToolkitScriptManager>
<asp:UpdatePanel ID="UpdatePanel1" runat="server">
<ContentTemplate>

<div class="container-fluid">
				<!-- BEGIN PAGE HEADER-->
				<div class="row-fluid" id="fixedPageHeader">
					<div class="span12"> 
						<!-- BEGIN PAGE TITLE & BREADCRUMB-->
						<h3 class="page-user-md">การประเมินผลตัวชี้วัดรายบุคคล (KPI) <font color="blue">(ScreenID : S-HDR-08)</font>
                        <asp:Label ID="lblReportTime" Font-Size="14px" ForeColor="Green" runat="Server"></asp:Label>
                        </h3>					
									
						<ul class="breadcrumb">
                            
                        	<li>
                        	    <i class="icon-user"></i> <a href="javascript:;">การประเมินพนักงานรายบุคคล</a><i class="icon-angle-right"></i>
                        	</li>
                        	<li>
                        	    <i class="icon-th-list"></i> <a href="javascript:;">การประเมินผลตัวชี้วัด (KPI)</a>
                        	</li>
                        	<uc1:WUC_DateReporter ID="WUC_DateReporter1" runat="server" />
                        </ul>
						<!-- END PAGE TITLE & BREADCRUMB-->
									        
				     </div>

				
			    </div>
		        <!-- END PAGE HEADER-->
				
				<asp:Panel ID="pnlList" runat="server" Visible="True">
                    <div class="row-fluid">
					
					    <div class="span12">
						    <!-- BEGIN SAMPLE TABLE PORTLET-->
						
							
							  
							                   	<asp:Panel CssClass="row-fluid form-horizontal" ID="pnlSearch" runat="server" DefaultButton="btnSearch">
												     
												     <div class="row-fluid form-horizontal">
												          <div class="span4 ">
														    <div class="control-group">
													            <label class="control-label"><i class="icon-retweet"></i> สำหรับรอบการประเมิน</label>
													            <div class="controls">
														            <asp:DropDownList ID="ddlRound" OnSelectedIndexChanged="Search" runat="server" AutoPostBack="true" CssClass="medium m-wrap">
														            </asp:DropDownList>
														            <asp:Button ID="btnSearch" OnClick="Search" runat="server" Style="display:none" />
														        </div>
												            </div>
													    </div>	
													    <div class="span6 ">
														    <div class="control-group">
													            <label class="control-label"><i class="icon-user"></i> ชื่อ</label>
													            <div class="controls">
														            <asp:TextBox ID="txt_Search_Name" OnTextChanged ="Search" runat="server" AutoPostBack="true" CssClass="m-wrap medium" placeholder="ค้นหาจากชื่อ/เลขประจำตัว/ตำแหน่ง"></asp:TextBox>
													            </div>
												            </div>
													    </div>	
												     </div>
												    <div class="row-fluid form-horizontal">
												        <div class="span4 ">
													        <div class="control-group">
													            <label class="control-label"><i class="icon-sitemap"></i> ฝ่าย</label>
													            <div class="controls">
														            <asp:DropDownList ID="ddlSector" OnSelectedIndexChanged="Search" runat="server" AutoPostBack="true" CssClass="medium m-wrap">
														            </asp:DropDownList>
														        </div>
												            </div>
													    </div>
													     <div class="span6 ">
														    <div class="control-group">
													            <label class="control-label"><i class="icon-sitemap"></i> ชื่อหน่วยงาน</label>
													            <div class="controls">
														            <asp:TextBox ID="txt_Search_Organize" OnTextChanged="Search" runat="server" AutoPostBack="true" CssClass="m-wrap medium" placeholder="ค้นหาจากหน่วยงาน/ตำแหน่ง"></asp:TextBox>
														        </div>
												            </div>
													    </div>													    
												    </div>
												    <div class="row-fluid form-horizontal">
												        <div class="span6 ">
														    <div class="control-group">
													            <label class="control-label"><i class="icon-bolt"></i> ความคืบหน้า</label>
													            <div class="controls">
														            <asp:DropDownList ID="ddlStatus" OnSelectedIndexChanged="Search" runat="server" AutoPostBack="true" CssClass="medium m-wrap">
														                <asp:ListItem Text="ทั้งหมด" Value="-1" Selected="true"></asp:ListItem>
														                <asp:ListItem Text="ยังไม่ส่งแบบประเมิน" Value="0"></asp:ListItem>
					                                                    <asp:ListItem Text="รออนุมัติแบบประเมิน" Value="1"></asp:ListItem>
					                                                    <asp:ListItem Text="อนุมัติแบบประเมินแล้ว" Value="2"></asp:ListItem>
					                                                    <asp:ListItem Text="พนักงานประเมินตนเอง" Value="3"></asp:ListItem>
					                                                    <asp:ListItem Text="รออนุมัติผลการประเมิน" Value="4"></asp:ListItem>
					                                                    <asp:ListItem Text="ประเมินเสร็จสมบูรณ์" Value="5"></asp:ListItem>
														            </asp:DropDownList>
														        </div>
												            </div>
													    </div>				
												    </div>
																																			   
												</asp:Panel>
								            
							<div class="portlet-body no-more-tables">
								<asp:Label ID="lblCountPSN" runat="server" Font-Size="14px" Font-Bold="true"></asp:Label>
								<table class="table table-full-width table-advance dataTable no-more-tables table-hover">
									<thead>
										<tr>
											<th><i class="icon-sitemap"></i> ฝ่าย</th>
											<th><i class="icon-sitemap"></i> หน่วยงาน</th>
											<th><i class="icon-user"></i> ชื่อ</th>
											<th><i class="icon-briefcase"></i> ตำแหน่ง</th>
											<th><i class="icon-bookmark"></i> ระดับ</th>
											<th><i class="icon-briefcase"></i> ประเภท</th>
											<th><i class="icon-user-md"></i> ผู้ประเมิน</th>
											<th><i class="icon-info"></i> สถานะ</th>
											<th><i class="icon-bolt"></i> ดำเนินการ</th>
										</tr>
									</thead>
									<tbody>
										<asp:Repeater ID="rptKPI" OnItemCommand="rptKPI_ItemCommand" OnItemDataBound="rptKPI_ItemDataBound" runat="server">
									        <ItemTemplate>
									            <tr>                                        
											        <td data-title="ฝ่าย"><asp:Label ID="lnkPSNSector" runat="server"></asp:Label></td>
											        <td data-title="หน่วยงาน"><asp:Label ID="lnkPSNDept" runat="server"></asp:Label></td>
											        <td data-title="ชื่อ"><asp:Label ID="lblPSNName" runat="server"></asp:Label></td>
											        <td data-title="ตำแหน่ง"><asp:Label ID="lblPSNPos" runat="server"></asp:Label></td>
											        <td data-title="ระดับ"><asp:Label ID="lblPSNClass" runat="server"></asp:Label></td>
											        <td data-title="ประเภท"><asp:Label ID="lblPSNType" runat="server"></asp:Label></td>
											        <td data-title="ผู้ประเมิน"><asp:Label ID="lblPSNASSESSOR" runat="server"></asp:Label></td>
											        <td data-title="สถานะ"><asp:Label ID="lblPSNKPIStatus" runat="server"></asp:Label></td>
											        <td data-title="ดำเนินการ">
											            <asp:Button ID="btnPSNEdit" runat="server" CssClass="btn mini blue" CommandName="Edit" Text="การประเมิน" />											           
										            </td>
										        </tr>	
									        </ItemTemplate>
										</asp:Repeater>
																							
									</tbody>
								</table>
								<asp:PageNavigation ID="Pager" OnPageChanging="Pager_PageChanging" MaximunPageCount="10" PageSize="20" runat="server" />
							</div>
						
						<!-- END TABLE PORTLET-->
						
					</div>
                </div>
              </asp:Panel>
  
              <asp:Panel ID="pnlEdit" runat="server" Visible="False">              
				          
			                
    			                <div class="form-horizontal form-view">
    			                
    			                    <div class="btn-group pull-left">
                                         <asp:Button ID="btnBack_TOP" OnClick="btnBack_Click" runat="server" CssClass="btn" Text="ย้อนกลับ" />
                                    </div>
    			                    <div class="btn-group pull-right">                                    
			                                <a data-toggle="dropdown" class="btn dropdown-toggle">พิมพ์ <i class="icon-angle-down"></i></a>										
			                                <ul class="dropdown-menu pull-right">                                                       
                                                <li><a id="btnPDF" runat="server" target="_blank">รูปแบบ PDF</a></li>
                                                <li><a id="btnExcel" runat="server" target="_blank">รูปแบบ Excel</a></li>											
			                                </ul>
		                             </div>   
    			                    
                			        <h3 style="text-align:center; margin-top:0px;">
					                  สถานะความคืบหน้า (KPI) <asp:Label ID="lblRound" runat="server"></asp:Label> : 
					                  <asp:DropDownList ID="ddlKPIStatus" OnSelectedIndexChanged="ddlKPIStatus_SelectedIndexChanged" runat="server" AutoPostBack="true"
					                  style="background-color:White; border:none; color:Black; font-size:24px; padding-top:0px; height:35px; width:280px;">
					                    <asp:ListItem Text="ไม่พบแบบประเมิน" Value="-1"></asp:ListItem>
					                    <asp:ListItem Text="ยังไม่ส่งแบบประเมิน" Value="0"></asp:ListItem>
					                    <asp:ListItem Text="รออนุมัติแบบประเมิน" Value="1"></asp:ListItem>
					                    <asp:ListItem Text="อนุมัติแบบประเมินแล้ว" Value="2"></asp:ListItem>
					                    <asp:ListItem Text="พนักงานประเมินตนเอง" Value="3"></asp:ListItem>
					                    <asp:ListItem Text="รออนุมัติผลการประเมิน" Value="4"></asp:ListItem>
					                    <asp:ListItem Text="ประเมินเสร็จสมบูรณ์" Value="5"></asp:ListItem>
					                  </asp:DropDownList>
					                  <font style="font-style:italic; color:Silver;">(เลือกเพื่อปรับสถานะ)</font>
					                </h3>  
    			                   
    			                     <div class="row-fluid" style="border-bottom:1px solid #EEEEEE;">
					                        <div class="span4 ">
					                            <div class="control-group">
								                    <label class="control-label" style="width:100px;">ผู้รับการประเมิน :</label>
								                    <div class="controls" style="margin-left: 120px;">
									                    <asp:Label ID="lblPSNName" runat="server" CssClass="text bold"></asp:Label>
								                    </div>
							                    </div>
					                        </div>
					                        <div class="span3 ">
					                            <div class="control-group">
								                    <label class="control-label" style="width:100px;">ตำแหน่ง :</label>
								                    <div class="controls" style="margin-left: 120px;">
									                    <asp:Label ID="lblPSNPos" runat="server" CssClass="text bold"></asp:Label>
								                    </div>
							                    </div>
					                        </div>
					                        <div class="span5 ">
					                            <div class="control-group">
								                    <label class="control-label" style="width:100px;">สังกัด :</label>
								                    <div class="controls" style="margin-left: 120px;">
									                    <asp:Label ID="lblPSNDept" runat="server" CssClass="text bold"></asp:Label>
								                    </div>
							                    </div>
					                        </div>
					                     </div>
                					     
					                      <div class="row-fluid" style="margin-top:10px;">
					                        <div class="span4 ">
					                            <div class="control-group">
								                    <label class="control-label" style="width:100px;">ผู้ประเมิน :
								                    <asp:LinkButton ID="btn_Fixed_Assessor" OnClick="btn_Fixed_Assessor_Click" runat="server" CssClass="btn red mini"><i class="icon-screenshot"></i> เพิ่มเติม</asp:LinkButton>
								                    </label>
								                    <div class="controls" style="margin-left: 120px;">
									                    <asp:DropDownList ID="ddlASSName" OnSelectedIndexChanged="ddlASSName_SelectedIndexChanged" runat="Server" AutoPostBack="True" style="font-weight:bold; color:Black; border:none;">									        
									                    </asp:DropDownList>
									                    
									                </div>
							                    </div>
					                        </div>
					                        <div class="span3 ">
					                            <div class="control-group">
								                    <label class="control-label" style="width:100px;">ตำแหน่ง :</label>
								                    <div class="controls" style="margin-left: 120px;">
									                    <asp:Label ID="lblASSPos" runat="server" CssClass="text bold"></asp:Label>
								                    </div>
							                    </div>
					                        </div>
					                        <div class="span5 ">
					                            <div class="control-group">
								                    <label class="control-label" style="width:100px;">สังกัด :</label>
								                    <div class="controls" style="margin-left: 120px;">
									                    <asp:Label ID="lblASSDept" runat="server" CssClass="text bold"></asp:Label>
								                    </div>
							                    </div>
					                        </div>
					                     </div>
					                     
    			                </div>					
												
				                <!-- BEGIN FORM-->
				                <div class="portlet-body no-more-tables">				                
				                        <table class="table table-bordered no-more-tables" style="background-color:White;">
									        <thead>
										            <tr>
											            <th rowspan="2" style="text-align:center; background-color:Silver;"> ลำดับ</th>
											            <th rowspan="2" style="text-align:center; background-color:Silver;"> งาน</th>
											            <th rowspan="2" style="text-align:center; background-color:Silver;"> ตัวชี้วัดผลงาน</th>
											            <th colspan="5" style="text-align:center; background-color:Silver;"> คะแนนตามระดับค่าเป้าหมาย</th>
											            <th rowspan="2" style="text-align:center; background-color:Silver;"> คะแนน</th>
											            <th rowspan="2" style="text-align:center; background-color:Silver;"> น้ำหนัก(%)</th>											        
										            </tr>
                                                    <tr>
										              <th class="AssLevel1" style="text-align:center; font-weight:bold; vertical-align:middle;">1</th>
										              <th class="AssLevel2" style="text-align:center; font-weight:bold; vertical-align:middle;">2</th>
										              <th class="AssLevel3" style="text-align:center; font-weight:bold; vertical-align:middle;">3</th>
										              <th class="AssLevel4" style="text-align:center; font-weight:bold; vertical-align:middle;">4</th>
										              <th class="AssLevel5" style="text-align:center; font-weight:bold; vertical-align:middle;">5</th>
								                  </tr>
									        </thead>
									        <tbody>
									            <asp:Repeater ID="rptAss" OnItemCommand="rptAss_ItemCommand" OnItemDataBound="rptAss_ItemDataBound" runat="server">
									                <ItemTemplate>
									                <tr>
										              <td data-title="ลำดับ" rowspan="3" id="ass_no" runat="server" title="Click เพื่อแก้ไข" style="text-align:center; font-weight:bold; vertical-align:top; cursor:pointer;">001</td>
										              <td data-title="งาน" id="job" class="TextLevel0" runat="server" title="Click เพื่อแก้ไข" style="background-color:#f8f8f8; cursor:pointer; ">xxxxxxxxxxxx xxxxxxxxxxxx</td>
										              <td data-title="ตัวชี้วัดผลงาน" id="target" runat="server" title="Click เพื่อแก้ไข" class="TextLevel0" style="background-color:#f8f8f8; cursor:pointer; ">xxxxxxxxxxxx xxxxxxxxxxxx</td>
										              <td data-title="คลิกเพื่อประเมิน 1 คะแนน" title="Click เพื่อแก้ไข" id="choice1" class="TextLevel0" runat="server" style="cursor:pointer; background-color:White;">xxxxxxxxxxxx xxxxxxxxxxxx xxxxxxxxxxxx xxxxxxxxxxxx xxxxxxxxxxxx xxxxxxxxxxxx</td>
                                                      <td data-title="คลิกเพื่อประเมิน 2 คะแนน" title="Click เพื่อแก้ไข" id="choice2" class="TextLevel0" runat="server" style="cursor:pointer; background-color:White;">xxxxxxxxxxxx xxxxxxxxxxxx xxxxxxxxxxxx xxxxxxxxxxxx xxxxxxxxxxxx xxxxxxxxxxxx</td>
                                                      <td data-title="คลิกเพื่อประเมิน 3 คะแนน" title="Click เพื่อแก้ไข" id="choice3" class="TextLevel0" runat="server" style="cursor:pointer; background-color:White;">xxxxxxxxxxxx xxxxxxxxxxxx xxxxxxxxxxxx xxxxxxxxxxxx xxxxxxxxxxxx xxxxxxxxxxxx</td>
                                                      <td data-title="คลิกเพื่อประเมิน 4 คะแนน" title="Click เพื่อแก้ไข" id="choice4" class="TextLevel0" runat="server" style="cursor:pointer; background-color:White;">xxxxxxxxxxxx xxxxxxxxxxxx xxxxxxxxxxxx xxxxxxxxxxxx xxxxxxxxxxxx xxxxxxxxxxxx</td>
                                                      <td data-title="คลิกเพื่อประเมิน 5 คะแนน" title="Click เพื่อแก้ไข" id="choice5" class="TextLevel0" runat="server" style="cursor:pointer; background-color:White;">xxxxxxxxxxxx xxxxxxxxxxxx xxxxxxxxxxxx xxxxxxxxxxxx xxxxxxxxxxxx xxxxxxxxxxxx</td>
										              <td data-title="คะแนน" rowspan="5" title="คะแนน" id="mark" runat="server" style="text-align:center; font-size:16px; font-weight:bold; vertical-align:middle;">-</td>
										              <td data-title="น้ำหนัก" rowspan="5" title="Click เพื่อแก้ไข" id="weight" runat="server" style="text-align:center; font-size:16px; font-weight:bold; vertical-align:middle; cursor:pointer;">10%</td>
    										        </tr>
									                
									                <tr>
									                           
									                           <td colspan="2" data-title="หมายเหตุ (ผู้ถูกประเมิน)" style=" font:inherit; background-color:#f8f8f8; text-align:center; font-size:14px;">หมายเหตุ (ผู้ถูกประเมิน)</td>
									                           <td colspan="5" data-title="หมายเหตุ (ผู้ถูกประเมิน)" style="padding:0px;"> <asp:Textbox MaxLength="2000" ID="txtPSN"  runat="server" style="width:96%; padding:5px; background-color:Transparent; height:53px; border:0px none transparent;margin:0px;" TextMode="Multiline" Placeholder="ถ้ามี"></asp:Textbox></td>
									                </tr>
        									        <tr>
        									                    <td data-title="หมายเหตุ (ผู้ประเมิน)" colspan="2" style=" font:inherit; background-color:#f8f8f8; text-align:center; font-size:14px;">หมายเหตุ(ผู้ประเมิน)
									                                <asp:Button ID="btn_Remark_PSN" runat="server" style="display:none; " CommandName="Remark_PSN" />
									                                <asp:Button ID="btn_Remark_MGR" runat="server" style="display:none; " CommandName="Remark_MGR" />
									                                <asp:Button ID="btn_Edit" runat="server" style="display:none; " CommandName="Edit"  />
									                           </td>
									                           <td data-title="หมายเหตุ (ผู้ประเมิน)" colspan="5" style="padding:0px;"><asp:Textbox  MaxLength="2000" ID="txtMGR" runat="server" style="width:96%; padding:5px; background-color:Transparent; height:53px; border:0px none transparent;  margin:0px;" TextMode="Multiline" Placeholder="ถ้ามี"></asp:Textbox></td>
        									        </tr>
        									        
        									        
                                                    
									                <tr>
									                      <td rowspan="2" data-title="ลบ" id="cel_delete" runat="server" style="cursor:pointer; font-weight:bold; text-align:center; padding:0px !important; height:10px !important; background-color:#eeeeee;">
									                        ลบ<asp:Button ID="btn_Delete" runat="server" style="display:none;" CommandName="Delete" Text="" />
									                        <asp:ConfirmButtonExtender TargetControlID="btn_Delete" ID="cfm_Delete" runat="server"></asp:ConfirmButtonExtender>
									                      </td>
									                      <td id="cel_officer" runat="server" class="TextLevel0" colspan="2" style="text-align:center; font-weight:bold; padding:0px !important; height:10px !important;" >พนักงานประเมินตนเอง</td>
									                      <td data-title="คลิกเพื่อประเมิน 1 คะแนน" id="selOfficialColor1" runat="server" style="cursor:pointer; padding:0px !important; height:10px !important; border-right:0px none;"><asp:Button ID="btnOff_1" runat="server" style="display:none;" CommandName="SelectOff" /></td>
                                                          <td data-title="คลิกเพื่อประเมิน 2 คะแนน" id="selOfficialColor2" runat="server" style="cursor:pointer; padding:0px !important; height:10px !important; border-right:0px none; border-left:0px none;"><asp:Button ID="btnOff_2" runat="server" style="display:none;" CommandName="SelectOff" /></td>
                                                          <td data-title="คลิกเพื่อประเมิน 3 คะแนน" id="selOfficialColor3" runat="server" style="cursor:pointer; padding:0px !important; height:10px !important; border-right:0px none; border-left:0px none;"><asp:Button ID="btnOff_3" runat="server" style="display:none;" CommandName="SelectOff" /></td>
                                                          <td data-title="คลิกเพื่อประเมิน 4 คะแนน" id="selOfficialColor4" runat="server" style="cursor:pointer; padding:0px !important; height:10px !important; border-right:0px none; border-left:0px none;"><asp:Button ID="btnOff_4" runat="server" style="display:none;" CommandName="SelectOff" /></td>
                                                          <td data-title="คลิกเพื่อประเมิน 5 คะแนน" id="selOfficialColor5" runat="server" style="cursor:pointer; padding:0px !important; height:10px !important; border-left:0px none;"><asp:Button ID="btnOff_5" runat="server" style="display:none;" CommandName="SelectOff" /></td>
                                                    </tr>
									                <tr>
									                      <td id="cel_header" runat="server" class="TextLevel0" colspan="2" style="text-align:center; font-weight:bold; padding:0px !important; height:10px !important;" >ผู้บังคับบัญชาประเมิน</td>
									                      <td data-title="คลิกเพื่อประเมิน 1 คะแนน" id="selHeaderColor1" runat="server" style="cursor:pointer; padding:0px !important; height:10px !important; border-right:0px none;"><asp:Button ID="btnHead_1" runat="server" style="display:none;" CommandName="SelectHead" /></td>
                                                          <td data-title="คลิกเพื่อประเมิน 2 คะแนน" id="selHeaderColor2" runat="server" style="cursor:pointer; padding:0px !important; height:10px !important; border-right:0px none; border-left:0px none;"><asp:Button ID="btnHead_2" runat="server" style="display:none;" CommandName="SelectHead" /></td>
                                                          <td data-title="คลิกเพื่อประเมิน 3 คะแนน" id="selHeaderColor3" runat="server" style="cursor:pointer; padding:0px !important; height:10px !important; border-right:0px none; border-left:0px none;"><asp:Button ID="btnHead_3" runat="server" style="display:none;" CommandName="SelectHead" /></td>
                                                          <td data-title="คลิกเพื่อประเมิน 4 คะแนน" id="selHeaderColor4" runat="server" style="cursor:pointer; padding:0px !important; height:10px !important; border-right:0px none; border-left:0px none;"><asp:Button ID="btnHead_4" runat="server" style="display:none;" CommandName="SelectHead" /></td>
                                                          <td data-title="คลิกเพื่อประเมิน 5 คะแนน" id="selHeaderColor5" runat="server" style="cursor:pointer; padding:0px !important; height:10px !important; border-left:0px none;"><asp:Button ID="btnHead_5" runat="server" style="display:none;" CommandName="SelectHead" /></td>
                                                    </tr>
                                                   
									                </ItemTemplate>
									                <FooterTemplate>									                
									                  <tfoot>
									                        <tr>
									                            <td colspan="8" style="text-align:center; background-color:#EEEEEE; font-size:14px; font-weight:bold;">
									                            ผลรวมการประมิน
									                            </td>
									                            <td colspan="2" style="text-align:center; background-color:#EEEEEE; font-size:14px; font-weight:bold;">
									                            คะแนนที่ได้
									                            </td>
									                        </tr>
									                        <tr>
									                            <th colspan="3" style="text-align:center; background-color:#EEEEEE; border-bottom:1px solid #DDDDDD; font-size:14px; font-weight:bold;" id="cell_total" runat="server">
									                           รวม
									                            </th>
									                              <th id="cell_sum_1" runat="server" style="text-align:center; border-bottom:1px solid #DDDDDD;; font-size:14px; font-weight:bold; vertical-align:middle;">-</th>
										                          <th id="cell_sum_2" runat="server" style="text-align:center; border-bottom:1px solid #DDDDDD;; font-size:14px; font-weight:bold; vertical-align:middle;">-</th>
										                          <th id="cell_sum_3" runat="server" style="text-align:center; border-bottom:1px solid #DDDDDD;; font-size:14px; font-weight:bold; vertical-align:middle;">-</th>
										                          <th id="cell_sum_4" runat="server" style="text-align:center; border-bottom:1px solid #DDDDDD;; font-size:14px; font-weight:bold; vertical-align:middle;">-</th>
										                          <th id="cell_sum_5" runat="server" style="text-align:center; border-bottom:1px solid #DDDDDD;; font-size:14px; font-weight:bold; vertical-align:middle;">-</th>
								                                  <th id="cell_sum_raw" runat="server" style="text-align:center; border-bottom:1px solid #DDDDDD; font-size:16px; font-weight:bold;" colspan="2">-</th>
									                        </tr>
									                        <tr>
									                            <th colspan="8" style="background-color:#FFFFFF; border:none;"></th>
									                            <th style="text-align:center; background-color:#EEEEEE; font-size:14px; font-weight:bold;" colspan="2" >คิดเป็น</th>									                          
									                        </tr>
									                        <tr>
									                            <th colspan="8" style="background-color:#FFFFFF; border:none;"></th>
									                            <th id="cell_sum_mark" runat="server" style="text-align:center; font-size:24px; font-weight:bold;" colspan="2">-</th>									                          
									                        </tr>
									                    </tfoot>									                
									                </FooterTemplate>
									            </asp:Repeater>	
									        </tbody>
									        
								        </table>
								        
								        
						         <asp:Panel CssClass="form-actions" id="pnlActivity" runat="server">
						                    <asp:Button ID="btnAdd" OnClick="btnAdd_Click" runat="server" CssClass="btn purple" Text="เพิ่มตัวชี้วัด" />									
						                    <asp:Button ID="btnBack" OnClick="btnBack_Click" runat="server" CssClass="btn" Text="ย้อนกลับ" />								                    
						          </asp:Panel>
							</div>
			                
                                
			   </asp:Panel>
			    
			    
			    <div class="row-fluid" id="divEdit" runat="server" Visible="False">
                    <div class="span12">
				                    <div class="portlet-body form">
									    <h3 class="form-section"><i class="icon-th-list"></i> 
									        กำหนดตัวชี้วัดผลงาน สำหรับรอบการประเมิน <asp:Label ID="lblKPIRound" runat="server"></asp:Label><br>
									    </h3>
									    <h4 class="form-section"><i class="icon-th-list"></i> ลำดับ <asp:Label ID="lblKPINo" runat="server"></asp:Label></h4>
									    <div class="row-fluid form-horizontal">
									        <div class="span4 ">
											    <div class="control-group">
										            <label class="control-label">งาน</label>
										            <div class="controls">
										                <asp:TextBox TextMode="MultiLine" runat="server" ID="txt_KPI_Job" CssClass="large m-wrap" Height="100px"></asp:TextBox>
											        </div>
									            </div>
									            <div class="control-group">
										            <label class="control-label">ตัวชี้วัดผลงาน</label>
										            <div class="controls">
											            <asp:TextBox TextMode="MultiLine" runat="server" ID="txt_KPI_Target" CssClass="large m-wrap" Height="100px"></asp:TextBox>
										            </div>
									            </div>
									            <div class="control-group">
										            <label class="control-label">1 คะแนน</label>
										            <div class="controls">
											            <asp:TextBox TextMode="MultiLine" runat="server" ID="txt_KPI_Choice1" CssClass="large m-wrap" Height="100px"></asp:TextBox>
										            </div>
									            </div>
									            <div class="control-group">
										            <label class="control-label">2 คะแนน</label>
										            <div class="controls">
											            <asp:TextBox TextMode="MultiLine" runat="server" ID="txt_KPI_Choice2" CssClass="large m-wrap" Height="100px"></asp:TextBox>
										            </div>
									            </div>
									            <div class="control-group">
										            <label class="control-label">3 คะแนน</label>
										            <div class="controls">
											            <asp:TextBox TextMode="MultiLine" runat="server" ID="txt_KPI_Choice3" CssClass="large m-wrap" Height="100px"></asp:TextBox>
										            </div>
									            </div>
									            <div class="control-group">
										            <label class="control-label">4 คะแนน</label>
										            <div class="controls">
											            <asp:TextBox TextMode="MultiLine" runat="server" ID="txt_KPI_Choice4" CssClass="large m-wrap" Height="100px"></asp:TextBox>
										            </div>
									            </div>
									            <div class="control-group">
										            <label class="control-label">5 คะแนน</label>
										            <div class="controls">
											            <asp:TextBox TextMode="MultiLine" runat="server" ID="txt_KPI_Choice5" CssClass="large m-wrap" Height="100px"></asp:TextBox>
										            </div>
									            </div>
									            <div class="control-group">
										            <label class="control-label">น้ำหนัก</label>
										            <div class="controls">
											            <asp:TextBox runat="server" ID="txt_KPI_Weight" CssClass="m-wrap small" ></asp:TextBox>
											            <span class="help-inline">%</span>
										            </div>
									            </div>
										    </div>													
									    </div>
								    <div class="form-actions">
								        <asp:Button ID="btnOK" OnClick="btnOK_Click" runat="server" CssClass="btn blue" Text="บันทึก" />
								        <asp:Button ID="btnClose" OnClick="btnClose_Click" runat="server" CssClass="btn" Text="ยกเลิก" />							            
						            </div>												
							    </div>
						</div>
                </div>
				               
				<asp:Panel ID="pnlAddAss" DefaultButton="btn_Search_Fixed_Ass" runat="server" Visible="False">
                    <div style="z-index: 1049;" class="modal-backdrop fade in"></div>
                    <div style="z-index: 1050; max-width:90%; min-width:70%; left:30%; max-height:95%; top:5%;" class="modal">
                        
				            <div class="modal-header">
	                            <h4><i class="icon-search"></i> เลือกผู้ประเมินเพิ่มเติม (ตั้งแต่อดีตจนถึงปัจจุบัน)</h4>
                            </div>
                            <div class="modal-body" style="max-height:85%;">
	                                  	<div >
											<div class="row-fluid">																			
												    <div class="row-fluid">												           					
												            <div class="span4 ">
														        <div class="control-group">
													                <label class="control-label"><i class="icon-sitemap"></i> หน่วยงาน/ตำแหน่ง</label>
													                <div class="controls">
														                <asp:TextBox ID="txt_Search_ASS_POS" OnTextChanged="btn_Fixed_Assessor_Click" runat="server" AutoPostBack="true" CssClass="m-wrap medium" placeholder="ค้นหาจากหน่วยงาน/ตำแหน่ง"></asp:TextBox>
														                <asp:Button ID="btn_Search_Fixed_Ass" OnClick="btn_Fixed_Assessor_Click" runat="server" style="display:none;" />
														            </div>
												                </div>
													        </div>													    
													       <div class="span4 ">
												                <div class="control-group">
													                <label class="control-label"><i class="icon-user"></i> ชื่อ/เลขประจำตัว</label>
													                <div class="controls">
														                <asp:TextBox ID="TextBox2" OnTextChanged="btn_Fixed_Assessor_Click" runat="server" AutoPostBack="true" CssClass="m-wrap medium" placeholder="ค้นหาจากชื่อ/เลขประจำตัว"></asp:TextBox>
														                <asp:Button ID="txt_Search_ASS_Name" OnClick="btn_Fixed_Assessor_Click" runat="server" Text="" style="display:none;" />
													                </div>
												                </div>														        
													        </div>			
													        
												    </div>
												    	
												     <asp:Label ID="lblCountFixedAssessor" runat="server" Font-Size="14px" Font-Bold="true"></asp:Label>
								                    <table class="table table-full-width table-advance dataTable no-more-tables table-hover">
									                    <thead>
										                    <tr>
											                    <th><i class="icon-sitemap"></i> หน่วยงาน</th>
											                    <th><i class="icon-user"></i>พนักงาน</th>
											                    <th><i class="icon-briefcase"></i> ตำแหน่ง</th>
											                    <th><i class="icon-bookmark"></i> ระดับ</th>											                    
											                    <th><i class="icon-bolt"></i> เลือก</th>
										                    </tr>
									                    </thead>
									                    <tbody>
									                        <asp:Repeater ID="rptFixedAssessor" OnItemCommand="rptFixedAssessor_ItemCommand" OnItemDataBound="rptFixedAssessor_ItemDataBound" runat="server">
									                            <ItemTemplate>
    									                            <tr>                                        
											                            <td data-title="หน่วยงาน"><asp:Label ID="lblPSNDept" runat="server"></asp:Label></td>
											                            <td data-title="พนักงาน"><asp:Label ID="lblPSNName" runat="server"></asp:Label></td>
											                            <td data-title="ตำแหน่ง"><asp:Label ID="lblPSNPos" runat="server"></asp:Label></td>
											                            <td data-title="ระดับ"><asp:Label ID="lblPSNClass" runat="server"></asp:Label></td>											                            
											                            <td data-title="เลือก">
											                                <asp:Button ID="btnSelect" runat="server" CssClass="btn mini blue" CommandName="Select" Text="เลือก" />
											                                <asp:ConfirmButtonExtender ID="cfmbtnSelect" TargetControlID="btnSelect" runat="server" ></asp:ConfirmButtonExtender>
										                                </td>
										                            </tr>	
									                            </ItemTemplate>
									                        </asp:Repeater>
									                    </tbody>
								                    </table>
                    								
								                    <asp:PageNavigation ID="PagerFixedAssessor" OnPageChanging="PagerFixedAssessor_PageChanging" MaximunPageCount="10" PageSize="7" runat="server" />
											  
											</div>
										</div>
                            </div>                            
                            
                            <asp:LinkButton ID="btnCloseFixedAss" OnClick="btnCloseFixedAss_Click" runat="server" CssClass="fancybox-item fancybox-close" ToolTip="Close" />
                     
                    </div>
               </asp:Panel>       
</div>
</ContentTemplate>           
</asp:UpdatePanel>					 
</asp:Content>



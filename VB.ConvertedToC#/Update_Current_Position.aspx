﻿
<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPage.Master" CodeBehind="Update_Current_Position.aspx.cs" Inherits="VB.Update_Current_Position" %>

<%@ Register src="WUC_DateReporter.ascx" tagname="WUC_DateReporter" tagprefix="uc1" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<asp:UpdatePanel ID="UpdatePanel1" runat="server">      
<ContentTemplate>

<asp:ToolkitScriptManager ID="toolkit1" runat="server"></asp:ToolkitScriptManager>

<div class="container-fluid">
				<!-- BEGIN PAGE HEADER-->
				<div class="row-fluid" id="fixedPageHeader">
					<div class="span12"> 
						<!-- BEGIN PAGE TITLE & BREADCRUMB-->
						<h3 class="page-user-md">UPDATE ตำแหน่งประเมินทั้งหมด <font color="blue">(Manual)</font></h3>					
									
						<ul class="breadcrumb">
                            <li>
                            	<i class="icon-user"></i> <a href="javascript:;">UPDATE ตำแหน่งประเมินทั้งหมด</a><i class="icon-angle-right"></i>
                            </li>
                        	<%--<li>
                        	    <i class="icon-check"></i> <a href="javascript:;">การประเมินผลตัวชี้วัด (KPI)</a>
                        	</li>--%>                        	
                        	
                            <uc1:WUC_DateReporter ID="WUC_DateReporter1" runat="server" />
                        </ul>
						<!-- END PAGE TITLE & BREADCRUMB-->
									        
				     </div>

				
			    </div>
		        <!-- END PAGE HEADER-->



                                                    <div class="row-fluid form-horizontal">
												          <div class="span4 ">
														    <div class="control-group">
													            <label class="control-label"><i class="icon-retweet"></i> สำหรับรอบการประเมิน</label>
													            <div class="controls">
														            <asp:DropDownList ID="ddlRound"  runat="server" AutoPostBack="true" 
                                                                        CssClass="medium m-wrap" onselectedindexchanged="ddlRound_SelectedIndexChanged">
														            </asp:DropDownList>
														        </div>
												            </div>
													    </div>	
													    <div class="span6 ">
														    <div class="control-group">
													            <label class="control-label"><i class="icon-user"></i> ประเภท</label>
													            <div class="controls">
                                                                    <asp:DropDownList ID="ddlType"  runat="server" AutoPostBack="true" CssClass="medium m-wrap">
                                                                        <asp:ListItem Text="KPI" Value ="KPI"></asp:ListItem>
                                                                        <asp:ListItem Text="COPM" Value ="COPM"></asp:ListItem>
														                </asp:DropDownList>
													            </div>
												            </div>
													    </div>	
												     </div>
                                                     <div class="row-fluid form-horizontal">
												        <div class="span4 ">
													        <div class="control-group">
													            <label class="control-label"><i class="icon-sitemap"></i> ฝ่าย</label>
													            <div class="controls">
														            <asp:DropDownList ID="ddlSector" runat="server" AutoPostBack="true" 
                                                                        CssClass="medium m-wrap" 
                                                                        onselectedindexchanged="ddlSector_SelectedIndexChanged">
														            </asp:DropDownList>
														        </div>
												            </div>
													    </div>
													     <div class="span6 ">
														    <%--<div class="control-group">
													            <label class="control-label"><i class="icon-sitemap"></i> ชื่อหน่วยงาน</label>
													            <div class="controls">
														            <asp:TextBox ID="txt_Search_Organize" OnTextChanged="Search" runat="server" AutoPostBack="true" CssClass="m-wrap medium" placeholder="ค้นหาจากหน่วยงาน/ตำแหน่ง"></asp:TextBox>
														        </div>
												            </div>--%>
													    </div>													    
												    </div>
                                                    <div class="row-fluid form-horizontal">
												          <div class="span4 ">
														    <div class="control-group">
													            <div class="controls">
                                                                    <asp:Label ID="lblCountPSN" runat="server" Font-Size="14px" Font-Bold="true"></asp:Label>
														            <asp:Label ID="lblCountSuccess" runat="server" Font-Size="14px" Font-Bold="true" Text ="-" style=" display:none;"></asp:Label>
                                                                </div>
												            </div>
													    </div>	
													    <div class="span6 ">
														    <div class="control-group">
													            <div class="controls" style="text-align :left ;">
                                                                    
                                                                    <asp:Button id="btnOK"  runat="server" CssClass="btn blue pull-right " 
                                                                        Text="ดำเนินการ" onclick="btnOK_Click" />
													            </div>
												            </div>
													    </div>	


												     </div>

                                                    <div class="row-fluid" style="margin-top:10px; display :none;">
					                                    <div class="span4 ">
					                                        <div class="control-group">
								                                <label class="control-label" style="width:100px;">ผู้ประเมิน :</label>
								                                <div class="controls" style="margin-left: 120px;">
									                                <asp:DropDownList ID="ddlASSName" runat="Server" AutoPostBack="True" style="font-weight:bold; color:Black; border:none;">									        
									                                </asp:DropDownList>
									                            </div>
							                                </div>
					                                    </div>
					                                    <div class="span3 ">
					                                        <div class="control-group">
								                                <label class="control-label" style="width:100px;">ตำแหน่ง :</label>
								                                <div class="controls" style="margin-left: 120px;">
									                                <asp:Label ID="lblASSPos" runat="server" CssClass="text bold"></asp:Label>
								                                </div>
							                                </div>
					                                    </div>
					                                    <div class="span5 ">
					                                        <div class="control-group">
								                                <label class="control-label" style="width:100px;">สังกัด :</label>
								                                <div class="controls" style="margin-left: 120px;">
									                                <asp:Label ID="lblASSDept" runat="server" CssClass="text bold"></asp:Label>
								                                </div>
							                                </div>
					                                    </div>
					                                 </div>


</div>

</ContentTemplate>           
</asp:UpdatePanel>					 
</asp:Content>

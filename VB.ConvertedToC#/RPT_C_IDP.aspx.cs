using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Xml.Linq;
using System.Diagnostics;
using System.Collections.Specialized;
using System.Configuration;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Caching;
using System.Web.SessionState;
using System.Web.Security;
using System.Web.Profile;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using System.Data.SqlClient;
namespace VB
{

	public partial class RPT_C_IDP : System.Web.UI.Page
	{


		HRBL BL = new HRBL();

		GenericLib GL = new GenericLib();
		public int R_Year {
			get {
				try {
					return GL.CINT( GL.SplitString(ddlRound.Items[ddlRound.SelectedIndex].Value, "-")[0]);
				} catch (Exception ex) {
					return 0;
				}
			}
		}

		public int R_Round {
			get {
				try {
					return GL.CINT(GL.SplitString(ddlRound.Items[ddlRound.SelectedIndex].Value, "-")[1]);
				} catch (Exception ex) {
					return 0;
				}
			}
		}


		protected void Page_Load(object sender, System.EventArgs e)
		{
			if ((Session["USER_PSNL_NO"] == null)) {
				ScriptManager.RegisterStartupScript(this.Page, typeof(string), "Alert", "showAlert('กรุณาลงชื่อเข้าใช้ระบบ');", true);
				Response.Redirect("Login.aspx");
				return;
			}

			if (!IsPostBack) {
				BL.BindDDlYearRound(ddlRound);
				ddlRound.Items.RemoveAt(0);
				BL.BindDDlPSNType(ddlPSNType);
				BL.BindDDlFLDCode(ddlFN);
                BL.BindDDlClassAll(ddlClassGroupFrom);
                BL.BindDDlClassAll(ddlClassGroupTo);

				BindPersonalList();               
			}
		}

		private void BindPersonalList()
		{
			string SQL = " SELECT DISTINCT" + "\n";
			SQL += " R_Year,R_Round,DEPT_CODE,DEPT_NAME,COMP.COMP_Type_Id,CT.COMP_Type_Name_EN,CT.COMP_Type_Name_TH," + "\n";
			SQL += " Master_No,COMP.COMP_Comp,PSNL_NO,PSNL_Fullname,POS_No,POS_Name,PNPS_CLASS,FN_Name," + "\n";
            SQL += " WAGE_TYPE,WAGE_NAME,COMP_Type_Name_TH,Score,Score-5 GAP ,COMP_Status,COMP_Status_Name" + "\n";

			SQL += " FROM " + "\n";
			SQL += " (SELECT * FROM vw_COMP_CORE_GAP_WITH_NO_BHV WHERE COMP_Status=5 AND R_Year=" + R_Year + " AND R_Round=" + R_Round + " AND Score>0 UNION ALL" + "\n";
			SQL += " SELECT * FROM vw_COMP_FN_GAP_WITH_NO_BHV WHERE COMP_Status=5 AND R_Year=" + R_Year + " AND R_Round=" + R_Round + " AND Score>0 UNION ALL" + "\n";
			SQL += " SELECT * FROM vw_COMP_MGR_GAP_WITH_NO_BHV WHERE COMP_Status=5 AND R_Year=" + R_Year + " AND R_Round=" + R_Round + " AND Score>0 ) COMP" + "\n";
			SQL += " LEFT JOIN tb_HR_COMP_Type CT ON COMP.COMP_Type_Id=CT.COMP_Type_Id " + "\n";
			string Filter = "";
			string StartTitle = " ปี " + R_Year + " รอบ " + R_Round;
			string Title = "";

			switch (ddlPSNType.SelectedIndex) {
				case 1:
					//--------- ชั่วโมง ------------
					Filter += "WAGE_TYPE=2 AND " + "\n";
					Title += " พนักงานรายชั่วโมง ";
					break;
				case 2:
					//--------- วัน ------------
					Filter += "WAGE_TYPE=1 AND " + "\n";
					Title += " พนักงานรายวัน ";
					break;
				case 3:
					//--------- เดือน ------------
					Filter += "WAGE_TYPE NOT IN (1,2) AND " + "\n";
					Title += " พนักงานรายเดือน ";
					break;
				default:
					//--------All---
					break;

			}

			if (!string.IsNullOrEmpty(Title))
				Title = "ของ" + Title;

			if (ddlFN.SelectedIndex > 0) {
				Filter += " FN_Name='" + ddlFN.Items[ddlFN.SelectedIndex].Text.Replace("'", "''") + "' AND " + "\n";
				if (Strings.Len(ddlFN.Items[ddlFN.SelectedIndex].Text) > 3) {
					Title += ddlFN.Items[ddlFN.SelectedIndex].Text;
				} else {
					Title += " สายงาน " + ddlFN.Items[ddlFN.SelectedIndex].Text;
				}
			}
            

            if (ddlClassGroupFrom.SelectedIndex > 0 && (ddlClassGroupTo.SelectedIndex == 0))
            {
                Filter += " PNPS_CLASS=" + ddlClassGroupFrom.Items[ddlClassGroupFrom.SelectedIndex].Value + " AND " + "\n";
                Title += " ระดับ " + ddlClassGroupFrom.Items[ddlClassGroupFrom.SelectedIndex].Text;
            }

            if ((ddlClassGroupFrom.SelectedIndex > 0) && (ddlClassGroupTo.SelectedIndex > 0))
            {
                Filter += " ( PNPS_CLASS >= " + ddlClassGroupFrom.Items[ddlClassGroupFrom.SelectedIndex].Value + " AND PNPS_CLASS <= " + ddlClassGroupTo.Items[ddlClassGroupTo.SelectedIndex].Value + " )" + " AND " + "\n";

                Title += " ระดับ " + ddlClassGroupFrom.Items[ddlClassGroupFrom.SelectedIndex].Text + " ถึง " + ddlClassGroupTo.Items[ddlClassGroupTo.SelectedIndex].Text;
            }




			if (!string.IsNullOrEmpty(txtDept.Text)) {
				//Title &= " หน่วยงาน '" & txtDept.Text & "' "
				Filter += " (DEPT_Name LIKE '%" + txtDept.Text.Replace("'", "''") + "%' OR DEPT_CODE LIKE '%" + txtDept.Text.Replace("'", "''") + "%') AND " + "\n";
			}

			if ((chkCOMP1.Checked & chkCOMP2.Checked & chkCOMP3.Checked) | (!chkCOMP1.Checked & !chkCOMP2.Checked & !chkCOMP3.Checked)) {
				//-----------Do Nothing--------------
			} else {
				string sel = "";
				Title += " สำหรับ";
				if (chkCOMP1.Checked) {
					sel += "1,";
					Title += "สมรรถนะหลัก(Core),";
				}
				if (chkCOMP2.Checked) {
					sel += "2,";
					Title += "สมรรถนะตามสายงาน(Functional),";
				}
				if (chkCOMP3.Checked) {
					sel += "3,";
					Title += "สมรรถนะตามสายระดับ/การจัดการทั่วไป(Managerial),";
				}
				Title = Title.Substring(0, Title.Length - 1);
				Filter += " COMP_TYPE_ID IN (" + sel.Substring(0, sel.Length - 1) + ") AND " + "\n";
			}
//Filter += " COMP.COMP_TYPE_ID IN (" + sel.Substring(0, sel.Length - 1) + ") AND " + "\n";
			if (!string.IsNullOrEmpty(txtCOMP_COMP.Text)) {
				Filter += " (COMP_COMP LIKE '%" + txtCOMP_COMP.Text.Replace("'", "''") + "%') AND " + "\n";
			}


			if ((chkGAP0.Checked & chkGAP1.Checked & chkGAP2.Checked & chkGAP3.Checked & chkGAP4.Checked) | (!chkGAP0.Checked & !chkGAP1.Checked & !chkGAP2.Checked & !chkGAP3.Checked & !chkGAP4.Checked)) {
				//-----------Do Nothing--------------
			} else {
				string sel = "";
				string t = "";
				if (chkGAP0.Checked) {
					sel += "5,";
					t += "0,";
				}
				if (chkGAP1.Checked) {
					sel += "4,";
					t += "-1,";
				}
				if (chkGAP2.Checked) {
					sel += "3,";
					t += "-2,";
				}
				if (chkGAP3.Checked) {
					sel += "2,";
					t += "-3,";
				}
				if (chkGAP4.Checked) {
					sel += "1,";
					t += "-4,";
				}
				Filter += " Score IN (" + sel.Substring(0, sel.Length - 1) + ") AND " + "\n";
				if (chkGAP1.Checked & chkGAP2.Checked & chkGAP3.Checked & chkGAP4.Checked) {
					Title += " ที่มีช่องว่างสมรรถนะ";
				} else if (chkGAP0.Checked & !chkGAP1.Checked & !chkGAP2.Checked & !chkGAP3.Checked & !chkGAP4.Checked) {
					Title += " ที่ไม่มีช่องว่างสมรรถนะ";
				} else {
					Title += " ที่มีช่องว่าง " + t.Substring(0, t.Length - 1);
				}

			}

            //if (!string.IsNullOrEmpty(Filter)) {
            //    SQL += " WHERE " + Filter.Substring(0, Filter.Length - (" AND " + "\n").Length) + "\n";
            //}
			if (!string.IsNullOrEmpty(Title)) {
				Title = StartTitle + " " + Title;
			} else {
				Title = StartTitle;
			}



			SQL += "  ORDER BY DEPT_CODE,COMP.COMP_Type_Id,COMP.Master_No,Score,PNPS_CLASS DESC,POS_No" + "\n";
			SqlDataAdapter DA = new SqlDataAdapter(SQL, BL.ConnectionString());
			DA.SelectCommand.CommandTimeout = 200;
			DataTable DT = new DataTable();
			DA.Fill(DT);

            if (Filter != "") { 
                DT.DefaultView.RowFilter = "  " + Filter.Substring(0, Filter.Length - 6) + "\n";  
            }

            
            //DT.DefaultView.RowFilter = Filter;
            DT = DT.DefaultView.ToTable();
            Session["RPT_C_IDP"] = DT;


            //Session["RPT_C_IDP"] = DT;
			Session["RPT_C_IDP_Title"] = Title;
			Pager.SesssionSourceName = "RPT_C_IDP";
			Pager.RenderLayout();

			lblTitle.Text = Title;
			//If DT.Rows.Count = 0 Then
			//    lblCountPSN.Text = "ไม่พบรายการดังกล่าว"
			//Else
			//    lblCountPSN.Text = "พบ " & GL.StringFormatNumber(DT.Rows.Count, 0) & " รายการ"
			//End If

			if (DT.Rows.Count == 0) {
				lblTitle.Text += "\n" + "<br>ไม่พบรายการดังกล่าว";
			} else {
				lblTitle.Text += "\n" + "<br>พบ " + GL.StringFormatNumber(DT.Rows.Count, 0) + " รายการ";

				string[] Col = { "PSNL_NO" };
				DataTable tmp = DT.DefaultView.ToTable(true, Col).Copy();

				lblTitle.Text += " พนักงาน " + GL.StringFormatNumber(tmp.Rows.Count, 0) + " คน";

			}

		}

		protected void Pager_PageChanging(PageNavigation Sender)
		{
			Pager.TheRepeater = rptCOMP;
		}

		string LastDept = "";
		string LastType = "";
		string LastComp = "";
		protected void rptCOMP_ItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
		{
			if (e.Item.ItemType != ListItemType.AlternatingItem & e.Item.ItemType != ListItemType.Item)
				return;

            HtmlTableRow tdDept = (HtmlTableRow)e.Item.FindControl("tdDept");
            HtmlTableRow tdType = (HtmlTableRow)e.Item.FindControl("tdType");
            Label lblOrganize = (Label)e.Item.FindControl("lblOrganize");
            Label lblCOMPType = (Label)e.Item.FindControl("lblCOMPType");
            Label lblCOMPComp = (Label)e.Item.FindControl("lblCOMPComp");
            Label lblNo = (Label)e.Item.FindControl("lblNo");
            Label lblPSNName = (Label)e.Item.FindControl("lblPSNName");
            Label lblPSNPos = (Label)e.Item.FindControl("lblPSNPos");
            Label lblPSNType = (Label)e.Item.FindControl("lblPSNType");
            Label lblClass = (Label)e.Item.FindControl("lblClass");
            Label lblScore = (Label)e.Item.FindControl("lblScore");
            Label lblGAP = (Label)e.Item.FindControl("lblGAP");
            HtmlAnchor btnPrint = (HtmlAnchor)e.Item.FindControl("btnPrint");

			//--------- Table Cell--------------
            HtmlTableCell td1 = (HtmlTableCell)e.Item.FindControl("td1");
            HtmlTableCell td2 = (HtmlTableCell)e.Item.FindControl("td2");
            HtmlTableCell td3 = (HtmlTableCell)e.Item.FindControl("td3");
            HtmlTableCell td4 = (HtmlTableCell)e.Item.FindControl("td4");
            HtmlTableCell td5 = (HtmlTableCell)e.Item.FindControl("td5");
            HtmlTableCell td6 = (HtmlTableCell)e.Item.FindControl("td6");
            HtmlTableCell td7 = (HtmlTableCell)e.Item.FindControl("td7");
            HtmlTableCell td8 = (HtmlTableCell)e.Item.FindControl("td8");
            HtmlTableCell td9 = (HtmlTableCell)e.Item.FindControl("td9");

            DataRowView drv = (DataRowView)e.Item.DataItem;

			string DisplayCOMPType = drv["COMP_Type_Name_TH"].ToString() + " (" + drv["COMP_Type_Name_EN"].ToString() + ")";
			//---------- แยกหน่วยงาน---------
			if (drv["DEPT_NAME"].ToString() != LastDept) {
				LastDept = drv["DEPT_NAME"].ToString();
				lblOrganize.Text = LastDept;

				LastType = DisplayCOMPType;
				lblCOMPType.Text = LastType;

				LastComp = drv["COMP_Comp"].ToString();
				lblCOMPComp.Text = LastComp;

				//'----------- Report Number ---------------
				//Dim Col() As String = {"PSNL_NO"}
				//Dim DT As DataTable = CType(Session("RPT_C_IDP"), DataTable)
				//Dim tmp As DataTable = DT.DefaultView.ToTable.Copy

				//lblTitle.Text &= " พนักงาน " & GL.StringFormatNumber(tmp.Rows.Count, 0) & " คน"

			//---------- แยกประเภทสมรรถนะ---------
			} else if (LastType != DisplayCOMPType) {
				tdDept.Visible = false;

				LastType = DisplayCOMPType;
				lblCOMPType.Text = LastType;

				LastComp = drv["COMP_Comp"].ToString();
				lblCOMPComp.Text = LastComp;



			//---------- แยกสมรรถนะ---------
			} else if (LastComp != drv["COMP_Comp"].ToString()) {
				tdDept.Visible = false;
				tdType.Visible = false;

				LastComp = drv["COMP_Comp"].ToString();
				lblCOMPComp.Text = LastComp;

				td1.Style["border-top"] = "none";


			//---------- แยกคน---------
			} else {
				tdDept.Visible = false;
				tdType.Visible = false;

				td1.Style["border-top"] = "none";
				td2.Style["border-top"] = "none";
			}

			lblNo.Text = drv["PSNL_NO"].ToString();
			lblPSNName.Text = drv["PSNL_Fullname"].ToString();
			lblPSNPos.Text = drv["POS_Name"].ToString();
			lblPSNType.Text =drv["WAGE_NAME"].ToString();
			if (!GL.IsEqualNull(drv["PNPS_CLASS"])) {
				lblClass.Text = GL.CINT (drv["PNPS_CLASS"]).ToString();
			} else {
				lblClass.Text = "";
			}

			lblScore.Text = drv["Score"].ToString();
			lblGAP.Text = drv["GAP"].ToString();

            btnPrint.HRef = "Print/RPT_C_PNSAss.aspx?MODE=PDF&R_Year=" + R_Year + "&R_Round=" + R_Round + "&PSNL_No=" + drv["PSNL_NO"].ToString() + "&Status=" + drv["COMP_Status"].ToString();
		}

        protected void btnSearch_Click(object sender, System.EventArgs e)
		{
			BindPersonalList();
		}
		public RPT_C_IDP()
		{
			Load += Page_Load;
		}
	}
}

using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Xml.Linq;
using System.Diagnostics;
using System.Collections.Specialized;
using System.Configuration;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Caching;
using System.Web.SessionState;
using System.Web.Security;
using System.Web.Profile;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
namespace VB
{

	public partial class CP_View_Personal_Self : System.Web.UI.Page
	{


		HRBL BL = new HRBL();

		GenericLib GL = new GenericLib();
		protected void Page_Load(object sender, System.EventArgs e)
		{
			if ((Session["USER_PSNL_NO"] == null)) {
				ScriptManager.RegisterStartupScript(this.Page, typeof(string), "Alert", "showAlert('กรุณาลงชื่อเข้าใช้ระบบ');", true);
				Response.Redirect("Login.aspx");
				return;
			}
			if (!IsPostBack) {
				BindPersonal();
				ClearForm();
			}
            
		}
               
        public string PNPS_PSNL_NO
        {
			get {
				try {
					return Session["USER_PSNL_NO"].ToString ();
				} catch (Exception ex) {
					return "";
				}
			}
		}

		public string POS_NO {
			get { return lblName.Attributes["POS_NO"]; }
			set { lblName.Attributes["POS_NO"] = value; }
		}
		private void ClearForm()
		{
			POS_NO = "";
			PnlShow.Visible = true;
		}

        private void BindPersonal()
        {

            DataTable DT = new DataTable();
            DT = BL.PersonalCareerPath(PNPS_PSNL_NO);

            if (!string.IsNullOrEmpty(PNPS_PSNL_NO))
            {
                if (DT.Rows.Count > 0)
                {
                    lblName.Text = "<B>" + DT.Rows[0]["PSNL_NO"].ToString() + " : " + DT.Rows[0]["PSNL_Fullname"].ToString() + "</B>";
                    lblDept.Text = Convert.ToString(DT.Rows[0]["DEPT_NAME"]);
                    lblPos.Text = "<B>" + DT.Rows[0]["POS_Name"].ToString() + "</B>";
                    POS_NO = DT.Rows[0]["POS_NO"].ToString();
                    lblClass.Text = "<B>" + Convert.ToInt32(DT.Rows[0]["PNPS_CLASS"]) + "</B>";

                    switch (DT.Rows[0]["History_PUNISH"].ToString())
                    {
                        case "ไม่เคยรับโทษ":
                            lblBoxPUNISH_No.Text = " ไม่เคยโดนรับโทษ ";
                            Box_PUNISH_Yes.Visible = false;
                            Box_PUNISH_No.Visible = true;
                            break;
                        case "เคยรับโทษ":
                            lblBoxPUNISH_Yes.Text = " เคยรับโทษทางวินัย ";
                            Box_PUNISH_Yes.Visible = true;
                            Box_PUNISH_No.Visible = false;
                            break;
                    }

                }
                else
                {

                }
                //---------------------ผลการประเมิน-----------------------
                BindAss();
                //---------------------ประวัติการอบรม-----------------------
                BindCourse();
                //---------------------จำนวนวันลา-----------------------
                BindLEAVE();
                //---------------------ผลการสอบ-----------------------
                BindTest();
                //---------------------ประวัติการครองตำแหน่ง-----------------------
                BindHPosition();
                //---------------------ตำแหน่งที่สามารถเข้ารับได้-----------------------
                BindPos_Route();

            }
            else
            {
            }



        }

		#region "Show"

		protected void btnSearch_Show_Click(object sender, System.EventArgs e)
		{
			BindCourse();
			BindLEAVE();
			BindTest();
			BindHPosition();
		}

        private void BindAss()
        {

            string SQL = "";
            SQL += "SELECT * FROM vw_Path_3Round_Yearly_Result \n";
            SQL += " WHERE PSNL_NO='" + PNPS_PSNL_NO + "'\n";
            SQL += " ORDER BY  R_Year DESC , R_Round DESC";
            SqlDataAdapter DA = new SqlDataAdapter(SQL, BL.ConnectionString());
            DataTable DT = new DataTable();
            DA.Fill(DT);

            rptAss.DataSource = DT;
            rptAss.DataBind();

            if (DT.Rows.Count > 0)
            {
                DT.DefaultView.RowFilter = " KPI_StatusAss =5 AND COMP_StatusAss=5";
                if (DT.DefaultView.Count > 0)
                {
                    double SUM_AVG = (GL.CDBL(DT.Compute("SUM(Round_Result)", " KPI_StatusAss =5 AND COMP_StatusAss=5")) / (DT.DefaultView.Count));
                    lblSUM_AVG.Text = "<B>" + GL.StringFormatNumber(SUM_AVG) + "</B>";
                    lblHeadAss.Text = " ";
                    table_Ass.Visible = true;
                }
                else
                {
                    lblSUM_AVG.Text = "<B>" + GL.StringFormatNumber(0) + "</B>";
                    lblHeadAss.Text = " ผลประเมินยังไม่เสร็จสมบูรณ์";
                    table_Ass.Visible = false;
                }

            }
            else
            {
                lblHeadAss.Text = " ไม่พบประวัติการวัดผลประเมิน";
                table_Ass.Visible = false;
            }


        }

        protected void rptAss_ItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType != ListItemType.Item & e.Item.ItemType != ListItemType.AlternatingItem)
                return;
            Label lblYear = (Label)e.Item.FindControl("lblYear");
            Label lblKPI_Result = (Label)e.Item.FindControl("lblKPI_Result");
            Label lblCOMP_Result = (Label)e.Item.FindControl("lblCOMP_Result");
            Label lblSUM_Result = (Label)e.Item.FindControl("lblSUM_Result");

            DataRowView drv = (DataRowView)e.Item.DataItem;

            lblYear.Text = "<B>" + "ปี " + drv["R_Year"] + " รอบ " + drv["R_Round"] + "</B>" + "  ( KPI " + drv["Weight_KPI"] + "% : COMP " + drv["Weight_COMP"] + "% )";

            if (GL.CINT(drv["KPI_StatusAss"]) == 5 && GL.CINT(drv["COMP_StatusAss"]) == 5)
            { 
             lblKPI_Result.Text = GL.StringFormatNumber(GL.CDBL(drv["KPI_Result"]));
             lblCOMP_Result.Text = GL.StringFormatNumber(GL.CDBL(drv["COMP_Result"]));
             lblSUM_Result.Text = GL.StringFormatNumber(GL.CDBL(drv["Ass_Score"]));
            }else {
                lblKPI_Result.Text = "-";
                lblCOMP_Result.Text = "-";
                lblSUM_Result.Text = "-";
            }

        }

		private void BindCourse()
		{
			string SQL = "";
			SQL += " SELECT distinct * FROM vw_Path_Course \n";
			SQL += " WHERE PNPS_PSNL_NO='" + PNPS_PSNL_NO + "'\n";
			SQL += " AND ENTRY_DAY > 0\n";
			if (!string.IsNullOrEmpty(txt_Search_Course.Text)) {
				SQL += " AND ( COURSE_DESC LIKE '%" + txt_Search_Course.Text.Replace("'", "''") + "%')\n";
			}
			SQL += " ORDER BY COURSE_DESC";
			SqlDataAdapter DA = new SqlDataAdapter(SQL, BL.ConnectionString());
			DataTable DT = new DataTable();
			DA.SelectCommand.CommandTimeout = 90;
			DA.Fill(DT);

			if (DT.Rows.Count > 20) {
				Search_Course.Visible = true;
			}

			if (DT.Rows.Count > 0) {
				lblCountCourse.Text = " พบ " + Conversion.Val(DT.Rows.Count) + " หลักสูตร";
				table_Course.Visible = true;
			} else {
				lblCountCourse.Text = " ไม่พบประวัติการฝึกอบรมที่เข้าร่วม ";
				table_Course.Visible = false;
			}


			Session["View_Personal_Course"] = DT;
			Page_Course.SesssionSourceName = "View_Personal_Course";
			Page_Course.RenderLayout();

		}

		protected void Page_Course_PageChanging(PageNavigation Sender)
		{
			Page_Course.TheRepeater = rptCourse;
		}

		protected void rptCourse_ItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
		{
			if (e.Item.ItemType != ListItemType.Item & e.Item.ItemType != ListItemType.AlternatingItem)
				return;
            Label lblCourse = (Label)e.Item.FindControl("lblCourse");
            Label lblDateFrom = (Label)e.Item.FindControl("lblDateFrom");
            Label lblDateTo = (Label)e.Item.FindControl("lblDateTo");

            DataRowView drv = (DataRowView)e.Item.DataItem;
			
			lblCourse.Text = drv["COURSE_DESC"].ToString();
			if (GL.CINT (drv["ENTRY_DAY"]) > 0) {
				lblDateFrom.Text = GL.CINT(drv["ENTRY_DAY"]).ToString ();
				//& " วัน"
			} else {
				lblDateFrom.Text = " ไม่เข้าอบรม";
			}
			lblDateTo.Text = "";
		}

		private void BindLEAVE()
		{
			string SQL = " ";

			SQL += " Select DISTINCT\n";
			SQL += " KPI_1.R_Year, KPI_1.SECTOR_CODE, KPI_1.SECTOR_NAME, KPI_1.DEPT_CODE, KPI_1.DEPT_NAME\n";
			SQL += " ,KPI_1.PSNL_NO,KPI_1.PSNL_Fullname,KPI_1.POS_Name,KPI_1.PNPS_CLASS,KPI_1.WAGE_TYPE,KPI_1.WAGE_NAME,KPI_1.POS_No\n";

			SQL += " ,ISNULL(SUM(L_Day),0) Leave_Day\n";
			SQL += " ,- CASE WHEN ISNULL(SUM(L_Day),0)<=10 THEN 0\n";
			SQL += "  --หักคะแนนเพิ่มเป็นวันละ 2.5--\n";
			SQL += "  WHEN SUM(L_Day)>10 AND SUM(L_Day)<=30 THEN 2.5*(SUM(L_Day)-10)\n";
			SQL += "  ELSE 50 END\n";
			SQL += "    Leave_Score \n";
			SQL += " FROM vw_RPT_KPI_Result KPI_1  \n";
			SQL += " /**/RIGHT JOIN tb_HR_Round R1 ON R1.R_Year=KPI_1.R_Year AND R1.R_Round=1\n";
			SQL += " LEFT JOIN tb_HR_Round R2 ON R2.R_Year=R1.R_Year AND R2.R_Round=2  \n";
			SQL += " LEFT JOIN vw_PN_LEAVE LEAVE ON KPI_1.PSNL_NO=LEAVE.PSNL_NO\n";
			SQL += " \t\t\t\tAND L_YYMM BETWEEN RIGHT(Year(R1.R_Start)+543,2)+CASE WHEN MONTH(R1.R_Start)<10 THEN '0' ELSE '' END+CAST(MONTH(R1.R_Start) AS nvarchar)\n";
			SQL += " \t\t\t\t\t\t\tAND ISNULL(RIGHT(Year(R2.R_End)+543,2),'99')+ISNULL(CASE WHEN MONTH(R2.R_End)<10 THEN '0' ELSE '' END+CAST(MONTH(R2.R_End) AS nvarchar),'99')\n";
			SQL += "  WHERE KPI_1.R_Round = 1 \n";
			SQL += " AND KPI_1 .PSNL_NO ='" + PNPS_PSNL_NO + "'\n";
			SQL += " AND KPI_1 .R_Year =2557 \n";
			SQL += " GROUP BY KPI_1.R_Year,KPI_1.R_Round,KPI_1.SECTOR_CODE,KPI_1.SECTOR_NAME,KPI_1.DEPT_CODE,KPI_1.DEPT_NAME,KPI_1.PSNL_NO,KPI_1.PSNL_Fullname,KPI_1.POS_Name,KPI_1.PNPS_CLASS,KPI_1.WAGE_TYPE,KPI_1.WAGE_NAME,KPI_1.POS_No\n";
			SQL += " ,KPI_1.Ass_Status,KPI_1.RESULT\n";
			SqlDataAdapter DA = new SqlDataAdapter(SQL, BL.ConnectionString());
			DataTable DT = new DataTable();
			DA.SelectCommand.CommandTimeout = 90;
			DA.Fill(DT);
			if (DT.Rows.Count > 0) {
				lblL_HEALTH.Text = GL.StringFormatNumber(GL.CDBL(DT.Rows[0]["Leave_Day"]));
			} else {
				lblL_HEALTH.Text = "0";
			}
		}


		private void BindTest()
		{
			string SQL = " ";
			SQL += " SELECT  * FROM vw_Path_Test\n";
			SQL += " WHERE CPSP_PERSONAL_NO='" + PNPS_PSNL_NO + "'\n";
			if (!string.IsNullOrEmpty(txtSearch_test.Text)) {
				SQL += " AND ( CPSM_SUBJECT_NAME LIKE '%" + txtSearch_test.Text.Replace("'", "''") + "%')\n";
			}
			SQL += " ORDER BY  CPSP_EXAM_DATE DESC,CPSM_SUBJECT_NAME";
			SqlDataAdapter DA = new SqlDataAdapter(SQL, BL.ConnectionString());
			DataTable DT = new DataTable();
			DA.SelectCommand.CommandTimeout = 90;
			DA.Fill(DT);

			if (DT.Rows.Count > 20) {
				Search_Test.Visible = true;
			}
			if (DT.Rows.Count > 0) {
				lblCount_Test.Text = " พบ " + Conversion.Val(DT.Rows.Count) + " วิชา";
				table_Test.Visible = true;
			} else {
				lblCount_Test.Text = " ไม่พบประวัติการสอบวัดผล";
				table_Test.Visible = false;
			}


			Session["View_Personal_Test"] = DT;
			Page_Test.SesssionSourceName = "View_Personal_Test";
			Page_Test.RenderLayout();
		}

		protected void Page_Test_PageChanging(PageNavigation Sender)
		{
			Page_Test.TheRepeater = rptTest;
		}

		protected void rptTest_ItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
		{
			if (e.Item.ItemType != ListItemType.Item & e.Item.ItemType != ListItemType.AlternatingItem)
				return;
            Label lblSubject = (Label)e.Item.FindControl("lblSubject");
            Label lblDate = (Label)e.Item.FindControl("lblDate");
            Label lblPOINT = (Label)e.Item.FindControl("lblPOINT");
            Label lblPOINT_RULE = (Label)e.Item.FindControl("lblPOINT_RULE");
            Label lblScore = (Label)e.Item.FindControl("lblScore");

            DataRowView drv = (DataRowView)e.Item.DataItem;
			
			lblSubject.Text = drv["CPSM_SUBJECT_NAME"].ToString();
			if (GL.IsEqualNull(drv["CPSP_EXAM_DATE"])) {
				lblDate.Text = "-";
			} else {
				lblDate.Text =BL.DateTimeToThaiDate((DateTime)drv["CPSP_EXAM_DATE"]);
			}
			if (GL.CINT (drv["CPSP_POINT"]) > 0) {
                lblScore.Text = GL.StringFormatNumber(GL.CDBL(drv["CPSP_POINT"]));
			} else {
                lblScore.Text = GL.StringFormatNumber(0);
			}
			if (GL.CINT (drv["CPSM_SUBJECT_POINT"]) > 0) {
				lblPOINT.Text = GL.StringFormatNumber(GL.CDBL(drv["CPSM_SUBJECT_POINT"]));
			} else {
                lblPOINT.Text = GL.StringFormatNumber(0);
			}
			if (GL.CINT (drv["CPSM_SUBJECT_POINT_RULE"]) > 0) {
				lblPOINT_RULE.Text = GL.StringFormatNumber(GL.CDBL(drv["CPSM_SUBJECT_POINT_RULE"]));
			} else {
                lblPOINT_RULE.Text = GL.StringFormatNumber(0);
			}

		}

		protected void Page_HPosition_PageChanging(PageNavigation Sender)
		{
			Page_HPosition.TheRepeater = rptHPosition;
		}
		private void BindHPosition()
		{
			string SQL = " ";
            SQL += "  SELECT PSNL_NO ,PSNL_Fullname ,PNHH_POS_NAME ,PNHH_EDU_NAME ,ISNULL(PNHH_EFFECT_STR,PNHH_DATE_END) PNHH_EFFECT_STR ,ISNULL(PNHH_DATE_END,PNHH_EFFECT_STR) PNHH_DATE_END FROM vw_Path_HPosition \n";
			SQL += " WHERE PSNL_NO='" + PNPS_PSNL_NO + "'\n";
			if (!string.IsNullOrEmpty(txtSearch_HPostion.Text)) {
				SQL += " AND ( PNHH_POS_NAME LIKE '%" + txtSearch_HPostion.Text.Replace("'", "''") + "%')\n";
			}
            SQL += "  ORDER BY PSNL_NO,PNHH_DATE_END DESC ,PNHH_EFFECT_STR  DESC, PNHH_POS_NAME \n";
			SqlDataAdapter DA = new SqlDataAdapter(SQL, BL.ConnectionString());
			DataTable DT = new DataTable();
			DA.SelectCommand.CommandTimeout = 90;
			DA.Fill(DT);

			if (DT.Rows.Count > 20) {
				Search_HPosition.Visible = true;
			}
			if (DT.Rows.Count > 0) {
				lblCount_HPosition.Text = " พบ " + Conversion.Val(DT.Rows.Count) + " ตำแหน่ง";
				table_HPosition.Visible = true;
			} else {
				lblCount_HPosition.Text = " ไม่พบประวัติการครองตำแหน่ง";
				table_HPosition.Visible = false;
			}
			Session["View_Personal_HPosition"] = DT;
			Page_HPosition.SesssionSourceName = "View_Personal_HPosition";
			Page_HPosition.RenderLayout();
		}
		protected void rptHPosition_ItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
		{
			if (e.Item.ItemType != ListItemType.Item & e.Item.ItemType != ListItemType.AlternatingItem)
				return;
            Label lblPosition = (Label)e.Item.FindControl("lblPosition");
            Label lblDateFrom = (Label)e.Item.FindControl("lblDateFrom");
            Label lblDateTo = (Label)e.Item.FindControl("lblDateTo");

            DataRowView drv = (DataRowView)e.Item.DataItem;
			
			lblPosition.Text = drv["PNHH_POS_NAME"].ToString();


            if (drv["PNHH_EFFECT_STR"].ToString() == drv["PNHH_DATE_END"].ToString())
            {
                lblDateTo.Text = BL.DateTimeToThaiDate((DateTime)drv["PNHH_EFFECT_STR"]);
                lblDateFrom.Text = "";
            }
            else { 
                if (!GL.IsEqualNull(drv["PNHH_EFFECT_STR"])) {
                    lblDateTo.Text = BL.DateTimeToThaiDate((DateTime)drv["PNHH_EFFECT_STR"]);
			    } else {
				    lblDateTo.Text = "";
			    }
                if (!GL.IsEqualNull(drv["PNHH_DATE_END"]) && !GL.IsEqualNull(drv["PNHH_EFFECT_STR"]))
                {
				    lblDateFrom.Text = " - " + BL.DateTimeToThaiDate((DateTime)drv["PNHH_DATE_END"]);
                }
                else if (!GL.IsEqualNull(drv["PNHH_DATE_END"]) && GL.IsEqualNull(drv["PNHH_EFFECT_STR"]))
                {
                    lblDateFrom.Text =  BL.DateTimeToThaiDate((DateTime)drv["PNHH_DATE_END"]);
                }      
                else {
				    lblDateFrom.Text = "";
			    }
            }

			

		}

		private void BindPos_Route()
		{
			string SQL = "";
			SQL += "  SELECT From_POS_No , To_POS_No\n";
			SQL += "  ,vw_PN_Position.POS_NO\n";
			SQL += "  ,vw_PN_Position.DEPT_NAME\n";
			SQL += "  ,vw_PN_Position.PNPO_CLASS\n";
			SQL += "  ,vw_PN_Position.MGR_NAME\n";
			SQL += "  FROM  tb_Path_Setting_Route Setting_Route\n";
			SQL += "  LEFT JOIN tb_Path_Setting Path_Setting  ON Path_Setting.POS_No=Setting_Route.From_POS_No\n";
			SQL += "  INNER JOIN vw_PN_Position ON vw_PN_Position.POS_NO=Setting_Route.To_POS_No\n";
			SQL += "  WHERE From_POS_No ='" + POS_NO + "'";
			SqlDataAdapter DA = new SqlDataAdapter(SQL, BL.ConnectionString());
			DataTable DT = new DataTable();
			DA.SelectCommand.CommandTimeout = 90;
			DA.Fill(DT);
			if (DT.Rows.Count > 0) {
				lblCountPos_Route.Text = "พบ " + DT.Rows.Count + " รายการ";
				table_Pos_Route.Visible = true;
			} else {
				lblCountPos_Route.Text = " ไม่พบตำแหน่งที่สามารถเข้ารับได้ ณ ขณะนี้";
				table_Pos_Route.Visible = false;
			}
			rptPos_Route.DataSource = DT;
			rptPos_Route.DataBind();
		}

		string LastDept = "";
		string LastDept_Route = "";
		protected void rptPos_Route_ItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
		{
			if (e.Item.ItemType != ListItemType.Item & e.Item.ItemType != ListItemType.AlternatingItem)
				return;
			Label lblDept =(Label) e.Item.FindControl("lblDept");
            Label lblPosName = (Label)e.Item.FindControl("lblPosName");
			Label lblClass = (Label) e.Item.FindControl("lblClass");

            DataRowView drv = (DataRowView)e.Item.DataItem;
			
			if (LastDept != drv["DEPT_NAME"].ToString()) {
				lblDept.Text = drv["DEPT_NAME"].ToString();
				LastDept = drv["DEPT_NAME"].ToString();
			}

			lblPosName.Text = drv["POS_NO"] + " : " + drv["MGR_NAME"].ToString();
			lblClass.Text = GL.CINT (drv["PNPO_CLASS"]).ToString ();



		}
		#endregion


	}
}

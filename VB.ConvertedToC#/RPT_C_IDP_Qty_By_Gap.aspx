﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="RPT_C_IDP_Qty_By_Gap.aspx.cs" Inherits="VB.RPT_C_IDP_Qty_By_Gap" %>

<%@ Register src="WUC_DateReporter.ascx" tagname="WUC_DateReporter" tagprefix="uc1" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<asp:ToolkitScriptManager ID="toolkit1" runat="server"></asp:ToolkitScriptManager>
<asp:UpdatePanel ID="UpdatePanel1" runat="server">      
<ContentTemplate>
<div class="container-fluid">
            	<!-- BEGIN PAGE HEADER-->
				<div class="row-fluid" id="fixedPageHeader">
					<div class="span12"> 
						<!-- BEGIN PAGE TITLE & BREADCRUMB-->
						<h3>รายงานสรุปจำนวนพนักงานแยกตามสมรรถนะ<font color="blue"> (ScreenID : R-COMP-10)</font></h3>						
						<ul class="breadcrumb">
                            <li>
                            	<i class="icon-copy"></i> <a href="javascript:;">รายงาน</a><i class="icon-angle-right"></i>
                            </li>
                            <li>
                            	<i class="icon-copy"></i> <a href="javascript:;">การประเมินผลสมรรถนะ</a><i class="icon-angle-right"></i>
                            </li>
                            <li>
                            	<i class="icon-copy"></i> <a href="javascript:;">รายงานประจำรอบ</a><i class="icon-angle-right"></i>
                            </li>
                        	<li>
                        	    <i class="icon-book"></i> <a href="javascript:;">สรุปจำนวนพนักงานแยกตามสมรรถนะ</a>
                        	</li>                      	
                        	
                            <uc1:WUC_DateReporter ID="WUC_DateReporter1" runat="server" />
                        </ul>
						<!-- END PAGE TITLE & BREADCRUMB-->
									        
				     </div>

				
			    </div>
		        <!-- END PAGE HEADER-->
                
                <asp:Panel ID="pnlList" runat="server" Visible="True" DefaultButton="btnSearch">
				<div class="row-fluid">
					
					<div class="span12">
						<!-- BEGIN SAMPLE TABLE PORTLET-->
                                        <div class="row-fluid form-horizontal">
                			                  <div class="btn-group pull-left">
                                                    <asp:Button ID="btnSearch" OnClick="btnSearch_Click"  runat="server" CssClass="btn green" Text="ค้นหา" />
                                              </div> 
				                            <div class="btn-group pull-right">                                    
								                <button data-toggle="dropdown" class="btn dropdown-toggle">พิมพ์ <i class="icon-angle-down"></i></button>										
								                <ul class="dropdown-menu pull-right">                                                       
                                                    <li><a href="Print/RPT_C_IDP_Qty_By_Gap.aspx?Mode=PDF" target="_blank">รูปแบบ PDF</a></li>
                                                    <li><a href="Print/RPT_C_IDP_Qty_By_Gap.aspx?Mode=EXCEL" target="_blank">รูปแบบ Excel</a></li>											
										         </ul>
							                </div>
									     </div>       
						                 <div class="row-fluid form-horizontal">
						                             <div class="span3 ">
														<div class="control-group">
													        <label class="control-label" style="width:100px;"> รอบการประเมิน</label>
													        <div class="controls" style="margin-left:120px;">
														        <asp:DropDownList ID="ddlRound" runat="server" CssClass="medium m-wrap">
							                                    </asp:DropDownList>
													        </div>
												        </div>
													</div>
													
													 <div class="span3 ">
												        <div class="control-group" >
											                <label class="control-label" style="width:100px;"> หน่วยงาน</label>
											                <div class="controls" style="margin-left:120px;">
												                <asp:TextBox ID="txtDept" runat="server" CssClass="medium m-wrap" PlaceHolder="ชื่อ/รหัสหน่วยงาน"></asp:TextBox>		
											                </div>
										                </div>
											        </div>			   
										  </div>
												
												<div class="row-fluid form-horizontal" >
											         <div class="span3">
												        <div class="control-group" >
											                <label class="control-label" style="width:100px;"> ประเภทพนักงาน</label>
											                <div class="controls" style="margin-left:120px;">
												                <asp:DropDownList ID="ddlPSNType" runat="server" CssClass="medium m-wrap">
												                </asp:DropDownList>
											                </div>
										                </div>
											        </div>	
											        
											        <div class="span3 ">
												        <div class="control-group" >
											                <label class="control-label" style="width:100px;"> ระดับ</label>
											                <div class="controls" style="margin-left:120px;">
												                <asp:DropDownList ID="ddlClassGroupFrom" runat="server" Width ="80px" CssClass=" m-wrap">
												                </asp:DropDownList>
                                                                 - 
                                                                <asp:DropDownList ID="ddlClassGroupTo" runat="server" Width ="80px" CssClass=" m-wrap">
												                </asp:DropDownList>		
											                </div>
										                </div>
											        </div>
											        
											        <div class="span3 ">
												        <div class="control-group">
											                <label class="control-label" style="width:100px;"> สายงาน</label>
											                <div class="controls" style="margin-left:120px;">
												                <asp:DropDownList ID="ddlFN" runat="server" CssClass="medium m-wrap">
												                </asp:DropDownList>
											                </div>
										                </div>
											        </div>	
											     </div>
											     
									            <div class="row-fluid form-horizontal">
											         <div class="span6">
												        <div class="control-group">
													        <label class="control-label" style="width:100px;"> สมรรถนะ</label>
													        <div class="controls" style="padding-top:8px; font-size:14px; margin-left:120px;">											                            
											                            <asp:CheckBox ID="chkCOMP1" runat="server" Text="" Checked="true" />หลัก (Core) &nbsp; &nbsp;
											                            <asp:CheckBox ID="chkCOMP3" runat="server" Text="" Checked="true" />สายระดับ (Managerial)&nbsp; &nbsp;
											                            <asp:CheckBox ID="chkCOMP2" runat="server" Text="" Checked="true" />สายงาน (Functional) 
											                </div>
												        </div>
                                                        <div class="control-group" >
											                <label class="control-label" style="width:100px;"> หัวข้อสมรรถนะ</label>
											                <div class="controls" style="margin-left:120px;">
												                <asp:TextBox ID="txtCOMP_COMP" OnTextChanged="btnSearch_Click" runat="server" CssClass=" span12 m-wrap" PlaceHolder="หัวข้อสมรรถนะ" ></asp:TextBox>		
											                </div>
										                </div>
													</div>
													
													<div class="span5">
												        <div class="control-group">
													        <label class="control-label" style="width:100px;"> ช่องว่าง</label>
													        <div class="controls" style="padding-top:8px; font-size:14px; margin-left:120px;">
											                            
											                            <asp:CheckBox ID="chkGAP0" runat="server" Text="" Checked="false" />0 &nbsp; &nbsp;
											                            <asp:CheckBox ID="chkGAP1" runat="server" Text="" Checked="false" />-1 &nbsp; &nbsp;
											                            <asp:CheckBox ID="chkGAP2" runat="server" Text="" Checked="true" />-2 &nbsp; &nbsp;
											                            <asp:CheckBox ID="chkGAP3" runat="server" Text="" Checked="true" />-3 &nbsp; &nbsp;
											                            <asp:CheckBox ID="chkGAP4" runat="server" Text="" Checked="true" />-4 &nbsp; &nbsp;
											                </div>
												        </div>
													</div>
									            </div>
												
								            
							<div class="portlet-body no-more-tables">
								<%--<asp:Label ID="lblCountPSN" runat="server" Font-Size="14px" Font-Bold="true"></asp:Label> &nbsp;--%>
								<div style="text-align:center; width:100%; padding-top:10px; padding-bottom:20px; font-size:14px; font-weight:bold;">
								รายงานสรุปจำนวนพนักงานแยกตามสมรรถนะ <asp:Label ID="lblTitle" runat="server"></asp:Label> 
								</div>
								
								<table class="table table-full-width table-bordered dataTable table-hover">
									<thead>
										<tr>
											<th style="text-align:center;"> สมรรถนะ</th>	
                                            <th style="text-align:center;"> หน่วยงาน</th>
                                            <th style="text-align:center;"> ระดับพนักงาน</th>
											<th style="text-align:center;"> จำนวน</th>									
										</tr>
									</thead>
								<tbody>
								<asp:Repeater ID="rptIDP" OnItemDataBound="rptIDP_ItemDataBound" runat="server">
							        <ItemTemplate>
							            <tr>
							                <td data-title="สมรรถนะ" id="td1" runat="server"><asp:Label ID="lblComp" Font-Bold="true" Font-Size="16px" runat="server"></asp:Label></td>
							                <td data-title="หน่วยงาน" id="td2" runat="server"><asp:Label ID="lblDept" runat="server"></asp:Label></td>
							                <td data-title="ระดับพนักงาน" id="td3" runat="server" style="text-align:center;"><asp:Label ID="lblClass" runat="server"></asp:Label></td>
							                <td data-title="จำนวน" id="td4" runat="server" style="text-align:center;"><asp:Label ID="lblTotal" runat="server"></asp:Label></td>
									   </tr>
                                        <tr id="trFooter" runat="server">
							                <td>&nbsp;</td>
                                            <td colspan="2" style="text-align:center;" >
							                    <asp:Label ID="lblSector" Font-Bold="true" Font-Size="16px" runat="server"></asp:Label>
							                </td>
                                            <td style="text-align:center;" >
							                    <asp:Label ID="lblSumSector" Font-Bold="true" Font-Size="16px" runat="server"></asp:Label>
							                </td>
							            </tr>
                                        <tr id="trCompSum" runat="server">
							                <td colspan="3" style="text-align:center;" >
							                    <asp:Label ID="lblCompFooter" Font-Bold="true" Font-Size="18px" runat="server"></asp:Label>
							                </td>
                                            <td style="text-align:center;" >
							                    <asp:Label ID="lblCompSum" Font-Bold="true" Font-Size="18px" runat="server"></asp:Label>
							                </td>
							            </tr>
							        </ItemTemplate>
                                    <FooterTemplate>
                                        <tr id="trSum" runat="server" visible="false">
							                <td colspan="10" style="text-align:center; font-weight:bold; background-color:#EEEEEE;" >
							                    <asp:Label ID="lblSum" runat="server"></asp:Label>
							                </td>
							            </tr>
                                    </FooterTemplate>
								</asp:Repeater>
																							
									</tbody>
								</table>
								
							</div>
					</div>
                </div>  
                </asp:Panel>
                
</div>
</ContentTemplate>           
</asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ScriptPlaceHolder" runat="server">
</asp:Content>

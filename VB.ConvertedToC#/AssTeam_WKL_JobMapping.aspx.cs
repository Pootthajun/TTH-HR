using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Xml.Linq;
using System.Diagnostics;
using System.Collections.Specialized;
using System.Configuration;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Caching;
using System.Web.SessionState;
using System.Web.Security;
using System.Web.Profile;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
namespace VB
{

	public partial class AssTeam_WKL_JobMapping : System.Web.UI.Page
	{


		HRBL BL = new HRBL();

		GenericLib GL = new GenericLib();
		public string PSNL_NO {
			get {
				try {
					return Session["USER_PSNL_NO"].ToString ();
				} catch (Exception ex) {
					return "";
				}
			}
		}

		public string DEPT_CODE {
			get {
				try {
					return GL.SplitString(ddlOrganize.Items[ddlOrganize.SelectedIndex].Value, "-")[0];
				} catch (Exception ex) {
					return "";
				}
			}
		}

		public string MINOR_CODE {
			get {
				try {
					return GL.SplitString(ddlOrganize.Items[ddlOrganize.SelectedIndex].Value, "-")[1];
				} catch (Exception ex) {
					return "00";
				}
			}
		}

		protected void Page_Load(object sender, System.EventArgs e)
		{
			if ((Session["USER_PSNL_NO"] == null)) {
				ScriptManager.RegisterStartupScript(this.Page, typeof(string), "Alert", "showAlert('กรุณาลงชื่อเข้าใช้ระบบ');", true);
				Response.Redirect("Login.aspx");
				return;
			}

            if (!Convert.ToBoolean(Session["USER_Is_Workload_Admin"]) & !Convert.ToBoolean(Session["USER_Is_Workload_User"]) & !Convert.ToBoolean(Session["USER_Is_Manager"]))
            {
                ScriptManager.RegisterStartupScript(this.Page, typeof(string), "Alert", "showAlert('คุณไม่มีสิทธิ์เข้าถึงข้อมูลส่วนนี้');", true);
                Response.Redirect("Overview.aspx");
                return;
            }

			if (!IsPostBack) {
                if(Convert.ToBoolean(Session["USER_Is_Workload_Admin"]))
                {
                    BL.BindDDlWorkloadDepartment(ddlOrganize,"","");
                }
                else{
                    BL.BindDDlWorkloadDepartment(ddlOrganize, "", (string)Session["USER_DEPT_CODE"]);
                    ddlOrganize.Enabled = false;
                }

				if (ddlOrganize.Items.Count == 1) {
					ddlOrganize.Items[0].Text = "คุณไม่มีสิทธิ์เข้าถึงข้อมูลส่วนนี้";
					return;
				} else {
					ddlOrganize.Items.RemoveAt(0);
					ddlOrganize.SelectedIndex = 0;
				}

				BindClass();
				BindJob();
				//----------- Add Class Header--------
				rptHeaderClass.DataSource = DeptClass;
				rptHeaderClass.DataBind();
			}

		}

		protected void ddlOrganize_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			BindClass();
			BindJob();
			//----------- Add Class Header--------
			rptHeaderClass.DataSource = DeptClass;
			rptHeaderClass.DataBind();
		}

		DataTable JobData;

		DataTable DeptClass;

		private void BindClass()
		{
			string SQL = "SELECT DEPT_CODE,MINOR_CODE,Class_ID,PNPO_CLASS_Start,PNPO_CLASS_End\n";
			SQL += " FROM tb_HR_Job_Mapping_Class\n";
			SQL += " WHERE DEPT_CODE='" + DEPT_CODE.Replace("'", "''") + "' AND MINOR_CODE='" + MINOR_CODE.Replace("'", "''") + "' \n";
			SQL += " ORDER BY Class_ID DESC\n";
			SqlDataAdapter DA = new SqlDataAdapter(SQL, BL.ConnectionString());
			DeptClass = new DataTable();
			DA.Fill(DeptClass);

			//----------- ใส่ Default ให้ กรณีที่ ยังไม่ได้ตั้งค่าระดับ --------------
			if (DeptClass.Rows.Count == 0) {
				BL.SetJobMappingAutoClass(DEPT_CODE, MINOR_CODE);
				DA = new SqlDataAdapter(SQL, BL.ConnectionString());
				DeptClass = new DataTable();
				DA.Fill(DeptClass);
			}

		}

		private void BindJob()
		{
			//----------- Check Class is set----------
			string SQL = "";

			SQL += "  SELECT Job_LEVEL,Job_No,Header.Job_ID,Job_Parent,Header.Job_Name,Detail.Job_Name Job_Detail,Class_ID\n";
			SQL += "  FROM tb_HR_Job_Mapping Header\n";
			SQL += "  LEFT JOIN tb_HR_Job_Mapping_Detail Detail ON Header.Job_ID=Detail.Job_ID\n";
			SQL += "  WHERE PNDP_DEPT_CODE='" + DEPT_CODE.Replace("'", "''") + "' AND PNDP_MINOR_CODE='" + MINOR_CODE.Replace("'", "''") + "' \n";
			SQL += "  ORDER BY  Header.Job_LEVEL,Header.Job_No\n";

			SqlDataAdapter DA = new SqlDataAdapter(SQL, BL.ConnectionString());
			JobData = new DataTable();
			DA.Fill(JobData);

			//----------- Sort For Binding Structure----------
			DataTable DT = JobData.Copy();
			DT.Rows.Clear();
			DT.AcceptChanges();

			JobData.DefaultView.RowFilter = "Job_LEVEL=1";
			DataTable L1 = JobData.DefaultView.ToTable().Copy();
			JobData.DefaultView.RowFilter = "Job_LEVEL=2";
			DataTable L2 = JobData.DefaultView.ToTable().Copy();
			JobData.DefaultView.RowFilter = "Job_LEVEL=3";
			DataTable L3 = JobData.DefaultView.ToTable().Copy();
			for (int i = 0; i <= L1.Rows.Count - 1; i++) {
				DataRow R1 = DT.NewRow();
				R1.ItemArray = L1.Rows[i].ItemArray;
				DT.Rows.Add(R1);
				L2.DefaultView.RowFilter = "Job_Parent=" + R1["Job_ID"];
				for (int j = 0; j <= L2.DefaultView.Count - 1; j++) {
					DataRow R2 = DT.NewRow();
					R2.ItemArray = L2.DefaultView[j].Row.ItemArray;
					DT.Rows.Add(R2);
					L3.DefaultView.RowFilter = "Job_Parent=" + R2["Job_ID"];
					for (int k = 0; k <= L3.DefaultView.Count - 1; k++) {
						DataRow R3 = DT.NewRow();
						R3.ItemArray = L3.DefaultView[k].Row.ItemArray;
						DT.Rows.Add(R3);
					}
				}
			}
			JobData = DT.Copy();
			string[] Col = {
				"Job_LEVEL",
				"Job_No",
				"Job_ID",
				"Job_Parent",
				"Job_Name"
			};
			DT.DefaultView.RowFilter = "";
			DT = DT.DefaultView.ToTable(true, Col);
			rptJob.DataSource = DT;
			rptJob.DataBind();
			pnlAction.Visible = !string.IsNullOrEmpty(ddlOrganize.Text);

		}

		protected void rptHeaderClass_ItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
		{
			if (e.Item.ItemType != ListItemType.Item & e.Item.ItemType != ListItemType.AlternatingItem)
				return;
            Label lblClassName = (Label)e.Item.FindControl("lblClassName");
            DataRowView drv = (DataRowView)e.Item.DataItem;
            int _start = GL.CINT(drv["PNPO_CLASS_Start"]);
            int _end = GL.CINT(drv["PNPO_CLASS_End"]);

            lblClassName.Attributes["Class_ID"] =(drv["Class_ID"].ToString ());
			if (_start == _end) {
				lblClassName.Text = "พนักงานระดับ " + _start;
			} else {
				lblClassName.Text = "พนักงานระดับ " + _start + "-" + _end;
			}
		}

		protected void rptJob_ItemCommand(object source, System.Web.UI.WebControls.RepeaterCommandEventArgs e)
		{
			if (e.Item.ItemType != ListItemType.Item & e.Item.ItemType != ListItemType.AlternatingItem)
				return;

			Label lblName =(Label) e.Item.FindControl("lblName");

            DataTable DT;
            SqlCommandBuilder cmd;
            SqlDataAdapter DA;
            string SQL ;
            int CurrentNo;
            int TargetNo;

			switch (e.CommandName) {
				case "AddSub":

					Job_ID = 0;
					Job_LEVEL = Convert.ToInt32(lblName.Attributes["Job_LEVEL"]) + 1;
					Job_Parent = GL.CINT(lblName.Attributes["Job_ID"]);
					Job_No = 0;
					Class_ID = 0;
					Job_Name = "";
					EditMode = "AddSub";

					divModal.Style["visibility"] = "visible";
					lblHeader.Text = "เพิ่มงานย่อย";

					break;
				case "Delete":

					int[] TargetJobID = { GL.CINT((lblName.Attributes["Job_ID"])) };

					SQL = "SELECT * FROM tb_HR_Job_Mapping \n";
					SQL += "  WHERE PNDP_DEPT_CODE='" + DEPT_CODE.Replace("'", "''") + "' AND PNDP_MINOR_CODE='" + MINOR_CODE.Replace("'", "''") + "' \n";
					DA = new SqlDataAdapter(SQL, BL.ConnectionString());
					DT = new DataTable();
					DA.Fill(DT);

					switch (GL.CINT (lblName.Attributes["Job_LEVEL"])) {
						case 3:
							break;
						//-------- Not need to find child-------
						case 2:
							DT.DefaultView.RowFilter = "Job_Parent=" + lblName.Attributes["Job_ID"];
							for (int i = 0; i <= DT.DefaultView.Count - 1; i++) {
                                GL.PushArray_Integer(TargetJobID, GL.CINT(DT.DefaultView[i]["Job_ID"]));
							}

							break;
						case 1:
							DT.DefaultView.RowFilter = "Job_Parent=" + lblName.Attributes["Job_ID"];
							for (int i = 0; i <= DT.DefaultView.Count - 1; i++) {
								GL.PushArray_Integer(TargetJobID, GL.CINT(DT.DefaultView[i]["Job_ID"]));
								DT.DefaultView.RowFilter = "Job_Parent=" + DT.DefaultView[i]["Job_ID"];
								for (int j = 0; j <= DT.DefaultView.Count - 1; j++) {
                                    GL.PushArray_Integer(TargetJobID, GL.CINT(DT.DefaultView[j]["Job_ID"]));
								}
								//----------- Restore Previous Loop Filter --------
								DT.DefaultView.RowFilter = "Job_Parent=" + lblName.Attributes["Job_ID"];
							}

							break;
					}

					SqlConnection Conn = new SqlConnection(BL.ConnectionString());
					Conn.Open();
					SqlCommand COMM = new SqlCommand();
					var _with1 = COMM;
					_with1.CommandType = CommandType.Text;
					_with1.Connection = Conn;
					for (int i = TargetJobID.Length - 1; i >= 0; i += -1) {
						_with1.CommandText += "DELETE FROM tb_HR_Workload_Slot WHERE Job_ID=" + TargetJobID[i] + "\n";
						_with1.CommandText += "DELETE FROM tb_HR_Job_Mapping_Detail WHERE Job_ID=" + TargetJobID[i] + "\n";
						_with1.CommandText += "DELETE FROM tb_HR_Job_Mapping WHERE Job_ID=" + TargetJobID[i] + "\n";
						_with1.ExecuteNonQuery();
					}

					_with1.Dispose();
					Conn.Close();
					Conn.Dispose();
					//-------------- Re-order Job_no------------------
					if (GL.CINT (lblName.Attributes["Job_LEVEL"]) == 1) {
						//------------ Run In The Same SubScript Group
						SQL += " AND Job_LEVEL=1";
					} else {
						SQL += " AND Job_Parent=" + lblName.Attributes["Job_Parent"];
					}
					SQL += " ORDER BY Job_No";
					DA = new SqlDataAdapter(SQL, BL.ConnectionString());
					DT = new DataTable();
					DA.Fill(DT);
					for (int i = 0; i <= DT.Rows.Count - 1; i++) {
						DT.Rows[i]["JOB_No"] = i + 1;
					}

					try {
						 cmd = new SqlCommandBuilder(DA);
						DA.Update(DT);
					} catch (Exception ex) {
					}
					BindClass();
					BindJob();

					break;
				case "Up":
					 CurrentNo = GL.CINT(lblName.Attributes["Job_No"]);
					 TargetNo = CurrentNo - 1;
					 SQL = "SELECT * FROM tb_HR_Job_Mapping \n";
					SQL += "  WHERE PNDP_DEPT_CODE='" + DEPT_CODE.Replace("'", "''") + "' AND PNDP_MINOR_CODE='" + MINOR_CODE.Replace("'", "''") + "' \n";
					switch (GL.CINT (lblName.Attributes["Job_LEVEL"])) {
						case 1:
							SQL += "  AND Job_LEVEL=1 AND Job_No IN (" + CurrentNo + "," + TargetNo + ")";
							break;
						case 2:
						case 3:
							SQL += "  AND Job_Parent=" + lblName.Attributes["Job_Parent"] + " AND Job_No IN (" + CurrentNo + "," + TargetNo + ")";
							break;
					}
					 DA = new SqlDataAdapter(SQL, BL.ConnectionString());
					 DT = new DataTable();
					DA.Fill(DT);
					for (int i = 0; i <= DT.Rows.Count - 1; i++) {
						if (GL.CINT(DT.Rows[i]["Job_No"]) == CurrentNo) {
							DT.Rows[i]["Job_No"] = TargetNo;
						} else if (GL.CINT(DT.Rows[i]["Job_No"]) == TargetNo) {
							DT.Rows[i]["Job_No"] = CurrentNo;
						}
					}

					 cmd = new SqlCommandBuilder(DA);
					DA.Update(DT);
					BindClass();
					BindJob();

					break;
				case "Down":
					 CurrentNo = GL.CINT(lblName.Attributes["Job_No"]);
					 TargetNo = CurrentNo + 1;

					 SQL = "SELECT * FROM tb_HR_Job_Mapping \n";
					SQL += "  WHERE PNDP_DEPT_CODE='" + DEPT_CODE.Replace("'", "''") + "' AND PNDP_MINOR_CODE='" + MINOR_CODE.Replace("'", "''") + "' \n";
					switch (GL.CINT(lblName.Attributes["Job_LEVEL"])) {
						case 1:
							SQL += "  AND Job_LEVEL=1 AND Job_No IN (" + CurrentNo + "," + TargetNo + ")";
							break;
						case 2:
						case 3:
							SQL += "  AND Job_Parent=" + lblName.Attributes["Job_Parent"] + " AND Job_No IN (" + CurrentNo + "," + TargetNo + ")";
							break;
					}
					 DA = new SqlDataAdapter(SQL, BL.ConnectionString());
					 DT = new DataTable();
					DA.Fill(DT);
					for (int i = 0; i <= DT.Rows.Count - 1; i++) {
						if (GL.CINT(DT.Rows[i]["Job_No"]) == CurrentNo) {
							DT.Rows[i]["Job_No"] = TargetNo;
						} else if (GL.CINT(DT.Rows[i]["Job_No"]) == TargetNo) {
							DT.Rows[i]["Job_No"] = CurrentNo;
						}
					}

					 cmd = new SqlCommandBuilder(DA);
					DA.Update(DT);
					BindClass();
					BindJob();

					break;
			}
		}

		protected void rptJob_ItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
		{
			if (e.Item.ItemType != ListItemType.Item & e.Item.ItemType != ListItemType.AlternatingItem)
				return;

			HtmlTableCell tdIndent =(HtmlTableCell) e.Item.FindControl("tdIndent");
			HtmlTableCell tdLevel =(HtmlTableCell) e.Item.FindControl("tdLevel");
			Label lblName =(Label) e.Item.FindControl("lblName");
			Label lblDisplayName =(Label) e.Item.FindControl("lblDisplayName");
			Repeater rptSub =(Repeater) e.Item.FindControl("rptSub");
            AjaxControlToolkit.ConfirmButtonExtender cfmBtnDelete = (AjaxControlToolkit.ConfirmButtonExtender)e.Item.FindControl("cfmBtnDelete");

            DataRowView drv = (DataRowView)e.Item.DataItem;
			lblName.Attributes["Job_ID"] = drv["Job_ID"].ToString ();
            lblName.Attributes["Job_LEVEL"] = drv["Job_LEVEL"].ToString();
            lblName.Attributes["Job_No"] = drv["Job_No"].ToString();
			lblName.Attributes["Job_Parent"] = drv["Job_Parent"].ToString();
            lblName.Text = drv["Job_Name"].ToString();

            HtmlAnchor btnInsert = (HtmlAnchor)e.Item.FindControl("btnInsert");
            HtmlAnchor btnAppend = (HtmlAnchor)e.Item.FindControl("btnAppend");
            LinkButton btnAddSub = (LinkButton)e.Item.FindControl("btnAddSub");
            LinkButton btnUp = (LinkButton)e.Item.FindControl("btnUp");
            LinkButton btnDown = (LinkButton)e.Item.FindControl("btnDown");
            LinkButton btnDelete = (LinkButton)e.Item.FindControl("btnDelete");

			//----------- Set Up Button -------------------------
			btnUp.Visible = GL.CINT(drv["Job_No"]) > 1;

            DataTable DT;

			switch (GL.CINT (drv["Job_LEVEL"])) {
				case 1:
					tdIndent.Visible = false;
					tdLevel.ColSpan = DeptClass.Rows.Count + 2;

					lblDisplayName.Text = drv["Job_No"] + "." + drv["Job_Name"].ToString ().Replace("\n", "<br>");
					lblDisplayName.Font.Bold = true;

					//-------------- Has Sub ------------
					DT = JobData.Copy();

					btnDelete.Text = "<i class='icon-trash'></i>ลบ Task นี้";
					cfmBtnDelete.ConfirmText = "ยืนยันลบ Task นี้";
					//----------- Set btn------------
					btnAddSub.Visible = true;
					//----------- Set Down Button ---------
					btnDown.Visible = drv["Job_No"] != DT.Compute("MAX(Job_No)", "Job_LEVEL=1");
					//----------- Set btnInsert -----------
					btnInsert.InnerHtml = "<i class='icon-indent-right'></i> แทรก Task ก่อนหน้า";
					btnAppend.InnerHtml = "<i class='icon-indent-right'></i> เพิ่ม Task ถัดไป";

					break;
				case 2:
					tdIndent.Visible = true;

					lblDisplayName.Text = drv["Job_Name"].ToString ().Replace("\n", "<br>");

					//------------ Check Has Sub ----------------
					DT = JobData.Copy();


					int _display_parent_no = GL.CINT(DT.Compute("MAX(Job_No)", "Job_ID=" + drv["Job_Parent"]));

					DT.DefaultView.RowFilter = "Job_LEVEL=2 AND Job_Parent=" + drv["Job_Parent"];
					//--------- หารายการที่อยู่ใน Sub เดียวกัน ------------ 
					DataTable tmp = DT.DefaultView.ToTable().Copy();
					for (int i = 0; i <= tmp.Rows.Count - 1; i++) {
						DT.DefaultView.RowFilter = "Job_Parent=" + tmp.Rows[i]["Job_ID"];
						if (DT.DefaultView.Count > 0) {
							lblDisplayName.Text = _display_parent_no + "." + drv["Job_No"] + " " + drv["Job_Name"].ToString ().Replace("\n", "<br>");
							lblDisplayName.Font.Bold = true;
						}
					}


					//-------------- Has Sub ------------
                    int count_child= GL.CINT(DT.Compute("COUNT(Job_ID)", "Job_LEVEL=3 AND Job_Parent=" + drv["Job_ID"]));

                    if (count_child>0)
                    {
						tdLevel.ColSpan = DeptClass.Rows.Count + 1;
					} else {
						//-------------- No Sub ------------
						tdLevel.Style["padding-left"] = "5px";
						DT.DefaultView.RowFilter = "Job_LEVEL=2 AND Job_ID=" + drv["Job_ID"];
						tmpJob = DT.DefaultView.ToTable().Copy();
						rptSub.ItemDataBound += rptSub_ItemDataBound;
						rptSub.DataSource = DeptClass;
						rptSub.DataBind();
					}

					btnDelete.Text = "<i class='icon-trash'></i>ลบงานนี้";
					cfmBtnDelete.ConfirmText = "ยืนยันลบงานนี้";

					//----------- Set btn------------
					btnAddSub.Visible = true;
					//----------- Set Down Button ---------
					btnDown.Visible = drv["Job_No"] != DT.Compute("MAX(Job_No)", "Job_Parent=" + drv["Job_Parent"]);
					//----------- Set btnInsert -----------
					btnInsert.InnerHtml = "<i class='icon-indent-right'></i> แทรกงานก่อนหน้า";
					btnAppend.InnerHtml = "<i class='icon-indent-right'></i> เพิ่มงานถัดไป";
					break;
				case 3:
					tdIndent.Visible = true;
					DT = JobData.Copy();
					//-------------- No Sub ------------
					lblDisplayName.Text = drv["Job_Name"].ToString ().Replace("\n", "<br>");
					tdLevel.Style["padding-left"] = "30px";
					DT.DefaultView.RowFilter = " Job_ID=" + drv["Job_ID"];
					tmpJob = DT.DefaultView.ToTable().Copy();
					rptSub.ItemDataBound += rptSub_ItemDataBound;
					rptSub.DataSource = DeptClass;
					rptSub.DataBind();

					btnDelete.Text = "<i class='icon-trash'></i>ลบงานนี้";
					cfmBtnDelete.ConfirmText = "ยืนยันลบงานนี้";
					//----------- Set btn------------
					btnAddSub.Visible = false;
					//----------- Set Down Button ---------
					btnDown.Visible = drv["Job_No"] != DT.Compute("MAX(Job_No)", "Job_Parent=" + drv["Job_Parent"]);
					//----------- Set btnInsert -----------
					btnInsert.InnerHtml = "<i class='icon-indent-right'></i> แทรกงานก่อนหน้า";
					btnAppend.InnerHtml = "<i class='icon-indent-right'></i> เพิ่มงานถัดไป";
					break;
			}

			//----------- Editable Cell --------------------------
			string Script = "showDialog(" + drv["Job_ID"] + "," + drv["Job_LEVEL"] + ",";
			if (!GL.IsEqualNull(drv["Job_Parent"])) {
				Script += drv["Job_Parent"];
			} else {
				Script += "0";
			}
			Script += "," + drv["Job_No"] + ",0,'" + lblName.Text.Replace("'", "\\'").Replace("\n", "\\n") + "','EditJob','รายละเอียดงาน');";
			tdLevel.Attributes["onClick"] = Script;
			//----------- Set btnInsert----------
			if (GL.CINT (drv["Job_LEVEL"]) == 1) {
				Script = "showDialog(" + drv["Job_ID"] + "," + drv["Job_LEVEL"] + ",0," + drv["Job_No"] + ",0,'','Insert','เพิ่มงานหลัก (Task)')";
			} else {
				Script = "showDialog(" + drv["Job_ID"] + "," + drv["Job_LEVEL"] + "," + drv["Job_Parent"] + "," + drv["Job_No"] + ",0,'','Insert','เพิ่มรายละเอียดงาน')";
			}
			btnInsert.Attributes["onClick"] = Script;

			//----------- Set btnAppend----------
			btnAppend.Attributes["onClick"] = Script;
			if (GL.CINT (drv["Job_LEVEL"]) == 1) {
				Script = "showDialog(" + drv["Job_ID"] + "," + drv["Job_LEVEL"] + ",0," + drv["Job_No"] + ",0,'','Append','เพิ่มงานหลัก (Task)')";
			} else {
				Script = "showDialog(" + drv["Job_ID"] + "," + drv["Job_LEVEL"] + "," + drv["Job_Parent"] + "," + drv["Job_No"] + ",0,'','Append','เพิ่มรายละเอียดงาน')";
			}
			btnAppend.Attributes["onClick"] = Script;

		}

		DataTable tmpJob;
		protected void rptSub_ItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
		{
			if (e.Item.ItemType != ListItemType.Item & e.Item.ItemType != ListItemType.AlternatingItem)
				return;

			Label lblTask =(Label) e.Item.FindControl("lblTask");
			HtmlTableCell tdTask =(HtmlTableCell) e.Item.FindControl("tdTask");
            DataRowView drv = (DataRowView)e.Item.DataItem;
			lblTask.Attributes["Class_ID"] = drv["Class_ID"].ToString ();
			tmpJob.DefaultView.RowFilter = "Class_ID=" + drv["Class_ID"];

			//------------ แสดงรายการที่มีอยู่แล้ว เท่านั้น ------------
			if (tmpJob.DefaultView.Count > 0) {
				lblTask.Text = tmpJob.DefaultView[0]["Job_Detail"].ToString().Replace("\n", "<br>");
			}

			//----------- หาว่า LEVEL นี้ สามารถ Edit ได้หรือไม่ -----------
			RepeaterItem ParentItem = (RepeaterItem)e.Item.Parent.Parent;
			Label lblName =(Label) ParentItem.FindControl("lblName");
			HtmlTableCell tdLevel =(HtmlTableCell) ParentItem.FindControl("tdLevel");

			int _Job_ID = GL.CINT(lblName.Attributes["Job_ID"]);
			//
			int _Job_LEVEL = GL.CINT(lblName.Attributes["Job_LEVEL"]);
			//
			int _Class_ID = GL.CINT(drv["Class_ID"]);
			//

			//-------------- หาว่าอยู่ Column ไหน -------------
			DataTable cls = DeptClass.Copy();
			cls.DefaultView.RowFilter = "Class_ID=" + _Class_ID;
			string HeaderText = lblName.Text.Replace("\n", "\\n").Replace("'", "\"");
			if (HeaderText.Length > 100) {
				HeaderText = HeaderText.Substring(0, 100) + "...";
			}
			if (cls.DefaultView.Count > 0) {
                int _start = GL.CINT( cls.DefaultView[0]["PNPO_CLASS_Start"]);
                int _end = GL.CINT(cls.DefaultView[0]["PNPO_CLASS_End"]);
				if (_start == _end) {
					HeaderText += " สำหรับพนักงานระดับ " + _start;
				} else {
					HeaderText += " สำหรับพนักงานระดับ " + _start + "-" + _end;
				}
			}

			//-------------- Check Editatble For this Cell-----------
			//If _Job_LEVEL = 3 Or tdLevel.ColSpan = 2 Then
			//If _Job_LEVEL = 3 Or Then
			tdTask.Attributes["onClick"] = "showDialog(" + _Job_ID + "," + _Job_LEVEL + ",0,0," + _Class_ID + ",'" + lblTask.Text.Replace("'", "\\'").Replace("<br>", "\\n") + "','EditDetail','" + HeaderText + "');";
			//Else
			tdTask.Style["Cursor"] = "Pointer";
			//End If

		}

		#region "Edit Property"
		public int Job_ID {
			get { return Convert.ToInt32(txt_Job_ID.Text); }
			set { txt_Job_ID.Text = value.ToString (); }
		}
		public int Job_LEVEL {
			get { return Convert.ToInt32(txt_Job_LEVEL.Text); }
			set { txt_Job_LEVEL.Text = value.ToString (); }
		}
		public int Job_Parent {
			get { return Convert.ToInt32(txt_Job_Parent.Text); }
			set { txt_Job_Parent.Text = value.ToString (); }
		}
		public int Job_No {
			get { return Convert.ToInt32(txt_Job_No.Text); }
			set { txt_Job_No.Text = value.ToString (); }
		}
		public int Class_ID {
			get { return Convert.ToInt32(txt_Class_ID.Text); }
			set { txt_Class_ID.Text = value.ToString (); }
		}
		public string Job_Name {
			get { return txtDialog.Text; }
			set { txtDialog.Text = value.ToString (); }
		}
		public string EditMode {
			get { return txt_Mode.Text; }
			set { txt_Mode.Text = value.ToString (); }
		}
		#endregion



		protected void btnOK_Click(object sender, System.EventArgs e)
		{

            string SQL;
            DataTable DT;
            SqlCommandBuilder cmd;
            SqlDataAdapter DA;
            DataRow DR;
            object _job_no;

			switch (EditMode) {
				case "AppendJob":
					SQL = "SELECT * FROM tb_HR_Job_Mapping \n";
					SQL += "  WHERE PNDP_DEPT_CODE='" + DEPT_CODE.Replace("'", "''") + "' AND PNDP_MINOR_CODE='" + MINOR_CODE.Replace("'", "''") + "' \n";
					DA = new SqlDataAdapter(SQL, BL.ConnectionString());
					DT = new DataTable();
					DA.Fill(DT);

					DR = DT.NewRow();
					DR["Job_ID"] = BL.GetNewJobMappingID();
					DR["PNDP_DEPT_CODE"] = DEPT_CODE;
					DR["PNDP_MINOR_CODE"] = MINOR_CODE;
					DR["Job_LEVEL"] = 1;
					DR["Job_Parent"] = DBNull.Value;

                    _job_no = DT.Compute("MAX(Job_No)", "Job_LEVEL=1");                   
                    Job_No = (object.Equals(_job_no,DBNull.Value)) ? 1 : GL.CINT(_job_no) + 1; 
					
                    DR["Job_No"] = Job_No;
					DR["Job_Name"] = txtDialog.Text;
					DR["Update_By"] = PSNL_NO;
					DR["Update_Time"] = DateAndTime.Now;

					DT.Rows.Add(DR);
					 cmd = new SqlCommandBuilder(DA);
					DA.Update(DT);

					break;
				case "AddSub":

					SqlConnection Conn = new SqlConnection(BL.ConnectionString());
					Conn.Open();

					 SQL = "SELECT * FROM tb_HR_Job_Mapping \n";
					SQL += "  WHERE PNDP_DEPT_CODE='" + DEPT_CODE.Replace("'", "''") + "' AND PNDP_MINOR_CODE='" + MINOR_CODE.Replace("'", "''") + "' \n";
					 DA = new SqlDataAdapter(SQL, Conn);
					 DT = new DataTable();
					DA.Fill(DT);

					 DR = DT.NewRow();
					DR["Job_ID"] = BL.GetNewJobMappingID();
					DR["PNDP_DEPT_CODE"] = DEPT_CODE;
					DR["PNDP_MINOR_CODE"] = MINOR_CODE;
					DR["Job_LEVEL"] = Job_LEVEL;
					DR["Job_Parent"] = Job_Parent;
                    
                    _job_no =  DT.Compute("MAX(Job_No)", "Job_LEVEL=" + Job_LEVEL + " AND Job_Parent=" + Job_Parent);
                    Job_No = (object.Equals(_job_no, DBNull.Value)) ? 1 :GL.CINT(_job_no) + 1;
                    DR["Job_No"] = Job_No;
					DR["Job_Name"] = txtDialog.Text;
					DR["Update_By"] = PSNL_NO;
					DR["Update_Time"] = DateAndTime.Now;

					DT.Rows.Add(DR);
					 cmd = new SqlCommandBuilder(DA);
					DA.Update(DT);
					//----------------- ลบ Detail เก่ากรณี เพิ่ม Sub ของ LEVEL 2 แต่ LEVEL 2 มีการกรอก Detail อยู่ก่อนแล้ว เพราะ ตอนนี้ LEVEL 2 ทำตัวเป็น Parent -------
					if (Job_LEVEL == 3) {
						SqlCommand COMM = new SqlCommand();
						var _with2 = COMM;
						_with2.CommandType = CommandType.Text;
						_with2.Connection = Conn;
						_with2.CommandText += "DELETE FROM tb_HR_Job_Mapping_Detail WHERE Job_ID=" + Job_Parent + "\n";
						_with2.ExecuteNonQuery();
						_with2.Dispose();
					}
					//---------------------------------------------------------------------------------------------------------------------------
					Conn.Close();
					Conn.Dispose();

					break;
				case "EditJob":
					SQL = "SELECT * FROM tb_HR_Job_Mapping \n";
					SQL += "  WHERE PNDP_DEPT_CODE='" + DEPT_CODE.Replace("'", "''") + "' AND PNDP_MINOR_CODE='" + MINOR_CODE.Replace("'", "''") + "' \n";
					SQL += "  AND Job_ID=" + Job_ID;

					DA = new SqlDataAdapter(SQL, BL.ConnectionString());
					DT = new DataTable();
					DA.Fill(DT);
					 DR = null;
					if (DT.Rows.Count == 0) {
						DR = DT.NewRow();
						DR["Job_ID"] = Job_ID;
						DR["PNDP_DEPT_CODE"] = DEPT_CODE;
						DR["PNDP_MINOR_CODE"] = MINOR_CODE;
					} else {
						DR = DT.Rows[0];
					}
					DR["Job_LEVEL"] = Job_LEVEL;
					if (Information.IsNumeric(Job_Parent) && Job_Parent > 0) {
						DR["Job_Parent"] = Job_Parent;
					} else {
						DR["Job_Parent"] = DBNull.Value;
					}
					DR["Job_No"] = Job_No;
					DR["Job_Name"] = Job_Name;
					DR["Update_By"] = PSNL_NO;
					DR["Update_Time"] = DateAndTime.Now;

					if (DT.Rows.Count == 0)
						DT.Rows.Add(DR);
					 cmd = new SqlCommandBuilder(DA);
					DA.Update(DT);

					break;
				case "EditDetail":
					//------------- Edit ระดับ Class -----------

					//---------- Delete -----------
					if (string.IsNullOrEmpty(Job_Name)) {
						 Conn = new SqlConnection(BL.ConnectionString());
						Conn.Open();
						SqlCommand COMM = new SqlCommand();
						var _with3 = COMM;
						_with3.CommandType = CommandType.Text;
						_with3.Connection = Conn;
						_with3.CommandText += "DELETE FROM tb_HR_Job_Mapping_Detail WHERE Job_ID=" + Job_ID + " AND Class_ID=" + Class_ID + "\n";
						_with3.ExecuteNonQuery();
						_with3.Dispose();
						Conn.Close();
						Conn.Dispose();
					} else {
						 SQL = "SELECT * FROM tb_HR_Job_Mapping_Detail WHERE Job_ID=" + Job_ID + " AND Class_ID=" + Class_ID;
						 DA = new SqlDataAdapter(SQL, BL.ConnectionString());
						 DT = new DataTable();
						DA.Fill(DT);
						 DR = null;
						if (DT.Rows.Count == 0) {
							DR = DT.NewRow();
							DR["Job_ID"] = Job_ID;
							DR["Class_ID"] = Class_ID;
						} else {
							DR = DT.Rows[0];
						}
						DR["Job_Name"] = Job_Name;
						DR["Update_By"] = PSNL_NO;
						DR["Update_Time"] = DateAndTime.Now;
						if (DT.Rows.Count == 0)
							DT.Rows.Add(DR);
						 cmd = new SqlCommandBuilder(DA);
						DA.Update(DT);
					}

					break;
				case "Insert":
				case "Append":

					//------------ บันทึก Record ใหม่ -----------
					 SQL = "SELECT * FROM tb_HR_Job_Mapping \n";
					SQL += "  WHERE PNDP_DEPT_CODE='" + DEPT_CODE.Replace("'", "''") + "' AND PNDP_MINOR_CODE='" + MINOR_CODE.Replace("'", "''") + "' \n";
					SQL += " AND Job_LEVEL=" + Job_LEVEL;
					 DA = new SqlDataAdapter(SQL, BL.ConnectionString());
					 DT = new DataTable();
					DA.Fill(DT);
					 DR = DT.NewRow();
					DR["Job_ID"] = BL.GetNewJobMappingID();
					DR["PNDP_DEPT_CODE"] = DEPT_CODE;
					DR["PNDP_MINOR_CODE"] = MINOR_CODE;
					DR["Job_LEVEL"] = Job_LEVEL;
					if (Job_LEVEL == 1) {
						DR["Job_Parent"] = DBNull.Value;
					} else {
						DR["Job_Parent"] = Job_Parent;
					}
					DR["Job_No"] = 0;
					DR["Job_Name"] = txtDialog.Text;
					DR["Update_By"] = PSNL_NO;
					DR["Update_Time"] = DateAndTime.Now;
					DT.Rows.Add(DR);
					 cmd = new SqlCommandBuilder(DA);
					DA.Update(DT);
					DT.AcceptChanges();
					//---------- Order Job No---------
					int TargetPos = (EditMode == "Insert" ? GL.CINT(Job_No) : GL.CINT(Job_No) + 1);
					//--------- ต่างกันแค่ตรงนี้

					//----------- ถ้าไม่ใช่หมวด ให้คิดเฉพาะรายการที่อยู่ใน Subscript เดียวกัน -------------
					if (Job_LEVEL != 1) {
						DT.DefaultView.RowFilter = "Job_Parent=" + Job_Parent;
					}
					//------------------------------------------------
					for (int i = 0; i <= DT.DefaultView.Count - 1; i++) {
						if (GL.CINT(DT.DefaultView[i]["Job_No"]) >= TargetPos) {
                            int tmp = GL.CINT(DT.DefaultView[i]["Job_No"]) + 1;
                            DT.DefaultView[i]["Job_No"] = tmp;
						}
					}

					DR["Job_No"] = TargetPos;
					cmd = new SqlCommandBuilder(DA);
					DA.Update(DT);
					DT.AcceptChanges();

					break;
			}

			divModal.Style["visibility"] = "hidden";
			BindClass();
			BindJob();

		}

		protected void btnClear_Click(object sender, System.EventArgs e)
		{
			SqlConnection Conn = new SqlConnection(BL.ConnectionString());
			Conn.Open();
			SqlCommand Comm = new SqlCommand();
			var _with4 = Comm;
			_with4.CommandType = CommandType.Text;
			_with4.CommandText = " DELETE FROM tb_HR_Workload_Slot WHERE Job_ID IN (SELECT Job_ID FROM tb_HR_Job_Mapping WHERE PNDP_DEPT_CODE='" + DEPT_CODE.Replace("'", "''") + "' AND PNDP_MINOR_CODE='" + MINOR_CODE.Replace("'", "''") + "' )\n";
			_with4.CommandText += " DELETE FROM tb_HR_Job_Mapping_Detail WHERE Job_ID IN (SELECT Job_ID FROM tb_HR_Job_Mapping WHERE PNDP_DEPT_CODE='" + DEPT_CODE.Replace("'", "''") + "' AND PNDP_MINOR_CODE='" + MINOR_CODE.Replace("'", "''") + "' )\n";
			_with4.CommandText += " DELETE FROM tb_HR_Workload_PSN WHERE DEPT_CODE='" + DEPT_CODE.Replace("'", "''") + "' AND MINOR_CODE='" + MINOR_CODE.Replace("'", "''") + "' \n";
			_with4.CommandText += " DELETE FROM tb_HR_Job_Mapping WHERE PNDP_DEPT_CODE='" + DEPT_CODE.Replace("'", "''") + "' AND PNDP_MINOR_CODE='" + MINOR_CODE.Replace("'", "''") + "' \n";
			_with4.Connection = Conn;
			_with4.ExecuteNonQuery();
			_with4.Dispose();
			Conn.Close();
			Conn.Dispose();
			BindClass();
			BindJob();
		}

		public AssTeam_WKL_JobMapping()
		{
			Load += Page_Load;
		}


	}
}

﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" UICulture="TH" Culture="th-TH" CodeBehind="RPT_A_DEPTAssCompleted.aspx.cs" Inherits="VB.RPT_A_DEPTAssCompleted" %>

<%@ Register src="WUC_DateReporter.ascx" tagname="WUC_DateReporter" tagprefix="uc1" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<%@ Register src="PageNavigation.ascx" tagname="PageNavigation" tagprefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<asp:ToolkitScriptManager ID="toolkit1" runat="server" EnableScriptGlobalization="true" ></asp:ToolkitScriptManager>
<asp:UpdatePanel ID="UpdatePanel1" runat="server">
<ContentTemplate>

<div class="container-fluid">
				<!-- BEGIN PAGE HEADER-->
				<div class="row-fluid" id="fixedPageHeader">
					<div class="span12"> 
						<!-- BEGIN PAGE TITLE & BREADCRUMB-->
						<h3>การส่งคะแนนประเมินผล KPI และ Competency <font color="blue">(ScreenID : R-ALL-06)</font></h3>						
						<ul class="breadcrumb">
                            <li>
                            	<i class="icon-copy"></i> <a href="javascript:;">รายงาน</a><i class="icon-angle-right"></i>
                            </li>
                            <li>
                            	<i class="icon-copy"></i> <a href="javascript:;">การประเมินผลตัวชี้วัดและสมรรถนะ</a><i class="icon-angle-right"></i>
                            </li>
                            <li>
                        	    <i class="icon-check"></i> <a href="javascript:;">การส่งคะแนนประเมินผล KPI และ Competency</a>
                        	</li>                      	
                        	
                            <uc1:WUC_DateReporter ID="WUC_DateReporter1" runat="server" />
                        </ul>
						<!-- END PAGE TITLE & BREADCRUMB-->								        
				     </div>                     		
			    </div>
		        <!-- END PAGE HEADER-->
				
                <asp:Panel ID="pnlList" runat="server" Visible="True" DefaultButton="btnSearch">
				<div class="row-fluid">
					
					<div class="span12">
						<!-- BEGIN SAMPLE TABLE PORTLET-->
                                        <div class="row-fluid form-horizontal">
                			                  <div class="btn-group pull-left">
                                                    <asp:Button ID="btnSearch" OnClick="btnSearch_Click"  runat="server" CssClass="btn green" Text="ค้นหา" />
                                              </div> 
				                            <div class="btn-group pull-right">                                    
								                <button data-toggle="dropdown" class="btn dropdown-toggle" id="Button1">พิมพ์ <i class="icon-angle-down"></i></button>										
								                <ul class="dropdown-menu pull-right">                                                       
                                                    <li><a href="Print/RPT_A_DEPTAssCompleted.aspx?Mode=PDF" target="_blank">รูปแบบ PDF</a></li>
                                                    <li><a href="Print/RPT_A_DEPTAssCompleted.aspx?Mode=EXCEL" target="_blank">รูปแบบ Excel</a></li>											
										        </ul>
							                </div>
									     </div>      
						                 
												
												<div class="row-fluid form-horizontal">
											         <div class="span4 ">
												        <div class="control-group">
											                <label class="control-label"> ฝ่าย</label>
											                <div class="controls">
												                <asp:DropDownList ID="ddlSector" OnSelectedIndexChanged="ddlSector_SelectedIndexChanged" runat="server" AutoPostBack="true" CssClass="medium m-wrap">
												                </asp:DropDownList>
											                </div>
										                </div>
											        </div>	
    												
											         <div class="span3 ">
												        <div class="control-group">
											                <label class="control-label"> หน่วยงาน</label>
											                <div class="controls">
												                <asp:DropDownList ID="ddlDept" runat="server" CssClass="medium m-wrap">
												                </asp:DropDownList>		
											                </div>
										                </div>
											        </div>		
											     </div>
											     
											     <div class="row-fluid form-horizontal">
						                             <div class="span4 ">
														<div class="control-group">
													        <label class="control-label"> รอบการประเมิน</label>
													        <div class="controls">
														        <asp:DropDownList ID="ddlRound" runat="server" CssClass="medium m-wrap">
							                                    </asp:DropDownList>
													        </div>
												        </div>
													</div>
													
													<div class="span8 ">
														<div class="control-group">
													        <label class="control-label">KPI ภายในวันที่</label>
													        <div class="controls">
                                                                <asp:DropDownList ID="ddlDateModeKPI" runat="server" CssClass="medium m-wrap" AutoPostBack="True" OnSelectedIndexChanged="ddlDateModeKPI_SelectedIndexChanged">
                                                                    <asp:ListItem Selected="True">กำหนดตามรอบ</asp:ListItem>
                                                                    <asp:ListItem>เลือกวันที่</asp:ListItem>
                                                                </asp:DropDownList>                                                                                                                        												        
                                                                <asp:TextBox ID="txtManualDateKPI" runat="server" CssClass="m-wrap medium" Visible="false"></asp:TextBox>
                                                                <asp:CalendarExtender TargetControlID="txtManualDateKPI" BehaviorID="txtManualDateKPI" ID="calManualDateKPI" runat="server"
                                                                Format="dd/MM/yyyy"></asp:CalendarExtender>
													        </div>
												        </div>
													</div>
                                           </div>
									       <div class="row-fluid form-horizontal">
                                                    <div class="span4 ">
														
													</div>
                                                    <div class="span8 ">
														<div class="control-group">
													        <label class="control-label">Competency ภายในวันที่</label>
													        <div class="controls">
                                                                <asp:DropDownList ID="ddlDateModeCOMP" runat="server" CssClass="medium m-wrap" AutoPostBack="True" OnSelectedIndexChanged="ddlDateModeCOMP_SelectedIndexChanged">
                                                                    <asp:ListItem Selected="True">กำหนดตามรอบ</asp:ListItem>
                                                                    <asp:ListItem>เลือกวันที่</asp:ListItem>
                                                                </asp:DropDownList>                                                                                                                        												        
                                                                <asp:TextBox ID="txtManualDateCOMP" runat="server" CssClass="m-wrap medium" Visible="false"></asp:TextBox>
                                                                <asp:CalendarExtender TargetControlID="txtManualDateCOMP" BehaviorID="txtManualDateCOMP" ID="calManualDateCOMP" runat="server"
                                                                Format="dd/MM/yyyy"></asp:CalendarExtender>
													        </div>
												        </div>
													</div>
                                           </div>
								            
							<div class="portlet-body no-more-tables">
								<asp:Label ID="lblCountPSN" runat="server" Font-Size="14px" Font-Bold="true"></asp:Label>
								<table class="table table-full-width table-bordered dataTable table-hover">
									<thead>
										<tr>
											<th style="text-align:center;" colspan="2"> ส่วนงาน</th>                                            
                                            <th style="text-align:center;" colspan="3"> KPI (กำหนดส่ง <asp:Label ID="lblKPI_Title" runat="server"></asp:Label>)</th>
                                            <th style="text-align:center;" colspan="3"> Competency (กำหนดส่ง <asp:Label ID="lblCOMP_Title" runat="server"></asp:Label>)</th>											
										</tr>
                                        <tr>
                                            <th style="text-align:center;">ฝ่าย</th>
                                            <th style="text-align:center;">หน่วยงาน</th>
                                            <th style="text-align:center; width:100px;">ส่งวันที่</th>
                                            <th style="text-align:center;">ภายในกำหนด</th>
                                            <th style="text-align:center;">ล่าช้ากว่ากำหนด</th>
                                            <th style="text-align:center; width:100px;">ส่งวันที่</th>
                                            <th style="text-align:center;">ภายในกำหนด</th>
                                            <th style="text-align:center;">ล่าช้ากว่ากำหนด</th>
                                        </tr>
									</thead>
									<tbody>
										<asp:Repeater ID="rptAll" OnItemDataBound="rptAll_ItemDataBound" runat="server">
									        <ItemTemplate>
									            <tr>    
									                <td data-title="ฝ่าย" id="td1" runat="server"><asp:Label ID="lblSector" runat="server"></asp:Label></td>
                                                    <td data-title="หน่วยงาน" id="td2" runat="server"><asp:Label ID="lblDept" runat="server"></asp:Label></td>
                                                    <td data-title="ส่ง KPI วันที่" id="td3" runat="server" style="text-align:center;"><asp:Label ID="lblKPIDate" runat="server"></asp:Label></td>
                                                    <td data-title="ส่ง KPI ภายในกำหนด" id="td4" runat="server" style="text-align:center;"><asp:Image ID="imgKPIOK" runat="server" Visible="false" ImageUrl="images/check.png"></asp:Image></td>
                                                    <td data-title="ส่ง KPI ช้ากว่ากำหนด" id="td5" runat="server" style="text-align:center;"><asp:Image ID="imgKPILate" runat="server" Visible="false" ImageUrl="images/check_red.png"></asp:Image></td>
                                                     <td data-title="ส่ง Competency วันที่" id="td6" runat="server" style="text-align:center;"><asp:Label ID="lblCOMPDate" runat="server"></asp:Label></td>
                                                    <td data-title="ส่ง Competency ภายในกำหนด" id="td7" runat="server" style="text-align:center;"><asp:Image ID="imgCOMPOK" runat="server" Visible="false" ImageUrl="images/check.png"></asp:Image></td>
                                                    <td data-title="ส่ง Competency ช้ากว่ากำหนด" id="td8" runat="server" style="text-align:center;"><asp:Image ID="imgCOMPLate" runat="server" Visible="false" ImageUrl="images/check_red.png"></asp:Image></td>
										        </tr>                                               
									        </ItemTemplate>
                                            <FooterTemplate>
                                                <tr>
                                                    <td colspan="2" rowspan="2" style="text-align:center; font-weight:bold;">รวมจำนวน</td>
                                                    <td style="text-align:center; font-weight:bold;">จำนวนส่วนงานที่แสดงทั้งหมด</td>
                                                    <td style="text-align:center; font-weight:bold;">จำนวนส่วนงานที่ส่ง KPI ภายในกำหนด</td>
                                                    <td style="text-align:center; font-weight:bold;">จำนวนส่วนงานที่ส่ง KPI ช้ากว่ากำหนด</td>
                                                    <td style="text-align:center; font-weight:bold;">จำนวนส่วนงานที่แสดงทั้งหมด</td>
                                                    <td style="text-align:center; font-weight:bold;">จำนวนส่วนงานที่ส่ง Competency ภายในกำหนด</td>
                                                    <td style="text-align:center; font-weight:bold;">จำนวนส่วนงานที่ส่ง Competency ช้ากว่ากำหนด</td>
                                                </tr>
                                                <tr>                                                    
                                                    <td data-title="จำนวนส่วนงานที่แสดงทั้งหมด" style="text-align:center; font-weight:bold;"><asp:Label ID="lblKPITotal" runat="server"></asp:Label></td>
                                                    <td data-title="จำนวนส่วนงานที่ส่ง KPI ภายในกำหนด"  style="text-align:center; font-weight:bold;"><asp:Label ID="lblKPIOK" runat="server"></asp:Label></td>
                                                    <td data-title="จำนวนส่วนงานที่ส่ง KPI ช้ากว่ากำหนด"  style="text-align:center; font-weight:bold;"><asp:Label ID="lblKPILate" runat="server"></asp:Label></td>
                                                    <td data-title="จำนวนส่วนงานที่แสดงทั้งหมด"  style="text-align:center; font-weight:bold;"><asp:Label ID="lblCOMPTotal" runat="server"></asp:Label></td>
                                                    <td data-title="จำนวนส่วนงานที่ส่ง Competency ภายในกำหนด"  style="text-align:center; font-weight:bold;"><asp:Label ID="lblCOMPOK" runat="server"></asp:Label></td>
                                                    <td data-title="จำนวนส่วนงานที่ส่ง Competency ช้ากว่ากำหนด"  style="text-align:center; font-weight:bold;"><asp:Label ID="lblCOMPLate" runat="server"></asp:Label></td>
                                                </tr>
                                            </FooterTemplate>
										</asp:Repeater>																							
									</tbody>
								</table>
								<asp:PageNavigation ID="Pager" OnPageChanging="Pager_PageChanging" MaximunPageCount="10" PageSize="20" runat="server" />
							</div>
					</div>
                </div>  
                </asp:Panel>  
</div>

</ContentTemplate>
</asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ScriptPlaceHolder" runat="server">
</asp:Content>

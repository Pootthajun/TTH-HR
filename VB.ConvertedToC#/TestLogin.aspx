﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TestLogin.aspx.cs" Inherits="VB.TestLogin" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">

    <asp:Label ID="Label1" runat="server" Text="User : "></asp:Label>
    <asp:TextBox ID="txtUser" runat="server" Width="200px"></asp:TextBox>
    <br />
    <asp:Label ID="Label2" runat="server" Text="Pass : "></asp:Label>
    <asp:TextBox ID="txtPass" runat="server" Width="200px"></asp:TextBox>
    <br />
    <br />
    <asp:RadioButton ID="rdoNew" runat="server" Text="Use AccountManagement (ใช้ Domain)" 
        Checked="True" GroupName="fn" /> &nbsp;:
    <asp:TextBox ID="txtDomain" runat="server" Width="200px" Text="tobacco.or.th"></asp:TextBox>
    <asp:Button ID="btnGetDomain" runat="server" onclick="btnGetDomain_Click" 
        Text="GetCurrent" />
<br /><br />
    <asp:RadioButton ID="rdoOld" runat="server" Text="Use DirectoryService (ใช้ Domain + LDAP)" 
        GroupName="fn" /> &nbsp;:
    <asp:TextBox ID="txtLDAP" runat="server" Width="200px" Text="LDAP://tobacco.or.th"></asp:TextBox>
    <asp:Button ID="btnGetLDAP" runat="server" Text="GetCurrent" 
        onclick="btnGetLDAP_Click" />
    <br />
    <br />
    <asp:RadioButton ID="rdoLDAP" runat="server" Text="Use LDAP Protocol (ใช้ LDAP อย่างเดียว)" 
        GroupName="fn" /> &nbsp;:
    <asp:TextBox ID="txtLDAPProtocol" runat="server" Width="200px" 
        Text="LDAP://tobacco.or.th"></asp:TextBox>
    <asp:Button ID="btnGetLDAPProtocol" runat="server" Text="GetCurrent" onclick="btnGetLDAPProtocol_Click" 
        />
    <br />
    
    <br />
    
    <asp:Button ID="btnSubmit" runat="server" Text="Submit" 
        onclick="btnSubmit_Click" />
    <br />
    <br />
    <asp:Label ID="lblResult" runat="server" Text="Result"></asp:Label>
    <br />

    </form>
</body>
</html>

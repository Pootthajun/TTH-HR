﻿<%@ Page  Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPage.Master" CodeBehind="AssessmentSetting_Assessor.aspx.cs" Inherits="VB.AssessmentSetting_Assessor" %>

<%@ Register src="WUC_DateReporter.ascx" tagname="WUC_DateReporter" tagprefix="uc1" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<%@ Register src="PageNavigation.ascx" tagname="PageNavigation" tagprefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <link type="text/css" rel="stylesheet" href="assets/plugins/font-awesome/css/font-awesome.css" />
	<link type="text/css" rel="stylesheet" href="assets/css/pages/search.css" />
	<link type="text/css" rel="stylesheet" href="assets/css/pages/profile.css" />
	<link type="text/css" rel="stylesheet" href="assets/plugins/chosen-bootstrap/chosen/chosen.css" />
	<style>
	    .portfolio-btn a:hover {
            background: none repeat scroll 0 0 red !important;
	    }
	</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<asp:ToolkitScriptManager ID="toolkit1" runat="server"></asp:ToolkitScriptManager>


    <div class="container-fluid">
                <!-- BEGIN PAGE HEADER-->
				<div class="row-fluid" id="fixedPageHeader">
					<div class="span12"> 
						<!-- BEGIN PAGE TITLE & BREADCRUMB-->
						<h3 class="page-user-md">ตำแหน่งผู้ประเมินหลัก <font color="blue">(ScreenID : S-HDR-10)</font>
                        <asp:Label ID="lblReportTime" Font-Size="14px" ForeColor="Green" runat="Server"></asp:Label>
                        </h3>					
									
						<ul class="breadcrumb">
                            <li>
                            	<i class="icon-th-list"></i><a href="javascript:;">การกำหนดข้อมูลการประเมิน</a><i class="icon-angle-right"></i>
                            </li>
                        	<li><i class="icon-user-md"></i> <a href="javascript:;">ตำแหน่งผู้ประเมินหลัก</a></li>                        	
                            <uc1:WUC_DateReporter ID="WUC_DateReporter1" runat="server" />
                        </ul>
						<!-- END PAGE TITLE & BREADCRUMB-->
									        
				     </div>
				 </div>
                 <!-- END PAGE HEADER-->
                 
                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>

                   <asp:Panel ID="pnlList" runat="server" Visible="False" DefaultButton="btnSearch">
                        <div class="row-fluid">        					
					        <div class="span12">
						        <!-- BEGIN SAMPLE TABLE PORTLET-->        								                           
							          <div class="btn-group pull-right">                                    
								            <button data-toggle="dropdown" class="btn dropdown-toggle">พิมพ์ <i class="icon-angle-down"></i></button>										
								            <ul class="dropdown-menu pull-right">
                                               
                                                <li><a href="Print/RPT_A_Assessor_Mapping.aspx?Mode=PDF" target="_blank">รูปแบบ PDF</a></li>
                                                <li><a href="Print/RPT_A_Assessor_Mapping.aspx?Mode=EXCEL" target="_blank">รูปแบบ Excel</a></li>												
								            </ul>
							            </div>
							          <div class="alert">
												<li>
													เมนูนี้ใช้กำหนดความสัมพันธ์ระหว่างตำแหน่งผู้ประเมินและผู้ถูกประเมิน  ตามระเบียบการประเมินของการยาสูบแห่งประเทศไทย 
													ซึ่งจะมีผลในทุกรอบการประเมินหลังจากนี้ 
												</li>
												<li>
												    โดยความสัมพันธ์ระหว่างผู้ประเมินและผู้ถูกประเมิน จะถูกกำหนดโดยเลขตำแหน่งที่ตั้งค่าในเมนูนี้โดยปริยาย
												</li>
												<li>
												    หากในรอบการประเมินที่ไม่มีผู้ประเมินถือครองตำแหน่งที่ระบุ เจ้าหน้าที่สามารถ "กำหนดผู้รักษาการแทน" ได้ในเมนู
												    <a href="AssessmentSetting_Assessor_Assigned.aspx">กำหนดข้อมูลการประเมิน > กำหนดผู้ประเมินรักษาการแทน</a>
												</li>
									  </div>
							           
							           <div class="row-fluid form-horizontal">
									          <div class="span6 ">
											    <div class="control-group">
										            <label class="control-label"><i class="icon-user-md"></i> ตำแหน่งผู้ประเมิน</label>
										            <div class="controls">
											            <asp:TextBox ID="txtSearch_ASS_POS" OnTextChanged="btnSearch_Click" runat="server" AutoPostBack="true" CssClass="m-wrap medium" placeholder="เลขตำแหน่ง/ชื่อตำแหน่ง"></asp:TextBox>
											            <asp:Button ID="btnSearch" OnClick="btnSearch_Click" runat="server" Style="display:none" />
											        </div>
									            </div>
										    </div>	
										    <div class="span6 ">
											    <div class="control-group">
										            <label class="control-label"><i class="icon-user"></i> ตำแหน่งผู้ถูกประเมิน</label>
										            <div class="controls">
											            <asp:TextBox ID="txtSearch_PSN_POS" OnTextChanged="btnSearch_Click" runat="server" AutoPostBack="true" CssClass="m-wrap medium" placeholder="เลขตำแหน่ง/ชื่อตำแหน่ง"></asp:TextBox>
										            </div>
									            </div>
										    </div>	
									     </div>
									     <div class="row-fluid form-horizontal">
									          <div class="span6 ">
											    <div class="control-group">
										            <label class="control-label"><i class="icon-sitemap"></i> หน่วยงานผู้ประเมิน</label>
										            <div class="controls">
											            <asp:TextBox ID="txtSearch_ASS_DEPT" OnTextChanged="btnSearch_Click" runat="server" AutoPostBack="true" CssClass="m-wrap medium" placeholder="ชื่อหน่วยงาน"></asp:TextBox>
											        </div>
									            </div>
										    </div>	
										    <div class="span6 ">
											    <div class="control-group">
										            <label class="control-label"><i class="icon-sitemap"></i> หน่วยงานผู้ถูกประเมิน</label>
										            <div class="controls">
											            <asp:TextBox ID="txtSearch_PSN_DEPT" OnTextChanged="btnSearch_Click" runat="server" AutoPostBack="true" CssClass="m-wrap medium" placeholder="ชื่อหน่วยงาน"></asp:TextBox>
										            </div>
									            </div>
										    </div>	
									     </div>
                                         <div class="row-fluid form-horizontal">
									          <div class="span6 ">
											    <div class="control-group">
										            <label class="control-label"><i class="icon-user-md"></i> ชื่อผู้ประเมิน</label>
										            <div class="controls">
											            <asp:TextBox ID="txtSearch_MGR_CODE" OnTextChanged="btnSearch_Click" runat="server" AutoPostBack="true" CssClass="m-wrap medium" placeholder="ค้นหาชื่อผู้ประเมิน/เลขประจำตัว"></asp:TextBox>
											        </div>
									            </div>
										    </div>	
										    <div class="span6 ">
                                                <div class="control-group">
										            <label class="control-label"><i class="icon-user"></i> ชื่อผู้ถูกประเมิน</label>
										            <div class="controls">
											            <asp:TextBox ID="txtSearch_PSN_CODE" OnTextChanged="btnSearch_Click" runat="server" AutoPostBack="true" CssClass="m-wrap medium" placeholder="ค้นหาชื่อผู้ถูกประเมิน/เลขประจำตัว"></asp:TextBox>
										            </div>
									            </div>
                                                <div class="control-group">
													        <label class="control-label"><i class="icon-male"></i> ประเภทพนักงาน</label>
													        <div class="controls">
														            <asp:DropDownList ID="ddlPSNType"  OnSelectedIndexChanged="btnSearch_Click" runat="server" AutoPostBack="true" CssClass="medium m-wrap">
														             </asp:DropDownList>														       
													        </div>
												        </div>
											    <div class="control-group">
										            <label class="control-label"><i class="icon-user"></i> ระดับผู้ถูกประเมิน</label>
										            	    <div class="controls">
											                    <label class="checkbox">
											                        <div class="checker"><span>
											                        <asp:CheckBox ID="chkClass1" OnCheckedChanged="btnSearch_Click" runat="server" AutoPostBack="true" Text=""  />
											                        </span></div> 1
											                    </label>
											                    <label class="checkbox">
											                        <div class="checker"><span>
											                        <asp:CheckBox ID="chkClass2" OnCheckedChanged="btnSearch_Click" runat="server" AutoPostBack="true" Text="" />
											                        </span></div> 2
											                    </label>
											                    <label class="checkbox">
											                        <div class="checker"><span>
											                        <asp:CheckBox ID="chkClass3" OnCheckedChanged="btnSearch_Click" runat="server" AutoPostBack="true" Text="" />
											                        </span></div> 3
											                    </label>
											                    <label class="checkbox">
											                        <div class="checker"><span>
											                        <asp:CheckBox ID="chkClass4" OnCheckedChanged="btnSearch_Click" runat="server" AutoPostBack="true" Text="" />
											                        </span></div> 4
											                    </label>
											                    <label class="checkbox">
											                        <div class="checker"><span>
											                        <asp:CheckBox ID="chkClass5" OnCheckedChanged="btnSearch_Click" runat="server" AutoPostBack="true" Text=""  />
											                        </span></div> 5
											                    </label>
											                    <label class="checkbox">
											                        <div class="checker"><span>
											                        <asp:CheckBox ID="chkClass6" OnCheckedChanged="btnSearch_Click" runat="server" AutoPostBack="true" Text="" />
											                        </span></div> 6
											                    </label>
                                                                <label class="checkbox">
											                        <div class="checker"><span>
											                        <asp:CheckBox ID="chkClass7" OnCheckedChanged="btnSearch_Click" runat="server" AutoPostBack="true" Text="" />
											                        </span></div> 7
											                    </label>

										                   </div>
                                                           <div class="controls">
											                    
											                    
											                    <label class="checkbox">
											                        <div class="checker"><span>
											                        <asp:CheckBox ID="chkClass8" OnCheckedChanged="btnSearch_Click" runat="server" AutoPostBack="true" Text="" />
											                        </span></div> 8
											                    </label>
											                    <label class="checkbox">
											                        <div class="checker"><span>
											                        <asp:CheckBox ID="chkClass9" OnCheckedChanged="btnSearch_Click" runat="server" AutoPostBack="true" Text=""  />
											                        </span></div> 9
											                    </label>
											                    <label class="checkbox">
											                        <div class="checker"><span>
											                        <asp:CheckBox ID="chkClass10" OnCheckedChanged="btnSearch_Click" runat="server" AutoPostBack="true" Text="" />
											                        </span></div> 10
											                    </label>
											                    <label class="checkbox">
											                        <div class="checker"><span>
											                        <asp:CheckBox ID="chkClass11" OnCheckedChanged="btnSearch_Click" runat="server" AutoPostBack="true" Text="" />
											                        </span></div> 11
											                    </label>
											                    <label class="checkbox">
											                        <div class="checker"><span>
											                        <asp:CheckBox ID="chkClass12" OnCheckedChanged="btnSearch_Click" runat="server" AutoPostBack="true" Text="" />
											                        </span></div> 12
											                    </label>
											                    <label class="checkbox">
											                        <div class="checker"><span>
											                        <asp:CheckBox ID="chkClass13" OnCheckedChanged="btnSearch_Click" runat="server" AutoPostBack="true" Text="" />
											                        </span></div> 13
											                    </label>
										                   </div>
									            </div>
										    </div>	
									     </div>
									     <div class="row-fluid form-horizontal">
									        <div class="span6">
									        
									        </div>
									     
									        <div class="span6">
											        <div class="control-group">
											                <label class="control-label"></label>
											                <div class="controls">
											                    <label class="checkbox">
											                        <div class="checker"><span>
											                        <asp:CheckBox ID="chkBlank" OnCheckedChanged="btnSearch_Click" runat="server" AutoPostBack="true" Text=""  />
											                        </span></div> แสดงตำแหน่งที่ยังไม่กำหนดผู้ประเมินหลัก
											                    </label>											                    
										                   </div>
											        </div>
											 </div>
									     </div>
							              								        
						            <div class="portlet-body no-more-tables">
						            
						            <style type="text/css">
						                th{
						                    background-color:#eeeeee !important;
						                }
						            </style>
						            
						            <asp:Label ID="lblCountList" runat="server" Font-Size="14px" Font-Bold="true"></asp:Label>
								        <table class="table table-full-width table-advance dataTable no-more-tables table-hover">
									        <thead>
										        <tr>
											        <th colspan="4" style="text-align:center; border-bottom:none; border-right:none; "><i class="icon-user-md"></i> หัวหน้า/ผู้ประเมิน</th>
											        <th rowspan="2" style="text-align:center; border-bottom:none; border-right:1px solid #cccccc;"><i class="icon-bolt"></i> ดำเนินการ</th>											        

											        <th colspan="5" style="text-align:center; border-bottom:none; border-right:1px solid #cccccc;"><i class="icon-user"></i> พนักงาน/ผู้ถูกประเมิน</th>											        
										        </tr>
										        <tr>
											        <th style="border-top:none;"><i class="icon-briefcase"></i> ตำแหน่ง</th>
											        <th style="text-align:center; border-top:none;"><i class="icon-bookmark"></i> ระดับ</th>
											        <th style="border-top:none;"><i class="icon-sitemap"></i> หน่วยงาน</th>
											        <th style="border-top:none; border-right: none;"><i class="icon-male"></i> ผู้ครองตำแหน่ง</th>
											        
											        <th style="border-top:none;"><i class="icon-briefcase"></i> ตำแหน่ง</th>
											        <th style="text-align:center; border-top:none;"><i class="icon-bookmark"></i> ระดับ</th>
											        <th style="border-top:none;"><i class="icon-sitemap"></i> หน่วยงาน</th>	
                                                    <th style="text-align:center; border-top:none;"><i class="icon-male"></i> ประเภท</th>
											        <th style="border-top:none; border-right:1px solid #cccccc;"><i class="icon-male"></i> ผู้ครองตำแหน่ง</th>										       
										        </tr>
									        </thead>
									        <tbody>
										        <asp:Repeater ID="rptList" OnItemCommand="rptList_ItemCommand" OnItemDataBound="rptList_ItemDataBound" runat="server"> 
										            <ItemTemplate>
										                    <tr>
											                    <td data-title="ตำแหน่ง" id="td1" runat="server"><asp:Label ID="lbl_ASS_POS" runat="server"></asp:Label></td>
											                    <td data-title="ระดับ" id="td2" runat="server"><asp:Label ID="lbl_ASS_CLASS" runat="server"></asp:Label></td>
											                    <td data-title="หน่วยงาน" id="td3" runat="server"><asp:Label ID="lbl_ASS_DEPT" runat="server"></asp:Label></td>
											                    <td data-title="ผู้ครองตำแหน่ง" id="td4" runat="server" ><asp:Label ID="lbl_ASS_NAME" runat="server"></asp:Label></td>
											                    
											                    <td data-title="ดำเนินการ" id="tdEdit" runat ="server" style=" border-right:1px solid #cccccc;  border-left:none;">
											                        <asp:LinkButton ID="btnEdit" runat="server" CssClass="btn mini red" CommandName="EDIT"><i class="icon-bolt"></i> กำหนด</asp:LinkButton>											                        
											                        <asp:ConfirmButtonExtender TargetControlID="btnEdit" ID="cfm_btnEdit" runat="server"></asp:ConfirmButtonExtender>
											                    </td>

											                    <td data-title="ตำแหน่ง" id="td5" runat="server"><asp:Label ID="lbl_PSN_POS" runat="server"></asp:Label></td>
											                    <td data-title="ระดับ" id="td6" runat="server"><asp:Label ID="lbl_PSN_CLASS" runat="server"></asp:Label></td>
											                    <td data-title="หน่วยงาน" id="td7" runat="server"><asp:Label ID="lbl_PSN_DEPT" runat="server"></asp:Label></td>	
                                                                <td data-title="หน่วยงาน" id="td9" runat="server"><asp:Label ID="lbl_PSNType" runat="server"></asp:Label></td>
											                    <td data-title="ผู้ครองตำแหน่ง" id="td8" runat="server" style="border-right:1px solid #cccccc;"><asp:Label ID="lbl_PSN_NAME" runat="server"></asp:Label></td>	
											                    								                    
										                    </tr>	
										            </ItemTemplate>
										        </asp:Repeater>        															
									        </tbody>
								        </table>
								        
								        <asp:PageNavigation ID="Pager"  OnPageChanging="Pager_PageChanging" MaximunPageCount="20" PageSize="20" runat="server" />
							        </div>
							        
							        <div class="form-actions">
	                                    <asp:Button ID="btnAdd" OnClick="btnAdd_Click" runat="server" CssClass="btn blue" Text="เพิ่มผู้ประเมิน" />
	                                </div>
        						<!-- END SAMPLE TABLE PORTLET-->       						
					        </div>
					        
                        </div>
               </asp:Panel>
				
				
				<asp:Panel ID="pnlEdit" runat="server" >
				    <div class="portlet-body form">
							<h4 class="form-section"><i class="icon-user-md"></i> ตำแหน่งผู้ประเมินหลัก <asp:Button ID="btnChooseAss" OnClick="btnChooseAss_Click" CssClass="btn blue" runat="server" Text="เปลี่ยน" /></h4>
								<!-- BEGIN FORM-->
								
							    			    
								<div class="form-horizontal form-view" id="divView" runat="server">									
									
									<div class="row-fluid">													
										<div class="span6 ">
											<div class="control-group">
												<label class="control-label">ตำแหน่ง:</label>
												<div class="controls">
													<asp:Label ID="lblASSName" runat="server" CssClass="text bold"></asp:Label>
												</div>
											</div>
										</div>
										<!--/span-->
										<div class="span6 ">
											<div class="control-group">
												<label class="control-label">ระดับ:</label>
												<div class="controls">
													<asp:Label ID="lblASSClass" runat="server" CssClass="text bold"></asp:Label>
												</div>
											</div>
										</div>
										<!--/span-->
									</div>
									<!--/row-->
									<div class="row-fluid">
									    <div class="span6 ">
											<div class="control-group">
												<label class="control-label">หน่วยงาน:</label>
												<div class="controls">
													<asp:Label ID="lblASSDept" runat="server" CssClass="text bold"></asp:Label>
												</div>
											</div>
										</div>
										<!--/span-->
										<div class="span6 ">
										    <div class="control-group">
												<label class="control-label">ผู้ครองตำแหน่ง(ปัจจุบัน):</label>
												<div class="controls">
													<asp:Label ID="lblASSNow" runat="server" CssClass="text bold" ForeColor="#cccccc">123456</asp:Label>
												</div>
											</div>
										</div>										
									</div>
									
									<!--/row-->        
                                          
									<h4 class="form-section"><i class="icon-user"></i> ตำแหน่งพนักงาน (ทุกประเภท) ที่จะถูกประเมิน โดยผู้ประเมินตำแหน่งนี้</h4>
									   <div class="row-fluid " id="pnlAssignedList" runat="server">
										<div class="span12 ">
											<div class="control-group">
												<div class="controls">
												    <div class="span10">
													    <div class="portlet-body no-more-tables">
					                                        <table class="table  table-hover">
						                                        <thead>
							                                        <tr>
								                                        <th>#</th>
								                                        <th><i class="icon-briefcase"></i> ตำแหน่ง</th>
								                                        <th><i class="icon-bookmark"></i> ระดับ</th>
								                                        <th><i class="icon-sitemap"></i> หน่วยงาน</th>
								                                        <th><i class="icon-user"></i> ผู้ครองตำแหน่ง (ปัจจุบัน)</th>
								                                        <th><i class="icon-bolt"></i> ดำเนินการ</th>
							                                        </tr>
						                                        </thead>
						                                        <tbody>
							                                        <asp:Repeater ID="rptPSNPOS" OnItemCommand="rptPSNPOS_ItemCommand" OnItemDataBound="rptPSNPOS_ItemDataBound" runat="server">
							                                            <ItemTemplate>
							                                                    <tr>
								                                                    <td data-title="#"><asp:Label ID="lblNo" runat="server"></asp:Label></td>
								                                                    <td data-title="ตำแหน่ง"><asp:Label ID="lblPSNPos" runat="server"></asp:Label></td>
								                                                    <td data-title="ระดับ"><asp:Label ID="lblPSNClass" runat="server"></asp:Label></td>
								                                                    <td data-title="หน่วยงาน"><asp:Label ID="lblPSNDept" runat="server"></asp:Label></td>
								                                                    <td data-title="ผู้ครองตำแหน่ง(ปัจจุบัน)"><asp:Label ID="lblPSNNow" runat="server" ForeColor="#cccccc"></asp:Label></td>
								                                                    <td data-title="ดำเนินการ">
								                                                        <asp:Button CssClass="btn red mini" Text="ยกเลิก" ID="btnPSNDelete" runat="server" CommandName="Delete"/>
								                                                        <asp:ConfirmButtonExtender TargetControlID="btnPSNDelete" ID="cfm_PSN_Delete" runat="server"></asp:ConfirmButtonExtender>
								                                                    </td>
							                                                    </tr>
							                                            </ItemTemplate>
							                                        </asp:Repeater>							                                        									                    
						                                        </tbody>
					                                        </table>
				                                        </div>
												    </div>
												</div>
											</div>
										</div>
									</div>
									
									<div class="form-actions">
									    <asp:Button ID="btnBack" OnClick="btnBack_Click" CssClass="btn" runat="server" Text="ยกเลิก" />
										<asp:Button ID="btnSave" OnClick="btnSave_Click" runat="server" CssClass="btn green" Text="บันทึก" />
										<asp:ConfirmButtonExtender TargetControlID="btnSave" ID="cfm_btnSave" runat="server" ConfirmText="การบันทึกจะมีผลกับความสัมพันธ์ของผู้ประเมินและ
										ผู้ถูกประเมินตามหน้าจอที่แสดงอยู่ในขณะนี้ทันที 										
										ยืนยันดำเนินการต่อ?"></asp:ConfirmButtonExtender>
										<asp:Button ID="btnChoosePSN" OnClick="btnChoosePSN_Click" runat="server" CssClass="btn blue" Text="เพิ่มตำแหน่งพนักงาน/ผู้ถูกประเมิน" />
									</div>
								</div>
								
							
                
								<!-- END FORM-->  
				</div>
					    
		</asp:Panel>
		
		
		<asp:Panel ID="pnlPOSDialog" DefaultButton="btn_Search_POS" runat="server" Visible="false">
                    <div style="z-index: 10049;" class="modal-backdrop fade in"></div>
                    <div style="z-index: 10050; max-width:90%; min-width:70%; left:30%; max-height:95%; top:5%;" class="modal">
                        
				            <div class="modal-header">
	                            <h3><i class="icon-search"></i> <asp:Label ID="lblChooseMode" runat="server" Text="พนักงาน/ผู้ถูกประเมิน"></asp:Label></h3>
                            </div>
                            <div class="modal-body" style="max-height:85%;">
	                                  	<div >
											<div class="row-fluid">
																			
												    <div class="row-fluid">												            
													         <div class="span4 ">
														        <div class="control-group">
													                <label class="control-label"><i class="icon-sitemap"></i> หน่วยงาน</label>
													                <div class="controls">
														                <asp:TextBox ID="txt_Search_Organize" OnTextChanged="btn_Search_POS_Click" runat="server" AutoPostBack="true" CssClass="m-wrap medium" placeholder="ค้นหาจากฝ่าย/หน่วยงาน"></asp:TextBox>
														            </div>
												                </div>
													        </div>
													    
													       <div class="span4 ">
												                <div class="control-group">
													                <label class="control-label"><i class="icon-briefcase"></i> ตำแหน่ง</label>
													                <div class="controls">
														                <asp:TextBox ID="txt_Search_Name" OnTextChanged="btn_Search_POS_Click" runat="server" AutoPostBack="true" CssClass="m-wrap medium" placeholder="ค้นหาจากเลขตำแหน่ง/ชื่อตำแหน่ง"></asp:TextBox>
														                <asp:Button ID="btn_Search_POS" OnClick="btn_Search_POS_Click"  runat="server" Text="" style="display:none;" />
													                </div>
												                </div>														        
													        </div>	
													         <div class="span4 ">
												                <div class="control-group">
													                <label class="control-label"><i class="icon-bookmark"></i> ระดับ</label>
													                <div class="controls">
														                <asp:TextBox ID="txt_Search_Class" OnTextChanged="btn_Search_POS_Click" runat="server" AutoPostBack="true" CssClass="m-wrap medium" placeholder="เลขระดับ"></asp:TextBox>
														            </div>
												                </div>														        
													        </div>												        
												    </div>
												    	
												     <asp:Label ID="lblCountPSN" runat="server" Font-Size="14px" Font-Bold="true"></asp:Label>
								                    <table class="table table-full-width table-advance dataTable no-more-tables table-hover">
									                    <thead>
										                    <tr>
											                    <th ><i class="icon-briefcase"></i> ตำแหน่ง</th>
											                    <th style="text-align:center;"><i class="icon-list"></i> ระดับ</th>
											                    <th ><i class="icon-sitemap"></i> หน่วยงาน</th>
											                    <th ><i class="icon-user"></i>ผู้ครองตำแหน่ง(ปัจจุบัน)</td>
											                    <th style="text-align:center;"><i class="icon-bolt"></i> เลือก</th>
										                    </tr>
									                    </thead>
									                    <tbody>
									                        <asp:Repeater ID="rptPOSDialog" OnItemCommand="rptPOSDialog_ItemCommand" OnItemDataBound="rptPOSDialog_ItemDataBound" runat="server">
									                            <ItemTemplate>
    									                            <tr>                                        
											                            <td data-title="ตำแหน่ง" ><asp:Label ID="lblListPOS" runat="server"></asp:Label></td>
											                            <td data-title="ระดับ" style="text-align:center;"><asp:Label ID="lblListClass" runat="server"></asp:Label></td>
											                            <td data-title="หน่วยงาน" ><asp:Label ID="lblListDEPT" runat="server"></asp:Label></td>
											                            <td data-title="ผู้ครองตำแหน่ง(ปัจจุบัน)"><asp:Label ID="lblListNow" runat="server"></asp:Label></td>
											                            <td data-title="เลือก" style="text-align:center;">
											                                <input type="button" ID="btnPreSelect" runat="server" class="btn mini blue" value="เลือก" />
											                                <asp:Button ID="btnSelect" runat="server" CssClass="btn mini blue" Text="เลือก" CommandName="Select" Style="display:none;" />
										                                </td>
										                            </tr>	
									                            </ItemTemplate>
									                        </asp:Repeater>
									                    </tbody>
								                    </table>
                    								
								                    <asp:PageNavigation ID="PagerPOS" OnPageChanging="PagerPOS_PageChanging" MaximunPageCount="10" PageSize="7" runat="server" />
											  
											</div>
										</div>
                            </div>                            
                            <asp:LinkButton CssClass="fancybox-item fancybox-close" ToolTip="Close" ID="btnCloseDialog" OnClick="btnCloseDialog_Click" runat="server"></asp:LinkButton>
                                                  
            </div>
       </asp:Panel>
			    </ContentTemplate>
                </asp:UpdatePanel>
		        
    </div>
    
    
    						
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ScriptPlaceHolder" Runat="Server">
</asp:Content>

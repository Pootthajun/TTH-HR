﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="VB.Login" %>

<%@ Register src="UpdateProgress.ascx" tagname="UpdateProgress" tagprefix="uc1" %>

<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
	<meta charset="utf-8" />
	<title>ระบบประเมินผลการปฏิบัติงาน</title>
	<meta content="width=device-width, initial-scale=1.0" name="viewport" />
	<meta content="" name="description" />
	<meta content="" name="author" />
	<!-- BEGIN GLOBAL MANDATORY STYLES -->
	<link href="assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
	<link href="assets/plugins/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" type="text/css"/>
	<link href="assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
	<link href="assets/css/style-metro.css" rel="stylesheet" type="text/css"/>
	<link href="assets/css/style.css" rel="stylesheet" type="text/css"/>
	<link href="assets/css/style-responsive.css" rel="stylesheet" type="text/css"/>
	<link href="assets/css/themes/default.css" rel="stylesheet" type="text/css" id="style_color"/>
	<link href="assets/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css"/>
	<link rel="stylesheet" type="text/css" href="assets/plugins/select2/select2_metro.css" />
	<link rel="stylesheet" href="assets/plugins/fancybox/source/jquery.fancybox.css" />
	<!-- END GLOBAL MANDATORY STYLES -->
	<!-- BEGIN PAGE LEVEL STYLES -->
	<link href="assets/css/pages/login-soft.css" rel="stylesheet" type="text/css"/>
	<!-- END PAGE LEVEL STYLES -->
	<link rel="icon" href="images/Logo.ico" type="image/ico"/>  
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="login">
	<!-- BEGIN LOGO -->
	<div class="logo">	
	    <img src="images/Logo.png" style="height:30px;"/> &nbsp;
		<font style="color:#fff; font-size:28px;">ระบบประเมินผลการปฏิบัติงาน</font>		
	</div>
	<!-- END LOGO -->
	<!-- BEGIN LOGIN -->
	<div class="content">
		<!-- BEGIN LOGIN FORM -->
		<form CssClass="form-vertical login-form" ID="form1" runat="server" defaultbutton="btnLogin">
		<asp:ScriptManager ID="scm" runat="server"></asp:ScriptManager>
		<asp:UpdatePanel ID="udp" runat="server">
		<ContentTemplate>
			<h3 class="form-title">ลงชื่อเข้าสู่ระบบ</h3>
			
			
			<div class="control-group">
				<!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
				<label class="control-label visible-ie8 visible-ie9">ชื่อผู้ใช้ระบบ</label>
				<div class="controls">
					<div class="input-icon left">
						<i class="icon-user"></i>
						<asp:TextBox ID="txtUserName" runat="server" CssClass="m-wrap placeholder-no-fix" autocomplete="off" placeholder="ชื่อผู้ใช้ระบบ" ></asp:TextBox>
					</div>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label visible-ie8 visible-ie9">รหัสผ่าน</label>
				<div class="controls">
					<div class="input-icon left">
						<i class="icon-lock"></i>
						<asp:TextBox ID="txtPassword" TextMode="Password" runat="server" CssClass="m-wrap placeholder-no-fix" autocomplete="off" placeholder="รหัสผ่าน" ></asp:TextBox>
						<asp:TextBox ID="txtStorePassword" runat="server" style="display:none;"></asp:TextBox>
					</div>
				</div>
			</div>
			<div class="form-actions" style="margin-bottom:-50px;">
				<label class="checkbox" style="margin-left:20px;">
				<asp:CheckBox ID="chkRemember" runat="server" /> จำฉันไว้ในระบบ
				</label>
				<asp:Button id="btnLogin" OnClick="btnLogin_Click" runat="server" CssClass="btn blue pull-right " 
                    Text="เข้าสู่ระบบ" />
				
				<h5 style="text-align:center; color:Teal; background-color:#EEEEEE; padding: 10px 10px 10px 10px;" id="pnlChrome" runat="server">
				   <table width="100%">
				        <tr>
				            <td><a href="http://www.google.co.th/intl/th/chrome/" target="_blank" title="Click เพื่อ Download Google Chrome"><img src="images/chrome.png" border="0" alt="Google Chrome" /></a></td>
				            <td align="left" style="padding-left:10px;">เพื่อรองรับการแสดงผลอย่างถูกต้อง กรุณาใช้งานผ่าน Google Chrome 
				            <br>โดยท่านสามารถ Download Google Chrome โดย <a href="http://www.google.co.th/intl/th/chrome/" target="_blank" style="text-decoration:underline;">คลิกที่นี</a></td>
				        </tr>
				   </table>
				</h5>
				
				
				
				<h5 style="text-align:center; color:Teal; background-color:#EEEEEE; padding: 10px 10px 10px 10px;" id="pnlManual" runat="server">
				    
				    <table width="100%">
				        <tr>
				            <td><a href="Doc/Manual/Login_Manual.pdf" target="_blank" title="Click เพื่อ Download คู่มือสำหรับการเข้าใช้งานระบบ"><img src="images/PDF.png" width="30" border="0" alt="Login Manual" /></a></td>
				            <td align="left" style="padding-left:10px;">คู่มือสำหรับการเข้าใช้งานระบบ <a href="Doc/Manual/Login_Manual.pdf" target="_blank" style="text-decoration:underline;">คลิกที่นี</a></td>
				        </tr>
				   </table>
				</h5>
				
				
			</div>
			
			<div class="forget-password">
			    				
			</div>
			
				<div ID="alertModal" style="visibility:hidden;">
				<center>
                    <div style="z-index: 10059;" class="modal-backdrop fade in"></div>
                    <div style="z-index: 10060; max-width:80%; top:25%; width:40%; left:25%; margin:10px 10px 10px 10px;" class="modal">
                            <div class="modal-body" style="max-height:90%; text-align:center; font-size:16px; cursor:pointer;" id="alertMsg" onclick="document.getElementById('alertModal').style.visibility='hidden';">
	                                  	
                            </div>                            
                            <a href="javascript:;" onclick="document.getElementById('alertModal').style.visibility='hidden';" class="fancybox-item fancybox-close" title="Close"></a>
                    </div>
				</center>
               </div>
               
	    </ContentTemplate>
	    </asp:UpdatePanel>
		</form>
		<!-- END LOGIN FORM -->   
	</div>
	<!-- END LOGIN -->
	<!-- BEGIN COPYRIGHT -->
	<div class="copyright" style="width:300px;">
		
		<%--2013 &copy; tapioca.dft.go.th. All Right Reserved.--%>
	</div>
	<!-- END COPYRIGHT -->
	<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
	<!-- BEGIN CORE PLUGINS -->   
	<script src="assets/plugins/jquery-1.10.1.min.js" type="text/javascript"></script>
	<script src="assets/plugins/jquery-migrate-1.2.1.min.js" type="text/javascript"></script>
	<!-- IMPORTANT! Load jquery-ui-1.10.1.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
	<script src="assets/plugins/jquery-ui/jquery-ui-1.10.1.custom.min.js" type="text/javascript"></script>      
	<script src="assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
	<script src="assets/plugins/bootstrap-hover-dropdown/twitter-bootstrap-hover-dropdown.min.js" type="text/javascript" ></script>
	<!--[if lt IE 9]>
	<script src="assets/plugins/excanvas.min.js"></script>
	<script src="assets/plugins/respond.min.js"></script>  
	<![endif]-->   
	<script src="assets/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
	<script src="assets/plugins/jquery.blockui.min.js" type="text/javascript"></script>  
	<script src="assets/plugins/jquery.cookie.min.js" type="text/javascript"></script>
	<script src="assets/plugins/uniform/jquery.uniform.min.js" type="text/javascript" ></script>
	<!-- END CORE PLUGINS -->
	<!-- BEGIN PAGE LEVEL PLUGINS -->
	<script src="assets/plugins/jquery-validation/dist/jquery.validate.min.js" type="text/javascript"></script>
	<script src="assets/plugins/backstretch/jquery.backstretch.min.js" type="text/javascript"></script>
	<script type="text/javascript" src="assets/plugins/select2/select2.min.js"></script>
	<!-- END PAGE LEVEL PLUGINS -->
	<!-- BEGIN PAGE LEVEL SCRIPTS -->
	<script src="assets/scripts/app.js" type="text/javascript"></script>
	<script src="assets/scripts/login-soft.js" type="text/javascript"></script>      
	<!-- END PAGE LEVEL SCRIPTS --> 
	<script>
		jQuery(document).ready(function() {     
		  App.init();
		  Login.init();
		});
		
		function showAlert(msg)
		{
			document.getElementById("alertModal").style.visibility="visible";
			document.getElementById("alertMsg").innerHTML=msg;
		} 
				
	</script>
	<!-- END JAVASCRIPTS -->
	
    <uc1:UpdateProgress ID="UpdateProgress1" runat="server" />
</body>
<!-- END BODY -->
</html>
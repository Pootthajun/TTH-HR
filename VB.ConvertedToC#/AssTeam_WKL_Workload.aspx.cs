using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Xml.Linq;
using System.Diagnostics;
using System.Collections.Specialized;
using System.Configuration;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Caching;
using System.Web.SessionState;
using System.Web.Security;
using System.Web.Profile;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
namespace VB
{

    public partial class AssTeam_WKL_Workload : System.Web.UI.Page
    {


        HRBL BL = new HRBL();
        GenericLib GL = new GenericLib();

        textControlLib TC = new textControlLib();
        public string PSNL_NO
        {
            get
            {
                try
                {
                    return (string)Session["USER_PSNL_NO"];
                }
                catch (Exception ex)
                {
                    return "";
                }
            }
        }

        public string DEPT_CODE
        {
            get
            {
                try
                {
                    return GL.SplitString(ddlOrganize.Items[ddlOrganize.SelectedIndex].Value, "-")[0];
                }
                catch (Exception ex)
                {
                    return "";
                }
            }
        }

        public string MINOR_CODE
        {
            get
            {
                try
                {
                    return GL.SplitString(ddlOrganize.Items[ddlOrganize.SelectedIndex].Value, "-")[1];
                }
                catch (Exception ex)
                {
                    return "00";
                }
            }
        }

        protected void Page_Load(object sender, System.EventArgs e)
        {
            if ((Session["USER_PSNL_NO"] == null))
            {
                ScriptManager.RegisterStartupScript(this.Page, typeof(string), "Alert", "showAlert('กรุณาลงชื่อเข้าใช้ระบบ');", true);
                Response.Redirect("Login.aspx");
                return;
            }

            if (!Convert.ToBoolean(Session["USER_Is_Workload_Admin"]) & !Convert.ToBoolean(Session["USER_Is_Workload_User"]) & !Convert.ToBoolean(Session["USER_Is_Manager"]))
            {
                ScriptManager.RegisterStartupScript(this.Page, typeof(string), "Alert", "showAlert('คุณไม่มีสิทธิ์เข้าถึงข้อมูลส่วนนี้');", true);
                Response.Redirect("Overview.aspx");
                return;
            }

            if (!IsPostBack)
            {
                if (Convert.ToBoolean(Session["USER_Is_Workload_Admin"]))
                {
                    BL.BindDDlWorkloadDepartment(ddlOrganize, "", "");
                }
                else
                {
                    BL.BindDDlWorkloadDepartment(ddlOrganize, "", Session["USER_DEPT_CODE"].ToString());
                    ddlOrganize.Enabled = false;
                }

                if (ddlOrganize.Items.Count == 1)
                {
                    ddlOrganize.Items[0].Text = "คุณไม่มีสิทธิ์เข้าถึงข้อมูลส่วนนี้";
                    return;
                }
                else
                {
                    ddlOrganize.Items.RemoveAt(0);
                    ddlOrganize.SelectedIndex = 0;
                }

                //-----------Bind Data----------
                BindWholeTable();
            }

            if (IsPostBack)
            {
                ScriptManager.RegisterStartupScript(this.Page, typeof(string), "Alert", "activeLastFocusControl();", true);
            }

        }

        protected void btnExcel_click(object sender, System.EventArgs e)
        {
            //
            ScriptManager.RegisterStartupScript(this.Page, typeof(string), "Excel", "window.location.href='print/RPT_W_Excel.aspx?D=" + DEPT_CODE + "&M=" + MINOR_CODE + "';", true);
        }

        protected void ddlOrganize_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            BindWholeTable();
        }

        private void BindWholeTable()
        {
            BindClass();
            BindPSN();
            BindHeader();
            //----------- Add Class Header + Personal Detail--------
            BindJob();
        }

        //--------- ข้อมูล แผนก -------------
        DataTable DeptClass;
        DataTable PSNData;
        private void BindClass()
        {
            string SQL = "SELECT Class_ID,PNPO_CLASS_Start,PNPO_CLASS_End\n";
            SQL += " FROM tb_HR_Job_Mapping_Class\n";
            SQL += " WHERE DEPT_CODE='" + DEPT_CODE.Replace("'", "''") + "' AND MINOR_CODE='" + MINOR_CODE.Replace("'", "''") + "' \n";
            SQL += " ORDER BY Class_ID DESC\n";
            SqlDataAdapter DA = new SqlDataAdapter(SQL, BL.ConnectionString());
            DeptClass = new DataTable();
            DA.Fill(DeptClass);

            //----------- ใส่ Default ให้ กรณีที่ ยังไม่ได้ตั้งค่าระดับ --------------
            if (DeptClass.Rows.Count == 0)
            {
                BL.SetJobMappingAutoClass(DEPT_CODE, MINOR_CODE);
                DA = new SqlDataAdapter(SQL, BL.ConnectionString());
                DeptClass = new DataTable();
                DA.Fill(DeptClass);
            }
        }

        private void BindPSN()
        {
            string SQL = " SELECT \n";
            SQL += " CLS.Class_ID,PSN_ID,REF_PSNL_NO PSNL_NO,Slot_No,Slot_Name\n";
            SQL += " FROM tb_HR_Job_Mapping_Class CLS\n";
            SQL += " LEFT JOIN tb_HR_Workload_PSN PSN ON PSN.Class_ID=CLS.Class_ID\n";
            SQL += " \t\t\t\t\t\t\tAND PSN.DEPT_CODE=CLS.DEPT_CODE \n";
            SQL += " \t\t\t\t\t\t\tAND PSN.MINOR_CODE=CLS.MINOR_CODE\n";
            SQL += " WHERE CLS.DEPT_CODE='" + DEPT_CODE.Replace("'", "''") + "' AND CLS.MINOR_CODE='" + MINOR_CODE.Replace("'", "''") + "'\n";
            SQL += " ORDER BY CLS.Class_ID DESC,Slot_No ASC\n";

            SqlDataAdapter DA = new SqlDataAdapter(SQL, BL.ConnectionString());
            PSNData = new DataTable();
            DA.Fill(PSNData);

            //----------- ใส่ Default ให้ กรณีที่ ยังไม่ได้ตั้งค่าบุคคล --------------
            PSNData.DefaultView.RowFilter = "PSN_ID IS NOT NULL";
            if (PSNData.DefaultView.Count == 0)
            {
                BL.SetWorkloadDefaultPSN(DEPT_CODE, MINOR_CODE);
                DA = new SqlDataAdapter(SQL, BL.ConnectionString());
                PSNData = new DataTable();
                DA.Fill(PSNData);
            }

        }

        private void BindHeader()
        {
            //----------- Add Class Header--------
            rptClass.DataSource = DeptClass;
            rptClass.DataBind();
            //----------- Add Personal Detail--------
            rptPSN.DataSource = PSNData;
            rptPSN.DataBind();
        }

        protected void rptClass_ItemCommand(object source, System.Web.UI.WebControls.RepeaterCommandEventArgs e)
        {
            switch (e.CommandName)
            {
                case "Default":
                    HtmlAnchor lnkClassName = (HtmlAnchor)e.Item.FindControl("lnkClassName");
                    BL.SetWorkloadDefaultPSN(DEPT_CODE.ToString(), MINOR_CODE.ToString(), GL.CINT(lnkClassName.Attributes["Class_ID"]));
                    BindWholeTable();
                    break;
            }
        }

        protected void rptClass_ItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType != ListItemType.Item & e.Item.ItemType != ListItemType.AlternatingItem)
                return;

            HtmlTableCell thClass = (HtmlTableCell)e.Item.FindControl("thClass");
            HtmlAnchor lnkClassName = (HtmlAnchor)e.Item.FindControl("lnkClassName");
            HtmlAnchor btnAddPSN = (HtmlAnchor)e.Item.FindControl("btnAddPSN");
            LinkButton btnDefault = (LinkButton)e.Item.FindControl("btnDefault");
            DataRowView drv = (DataRowView)e.Item.DataItem;
            int _start = GL.CINT(drv["PNPO_CLASS_Start"]);
            int _end = GL.CINT(drv["PNPO_CLASS_End"]);
            if (_start == _end)
            {
                lnkClassName.InnerHtml = "พนักงานระดับ " + _start + " <i class='icon-angle-down'></i>";
            }
            else
            {
                lnkClassName.InnerHtml = "พนักงานระดับ " + _start + "-" + _end + " <i class='icon-angle-down'></i>";
            }

            //---------------- Set Binding Data ----------
            lnkClassName.Attributes["Class_ID"] = drv["Class_ID"].ToString();
            lnkClassName.Attributes["PNPO_CLASS_Start"] = drv["PNPO_CLASS_Start"].ToString();
            lnkClassName.Attributes["PNPO_CLASS_End"] = drv["PNPO_CLASS_End"].ToString();

            //---------------- Set ColSpan ---------------
            PSNData.DefaultView.RowFilter = "Class_ID=" + drv["Class_ID"];
            if (PSNData.DefaultView.Count > 1)
            {
                thClass.ColSpan = PSNData.DefaultView.Count;
            }
            PSNData.DefaultView.RowFilter = "";
            //---------------- Set Button ----------------
            btnAddPSN.Attributes["onClick"] = "showDialog(" + drv["Class_ID"] + ",'พนักงานใหม่','เพิ่มผู้ปฏิบัติ','Add')";
        }

        protected void rptPSN_ItemCommand(object source, System.Web.UI.WebControls.RepeaterCommandEventArgs e)
        {
            switch (e.CommandName)
            {
                case "Delete":
                    string SQL = "DELETE FROM tb_HR_Workload_Slot WHERE PSN_ID=" + e.CommandArgument + "\n";
                    SQL += "DELETE FROM tb_HR_Workload_PSN WHERE PSN_ID=" + e.CommandArgument + "\n";

                    SqlConnection Conn = new SqlConnection(BL.ConnectionString());
                    Conn.Open();
                    SqlCommand COMM = new SqlCommand();
                    var _with1 = COMM;
                    _with1.CommandType = CommandType.Text;
                    _with1.Connection = Conn;
                    _with1.CommandText = SQL;
                    _with1.ExecuteNonQuery();
                    _with1.Dispose();

                    //-------- ReOrder Slot No---------
                    LinkButton btnDelete = (LinkButton)e.CommandSource;
                    SQL = " SELECT * FROM tb_HR_Workload_PSN \n";
                    SQL += " WHERE DEPT_CODE='" + DEPT_CODE.Replace("'", "''") + "' AND MINOR_CODE='" + MINOR_CODE.Replace("'", "''") + "'\n";
                    SQL += " AND Class_ID=" + btnDelete.Attributes["Class_ID"] + "\n";
                    SQL += " ORDER BY Slot_No DESC  ";
                    DataTable DT = new DataTable();
                    SqlDataAdapter DA = new SqlDataAdapter(SQL, BL.ConnectionString());
                    DA.Fill(DT);
                    for (int i = 0; i <= DT.Rows.Count - 1; i++)
                    {
                        DT.Rows[i]["Slot_No"] = i + 1;
                    }

                    try
                    {
                        SqlCommandBuilder cmd = new SqlCommandBuilder(DA);
                        DA.Update(DT);
                    }
                    catch
                    {
                    }

                    Conn.Close();
                    Conn.Dispose();

                    BindWholeTable();

                    break;
            }
        }

        protected void rptPSN_ItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType != ListItemType.Item & e.Item.ItemType != ListItemType.AlternatingItem)
                return;

            HtmlAnchor lnkPSNName = (HtmlAnchor)e.Item.FindControl("lnkPSNName");
            HtmlAnchor btnRename = (HtmlAnchor)e.Item.FindControl("btnRename");
            LinkButton btnDelete = (LinkButton)e.Item.FindControl("btnDelete");

            HtmlGenericControl btnGroup = (HtmlGenericControl)e.Item.FindControl("btnGroup");

            DataRowView drv = (DataRowView)e.Item.DataItem;

            AjaxControlToolkit.ConfirmButtonExtender cfmBtnDelete = (AjaxControlToolkit.ConfirmButtonExtender)e.Item.FindControl("cfmBtnDelete");
            if (!GL.IsEqualNull(drv["Slot_Name"]) || !GL.IsEqualNull(drv["PSN_ID"]))
            {
                lnkPSNName.InnerHtml = drv["Slot_Name"].ToString() + " <i class='icon-angle-down'></i>";
                lnkPSNName.Attributes["Slot_Name"] = drv["Slot_Name"].ToString();

                //---------------- Set Button ----------------
                btnRename.Attributes["onClick"] = "showDialog(" + drv["PSN_ID"].ToString() + ",'" + drv["Slot_Name"].ToString().Replace("'", "\\'").Replace("\n", "\\n") + "','เปลี่ยนชื่อ','Rename');";
                cfmBtnDelete.ConfirmText = "ยีนยันลบ '" + drv["Slot_Name"] + "' ?";
                btnDelete.CommandArgument = drv["PSN_ID"].ToString();
                btnDelete.Attributes["Class_ID"] = drv["Class_ID"].ToString();
            }
            else
            {
                lnkPSNName.InnerHtml = "";
                btnGroup.Visible = false;
            }
            //---------------- Set Binding Data ----------
            lnkPSNName.Attributes["Class_ID"] = drv["Class_ID"].ToString();

        }

        //---------- ข้อมูลแผนที่งาน -----------
        DataTable JobData;
        //----------- ข้อมูลเวลา -------------
        DataTable SlotData;

        DataTable MapClass;
        private void BindJob()
        {
            string SQL = null;
            SQL = "  SELECT Job_LEVEL,Job_No,\n";
            SQL += "   Header.Job_ID,Job_Parent,Header.Job_Name,NumPerson,MinPerTime,TimePerYear,\n";
            SQL += "   CAST(COUNT(Detail.Class_ID) AS BIT) Editable\n";
            SQL += "   FROM tb_HR_Job_Mapping Header  \n";
            SQL += "   LEFT JOIN tb_HR_Job_Mapping_Detail Detail ON Header.Job_ID=Detail.Job_ID\n";
            SQL += "   WHERE PNDP_DEPT_CODE='" + DEPT_CODE.Replace("'", "''") + "' AND PNDP_MINOR_CODE='" + MINOR_CODE.Replace("'", "''") + "' \n";
            SQL += "   GROUP BY PNDP_DEPT_CODE,PNDP_MINOR_CODE,Job_LEVEL,Job_No,Header.Job_ID,Job_Parent,Header.Job_Name,\n";
            SQL += "   NumPerson,MinPerTime,TimePerYear\n";
            SQL += "   ORDER BY  Header.Job_LEVEL,Header.Job_No\n";

            SqlDataAdapter DA = new SqlDataAdapter(SQL, BL.ConnectionString());
            JobData = new DataTable();
            DA.Fill(JobData);

            //----------- Sort For Binding Structure----------
            DataTable DT = JobData.Copy();
            DT.Rows.Clear();
            DT.AcceptChanges();
            JobData.DefaultView.RowFilter = "Job_LEVEL=1";
            DataTable L1 = JobData.DefaultView.ToTable().Copy();
            JobData.DefaultView.RowFilter = "Job_LEVEL=2";
            DataTable L2 = JobData.DefaultView.ToTable().Copy();
            JobData.DefaultView.RowFilter = "Job_LEVEL=3";
            DataTable L3 = JobData.DefaultView.ToTable().Copy();
            for (int i = 0; i <= L1.Rows.Count - 1; i++)
            {
                DataRow R1 = DT.NewRow();
                R1.ItemArray = L1.Rows[i].ItemArray;
                DT.Rows.Add(R1);
                L2.DefaultView.RowFilter = "Job_Parent=" + R1["Job_ID"];
                for (int j = 0; j <= L2.DefaultView.Count - 1; j++)
                {
                    DataRow R2 = DT.NewRow();
                    R2.ItemArray = L2.DefaultView[j].Row.ItemArray;
                    DT.Rows.Add(R2);
                    L3.DefaultView.RowFilter = "Job_Parent=" + R2["Job_ID"];
                    for (int k = 0; k <= L3.DefaultView.Count - 1; k++)
                    {
                        DataRow R3 = DT.NewRow();
                        R3.ItemArray = L3.DefaultView[k].Row.ItemArray;
                        DT.Rows.Add(R3);
                    }
                }
            }
            // ERROR: Not supported in C#: OnErrorStatement

            JobData = DT.Copy();
            JobData.Columns.Add("PersonMin", typeof(long), "NumPerson*MinPerTime");
            JobData.Columns.Add("TotalMin", typeof(long), "PersonMin*TimePerYear");
            //----------- Job Data -------------
            string[] Col = {
				"Job_LEVEL",
				"Job_No",
				"Job_ID",
				"Job_Parent",
				"Job_Name",
				"Editable",
				"NumPerson",
				"MinPerTime",
				"TimePerYear"
			};
            DT.DefaultView.RowFilter = "";
            DT = DT.DefaultView.ToTable(true, Col);
            DataTable _job = DT.Copy();

            //----------- Get Slot Data ---------
            DT.DefaultView.RowFilter = "Editable=1";
            string _jid = "0,";
            for (int i = 0; i <= DT.DefaultView.Count - 1; i++)
            {
                _jid += DT.DefaultView[i]["Job_ID"] + ",";
            }
            if (!string.IsNullOrEmpty(_jid))
                _jid = _jid.Substring(0, _jid.Length - 1);
            SQL = " SELECT Header.Job_ID,Slot_ID,Slot.PSN_ID,MinPerYear\n";
            SQL += "   FROM tb_HR_Job_Mapping Header  \n";
            SQL += "   INNER JOIN tb_HR_Workload_Slot Slot ON Header.Job_ID=Slot.Job_ID\n";
            SQL += "   INNER JOIN tb_HR_Workload_PSN PSN ON Slot.PSN_ID=PSN.PSN_ID\n";
            SQL += "   INNER JOIN tb_HR_Job_Mapping_Class CLS ON PSN.Class_ID=CLS.Class_ID\n";
            SQL += "                            AND PSN.DEPT_CODE=CLS.DEPT_CODE\n";
            SQL += "                            AND PSN.MINOR_CODE=CLS.MINOR_CODE\n";
            SQL += "    WHERE Header.Job_ID IN (" + _jid + ")\n";
            SlotData = new DataTable();
            DA = new SqlDataAdapter(SQL, BL.ConnectionString());
            DA.Fill(SlotData);
            //----------- SlotData -------------


            //----------- Get Mapping Class------
            SQL = " SELECT Header.Job_ID,Detail.Class_ID\n";
            SQL += " FROM tb_HR_Job_Mapping Header  \n";
            SQL += " INNER JOIN tb_HR_Job_Mapping_Detail Detail ON Header.Job_ID=Detail.Job_ID\n";
            MapClass = new DataTable();
            DA = new SqlDataAdapter(SQL, BL.ConnectionString());
            DA.Fill(MapClass);
            //----------- for check requirement---

            rptJob.DataSource = JobData;
            rptJob.DataBind();

        }

        protected void rptJob_ItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
        {
            object _sumMin;
            DataTable DT;
            //SqlCommandBuilder cmd;
            //SqlDataAdapter DA;
            //string SQL = "";

            switch (e.Item.ItemType)
            {
                case ListItemType.Item:
                case ListItemType.AlternatingItem:

                    Label lblNo = (Label)e.Item.FindControl("lblNo");
                    Label lblTask = (Label)e.Item.FindControl("lblTask");
                    HtmlTableCell tdTask = (HtmlTableCell)e.Item.FindControl("tdTask");
                    TextBox txtNumPerson = (TextBox)e.Item.FindControl("txtNumPerson");
                    TextBox txtMinPerTime = (TextBox)e.Item.FindControl("txtMinPerTime");
                    Label lblPersonMin = (Label)e.Item.FindControl("lblPersonMin");
                    TextBox txtTimePerYear = (TextBox)e.Item.FindControl("txtTimePerYear");
                    Label lblTotalMin = (Label)e.Item.FindControl("lblTotalMin");

                    HtmlTableCell cellNumPerson = (HtmlTableCell)e.Item.FindControl("cellNumPerson");
                    HtmlTableCell cellMinPerTime = (HtmlTableCell)e.Item.FindControl("cellMinPerTime");
                    HtmlTableCell cellTimePerYear = (HtmlTableCell)e.Item.FindControl("cellTimePerYear");
                    HtmlTableCell cellTotalMin = (HtmlTableCell)e.Item.FindControl("cellTotalMin");
                    HtmlTableCell cellSumMin = (HtmlTableCell)e.Item.FindControl("cellSumMin");
                    Label lblSumMin = (Label)e.Item.FindControl("lblSumMin");
                    //---------- Job Header -----------

                    DataRowView drv = (DataRowView)e.Item.DataItem;

                    lblNo.Attributes["Job_ID"] = drv["Job_ID"].ToString();
                    if (drv["Job_LEVEL"].ToString() == "1")
                        lblNo.Text = drv["Job_No"].ToString();
                    if (!GL.IsEqualNull(drv["PersonMin"]))
                        lblPersonMin.Text = GL.StringFormatNumber(drv["PersonMin"].ToString(), 0);

                    //----------- Calculate Sum Min -------------
                    _sumMin = SlotData.Compute("SUM(MinPerYear)", "Job_ID=" + drv["Job_ID"]);
                    if (!GL.IsEqualNull(_sumMin))
                    {
                        lblSumMin.Text = GL.StringFormatNumber(_sumMin.ToString(), 0);
                    }

                    if (!GL.IsEqualNull(drv["TotalMin"]))
                    {
                        lblTotalMin.Text = GL.StringFormatNumber(drv["TotalMin"], 0);
                        //-------------------- Calculate Matching ------------------
                        if (GL.IsEqualNull(_sumMin) || _sumMin.ToString() != drv["TotalMin"].ToString())
                        {
                            cellTotalMin.Attributes["class"] += " WorkLoadBGNotMatch";
                            cellTotalMin.Style.Remove("background-color");
                            cellTotalMin.Style.Remove("color");
                            cellSumMin.Attributes["class"] += " WorkLoadBGNotMatch";
                            cellSumMin.Style.Remove("background-color");
                            cellSumMin.Style.Remove("color");

                            if (GL.IsEqualNull(_sumMin))
                            {
                                cellTotalMin.Attributes["title"] = "เวลาประเมินและผลรวมเวลาที่ปฏิบัติไม่เท่ากัน";
                            }
                            else if (GL.CDBL(drv["TotalMin"]) > GL.CDBL(_sumMin))
                            {
                                cellTotalMin.Attributes["title"] = "ผลรวมเวลาที่ปฏิบัติน้อยกว่าอยู่ " + GL.StringFormatNumber(GL.CDBL(drv["TotalMin"]) - GL.CDBL(_sumMin), 0) + " นาที";
                            }
                            else if (GL.CDBL(drv["TotalMin"]) < GL.CDBL(_sumMin))
                            {
                                cellTotalMin.Attributes["title"] = "เวลาที่ปฏิบัติมากกว่ากว่าอยู่ " + GL.StringFormatNumber(GL.CDBL(_sumMin) - GL.CDBL(drv["TotalMin"]), 0) + " นาที";
                            }

                        }
                        else
                        {
                            cellTotalMin.Attributes["class"] += " WorkLoadBGMatch";
                            cellTotalMin.Style.Remove("background-color");
                            cellTotalMin.Style.Remove("color");
                            cellSumMin.Attributes["class"] += " WorkLoadBGMatch";
                            cellSumMin.Style.Remove("background-color");
                            cellSumMin.Style.Remove("color");
                        }
                    }



                    switch (GL.CINT(drv["Job_LEVEL"]))
                    {
                        case 1:
                            lblTask.Text = drv["Job_Name"].ToString().Replace("\n", "<br>");
                            lblTask.Font.Bold = true;
                            break;
                        case 2:
                            lblTask.Text = drv["Job_Name"].ToString().Replace("\n", "<br>");
                            //------------ Check Has Sub ----------------
                            DT = JobData.Copy();
                            int _parent_no = (int)DT.Compute("MAX(Job_No)", "Job_ID=" + drv["Job_Parent"]);
                            DT.DefaultView.RowFilter = "Job_LEVEL=2 AND Job_Parent=" + drv["Job_Parent"];
                            //--------- หารายการที่อยู่ใน Sub เดียวกัน ------------ 
                            DataTable tmp = DT.DefaultView.ToTable().Copy();
                            for (int i = 0; i <= tmp.Rows.Count - 1; i++)
                            {
                                DT.DefaultView.RowFilter = "Job_Parent=" + tmp.Rows[i]["Job_ID"];
                                if (DT.DefaultView.Count > 0)
                                {
                                    lblTask.Text = _parent_no + "." + drv["Job_No"] + " " + drv["Job_Name"].ToString().Replace("\n", "<br>");
                                    lblTask.Font.Bold = true;
                                    break; // TODO: might not be correct. Was : Exit For
                                }
                            }

                            break;
                        case 3:
                            lblTask.Text = drv["Job_Name"].ToString().Replace("\n", "<br>");
                            tdTask.Style["padding-left"] = "30px";
                            break;
                    }

                    bool Editable = GL.CBOOL(drv["Editable"]);

                    txtNumPerson.Visible = Editable;
                    txtMinPerTime.Visible = Editable;
                    lblPersonMin.Visible = Editable;
                    txtTimePerYear.Visible = Editable;
                    lblTotalMin.Visible = Editable;
                    if (Editable)
                    {
                        TC.ImplementJavaIntegerText(txtNumPerson);
                        TC.ImplementJavaIntegerText(txtMinPerTime);
                        TC.ImplementJavaIntegerText(txtTimePerYear);
                        //--------------- Auto Save and KeyPress Event Handler--------------
                        txtNumPerson.Attributes["onChange"] = "updateHeader(" + drv["Job_ID"] + ",this.value,0,'NumPerson');";
                        txtMinPerTime.Attributes["onChange"] = "updateHeader(" + drv["Job_ID"] + ",this.value,0,'MinPerTime');";
                        txtTimePerYear.Attributes["onChange"] = "updateHeader(" + drv["Job_ID"] + ",this.value,0,'TimePerYear');";

                        txtNumPerson.Attributes["onFocus"] = "textboxFocused(" + drv["Job_ID"] + ",this.value,0,'NumPerson'); setLastFocusControl(this);";
                        txtMinPerTime.Attributes["onFocus"] = "textboxFocused(" + drv["Job_ID"] + ",this.value,0,'MinPerTime'); setLastFocusControl(this);";
                        txtTimePerYear.Attributes["onFocus"] = "textboxFocused(" + drv["Job_ID"] + ",this.value,0,'TimePerYear'); setLastFocusControl(this);";

                        txtNumPerson.Attributes["onKeyPress"] = "document.getElementById('ctl00_ContentPlaceHolder1_txt_Value').value=this.value;";
                        txtMinPerTime.Attributes["onKeyPress"] = "document.getElementById('ctl00_ContentPlaceHolder1_txt_Value').value=this.value;";
                        txtTimePerYear.Attributes["onKeyPress"] = "document.getElementById('ctl00_ContentPlaceHolder1_txt_Value').value=this.value;";

                        if (!GL.IsEqualNull(drv["NumPerson"]))
                        {
                            txtNumPerson.Text = GL.StringFormatNumber(drv["NumPerson"], 0);
                        }
                        cellNumPerson.Attributes["class"] += " WorkLoadBGNotFill";

                        if (!GL.IsEqualNull(drv["MinPerTime"]))
                        {
                            txtMinPerTime.Text = GL.StringFormatNumber(drv["MinPerTime"], 0);
                        }
                        cellMinPerTime.Attributes["class"] += " WorkLoadBGNotFill";

                        if (!GL.IsEqualNull(drv["TimePerYear"]))
                        {
                            txtTimePerYear.Text = GL.StringFormatNumber(drv["TimePerYear"], 0);
                        }
                        cellTimePerYear.Attributes["class"] += " WorkLoadBGNotFill";

                        cellNumPerson.Attributes["onClick"] = "document.getElementById('" + txtNumPerson.ClientID + "').focus();";
                        cellMinPerTime.Attributes["onClick"] = "document.getElementById('" + txtMinPerTime.ClientID + "').focus();";
                        cellTimePerYear.Attributes["onClick"] = "document.getElementById('" + txtTimePerYear.ClientID + "').focus();";
                    }

                    //---------- Bind Slot ---------
                    Repeater rptSlot = (Repeater)e.Item.FindControl("rptSlot");
                    rptSlot.ItemDataBound += rptSlot_ItemDataBound;
                    rptSlot.DataSource = PSNData;
                    rptSlot.DataBind();

                    break;
                case ListItemType.Footer:
                    //------------- Calculate Summary -------------
                    DT = JobData.Copy();

                    double _result;
                    object Tmp = DT.Compute("SUM(TotalMin)", "");
                    if (!GL.IsEqualNull(Tmp))
                    {
                        Label lblTotalMinPerYear = (Label)e.Item.FindControl("lblTotalMinPerYear");
                        Label lblTotalHourPerYear = (Label)e.Item.FindControl("lblTotalHourPerYear");
                        Label lblFTEYear = (Label)e.Item.FindControl("lblFTEYear");


                        _result = GL.CDBL(Tmp);
                        lblTotalMinPerYear.Text = GL.StringFormatNumber(_result, 0);
                        _result /= 60;
                        lblTotalHourPerYear.Text = GL.StringFormatNumber(_result, 2);
                        _result /= BL.MasterFTE();
                        lblFTEYear.Text = GL.StringFormatNumber(_result, 2);
                    }

                    _sumMin = SlotData.Compute("SUM(MinPerYear)", "");
                    if (!GL.IsEqualNull(_sumMin))
                    {
                        Label lblSumMinPerYear = (Label)e.Item.FindControl("lblSumMinPerYear");
                        Label lblSumHourPerYear = (Label)e.Item.FindControl("lblSumHourPerYear");
                        Label lblFTESum = (Label)e.Item.FindControl("lblFTESum");

                        _result = GL.CDBL(_sumMin);
                        lblSumMinPerYear.Text = GL.StringFormatNumber(_result.ToString(), 0);
                        _result /= 60;
                        lblSumHourPerYear.Text = GL.StringFormatNumber(_result.ToString(), 2);
                        _result /= BL.MasterFTE();
                        lblFTESum.Text = GL.StringFormatNumber(_result.ToString(), 2);
                    }

                    //------------- Display Total Personal Minute Per Year ---------
                    Repeater rptMin = (Repeater)e.Item.FindControl("rptMin");
                    rptMin.ItemDataBound += rptMin_ItemDataBound;
                    rptMin.DataSource = PSNData;
                    rptMin.DataBind();

                    //------------- Display Total Personal Hour Per Year ---------
                    Repeater rptHour = (Repeater)e.Item.FindControl("rptHour");
                    rptHour.ItemDataBound += rptHour_ItemDataBound;
                    rptHour.DataSource = PSNData;
                    rptHour.DataBind();

                    //------------- Display Total Personal FTE Per Year ---------
                    Repeater rptFTE = (Repeater)e.Item.FindControl("rptFTE");
                    rptFTE.ItemDataBound += rptFTE_ItemDataBound;
                    rptFTE.DataSource = PSNData;
                    rptFTE.DataBind();
                    break;
            }
        }

        protected void rptSlot_ItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType != ListItemType.Item & e.Item.ItemType != ListItemType.AlternatingItem)
                return;

            TextBox txtSlot = (TextBox)e.Item.FindControl("txtSlot");
            HtmlTableCell cellSlot = (HtmlTableCell)e.Item.FindControl("cellSlot");
            DataTable Slot = (DataTable)SlotData.Copy();
            DataTable Job = (DataTable)JobData.Copy();

            RepeaterItem ParentItem = (RepeaterItem)e.Item.Parent.Parent;
            Label lblNo = (Label)ParentItem.FindControl("lblNo");

            DataRowView drv = (DataRowView)e.Item.DataItem;

            //-------------- ไม่มีคนอยู่ใน Slot ------------
            if (GL.IsEqualNull(drv["PSN_ID"]))
            {
                txtSlot.Visible = false;
                return;
            }

            Job.DefaultView.RowFilter = "Job_ID=" + lblNo.Attributes["Job_ID"];

            if (Job.DefaultView.Count > 0 && GL.CBOOL(Job.DefaultView[0]["Editable"]))
            {
                txtSlot.Visible = true;
                Slot.DefaultView.RowFilter = "Job_ID=" + lblNo.Attributes["Job_ID"] + " AND PSN_ID=" + drv["PSN_ID"];
                if (Slot.DefaultView.Count > 0 && !GL.IsEqualNull(Slot.DefaultView[0]["MinPerYear"]))
                {
                    txtSlot.Text = GL.StringFormatNumber(Slot.DefaultView[0]["MinPerYear"].ToString(), 0);
                }
                //------------ Check Requirement---------
                MapClass.DefaultView.RowFilter = "Job_ID=" + lblNo.Attributes["Job_ID"] + " AND Class_ID=" + drv["Class_ID"];
                if (MapClass.DefaultView.Count > 0)
                {
                    cellSlot.Attributes["class"] += " WorkLoadBGNotFill";
                }
                TC.ImplementJavaIntegerText(txtSlot);
                cellSlot.Attributes["onClick"] = "document.getElementById('" + txtSlot.ClientID + "').focus();";
            }
            else
            {
                txtSlot.Visible = false;
            }

            txtSlot.Attributes["onChange"] = "updateHeader(" + lblNo.Attributes["Job_ID"] + ",this.value," + drv["PSN_ID"] + ",'MinPerYear');";
            txtSlot.Attributes["onFocus"] = "textboxFocused(" + lblNo.Attributes["Job_ID"] + ",this.value," + drv["PSN_ID"] + ",'MinPerYear'); setLastFocusControl(this);";
            txtSlot.Attributes["onKeyPress"] = "document.getElementById('ctl00_ContentPlaceHolder1_txt_Value').value=this.value;";
        }

        protected void rptMin_ItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
        {

            if (e.Item.ItemType != ListItemType.Item & e.Item.ItemType != ListItemType.AlternatingItem)
                return;

            DataRowView drv = (DataRowView)e.Item.DataItem;

            if (GL.IsEqualNull(drv["PSN_ID"]))
                return;

            DataTable DT = SlotData.Copy();
            object Tmp = DT.Compute("SUM(MinPerYear)", "PSN_ID=" + drv["PSN_ID"].ToString());
            if (!GL.IsEqualNull(Tmp))
            {
                Label lblMin = (Label)e.Item.FindControl("lblMin");
                lblMin.Text = GL.StringFormatNumber(Tmp.ToString(), 0).ToString();
            }
        }

        protected void rptHour_ItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType != ListItemType.Item & e.Item.ItemType != ListItemType.AlternatingItem)
                return;

            DataRowView drv = (DataRowView)e.Item.DataItem;

            if (GL.IsEqualNull(drv["PSN_ID"]))
                return;

            DataTable DT = SlotData.Copy();
            object Tmp = DT.Compute("SUM(MinPerYear)", "PSN_ID=" + drv["PSN_ID"]);


            if (!GL.IsEqualNull(Tmp))
            {
                Label lblHour = (Label)e.Item.FindControl("lblHour");

                double _result = GL.CDBL(Tmp);
                _result /= 60;
                lblHour.Text = GL.StringFormatNumber(_result.ToString(), 2);
            }
        }

        protected void rptFTE_ItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType != ListItemType.Item & e.Item.ItemType != ListItemType.AlternatingItem)
                return;
            DataRowView drv = (DataRowView)e.Item.DataItem;
            if (GL.IsEqualNull(drv["PSN_ID"]))
                return;

            DataTable DT = SlotData.Copy();
            object Tmp = DT.Compute("SUM(MinPerYear)", "PSN_ID=" + drv["PSN_ID"]);
            if (!GL.IsEqualNull(Tmp))
            {
                Label lblFTE = (Label)e.Item.FindControl("lblFTE");
                double _result = GL.CDBL(Tmp);
                _result /= 60;
                _result /= BL.MasterFTE();
                lblFTE.Text = GL.StringFormatNumber(_result.ToString(), 2);
            }
        }

        #region "Autosave Property"

        public string EditMode
        {
            get { return txt_Mode.Text; }
            set { txt_Mode.Text = value.ToString(); }
        }

        public int Job_ID
        {
            get { return GL.CINT(txt_Job_ID.Text); }
            set { txt_Job_ID.Text = value.ToString(); }
        }

        public int PSN_ID
        {
            get { return GL.CINT(txt_PSN_ID.Text); }
            set { txt_PSN_ID.Text = value.ToString(); }
        }

        public string EditValue
        {
            get { return txt_Value.Text; }
            set { txt_Value.Text = value.ToString(); }
        }

        public string DialogText
        {
            get { return txtDialog.Text; }
            set { txtDialog.Text = value.ToString(); }
        }

        #endregion
        protected void btnCalculate_Click(object sender, System.EventArgs e)
        {
            string SQL;
            switch (EditMode)
            {
                case "NumPerson":
                case "MinPerTime":
                case "TimePerYear":
                    SQL = "UPDATE tb_HR_Job_Mapping SET " + EditMode + "=";
                    if (!Information.IsNumeric(EditValue))
                    {
                        SQL += " NULL WHERE Job_ID=" + Job_ID;
                    }
                    else
                    {
                        SQL += EditValue + " WHERE Job_ID=" + Job_ID;
                    }

                    SqlConnection Conn = new SqlConnection(BL.ConnectionString());
                    Conn.Open();
                    SqlCommand COMM = new SqlCommand();
                    var _with2 = COMM;
                    _with2.CommandType = CommandType.Text;
                    _with2.Connection = Conn;
                    _with2.CommandText = SQL;
                    _with2.ExecuteNonQuery();
                    _with2.Dispose();
                    Conn.Close();
                    Conn.Dispose();

                    BindWholeTable();

                    break;
                case "MinPerYear":

                    SQL = "DELETE FROM tb_HR_Workload_Slot WHERE PSN_ID=" + PSN_ID + " AND Job_ID=" + Job_ID + "\n";
                    SqlDataAdapter DA = new SqlDataAdapter(SQL, BL.ConnectionString());
                    DataTable DT = new DataTable();
                    DA.Fill(DT);

                    SQL = " SELECT * FROM tb_HR_Workload_Slot WHERE PSN_ID=" + PSN_ID + " AND Job_ID=" + Job_ID + "\n";
                    DA = new SqlDataAdapter(SQL, BL.ConnectionString());
                    DT = new DataTable();
                    DA.Fill(DT);

                    if (!Information.IsNumeric(EditValue))
                    {
                        BindWholeTable();
                        return;
                    }

                    DataRow DR = DT.NewRow();
                    DR["Slot_ID"] = BL.GetNewWorkLoadSlotID();
                    DR["PSN_ID"] = PSN_ID;
                    DR["Job_ID"] = Job_ID;
                    DR["MinPerYear"] = Convert.ToInt32(EditValue);
                    DR["Update_By"] = PSNL_NO;
                    DR["Update_Time"] = DateAndTime.Now;
                    DT.Rows.Add(DR);
                    SqlCommandBuilder cmd = new SqlCommandBuilder(DA);
                    DA.Update(DT);

                    BindWholeTable();

                    break;
            }
        }

        protected void btnOK_Click(object sender, System.EventArgs e)
        {
            if (string.IsNullOrEmpty(Strings.Trim(DialogText)))
            {
                ScriptManager.RegisterStartupScript(this.Page, typeof(string), "Alert", "showAlert('กรอกชื่อ');", true);
                return;
            }
            switch (EditMode)
            {
                case "Add":
                    int Class_ID = PSN_ID;
                    //-------- ขอยืมช่องใส่ --------
                    string Sql = " SELECT * FROM tb_HR_Workload_PSN \n";
                    Sql += " WHERE DEPT_CODE='" + DEPT_CODE.Replace("'", "''") + "' AND MINOR_CODE='" + MINOR_CODE.Replace("'", "''") + "'\n";
                    Sql += " AND Class_ID=" + Class_ID + "\n";
                    Sql += " ORDER BY Slot_No";

                    SqlDataAdapter DA = new SqlDataAdapter(Sql, BL.ConnectionString());
                    DataTable DT = new DataTable();
                    DA.Fill(DT);

                    DataRow DR = DT.NewRow();
                    DR["PSN_ID"] = BL.GetNewWorkLoadPSNID();
                    DR["DEPT_CODE"] = DEPT_CODE;
                    DR["MINOR_CODE"] = MINOR_CODE;
                    DR["Class_ID"] = Class_ID;
                    if (DT.Rows.Count > 0)
                    {
                        int tmp = GL.CINT(DT.Rows[DT.Rows.Count - 1]["Slot_No"].ToString());
                        DR["Slot_No"] = tmp + 1;
                    }
                    else
                    {
                        DR["Slot_No"] = 1;
                    }
                    DR["Slot_Name"] = DialogText;
                    DR["REF_PSNL_NO"] = DBNull.Value;
                    //----------- Manual Add ------------
                    DR["Update_By"] = PSNL_NO;
                    DR["Update_Time"] = DateAndTime.Now;
                    DT.Rows.Add(DR);

                    SqlCommandBuilder cmd = new SqlCommandBuilder(DA);
                    DA.Update(DT);
                    BindWholeTable();

                    break;
                case "Rename":
                    string SQL = "UPDATE tb_HR_Workload_PSN SET Slot_Name='" + DialogText.Replace("'", "''") + "'\n";
                    SQL += " WHERE PSN_ID=" + PSN_ID;

                    SqlConnection Conn = new SqlConnection(BL.ConnectionString());
                    Conn.Open();
                    SqlCommand COMM = new SqlCommand();
                    var _with3 = COMM;
                    _with3.CommandType = CommandType.Text;
                    _with3.Connection = Conn;
                    _with3.CommandText = SQL;
                    _with3.ExecuteNonQuery();
                    _with3.Dispose();
                    Conn.Close();
                    Conn.Dispose();

                    BindClass();
                    BindPSN();
                    BindHeader();
                    break;
            }
        }

        protected void btnDefault_Click(object sender, System.EventArgs e)
        {
            BL.SetWorkloadDefaultPSN(DEPT_CODE, MINOR_CODE);
            BindWholeTable();
        }

        protected void btnClear_Click(object sender, System.EventArgs e)
        {
            string SQL = "";

            SQL += " DELETE FROM tb_HR_Workload_Slot WHERE JOB_ID IN\n";
            SQL += " (SELECT DISTINCT JOB_ID FROM tb_HR_Job_Mapping \n";
            SQL += " WHERE PNDP_DEPT_CODE='" + DEPT_CODE.Replace("'", "''") + "' AND PNDP_MINOR_CODE='" + MINOR_CODE.Replace("'", "''") + "')\n\n";

            SQL += " UPDATE tb_HR_Job_Mapping SET NumPerson=Null, MinPerTime=NUll, TimePerYear=Null\n";
            SQL += " WHERE PNDP_DEPT_CODE='" + DEPT_CODE.Replace("'", "''") + "' AND PNDP_MINOR_CODE='" + MINOR_CODE.Replace("'", "''") + "' \n\n";

            SqlConnection Conn = new SqlConnection(BL.ConnectionString());
            Conn.Open();
            SqlCommand COMM = new SqlCommand();
            var _with4 = COMM;
            _with4.CommandType = CommandType.Text;
            _with4.Connection = Conn;
            _with4.CommandText = SQL;
            _with4.ExecuteNonQuery();
            _with4.Dispose();
            Conn.Close();
            Conn.Dispose();

            BindWholeTable();

        }
    }
}

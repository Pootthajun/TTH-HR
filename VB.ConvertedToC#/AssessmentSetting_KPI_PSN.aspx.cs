using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Xml.Linq;
using System.Diagnostics;
using System.Collections.Specialized;
using System.Configuration;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Caching;
using System.Web.SessionState;
using System.Web.Security;
using System.Web.Profile;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
namespace VB
{

	public partial class AssessmentSetting_KPI_PSN : System.Web.UI.Page
	{


		HRBL BL = new HRBL();
		GenericLib GL = new GenericLib();

		textControlLib TC = new textControlLib();
		public int R_Year {
			get {
				try {
					return GL.CINT( GL.SplitString(ddlRound.Items[ddlRound.SelectedIndex].Value, "-")[0]);
				} catch (Exception ex) {
					return 0;
				}
			}
		}

		public int R_Round {
			get {
				try {
					return GL.CINT(GL.SplitString(ddlRound.Items[ddlRound.SelectedIndex].Value, "-")[1]);
				} catch (Exception ex) {
					return 0;
				}
			}
		}

		public string PSNL_NO {
			get { return lblPSNName.Attributes["PSNL_NO"]; }
			set { lblPSNName.Attributes["PSNL_NO"] = value; }
		}

		public string ASSESSOR_BY {
			get {
				try {
					string[] tmp = Strings.Split(ddlASSName.Items[ddlASSName.SelectedIndex].Value, ":::");
					return tmp[0];
				} catch (Exception ex) {
					return "";
				}
			}
		}

		public string ASSESSOR_NAME {
			get {
				try {
					return ddlASSName.Items[ddlASSName.SelectedIndex].Text;
				} catch (Exception ex) {
					return "";
				}
			}
		}

		public string ASSESSOR_POS {
			get {
				try {
					string[] tmp = Strings.Split(ddlASSName.Items[ddlASSName.SelectedIndex].Value, ":::");
					return tmp[1];
				} catch (Exception ex) {
					return "";
				}
			}
		}

		public string ASSESSOR_DEPT {
			get {
				try {
					string[] tmp = Strings.Split(ddlASSName.Items[ddlASSName.SelectedIndex].Value, ":::");
					return tmp[2];
				} catch (Exception ex) {
					return "";
				}
			}
		}

		//--------------------- Get Assessor First--------------------
		public DataTable AssessorList {
            get { return (DataTable)Session["AssessmentSetting_KPI_PSN_Assessor"]; }
			set { Session["AssessmentSetting_KPI_PSN_Assessor"] = value; }
		}

        //--------------------- Get Assessor rptKPI--------------------
        public DataTable AssessorList_rptKPI
        {
            get { return (DataTable)Session["rptKPI_KPI_PSN_Assessor"]; }
            set { Session["rptKPI_KPI_PSN_Assessor"] = value; }
        }


		public int KPI_Status {
            get { return GL.CINT (ddlKPIStatus.Items[ddlKPIStatus.SelectedIndex].Value); }
			set {
				ddlKPIStatus.SelectedIndexChanged -= ddlKPIStatus_SelectedIndexChanged;
				ddlKPIStatus.SelectedIndex = 0;
				for (int i = 0; i <= ddlKPIStatus.Items.Count - 1; i++) {
					if (Convert.ToInt32(ddlKPIStatus.Items[i].Value) == Convert.ToInt32(value)) {
						ddlKPIStatus.SelectedIndex = i;
					}
				}
				ddlKPIStatus.SelectedIndexChanged += ddlKPIStatus_SelectedIndexChanged;
			}
		}

		protected void Page_Load(object sender, System.EventArgs e)
		{
			if ((Session["USER_PSNL_NO"] == null)) {
				ScriptManager.RegisterStartupScript(this.Page, typeof(string), "Alert", "showAlert('กรุณาลงชื่อเข้าใช้ระบบ');", true);
				Response.Redirect("Login.aspx");
				return;
			}


			if (!IsPostBack) {

                // Keep Start Time
                DateTime StartTime = DateTime.Now;

                //------------ Get All Assessor ------------
				BindAllAssessor();
                BindAssessor_rptKPI();
				//------------ Do it From Team KPI ---------
				BL.BindDDlSector(ddlSector);
				//------------ Remove อยส -----------
				for (int i = 0; i <= ddlSector.Items.Count - 1; i++) {
					if (ddlSector.Items[i].Value == "10") {
						ddlSector.Items.RemoveAt(i);
						break; // TODO: might not be correct. Was : Exit For
					}
				}
				BindFixedAssessor();

				BL.BindDDlYearRound(ddlRound);
                //----------------- ทำสองที่ Page Load กับ Update Status ---------------
                //BL.Update_KPI_Status_To_Assessment_Period("",R_Year,R_Round);

				BindPersonalList();
				pnlList.Visible = true;
				pnlEdit.Visible = false;
				pnlAddAss.Visible = false;

                //Report Time To Entry This Page
                DateTime EndTime = DateTime.Now;
                lblReportTime.Text = "เริ่มเข้าสู่หน้านี้ " + StartTime.ToString("HH:mm:ss.ff") + " ถึง " + EndTime.ToString("HH:mm:ss.ff") + " ใช้เวลา " + Math.Round( GL.DiffTimeDecimalSeconds(StartTime,EndTime),2) + " วินาที";
			}
		}


		protected void Search(object sender, System.EventArgs e)
		{
			BindPersonalList();
		}

		private void BindAllAssessor()
		{
			string SQL = " SELECT DISTINCT R_Year,R_Round,MGR_PSNL_NO ASSESSOR_CODE,\n";
			SQL += " MGR_PSNL_Fullname ASSESSOR_NAME,PSN_PSNL_NO PSNL_NO\n";
			SQL += " FROM vw_HR_ASSESSOR_PSN\n";
			SqlDataAdapter DA = new SqlDataAdapter(SQL, BL.ConnectionString());
			DA.SelectCommand.CommandTimeout = 90;
			DataTable DT = new DataTable();
			DA.Fill(DT);

			AssessorList = DT;
		}

        private void BindAssessor_rptKPI()
        {
            string SQL = " ";
            SQL += " SELECT DISTINCT R_Year,R_Round,MGR_PSNL_NO ASSESSOR_CODE,\n";
            SQL += " MGR_PSNL_Fullname ASSESSOR_NAME,PSNL_NO\n";
            SQL += " FROM tb_HR_Actual_Assessor\n";

            SqlDataAdapter DA = new SqlDataAdapter(SQL, BL.ConnectionString());
            DataTable DT = new DataTable();
            DA.Fill(DT);

            AssessorList_rptKPI = DT;
        }


		private void BindPersonalList()
		{
            string SQL = "SELECT DISTINCT vw.R_Year,vw.R_Round,vw.SECTOR_CODE,vw.SECTOR_NAME,vw.DEPT_CODE,vw.DEPT_NAME,vw.PSNL_NO,vw.PSNL_Fullname,vw.PNPS_CLASS,vw.WAGE_NAME\n";
			SQL += " ,vw.POS_NO,vw.POS_Name,vw.KPI_Status,vw.KPI_Status_Name\n";
			SQL += " FROM vw_RPT_KPI_Status vw\n";
 SQL += " LEFT JOIN vw_PN_PSNL_ALL PSN ON vw.PSNL_NO=PSN.PSNL_NO  \n";
 SQL += " WHERE (ISNULL(PNPS_RESIGN_DATE,PNPS_RETIRE_DATE) > (SELECT MAX(R_End) FROM tb_HR_Round WHERE  R_Year=" + R_Year + " AND R_Round=" + R_Round + ")) \n";
           

			SQL += "ORDER BY SECTOR_CODE,DEPT_CODE,PNPS_CLASS DESC,POS_NO\n";


            string filter = "SECTOR_CODE<>'10' AND R_Year=" + R_Year + " AND R_Round=" + R_Round + "\n";
            // เปลี่ยนจาก Where ใน View เป็นใช้ Row Filter
            if (ddlSector.SelectedIndex > 0)
            {
                filter += " AND (SECTOR_CODE='" + ddlSector.Items[ddlSector.SelectedIndex].Value + "' \n";
                if (ddlSector.Items[ddlSector.SelectedIndex].Value == "3200")
                {
                    filter += " AND DEPT_CODE NOT IN ('32000300'))\n ";
                    //-----------ฝ่ายโรงงานผลิตยาสูบ 3
                }
                else if (ddlSector.Items[ddlSector.SelectedIndex].Value == "3203")
                {
                    filter += " OR DEPT_CODE IN ('32000300','32030000','32030100','32030200','32030300','32030500'))\n";
                    //-----------ฝ่ายโรงงานผลิตยาสูบ 4
                }
                else if (ddlSector.Items[ddlSector.SelectedIndex].Value == "3204")
                {
                    filter += " OR DEPT_CODE IN ('32040000','32040100','32040200','32040300','32040500','32040300'))\n";
                    //-----------ฝ่ายโรงงานผลิตยาสูบ 5
                }
                else if (ddlSector.Items[ddlSector.SelectedIndex].Value == "3205")
                {
                    filter += " OR DEPT_CODE IN ('32050000','32050100','32050200','32050300','32050500'))\t";
                }
                else
                {
                    filter += ")\n";
                }
            }
            if (txt_Search_Name.Text != "")
            {
                filter += " AND (PSNL_NO LIKE '%" + txt_Search_Name.Text.Replace("'", "''") + "%' \n";
                filter += " OR PSNL_Fullname LIKE '%" + txt_Search_Name.Text.Replace("'", "''") + "%' \n";
                filter += " OR POS_Name LIKE '%" + txt_Search_Name.Text.Replace("'", "''") + "%')\n";
            }
            if (txt_Search_Organize.Text != "")
            {
                filter += " AND (SECTOR_NAME LIKE '%" + txt_Search_Organize.Text.Replace("'", "''") + "%' OR \n";
                filter += " DEPT_NAME LIKE '%" + txt_Search_Organize.Text.Replace("'", "''") + "%')\n";
            }
            switch (ddlStatus.Items[ddlStatus.SelectedIndex].Value)
            {
                case "-1":
                    filter += "";
                    break;
                case "0":
                    filter += " AND (KPI_Status IS NULL OR KPI_Status IN (0,-1)) \n";
                    break;
                case "1":
                case "2":
                case "3":
                case "4":
                case "5":
                    filter += " AND KPI_Status =" + ddlStatus.Items[ddlStatus.SelectedIndex].Value + "\n";
                    break;
            }

			SqlDataAdapter DA = new SqlDataAdapter(SQL, BL.ConnectionString());
			DA.SelectCommand.CommandTimeout = 100;
			DataTable DT = new DataTable();
			DA.Fill(DT);

            string[] ditinct_col=new string[] {"SECTOR_CODE","SECTOR_NAME","DEPT_CODE","DEPT_NAME","PSNL_NO","PSNL_Fullname","PNPS_CLASS","WAGE_NAME","POS_NO","POS_Name","KPI_Status","KPI_Status_Name"};
            DT.DefaultView.RowFilter = filter;

            Session["Setting_Personal"] = DT.DefaultView.ToTable(true, ditinct_col);
			Pager.SesssionSourceName = "Setting_Personal";
			Pager.RenderLayout();

            if (DT.DefaultView.Count == 0)
            {
				lblCountPSN.Text = "ไม่พบรายการดังกล่าว";
			} else {
                lblCountPSN.Text = "พบ " + GL.StringFormatNumber(DT.DefaultView.Count, 0) + " รายการ";
			}

		}

		protected void Pager_PageChanging(PageNavigation Sender)
		{
			Pager.TheRepeater = rptKPI;
		}

		protected void rptKPI_ItemCommand(object source, System.Web.UI.WebControls.RepeaterCommandEventArgs e)
		{
			switch (e.CommandName) {
				case "Edit":
					Button btnPSNEdit =(Button) e.Item.FindControl("btnPSNEdit");
					//-------------- ดึงข้อมูลบุคคล ------------
					PSNL_NO = btnPSNEdit.CommandArgument;
					BindPersonal();
					KPI_Status = BL.GetKPIStatus(R_Year, R_Round, PSNL_NO);
					BindAssessor();
					BindMasterKPI();
					pnlList.Visible = false;
					pnlEdit.Visible = true;

					SetPrintButton();
					break;
			}
		}

		private void SetPrintButton()
		{
			btnPDF.HRef = "Print/RPT_K_PNSAss.aspx?MODE=PDF&R_Year=" + R_Year + "&R_Round=" + R_Round + "&PSNL_No=" + PSNL_NO + "&Status=" + KPI_Status;
			btnExcel.HRef = "Print/RPT_K_PNSAss.aspx?MODE=EXCEL&R_Year=" + R_Year + "&R_Round=" + R_Round + "&PSNL_No=" + PSNL_NO + "&Status=" + KPI_Status;
		}

		//------------ For Grouping -----------
		string LastSECTOR = "";
		string LastDEPT = "";
		protected void rptKPI_ItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
		{
			if (e.Item.ItemType != ListItemType.AlternatingItem & e.Item.ItemType != ListItemType.Item)
				return;

			Label lnkPSNSector =(Label) e.Item.FindControl("lnkPSNSector");
			Label lnkPSNDept = (Label)e.Item.FindControl("lnkPSNDept");
			Label lblPSNName = (Label)e.Item.FindControl("lblPSNName");
			 Label lblPSNPos = (Label)e.Item.FindControl("lblPSNPos");
			 Label lblPSNClass = (Label)e.Item.FindControl("lblPSNClass");
			Label lblPSNType =(Label) e.Item.FindControl("lblPSNType");
			Label lblPSNASSESSOR =(Label) e.Item.FindControl("lblPSNASSESSOR");
			Label lblPSNKPIStatus =(Label) e.Item.FindControl("lblPSNKPIStatus");
			Button btnPSNEdit =(Button) e.Item.FindControl("btnPSNEdit");

            DataRowView drv = (DataRowView)e.Item.DataItem;
			if (LastSECTOR != drv["SECTOR_NAME"].ToString()) {
				lnkPSNSector.Text = drv["SECTOR_NAME"].ToString();
				LastSECTOR = drv["SECTOR_NAME"].ToString();
				lnkPSNDept.Text = drv["DEPT_NAME"].ToString();
				LastDEPT = drv["DEPT_NAME"].ToString();
			} else if (LastDEPT != drv["DEPT_NAME"].ToString()) {
				lnkPSNDept.Text = drv["DEPT_NAME"].ToString();
				LastDEPT = drv["DEPT_NAME"].ToString();
			}

			lblPSNName.Text = drv["PSNL_NO"] + " : " + drv["PSNL_Fullname"];
			lblPSNPos.Text = drv["POS_Name"].ToString ();
			if (!GL.IsEqualNull(drv["PNPS_CLASS"])) {
				lblPSNClass.Text = GL.CINT (drv["PNPS_CLASS"]).ToString ();
			} else {
				lblPSNClass.Text = "-";
			}
			lblPSNType.Text =drv["WAGE_NAME"].ToString ();

			//lblPSNASSESSOR
            //DataTable ASS = AssessorList.Copy();  //เดิมแสดงรายชื่อผู้ประเมินทั้งปัจจุบันและที่เคยเลือก

            DataTable ASS = AssessorList_rptKPI.Copy(); 

			string Filter = "(R_Year IS NULL OR R_Year=" + R_Year + ") AND (R_Round IS NULL OR R_Round=" + R_Round + ")";
			Filter += " AND PSNL_NO='" + drv["PSNL_NO"].ToString().Replace("'", "''") + "' AND ASSESSOR_CODE<>'" + drv["PSNL_NO"].ToString().Replace("'", "''") + "'";
			ASS.DefaultView.RowFilter = Filter;

            if (ASS.DefaultView.Count == 0)
            {
                ASS = AssessorList.Copy();
                ASS.DefaultView.RowFilter = Filter;
            }

			string tmp = "";
			for (int i = 0; i <= ASS.DefaultView.Count - 1; i++) {
				tmp += ASS.DefaultView[i]["ASSESSOR_NAME"] + "<br>";
			}
			lblPSNASSESSOR.Text = tmp;

			lblPSNKPIStatus.Text = drv["KPI_Status_Name"].ToString ();
			

            //------ปรับสีสถานะ-----------
            //----<0
            if (GL.IsEqualNull(drv["KPI_Status"]) || GL.CINT(drv["KPI_Status"]) == HRBL.AssessmentStatus.Creating)
            {
                lblPSNKPIStatus.ForeColor = System.Drawing.Color.Red;
                //----1
            }
            else if (GL.IsEqualNull(drv["KPI_Status"]) || GL.CINT(drv["KPI_Status"]) == HRBL.AssessmentStatus.WaitCreatingApproved)
            {
                lblPSNKPIStatus.ForeColor = System.Drawing.Color.DarkRed;
                //----2
            }
            else if (GL.IsEqualNull(drv["KPI_Status"]) || GL.CINT(drv["KPI_Status"]) == HRBL.AssessmentStatus.CreatedApproved)
            {
                lblPSNKPIStatus.ForeColor = System.Drawing.Color.Orange;
                //----3
            }
            else if (GL.IsEqualNull(drv["KPI_Status"]) || GL.CINT(drv["KPI_Status"]) == HRBL.AssessmentStatus.WaitAssessment)
            {
                lblPSNKPIStatus.ForeColor = System.Drawing.Color.DarkBlue;
                //----4
            }
            else if (GL.IsEqualNull(drv["KPI_Status"]) || GL.CINT(drv["KPI_Status"]) == HRBL.AssessmentStatus.WaitConfirmAssessment)
            {
                lblPSNKPIStatus.ForeColor = System.Drawing.Color.BlueViolet;
                //----5
            }
            else if (GL.IsEqualNull(drv["KPI_Status"]) || GL.CINT(drv["KPI_Status"]) == HRBL.AssessmentStatus.AssessmentCompleted)
            {
                lblPSNKPIStatus.ForeColor = System.Drawing.Color.Green;
            }


			btnPSNEdit.CommandArgument = drv["PSNL_NO"].ToString ();

		}

		private void BindPersonal()
		{
			//----------Personal Info ----------
             int ret;

			ret = BL.GetKPIStatus(R_Year, R_Round, PSNL_NO);
            KPI_Status = ret ;
			HRBL.PersonalInfo PSNInfo = BL.GetAssessmentPersonalInfo(R_Year, R_Round, PSNL_NO, KPI_Status);
			lblPSNName.Text = PSNInfo.PSNL_Fullname;
			lblPSNDept.Text = PSNInfo.DEPT_NAME;
			if (!string.IsNullOrEmpty(PSNInfo.MGR_NAME)) {
				lblPSNPos.Text = PSNInfo.MGR_NAME;
			} else {
				lblPSNPos.Text = PSNInfo.FN_NAME;
			}

		}

		private void BindAssessor()
		{
			BL.BindDDLAssessor(ddlASSName, R_Year, R_Round, PSNL_NO);
			ddlASSName_SelectedIndexChanged(null, null);
			//----------- Lock ------------
			ddlASSName.Enabled = !BL.Is_Round_Completed(R_Year, R_Round);
		}

		protected void ddlASSName_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			lblASSPos.Text = ASSESSOR_POS;
			lblASSDept.Text = ASSESSOR_DEPT;
			SaveHeader();
		}

		private void BindMasterKPI()
		{
			lblRound.Text = ddlRound.Items[ddlRound.SelectedIndex].Text;
			//---------------- Set Status -------------------
			KPI_Status = BL.GetKPIStatus(R_Year, R_Round, PSNL_NO);
			//--------------- Bind Detail----------------------
			HRBL.DataManager DM = BL.GetKPIDetail(R_Year, R_Round, PSNL_NO);
			DataTable DT = DM.Table;
			SqlDataAdapter DA = DM.Adaptor;
			rptAss.DataSource = DM.Table;
			rptAss.DataBind();

			SaveHeader();
		}

		protected void rptAss_ItemCommand(object source, System.Web.UI.WebControls.RepeaterCommandEventArgs e)
		{

            HtmlTableCell ass_no = (HtmlTableCell)e.Item.FindControl("ass_no");
            int KPI_No = Convert.ToInt32(Conversion.Val(ass_no.InnerHtml));
            SqlCommandBuilder cmd;
            DataTable DT;
            HRBL.DataManager DM;
            Button btn;
            int SelectedChoice;

			switch (e.CommandName) {
				case "SelectOff":

                    btn = (Button)e.CommandSource;
					SelectedChoice = Convert.ToInt32(btn.ID.Replace("btnOff_", ""));					
					DM = BL.GetKPIDetail(R_Year, R_Round, PSNL_NO);
					DT = DM.Table;
					DT.DefaultView.RowFilter = "KPI_No=" + KPI_No;
					if (DT.DefaultView.Count > 0) {
						DataRow DR = DT.DefaultView[0].Row;
						DR["KPI_Answer_PSN"] = SelectedChoice;
					}

					cmd = new SqlCommandBuilder(DM.Adaptor);
					DM.Adaptor.Update(DT);
					DT.AcceptChanges();
					DT.DefaultView.RowFilter = "";
					rptAss.DataSource = DT;
					rptAss.DataBind();

					break;
				case "SelectHead":

                    btn = (Button)e.CommandSource;
					SelectedChoice = Convert.ToInt32(btn.ID.Replace("btnHead_", ""));
					DM = BL.GetKPIDetail(R_Year, R_Round, PSNL_NO);
					DT = DM.Table;
					DT.DefaultView.RowFilter = "KPI_No=" + KPI_No;
					if (DT.DefaultView.Count > 0) {
						DataRow DR = DT.DefaultView[0].Row;
						DR["KPI_Answer_MGR"] = SelectedChoice;
					}

					cmd = new SqlCommandBuilder(DM.Adaptor);
					DM.Adaptor.Update(DT);
					DT.AcceptChanges();
					DT.DefaultView.RowFilter = "";
					rptAss.DataSource = DT;
					rptAss.DataBind();

					break;
				case "Remark_MGR":

                    TextBox txtMGR = (TextBox)e.Item.FindControl("txtMGR");
                    //-------------- Must Update This -----------
                    if (txtMGR.Text != "")
                    {
                        if (txtMGR.Text.Length > 3990)
                        {
                            txtMGR.Text = txtMGR.Text.Substring(0, 3990);
                        }

                    }
					DM = BL.GetKPIDetail(R_Year, R_Round, PSNL_NO);
					DT = DM.Table;
					DT.DefaultView.RowFilter = "KPI_No=" + KPI_No;
					if (DT.DefaultView.Count > 0) {
						DataRow DR = DT.DefaultView[0].Row;
						DR["KPI_Remark_MGR"] = txtMGR.Text;
					}
					cmd = new SqlCommandBuilder(DM.Adaptor);
					DM.Adaptor.Update(DT);
					DT.AcceptChanges();
					DT.DefaultView.RowFilter = "";
					rptAss.DataSource = DT;
					rptAss.DataBind();

					break;
				case "Remark_PSN":

                    TextBox txtPSN = (TextBox)e.Item.FindControl("txtPSN");
                    //-------------- Must Update This -----------
                    if (txtPSN.Text != "")
                    {
                        if (txtPSN.Text.Length > 3990)
                        {
                            txtPSN.Text = txtPSN.Text.Substring(0, 3990);
                        }

                    }
					DM = BL.GetKPIDetail(R_Year, R_Round, PSNL_NO);
					DT = DM.Table;
					DT.DefaultView.RowFilter = "KPI_No=" + KPI_No;
					if (DT.DefaultView.Count > 0) {
						DataRow DR = DT.DefaultView[0].Row;
						DR["KPI_Remark_PSN"] = txtPSN.Text;
					}
					cmd = new SqlCommandBuilder(DM.Adaptor);
					DM.Adaptor.Update(DT);
					DT.AcceptChanges();
					DT.DefaultView.RowFilter = "";
					rptAss.DataSource = DT;
					rptAss.DataBind();

					break;
				case "Edit":

					DT = BL.GetKPIDetail(R_Year, R_Round, PSNL_NO).Table;		
					DT.DefaultView.RowFilter = "KPI_No=" + KPI_No;
					if (DT.DefaultView.Count == 0) {
						ScriptManager.RegisterStartupScript(this.Page, typeof(string), "Alert", "showAlert('กำลังปรับปรุงข้อมูลล่าสุด');", true);
						BindMasterKPI();
						return;
					}

					//-----------------------------
					ClearEditForm();
					lblKPIRound.Text = ddlRound.Text;
					lblKPINo.Text = KPI_No.ToString().PadLeft(2, GL.chr0);
					txt_KPI_Job.Text = DT.DefaultView[0]["KPI_Job"].ToString();
                    txt_KPI_Target.Text = DT.DefaultView[0]["KPI_Target_Text"].ToString();
                    txt_KPI_Choice1.Text = DT.DefaultView[0]["KPI_Choice_1"].ToString();
                    txt_KPI_Choice2.Text = DT.DefaultView[0]["KPI_Choice_2"].ToString();
                    txt_KPI_Choice3.Text = DT.DefaultView[0]["KPI_Choice_3"].ToString();
                    txt_KPI_Choice4.Text = DT.DefaultView[0]["KPI_Choice_4"].ToString();
                    txt_KPI_Choice5.Text = DT.DefaultView[0]["KPI_Choice_5"].ToString();
					txt_KPI_Weight.Text = DT.DefaultView[0]["KPI_Weight"].ToString();


					pnlEdit.Visible = false;
					divEdit.Visible = true;

					break;
				case "Delete":

					string SQL = "DELETE FROM tb_HR_KPI_Detail \n";
					SQL += " WHERE R_Year=" + R_Year + " AND R_Round=" + R_Round + "\n";
					SQL += " AND PSNL_NO='" + PSNL_NO.Replace("'", "''") + "' AND KPI_No=" + KPI_No + "\n";

					SqlConnection Conn = new SqlConnection(BL.ConnectionString());
					Conn.Open();
					SqlCommand Comm = new SqlCommand();
					var _with1 = Comm;
					_with1.Connection = Conn;
					_with1.CommandType = CommandType.Text;
					_with1.CommandText = SQL;
					_with1.ExecuteNonQuery();
					_with1.Dispose();
					Conn.Close();
					Conn.Dispose();

					//-----------------Start REORDER ---------------
					DM = BL.GetKPIDetail(R_Year, R_Round, PSNL_NO);
					DT = DM.Table;
					SqlDataAdapter DA = DM.Adaptor;
					//---------- Need To Reorder------------
					bool NeedToReOrder = false;
					for (int i = 0; i <= DT.Rows.Count - 1; i++) {
						if ( GL.CINT(DT.Rows[i]["KPI_No"].ToString()) != i + 1) {
							DT.Rows[i]["KPI_No"] = i + 1;
							NeedToReOrder = true;
						}
					}

					if (NeedToReOrder) {
						cmd = new SqlCommandBuilder(DA);
						DA.Update(DT);
						DT.AcceptChanges();
					}
					//-----------------End REORDER ---------------

					BindMasterKPI();
					break;
			}
		}

		protected void rptAss_ItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
		{
			switch (e.Item.ItemType) {

				case ListItemType.AlternatingItem:
				case ListItemType.Item:

					HtmlTableCell ass_no =(HtmlTableCell)  e.Item.FindControl("ass_no");
					HtmlTableCell job =(HtmlTableCell) e.Item.FindControl("job");
					HtmlTableCell target =(HtmlTableCell) e.Item.FindControl("target");
					HtmlTableCell weight =(HtmlTableCell) e.Item.FindControl("weight");
					HtmlTableCell cel_officer =(HtmlTableCell) e.Item.FindControl("cel_officer");
					HtmlTableCell cel_header =(HtmlTableCell) e.Item.FindControl("cel_header");
					HtmlTableCell mark =(HtmlTableCell) e.Item.FindControl("mark");
					Button btn_Edit =(Button) e.Item.FindControl("btn_Edit");

					Button btn_Remark_MGR =(Button) e.Item.FindControl("btn_Remark_MGR");
					Button btn_Remark_PSN =(Button) e.Item.FindControl("btn_Remark_PSN");
					TextBox txtPSN =(TextBox) e.Item.FindControl("txtPSN");
					TextBox txtMGR =(TextBox) e.Item.FindControl("txtMGR");

                    DataRowView drv = (DataRowView)e.Item.DataItem;
					ass_no.InnerHtml = drv["KPI_No"].ToString ();
					//.ToString().PadLeft(2, GL.chr0)
					job.InnerHtml = drv["KPI_Job"].ToString().Replace("\n", "<br>");
					target.InnerHtml = drv["KPI_Target_Text"].ToString().Replace("\n", "<br>");
					weight.InnerHtml = drv["KPI_Weight"].ToString();

					txtPSN.Text = drv["KPI_Remark_PSN"].ToString();
					txtMGR.Text = drv["KPI_Remark_MGR"].ToString();

					for (int i = 1; i <= 5; i++) {
						HtmlTableCell choice =(HtmlTableCell) e.Item.FindControl("choice" + i);
						HtmlTableCell selHeaderColor =(HtmlTableCell) e.Item.FindControl("selHeaderColor" + i);
						Button btn =(Button) e.Item.FindControl("btn" + i);
						choice.InnerHtml = drv["KPI_Choice_" + i.ToString()].ToString().Replace("\n", "<br>");
					}



					int Officer = 0;
					int Manager = 0;
					 if (!GL.IsEqualNull(drv["KPI_Answer_PSN"]) && Information.IsNumeric(drv["KPI_Answer_PSN"]))
						Officer =GL.CINT ( drv["KPI_Answer_PSN"]);
					 if (!GL.IsEqualNull(drv["KPI_Answer_MGR"]) && Information.IsNumeric(drv["KPI_Answer_MGR"]))
						Manager =  GL.CINT (drv["KPI_Answer_MGR"]);

					//------------- Set Officer Selected Choice-----------
					if (Officer > 0) {
						cel_officer.Attributes["class"] = "AssLevel" + Officer;
						for (int j = 1; j <= Officer; j++) {
							HtmlTableCell _selOfficialColor =(HtmlTableCell) e.Item.FindControl("selOfficialColor" + j);
							_selOfficialColor.Attributes["class"] = "AssLevel" + drv["KPI_Answer_PSN"];
						}
						//------------- Collect value-------------
						cel_officer.Attributes["KPI_Answer_PSN"] = GL.CINT ( Officer).ToString ();
					}

					//------------ Set Manager Selected Choice -----------
					//---------- หัวหน้าประเมิน ---------
					if (Manager > 0) {
						cel_header.Attributes["class"] = "AssLevel" + Manager;
						for (int j = 1; j <= Manager; j++) {
							HtmlTableCell _selHeaderColor =(HtmlTableCell) e.Item.FindControl("selHeaderColor" + j);
							_selHeaderColor.Attributes["class"] = "AssLevel" + Manager;
						}
						weight.Attributes["class"] = "AssLevel" + Manager;
						mark.Attributes["class"] = "AssLevel" + Manager;
						//------------- Collect value-------------
						cel_header.Attributes["KPI_Answer_MGR"] =  GL.CINT (Manager).ToString ();
						//------------- Set Delelet Action --------
					}


					//--------------- Set Text ------------
					if (Manager > 0) {
						mark.InnerHtml = Manager.ToString ();
						job.Attributes["class"] = "TextLevel" + Manager;
						target.Attributes["class"] = "TextLevel" + Manager;
						HtmlTableCell _choice =(HtmlTableCell) e.Item.FindControl("choice" + Manager);
						_choice.Attributes["class"] = "TextLevel" + Manager;
					} else {
						mark.InnerHtml = "-";
					}

					//-------------- Set Action --------------
					txtMGR.Attributes["onchange"] = "document.getElementById('" + btn_Remark_MGR.ClientID + "').click();";
					txtPSN.Attributes["onchange"] = "document.getElementById('" + btn_Remark_PSN.ClientID + "').click();";
					for (int i = 1; i <= 5; i++) {
						HtmlTableCell choice =(HtmlTableCell) e.Item.FindControl("choice" + i);

						HtmlTableCell selOfficialColor =(HtmlTableCell) e.Item.FindControl("selOfficialColor" + i);
						Button btnOff =(Button) e.Item.FindControl("btnOff_" + i);
						selOfficialColor.Attributes["onclick"] = "document.getElementById('" + btnOff.ClientID + "').click();";

						HtmlTableCell selHeaderColor =(HtmlTableCell) e.Item.FindControl("selHeaderColor" + i);
						Button btnHead =(Button) e.Item.FindControl("btnHead_" + i);
						selHeaderColor.Attributes["onclick"] = "document.getElementById('" + btnHead.ClientID + "').click();";
						Button btn =(Button) e.Item.FindControl("btn" + i);
						choice.Attributes["onclick"] = "document.getElementById('" + btn_Edit.ClientID + "').click();";
					}

					ass_no.Attributes["onclick"] = "document.getElementById('" + btn_Edit.ClientID + "').click();";
					job.Attributes["onclick"] = "document.getElementById('" + btn_Edit.ClientID + "').click();";
					target.Attributes["onclick"] = "document.getElementById('" + btn_Edit.ClientID + "').click();";
					weight.Attributes["onclick"] = "document.getElementById('" + btn_Edit.ClientID + "').click();";

					//--------------- Delete Action---------------
					HtmlTableCell cel_delete =(HtmlTableCell) e.Item.FindControl("cel_delete");
					Button btn_Delete =(Button) e.Item.FindControl("btn_Delete");
					AjaxControlToolkit.ConfirmButtonExtender cfm_Delete =(AjaxControlToolkit.ConfirmButtonExtender) e.Item.FindControl("cfm_Delete");
					cel_delete.Attributes["onclick"] = "document.getElementById('" + btn_Delete.ClientID + "').click();";
					cfm_Delete.ConfirmText = "ยืนยันลบแบบประเมินรายการที่ " + ass_no.InnerHtml;

					break;
				case ListItemType.Footer:
					//----------------Report Summary ------------

					//----------- Control Difinition ------------
					HtmlTableCell cell_sum_mark =(HtmlTableCell) e.Item.FindControl("cell_sum_mark");
					HtmlTableCell cell_sum_weight =(HtmlTableCell) e.Item.FindControl("cell_sum_weight");
					HtmlTableCell cell_total =(HtmlTableCell) e.Item.FindControl("cell_total");
					Label lblDone =(Label) e.Item.FindControl("lblDone");
					Label lblTotalItem =(Label) e.Item.FindControl("lblTotalItem");
					HtmlTableCell cell_sum_raw =(HtmlTableCell) e.Item.FindControl("cell_sum_raw");

					//------------ Report -------------
					DataTable DT =(DataTable) rptAss.DataSource;
					DT.Columns.Add("Final_KPI", typeof(Int32), "IIF(KPI_Answer_MGR>0,KPI_Answer_MGR,0)");


					int TotalItem = DT.Rows.Count;
					int AssCount =GL.CINT ( DT.Compute("COUNT(KPI_No)", "KPI_Answer_MGR>0"));

					if (TotalItem == 0) {
						cell_total.InnerHtml = "ไม่พบหัวข้อในแบบประเมิน";
					} else if (AssCount == 0) {
						cell_total.InnerHtml = "ยังไม่ได้ประเมิน";
					} else if (AssCount == TotalItem) {
						cell_total.InnerHtml = "ประเมินครบ " + TotalItem + " ข้อแล้ว";
					} else {
						cell_total.InnerHtml = "ประเมินแล้ว " + AssCount + " ข้อจากทั้งหมด " + TotalItem + " ข้อ";
					}

					for (int i = 1; i <= 5; i++) {
						HtmlTableCell cell_sum =(HtmlTableCell) e.Item.FindControl("cell_sum_" + i);
						int cnt = GL .CINT ( DT.Compute("COUNT(KPI_No)", "Final_KPI=" + i).ToString());
						if (cnt == 0) {
							cell_sum.InnerHtml = "-";
						} else {
							cell_sum.InnerHtml = "ได้ " + DT.Compute("COUNT(KPI_No)", "Final_KPI=" + i).ToString() + " ข้อ";
							cell_sum.Attributes["class"] = "AssLevel" + i;
						}
					}


					if (TotalItem != 0) {
						DT.Columns.Add("MARK", typeof(Int32), "Final_KPI");
						DT.Columns.Add("WEIGHT", typeof(double));

						for (int i = 0; i <= DT.Rows.Count - 1; i++) {
							if (!GL.IsEqualNull(DT.Rows[i]["KPI_Weight"]) && Information.IsNumeric(DT.Rows[i]["KPI_Weight"].ToString().Replace("%", ""))) {
								DT.Rows[i]["WEIGHT"] = DT.Rows[i]["KPI_Weight"].ToString().Replace("%", "");
							}
						}
						DT.Columns.Add("MARKxWEIGHT", typeof(double), "MARK*WEIGHT");
						DT.Columns.Add("WEIGHTx5", typeof(double), "WEIGHT*5");

						object _SUM = DT.Compute("SUM(MARKxWEIGHT)", "");
						object _TOTAL = DT.Compute("SUM(WEIGHTx5)", "");

						if (!GL.IsEqualNull(_SUM) & !GL.IsEqualNull(_TOTAL)) {
							double Result = Convert.ToDouble(_SUM) * 100 / Convert.ToDouble(_TOTAL);
							if (Strings.Len(Result) > 5)
								Result = GL.CDBL( GL.StringFormatNumber(Result));
							cell_sum_mark.InnerHtml = Result + "%";
							cell_sum_raw.InnerHtml = (Result * 5) + "/500";

							if (Result <= 20) {
								cell_sum_mark.Attributes["class"] = "AssLevel1";
								cell_sum_raw.Attributes["class"] = "AssLevel1";
							} else if (Result <= 40) {
								cell_sum_mark.Attributes["class"] = "AssLevel2";
								cell_sum_raw.Attributes["class"] = "AssLevel2";
							} else if (Result <= 60) {
								cell_sum_mark.Attributes["class"] = "AssLevel3";
								cell_sum_raw.Attributes["class"] = "AssLevel3";
							} else if (Result <= 80) {
								cell_sum_mark.Attributes["class"] = "AssLevel4";
								cell_sum_raw.Attributes["class"] = "AssLevel4";
							} else {
								cell_sum_mark.Attributes["class"] = "AssLevel5";
								cell_sum_raw.Attributes["class"] = "AssLevel5";
							}
						} else {
							cell_sum_mark.Attributes["class"] = "";
							cell_sum_mark.InnerHtml = "0%";
							cell_sum_raw.InnerHtml = "0/500";
						}

					}
					break;
			}


		}

		protected void btnBack_Click(object sender, System.EventArgs e)
		{
			BindAllAssessor();
            BindAssessor_rptKPI();
			BindPersonalList();
			pnlList.Visible = true;
			pnlEdit.Visible = false;
		}


		protected void btnAdd_Click(object sender, System.EventArgs e)
		{
			ClearEditForm();

			lblKPIRound.Text = ddlRound.Text;
			lblKPINo.Text = BL.GetNewKPINo(R_Year, R_Round, PSNL_NO).ToString();
			//.ToString().PadLeft(2, GL.chr0)
			pnlEdit.Visible = false;
			divEdit.Visible = true;

		}

		private void ClearEditForm()
		{
			lblKPIRound.Text = "";
			lblKPINo.Text = "";
			txt_KPI_Job.Text = "";
			txt_KPI_Target.Text = "";
			txt_KPI_Choice1.Text = "";
			txt_KPI_Choice2.Text = "";
			txt_KPI_Choice3.Text = "";
			txt_KPI_Choice4.Text = "";
			txt_KPI_Choice5.Text = "";
			txt_KPI_Weight.Text = "";
			TC.ImplementJavaFloatText(txt_KPI_Weight);
			txt_KPI_Weight.Style["text-align"] = "center";
		}

		protected void btnClose_Click(object sender, System.EventArgs e)
		{
			BindMasterKPI();
			pnlEdit.Visible = true;
			divEdit.Visible = false;
		}


		protected void btnOK_Click(object sender, System.EventArgs e)
		{
			//------------------ Save Detail-----------------
			HRBL.DataManager DM = BL.GetKPIDetail(R_Year, R_Round, PSNL_NO);
			DataTable DT = DM.Table;
			SqlDataAdapter DA = DM.Adaptor;
			int KPI_No = Convert.ToInt32(Conversion.Val(lblKPINo.Text));
			DM.Table.DefaultView.RowFilter = "KPI_No=" + KPI_No;

			DataRow DR = null;
			if (DT.DefaultView.Count == 0) {
				DR = DT.NewRow();
				DR["PSNL_NO"] = PSNL_NO;
				DR["R_Year"] = R_Year;
				DR["R_Round"] = R_Round;
				DR["KPI_No"] = KPI_No;
			} else {
				DR = DT.DefaultView[0].Row;
			}

			DR["KPI_Job"] = txt_KPI_Job.Text;
			DR["KPI_Target_Text"] = txt_KPI_Target.Text;
			DR["KPI_Choice_1"] = txt_KPI_Choice1.Text;
			DR["KPI_Choice_2"] = txt_KPI_Choice2.Text;
			DR["KPI_Choice_3"] = txt_KPI_Choice3.Text;
			DR["KPI_Choice_4"] = txt_KPI_Choice4.Text;
			DR["KPI_Choice_5"] = txt_KPI_Choice5.Text;

			if (Information.IsNumeric(txt_KPI_Weight.Text)) {
				DR["KPI_Weight"] = Convert.ToDouble(txt_KPI_Weight.Text);
			} else {
				DR["KPI_Weight"] = DBNull.Value;
			}

			if (DT.DefaultView.Count == 0)
				DT.Rows.Add(DR);
			SqlCommandBuilder cmd = new SqlCommandBuilder(DA);
			DA.Update(DT);

			BindMasterKPI();
			divEdit.Visible = false;
			pnlEdit.Visible = true;

		}

		protected void ddlKPIStatus_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			SaveHeader();
			ScriptManager.RegisterStartupScript(this.Page, typeof(string), "Alert", "showAlert('ปรับสถานะการประเมินแล้ว');", true);
		}


		private void SaveHeader()
		{
			//------------- Check Round ---------------
			if (R_Year == 0 | R_Round == 0)
				return;
			//------------- Check Progress-------------

			string SQL = "";
			SQL = " SELECT * \n";
			SQL += " FROM tb_HR_KPI_Header\n";
			SQL += " WHERE PSNL_NO='" + PSNL_NO.Replace("'", "''") + "' AND R_Year=" + R_Year + " AND R_Round=" + R_Round;
			SqlDataAdapter DA = new SqlDataAdapter(SQL, BL.ConnectionString());
			DataTable DT = new DataTable();
			DA.Fill(DT);

			DataRow DR = null;
			if (DT.Rows.Count == 0) {
				DR = DT.NewRow();
				DR["PSNL_NO"] = PSNL_NO;
				DR["R_Year"] = R_Year;
				DR["R_Round"] = R_Round;
				DR["Create_By"] = Session["USER_PSNL_NO"];
				DR["Create_Time"] = DateAndTime.Now;
			} else {
				DR = DT.Rows[0];
			}
			if (GL.CINT (KPI_Status) == -1) {
				DR["KPI_Status"] = 0;
			} else {
				DR["KPI_Status"] = KPI_Status;
			}
			//------------- Round Detail------------- 
			SQL = "SELECT * FROM tb_HR_Round ";
			SQL += " WHERE R_Year=" + R_Year + " AND R_Round=" + R_Round;
			DataTable PN = new DataTable();
			SqlDataAdapter PA = new SqlDataAdapter(SQL, BL.ConnectionString());
			PA.Fill(PN);
			DR["R_Start"] = PN.Rows[0]["R_Start"];
			DR["R_End"] = PN.Rows[0]["R_End"];
			DR["R_Remark"] = "";


			//============ป้องกันการ update ตำแหน่ง ณ สิ้นรอบประเมินและปิดรอบประเมิน======

            Boolean IsInPeriod = BL.IsTimeInPeriod(DateTime.Now, R_Year, R_Round);
            Boolean IsRoundCompleted = BL.Is_Round_Completed(R_Year, R_Round);
            Boolean ckUpdate =  IsInPeriod & !IsRoundCompleted;
            if (ckUpdate | DT.Rows.Count == 0 )
            {
				//------------- Personal Detail---------
				HRBL.PersonalInfo PSNInfo = BL.GetAssessmentPersonalInfo(R_Year, R_Round, PSNL_NO, KPI_Status);
				// Replace With New Updated Data If Exists
				if (!string.IsNullOrEmpty(PSNInfo.PSNL_No)) {
					if (!string.IsNullOrEmpty(PSNInfo.PSNL_No))
						DR["PSNL_No"] = PSNInfo.PSNL_No;
					else
						DR["PSNL_No"] = DBNull.Value;
					if (!string.IsNullOrEmpty(PSNInfo.PSNL_Fullname))
						DR["PSNL_Fullname"] = PSNInfo.PSNL_Fullname;
					else
						DR["PSNL_Fullname"] = DBNull.Value;
					if (!string.IsNullOrEmpty(PSNInfo.PNPS_CLASS))
						DR["PNPS_CLASS"] = PSNInfo.PNPS_CLASS;
					else
						DR["PNPS_CLASS"] = DBNull.Value;
					if (!string.IsNullOrEmpty(PSNInfo.PSNL_TYPE))
						DR["PSNL_TYPE"] = PSNInfo.PSNL_TYPE;
					else
						DR["PSNL_TYPE"] = DBNull.Value;
					if (!string.IsNullOrEmpty(PSNInfo.POS_NO))
						DR["POS_NO"] = PSNInfo.POS_NO;
					else
						DR["POS_NO"] = DBNull.Value;
					if (!string.IsNullOrEmpty(PSNInfo.WAGE_TYPE))
						DR["WAGE_TYPE"] = PSNInfo.WAGE_TYPE;
					else
						DR["WAGE_TYPE"] = DBNull.Value;
					if (!string.IsNullOrEmpty(PSNInfo.WAGE_NAME))
						DR["WAGE_NAME"] = PSNInfo.WAGE_NAME;
					else
						DR["WAGE_NAME"] = DBNull.Value;
					if (!string.IsNullOrEmpty(PSNInfo.SECTOR_CODE))
						DR["SECTOR_CODE"] = Strings.Left(PSNInfo.SECTOR_CODE, 2);
					else
						DR["SECTOR_CODE"] = DBNull.Value;
					if (!string.IsNullOrEmpty(PSNInfo.DEPT_CODE))
						DR["DEPT_CODE"] = PSNInfo.DEPT_CODE;
					else
						DR["DEPT_CODE"] = DBNull.Value;
					if (!string.IsNullOrEmpty(PSNInfo.MINOR_CODE))
						DR["MINOR_CODE"] = PSNInfo.MINOR_CODE;
					else
						DR["MINOR_CODE"] = DBNull.Value;
					if (!string.IsNullOrEmpty(PSNInfo.SECTOR_NAME))
						DR["SECTOR_NAME"] = PSNInfo.SECTOR_NAME;
					else
						DR["SECTOR_NAME"] = DBNull.Value;
					if (!string.IsNullOrEmpty(PSNInfo.DEPT_NAME))
						DR["DEPT_NAME"] = PSNInfo.DEPT_NAME;
					else
						DR["DEPT_NAME"] = DBNull.Value;
					if (!string.IsNullOrEmpty(PSNInfo.FN_ID))
						DR["FN_ID"] = PSNInfo.FN_ID;
					else
						DR["FN_ID"] = DBNull.Value;
					if (!string.IsNullOrEmpty(PSNInfo.FLD_Name))
						DR["FLD_Name"] = PSNInfo.FLD_Name;
					else
						DR["FLD_Name"] = DBNull.Value;
					if (!string.IsNullOrEmpty(PSNInfo.FN_CODE))
						DR["FN_CODE"] = PSNInfo.FN_CODE;
					else
						DR["FN_CODE"] = DBNull.Value;
					if (!string.IsNullOrEmpty(PSNInfo.FN_NAME))
						DR["FN_NAME"] = PSNInfo.FN_NAME;
					else
						DR["FN_NAME"] = DBNull.Value;
					if (!string.IsNullOrEmpty(PSNInfo.MGR_CODE))
						DR["MGR_CODE"] = PSNInfo.MGR_CODE;
					else
						DR["MGR_CODE"] = DBNull.Value;
					if (!string.IsNullOrEmpty(PSNInfo.MGR_NAME))
						DR["MGR_NAME"] = PSNInfo.MGR_NAME;
					else
						DR["MGR_NAME"] = DBNull.Value;
				}

            //------------- Assign Assessor Only-----------------------------
			DR["Create_Commit_By"] = ASSESSOR_BY;
			DR["Create_Commit_Name"] = ASSESSOR_NAME;
			DR["Create_Commit_DEPT"] = ASSESSOR_DEPT;
			DR["Create_Commit_POS"] = ASSESSOR_POS;
			DR["Assessment_Commit_By_MGR"] = ASSESSOR_BY;
			DR["Assessment_Commit_Name"] = ASSESSOR_NAME;
			DR["Assessment_Commit_DEPT"] = ASSESSOR_DEPT;
			DR["Assessment_Commit_POS"] = ASSESSOR_POS;

			}
            else {}
			DR["Update_By"] = Session["USER_PSNL_NO"];
			DR["Update_Time"] = DateAndTime.Now;
			

			if (DT.Rows.Count == 0)
				DT.Rows.Add(DR);
			SqlCommandBuilder cmd = new SqlCommandBuilder(DA);
			DA.Update(DT);

			//----------- Prevent Save----------
			if (BL.Is_Round_Completed(R_Year, R_Round)) {
				return;
			}

			DataTable TMP = BL.GetAssessorList(R_Year, R_Round, PSNL_NO);
			TMP.DefaultView.RowFilter = "MGR_PSNL_NO='" + ASSESSOR_BY.Replace("'", "''") + "'";

			//----------------- Update Information For History Assessor Structure ------------------
			SQL = "SELECT * ";
			SQL += " FROM tb_HR_Actual_Assessor ";
			SQL += " WHERE PSNL_NO='" + PSNL_NO + "' AND R_Year=" + R_Year + " AND R_Round=" + R_Round;
			DA = new SqlDataAdapter(SQL, BL.ConnectionString());
			DT = new DataTable();
			DA.Fill(DT);

			if (DT.Rows.Count > 0) {
				DT.Rows[0].Delete();
				cmd = new SqlCommandBuilder(DA);
				DA.Update(DT);
			}

			if (TMP.DefaultView.Count > 0) {
				DR = DT.NewRow();
				DR["PSNL_NO"] = PSNL_NO;
				DR["R_Year"] = R_Year;
				DR["R_Round"] = R_Round;
				DR["MGR_ASSESSOR_POS"] = TMP.DefaultView[0]["ASSESSOR_POS"];
				DR["MGR_PSNL_NO"] = TMP.DefaultView[0]["MGR_PSNL_NO"];
				DR["MGR_PSNL_Fullname"] = TMP.DefaultView[0]["MGR_PSNL_Fullname"];
				DR["MGR_PNPS_CLASS"] = TMP.DefaultView[0]["MGR_CLASS"];
				DR["MGR_PSNL_TYPE"] = TMP.DefaultView[0]["MGR_WAGE_TYPE"];
				DR["MGR_POS_NO"] = TMP.DefaultView[0]["MGR_POS_NO"];
				DR["MGR_WAGE_TYPE"] = TMP.DefaultView[0]["MGR_WAGE_TYPE"];
				DR["MGR_WAGE_NAME"] = TMP.DefaultView[0]["MGR_WAGE_NAME"];
				DR["MGR_DEPT_CODE"] = TMP.DefaultView[0]["MGR_DEPT_CODE"];
				//DR("MGR_MINOR_CODE") = TMP.DefaultView(0).Item("MGR_MINOR_CODE")
				DR["MGR_SECTOR_CODE"] = Strings.Left(TMP.DefaultView[0]["MGR_SECTOR_CODE"].ToString (), 2);
				DR["MGR_SECTOR_NAME"] = TMP.DefaultView[0]["MGR_SECTOR_NAME"];
				DR["MGR_DEPT_NAME"] = TMP.DefaultView[0]["MGR_DEPT_NAME"];
				//DR("MGR_FN_ID") = TMP.DefaultView(0).Item("MGR_FN_ID")
				//DR("MGR_FLD_Name") = TMP.DefaultView(0).Item("MGR_FLD_Name")
				DR["MGR_FN_CODE"] = TMP.DefaultView[0]["MGR_FN_CODE"];
				//DR("MGR_FN_TYPE") = TMP.DefaultView(0).Item("MGR_FN_TYPE")
				DR["MGR_FN_NAME"] = TMP.DefaultView[0]["MGR_FN_NAME"];
				if (!GL.IsEqualNull(TMP.DefaultView[0]["MGR_MGR_CODE"])) {
					DR["MGR_MGR_CODE"] = TMP.DefaultView[0]["MGR_MGR_CODE"];
				}
				if (!GL.IsEqualNull(TMP.DefaultView[0]["MGR_MGR_NAME"])) {
					DR["MGR_MGR_NAME"] = TMP.DefaultView[0]["MGR_MGR_NAME"];
				}
				try {
					 DR["MGR_PNPS_RETIRE_DATE"] = (BL.GetPSNRetireDate(TMP.DefaultView[0]["MGR_PSNL_NO"].ToString()));
				} catch {
				}
				DR["Update_By"] = Session["USER_PSNL_NO"];
				DR["Update_Time"] = DateAndTime.Now;
				DT.Rows.Add(DR);
				cmd = new SqlCommandBuilder(DA);
				DA.Update(DT);
			}

			//----------------- Update Information For History PSN Structure ------------------
			DataTable PSN = new DataTable();
			SQL = "SELECT * FROM vw_PN_PSNL WHERE PSNL_NO='" + PSNL_NO.Replace("'", "''") + "'";
			DA = new SqlDataAdapter(SQL, BL.ConnectionString());
			DA.Fill(PSN);

			SQL = "SELECT * ";
			SQL += " FROM tb_HR_Assessment_PSN ";
			SQL += " WHERE PSNL_NO='" + PSNL_NO.Replace("'", "''") + "' AND R_Year=" + R_Year + " AND R_Round=" + R_Round;
			DA = new SqlDataAdapter(SQL, BL.ConnectionString());
			DT = new DataTable();
			DA.Fill(DT);


			if (PSN.Rows.Count > 0) {
				if (DT.Rows.Count == 0) {
					DR = DT.NewRow();
					DR["PSNL_NO"] = PSNL_NO;
					DR["R_Year"] = R_Year;
					DR["R_Round"] = R_Round;
					if (TMP.DefaultView.Count > 0) {
						try {
							DR["PSN_ASSESSOR_POS"] = TMP.DefaultView[0]["ASSESSOR_POS"];
						} catch {
						}
					}
					DR["PSN_PSNL_NO"] = PSNL_NO;
					DR["PSN_PSNL_Fullname"] = PSN.Rows[0]["PSNL_Fullname"].ToString();
					DT.Rows.Add(DR);
				} else {
					DR = DT.Rows[0];
				}

                //============ป้องกันการ update ตำแหน่ง ณ สิ้นรอบประเมินและปิดรอบประเมิน======
                if (ckUpdate | DT.Rows.Count == 0 | string.IsNullOrEmpty(DT.Rows[0]["PSN_PNPS_CLASS"].ToString()))
                {
                    DR["PSN_PNPS_CLASS"] = PSN.Rows[0]["PNPS_CLASS"].ToString();
                    DR["PSN_PSNL_TYPE"] = PSN.Rows[0]["PSNL_TYPE"].ToString();
                    DR["PSN_POS_NO"] = PSN.Rows[0]["POS_NO"].ToString();
                    DR["PSN_WAGE_TYPE"] = PSN.Rows[0]["WAGE_TYPE"].ToString();
                    DR["PSN_WAGE_NAME"] = PSN.Rows[0]["WAGE_NAME"].ToString();
                    DR["PSN_DEPT_CODE"] = PSN.Rows[0]["DEPT_CODE"].ToString();
                    DR["PSN_MINOR_CODE"] = PSN.Rows[0]["MINOR_CODE"].ToString();
                    DR["PSN_SECTOR_CODE"] = Strings.Left(PSN.Rows[0]["SECTOR_CODE"].ToString(), 2);
                    DR["PSN_SECTOR_NAME"] = PSN.Rows[0]["SECTOR_NAME"].ToString();
                    DR["PSN_DEPT_NAME"] = PSN.Rows[0]["DEPT_NAME"].ToString();
                    DR["PSN_FN_ID"] = PSN.Rows[0]["FN_ID"].ToString();
                    DR["PSN_FLD_Name"] = PSN.Rows[0]["FLD_Name"].ToString();
                    DR["PSN_FN_CODE"] = PSN.Rows[0]["FN_CODE"].ToString();
                    DR["PSN_FN_TYPE"] = PSN.Rows[0]["FN_TYPE"].ToString();
                    DR["PSN_FN_NAME"] = PSN.Rows[0]["FN_NAME"].ToString();
                    if (!GL.IsEqualNull(PSN.Rows[0]["MGR_CODE"]))
                    {
                        DR["PSN_MGR_CODE"] = PSN.Rows[0]["MGR_CODE"];
                    }
                    if (!GL.IsEqualNull(PSN.Rows[0]["MGR_NAME"]))
                    {
                        DR["PSN_MGR_NAME"] = PSN.Rows[0]["MGR_NAME"];
                    }
                }
                else { }
				try {
					DR["PSN_PNPS_RETIRE_DATE"] = BL.GetPSNRetireDate(PSNL_NO);
				} catch {
				}
				DR["Update_By"] = Session["USER_PSNL_NO"];
				DR["Update_Time"] = DateAndTime.Now;
				cmd = new SqlCommandBuilder(DA);
				DA.Update(DT);

			}

		}

		protected void btn_Fixed_Assessor_Click(object sender, System.EventArgs e)
		{
			BindFixedAssessor();
			pnlAddAss.Visible = true;
		}

		private void BindFixedAssessor()
		{
			string SQL = "SELECT *,ISNULL(MGR_NAME,FN_Name) POS_NAME FROM vw_PN_PSNL_ALL \n";

			string Filter = "";
			if (!string.IsNullOrEmpty(txt_Search_ASS_POS.Text)) {
				Filter += " (\n";
				Filter += " DEPT_NAME LIKE '%" + txt_Search_ASS_POS.Text.Replace("'", "''") + "%' OR \n";
				Filter += " DEPT_CODE LIKE '%" + txt_Search_ASS_POS.Text.Replace("'", "''") + "%' OR\n";
				Filter += " POS_NO LIKE '%" + txt_Search_ASS_POS.Text.Replace("'", "''") + "%' OR\n";
				Filter += " ISNULL(MGR_NAME,FN_NAME) LIKE '%" + txt_Search_ASS_POS.Text.Replace("'", "''") + "%'\n";
				Filter += " ) AND ";
			}
			if (!string.IsNullOrEmpty(TextBox2.Text)) {
				Filter += " (\n";
				Filter += " PSNL_Fullname LIKE '%" + TextBox2.Text.Replace("'", "''") + "%' OR \n";
				Filter += " PSNL_NO LIKE '%" + TextBox2.Text.Replace("'", "''") + "%'\n";
				Filter += " ) AND ";
			}

			if (!string.IsNullOrEmpty(Filter)) {
				SQL += " WHERE " + Filter.Substring(0, Filter.Length - 4) + "\n";
			}

			SQL += " ORDER BY DEPT_CODE,PNPS_CLASS DESC,POS_NO,PSNL_Fullname\n";
			SqlDataAdapter DA = new SqlDataAdapter(SQL, BL.ConnectionString());
			DataTable DT = new DataTable();
			DA.Fill(DT);

			Session["Setting_KPI_Search_Assessor"] = DT;
			PagerFixedAssessor.SesssionSourceName = "Setting_KPI_Search_Assessor";
			PagerFixedAssessor.RenderLayout();

			if (DT.Rows.Count == 0) {
				lblCountFixedAssessor.Text = "ไม่พบรายการดังกล่าว";
			} else {
				lblCountFixedAssessor.Text = "พบ " + GL.StringFormatNumber(DT.Rows.Count, 0) + " รายการ";
			}
		}

		protected void PagerFixedAssessor_PageChanging(PageNavigation Sender)
		{
			PagerFixedAssessor.TheRepeater = rptFixedAssessor;
		}

		protected void btnCloseFixedAss_Click(object sender, System.EventArgs e)
		{
			pnlAddAss.Visible = false;
		}


		string LastOrgranizeName = "";

		protected void rptFixedAssessor_ItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
		{
			if (e.Item.ItemType != ListItemType.Item & e.Item.ItemType != ListItemType.AlternatingItem)
				return;

			 Label lblPSNPos = (Label)e.Item.FindControl("lblPSNPos");
			 Label lblPSNClass = (Label)e.Item.FindControl("lblPSNClass");
			Label lblPSNDept =(Label) e.Item.FindControl("lblPSNDept");
			Label lblPSNName = (Label)e.Item.FindControl("lblPSNName");
			AjaxControlToolkit.ConfirmButtonExtender cfmbtnSelect =(AjaxControlToolkit.ConfirmButtonExtender) e.Item.FindControl("cfmbtnSelect");
			Button btnSelect =(Button) e.Item.FindControl("btnSelect");

            DataRowView drv = (DataRowView)e.Item.DataItem;
			if ( (drv["DEPT_NAME"]).ToString() != LastOrgranizeName) {
				lblPSNDept.Text = drv["DEPT_NAME"].ToString ();
				LastOrgranizeName = drv["DEPT_NAME"].ToString ();
			}

			lblPSNName.Text = drv["PSNL_NO"] + " : " + drv["PSNL_Fullname"];
			lblPSNPos.Text = drv["POS_Name"].ToString ();
			lblPSNClass.Text = GL.CINT (drv["PNPS_CLASS"]).ToString ();
			btnSelect.CommandArgument = drv["PSNL_NO"].ToString ();

			cfmbtnSelect.ConfirmText = "ยืนยันระบุ " + drv["PSNL_Fullname"] + " เป็นผู้ประเมิน?";
		}

		protected void rptFixedAssessor_ItemCommand(object source, System.Web.UI.WebControls.RepeaterCommandEventArgs e)
		{
			switch (e.CommandName) {
				case "Select":
					string SQL = "SELECT * FROM vw_PN_PSNL_ALL WHERE PSNL_NO='" + e.CommandArgument.ToString().Replace("'", "''") + "'";
					SqlDataAdapter DA = new SqlDataAdapter(SQL, BL.ConnectionString());
					DataTable PSN = new DataTable();
					DA.Fill(PSN);

					if (PSN.Rows.Count == 0) {
						ScriptManager.RegisterStartupScript(this.Page, typeof(string), "Alert", "alert('ไม่พบรายการที่เลือก\\n\\nกรุณาตรวจสอบข้อมูลอีกครั้ง');", true);
						return;
					}

					SQL = "SELECT * FROM tb_HR_Actual_Assessor \n";
					SQL += " WHERE R_Year=" + R_Year + " AND R_Round=" + R_Round + " AND PSNL_NO='" + PSNL_NO.Replace("'", "''") + "'";
					DA = new SqlDataAdapter(SQL, BL.ConnectionString());
					DataTable DT = new DataTable();
					DA.Fill(DT);

					DataRow DR = null;
					if (DT.Rows.Count == 0) {
						DR = DT.NewRow();
						DR["PSNL_NO"] = PSNL_NO;
						DR["R_Year"] = R_Year;
						DR["R_Round"] = R_Round;
						DT.Rows.Add(DR);
					} else {
						DR = DT.Rows[0];
					}

					DR["MGR_ASSESSOR_POS"] = PSN.Rows[0]["POS_NO"].ToString();
					//--------------- นี่คือตัว Mapping -------------

					DR["MGR_PSNL_NO"] = PSN.Rows[0]["PSNL_NO"].ToString();
					DR["MGR_PSNL_Fullname"] = PSN.Rows[0]["PSNL_Fullname"].ToString();
					DR["MGR_PNPS_CLASS"] = PSN.Rows[0]["PNPS_CLASS"].ToString();
					DR["MGR_PSNL_TYPE"] = PSN.Rows[0]["PSNL_TYPE"].ToString();
					DR["MGR_POS_NO"] = PSN.Rows[0]["POS_NO"].ToString();
					DR["MGR_WAGE_TYPE"] = PSN.Rows[0]["WAGE_TYPE"].ToString();
					DR["MGR_WAGE_NAME"] = PSN.Rows[0]["WAGE_NAME"].ToString();
					DR["MGR_DEPT_CODE"] = PSN.Rows[0]["DEPT_CODE"].ToString();
					DR["MGR_MINOR_CODE"] = PSN.Rows[0]["MINOR_CODE"].ToString();
					DR["MGR_SECTOR_CODE"] = PSN.Rows[0]["SECTOR_CODE"].ToString();
					DR["MGR_SECTOR_NAME"] = PSN.Rows[0]["SECTOR_NAME"].ToString();
					DR["MGR_DEPT_NAME"] = PSN.Rows[0]["DEPT_NAME"].ToString();
					if (!GL.IsEqualNull(PSN.Rows[0]["FN_ID"])) {
						DR["MGR_FN_ID"] = PSN.Rows[0]["FN_ID"];
					} else {
						DR["MGR_FN_ID"] = DBNull.Value;
					}
					if (!GL.IsEqualNull(PSN.Rows[0]["FLD_Name"])) {
						DR["MGR_FLD_Name"] = PSN.Rows[0]["FLD_Name"];
					} else {
						DR["MGR_FLD_Name"] = DBNull.Value;
					}
					if (!GL.IsEqualNull(PSN.Rows[0]["FN_CODE"])) {
						DR["MGR_FN_CODE"] = PSN.Rows[0]["FN_CODE"];
					} else {
						DR["MGR_FN_CODE"] = DBNull.Value;
					}
					if (!GL.IsEqualNull(PSN.Rows[0]["FN_TYPE"])) {
						DR["MGR_FN_TYPE"] = PSN.Rows[0]["FN_TYPE"];
					} else {
						DR["MGR_FN_TYPE"] = DBNull.Value;
					}
					if (!GL.IsEqualNull(PSN.Rows[0]["FN_NAME"])) {
						DR["MGR_FN_NAME"] = PSN.Rows[0]["FN_NAME"];
					} else {
						DR["MGR_FN_NAME"] = DBNull.Value;
					}
					if (!GL.IsEqualNull(PSN.Rows[0]["MGR_CODE"])) {
						DR["MGR_MGR_CODE"] = PSN.Rows[0]["MGR_CODE"];
					} else {
						DR["MGR_MGR_CODE"] = DBNull.Value;
					}
					if (!GL.IsEqualNull(PSN.Rows[0]["MGR_NAME"])) {
						DR["MGR_MGR_NAME"] = PSN.Rows[0]["MGR_NAME"];
					} else {
						DR["MGR_MGR_NAME"] = DBNull.Value;
					}
					if (!GL.IsEqualNull(PSN.Rows[0]["PNPS_RETIRE_DATE"])) {
						DR["MGR_PNPS_RETIRE_DATE"] = PSN.Rows[0]["PNPS_RETIRE_DATE"];
					} else {
						DR["MGR_PNPS_RETIRE_DATE"] = DBNull.Value;
					}

					DR["Update_By"] = Session["USER_PSNL_NO"];
					DR["Update_Time"] = DateAndTime.Now;

					SqlCommandBuilder cmd = new SqlCommandBuilder(DA);
					DA.Update(DT);

					pnlAddAss.Visible = false;
					BindAssessor();

					break;
			}
		}
		public AssessmentSetting_KPI_PSN()
		{
			Load += Page_Load;
		}

		//Protected Sub btn_Search_Fixed_Ass_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_Search_Fixed_Ass.Click
		//    BindFixedAssessor()
		//End Sub
	}
}

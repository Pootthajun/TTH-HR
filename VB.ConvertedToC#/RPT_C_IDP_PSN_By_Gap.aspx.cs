﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using System.Web.UI.HtmlControls;

namespace VB
{
    public partial class RPT_C_IDP_PSN_By_Gap : System.Web.UI.Page
    {

        HRBL BL = new HRBL();
        GenericLib GL = new GenericLib();

        public int R_Year
        {
            get
            {
                try
                {
                    return GL.CINT(GL.SplitString(ddlRound.Items[ddlRound.SelectedIndex].Value, "-")[0]);
                }
                catch (Exception ex)
                {
                    return 0;
                }
            }
        }

        public int R_Round
        {
            get
            {
                try
                {
                    return GL.CINT(GL.SplitString(ddlRound.Items[ddlRound.SelectedIndex].Value, "-")[1]);
                }
                catch (Exception ex)
                {
                    return 0;
                }
            }
        }

        protected void Page_Load(object sender, System.EventArgs e)
        {
            if ((Session["USER_PSNL_NO"] == null))
            {
                ScriptManager.RegisterStartupScript(this.Page, typeof(string), "Alert", "showAlert('กรุณาลงชื่อเข้าใช้ระบบ');", true);
                Response.Redirect("Login.aspx");
                return;
            }

            if (!IsPostBack)
            {
                BL.BindDDlYearRound(ddlRound);
                ddlRound.Items.RemoveAt(0);
                BL.BindDDlPSNType(ddlPSNType);
                BL.BindDDlFLDCode(ddlFN);
                BL.BindDDlClassAll(ddlClassGroupFrom);
                BL.BindDDlClassAll(ddlClassGroupTo);

                BindPersonalList();
             }
        }

        private void BindPersonalList()
        {

            if (!chkCOMP1.Checked & !chkCOMP2.Checked & !chkCOMP3.Checked)
            {
                rptIDP.DataSource = null;
                rptIDP.DataBind();
                return;
            }

            string StartTitle = " ปี " + R_Year + " รอบ " + R_Round;
            string Title = "";

            //string Filter = "R_Year=" + R_Year + " AND R_Round=" + R_Round + " AND Selected=1 AND \n";
            //if (txtDept.Text != "")
            //{
            //    Filter += " (DEPT_CODE LIKE '%" + txtDept.Text.Replace("'", "''") + "%' OR DEPT_NAME LIKE '%" + txtDept.Text.Replace("'", "''") + "%')  AND \n";
            //}
            //if (ddlPSNType.SelectedIndex > 0)
            //{
            //    Filter += " WAGE_TYPE='" + ddlPSNType.SelectedValue.Replace("'","''") + "' AND \n";
            //    Title += " พนักงาน" + ddlPSNType.SelectedItem;
            //}
            //if (ddlClassGroupFrom.SelectedIndex > 0)
            //{
            //    Filter += "PNPS_CLASS>='" + ddlClassGroupFrom.SelectedValue.PadLeft(2, '0') + "' AND \n";
            //}
            //if ( ddlClassGroupTo.SelectedIndex > 0)
            //{
            //    Filter += "PNPS_CLASS<='" + ddlClassGroupTo.SelectedValue.PadLeft(2, '0') + "' AND \n";
            //}

            //if ((ddlClassGroupFrom.SelectedIndex > 0) && (ddlClassGroupTo.SelectedIndex > 0))
            //{
            //    Title += " ระดับ " + ddlClassGroupFrom.Items[ddlClassGroupFrom.SelectedIndex].Text + " ถึง " + ddlClassGroupTo.Items[ddlClassGroupTo.SelectedIndex].Text;
            //}


            //if (ddlFN.SelectedIndex > 0)
            //{
            //    Filter += " FN_ID='" + ddlFN.SelectedValue.Replace("'","''") + "' AND \n";
            //}
            //if (txtCOMP_COMP.Text != "")
            //{
            //    Filter += " GAP_Name LIKE '%" + txtCOMP_COMP.Text.Replace("'", "''") + "%' AND \n";
            //}

            //if ((chkGAP0.Checked & chkGAP1.Checked & chkGAP2.Checked & chkGAP3.Checked & chkGAP4.Checked) | (!chkGAP0.Checked & !chkGAP1.Checked & !chkGAP2.Checked & !chkGAP3.Checked & !chkGAP4.Checked))
            //{
            //    //-----------Do Nothing--------------
            //}
            //else
            //{
            //    string sel = "";
            //    string t = "";
            //    if (chkGAP0.Checked)
            //    {
            //        sel += "5,";
            //        t += "0,";
            //    }
            //    if (chkGAP1.Checked)
            //    {
            //        sel += "4,";
            //        t += "-1,";
            //    }
            //    if (chkGAP2.Checked)
            //    {
            //        sel += "3,";
            //        t += "-2,";
            //    }
            //    if (chkGAP3.Checked)
            //    {
            //        sel += "2,";
            //        t += "-3,";
            //    }
            //    if (chkGAP4.Checked)
            //    {
            //        sel += "1,";
            //        t += "-4,";
            //    }
            //    Filter += " Final_Score IN (" + sel.Substring(0, sel.Length - 1) + ") AND \n";

            //     if (chkGAP1.Checked & chkGAP2.Checked & chkGAP3.Checked & chkGAP4.Checked)
            //    {
            //        Title += " ที่มีช่องว่างสมรรถนะ";
            //    }
            //    else if (chkGAP0.Checked & !chkGAP1.Checked & !chkGAP2.Checked & !chkGAP3.Checked & !chkGAP4.Checked)
            //    {
            //        Title += " ที่ไม่มีช่องว่างสมรรถนะ";
            //    }
            //    else
            //    {
            //        Title += " ที่มีช่องว่าง " + t.Substring(0, t.Length - 1);
            //    }          
            //}



            string template="SELECT \n";
            template += "R_Year, R_Round, Selected ,GAP_ID,GAP_Name \n";
                    template += ",PSNL_NO,PSNL_Fullname \n";
                    template += ",POS_NO,POS_Name \n";
                    template += ",FN_ID,FN_Name \n";
                    template += ",DEPT_CODE,DEPT_NAME \n";
                    template += ",PNPS_CLASS \n";
                    template += ",WAGE_TYPE,WAGE_NAME \n";
                    template += ",COMP_Type_Id,COMP_Comp,Final_Score \n";
            string SQL = "SELECT * FROM \n(\n";

            //if (Filter != "") Filter = " WHERE " + Filter.Substring(0, Filter.Length - 5) + "\n";

            //String TitleCOMP = " ";
            //if (chkCOMP1.Checked)
            //{
            //    SQL += template + " FROM vw_COMP_CORE_GAP \n" + Filter + "\nUNION ALL\n";
            //    TitleCOMP += "สมรรถนะหลัก(Core),";
            //}
            //if (chkCOMP3.Checked)
            //{
            //    SQL += template + " FROM vw_COMP_MGR_GAP \n" + Filter + "\nUNION ALL\n";
            //    TitleCOMP += "สมรรถนะตามสายระดับ/การจัดการทั่วไป(Managerial),";
            //}
            //if (chkCOMP2.Checked)
            //{
            //    SQL += template + " FROM vw_COMP_FN_GAP \n" + Filter + "\nUNION ALL\n";
            //    TitleCOMP += "สมรรถนะตามสายงาน(Functional),";
            //}





            string Filter = "R_Year=" + R_Year + " AND R_Round=" + R_Round + " AND Selected=1 AND   ";
            if (txtDept.Text != "")
            {
                Filter += " (DEPT_CODE LIKE '%" + txtDept.Text.Replace("'", "''") + "%' OR DEPT_NAME LIKE '%" + txtDept.Text.Replace("'", "''") + "%')  AND   ";
            }
            if (ddlPSNType.SelectedIndex > 0)
            {
                Filter += " WAGE_TYPE='" + ddlPSNType.SelectedValue.Replace("'", "''") + "' AND   ";
                Title += " พนักงาน" + ddlPSNType.SelectedItem;
            }
            if (ddlClassGroupFrom.SelectedIndex > 0)
            {
                Filter += "PNPS_CLASS>='" + ddlClassGroupFrom.SelectedValue.PadLeft(2, '0') + "' AND   ";
            }
            if (ddlClassGroupTo.SelectedIndex > 0)
            {
                Filter += "PNPS_CLASS<='" + ddlClassGroupTo.SelectedValue.PadLeft(2, '0') + "' AND   ";
            }

            if ((ddlClassGroupFrom.SelectedIndex > 0) && (ddlClassGroupTo.SelectedIndex > 0))
            {
                Title += " ระดับ " + ddlClassGroupFrom.Items[ddlClassGroupFrom.SelectedIndex].Text + " ถึง " + ddlClassGroupTo.Items[ddlClassGroupTo.SelectedIndex].Text;
            }


            if (ddlFN.SelectedIndex > 0)
            {

                Filter += " FN_ID='" + GL.SplitString(ddlFN.SelectedValue.Replace("'", "''"), "-")[0].Trim() + "' AND   ";
                //Filter += " FN_ID='" + ddlFN.SelectedValue.Replace("'", "''") + "' AND   ";
            }
            if (txtCOMP_COMP.Text != "")
            {
                Filter += " GAP_Name LIKE '%" + txtCOMP_COMP.Text.Replace("'", "''") + "%' AND   ";
            }

            if ((chkGAP0.Checked & chkGAP1.Checked & chkGAP2.Checked & chkGAP3.Checked & chkGAP4.Checked) | (!chkGAP0.Checked & !chkGAP1.Checked & !chkGAP2.Checked & !chkGAP3.Checked & !chkGAP4.Checked))
            {
                //-----------Do Nothing--------------
            }
            else
            {
                string sel = "";
                string t = "";
                if (chkGAP0.Checked)
                {
                    sel += "5,";
                    t += "0,";
                }
                if (chkGAP1.Checked)
                {
                    sel += "4,";
                    t += "-1,";
                }
                if (chkGAP2.Checked)
                {
                    sel += "3,";
                    t += "-2,";
                }
                if (chkGAP3.Checked)
                {
                    sel += "2,";
                    t += "-3,";
                }
                if (chkGAP4.Checked)
                {
                    sel += "1,";
                    t += "-4,";
                }
                Filter += " Final_Score IN (" + sel.Substring(0, sel.Length - 1) + ") AND   ";

                if (chkGAP1.Checked & chkGAP2.Checked & chkGAP3.Checked & chkGAP4.Checked)
                {
                    Title += " ที่มีช่องว่างสมรรถนะ";
                }
                else if (chkGAP0.Checked & !chkGAP1.Checked & !chkGAP2.Checked & !chkGAP3.Checked & !chkGAP4.Checked)
                {
                    Title += " ที่ไม่มีช่องว่างสมรรถนะ";
                }
                else
                {
                    Title += " ที่มีช่องว่าง " + t.Substring(0, t.Length - 1);
                }
            }
            if (Filter != "") Filter = "  " + Filter.Substring(0, Filter.Length - 6) + "";

            String TitleCOMP = " ";
            if (chkCOMP1.Checked)
            {
                SQL += template + " FROM vw_COMP_CORE_GAP \n"  + "\nUNION ALL\n";
                TitleCOMP += "สมรรถนะหลัก(Core),";
            }
            if (chkCOMP3.Checked)
            {
                SQL += template + " FROM vw_COMP_MGR_GAP \n"  + "\nUNION ALL\n";
                TitleCOMP += "สมรรถนะตามสายระดับ/การจัดการทั่วไป(Managerial),";
            }
            if (chkCOMP2.Checked)
            {
                SQL += template + " FROM vw_COMP_FN_GAP \n"  + "\nUNION ALL\n";
                TitleCOMP += "สมรรถนะตามสายงาน(Functional),";
            }

            Title += TitleCOMP.Substring(0, TitleCOMP.Length - 1);

           


            SQL = SQL.Substring(0, SQL.Length - ("\nUNION ALL").Length) + ") IDP\n ORDER BY ";
            SQL += "GAP_ID,DEPT_Code,PNPS_CLASS DESC,POS_NO";

            SqlDataAdapter DA = new SqlDataAdapter(SQL, BL.ConnectionString());
            DA.SelectCommand.CommandTimeout = 400;
            DataTable DT = new DataTable();
            DA.Fill(DT);



            if (!string.IsNullOrEmpty(Title))
            {
                Title = StartTitle + " " + Title;
            }
            else
            {
                Title = StartTitle;
            }

            DT.DefaultView.RowFilter = Filter;

            Session["RPT_C_IDP_PSN_BY_GAP"] = DT.DefaultView.ToTable();

            //Session["RPT_C_IDP_PSN_BY_GAP"] = DT;
            //--------- Title --------------
            Session["RPT_C_IDP_PSN_BY_GAP_Title"] = Title;
            //--------- Sub Title ----------
            Session["RPT_C_IDP_PSN_BY_GAP_SubTitle"] = "ปี " + R_Year + " รอบ " + R_Round;
            Session["RPT_C_IDP_PSN_BY_GAP_Round"]=R_Round;
            Session["RPT_C_IDP_PSN_BY_GAP_Year"]=R_Year;

            //lblTitle.Text = "ปี " + R_Year + " รอบ " + R_Round;
            lblTitle.Text = Title;

            if (DT.DefaultView.Count == 0)
            {
                lblTitle.Text += "\n" + "<br>ไม่พบรายการดังกล่าว";
            }
            else
            {
                lblTitle.Text += "\n" + "<br>พบ " + GL.StringFormatNumber(DT.DefaultView.Count, 0) + " รายการ";

                string[] Col = { "PSNL_NO" };
                DataTable tmp = DT.DefaultView.ToTable(true, Col).Copy();

                lblTitle.Text += " พนักงาน " + GL.StringFormatNumber(tmp.Rows.Count, 0) + " คน";

            }


            rptIDP.DataSource = Session["RPT_C_IDP_PSN_BY_GAP"];
            rptIDP.DataBind();
        }

        string LastGap = "";
        int pno = 0;
        int totalGap = 0;
        protected void rptIDP_ItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
        {
            switch (e.Item.ItemType)
            { 
                case ListItemType.AlternatingItem:
                case ListItemType.Item:
                    Label lblCourse = (Label)e.Item.FindControl("lblCourse");
                    Label lblNo = (Label)e.Item.FindControl("lblNo");
                    Label lblPSNNo = (Label)e.Item.FindControl("lblPSNNo");
                    Label lblPSNName = (Label)e.Item.FindControl("lblPSNName");
                    Label lblPSNPos = (Label)e.Item.FindControl("lblPSNPos");
                    Label lblDept = (Label)e.Item.FindControl("lblDept");
                    Label lblClass = (Label)e.Item.FindControl("lblClass");
                    Label lblComp = (Label)e.Item.FindControl("lblComp");
                    Label lblScore = (Label)e.Item.FindControl("lblScore");
                    Label lblGap = (Label)e.Item.FindControl("lblGap");
                                        
                    HtmlTableRow trFooter = (HtmlTableRow)e.Item.FindControl("trFooter");
                    Label lblFooter = (Label)e.Item.FindControl("lblFooter");

                    DataRowView drv = (DataRowView)e.Item.DataItem;
                    if (LastGap != drv["GAP_Name"].ToString())
                    {
                        LastGap = drv["GAP_Name"].ToString();
                        lblCourse.Text = LastGap;
                        totalGap += 1;
                        pno = 0;
                    }
                    pno += 1;
                    lblNo.Text = pno.ToString();
                    lblPSNNo.Text= drv["PSNL_NO"].ToString();
                    lblPSNName.Text= drv["PSNL_Fullname"].ToString();
                    lblPSNPos.Text= drv["POS_Name"].ToString();
                    lblDept.Text= drv["DEPT_NAME"].ToString();
                    lblClass.Text= GL.CINT( drv["PNPS_CLASS"].ToString()).ToString();
                    lblComp.Text= drv["COMP_Comp"].ToString();
                    lblScore.Text= drv["Final_Score"].ToString();

                    if ((5 - GL.CINT(drv["Final_Score"])) == 0) { 
                        lblGap.Text =  (5 - GL.CINT(drv["Final_Score"])).ToString();
                    }else{
                        lblGap.Text = "-" + (5 - GL.CINT(drv["Final_Score"])).ToString();
                    }

                    DataTable dt = (DataTable)rptIDP.DataSource;
                    if (e.Item.ItemIndex == dt.Rows.Count-1)
                    {
                        lblFooter.Text = "รวมจำนวนผู้เข้าอบรมหลักสูตร " + LastGap + "จำนวน " + GL.StringFormatNumber(pno,0) + " คน";
                    }
                    else if (dt.Rows[e.Item.ItemIndex + 1]["GAP_Name"].ToString() != LastGap)
                    {
                        lblFooter.Text = "รวมจำนวนผู้เข้าอบรมหลักสูตร " + LastGap + "จำนวน " + GL.StringFormatNumber(pno,0) + " คน";
                    }
                    else
                    {
                        trFooter.Visible = false;
                    }
                    break;

                case ListItemType.Footer:

                    HtmlTableRow trSum = (HtmlTableRow)e.Item.FindControl("trSum");
                    Label lblSum = (Label)e.Item.FindControl("lblSum");

                    lblSum.Text = "รวม " + GL.StringFormatNumber(totalGap, 0) + " หลักสูตร";
                    break;
            }            
        }



        protected void btnSearch_Click(object sender, System.EventArgs e)
		{
			BindPersonalList();
		}

        public RPT_C_IDP_PSN_By_Gap()
		{
			Load += Page_Load;
		}

    }
}
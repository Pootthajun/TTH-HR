﻿<%@ Page  Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPage.Master" CodeBehind="AssessmentSetting_COMP_Core.aspx.cs" Inherits="VB.AssessmentSetting_COMP_Core" %>

<%@ Register src="WUC_DateReporter.ascx" tagname="WUC_DateReporter" tagprefix="uc1" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<%@ Register src="PageNavigation.ascx" tagname="PageNavigation" tagprefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<asp:ToolkitScriptManager ID="toolkit1" runat="server"></asp:ToolkitScriptManager>
<asp:UpdatePanel ID="UpdatePanel1" runat="server">
<ContentTemplate>
<div class="container-fluid">
				<!-- BEGIN PAGE HEADER-->
				<div class="row-fluid" id="fixedPageHeader">
					<div class="span12"> 
						<!-- BEGIN PAGE TITLE & BREADCRUMB-->
						<h3 class="page-user-md">สมรรถนะหลัก (Core) <font color="blue">(ScreenID : S-HDR-05)</font></h3>					
									
						<ul class="breadcrumb">
                            
                        	<li>
                        	    <i class="icon-th-list"></i> <a href="javascript:;">การกำหนดข้อมูลสมรรถนะ</a><i class="icon-angle-right"></i>
                        	</li>
                        	<li>
                        	    <i class="icon-th-list"></i> <a href="javascript:;">สมรรถนะหลัก (Core)</a></i>
                        	</li>
                        	
                            <uc1:WUC_DateReporter ID="WUC_DateReporter1" runat="server" />
                        </ul>
						<!-- END PAGE TITLE & BREADCRUMB-->
									        
				     </div>

				
			    </div>
		        <!-- END PAGE HEADER-->
				
				
		    <asp:Panel ID="pnlList" runat="server" Visible="True">
                <div class="row-fluid">
					
					<div class="span12">
						<!-- BEGIN SAMPLE TABLE PORTLET-->
						            	        <div class="row-fluid form-horizontal">
												     <div class="span4 ">
														<div class="control-group">
													        <label class="control-label"><i class="icon-retweet"></i> สำหรับรอบการประเมิน</label>
													        <div class="controls">
														            <asp:DropDownList ID="ddlRound" OnSelectedIndexChanged="ddlRound_SelectedIndexChanged" runat="server" AutoPostBack="true" CssClass="medium m-wrap">
														            </asp:DropDownList>
													        </div>
												        </div>
													</div>
													
													<div class="span6 ">
														<div class="control-group">
													        <label class="control-label"><i class="icon-male"></i> ประเภทพนักงาน</label>
													        <div class="controls">
														            <asp:DropDownList ID="ddlPSNType" OnSelectedIndexChanged="Search" runat="server" AutoPostBack="true" CssClass="medium m-wrap">
														             </asp:DropDownList>														       
													        </div>
												        </div>
													</div>									   
												</div>
												
												<div class="row-fluid form-horizontal">												    
												    <div class="span4 ">
														<div class="control-group">
													        <label class="control-label"><i class="icon-list-ol"></i> กลุ่มระดับ</label>
													        <div class="controls">
														        <asp:DropDownList ID="ddlClassGroup" OnSelectedIndexChanged="Search" runat="server" AutoPostBack="true" CssClass="medium m-wrap">
														        </asp:DropDownList>
													        </div>
												        </div>
													</div>
													<div class="span6 ">
														<div class="control-group">
													        <label class="control-label"></label>
													        <div class="controls">
														        <asp:CheckBox ID="chkBlank" OnCheckedChanged="Search" runat="server" Text="" AutoPostBack="true" /> 
														        <span style="font-size:14px; position:relative; top:3px;">แสดงเฉพาะที่ยังไม่ได้กำหนด</span>
													        </div>
												        </div>
													</div>	
												</div>
												
								            
							<div class="portlet-body no-more-tables">
								<asp:Label ID="lblCountList" runat="server" Font-Size="14px" Font-Bold="true"></asp:Label>
								<table class="table table-full-width table-advance dataTable no-more-tables table-hover">
									<thead>
										<tr>
											<th><i class="icon-list-ol"></i> กลุ่มระดับ</th>
											<th><i class="icon-male"></i> ประเภทพนักงาน</th>
											<th><i class="icon-th-list"></i> การประเมิน(ข้อ)</th>
											<th><i class="icon-book"></i> หลักสูตรการพัฒนา</th>
											<th><i class="icon-bolt"></i> ดำเนินการ</th>
										</tr>
									</thead>
									<tbody>
										<asp:Repeater ID="rptList" OnItemCommand="rptList_ItemCommand" OnItemDataBound="rptList_ItemDataBound" runat="server">
										    <ItemTemplate>
										            <tr>
											            <td data-title="กลุ่มระดับ"><asp:Label ID="lblClassGroup" runat="server"></asp:Label></td>
											            <td data-title="ประเภทพนักงาน"><asp:Label ID="lblPSNType" runat="server"></asp:Label></td>
											            <td data-title="การประเมิน(ข้อ)"><asp:Label ID="lblTotalCOMP" runat="server"></asp:Label></td>
											            <td data-title="หลักสูตรการพัฒนา"><i class="icon-ok-sign" id="iconGAP" runat="server"></i></td>
											            <td data-title="ดำเนินการ">
											            <asp:Button ID="btnEdit" runat="server" CssClass="btn mini blue" Text="กำหนดแบบประเมิน" CommandName="Edit" /> 
											            <asp:Button ID="btnDelete" runat="server" CssClass="btn red mini"  Text="ลบแบบประเมิน" CommandName="Delete" />
											            <asp:ConfirmButtonExtender TargetControlID="btnDelete" ID="cfm_Delete" runat="server"></asp:ConfirmButtonExtender>
											            </td>
										            </tr>
										    </ItemTemplate>
										</asp:Repeater>
									</tbody>
								</table>
								
								<asp:PageNavigation ID="Pager" OnPageChanging="Pager_PageChanging" MaximunPageCount="10" PageSize="20" runat="server" />
								
							</div>
						
						<!-- END SAMPLE TABLE PORTLET-->						
					</div>
                </div>
              </asp:Panel>
  
            <asp:Panel ID="pnlEdit" runat="server" Visible="False">                
				<div class="portlet-body form">
					<div class="btn-group pull-left">
                        <asp:Button ID="btnBack_TOP" OnClick="btnCancel_Click" runat="server" CssClass="btn" Text="ย้อนกลับ" />
                    </div>	<br />				
			        <h3 class="form-section"><i class="icon-th-list"></i> แบบประเมินสมรรถนะหลัก<asp:Label ID="lblYearRound" runat="server"></asp:Label>
			        สำหรับพนักงาน<asp:Label ID="lblPSNType" runat="server" PSNL_Type_Code="-1"></asp:Label>
			        ระดับ <asp:Label ID="lblClassName" runat="server" CLSGP_ID="0"></asp:Label>
			        </h3>    					
				    <!-- BEGIN FORM-->
				        <div class="portlet-body no-more-tables" id="divView" runat="server">
				                        <table class="table table-bordered no-more-tables" style="background-color:White;">
									        <thead>
										        <tr>
											        <th rowspan="2" style="text-align:center;"> รหัส</th>
											        <th rowspan="2" style="text-align:center;"> สมรรถนะ</th>
											        <th rowspan="2" style="text-align:center;"> มาตรฐานพฤติกรรม</th>
											        <th colspan="5" style="text-align:center;"> คะแนนตามระดับค่าเป้าหมาย</th>
											        <th rowspan="2" style="text-align:center;"> หลักสูตรการพัฒนา</th>
											        <th rowspan="2" style="text-align:center;"> ดำเนินการ</th>
										        </tr>
                                                <tr>
										          <th style="text-align:center;">1</th>
										          <th style="text-align:center;">2</th>
										          <th style="text-align:center;">3</th>
										          <th style="text-align:center;">4</th>
										          <th style="text-align:center;">5</th>
								              </tr>
									        </thead>
									        <tbody>										        
										        <asp:Repeater ID="rptCOMP" OnItemCommand="rptCOMP_ItemCommand" OnItemDataBound="rptCOMP_ItemDataBound" runat="server">
										            <ItemTemplate>
							                                <tr>
										                      <td data-title="รหัส" id="ass_no" runat="server" style="text-align:center; font-weight:bold; vertical-align:top; cursor:pointer;">001</td>
										                      <td data-title="สมรรถนะ" id="COMP_Name" runat="server">xxxxxxxxxxxx xxxxxxxxxxxx</td>
										                      <td data-title="มาตรฐานพฤติกรรม" id="target" runat="server">xxxxxxxxxxxx xxxxxxxxxxxx</td>
										                      <td data-title="ได้ 1 คะแนน" id="choice1" runat="server">xxxxxxxxxxxx xxxxxxxxxxxx xxxxxxxxxxxx xxxxxxxxxxxx xxxxxxxxxxxx xxxxxxxxxxxx</td>
                                                              <td data-title="ได้ 2 คะแนน" id="choice2" runat="server">xxxxxxxxxxxx xxxxxxxxxxxx xxxxxxxxxxxx xxxxxxxxxxxx xxxxxxxxxxxx xxxxxxxxxxxx</td>
                                                              <td data-title="ได้ 3 คะแนน" id="choice3" runat="server">xxxxxxxxxxxx xxxxxxxxxxxx xxxxxxxxxxxx xxxxxxxxxxxx xxxxxxxxxxxx xxxxxxxxxxxx</td>
                                                              <td data-title="ได้ 4 คะแนน" id="choice4" runat="server">xxxxxxxxxxxx xxxxxxxxxxxx xxxxxxxxxxxx xxxxxxxxxxxx xxxxxxxxxxxx xxxxxxxxxxxx</td>
                                                              <td data-title="ได้ 5 คะแนน" id="choice5" runat="server">xxxxxxxxxxxx xxxxxxxxxxxx xxxxxxxxxxxx xxxxxxxxxxxx xxxxxxxxxxxx xxxxxxxxxxxx</td>
										                      <td data-title="หลักสูตรการพัฒนา" id="GAPList" runat="server">xxxxxxxxxxxxxxxx</td>
										                      <td data-title="ดำเนินการ" style="text-align:center;">
										                          <asp:Button CssClass="btn" ID="btnEdit" CommandName="Edit" Style="display:none;" runat="server" Text="แก้ไข" />
										                          <asp:Button CssClass="btn red mini" ID="btnDelete" CommandName="Delete" runat="server" Text="ลบออก" />
										                          <asp:ConfirmButtonExtender TargetControlID="btnDelete" ID="cfm_Delete" runat="server"></asp:ConfirmButtonExtender>
										                      </td>
									                        </tr>     
							                        </ItemTemplate>
										        </asp:Repeater>					
									        </tbody>
								        </table>								        
						                <div class="form-actions">
										    <asp:Button ID="btnAdd" OnClick="btnAdd_Click" runat="server" CssClass="btn purple" Text="เพิ่มสมรรถนะ" />
											<asp:Button CssClass="btn" runat="server" ID="btnCancel" OnClick="btnCancel_Click" Text="ย้อนกลับ" />
										</div>
				                  
								                
					</div>
						
				    <!-- END FORM-->  
				    
			        <div class="row-fluid" id="divEdit" runat="server" >
					     <div class="span12">
					         <div class="portlet-body form">
    					                <h3 class="form-section"><i class="icon-th-list"></i> 
							                 กำหนดแบบประเมินสมรรถนะหลัก ลำดับ <asp:Label ID="lblCOMPNo" runat="server"></asp:Label>
							            </h3>
					         </div>
					         <div class="row-fluid form-horizontal">
					                <div class="span4 ">
					                        <div class="control-group">
                                                <asp:Label class="control-label" runat ="server" Font-Size ="14px" >สมรรถนะ</asp:Label>
		                                        <div class="controls">
		                                            <asp:TextBox TextMode="MultiLine" runat="server" MaxLength="500" ID="txtCOMP" CssClass="large m-wrap" Height="100px"></asp:TextBox>
			                                    </div>
	                                        </div>
					                        <div class="control-group">
		                                        <asp:Label class="control-label" runat ="server" Font-Size ="14px" >มาตรฐานพฤติกรรม</asp:Label>
		                                        <div class="controls">
		                                            <asp:TextBox TextMode="MultiLine" runat="server" MaxLength="500" ID="txtSTD" CssClass="large m-wrap" Height="100px"></asp:TextBox>
			                                    </div>
	                                        </div>
	                                        <div class="control-group">
		                                        <asp:Label class="control-label" runat ="server" Font-Size ="14px" >เป้าหมายระดับที่ 1</asp:Label>
		                                        <div class="controls">
		                                            <asp:TextBox TextMode="MultiLine" runat="server" MaxLength="1000" ID="txtChoice1" CssClass="large m-wrap" Height="100px"></asp:TextBox>
			                                    </div>
	                                        </div>
	                                         <div class="control-group">
		                                        <asp:Label class="control-label" runat ="server" Font-Size ="14px" >เป้าหมายระดับที่ 2</asp:Label>
		                                        <div class="controls">
		                                            <asp:TextBox TextMode="MultiLine" runat="server" MaxLength="1000" ID="txtChoice2" CssClass="large m-wrap" Height="100px"></asp:TextBox>
			                                    </div>
	                                        </div>
	                                         <div class="control-group">
		                                        <asp:Label class="control-label" runat ="server" Font-Size ="14px" >เป้าหมายระดับที่ 3</asp:Label>
		                                        <div class="controls">
		                                            <asp:TextBox TextMode="MultiLine" runat="server" MaxLength="1000" ID="txtChoice3" CssClass="large m-wrap" Height="100px"></asp:TextBox>
			                                    </div>
	                                        </div>
	                                         <div class="control-group">
		                                        <asp:Label class="control-label" runat ="server" Font-Size ="14px" >เป้าหมายระดับที่ 4</asp:Label>
		                                        <div class="controls">
		                                            <asp:TextBox TextMode="MultiLine" runat="server" MaxLength="1000" ID="txtChoice4" CssClass="large m-wrap" Height="100px"></asp:TextBox>
			                                    </div>
	                                        </div>
	                                         <div class="control-group">
		                                        <asp:Label class="control-label" runat ="server" Font-Size ="14px" >เป้าหมายระดับที่ 5</asp:Label>
		                                        <div class="controls">
		                                            <asp:TextBox TextMode="MultiLine" runat="server" MaxLength="1000" ID="txtChoice5" CssClass="large m-wrap" Height="100px"></asp:TextBox>
			                                    </div>
	                                        </div>
	                                        <div class="control-group">
								                <asp:Label class="control-label" runat ="server" Font-Size ="14px" >หลักสูตรการพัฒนา</asp:Label>
								                <div class="controls">
								                
						                        <table>
                                                        <tbody>
                                                            <asp:Repeater ID="rptGAP" OnItemDataBound="rptGAP_ItemDataBound" runat="server">
								                                    <ItemTemplate>									                    
											                                <tr>
			                                                                    <td><asp:Label ID="lblGAP" runat="server"></asp:Label></td>			                                                                    
		                                                                    </tr>		
								                                    </ItemTemplate>
								                             </asp:Repeater>        								                    
						                                </tbody>	
                                                 </table>									                
									           </div>
							                </div>		
					                </div>
					        </div>
					       
					       
					        <div class="form-actions">
                                <asp:Button ID="btnOK" OnClick="btnOK_Click" runat="server" CssClass="btn blue" Text="บันทึก" />
                                <asp:Button ID="btnClose" OnClick="btnClose_Click" runat="server" CssClass="btn" Text="ยกเลิก" />
                            </div>
					     </div>
					      
			        </div>
			    </div>	
		    </asp:Panel>
</div>	

</ContentTemplate>				                
</asp:UpdatePanel>					 
</asp:Content>


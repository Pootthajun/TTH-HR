﻿<%@ Page  Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPage.Master" CodeBehind="RPT_A_Round_Result.aspx.cs" Inherits="VB.RPT_A_Round_Result" %>


<%@ Register src="WUC_DateReporter.ascx" tagname="WUC_DateReporter" tagprefix="uc1" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<%@ Register src="PageNavigation.ascx" tagname="PageNavigation" tagprefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<asp:ToolkitScriptManager ID="toolkit1" runat="server"></asp:ToolkitScriptManager>

<asp:UpdatePanel ID="UpdatePanel1" runat="server">      
<ContentTemplate>
<div class="container-fluid">
				<!-- BEGIN PAGE HEADER-->
				<div class="row-fluid" id="fixedPageHeader">
					<div class="span12"> 
						<!-- BEGIN PAGE TITLE & BREADCRUMB-->
						<h3>สรุปผลการประเมินประจำรอบของพนักงาน <font color="blue"> (ScreenID : R-ALL-05)</font></h3>						
						<ul class="breadcrumb">
                            <li>
                            	<i class="icon-copy"></i> <a href="javascript:;">รายงาน</a><i class="icon-angle-right"></i>
                            </li>
                            <li>
                            	<i class="icon-copy"></i> <a href="javascript:;">การประเมินผลตัวชี้วัดและสมรรถนะ</a><i class="icon-angle-right"></i>
                            </li>
                            <li>
                        	    <i class="icon-check"></i> <a href="javascript:;">สรุปผลการประเมินประจำรอบของพนักงาน</a>
                        	</li>                      	
                        	
                            <uc1:WUC_DateReporter ID="WUC_DateReporter1" runat="server" />
                        </ul>
						<!-- END PAGE TITLE & BREADCRUMB-->						
				     </div>

				
			    </div>
		        <!-- END PAGE HEADER-->
				
                <asp:Panel ID="pnlList" runat="server" Visible="True" DefaultButton="btnSearch">
				<div class="row-fluid">
					
					<div class="span12">
						<!-- BEGIN SAMPLE TABLE PORTLET-->
                                       <div class="row-fluid form-horizontal">
                			                  <div class="btn-group pull-left">
                                                    <asp:Button ID="btnSearch" OnClick="btnSearch_Click"  runat="server" CssClass="btn green" Text="ค้นหา" />
                                              </div>                                        
 
						                     <div class="btn-group pull-right">                                    
								                <button data-toggle="dropdown" class="btn dropdown-toggle" id="Button1">พิมพ์ <i class="icon-angle-down"></i></button>										
								                <ul class="dropdown-menu pull-right">
                                               
                                                    <li><a href="Print/RPT_A_PSNAssStatus.aspx?Mode=PDF" target="_blank">รูปแบบ PDF</a></li>
                                                    <li><a href="Print/RPT_A_PSNAssStatus.aspx?Mode=EXCEL" target="_blank">รูปแบบ Excel</a></li>												
								                </ul>
							                </div>
                                        </div>	
							            
						                 <div class="row-fluid form-horizontal">
						                             <div class="span5 ">
														<div class="control-group">
													        <label class="control-label"> รอบการประเมิน</label>
													        <div class="controls">
														        <asp:DropDownList ID="ddlRound" runat="server" CssClass="medium m-wrap">
							                                    </asp:DropDownList>
														    </div>
												        </div>
													</div>
													<div class="span5 ">
														<div class="control-group">
													        <label class="control-label"> ชื่อพนักงาน</label>
													        <div class="controls">
														        <asp:TextBox ID="txtName" runat="server" CssClass="medium m-wrap" Placeholder="ค้นหาชื่อพนักงาน/เลขประจำตัว"></asp:TextBox>		
													        </div>
												        </div>
													</div>	   
    										</div>
    										<div class="row-fluid form-horizontal">		
												     <div class="span5 ">
														<div class="control-group">
													        <label class="control-label"> ฝ่าย</label>
													        <div class="controls">
														        <asp:DropDownList ID="ddlSector" runat="server" CssClass="medium m-wrap">
														        </asp:DropDownList>
													        </div>
												        </div>
													</div>	
													
													<div class="span5 ">
														<div class="control-group">
													        <label class="control-label"> ชื่อหน่วยงาน</label>
													        <div class="controls">
														        <asp:TextBox ID="txtDeptName" runat="server" CssClass="medium m-wrap" Placeholder="ค้นหาชื่อฝ่าย/หน่วยงาน"></asp:TextBox>		
													        </div>
												        </div>
													</div>
											</div>
											<div class="row-fluid form-horizontal">
											    <div class="span12">
											        <label class="control-label">ระดับ</label>
											                <div class="controls">
											                    <label class="checkbox">
											                        <asp:CheckBox ID="chkClass1" runat="server" Text=""/>&nbsp;1
											                    </label>
											                    <label class="checkbox">
											                        <asp:CheckBox ID="chkClass2" runat="server" Text=""/>&nbsp;2
											                    </label>
											                    <label class="checkbox">
											                        <asp:CheckBox ID="chkClass3" runat="server" Text=""/>&nbsp;3
											                    </label>
											                    <label class="checkbox">
											                        <asp:CheckBox ID="chkClass4" runat="server" Text=""/>&nbsp;4
											                    </label>
											                    <label class="checkbox">
											                        <asp:CheckBox ID="chkClass5" runat="server" Text=""/>&nbsp;5
											                    </label>
											                    <label class="checkbox">
											                        <asp:CheckBox ID="chkClass6" runat="server" Text=""/>&nbsp;6
											                    </label>
											                    <label class="checkbox">
											                        <asp:CheckBox ID="chkClass7" runat="server" Text=""/>&nbsp;7
											                    </label>
											                    <label class="checkbox">
											                        <asp:CheckBox ID="chkClass8" runat="server" Text=""/>&nbsp;8
											                    </label>
											                    <label class="checkbox">
											                        <asp:CheckBox ID="chkClass9" runat="server" Text=""/>&nbsp;9
											                    </label>
											                    <label class="checkbox">
											                        <asp:CheckBox ID="chkClass10" runat="server" Text=""/>&nbsp;10
											                    </label>
											                    <label class="checkbox">
											                        <asp:CheckBox ID="chkClass11" runat="server" Text=""/>&nbsp;11
											                    </label>
											                    <label class="checkbox">
											                        <asp:CheckBox ID="chkClass12" runat="server" Text=""/>&nbsp;12
											                    </label>
											                    <label class="checkbox">
											                        <asp:CheckBox ID="chkClass13" runat="server" Text=""/>&nbsp;13
											                    </label>
										                   </div>
											    </div>
											</div>
											<div class="row-fluid form-horizontal">
											    
											        <div class="span5">
											            <div class="control-group">
										                    <label class="control-label">การประเมิน</label>
										                    <div class="controls">
                                                                <label class="checkbox">
											                        <asp:CheckBox ID="chkKPINo" runat="server" Text=""/>&nbsp;ยังประเมินไม่เสร็จ
											                    </label>
                                                                <label class="checkbox">
											                        <asp:CheckBox ID="chkKPIYes" runat="server" Text=""/>&nbsp;ประเมินเสร็จสมบูรณ์
											                    </label>
										                    </div>
									                    </div>
											        </div>

												    <div class="span6">
												        <div class="control-group">
													        <label class="control-label"> การประเมินสมรรถนะ (Competency)</label>
													        <div class="controls">
											                    <label class="checkbox">
											                    <div class="checker"><span>
											                    <asp:CheckBox ID="chkCOMPNo" runat="server" Text=""  />
											                    </span></div> ยังประเมินไม่เสร็จ
											                    </label>
											                    <label class="checkbox">
											                    <div class="checker"><span>
											                    <asp:CheckBox ID="chkCOMPYes" runat="server" Text="" />
											                    </span></div> ประเมินเสร็จสมบูรณ์
											                    </label>
										                    </div>
												        </div>
													</div>
											</div>
											
								    
								            
							<div class="portlet-body no-more-tables">
								<asp:Label ID="lblCountPSN" runat="server" Font-Size="14px" Font-Bold="true"></asp:Label>
								&nbsp;
								<asp:Label ID="lblTitle" runat="server" Font-Size="14px" Font-Bold="true" Width="100%" style="text-align:center"></asp:Label>
								<table class="table table-full-width table-bordered dataTable table-hover">
									<thead>
										<tr>
											<th rowspan="2" style="text-align:center;">เลขประจำตัว</th>
											<th rowspan="2" style="text-align:center;">ชื่อ</th>
											<th rowspan="2" style="text-align:center;">ตำแหน่ง</th>
											<th rowspan="2" style="text-align:center;">ระดับ</th>
											<th rowspan="2" style="text-align:center;">ประเภท</th>
											<th colspan="2" style="text-align:center;">การประเมินตัวชี้วัด (KPI)</th>
											<th colspan="2" style="text-align:center;">การประเมินสมรรถนะ (Competency)</th>
											<th rowspan="2" style="text-align:center;">คะแนน<br>ประเมิน</th>
											<th rowspan="2" style="text-align:center;">วันลา</th>
											<th rowspan="2" style="text-align:center;">คะแนน<br>วันลา</th>
											<th rowspan="2" style="text-align:center;">สรุปคะแนน</th>
										</tr>
										<tr>
										    <th style="text-align:center;">ประเมินเสร็จ</th>
											<th style="text-align:center;">คะแนน</th>
											<th style="text-align:center;">ประเมินเสร็จ</th>
											<th style="text-align:center;">คะแนน</th>											
										</tr>
									</thead>
									<tbody>
										<asp:Repeater ID="rptASS" OnItemDataBound="rptASS_ItemDataBound" runat="server">
									        <ItemTemplate>
									            <tr id="tdSector" runat="server">
							                        <td colspan="13" style="text-align:center; font-weight:bold; font-size:18px; background-color:#EEEEEE;" >
							                            <asp:Label ID="lblSector" runat="server"></asp:Label>
							                        </td>
							                    </tr>
							                    <tr id="tdDept" runat="server">
							                        <td colspan="13" style="text-align:left; font-size:14px; font-weight:bold;" >
							                            <asp:Label ID="lblDept" runat="server"></asp:Label>
							                        </td>
							                    </tr>
									            <tr>    
									                <td data-title="เลขประจำตัว" style="text-align:center;"><asp:Label ID="lblPSNNo" runat="server"></asp:Label></td>
									                <td data-title="ชื่อ" ><asp:Label ID="lblPSNName" runat="server"></asp:Label></td>     
									                <td data-title="ตำแหน่ง" ><asp:Label ID="lblPSNPos" runat="server"></asp:Label></td>
									                <td data-title="ระดับ" style="text-align:center;"><asp:Label ID="lblPSNClass" runat="server"></asp:Label></td>   
									                <td data-title="ประเภท" style="text-align:center;"><asp:Label ID="lblPSNType" runat="server"></asp:Label></td>   
											        <td data-title="ประเมินตัวชี้วัดเสร็จ" style="text-align:center;"><a ID="lbl_KPI_Status" runat="server" target="_blank"></a></td>
											        <td data-title="คะแนนตัวชี้วัด" style="text-align:center;"><a ID="lbl_KPI_Result" runat="server" target="_blank"></a></td>
											        <td data-title="ประเมินสมรรถนะเสร็จ" style="text-align:center;"><a ID="lbl_COMP_Status" runat="server" target="_blank"></a></td>
											        <td data-title="คะแนนสมรรถนะ" style="text-align:center;"><a ID="lbl_COMP_Result" runat="server" target="_blank"></a></td>
											        <td data-title="คะแนนประเมิน" style="text-align:center;"><asp:Label ID="lbl_Ass_Score" runat="server" Font-Bold="true"></asp:Label></td>
											        <td data-title="วันลา" style="text-align:center;"><asp:Label ID="lbl_Leave_Day" runat="server"></asp:Label></td>
											        <td data-title="คะแนนวันลา" style="text-align:center;"><asp:Label ID="lbl_Leave_Score" runat="server"></asp:Label></td>
											        <td data-title="สรุปคะแนน" style="text-align:center;"><asp:Label ID="lbl_Result" runat="server"></asp:Label></td>
											    </tr>
									        </ItemTemplate>
										</asp:Repeater>
																							
									</tbody>
								</table>
								<asp:PageNavigation ID="Pager" OnPageChanging="Pager_PageChanging" MaximunPageCount="10" PageSize="20" runat="server" />
							</div>
					</div>
                </div>  
                </asp:Panel>  


</div>

</ContentTemplate>           
</asp:UpdatePanel>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ScriptPlaceHolder" Runat="Server">
</asp:Content>


﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="Setting_Transfer_COMP_Oracle.aspx.cs" Inherits="VB.Setting_Transfer_COMP_Oracle" %>

<%@ Register src="WUC_DateReporter.ascx" tagname="WUC_DateReporter" tagprefix="uc1" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<%@ Register src="PageNavigation.ascx" tagname="PageNavigation" tagprefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<asp:ToolkitScriptManager ID="toolkit1" runat="server"></asp:ToolkitScriptManager>
<asp:UpdatePanel ID="UpdatePanel1" runat="server">      
<ContentTemplate>
<div class="container-fluid">
				<!-- BEGIN PAGE HEADER-->
				<div class="row-fluid" id="fixedPageHeader">
					<div class="span12"> 
						<!-- BEGIN PAGE TITLE & BREADCRUMB-->
						<h3>ส่งผลการประเมิน Competency ไปยัง Oracle <font color="blue"> (ScreenID : S-BAS-08)</font></h3>						
						<ul class="breadcrumb">
                            <li>
                            	<i class="icon-cogs"></i><a href="javascript:;">จัดการระบบ/ข้อมูลพื้นฐาน</a><i class="icon-angle-right"></i>
                            </li>
                        	<li><i class="icon-share-alt"></i> <a href="javascript:;">ส่งผลการประเมินไปยัง Oracle</a></li>
                        	
                            <uc1:WUC_DateReporter ID="WUC_DateReporter1" runat="server" />
                        </ul>
						<!-- END PAGE TITLE & BREADCRUMB-->						
				     </div>

				
			    </div>
		        <!-- END PAGE HEADER-->

                <asp:Panel ID="pnlList" runat="server" Visible="True" DefaultButton="btnSearch">
				<div class="row-fluid">
                    <div class="span12">
						<!-- BEGIN SAMPLE TABLE PORTLET-->
                         <div class="row-fluid form-horizontal">
                			    <div class="btn-group pull-left">
                                    <asp:Button ID="btnSearch" OnClick="btnSearch_Click"  runat="server" CssClass="btn green" Text="ค้นหา" />
                                </div>
                                 </div>

                                  <div class="row-fluid form-horizontal">
						                             <div class="span5 ">
											<div class="control-group">
												<label class="control-label"> รอบการประเมิน</label>
												<div class="controls">
													<asp:DropDownList ID="ddlRound" runat="server" CssClass="medium m-wrap">
							                        </asp:DropDownList>
												</div>
											</div>
										</div>
													<div class="span5 ">
														<div class="control-group">
													        <label class="control-label"> ชื่อพนักงาน</label>
													        <div class="controls">
														        <asp:TextBox ID="txtName" runat="server" CssClass="medium m-wrap" Placeholder="ค้นหาชื่อพนักงาน/เลขประจำตัว"></asp:TextBox>		
													        </div>
												        </div>
													</div>	   
    										</div>
    										<div class="row-fluid form-horizontal">		
												     <div class="span5 ">
														<div class="control-group">
													        <label class="control-label"> ฝ่าย</label>
													        <div class="controls">
														        <asp:DropDownList ID="ddlSector" runat="server"  CssClass="medium m-wrap">
														        </asp:DropDownList>
													        </div>
												        </div>
													</div>	
													
													<div class="span5 ">
														<div class="control-group">
													        <label class="control-label"> ชื่อหน่วยงาน</label>
													        <div class="controls">
														        <asp:TextBox ID="txtDeptName" runat="server" CssClass="medium m-wrap" Placeholder="ค้นหาชื่อฝ่าย/หน่วยงาน"></asp:TextBox>		
													        </div>
												        </div>
													</div>
											</div>

                                            <div class="row-fluid form-horizontal">		
												     <div class="span5 ">
														<div class="control-group">
													        <label class="control-label"> สถานะการโอน</label>
													        <div class="controls">
														        <asp:DropDownList ID="ddlStatus"  runat="server"  CssClass="medium m-wrap">
														                <asp:ListItem Text="ทั้งหมด" Value="-1" Selected="true"></asp:ListItem>
														                <asp:ListItem Text="โอนแล้ว" Value="1"></asp:ListItem>
					                                                    <asp:ListItem Text="ยังไม่โอน" Value="0"></asp:ListItem>
														            </asp:DropDownList>
													        </div>
												        </div>
													</div>	
													
													
												    <div class="span5">
												        <div class="control-group">
													        <label class="control-label"> การประเมินสมรรถนะ (Competency)</label>
													        
                                                            <div class="controls">
                                                                <label class="checkbox">
											                        <asp:CheckBox ID="chkCOMPNo" runat="server" Text=""/>&nbsp;ยังประเมินไม่เสร็จ
											                    </label>
                                                                <label class="checkbox">
											                        <asp:CheckBox ID="chkCOMPYes" runat="server" Text=""/>&nbsp;ประเมินเสร็จสมบูรณ์
											                    </label>
										                    </div>
												        </div>
													</div>
											</div>


											<div class="row-fluid form-horizontal">
											    <div class="span12">
											        <div class="control-group">
											                <label class="control-label">ระดับ</label>
											                <div class="controls">
											                    <label class="checkbox">
											                        <asp:CheckBox ID="chkClass1" runat="server" Text=""/>&nbsp;1
											                    </label>
											                    <label class="checkbox">
											                        <asp:CheckBox ID="chkClass2" runat="server" Text=""/>&nbsp;2
											                    </label>
											                    <label class="checkbox">
											                        <asp:CheckBox ID="chkClass3" runat="server" Text=""/>&nbsp;3
											                    </label>
											                    <label class="checkbox">
											                        <asp:CheckBox ID="chkClass4" runat="server" Text=""/>&nbsp;4
											                    </label>
											                    <label class="checkbox">
											                        <asp:CheckBox ID="chkClass5" runat="server" Text=""/>&nbsp;5
											                    </label>
											                    <label class="checkbox">
											                        <asp:CheckBox ID="chkClass6" runat="server" Text=""/>&nbsp;6
											                    </label>
											                    <label class="checkbox">
											                        <asp:CheckBox ID="chkClass7" runat="server" Text=""/>&nbsp;7
											                    </label>
											                    <label class="checkbox">
											                        <asp:CheckBox ID="chkClass8" runat="server" Text=""/>&nbsp;8
											                    </label>
											                    <label class="checkbox">
											                        <asp:CheckBox ID="chkClass9" runat="server" Text=""/>&nbsp;9
											                    </label>
											                    <label class="checkbox">
											                        <asp:CheckBox ID="chkClass10" runat="server" Text=""/>&nbsp;10
											                    </label>
											                    <label class="checkbox">
											                        <asp:CheckBox ID="chkClass11" runat="server" Text=""/>&nbsp;11
											                    </label>
											                    <label class="checkbox">
											                        <asp:CheckBox ID="chkClass12" runat="server" Text=""/>&nbsp;12
											                    </label>
											                    <label class="checkbox">
											                        <asp:CheckBox ID="chkClass13" runat="server" Text=""/>&nbsp;13
											                    </label>
										                   </div>
											        </div>
											    </div>
											</div>



                                <div class="portlet-body no-more-tables">
                                    <asp:Button ID="btnSend_Top" OnClick="btnSend_Click" runat="server" style="text-align:center; width:100%; font-weight:bold; font-size:14px;" CssClass="btn green btn-block" />
								    <asp:ConfirmButtonExtender TargetControlID="btnSend_Top" runat="server" ID="cfm_Top" ConfirmText="ยืนยันส่งผลการประเมินตามรายการข้างต้นไปยัง Oracle"></asp:ConfirmButtonExtender>
                                    <table class="table table-full-width table-bordered dataTable table-hover">
									<thead>
										<tr>
                                            <th style="text-align:center;">พนักงาน</th>
											<th style="text-align:center;">ตำแหน่ง</th>
											<th style="text-align:center;">ระดับ</th>
                                            <th style="text-align:center;">คะแนนเต็ม COMP</th>
                                            <th style="text-align:center;">คะแนน <asp:Label ID="lbl_Wage" runat="server"></asp:Label></th>
                                            <th style="text-align:center;">สถานะการโอน<br>ไปยัง Oracle</th>											
										</tr>										
									</thead>
									<tbody>
										<asp:Repeater ID="rptASS" OnItemDataBound="rptASS_ItemDataBound" runat="server">
									        <ItemTemplate>									            
                                                <tr id="tdSector" runat="server">
							                        <td colspan="18" style="text-align:center; font-weight:bold; font-size:18px; background-color:#EEEEEE;" >
							                            <asp:Label ID="lblSector" runat="server"></asp:Label>
							                        </td>
							                    </tr>
							                    <tr id="tdDept" runat="server">
							                        <td colspan="18" style="text-align:left; font-size:14px; font-weight:bold;" >
							                            <asp:Label ID="lblDept" runat="server"></asp:Label>
							                        </td>
							                    </tr>


                                                <tr>    
									                <td data-title="พนักงาน" style="text-align:left;">
									                <asp:Label ID="lblPSNNo" runat="server"></asp:Label> : <asp:Label ID="lblPSNName" runat="server"></asp:Label>
									                </td>
									                <td data-title="ตำแหน่ง" ><asp:Label ID="lblPSNPos" runat="server"></asp:Label></td>
									                <td data-title="ระดับ" style="text-align:center;"><asp:Label ID="lblPSNClass" runat="server"></asp:Label></td>   
											        
                                                    <td style="text-align:center;" data-title="คะแนนเต็ม COMP"><asp:Label ID="lblTotal" runat="server" Font-Bold="true"></asp:Label></td>
                                                    <td style="text-align:center;" data-title="คะแนน COMP ที่ได้"><asp:Label ID="lblMark" runat="server" Font-Bold="true"></asp:Label></td>

											        <td data-title="สถานะการโอนไปยัง Oracle" style="text-align:center;">
                                                        <asp:Image ID="imgStatusOracle" runat="server" ImageUrl=""  Height="24px"  />                                                        
                                                    </td>

                                                </tr>
									          
									        </ItemTemplate>
									        <FooterTemplate>
									        
									        </FooterTemplate>
										</asp:Repeater>
																							
									</tbody>
								</table>
								<asp:PageNavigation ID="Pager" OnPageChanging="Pager_PageChanging" MaximunPageCount="10" PageSize="20" runat="server" />
								<asp:Button ID="btnSend_Bottom" OnClick="btnSend_Click" runat="server" style="text-align:center; width:100%; font-weight:bold; font-size:14px; margin-top:20px; " CssClass="btn green btn-block" />
								<asp:ConfirmButtonExtender TargetControlID="btnSend_Bottom" runat="server" ID="cfm_Bottom" ConfirmText="ยืนยันส่งผลการประเมินตามรายการข้างต้นไปยัง Oracle"></asp:ConfirmButtonExtender>
                                </div>
                        </div>	
                    </div>
                </div>  
                </asp:Panel> 
</div>
</ContentTemplate>           
</asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ScriptPlaceHolder" runat="server">
</asp:Content>

using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Xml.Linq;
using System.Diagnostics;
using System.Collections.Specialized;
using System.Configuration;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Caching;
using System.Web.SessionState;
using System.Web.Security;
using System.Web.Profile;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using System.Data.SqlClient;
namespace VB
{

	public partial class AssessmentSetting_COMP_PSN : System.Web.UI.Page
	{

		GenericLib GL = new GenericLib();
		HRBL BL = new HRBL();

		textControlLib TC = new textControlLib();
		public int R_Year {
			get {
				try {
					return GL.CINT( GL.SplitString(ddlRound.Items[ddlRound.SelectedIndex].Value, "-")[0]);
				} catch (Exception ex) {
					return 0;
				}
			}
		}

		public int R_Round {
			get {
				try {
					return GL.CINT(GL.SplitString(ddlRound.Items[ddlRound.SelectedIndex].Value, "-")[1]);
				} catch (Exception ex) {
					return 0;
				}
			}
		}

		public string PSNL_NO {
			get { return lblPSNName.Attributes["PSNL_NO"]; }
			set { lblPSNName.Attributes["PSNL_NO"] = value; }
		}

		public string ASSESSOR_BY {
			get {
				try {
					string[] tmp = Strings.Split(ddlASSName.Items[ddlASSName.SelectedIndex].Value, ":::");
					return tmp[0];
				} catch (Exception ex) {
					return "";
				}
			}
		}

		public string ASSESSOR_NAME {
			get {
				try {
					return ddlASSName.Items[ddlASSName.SelectedIndex].Text;
				} catch (Exception ex) {
					return "";
				}
			}
		}

		public string ASSESSOR_POS {
			get {
				try {
					string[] tmp = Strings.Split(ddlASSName.Items[ddlASSName.SelectedIndex].Value, ":::");
					return tmp[1];
				} catch (Exception ex) {
					return "";
				}
			}
		}

		public string ASSESSOR_DEPT {
			get {
				try {
					string[] tmp = Strings.Split(ddlASSName.Items[ddlASSName.SelectedIndex].Value, ":::");
					return tmp[2];
				} catch (Exception ex) {
					return "";
				}
			}
		}

		public int PSNL_CLASS_GROUP {
			get {
				try {
					return Convert.ToInt32(Conversion.Val(lblPSNName.Attributes["CLSGP_ID"]));
				} catch (Exception ex) {
					return 0;
				}
			}
            set { lblPSNName.Attributes["CLSGP_ID"] = Convert.ToInt32(value).ToString(); }
		}

		public int PSNL_TYPE {
			get {
				try {
					return Convert.ToInt32(Conversion.Val(lblPSNType.Attributes["PSNL_Type_Code"]));
				} catch (Exception ex) {
					return -1;
				}
			}
			set { lblPSNType.Attributes["PSNL_Type_Code"] = Convert.ToInt32(value).ToString(); }
		}

		public string FN_CODE {
			get {
				try {
					return lblPSNPos.Attributes["FN_CODE"];
				} catch (Exception ex) {
					return "";
				}
			}
			set { lblPSNPos.Attributes["FN_CODE"] = value.ToString (); }
		}

		public string FN_Type {
			get {
				try {
					return lblPSNType.Attributes["FN_Type"];
				} catch (Exception ex) {
					return "";
				}
			}
			set { lblPSNType.Attributes["FN_Type"] = value.ToString (); }
		}

		public string FN_ID {
			get {
				try {
					return FN_CODE;
				} catch (Exception ex) {
					return "";
				}
			}
		}

		//------------------- For Behavior Setting-------------
		public int COMP_Type_Id {
            get { return Convert.ToInt32(Conversion.Val(txtCOMPTypeID.Text)); }
			set { txtCOMPTypeID.Text = Convert.ToInt32(value).ToString(); }
            
		}

        public int COMP_Type_IdTemp
        {
            get { return Convert.ToInt32(Conversion.Val(txtCOMPTypeID.Text)); }
            set { txtCOMPTypeID.Text = Convert.ToInt32(value).ToString(); }
        }
		public int Master_No {
			get { return Convert.ToInt32(Conversion.Val(txtMasterNo.Text)); }
			set { txtMasterNo.Text = Convert.ToInt32(value).ToString(); }
		}

		public int BHV_No {
			get {
				try {
					return GL.CINT(txtBehaviorNo.Text);
				} catch (Exception ex) {
					return 0;
				}
			}
			set { txtBehaviorNo.Text = value.ToString(); }
		}


        public string GAP_ID
        {
            get { return txtGAPID.Text; }
            set { txtGAPID.Text = value.ToString(); }
        }
		public string GAP_Name {
			get { return txtGAPName.Text; }
			set { txtGAPName.Text = value.ToString (); }
		}


		//txtGAPID

		//--------------------- Get Assessor First--------------------
		public DataTable AssessorList {
            get { return (DataTable)Session["AssessmentSetting_COMP_PSN_Assessor"]; }
			set { Session["AssessmentSetting_COMP_PSN_Assessor"] = value; }
		}

        //--------------------- Get Assessor rptCOMP--------------------
        public DataTable AssessorList_rptCOMP
        {
            get { return (DataTable)Session["rptCOMP_COMP_PSN_Assessor"]; }
            set { Session["rptCOMP_COMP_PSN_Assessor"] = value; }
        }

		public int COMP_Status {
            get { return GL.CINT(ddlCOMPStatus.Items[ddlCOMPStatus.SelectedIndex].Value); }
			set {
				ddlCOMPStatus.SelectedIndexChanged -= ddlCOMPStatus_SelectedIndexChanged;
				ddlCOMPStatus.SelectedIndex = 0;
				for (int i = 0; i <= ddlCOMPStatus.Items.Count - 1; i++) {
                    if (Convert.ToInt32(ddlCOMPStatus.Items[i].Value) == Convert.ToInt32(value)) {
						ddlCOMPStatus.SelectedIndex = i;
					}
				}
				ddlCOMPStatus.SelectedIndexChanged += ddlCOMPStatus_SelectedIndexChanged;
			}
		}

		protected void Page_Load(object sender, System.EventArgs e)
		{
			if ((Session["USER_PSNL_NO"] == null)) {
				ScriptManager.RegisterStartupScript(this.Page, typeof(string), "Alert", "showAlert('กรุณาลงชื่อเข้าใช้ระบบ');", true);
				Response.Redirect("Login.aspx");
				return;
			}


			if (!IsPostBack) {

                // Keep Start Time
                DateTime StartTime = DateTime.Now;

                //------------ Get All Assessor ------------
				BindAllAssessor();
                BindAssessor_rptCOMP();
				//------------ Do it From Team COMP ---------
				BL.BindDDlSector(ddlSector);
				//------------ Remove อยส -----------
				for (int i = 0; i <= ddlSector.Items.Count - 1; i++) {
					if (ddlSector.Items[i].Value == "10") {
						ddlSector.Items.RemoveAt(i);
						break; // TODO: might not be correct. Was : Exit For
					}
				}

				BL.BindDDlYearRound(ddlRound);
                //----------------- ทำสองที่ Page Load กับ Update Status ---------------
				BL.BindDDlCOMPClassGroup(ddl_Search_Class, R_Year, R_Round);
				BindPersonalList();
				pnlList.Visible = true;
				pnlEdit.Visible = false;
				pnlAddAss.Visible = false;

                //Report Time To Entry This Page
                DateTime EndTime = DateTime.Now;
                lblReportTime.Text = "เริ่มเข้าสู่หน้านี้ " + StartTime.ToString("HH:mm:ss.ff") + " ถึง " + EndTime.ToString("HH:mm:ss.ff") + " ใช้เวลา " + Math.Round( GL.DiffTimeDecimalSeconds(StartTime,EndTime),2) + " วินาที";
			}
		}

		protected void Search(object sender, System.EventArgs e)
		{
			BindPersonalList();
		}

		protected void ddlRound_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			BL.BindDDlCOMPClassGroup(ddl_Search_Class, R_Year, R_Round);
			BindPersonalList();
		}

		private void BindAllAssessor()
		{
			string SQL = " SELECT DISTINCT R_Year,R_Round,MGR_PSNL_NO ASSESSOR_CODE,\n";
			SQL += " MGR_PSNL_Fullname ASSESSOR_NAME,PSN_PSNL_NO PSNL_NO\n";
			SQL += " FROM vw_HR_ASSESSOR_PSN\n";
			SqlDataAdapter DA = new SqlDataAdapter(SQL, BL.ConnectionString());
			DA.SelectCommand.CommandTimeout = 90;
			DataTable DT = new DataTable();
			DA.Fill(DT);

			AssessorList = DT;
		}

        private void BindAssessor_rptCOMP()
        {
            string SQL = " ";
            SQL += " SELECT DISTINCT R_Year,R_Round,MGR_PSNL_NO ASSESSOR_CODE,\n";
            SQL += " MGR_PSNL_Fullname ASSESSOR_NAME,PSNL_NO\n";
            SQL += " FROM tb_HR_Actual_Assessor\n";

            SqlDataAdapter DA = new SqlDataAdapter(SQL, BL.ConnectionString());
            DataTable DT = new DataTable();
            DA.Fill(DT);

            AssessorList_rptCOMP = DT;
        }



		private void BindPersonalList()
		{
            string SQL = "SELECT DISTINCT R_Year,R_Round ,vw.SECTOR_CODE,vw.SECTOR_NAME,vw.DEPT_CODE,vw.DEPT_NAME,vw.PSNL_NO,vw.PSNL_Fullname,vw.PNPS_CLASS,vw.WAGE_NAME\n";
			SQL += " ,vw.POS_NO,vw.POS_Name,vw.COMP_Status,vw.COMP_Status_Name\n";
			SQL += " FROM vw_RPT_COMP_Status vw\n";
            SQL += " LEFT JOIN vw_PN_PSNL_ALL PSN ON vw.PSNL_NO=PSN.PSNL_NO \n";
            string Filter = " ";
            //SQL += " WHERE vw.SECTOR_CODE<>'10' AND R_Year=" + R_Year + " AND R_Round=" + R_Round + "\n";
			SQL += " WHERE vw.SECTOR_CODE<>'10' " + "\n";
            SQL += " AND  (ISNULL(PNPS_RESIGN_DATE,PNPS_RETIRE_DATE) > (SELECT MAX(R_End) FROM tb_HR_Round WHERE  R_Year=" + R_Year + " AND R_Round=" + R_Round  + ")) \n";
			if (ddlSector.SelectedIndex > 0) {

				SQL += " AND ( vw.SECTOR_CODE='" + ddlSector.Items[ddlSector.SelectedIndex].Value + "' \n";
				if (ddlSector.Items[ddlSector.SelectedIndex].Value == "3200") {
					SQL += " AND  vw.DEPT_CODE NOT IN ('32000300'))    \n";
				//-----------ฝ่ายโรงงานผลิตยาสูบ 3
				} else if (ddlSector.Items[ddlSector.SelectedIndex].Value == "3203") {
					SQL += " OR  vw.DEPT_CODE IN ('32000300','32030000','32030100','32030200','32030300','32030500'))  \n";
				//-----------ฝ่ายโรงงานผลิตยาสูบ 4
				} else if (ddlSector.Items[ddlSector.SelectedIndex].Value == "3204") {
					SQL += " OR  vw.DEPT_CODE IN ('32040000','32040100','32040200','32040300','32040500','32040300'))  \n";
				//-----------ฝ่ายโรงงานผลิตยาสูบ 5
				} else if (ddlSector.Items[ddlSector.SelectedIndex].Value == "3205") {
					SQL += " OR  vw.DEPT_CODE IN ('32050000','32050100','32050200','32050300','32050500'))\t  \n";
				} else {
					SQL += ")   \n";
				}

			}
			if (!string.IsNullOrEmpty(txt_Search_Name.Text)) {
				SQL += " AND (vw.PSNL_NO LIKE '%" + txt_Search_Name.Text.Replace("'", "''") + "%'\n";
				SQL += " OR vw.PSNL_Fullname LIKE '%" + txt_Search_Name.Text.Replace("'", "''") + "%'\n";
				SQL += " OR vw.POS_Name LIKE '%" + txt_Search_Name.Text.Replace("'", "''") + "%')\n";
			}
			if (!string.IsNullOrEmpty(txt_Search_Organize.Text)) {
				SQL += " AND (vw.SECTOR_NAME LIKE '%" + txt_Search_Organize.Text.Replace("'", "''") + "%' OR \n";
				SQL += " vw.DEPT_NAME LIKE '%" + txt_Search_Organize.Text.Replace("'", "''") + "%')\n";
			}
			if (ddl_Search_Class.SelectedIndex > 0) {
				SQL += " AND vw.CLSGP_ID=" + ddl_Search_Class.Items[ddl_Search_Class.SelectedIndex].Value + "\n";
			}

			switch (ddlStatus.Items[ddlStatus.SelectedIndex].Value) {
				case "-1":
					SQL += "";
					break;
				case "0":
					SQL += " AND ISNULL(vw.COMP_Status,0) IN (0,-1) \n";
					break;
				case "1":
				case "2":
				case "3":
				case "4":
				case "5":
					SQL += " AND vw.COMP_Status =" + ddlStatus.Items[ddlStatus.SelectedIndex].Value + "\n";
					break;
			}

			SQL += "ORDER BY vw.SECTOR_CODE,vw.DEPT_CODE,vw.PNPS_CLASS DESC,vw.POS_NO\n";

           

			SqlDataAdapter DA = new SqlDataAdapter(SQL, BL.ConnectionString());
			DA.SelectCommand.CommandTimeout = 90;
			DataTable DT = new DataTable();
			DA.Fill(DT);
            DT.DefaultView.RowFilter = " R_Year=" + R_Year + " AND R_Round=" + R_Round ;
            Session["Setting_Personal"] = DT.DefaultView.ToTable();
			Pager.SesssionSourceName = "Setting_Personal";
			Pager.RenderLayout();

            if (DT.DefaultView.Count == 0)
            {
				lblCountPSN.Text = "ไม่พบรายการดังกล่าว";
			} else {
                lblCountPSN.Text = "พบ " + GL.StringFormatNumber(DT.DefaultView.Count, 0) + " รายการ";
			}

		}

		protected void Pager_PageChanging(PageNavigation Sender)
		{
			Pager.TheRepeater = rptCOMP;
		}

		protected void rptCOMP_ItemCommand(object source, System.Web.UI.WebControls.RepeaterCommandEventArgs e)
		{
			switch (e.CommandName) {
				case "Edit":
                    Button btnPSNEdit = (Button)e.Item.FindControl("btnPSNEdit");
					//-------------- ดึงข้อมูลบุคคล ------------
					PSNL_NO = btnPSNEdit.CommandArgument;
					COMP_Status = BL.GetCOMPStatus(R_Year, R_Round, PSNL_NO);

                    SaveHeader();
                    BindPersonal();
                    First_AVG_Weight();
					BindAssessor();

					BindMasterCOMP();
					pnlList.Visible = false;
					pnlEdit.Visible = true;

					SetPrintButton();
					break;
			}
		}

		private void SetPrintButton()
		{
			btnCOMPFormPDF.HRef = "Print/RPT_C_PNSAss.aspx?MODE=PDF&R_Year=" + R_Year + "&R_Round=" + R_Round + "&PSNL_No=" + PSNL_NO + "&Status=" + COMP_Status;
            btnCOMPFormExcel.HRef = "Print/RPT_C_PNSAss.aspx?MODE=EXCEL&R_Year=" + R_Year + "&R_Round=" + R_Round + "&PSNL_No=" + PSNL_NO + "&Status=" + COMP_Status;

            btnIDPFormPDF.HRef = "Print/RPT_IDP_PSN.aspx?MODE=PDF&R_Year=" + R_Year + "&R_Round=" + R_Round + "&PSNL_No=" + PSNL_NO + "&Status=" + COMP_Status ;
			btnIDPFormExcel.HRef = "Print/RPT_IDP_PSN.aspx?MODE=EXCEL&R_Year=" + R_Year + "&R_Round=" + R_Round + "&PSNL_No=" + PSNL_NO + "&Status=" + COMP_Status;

            btnIDPResultFormPDF.HRef = "Print/RPT_IDP_PSN_Result.aspx?MODE=PDF&R_Year=" + R_Year + "&R_Round=" + R_Round + "&PSNL_No=" + PSNL_NO + "&Status=" + COMP_Status;
            btnIDPResultFormExcel.HRef = "Print/RPT_IDP_PSN_Result.aspx?MODE=EXCEL&R_Year=" + R_Year + "&R_Round=" + R_Round + "&PSNL_No=" + PSNL_NO + "&Status=" + COMP_Status;

		}

		//------------ For Grouping -----------
		string LastSECTOR = "";
		string LastDEPT = "";
        //DataTable ASS = AssessorList_rptCOMP(); 

		protected void rptCOMP_ItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
		{
			if (e.Item.ItemType != ListItemType.AlternatingItem & e.Item.ItemType != ListItemType.Item)
				return;

            Label lnkPSNSector = (Label)e.Item.FindControl("lnkPSNSector");
            Label lnkPSNDept = (Label)e.Item.FindControl("lnkPSNDept");
            Label lblPSNName = (Label)e.Item.FindControl("lblPSNName");
            Label lblPSNPos = (Label)e.Item.FindControl("lblPSNPos");
            Label lblPSNClass = (Label)e.Item.FindControl("lblPSNClass");
            Label lblPSNType = (Label)e.Item.FindControl("lblPSNType");
            Label lblPSNASSESSOR = (Label)e.Item.FindControl("lblPSNASSESSOR");
            Label lblPSNCOMPStatus = (Label)e.Item.FindControl("lblPSNCOMPStatus");
            Button btnPSNEdit = (Button)e.Item.FindControl("btnPSNEdit");

            DataRowView drv = (DataRowView)e.Item.DataItem;

			if (LastSECTOR != drv["SECTOR_NAME"].ToString()) {
				lnkPSNSector.Text = drv["SECTOR_NAME"].ToString();
				LastSECTOR = drv["SECTOR_NAME"].ToString();
				lnkPSNDept.Text = drv["DEPT_NAME"].ToString();
				LastDEPT = drv["DEPT_NAME"].ToString();
			} else if (LastDEPT != drv["DEPT_NAME"].ToString()) {
				lnkPSNDept.Text = drv["DEPT_NAME"].ToString();
				LastDEPT = drv["DEPT_NAME"].ToString();
			}

			lblPSNName.Text = drv["PSNL_NO"] + " : " + drv["PSNL_Fullname"];
			lblPSNPos.Text = drv["POS_Name"].ToString();
			if (!GL.IsEqualNull(drv["PNPS_CLASS"])) {
				lblPSNClass.Text = GL.CINT (drv["PNPS_CLASS"]).ToString();
			} else {
				lblPSNClass.Text = "-";
			}
			lblPSNType.Text =drv["WAGE_NAME"].ToString();

			//lblPSNASSESSOR
            //DataTable ASS = AssessorList.Copy();

            DataTable ASS = AssessorList_rptCOMP.Copy(); 

			string Filter = "(R_Year IS NULL OR R_Year=" + R_Year + ") AND (R_Round IS NULL OR R_Round=" + R_Round + ")";
			Filter += " AND PSNL_NO='" + drv["PSNL_NO"].ToString().Replace("'", "''") + "' AND ASSESSOR_CODE<>'" + drv["PSNL_NO"].ToString().Replace("'", "''") + "'";
			ASS.DefaultView.RowFilter = Filter;

            if (ASS.DefaultView.Count == 0)
            {
                ASS = AssessorList.Copy();
                ASS.DefaultView.RowFilter = Filter;
            }

			string tmp = "";
			for (int i = 0; i <= ASS.DefaultView.Count - 1; i++) {
				tmp += ASS.DefaultView[i]["ASSESSOR_NAME"] + "<br>";
			}
			lblPSNASSESSOR.Text = tmp;


			lblPSNCOMPStatus.Text = drv["COMP_Status_Name"].ToString();


			
            if (GL.IsEqualNull(drv["COMP_Status"]) || GL.CINT(drv["COMP_Status"]) == HRBL.AssessmentStatus.Creating)
            {
				lblPSNCOMPStatus.ForeColor = System.Drawing.Color.Red;
			//----1
            }
            else if (GL.IsEqualNull(drv["COMP_Status"]) || GL.CINT(drv["COMP_Status"]) == HRBL.AssessmentStatus.WaitCreatingApproved)
            {
				lblPSNCOMPStatus.ForeColor = System.Drawing.Color.DarkRed;
			//----2
            }
            else if (GL.IsEqualNull(drv["COMP_Status"]) || GL.CINT(drv["COMP_Status"]) == HRBL.AssessmentStatus.CreatedApproved)
            {
				lblPSNCOMPStatus.ForeColor = System.Drawing.Color.Orange;
			//----3
            }
            else if (GL.IsEqualNull(drv["COMP_Status"]) || GL.CINT(drv["COMP_Status"]) == HRBL.AssessmentStatus.WaitAssessment)
            {
				lblPSNCOMPStatus.ForeColor = System.Drawing.Color.DarkBlue;
			//----4
            }
            else if (GL.IsEqualNull(drv["COMP_Status"]) || GL.CINT(drv["COMP_Status"]) == HRBL.AssessmentStatus.WaitConfirmAssessment)
            {
				lblPSNCOMPStatus.ForeColor = System.Drawing.Color.BlueViolet;
			//----5
            }
            else if (GL.IsEqualNull(drv["COMP_Status"]) || GL.CINT(drv["COMP_Status"]) == HRBL.AssessmentStatus.AssessmentCompleted)
            {
				lblPSNCOMPStatus.ForeColor = System.Drawing.Color.Green;
			}
			btnPSNEdit.CommandArgument = drv["PSNL_NO"].ToString();

		}

		private void BindPersonal()
		{
			//----------Personal Info ----------
			HRBL.PersonalInfo PSNInfo = BL.GetAssessmentPersonalInfo(R_Year, R_Round, PSNL_NO, COMP_Status);
			lblPSNName.Text = PSNInfo.PSNL_Fullname;
			lblPSNDept.Text = PSNInfo.DEPT_NAME;
			if (!string.IsNullOrEmpty(PSNInfo.MGR_NAME)) {
				lblPSNPos.Text = PSNInfo.MGR_NAME;
			} else {
				lblPSNPos.Text = PSNInfo.FN_NAME;
			}
			lblPSNType.Text = "พนักงาน " + PSNInfo.WAGE_NAME + " ระดับ " + Convert.ToInt32(PSNInfo.PNPS_CLASS);

			PSNL_CLASS_GROUP = GL.CINT ( PSNInfo.CLSGP_ID);
			PSNL_TYPE = GL.CINT (PSNInfo.PSNL_TYPE);
			FN_CODE = PSNInfo.FN_ID;
			//FN_CODE = PSNInfo.FN_CODE
			FN_Type = PSNInfo.FN_Type;
		}

		private void BindAssessor()
		{
			BL.BindDDLAssessor(ddlASSName, R_Year, R_Round, PSNL_NO);
			ddlASSName_SelectedIndexChanged(null, null);
			//----------- Lock ------------
			ddlASSName.Enabled = !BL.Is_Round_Completed(R_Year, R_Round);
		}

		protected void ddlASSName_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			lblASSPos.Text = ASSESSOR_POS;
			lblASSDept.Text = ASSESSOR_DEPT;
			SaveHeader();
		}

		private void BindMasterCOMP()
		{
			lblRound.Text = ddlRound.Items[ddlRound.SelectedIndex].Text;
			//---------------- Set Status -------------------
			COMP_Status = BL.GetCOMPStatus(R_Year, R_Round, PSNL_NO);
			//--------------- Bind Detail----------------------
			DataTable DT = BL.GetCOMPBundle(PSNL_NO, R_Year, R_Round, PSNL_CLASS_GROUP, PSNL_TYPE, FN_ID, FN_Type, HRBL.CompetencyType.All);
			Behavior = BL.GetCOMPBehavior(PSNL_NO, R_Year, R_Round).Table;

			//--------------- Calculate Final Score -------------
			DT.Columns.Add("FinalScore", typeof(Int32));
			for (int i = 0; i <= DT.Rows.Count - 1; i++) {
				int TotalBehavior = GL.CINT ( Behavior.Compute("COUNT(Master_No)", "COMP_Type_Id=" + DT.Rows[i]["COMP_Type_Id"] + " AND Master_No=" + DT.Rows[i]["Master_No"]));
				int SelectedChoice = 0;
				if (!GL.IsEqualNull(DT.Rows[i]["COMP_Answer_MGR"]))
					SelectedChoice =GL.CINT ( DT.Rows[i]["COMP_Answer_MGR"]);
				if (SelectedChoice >= 3) {
					DT.Rows[i]["FinalScore"] = (TotalBehavior >= SelectedChoice ? SelectedChoice : TotalBehavior);
				} else {
					DT.Rows[i]["FinalScore"] = SelectedChoice;
				}
			}

			//---------- Get GAP Data------------
			GAPData = BL.GetGAPDetailPSN(PSNL_NO, HRBL.CompetencyType.All, R_Year, R_Round);

			rptAss.DataSource = DT;
			rptAss.DataBind();

			SaveHeader();
		}

		protected void rptAss_ItemCommand(object source, System.Web.UI.WebControls.RepeaterCommandEventArgs e)
		{

            HtmlTableCell ass_no =(HtmlTableCell)  e.Item.FindControl("ass_no");
			HtmlTableCell COMP_Name =(HtmlTableCell)  e.Item.FindControl("COMP_Name");
			HtmlTableCell target =(HtmlTableCell) e.Item.FindControl("target");
			TextBox txtWeight =(TextBox) e.Item.FindControl("txtWeight");
            string SQL = "";
            Button btn = (Button)e.CommandSource;
            DataTable DT;
            SqlCommandBuilder cmd;
            SqlDataAdapter DA;

            int YearRound = (R_Year * 10) + R_Round;
            int _COMP_Type_Id = GL.CINT(ass_no.Attributes["COMP_Type_Id"]);
            int _Master_No = GL.CINT(ass_no.InnerHtml);
            
			switch (e.CommandName) {
				case "Weight":

					SaveHeader();

					SQL = "SELECT * FROM tb_HR_COMP_Detail \n";
					SQL += " WHERE PSNL_NO='" + PSNL_NO.Replace("'", "''") + "' ";
					SQL += " AND R_Year=" + R_Year + "\n";
					SQL += " AND R_Round=" + R_Round + "\n";
                    SQL += " AND COMP_Type_Id=" + _COMP_Type_Id + "\n";
					SQL += " AND Master_No=" + ass_no.InnerHtml + "\n";

					 DA = new SqlDataAdapter(SQL, BL.ConnectionString());
					 DT = new DataTable();
					DA.Fill(DT);

					DataRow DR = null;
					if (DT.Rows.Count == 0) {
						DR = DT.NewRow();
						DR["PSNL_NO"] = PSNL_NO;
						DR["R_Year"] = R_Year;
						DR["R_Round"] = R_Round;
                        DR["COMP_Type_Id"] = _COMP_Type_Id;
                        DR["Master_No"] = _Master_No;
						for (int i = 1; i <= 5; i++) {
							HtmlTableCell choice =(HtmlTableCell) e.Item.FindControl("choice" + i);
							DR["COMP_Choice_" + i] = choice.InnerHtml;
						}
						DR["COMP_Comp"] = COMP_Name.InnerHtml;
						DR["COMP_Std"] = target.InnerHtml;
						DR["Create_By"] = Session["USER_PSNL_NO"];
						DR["Create_Time"] = DateAndTime.Now;
					} else {
						DR = DT.Rows[0];
					}
					//-------------- Must Update This -----------
					if (string.IsNullOrEmpty(txtWeight.Text)) {
						DR["COMP_Weight"] = DBNull.Value;
					} else {
						DR["COMP_Weight"] = Conversion.Val(txtWeight.Text);
					}
					DR["Update_By"] = Session["USER_PSNL_NO"];
					DR["Update_Time"] = DateAndTime.Now;

					if (DT.Rows.Count == 0)
						DT.Rows.Add(DR);
					 cmd = new SqlCommandBuilder(DA);
					DA.Update(DT);

					//------------- Refresh-----------
					BindMasterCOMP();

					break;
				case "SelectOff":

					SaveHeader();
                    				
					SQL = "SELECT * FROM tb_HR_COMP_Detail \n";
					SQL += " WHERE PSNL_NO='" + PSNL_NO.Replace("'", "''") + "' ";
					SQL += " AND R_Year=" + R_Year + "\n";
					SQL += " AND R_Round=" + R_Round + "\n";
                    SQL += " AND COMP_Type_Id=" + _COMP_Type_Id + "\n";
                    SQL += " AND Master_No=" + _Master_No + "\n";

					DA = new SqlDataAdapter(SQL, BL.ConnectionString());
					DT = new DataTable();
					DA.Fill(DT);

                    int SelectedChoice_Off = Convert.ToInt32(btn.ID.Replace("btnOff_", ""));

					 DR = null;
					if (DT.Rows.Count == 0) {
						DR = DT.NewRow();
						DR["PSNL_NO"] = PSNL_NO;
						DR["R_Year"] = R_Year;
						DR["R_Round"] = R_Round;
                        DR["COMP_Type_Id"] = _COMP_Type_Id;
						DR["Master_No"] = ass_no.InnerHtml;
						for (int i = 1; i <= 5; i++) {
							HtmlTableCell choice =(HtmlTableCell) e.Item.FindControl("choice" + i);
							DR["COMP_Choice_" + i] = choice.InnerHtml;
						}
						if (string.IsNullOrEmpty(txtWeight.Text)) {
							DR["COMP_Weight"] = DBNull.Value;
						} else {
							DR["COMP_Weight"] = Conversion.Val(txtWeight.Text);
						}
						DR["COMP_Comp"] = COMP_Name.InnerHtml;
						DR["COMP_Std"] = target.InnerHtml;
						DR["Create_By"] = Session["USER_PSNL_NO"];
						DR["Create_Time"] = DateAndTime.Now;
					} else {
						DR = DT.Rows[0];
					}
					//-------------- Must Update This -----------
					DR["COMP_Answer_PSN"] = SelectedChoice_Off;
					DR["Update_By"] = Session["USER_PSNL_NO"];
					DR["Update_Time"] = DateAndTime.Now;

					if (DT.Rows.Count == 0)
						DT.Rows.Add(DR);
					cmd = new SqlCommandBuilder(DA);
					DA.Update(DT);

                    //----------Show/Hide Behavior Auto Add Behavior--------------
                    if (YearRound >= BL.Start_Skip_Behavior_Round())
                    {
                        BL.AutoAddBehavior(PSNL_NO, R_Year, R_Round, _COMP_Type_Id, _Master_No, 5);
                    }
					//------------- Refresh-----------
					BindMasterCOMP();

					break;
				case "SelectHead":

					SaveHeader();


                    int SelectedChoice_SelectHead = GL.CINT(btn.ID.Replace("btnHead_", ""));
					
					 SQL = "SELECT * FROM tb_HR_COMP_Detail \n";
					SQL += " WHERE PSNL_NO='" + PSNL_NO.Replace("'", "''") + "' ";
					SQL += " AND R_Year=" + R_Year + "\n";
					SQL += " AND R_Round=" + R_Round + "\n";
                    SQL += " AND COMP_Type_Id=" + _COMP_Type_Id + "\n";
                    SQL += " AND Master_No=" + _Master_No + "\n";

					 DA = new SqlDataAdapter(SQL, BL.ConnectionString());
					 DT = new DataTable();
					DA.Fill(DT);

					 DR = null;
					if (DT.Rows.Count == 0) {
						DR = DT.NewRow();
						DR["PSNL_NO"] = PSNL_NO;
						DR["R_Year"] = R_Year;
						DR["R_Round"] = R_Round;
                        DR["COMP_Type_Id"] = _COMP_Type_Id;
                        DR["Master_No"] = _Master_No;
						for (int i = 1; i <= 5; i++) {
							HtmlTableCell choice =(HtmlTableCell) e.Item.FindControl("choice" + i);
							DR["COMP_Choice_" + i] = choice.InnerHtml;
						}
						if (string.IsNullOrEmpty(txtWeight.Text)) {
							DR["COMP_Weight"] = DBNull.Value;
						} else {
							DR["COMP_Weight"] = Conversion.Val(txtWeight.Text);
						}
						DR["COMP_Comp"] = COMP_Name.InnerHtml;
						DR["COMP_Std"] = target.InnerHtml;
						DR["Create_By"] = Session["USER_PSNL_NO"];
						DR["Create_Time"] = DateAndTime.Now;
					} else {
						DR = DT.Rows[0];
					}

					//-------------- Must Update This -----------
                    DR["COMP_Answer_MGR"] = SelectedChoice_SelectHead;
					DR["Update_By"] = Session["USER_PSNL_NO"];
					DR["Update_Time"] = DateAndTime.Now;

					if (DT.Rows.Count == 0)
						DT.Rows.Add(DR);
					 cmd = new SqlCommandBuilder(DA);
					DA.Update(DT);

                    //----------Show/Hide Behavior Auto Add Behavior--------------
                    if (YearRound >= BL.Start_Skip_Behavior_Round())
                    {
                        BL.AutoAddBehavior(PSNL_NO, R_Year, R_Round, _COMP_Type_Id, _Master_No, SelectedChoice_SelectHead);
                    }
					//------------- Refresh-----------
					BindMasterCOMP();

					break;
				case "AddBehavior":
					Button btnAddBehavior =(Button) e.Item.FindControl("btnAddBehavior");
                    COMP_Type_Id = Convert.ToInt32(ass_no.Attributes["COMP_Type_Id"]);
                    COMP_Type_IdTemp = Convert.ToInt32(ass_no.Attributes["COMP_Type_Id"]);
                    Master_No = Convert.ToInt32(ass_no.InnerHtml);
					BHV_No = Convert.ToInt32(btnAddBehavior.CommandArgument) + 1;

                    //lblBehaviorNo.Text = txtBehaviorNo.Text;
                    //txtBehavior.Text = "";
                    //ModalBehavior.Visible = true;
                    //ScriptManager.RegisterStartupScript(this.Page, typeof(string), "focus", "document.getElementById('" + txtBehavior.ClientID + "').focus();", true);


                    btnBehaviorDialog_Click(null, null);
					                    break;
				case "Remark_MGR":

					SaveHeader();

					
					TextBox txtMGR =(TextBox) e.Item.FindControl("txtMGR");
                    SQL = "";
					 SQL = "SELECT * FROM tb_HR_COMP_Detail \n";
					SQL += " WHERE PSNL_NO='" + PSNL_NO.Replace("'", "''") + "' ";
					SQL += " AND R_Year=" + R_Year + "\n";
					SQL += " AND R_Round=" + R_Round + "\n";
					SQL += " AND COMP_Type_Id=" + ass_no.Attributes["COMP_Type_Id"] + "\n";
					SQL += " AND Master_No=" + ass_no.InnerHtml + "\n";

					 DA = new SqlDataAdapter(SQL, BL.ConnectionString());
					 DT = new DataTable();
					DA.Fill(DT);

					 DR = null;
					if (DT.Rows.Count == 0) {
						DR = DT.NewRow();
						DR["PSNL_NO"] = PSNL_NO;
						DR["R_Year"] = R_Year;
						DR["R_Round"] = R_Round;
						DR["COMP_Type_Id"] = ass_no.Attributes["COMP_Type_Id"];
						DR["Master_No"] = ass_no.InnerHtml;
						for (int i = 1; i <= 5; i++) {
							HtmlTableCell choice =(HtmlTableCell) e.Item.FindControl("choice" + i);
							DR["COMP_Choice_" + i] = choice.InnerHtml;
						}
						if (string.IsNullOrEmpty(txtWeight.Text)) {
							DR["COMP_Weight"] = DBNull.Value;
						} else {
							DR["COMP_Weight"] = Conversion.Val(txtWeight.Text);
						}
						DR["COMP_Comp"] = COMP_Name.InnerHtml;
						DR["COMP_Std"] = target.InnerHtml;
						DR["Create_By"] = Session["USER_PSNL_NO"];
						DR["Create_Time"] = DateAndTime.Now;
					} else {
						DR = DT.Rows[0];
					}

					//-------------- Must Update This -----------

                    if (txtMGR.Text != "")
                    {
                        if (txtMGR.Text.Length > 3990)
                        {
                            txtMGR.Text = txtMGR.Text.Substring(0, 3990);
                        }

                    }

					DR["COMP_Remark_MGR"] = txtMGR.Text;
					DR["Update_By"] = Session["USER_PSNL_NO"];
					DR["Update_Time"] = DateAndTime.Now;

					if (DT.Rows.Count == 0)
						DT.Rows.Add(DR);
					 cmd = new SqlCommandBuilder(DA);
					DA.Update(DT);

					//------------- Refresh-----------
					BindMasterCOMP();

					break;
				case "Remark_PSN":
	            TextBox txtPSN =(TextBox) e.Item.FindControl("txtPSN");
                if (txtPSN.Text != "")
               

                    SaveHeader();

					
				

                     SQL = "SELECT * FROM tb_HR_COMP_Detail \n";
                    SQL += " WHERE PSNL_NO='" + PSNL_NO.Replace("'", "''") + "' ";
                    SQL += " AND R_Year=" + R_Year + "\n";
                    SQL += " AND R_Round=" + R_Round + "\n";
                    SQL += " AND COMP_Type_Id=" + ass_no.Attributes["COMP_Type_Id"] + "\n";
                    SQL += " AND Master_No=" + ass_no.InnerHtml + "\n";

                     DA = new SqlDataAdapter(SQL, BL.ConnectionString());
                     DT = new DataTable();
                    DA.Fill(DT);

                     DR = null;
                    if (DT.Rows.Count == 0) {
                        DR = DT.NewRow();
                        DR["PSNL_NO"] = PSNL_NO;
                        DR["R_Year"] = R_Year;
                        DR["R_Round"] = R_Round;
                        DR["COMP_Type_Id"] = ass_no.Attributes["COMP_Type_Id"];
                        DR["Master_No"] = ass_no.InnerHtml;
                        for (int i = 1; i <= 5; i++) {
                            HtmlTableCell choice =(HtmlTableCell) e.Item.FindControl("choice" + i);
                            DR["COMP_Choice_" + i] = choice.InnerHtml;
                        }
                        if (string.IsNullOrEmpty(txtWeight.Text)) {
                            DR["COMP_Weight"] = DBNull.Value;
                        } else {
                            DR["COMP_Weight"] = Conversion.Val(txtWeight.Text);
                        }
                        DR["COMP_Comp"] = COMP_Name.InnerHtml;
                        DR["COMP_Std"] = target.InnerHtml;
                        DR["Create_By"] = Session["USER_PSNL_NO"];
                        DR["Create_Time"] = DateAndTime.Now;
                    } else {
                        DR = DT.Rows[0];
                    }

                    //-------------- Must Update This -----------
                    if (txtPSN.Text != "")
                    {
                        if (txtPSN.Text.Length > 3990)
                        {
                            txtPSN.Text = txtPSN.Text.Substring(0, 3990);
                        }

                    }
                    DR["COMP_Remark_PSN"] = txtPSN.Text;
                    DR["Update_By"] = Session["USER_PSNL_NO"];
                    DR["Update_Time"] = DateAndTime.Now;

                    if (DT.Rows.Count == 0)
                        DT.Rows.Add(DR);
                     cmd = new SqlCommandBuilder(DA);
                    DA.Update(DT);

                    //------------- Refresh-----------
                    BindMasterCOMP();

					break;
				case "Delete":
					SaveHeader();

					
					 SQL = "DELETE FROM tb_HR_COMP_Behavior \n";
					SQL += " WHERE PSNL_NO='" + PSNL_NO.Replace("'", "''") + "' ";
					SQL += " AND R_Year=" + R_Year + "\n";
					SQL += " AND R_Round=" + R_Round + "\n";
					SQL += " AND COMP_Type_Id=" + ass_no.Attributes["COMP_Type_Id"] + "\n";
					SQL += " AND Master_No=" + ass_no.InnerHtml + "\n\n";


					SQL += " UPDATE tb_HR_COMP_Detail \n";
					SQL += " SET \n";
					SQL += " COMP_Answer_PSN=NULL, \n";
					SQL += " COMP_Answer_MGR=NULL, \n";
					SQL += " COMP_Remark_PSN=NULL, \n";
					SQL += " COMP_Remark_MGR=NULL \n\n";
					SQL += " WHERE PSNL_NO='" + PSNL_NO.Replace("'", "''") + "' ";
					SQL += " AND R_Year=" + R_Year + "\n";
					SQL += " AND R_Round=" + R_Round + "\n";
					SQL += " AND COMP_Type_Id=" + ass_no.Attributes["COMP_Type_Id"] + "\n";
					SQL += " AND Master_No=" + ass_no.InnerHtml + "\n\n";


					SqlConnection Conn = new SqlConnection(BL.ConnectionString());
					Conn.Open();
					SqlCommand Comm = new SqlCommand();
					var _with1 = Comm;
					_with1.Connection = Conn;
					_with1.CommandType = CommandType.Text;
					_with1.CommandText = SQL;
					_with1.ExecuteNonQuery();
					_with1.Dispose();
					Conn.Close();
					Conn.Dispose();

					//------------- Refresh-----------
					BindMasterCOMP();

					break;

                case "AddGAP":

                    int GAP_COMP_Type = GL.CINT(ass_no.Attributes["COMP_Type_Id"]);
                    int GAP_Master_No = GL.CINT(ass_no.InnerHtml);
                  
                    BindGAPList(GAP_COMP_Type,GAP_Master_No );
                    ModalGAP.Visible  = true;
                    break;

			}
		}

		int LastType = 0;

		DataTable Behavior = null;
		//----------------- To Bind Behavior ---------------
		protected void rptAss_ItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
		{
			switch (e.Item.ItemType) {

				case ListItemType.AlternatingItem:
				case ListItemType.Item:

						HtmlTableRow row_COMP_type =(HtmlTableRow) e.Item.FindControl("row_COMP_type");
					HtmlTableCell cell_COMP_type =(HtmlTableCell) e.Item.FindControl("cell_COMP_type");

					HtmlTableCell ass_no =(HtmlTableCell)  e.Item.FindControl("ass_no");
					HtmlTableCell COMP_Name =(HtmlTableCell)  e.Item.FindControl("COMP_Name");
					HtmlTableCell target =(HtmlTableCell) e.Item.FindControl("target");
					HtmlTableCell cel_weight =(HtmlTableCell)  e.Item.FindControl("cel_weight");
					TextBox txtWeight =(TextBox) e.Item.FindControl("txtWeight");
					Button btnWeight =(Button) e.Item.FindControl("btnWeight");

					HtmlTableCell cel_officer =(HtmlTableCell) e.Item.FindControl("cel_officer");
					HtmlTableCell cel_header =(HtmlTableCell) e.Item.FindControl("cel_header");
					HtmlTableCell mark =(HtmlTableCell) e.Item.FindControl("mark");
					//--------------- Textbox -------------
					TextBox txtPSN =(TextBox) e.Item.FindControl("txtPSN");
					TextBox txtMGR =(TextBox) e.Item.FindControl("txtMGR");
					Button btnRemark_PSN =(Button) e.Item.FindControl("btnRemark_PSN");
					Button btnRemark_MGR =(Button) e.Item.FindControl("btnRemark_MGR");

					//-------------------- Notification Handler ------------------
					Button btn_Cal_Bhv = (Button)e.Item.FindControl("btn_Cal_Bhv");
					Image imgAlert =(Image) e.Item.FindControl("imgAlert");
					Label lblAlert =(Label) e.Item.FindControl("lblAlert");

                    DataRowView drv = (DataRowView)e.Item.DataItem;
					ass_no.Attributes["COMP_Type_Id"] =GL.CINT ( drv["COMP_Type_Id"]).ToString ();
					if (Convert.ToInt32(LastType) != GL.CINT (drv["COMP_Type_Id"])) {
						switch (GL.CINT (drv["COMP_Type_Id"])) {
							case 1:
								cell_COMP_type.InnerHtml = "สมรรถนะหลัก (Core Competency)";
								break;
							case 2:
								cell_COMP_type.InnerHtml = "สมรรถนะตามสายงาน (Functional Competency)";
								break;
							case 3:
								cell_COMP_type.InnerHtml = "สมรรถนะตามสายระดับ (Managerial Competency)";
								break;
						}
						row_COMP_type.Visible = true;
						LastType = GL.CINT (drv["COMP_Type_Id"]);
					} else {
						row_COMP_type.Visible = false;
					}


                    ass_no.InnerHtml = drv["Master_No"].ToString();
                    //ass_no.Attributes["Master_No"] = GL.CINT(drv["Master_No"]).ToString();
					COMP_Name.InnerHtml = drv["COMP_Comp"].ToString().Replace("\n", "<br>");
					target.InnerHtml = drv["COMP_Std"].ToString().Replace("\n", "<br>");

					//---------------------- Weight --------------------------
					cel_weight.Attributes["onclick"] = "document.getElementById('" + txtWeight.ClientID + "').focus();";
					if (!GL.IsEqualNull(drv["COMP_Weight"])) {
						txtWeight.Text = GL.StringFormatNumber  (drv["COMP_Weight"]);
                        if (GL.CDBL (drv["COMP_Weight"]) > 100 | GL.CDBL (drv["COMP_Weight"]) <= 0) {
                            cel_weight.Style["background-color"] = "#f8f8f8";
                            txtWeight.Style["color"] = "black";
                        } else {
                            cel_weight.Style["background-color"] = "#f8f8f8";
                            txtWeight.Style["color"] = "black";
                        }
					} else {
                        cel_weight.Style["background-color"] = "#f8f8f8";
                        txtWeight.Style["color"] = "black";
					}


					TC.ImplementJavaFloatText(txtWeight);
					txtWeight.Style["text-align"] = "center";
					txtWeight.Attributes["onchange"] = "document.getElementById('" + btnWeight.ClientID + "').click();";
                    txtWeight.Style["color"] = "black";

					//---------------------- Remark --------------------------
					txtPSN.Text = drv["COMP_Remark_PSN"].ToString();
					txtMGR.Text = drv["COMP_Remark_MGR"].ToString();

					txtMGR.Attributes["onchange"] = "document.getElementById('" + btnRemark_MGR.ClientID + "').click();";
					txtPSN.Attributes["onchange"] = "document.getElementById('" + btnRemark_PSN.ClientID + "').click();";

					for (int i = 1; i <= 5; i++) {
						HtmlTableCell choice =(HtmlTableCell) e.Item.FindControl("choice" + i);
						choice.InnerHtml = drv["COMP_Choice_" + i].ToString().Replace("\n", "<br>");

						HtmlTableCell selOfficialColor =(HtmlTableCell) e.Item.FindControl("selOfficialColor" + i);
						Button btnOff =(Button) e.Item.FindControl("btnOff_" + i);
						selOfficialColor.Attributes["onclick"] = "document.getElementById('" + btnOff.ClientID + "').click();";

						HtmlTableCell selHeaderColor =(HtmlTableCell) e.Item.FindControl("selHeaderColor" + i);
						Button btnHead =(Button) e.Item.FindControl("btnHead_" + i);
						selHeaderColor.Attributes["onclick"] = "document.getElementById('" + btnHead.ClientID + "').click();";
					}


					//---------------------- Display PSN Answer For Suggession ------------------------
					if (!GL.IsEqualNull(drv["COMP_Answer_PSN"]) && Information.IsNumeric(drv["COMP_Answer_PSN"]) && GL.CINT(drv["COMP_Answer_PSN"]) != 0) {
						cel_officer.Attributes["class"] = "AssLevel" + drv["COMP_Answer_PSN"];
						for (int j = 1; j <= GL.CINT ( drv["COMP_Answer_PSN"]); j++) {
							HtmlTableCell _selOfficialColor =(HtmlTableCell) e.Item.FindControl("selOfficialColor" + j);
							_selOfficialColor.Attributes["class"] = "AssLevel" + drv["COMP_Answer_PSN"];
						}
						cel_officer.Attributes["COMP_Answer_PSN"] = GL.CINT (drv["COMP_Answer_PSN"]).ToString ();
					}

                    //------------------ Show/Hide Behavior-------------------
                    HtmlTableCell tdBehavior1 = (HtmlTableCell)e.Item.FindControl("tdBehavior1");
                    HtmlTableCell tdBehavior2 = (HtmlTableCell)e.Item.FindControl("tdBehavior2");
                    HtmlTableCell tdGAP1 = (HtmlTableCell)e.Item.FindControl("tdGAP1");
                    HtmlTableCell tdGAP2 = (HtmlTableCell)e.Item.FindControl("tdGAP2");
                    int YearRound = (R_Year * 10) + R_Round;
                    if (YearRound >= BL.Start_Skip_Behavior_Round())
                    {
                        tdBehavior1.Visible = false;
                        tdBehavior2.Visible = false;
                        tdGAP1.ColSpan = 2;
                        tdGAP2.ColSpan = 5;
                    }
                    else
                    {
                        tdBehavior1.Visible = true;
                        tdBehavior2.Visible = true;
                        tdGAP1.ColSpan = 1;
                        tdGAP2.ColSpan = 2;
                    }

					//---------------------- Actual Mark PSN Answer For Suggession ------------------------
					if (!GL.IsEqualNull(drv["COMP_Answer_MGR"]) && Information.IsNumeric(drv["COMP_Answer_MGR"]) && GL.CINT (drv["COMP_Answer_MGR"]) != 0) {
						cel_header.Attributes["class"] = "AssLevel" + drv["COMP_Answer_MGR"];
						//weight.Attributes("class") = "AssLevel" & drv["COMP_Answer_MGR"]
						COMP_Name.Attributes["class"] = "TextLevel" + drv["COMP_Answer_MGR"];
						target.Attributes["class"] = "TextLevel" + drv["COMP_Answer_MGR"];
						mark.Attributes["class"] = "AssLevel" + drv["COMP_Answer_MGR"];
						//------------------------------------------------------------------------------
                        cel_weight.Style.Remove("background-color");
						cel_weight.Style.Remove("color");
						txtWeight.Style.Remove("color");
						cel_weight.Attributes["class"] = "AssLevel" + drv["COMP_Answer_MGR"];
						txtWeight.Attributes["class"] = "AssLevel" + drv["COMP_Answer_MGR"];

						HtmlTableCell _choice =(HtmlTableCell) e.Item.FindControl("choice" + drv["COMP_Answer_MGR"]);
						_choice.Attributes["class"] = "TextLevel" + drv["COMP_Answer_MGR"];

						for (int j = 1; j <= GL.CINT (drv["COMP_Answer_MGR"]); j++) {
							HtmlTableCell _selHeaderColor =(HtmlTableCell) e.Item.FindControl("selHeaderColor" + j);
							_selHeaderColor.Attributes["class"] = "AssLevel" + drv["COMP_Answer_MGR"];
						}
						cel_officer.Attributes["COMP_Answer_MGR"] = GL.CINT (drv["COMP_Answer_MGR"]).ToString ();

						//------------- Add Wink Feature--------------
                        if (GL.CINT(drv["FinalScore"]) < GL.CINT(drv["COMP_Answer_MGR"]) & YearRound < BL.Start_Skip_Behavior_Round())
                        {
							mark.Attributes["class"] += " winkLevel" + drv["COMP_Answer_MGR"];
							cel_weight.Attributes["class"] += " winkLevel" + drv["COMP_Answer_MGR"];
							txtWeight.Attributes["class"] += " winkLevel" + drv["COMP_Answer_MGR"];
							mark.Attributes["title"] = "พฤติกรรมบ่งชี้น้อยกว่าคะแนนประเมิน";
							cel_weight.Attributes["title"] = mark.Attributes["title"];
							txtWeight.Attributes["title"] = mark.Attributes["title"];
							imgAlert.Visible = true;
							lblAlert.Text = "ต้องระบุพฤติกรรมบ่งชี้ " + drv["COMP_Answer_MGR"] + " ข้อ";
						}
						//------------- End Wink Feature--------------
					}

					//-------------- Report Score----------
					mark.InnerHtml = GL.CDBL ( drv["FinalScore"]).ToString ();

					//--------------- Behavior ------------
					HtmlTableCell cell_Add_Behavior =(HtmlTableCell) e.Item.FindControl("cell_Add_Behavior");
					Button btnAddBehavior =(Button) e.Item.FindControl("btnAddBehavior");
					Repeater rptBehavior =(Repeater) e.Item.FindControl("rptBehavior");

					rptBehavior.ItemDataBound += rptBehavior_ItemDataBound;
					Behavior.DefaultView.RowFilter = "COMP_Type_Id=" + drv["COMP_Type_Id"] + " AND Master_No=" + drv["Master_No"];
					rptBehavior.DataSource = Behavior.DefaultView.ToTable().Copy();
					rptBehavior.DataBind();

						btnAddBehavior.CommandArgument = GL.CINT( Behavior.DefaultView.Count).ToString ();
					//-----  จำนวนพฤติกรรมบ่งชี้ที่มี -----

					//--------------- Delete Action---------------
					HtmlTableCell cel_delete =(HtmlTableCell) e.Item.FindControl("cel_delete");
					Button btn_Delete =(Button) e.Item.FindControl("btn_Delete");
					AjaxControlToolkit.ConfirmButtonExtender cfm_Delete =(AjaxControlToolkit.ConfirmButtonExtender) e.Item.FindControl("cfm_Delete");
					cel_delete.Attributes["onclick"] = "document.getElementById('" + btn_Delete.ClientID + "').click();";
					cfm_Delete.ConfirmText = "ยืนยันลบผลการประเมินรายการที่ " + ass_no.InnerHtml;


					//--------------- GAP -------------------------
					GAPData.DefaultView.RowFilter = "COMP_Type_Id=" + drv["COMP_Type_Id"] + " AND Master_No=" + drv["Master_No"] + " AND Selected=1"  ;
					DataTable GAP = GAPData.DefaultView.ToTable();
					Repeater rptGAP =(Repeater) e.Item.FindControl("rptGAP");
					rptGAP.ItemDataBound += rptGAP_ItemDataBound;
					rptGAP.DataSource = GAP;
					rptGAP.DataBind();

					break;



				case ListItemType.Footer:
					//----------------Report Summary ------------

					//----------- Control Difinition ------------
					HtmlTableCell cell_sum_mark =(HtmlTableCell) e.Item.FindControl("cell_sum_mark");
					HtmlTableCell cell_sum_weight =(HtmlTableCell) e.Item.FindControl("cell_sum_weight");
					HtmlTableCell cell_total =(HtmlTableCell) e.Item.FindControl("cell_total");
					Label lblDone =(Label) e.Item.FindControl("lblDone");
					Label lblTotalItem =(Label) e.Item.FindControl("lblTotalItem");
					HtmlTableCell cell_sum_raw =(HtmlTableCell) e.Item.FindControl("cell_sum_raw");

					//------------ Report -------------
					DataTable DT =(DataTable) rptAss.DataSource;

					int TotalItem = DT.Rows.Count;
					int AssCount = GL.CINT ( DT.Compute("COUNT(COMP_No)", "COMP_Answer_MGR>0"));

					if (TotalItem == 0) {
						cell_total.InnerHtml = "ไม่พบหัวข้อในแบบประเมิน";
					} else if (AssCount == 0) {
						cell_total.InnerHtml = "ยังไม่ได้ประเมิน";
					} else if (AssCount == TotalItem) {
						cell_total.InnerHtml = "ประเมินครบ " + TotalItem + " ข้อแล้ว";
					} else {
						cell_total.InnerHtml = "ประเมินแล้ว " + AssCount + " ข้อจากทั้งหมด " + TotalItem + " ข้อ";
					}

					for (int i = 1; i <= 5; i++) {
						HtmlTableCell cell_sum =(HtmlTableCell) e.Item.FindControl("cell_sum_" + i);
						int cnt =GL.CINT ( DT.Compute("COUNT(COMP_Answer_MGR)", "COMP_Answer_MGR=" + i).ToString());
						if (cnt == 0) {
							cell_sum.InnerHtml = "-";
						} else {
							cell_sum.InnerHtml = "ได้ " + DT.Compute("COUNT(COMP_Answer_MGR)", "COMP_Answer_MGR=" + i).ToString() + " ข้อ";
							cell_sum.Attributes["class"] = "AssLevel" + i;
						}
					}


					if (TotalItem != 0) {
						DT.Columns.Add("WEIGHT", typeof(double));

						for (int i = 0; i <= DT.Rows.Count - 1; i++) {
							if (!GL.IsEqualNull(DT.Rows[i]["COMP_Weight"]) && Information.IsNumeric(DT.Rows[i]["COMP_Weight"].ToString().Replace("%", ""))) {
								DT.Rows[i]["WEIGHT"] = DT.Rows[i]["COMP_Weight"].ToString().Replace("%", "");
							}
						}
						DT.Columns.Add("MARKxWEIGHT", typeof(double), "FinalScore*WEIGHT");
						DT.Columns.Add("WEIGHTx5", typeof(double), "WEIGHT*5");

						object _SUM = DT.Compute("SUM(MARKxWEIGHT)", "");
						object _TOTAL = DT.Compute("SUM(WEIGHTx5)", "");

						if (!GL.IsEqualNull(_SUM) & !GL.IsEqualNull(_TOTAL)) {
							double Result = Convert.ToDouble(_SUM) * 100 / Convert.ToDouble(_TOTAL);
							if (Strings.Len(Result) > 5)
								Result = GL.CDBL ( GL.StringFormatNumber(Result));
							cell_sum_mark.InnerHtml = Result + "%";
							cell_sum_raw.InnerHtml = (Result * 5) + "/500";

							if (Result <= 20) {
								cell_sum_mark.Attributes["class"] = "AssLevel1";
								cell_sum_raw.Attributes["class"] = "AssLevel1";
							} else if (Result <= 40) {
								cell_sum_mark.Attributes["class"] = "AssLevel2";
								cell_sum_raw.Attributes["class"] = "AssLevel2";
							} else if (Result <= 60) {
								cell_sum_mark.Attributes["class"] = "AssLevel3";
								cell_sum_raw.Attributes["class"] = "AssLevel3";
							} else if (Result <= 80) {
								cell_sum_mark.Attributes["class"] = "AssLevel4";
								cell_sum_raw.Attributes["class"] = "AssLevel4";
							} else {
								cell_sum_mark.Attributes["class"] = "AssLevel5";
								cell_sum_raw.Attributes["class"] = "AssLevel5";
							}
						} else {
							cell_sum_mark.Attributes["class"] = "";
							cell_sum_mark.InnerHtml = "0%";
							cell_sum_raw.InnerHtml = "0/500";
						}
					}
					break;
			}


		}

		DataTable GAPData = null;
		protected void rptGAP_ItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
		{
			if (e.Item.ItemType != ListItemType.AlternatingItem & e.Item.ItemType != ListItemType.Item)
				return;

			HtmlAnchor aGAP =(HtmlAnchor) e.Item.FindControl("aGAP");
			HtmlImage imgSelect =(HtmlImage) e.Item.FindControl("imgSelect");
			Label lblGAP =(Label) e.Item.FindControl("lblGAP");
            DataRowView drv = (DataRowView)e.Item.DataItem;

			switch ( GL.CINT(drv["Selected"])) {
				case 1:
					imgSelect.Src = "images/check.png";
					break;
				case 0:
					imgSelect.Src = "images/nocheck.png";
					break;
			}
            
			HtmlTableCell ass_no =(HtmlTableCell) e.Item.Parent.Parent.FindControl("ass_no");
            
			lblGAP.Text = drv["GAP_Name"].ToString ();

            ////-------------------------------- toggleGAP Click Event---------------------------------------
            //string Script = "document.getElementById('" + txtCOMPTypeID.ClientID + "').value=" + ass_no.Attributes["COMP_Type_Id"] + ";";
            //Script += "document.getElementById('" + txtMasterNo.ClientID + "').value=" + ass_no.InnerHtml + ";";
            //Script += "document.getElementById('" + txtGAPID.ClientID + "').value='" + drv["GAP_ID"] + "';";
            //Script += "document.getElementById('" + txtGAPName.ClientID + "').value='" + drv["GAP_Name"].ToString().Replace("'", "\\'") + "';";
            //Script += "document.getElementById('" + btnToggleGAP.ClientID + "').click();";
            //aGAP.Attributes["onclick"] = Script;
		}


		protected void btnToggleGAP_Click(object sender, System.EventArgs e)
		{
			string SQL = " SELECT * FROM tb_HR_COMP_GAP \n";
			SQL += " WHERE R_Year = " + R_Year + " And R_Round =" + R_Round + "\n";
			SQL += " AND PSNL_NO='" + PSNL_NO.Replace("'", "''") + "' AND COMP_Type_Id=" + Convert.ToInt32(COMP_Type_Id) + "\n";
			SQL += " AND Master_No=" + Master_No + " AND GAP_ID='" + GAP_ID + "'\n";
			SqlDataAdapter DA = new SqlDataAdapter(SQL, BL.ConnectionString());
			DataTable DT = new DataTable();
			DA.Fill(DT);
			string GAP = GAP_ID;
			if (DT.Rows.Count == 0) {
				DataRow DR = DT.NewRow();
				DR["PSNL_NO"] = PSNL_NO;
				DR["R_Year"] = R_Year;
				DR["R_Round"] = R_Round;
                DR["COMP_Type_Id"] = Convert.ToInt32(COMP_Type_Id);
				DR["Master_No"] = Master_No;
				DR["GAP_ID"] = GAP_ID;
				DR["PSNL_TYPE"] = PSNL_TYPE;
				DR["CLSGP_ID"] = PSNL_CLASS_GROUP;
				DR["FN_ID"] = FN_ID;
				DR["FN_TYPE"] = FN_Type;
				DR["GAP_Name"] = GAP_Name;
				DR["Update_By"] = Session["USER_PSNL_NO"];
				DR["Update_Time"] = DateAndTime.Now;
				DT.Rows.Add(DR);
				SqlCommandBuilder cmd = new SqlCommandBuilder(DA);
				try {
					DA.Update(DT);
				} catch {
				}
			} else {
				DT.Rows[0].Delete();
				SqlCommandBuilder cmd = new SqlCommandBuilder(DA);
				try {
					DA.Update(DT);
				} catch {
				}
			}

			ModalBehavior.Visible = false;
            ModalGAP.Visible = true;
            BindGAPList(COMP_Type_Id ,Master_No );
			BindMasterCOMP();

		}

		protected void rptBehavior_ItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
		{
			if (e.Item.ItemType != ListItemType.AlternatingItem & e.Item.ItemType != ListItemType.Item)
				return;
            DataRowView drv = (DataRowView)e.Item.DataItem;
			HtmlAnchor aBehavior =(HtmlAnchor) e.Item.FindControl("aBehavior");
			HtmlAnchor aDelete =(HtmlAnchor) e.Item.FindControl("aDelete");
			aBehavior.InnerHtml = drv["BHV_No"].ToString() + " . " + drv["BHV_Content"].ToString();
			if (COMP_Status < HRBL.AssessmentStatus.AssessmentCompleted) {
				//-------------------------------- Add Click Event---------------------------------------
				string Script = "document.getElementById('" + txtCOMPTypeID.ClientID + "').value=" + drv["COMP_Type_Id"] + ";";
				Script += "document.getElementById('" + txtMasterNo.ClientID + "').value=" + drv["Master_No"] + ";";
				Script += "document.getElementById('" + txtBehaviorNo.ClientID + "').value='" + drv["BHV_No"] + "';";
				Script += "document.getElementById('" + btnBehaviorDialog.ClientID + "').click();";
				aBehavior.Attributes["onclick"] = Script;

				Script = "document.getElementById('" + txtCOMPTypeID.ClientID + "').value=" + drv["COMP_Type_Id"] + ";";
				Script += "document.getElementById('" + txtMasterNo.ClientID + "').value=" + drv["Master_No"] + ";";
				Script += "document.getElementById('" + txtBehaviorNo.ClientID + "').value=" + drv["BHV_No"] + ";";
				Script += "document.getElementById('" + btnBehaviorDelete.ClientID + "').click();";
				aDelete.Attributes["onclick"] = Script;
			} else {
				aDelete.Visible = false;
				aBehavior.Style["width"] = "";
				aBehavior.Style["cursor"] = "Default";
			}

		}

		protected void btnBehaviorDialog_Click(object sender, System.EventArgs e)
		{
			lblBehaviorNo.Text = txtBehaviorNo.Text;

			string SQL = "SELECT BHV_Content FROM tb_HR_COMP_Behavior \n";
			SQL += " WHERE PSNL_NO='" + PSNL_NO.Replace("'", "''") + "' ";
			SQL += " AND R_Year=" + R_Year + "\n";
			SQL += " AND R_Round=" + R_Round + "\n";
			SQL += " AND COMP_Type_Id=" + Convert.ToInt32(COMP_Type_Id) + "\n";
			SQL += " AND Master_No=" + Master_No + "\n";
			SQL += " AND BHV_No=" + BHV_No + "\n";

			SqlDataAdapter DA = new SqlDataAdapter(SQL, BL.ConnectionString());
			DataTable DT = new DataTable();
			DA.Fill(DT);

			if (DT.Rows.Count == 0) {
				txtBehavior.Text = "";
			} else {
				txtBehavior.Text = DT.Rows[0]["BHV_Content"].ToString ();
			}
			ModalBehavior.Visible = true;
			ScriptManager.RegisterStartupScript(this.Page, typeof(string), "focus", "document.getElementById('" + txtBehavior.ClientID + "').focus();", true);
		}


		protected void btnBehaviorDelete_Click(object sender, System.EventArgs e)
		{
			string SQL = "SELECT * FROM tb_HR_COMP_Behavior \n";
			SQL += " WHERE PSNL_NO='" + PSNL_NO.Replace("'", "''") + "' ";
			SQL += " AND R_Year=" + R_Year + "\n";
			SQL += " AND R_Round=" + R_Round + "\n";
			SQL += " AND COMP_Type_Id=" + Convert.ToInt32(COMP_Type_Id) + "\n";
			SQL += " AND Master_No=" + Master_No + "\n";

			SqlDataAdapter DA = new SqlDataAdapter(SQL, BL.ConnectionString());
			DataTable DT = new DataTable();
			DA.Fill(DT);
			//--------------- Delete  Target-----
			DT.DefaultView.RowFilter = "BHV_No=" + BHV_No;
			if (DT.DefaultView.Count > 0) {
				DT.DefaultView[0].Row.Delete();
			}
			SqlCommandBuilder cmd = new SqlCommandBuilder(DA);
			try {
				DA.Update(DT);
			} catch {
			}
			//--------------- ReOrder -----------
			for (int i = 0; i <= DT.Rows.Count - 1; i++) {
				DT.Rows[i]["BHV_No"] = i + 1;
			}
			cmd = new SqlCommandBuilder(DA);
			try {
				DA.Update(DT);
			} catch {
			}

			ModalBehavior.Visible = false;
			BindMasterCOMP();

		}

		protected void btnClose_Click(object sender, System.EventArgs e)
		{
			ModalBehavior.Visible = false;
		}

		protected void btnOK_Click(object sender, System.EventArgs e)
		{
			if (string.IsNullOrEmpty(Strings.Trim(txtBehavior.Text))) {
				ScriptManager.RegisterStartupScript(this.Page, typeof(string), "Alert", "showAlert('กรอกรายละเอียด');", true);
				return;
			}

			string SQL = "SELECT * FROM tb_HR_COMP_Behavior \n";
			SQL += " WHERE PSNL_NO='" + PSNL_NO.Replace("'", "''") + "' ";
			SQL += " AND R_Year=" + R_Year + "\n";
			SQL += " AND R_Round=" + R_Round + "\n";
			SQL += " AND COMP_Type_Id=" + Convert.ToInt32(COMP_Type_Id) + "\n";
			SQL += " AND Master_No=" + Master_No + "\n";
			SQL += " AND BHV_No=" + BHV_No + "\n";

			SqlDataAdapter DA = new SqlDataAdapter(SQL, BL.ConnectionString());
			DataTable DT = new DataTable();
			DA.Fill(DT);

			DataRow DR = null;
			if (DT.Rows.Count == 0) {
				DR = DT.NewRow();
				DR["PSNL_NO"] = PSNL_NO;
				DR["R_Year"] = R_Year;
				DR["R_Round"] = R_Round;
				DR["COMP_Type_Id"] = Convert.ToInt32(COMP_Type_Id);
				DR["Master_No"] = Master_No;
				DR["BHV_No"] = BHV_No;
			} else {
				DR = DT.Rows[0];
			}
			DR["BHV_Content"] = txtBehavior.Text;

			if (DT.Rows.Count == 0)
				DT.Rows.Add(DR);
			SqlCommandBuilder cmd = new SqlCommandBuilder(DA);
			try {
				DA.Update(DT);
			} catch (Exception ex) {
			}

			ModalBehavior.Visible = false;
			BindMasterCOMP();

		}

		protected void btnBack_Click(object sender, System.EventArgs e)
		{
			BindAllAssessor();
            BindAssessor_rptCOMP();
			BindPersonalList();
			pnlList.Visible = true;
			pnlEdit.Visible = false;
		}

		protected void ddlCOMPStatus_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			SaveHeader();
			ScriptManager.RegisterStartupScript(this.Page, typeof(string), "Alert", "showAlert('ปรับสถานะการประเมินแล้ว');", true);
		}


		private void SaveHeader()
		{
			//------------- Check Round ---------------
			if (R_Year == 0 | R_Round == 0)
				return;
			//------------- Check Progress-------------

			string SQL = "";
			SQL = " SELECT * \n";
			SQL += "  FROM tb_HR_COMP_Header\n";
			SQL += " WHERE PSNL_NO='" + PSNL_NO.Replace("'", "''") + "' AND R_Year=" + R_Year + " AND R_Round=" + R_Round;
			SqlDataAdapter DA = new SqlDataAdapter(SQL, BL.ConnectionString());
			DataTable DT = new DataTable();
			DA.Fill(DT);

			DataRow DR = null;
			if (DT.Rows.Count == 0) {
				DR = DT.NewRow();
				DR["PSNL_NO"] = PSNL_NO;
				DR["R_Year"] = R_Year;
				DR["R_Round"] = R_Round;
				DR["Create_By"] = Session["USER_PSNL_NO"];
				DR["Create_Time"] = DateAndTime.Now;
				DR["COMP_Status"] = 0;
			} else {
				DR = DT.Rows[0];
			}
			//DR("COMP_Status") = COMP_Status
			if (GL.CINT (COMP_Status) == -1) {
				DR["COMP_Status"] = 0;
			} else {
				DR["COMP_Status"] = COMP_Status;
			}
			//------------- Round Detail------------- 
			SQL = "SELECT * FROM tb_HR_Round ";
			SQL += " WHERE R_Year=" + R_Year + " AND R_Round=" + R_Round;
			DataTable PN = new DataTable();
			SqlDataAdapter PA = new SqlDataAdapter(SQL, BL.ConnectionString());
			PA.Fill(PN);
			DR["R_Start"] = PN.Rows[0]["R_Start"];
			DR["R_End"] = PN.Rows[0]["R_End"];
			DR["R_Remark"] = "";

			//============ป้องกันการ update ตำแหน่ง ณ สิ้นรอบประเมินและปิดรอบประเมิน======
            Boolean IsInPeriod = BL.IsTimeInPeriod(DateTime.Now, R_Year, R_Round);
            Boolean IsRoundCompleted = BL.Is_Round_Completed(R_Year, R_Round);
            Boolean ckUpdate =  IsInPeriod & !IsRoundCompleted;
            if (ckUpdate | DT.Rows.Count == 0 )
            {
				//------------- Personal Detail---------
				HRBL.PersonalInfo PSNInfo = BL.GetAssessmentPersonalInfo(R_Year, R_Round, PSNL_NO, COMP_Status);
				// Replace With New Updated Data If Exists
				if (!string.IsNullOrEmpty(PSNInfo.PSNL_No)) {
					if (!string.IsNullOrEmpty(PSNInfo.PSNL_No))
						DR["PSNL_No"] = PSNInfo.PSNL_No;
					else
						DR["PSNL_No"] = DBNull.Value;
					if (!string.IsNullOrEmpty(PSNInfo.PSNL_Fullname))
						DR["PSNL_Fullname"] = PSNInfo.PSNL_Fullname;
					else
						DR["PSNL_Fullname"] = DBNull.Value;
					if (!string.IsNullOrEmpty(PSNInfo.PNPS_CLASS))
						DR["PNPS_CLASS"] = PSNInfo.PNPS_CLASS;
					else
						DR["PNPS_CLASS"] = DBNull.Value;
					if (!string.IsNullOrEmpty(PSNInfo.PSNL_TYPE))
						DR["PSNL_TYPE"] = PSNInfo.PSNL_TYPE;
					else
						DR["PSNL_TYPE"] = DBNull.Value;
					if (!string.IsNullOrEmpty(PSNInfo.POS_NO))
						DR["POS_NO"] = PSNInfo.POS_NO;
					else
						DR["POS_NO"] = DBNull.Value;
					if (!string.IsNullOrEmpty(PSNInfo.WAGE_TYPE))
						DR["WAGE_TYPE"] = PSNInfo.WAGE_TYPE;
					else
						DR["WAGE_TYPE"] = DBNull.Value;
					if (!string.IsNullOrEmpty(PSNInfo.WAGE_NAME))
						DR["WAGE_NAME"] = PSNInfo.WAGE_NAME;
					else
						DR["WAGE_NAME"] = DBNull.Value;
					if (!string.IsNullOrEmpty(PSNInfo.SECTOR_CODE))
						DR["SECTOR_CODE"] = Strings.Left(PSNInfo.SECTOR_CODE, 2);
					else
						DR["SECTOR_CODE"] = DBNull.Value;
					if (!string.IsNullOrEmpty(PSNInfo.DEPT_CODE))
						DR["DEPT_CODE"] = PSNInfo.DEPT_CODE;
					else
						DR["DEPT_CODE"] = DBNull.Value;
					if (!string.IsNullOrEmpty(PSNInfo.MINOR_CODE))
						DR["MINOR_CODE"] = PSNInfo.MINOR_CODE;
					else
						DR["MINOR_CODE"] = DBNull.Value;
					if (!string.IsNullOrEmpty(PSNInfo.SECTOR_NAME))
						DR["SECTOR_NAME"] = PSNInfo.SECTOR_NAME;
					else
						DR["SECTOR_NAME"] = DBNull.Value;
					if (!string.IsNullOrEmpty(PSNInfo.DEPT_NAME))
						DR["DEPT_NAME"] = PSNInfo.DEPT_NAME;
					else
						DR["DEPT_NAME"] = DBNull.Value;
					if (!string.IsNullOrEmpty(PSNInfo.FN_ID))
						DR["FN_ID"] = PSNInfo.FN_ID;
					else
						DR["FN_ID"] = DBNull.Value;
					if (!string.IsNullOrEmpty(PSNInfo.FLD_Name))
						DR["FLD_Name"] = PSNInfo.FLD_Name;
					else
						DR["FLD_Name"] = DBNull.Value;
					if (!string.IsNullOrEmpty(PSNInfo.FN_CODE))
						DR["FN_CODE"] = PSNInfo.FN_CODE;
					else
						DR["FN_CODE"] = DBNull.Value;
					if (!string.IsNullOrEmpty(PSNInfo.FN_NAME))
						DR["FN_NAME"] = PSNInfo.FN_NAME;
					else
						DR["FN_NAME"] = DBNull.Value;
					if (!string.IsNullOrEmpty(PSNInfo.MGR_CODE))
						DR["MGR_CODE"] = PSNInfo.MGR_CODE;
					else
						DR["MGR_CODE"] = DBNull.Value;
					if (!string.IsNullOrEmpty(PSNInfo.MGR_NAME))
						DR["MGR_NAME"] = PSNInfo.MGR_NAME;
					else
						DR["MGR_NAME"] = DBNull.Value;
				}
			//------------- Assign Assessor Only-----------------------------
			DR["Create_Commit_By"] = ASSESSOR_BY;
			DR["Create_Commit_Name"] = ASSESSOR_NAME;
			DR["Create_Commit_DEPT"] = ASSESSOR_DEPT;
			DR["Create_Commit_POS"] = ASSESSOR_POS;
			DR["Assessment_Commit_By_MGR"] = ASSESSOR_BY;
			DR["Assessment_Commit_Name"] = ASSESSOR_NAME;
			DR["Assessment_Commit_DEPT"] = ASSESSOR_DEPT;
			DR["Assessment_Commit_POS"] = ASSESSOR_POS;

			}

			DR["Update_By"] = Session["USER_PSNL_NO"];
			DR["Update_Time"] = DateAndTime.Now;

			if (DT.Rows.Count == 0)
				DT.Rows.Add(DR);
			SqlCommandBuilder cmd = new SqlCommandBuilder(DA);
			DA.Update(DT);

			//----------- Prevent Save----------
			if (BL.Is_Round_Completed(R_Year, R_Round)) {
				return;
			}

			DataTable TMP = BL.GetAssessorList(R_Year, R_Round, PSNL_NO);
			TMP.DefaultView.RowFilter = "MGR_PSNL_NO='" + ASSESSOR_BY.Replace("'", "''") + "'";

			//----------------- Update Information For History Assessor Structure ------------------
			SQL = "SELECT * ";
			SQL += " FROM tb_HR_Actual_Assessor ";
			SQL += " WHERE PSNL_NO='" + PSNL_NO + "' AND R_Year=" + R_Year + " AND R_Round=" + R_Round;
			DA = new SqlDataAdapter(SQL, BL.ConnectionString());
			DT = new DataTable();
			DA.Fill(DT);

			if (DT.Rows.Count > 0) {
				DT.Rows[0].Delete();
				cmd = new SqlCommandBuilder(DA);
				DA.Update(DT);
			}

			if (TMP.DefaultView.Count > 0) {
				DR = DT.NewRow();
				DR["PSNL_NO"] = PSNL_NO;
				DR["R_Year"] = R_Year;
				DR["R_Round"] = R_Round;
				DR["MGR_ASSESSOR_POS"] = TMP.DefaultView[0]["ASSESSOR_POS"];
				DR["MGR_PSNL_NO"] = TMP.DefaultView[0]["MGR_PSNL_NO"];
				DR["MGR_PSNL_Fullname"] = TMP.DefaultView[0]["MGR_PSNL_Fullname"];
				DR["MGR_PNPS_CLASS"] = TMP.DefaultView[0]["MGR_CLASS"];
				DR["MGR_PSNL_TYPE"] = TMP.DefaultView[0]["MGR_WAGE_TYPE"];
				DR["MGR_POS_NO"] = TMP.DefaultView[0]["MGR_POS_NO"];
				DR["MGR_WAGE_TYPE"] = TMP.DefaultView[0]["MGR_WAGE_TYPE"];
				DR["MGR_WAGE_NAME"] = TMP.DefaultView[0]["MGR_WAGE_NAME"];
				DR["MGR_DEPT_CODE"] = TMP.DefaultView[0]["MGR_DEPT_CODE"];
				//DR("MGR_MINOR_CODE") = TMP.DefaultView(0).Item("MGR_MINOR_CODE")
				DR["MGR_SECTOR_CODE"] = Strings.Left(TMP.DefaultView[0]["MGR_SECTOR_CODE"].ToString (), 2);
				DR["MGR_SECTOR_NAME"] = TMP.DefaultView[0]["MGR_SECTOR_NAME"];
				DR["MGR_DEPT_NAME"] = TMP.DefaultView[0]["MGR_DEPT_NAME"];
				//DR("MGR_FN_ID") = TMP.DefaultView(0).Item("MGR_FN_ID")
				//DR("MGR_FLD_Name") = TMP.DefaultView(0).Item("MGR_FLD_Name")
				DR["MGR_FN_CODE"] = TMP.DefaultView[0]["MGR_FN_CODE"];
				//DR("MGR_FN_TYPE") = TMP.DefaultView(0).Item("MGR_FN_TYPE")
				DR["MGR_FN_NAME"] = TMP.DefaultView[0]["MGR_FN_NAME"];
				if (!GL.IsEqualNull(TMP.DefaultView[0]["MGR_MGR_CODE"])) {
					DR["MGR_MGR_CODE"] = TMP.DefaultView[0]["MGR_MGR_CODE"];
				}
				if (!GL.IsEqualNull(TMP.DefaultView[0]["MGR_MGR_NAME"])) {
					DR["MGR_MGR_NAME"] = TMP.DefaultView[0]["MGR_MGR_NAME"];
				}
				try {
					 DR["MGR_PNPS_RETIRE_DATE"] = (BL.GetPSNRetireDate(TMP.DefaultView[0]["MGR_PSNL_NO"].ToString()));
				} catch {
				}
				DR["Update_By"] = Session["USER_PSNL_NO"];
				DR["Update_Time"] = DateAndTime.Now;
				DT.Rows.Add(DR);
				cmd = new SqlCommandBuilder(DA);
				DA.Update(DT);
			}

			//----------------- Update Information For History PSN Structure ------------------
			DataTable PSN = new DataTable();
			SQL = "SELECT * FROM vw_PN_PSNL WHERE PSNL_NO='" + PSNL_NO.Replace("'", "''") + "'";
			DA = new SqlDataAdapter(SQL, BL.ConnectionString());
			DA.Fill(PSN);

			SQL = "SELECT * ";
			SQL += " FROM tb_HR_Assessment_PSN ";
			SQL += " WHERE PSNL_NO='" + PSNL_NO.Replace("'", "''") + "' AND R_Year=" + R_Year + " AND R_Round=" + R_Round;
			DA = new SqlDataAdapter(SQL, BL.ConnectionString());
			DT = new DataTable();
			DA.Fill(DT);
			if (PSN.Rows.Count > 0) {
				if (DT.Rows.Count == 0) {
					DR = DT.NewRow();
					DR["PSNL_NO"] = PSNL_NO;
					DR["R_Year"] = R_Year;
					DR["R_Round"] = R_Round;
					
					DT.Rows.Add(DR);
				} else {
					DR = DT.Rows[0];
				}

                //============ป้องกันการ update ตำแหน่ง ณ สิ้นรอบประเมินและปิดรอบประเมิน======
                if (ckUpdate | DT.Rows.Count == 0 | string.IsNullOrEmpty(DT.Rows[0]["PSN_PNPS_CLASS"].ToString()))
                {
                    if (TMP.DefaultView.Count > 0) {
						try {
							DR["PSN_ASSESSOR_POS"] = TMP.DefaultView[0]["ASSESSOR_POS"];
						} catch {
						}
					}
					DR["PSN_PSNL_NO"] = PSNL_NO;
					DR["PSN_PSNL_Fullname"] = PSN.Rows[0]["PSNL_Fullname"].ToString();

					DR["PSN_PNPS_CLASS"] = PSN.Rows[0]["PNPS_CLASS"].ToString();
					DR["PSN_PSNL_TYPE"] = PSN.Rows[0]["PSNL_TYPE"].ToString();
					DR["PSN_POS_NO"] = PSN.Rows[0]["POS_NO"].ToString();
					DR["PSN_WAGE_TYPE"] = PSN.Rows[0]["WAGE_TYPE"].ToString();
					DR["PSN_WAGE_NAME"] = PSN.Rows[0]["WAGE_NAME"].ToString();
					DR["PSN_DEPT_CODE"] = PSN.Rows[0]["DEPT_CODE"].ToString();
					DR["PSN_MINOR_CODE"] = PSN.Rows[0]["MINOR_CODE"].ToString();
					DR["PSN_SECTOR_CODE"] = Strings.Left(PSN.Rows[0]["SECTOR_CODE"].ToString(), 2);
					DR["PSN_SECTOR_NAME"] = PSN.Rows[0]["SECTOR_NAME"].ToString();
					DR["PSN_DEPT_NAME"] = PSN.Rows[0]["DEPT_NAME"].ToString();
					DR["PSN_FN_ID"] = PSN.Rows[0]["FN_ID"].ToString();
					DR["PSN_FLD_Name"] = PSN.Rows[0]["FLD_Name"].ToString();
					DR["PSN_FN_CODE"] = PSN.Rows[0]["FN_CODE"].ToString();
					DR["PSN_FN_TYPE"] = PSN.Rows[0]["FN_TYPE"].ToString();
					DR["PSN_FN_NAME"] = PSN.Rows[0]["FN_NAME"].ToString();
					if (!GL.IsEqualNull(PSN.Rows[0]["MGR_CODE"])) {
						DR["PSN_MGR_CODE"] = PSN.Rows[0]["MGR_CODE"];
					}
					if (!GL.IsEqualNull(PSN.Rows[0]["MGR_NAME"])) {
						DR["PSN_MGR_NAME"] = PSN.Rows[0]["MGR_NAME"];
					}
				}

				try {
					DR["PSN_PNPS_RETIRE_DATE"] = BL.GetPSNRetireDate(PSNL_NO);
				} catch {
				}
				DR["Update_By"] = Session["USER_PSNL_NO"];
				DR["Update_Time"] = DateAndTime.Now;
				cmd = new SqlCommandBuilder(DA);
				DA.Update(DT);

			}

		}

		protected void btnBack_TOP_Click(object sender, System.EventArgs e)
		{
			BindPersonalList();
			pnlList.Visible = true;
			pnlEdit.Visible = false;
		}

		protected void btn_Fixed_Assessor_Click(object sender, System.EventArgs e)
		{
			BindFixedAssessor();
			pnlAddAss.Visible = true;
		}

		private void BindFixedAssessor()
		{
			string SQL = "SELECT *,ISNULL(MGR_NAME,FN_Name) POS_NAME FROM vw_PN_PSNL_ALL \n";

			string Filter = "";
			if (!string.IsNullOrEmpty(txt_Search_ASS_POS.Text)) {
				Filter += " (\n";
				Filter += " DEPT_NAME LIKE '%" + txt_Search_ASS_POS.Text.Replace("'", "''") + "%' OR \n";
				Filter += " DEPT_CODE LIKE '%" + txt_Search_ASS_POS.Text.Replace("'", "''") + "%' OR\n";
				Filter += " POS_NO LIKE '%" + txt_Search_ASS_POS.Text.Replace("'", "''") + "%' OR\n";
				Filter += " ISNULL(MGR_NAME,FN_NAME) LIKE '%" + txt_Search_ASS_POS.Text.Replace("'", "''") + "%'\n";
				Filter += " ) AND ";
			}
			if (!string.IsNullOrEmpty(TextBox2.Text)) {
				Filter += " (\n";
				Filter += " PSNL_Fullname LIKE '%" + TextBox2.Text.Replace("'", "''") + "%' OR \n";
				Filter += " PSNL_NO LIKE '%" + TextBox2.Text.Replace("'", "''") + "%'\n";
				Filter += " ) AND ";
			}

			if (!string.IsNullOrEmpty(Filter)) {
				SQL += " WHERE " + Filter.Substring(0, Filter.Length - 4) + "\n";
			}

			SQL += " ORDER BY DEPT_CODE,PNPS_CLASS DESC,POS_NO,PSNL_Fullname\n";
			SqlDataAdapter DA = new SqlDataAdapter(SQL, BL.ConnectionString());
			DataTable DT = new DataTable();
			DA.Fill(DT);

			Session["Setting_KPI_Search_Assessor"] = DT;
			PagerFixedAssessor.SesssionSourceName = "Setting_KPI_Search_Assessor";
			PagerFixedAssessor.RenderLayout();

			if (DT.Rows.Count == 0) {
				lblCountFixedAssessor.Text = "ไม่พบรายการดังกล่าว";
			} else {
				lblCountFixedAssessor.Text = "พบ " + GL.StringFormatNumber(DT.Rows.Count, 0) + " รายการ";
			}
		}

		protected void PagerFixedAssessor_PageChanging(PageNavigation Sender)
		{
			PagerFixedAssessor.TheRepeater = rptFixedAssessor;
		}

		protected void btnCloseFixedAss_Click(object sender, System.EventArgs e)
		{
			pnlAddAss.Visible = false;
		}


		string LastOrgranizeName = "";

		protected void rptFixedAssessor_ItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
		{
			if (e.Item.ItemType != ListItemType.Item & e.Item.ItemType != ListItemType.AlternatingItem)
				return;

			 Label lblPSNPos = (Label)e.Item.FindControl("lblPSNPos");
			 Label lblPSNClass = (Label)e.Item.FindControl("lblPSNClass");
			Label lblPSNDept =(Label) e.Item.FindControl("lblPSNDept");
			Label lblPSNName = (Label)e.Item.FindControl("lblPSNName");
			AjaxControlToolkit.ConfirmButtonExtender cfmbtnSelect =(AjaxControlToolkit.ConfirmButtonExtender) e.Item.FindControl("cfmbtnSelect");
			Button btnSelect =(Button) e.Item.FindControl("btnSelect");

            DataRowView drv = (DataRowView)e.Item.DataItem;
            if (Convert.ToString(drv["DEPT_NAME"]) != LastOrgranizeName)
            {
				lblPSNDept.Text = drv["DEPT_NAME"].ToString ();
				LastOrgranizeName = drv["DEPT_NAME"].ToString ();
			}

			lblPSNName.Text = drv["PSNL_NO"] + " : " + drv["PSNL_Fullname"];
			lblPSNPos.Text = drv["POS_Name"].ToString ();
			lblPSNClass.Text = GL.CINT (drv["PNPS_CLASS"]).ToString ();
			btnSelect.CommandArgument = drv["PSNL_NO"].ToString ();

			cfmbtnSelect.ConfirmText = "ยืนยันระบุ " + drv["PSNL_Fullname"] + " เป็นผู้ประเมิน?";
		}

		protected void rptFixedAssessor_ItemCommand(object source, System.Web.UI.WebControls.RepeaterCommandEventArgs e)
		{
			switch (e.CommandName) {
				case "Select":
					string SQL = "SELECT * FROM vw_PN_PSNL_ALL WHERE PSNL_NO='" + e.CommandArgument.ToString().Replace("'", "''") + "'";
					SqlDataAdapter DA = new SqlDataAdapter(SQL, BL.ConnectionString());
					DataTable PSN = new DataTable();
					DA.Fill(PSN);

					if (PSN.Rows.Count == 0) {
						ScriptManager.RegisterStartupScript(this.Page, typeof(string), "Alert", "alert('ไม่พบรายการที่เลือก\\n\\nกรุณาตรวจสอบข้อมูลอีกครั้ง');", true);
						return;
					}

					SQL = "SELECT * FROM tb_HR_Actual_Assessor \n";
					SQL += " WHERE R_Year=" + R_Year + " AND R_Round=" + R_Round + " AND PSNL_NO='" + PSNL_NO.Replace("'", "''") + "'";
					DA = new SqlDataAdapter(SQL, BL.ConnectionString());
					DataTable DT = new DataTable();
					DA.Fill(DT);

					DataRow DR = null;
					if (DT.Rows.Count == 0) {
						DR = DT.NewRow();
						DR["PSNL_NO"] = PSNL_NO;
						DR["R_Year"] = R_Year;
						DR["R_Round"] = R_Round;
						DT.Rows.Add(DR);
					} else {
						DR = DT.Rows[0];
					}

					DR["MGR_ASSESSOR_POS"] = PSN.Rows[0]["POS_NO"].ToString();
					//--------------- นี่คือตัว Mapping -------------

					DR["MGR_PSNL_NO"] = PSN.Rows[0]["PSNL_NO"].ToString();
					DR["MGR_PSNL_Fullname"] = PSN.Rows[0]["PSNL_Fullname"].ToString();
					DR["MGR_PNPS_CLASS"] = PSN.Rows[0]["PNPS_CLASS"].ToString();
					DR["MGR_PSNL_TYPE"] = PSN.Rows[0]["PSNL_TYPE"].ToString();
					DR["MGR_POS_NO"] = PSN.Rows[0]["POS_NO"].ToString();
					DR["MGR_WAGE_TYPE"] = PSN.Rows[0]["WAGE_TYPE"].ToString();
					DR["MGR_WAGE_NAME"] = PSN.Rows[0]["WAGE_NAME"].ToString();
					DR["MGR_DEPT_CODE"] = PSN.Rows[0]["DEPT_CODE"].ToString();
					DR["MGR_MINOR_CODE"] = PSN.Rows[0]["MINOR_CODE"].ToString();
					DR["MGR_SECTOR_CODE"] = PSN.Rows[0]["SECTOR_CODE"].ToString();
					DR["MGR_SECTOR_NAME"] = PSN.Rows[0]["SECTOR_NAME"].ToString();
					DR["MGR_DEPT_NAME"] = PSN.Rows[0]["DEPT_NAME"].ToString();
					if (!GL.IsEqualNull(PSN.Rows[0]["FN_ID"])) {
						DR["MGR_FN_ID"] = PSN.Rows[0]["FN_ID"];
					} else {
						DR["MGR_FN_ID"] = DBNull.Value;
					}
					if (!GL.IsEqualNull(PSN.Rows[0]["FLD_Name"])) {
						DR["MGR_FLD_Name"] = PSN.Rows[0]["FLD_Name"];
					} else {
						DR["MGR_FLD_Name"] = DBNull.Value;
					}
					if (!GL.IsEqualNull(PSN.Rows[0]["FN_CODE"])) {
						DR["MGR_FN_CODE"] = PSN.Rows[0]["FN_CODE"];
					} else {
						DR["MGR_FN_CODE"] = DBNull.Value;
					}
					if (!GL.IsEqualNull(PSN.Rows[0]["FN_TYPE"])) {
						DR["MGR_FN_TYPE"] = PSN.Rows[0]["FN_TYPE"];
					} else {
						DR["MGR_FN_TYPE"] = DBNull.Value;
					}
					if (!GL.IsEqualNull(PSN.Rows[0]["FN_NAME"])) {
						DR["MGR_FN_NAME"] = PSN.Rows[0]["FN_NAME"];
					} else {
						DR["MGR_FN_NAME"] = DBNull.Value;
					}
					if (!GL.IsEqualNull(PSN.Rows[0]["MGR_CODE"])) {
						DR["MGR_MGR_CODE"] = PSN.Rows[0]["MGR_CODE"];
					} else {
						DR["MGR_MGR_CODE"] = DBNull.Value;
					}
					if (!GL.IsEqualNull(PSN.Rows[0]["MGR_NAME"])) {
						DR["MGR_MGR_NAME"] = PSN.Rows[0]["MGR_NAME"];
					} else {
						DR["MGR_MGR_NAME"] = DBNull.Value;
					}
					if (!GL.IsEqualNull(PSN.Rows[0]["PNPS_RETIRE_DATE"])) {
						DR["MGR_PNPS_RETIRE_DATE"] = PSN.Rows[0]["PNPS_RETIRE_DATE"];
					} else {
						DR["MGR_PNPS_RETIRE_DATE"] = DBNull.Value;
					}

					DR["Update_By"] = Session["USER_PSNL_NO"];
					DR["Update_Time"] = DateAndTime.Now;

					SqlCommandBuilder cmd = new SqlCommandBuilder(DA);
					DA.Update(DT);

					pnlAddAss.Visible = false;
					BindAssessor();

					break;
			}
		}


		//-------------------------เพิ่มปุ่ม เฉลี่ยคะแนน------btnAVG_Weight---------------------------		
        private void First_AVG_Weight()  //เข้ามาครั้งแรก ระบบเฉลี่ยให้ Auto
        {  
            if (COMP_Status == 0 | GL.CINT(COMP_Status) == -1)
            {
                string SQL = "  SELECT COMP_Status \n";
                SQL += " FROM tb_HR_COMP_Header \n";
                SQL += " WHERE R_Year =" + R_Year + " And R_Round =" + R_Round + " AND PSNL_NO='" + PSNL_NO.Replace("'", "''") + "'\n";
                SqlDataAdapter DA = new SqlDataAdapter(SQL, BL.ConnectionString());
                DataTable DT = new DataTable();
                DA.Fill(DT);
                 if ((DT.Rows.Count > 0) || GL.IsEqualNull(DT.Rows[0]["COMP_Status"]))
                {
                    AVG_Weight();  //เฉลี่ย นน.
                }

            }
            
        }
		private void AVG_Weight()
		{
				DataTable DT = BL.GetCOMPBundle(PSNL_NO, R_Year, R_Round, PSNL_CLASS_GROUP, PSNL_TYPE, FN_ID, FN_Type, HRBL.CompetencyType.All);
                DT.DefaultView.Sort = " COMP_Type_Sort DESC ,Master_No DESC";
                DT = DT.DefaultView.ToTable();
				if (DT.Rows.Count > 0) {
                    double AVG_Weight = (GL.CDBL(100) / GL.CDBL(DT.Rows.Count));
                    Double SUM_Weight = 0;
                    Double CDBL_MOD = (GL.CDBL(GL.StringFormatNumber(GL.CDBL(AVG_Weight), 2)) % GL.CDBL(0.50));
                        //------Clere Detail (สำหรับใส่ตาม Master)-------
                        string SQL_Del = "DELETE FROM tb_HR_COMP_Detail ";
                        SQL_Del += " WHERE PSNL_NO='" + PSNL_NO.Replace("'", "''") + "'\n";
                        SQL_Del += " AND R_Year=" + R_Year + "\n";
                        SQL_Del += " AND R_Round=" + R_Round + "\n";
                        SqlDataAdapter DADel = new SqlDataAdapter(SQL_Del, BL.ConnectionString());
                        DataTable DTDel = new DataTable();
                        DADel.Fill(DTDel);

                            //------Save Detail (AVG Weight)-------
							string SQL = "SELECT * FROM tb_HR_COMP_Detail ";
							SQL += " WHERE PSNL_NO='" + PSNL_NO.Replace("'", "''") + "'\n";
							SQL += " AND R_Year=" + R_Year + "\n";
                            SQL += " AND R_Round=" + R_Round + "\n";
                            //SQL += " AND COMP_Type_Id=" + DT.Rows[i]["COMP_Type_Id"] + "\n";
                            //SQL += " AND Master_No=" + DT.Rows[i]["Master_No"] + "\n";

							SqlDataAdapter DA = new SqlDataAdapter(SQL, BL.ConnectionString());
							DataTable DTAVG = new DataTable();
							DA.Fill(DTAVG);

						for (int i = 0; i <= DT.Rows.Count - 1; i++) {
							
							DataRow DR = null;
                            //if (DTAVG.Rows.Count == 0) {
								DR = DTAVG.NewRow();
								DR["PSNL_NO"] = PSNL_NO;
								DR["R_Year"] = R_Year;
								DR["R_Round"] = R_Round;
								DR["COMP_Type_Id"] = DT.Rows[i]["COMP_Type_Id"];
								DR["Master_No"] = DT.Rows[i]["Master_No"];
								DR["Create_By"] = Session["USER_PSNL_NO"];
								DR["Create_Time"] = DateAndTime.Now;
                            //} else {
                            //    DR = DTAVG.Rows[0];
                            //}
							DR["COMP_Comp"] = DT.Rows[i]["COMP_Comp"].ToString();
							DR["COMP_Std"] = DT.Rows[i]["COMP_Std"].ToString();
							for (int j = 1; j <= 5; j++) {
								DR["COMP_Choice_" + j] = DT.Rows[i]["COMP_Choice_" + j];
							}
                            if (GL.CINT(DT.Rows[i]["COMP_No"]) != 1)
                            {
                                if (GL.CDBL(CDBL_MOD) == 0)
                                {
                                    DR["COMP_Weight"] = GL.StringFormatNumber(AVG_Weight);
                                    SUM_Weight += GL.CDBL(GL.StringFormatNumber(AVG_Weight));
                                }
                                else
                                {
                                    DR["COMP_Weight"] = Math.Floor(AVG_Weight);
                                    SUM_Weight += Math.Floor(AVG_Weight);
                                }
                            }
                            else
                            {
                                DR["COMP_Weight"] = (GL.CDBL(100) - GL.CDBL(SUM_Weight));
                            }
							DR["Update_By"] = Session["USER_PSNL_NO"];
							DR["Update_Time"] = DateAndTime.Now;
                            //if (DTAVG.Rows.Count == 0)
								DTAVG.Rows.Add(DR);

						}
							SqlCommandBuilder cmd = new SqlCommandBuilder(DA);
							DA.Update(DTAVG);

					} else {
						return;
					}
		}



		protected void btnAVG_Weight_Click(object sender, System.EventArgs e)
		{
            AVG_Weight();            
			BindMasterCOMP();
		}

        //----------------------------Selece GAP--------------------------------------
        private void BindGAPList(int GAP_COMP_Type_Id, int GAP_Master_No)
        {
           
            //--------------- GAP -------------------------
            COMP_Type_Id = GL.CINT(GAP_COMP_Type_Id);
            Master_No = GL.CINT(GAP_Master_No);
            //---------- Get GAP Data------------
            GAPData = BL.GetGAPDetailPSN(PSNL_NO, HRBL.CompetencyType.All, R_Year, R_Round);
            GAPData.DefaultView.RowFilter = "COMP_Type_Id=" + GAP_COMP_Type_Id + " AND Master_No=" + GAP_Master_No;
            DataTable GAP = GAPData.DefaultView.ToTable();
            
            Session["AssessmentSetting_COMP_PSN_GAP"] = GAP;
            PagerGAP.SesssionSourceName = "AssessmentSetting_COMP_PSN_GAP";
            PagerGAP.RenderLayout();
            if (GAP.Rows.Count == 0)
            {
                lblCountGAP.Text = "ไม่พบรายการดังกล่าว";
            }
            else
            {
                lblCountGAP.Text = "พบ " + GL.StringFormatNumber(GAP.Rows.Count.ToString(), 0) + " รายการ";
            }

            GAP.DefaultView.RowFilter = "Selected=1" ;
            DataTable GAP_Selected = GAP.DefaultView.ToTable();
            if (GAP.Rows.Count == GAP_Selected.DefaultView.Count)
            { 
                 btnGAPAll .ImageUrl = "images/check.png";
            }else {
                btnGAPAll.ImageUrl = "images/none.png";
            }


        }

        protected void PagerGAP_PageChanging(PageNavigation Sender)
        {
            PagerGAP.TheRepeater = rptGAPDialog;
        }

        protected void rptGAPDialog_ItemCommand(object source, System.Web.UI.WebControls.RepeaterCommandEventArgs e)
        {
            Label lblNo = (Label)e.Item.FindControl("lblNo");
            Label lblName = (Label)e.Item.FindControl("lblName");
            

                    switch (e.CommandName)
                    {
                        case "Select":
                            ImageButton btn = (ImageButton)e.Item.FindControl("btnEditGAP");

                            switch (btn.ImageUrl)
                            {
                                case "images/check.png":
                                    btn.ImageUrl = "images/none.png";
                                    btnGAPAll.ImageUrl = "images/none.png";
                                    break;
                                default:
                                    btn.ImageUrl = "images/check.png";
                                    break;
                            }

                            break;

                    }

        }

        protected void rptGAPDialog_ItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType != ListItemType.AlternatingItem & e.Item.ItemType != ListItemType.Item)
                return;
            Label lblNo = (Label)e.Item.FindControl("lblNo");
            Label lblName = (Label)e.Item.FindControl("lblName");
            ImageButton btnEditGAP = (ImageButton)e.Item.FindControl("btnEditGAP");

            DataRowView drv = (DataRowView)e.Item.DataItem;
            lblNo.Text = drv["GAP_ID"].ToString().PadLeft(3, GL.chr0);
            lblName.Text = drv["GAP_Name"].ToString();

            if ((GL.CINT(drv["Selected"])) == 1)
            {
                btnEditGAP.ImageUrl = "images/check.png";
            }
            else
            {
                btnEditGAP.ImageUrl = "images/none.png";
            }
        }

        protected void btnModalGAP_Close_ServerClick(object sender, System.EventArgs e)
        {
            ModalGAP.Visible = false;
        }

        protected void btnGAPAll_Click(object sender, ImageClickEventArgs e)
        {
            ImageButton btn = (ImageButton)sender;
            switch (btn.ImageUrl)
            {
                case "images/check.png":
                    btn.ImageUrl = "images/none.png";
                    SETALL_SelectGAP();
                    break;
                default:
                    btn.ImageUrl = "images/check.png";
                    SETALL_SelectGAP();
                    break;
            }

            
        }

        protected void btnOKGAP_Click(object sender, EventArgs e)
        {
            DataTable DTGAP = new DataTable();
            DTGAP = Current_SelectGAP();            
            // ลบรายการก่อนบันทึกใหม่
             string SQL = " DELETE  FROM tb_HR_COMP_GAP \n";
             SQL += " WHERE R_Year = " + R_Year + " And R_Round =" + R_Round + "\n";
             SQL += " AND PSNL_NO='" + PSNL_NO.Replace("'", "''") + "' AND COMP_Type_Id=" + Convert.ToInt32(COMP_Type_Id) + "\n";
             SQL += " AND Master_No=" + Convert.ToInt32(Master_No) + "\n"; 
             SqlDataAdapter DA = new SqlDataAdapter(SQL, BL.ConnectionString());
             DataTable DT = new DataTable();
             DA.Fill(DT);
             if (DTGAP.Rows.Count > 0)
                 {
                     SQL = " SELECT *  FROM tb_HR_COMP_GAP WHERE 0=1 \n";
                     DA = new SqlDataAdapter(SQL, BL.ConnectionString());
                     DT = new DataTable();
                     DA.Fill(DT);
                     for (int i = 0; i <= DTGAP.Rows.Count - 1; i++)
                     {                      
                     DataRow DR = DT.NewRow();
                     DR["PSNL_NO"] = PSNL_NO;
                     DR["R_Year"] = R_Year;
                     DR["R_Round"] = R_Round;
                     DR["COMP_Type_Id"] = Convert.ToInt32(COMP_Type_Id);
                     DR["Master_No"] = Master_No;
                     DR["GAP_ID"] = DTGAP.Rows[i]["GAPNo"].ToString();
                     DR["PSNL_TYPE"] = PSNL_TYPE;
                     DR["CLSGP_ID"] = PSNL_CLASS_GROUP;
                     DR["FN_ID"] = FN_ID;
                     DR["FN_TYPE"] = FN_Type;
                     DR["GAP_Name"] = DTGAP.Rows[i]["GAPName"].ToString();
                     DR["Update_By"] = Session["USER_PSNL_NO"];
                     DR["Update_Time"] = DateAndTime.Now;
                     DT.Rows.Add(DR);                      
                     }
                     SqlCommandBuilder cmd = new SqlCommandBuilder(DA);
                     DA.Update(DT);                       
                 }
             ModalGAP.Visible = false;
             BindMasterCOMP();
        }

        private DataTable Current_SelectGAP()
        {
            DataTable DT = new DataTable();
            DataRow DR = null;
            DT.Columns.Add("GAPNo", typeof(string));
            DT.Columns.Add("GAPName", typeof(string));
            DT.Columns.Add("Select", typeof(Boolean));

            foreach (RepeaterItem Item in rptGAPDialog.Items)
            {
                ImageButton btnEditGAP = (ImageButton)Item.FindControl("btnEditGAP");
                Label lblNo = (Label)Item.FindControl("lblNo");
                Label lblName = (Label)Item.FindControl("lblName");

                if (btnEditGAP.ImageUrl == "images/check.png")
                {
                    DR = DT.NewRow();
                    DR["GAPNo"] = lblNo.Text;
                    DR["GAPName"] = lblName.Text;
                    DR["Select"] = true;
                    DT.Rows.Add(DR);
                }

            }
            return DT;
        }

        private DataTable SETALL_SelectGAP()
        {
            DataTable DT = new DataTable();
            foreach (RepeaterItem Item in rptGAPDialog.Items)
            {
                ImageButton btnEditGAP = (ImageButton)Item.FindControl("btnEditGAP");
                Label lblNo = (Label)Item.FindControl("lblNo");
                Label lblName = (Label)Item.FindControl("lblName");

                switch (btnGAPAll.ImageUrl)
                {
                    case "images/check.png":
                        btnEditGAP.ImageUrl = "images/check.png";
                        break;
                    default:
                        btnEditGAP.ImageUrl = "images/none.png";
                        break;
                }
            }
            return DT;
        }

        protected void btnCloseGAP_Click(object sender, EventArgs e)
        {
            ModalGAP.Visible = false;

        }

		public AssessmentSetting_COMP_PSN()
		{
			Load += Page_Load;
		}






	}
}

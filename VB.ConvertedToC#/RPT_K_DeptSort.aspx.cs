using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Xml.Linq;
using System.Diagnostics;
using System.Collections.Specialized;
using System.Configuration;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Caching;
using System.Web.SessionState;
using System.Web.Security;
using System.Web.Profile;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
namespace VB
{

	public partial class RPT_K_DeptSort : System.Web.UI.Page
	{


		HRBL BL = new HRBL();
		GenericLib GL = new GenericLib();

		Converter C = new Converter();
		public int R_Year {
			get {
				try {
					return GL.CINT( GL.SplitString(ddlRound.Items[ddlRound.SelectedIndex].Value, "-")[0]);
				} catch (Exception ex) {
					return 0;
				}
			}
		}

		public int R_Round {
			get {
				try {
					return GL.CINT(GL.SplitString(ddlRound.Items[ddlRound.SelectedIndex].Value, "-")[1]);
				} catch (Exception ex) {
					return 0;
				}
			}
		}

		protected void Page_Load(object sender, System.EventArgs e)
		{
			if ((Session["USER_PSNL_NO"] == null)) {
				ScriptManager.RegisterStartupScript(this.Page, typeof(string), "Alert", "showAlert('กรุณาลงชื่อเข้าใช้ระบบ');", true);
				Response.Redirect("Login.aspx");
				return;
			}

			if (!IsPostBack) {
				//----------------- ทำสองที่ Page Load กับ Update Status ---------------
				BL.BindDDlYearRound(ddlRound);
				ddlRound.Items.RemoveAt(0);
				BL.BindDDlSector(ddlSector);
				BL.BindDDlDepartment(ddlDept, ddlSector.SelectedValue);

				BindPersonalList();
			}
		}

        protected void btnSearch_Click(object sender, System.EventArgs e)
		{
			BindPersonalList();
		}

		protected void ddlSector_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			BL.BindDDlDepartment(ddlDept, ddlSector.SelectedValue);
		}

		private void BindPersonalList()
		{
			string SQL = "";
			SQL += " SELECT *" + "\n";
			SQL += " FROM vw_RPT_KPI_Result" + "\n";
			SQL += " WHERE R_Year=" + R_Year + "\n";
			SQL += " AND R_Round=" + R_Round + "\n";

			string Title = ddlRound.Items[ddlRound.SelectedIndex].Text + " ";


			if (ddlSector.SelectedIndex > 0) {
				SQL += " AND ( SECTOR_CODE='" + ddlSector.Items[ddlSector.SelectedIndex].Value + "' " + "\n";
				if (ddlSector.Items[ddlSector.SelectedIndex].Value == "3200") {
					SQL += " AND  DEPT_CODE NOT IN ('32000300'))    " + "\n";
				//-----------ฝ่ายโรงงานผลิตยาสูบ 3
				} else if (ddlSector.Items[ddlSector.SelectedIndex].Value == "3203") {
					SQL += " OR  DEPT_CODE IN ('32000300','32030000','32030100','32030200','32030300','32030500'))  " + "\n";
				//-----------ฝ่ายโรงงานผลิตยาสูบ 4
				} else if (ddlSector.Items[ddlSector.SelectedIndex].Value == "3204") {
					SQL += " OR  DEPT_CODE IN ('32040000','32040100','32040200','32040300','32040500','32040300'))  " + "\n";
				//-----------ฝ่ายโรงงานผลิตยาสูบ 5
				} else if (ddlSector.Items[ddlSector.SelectedIndex].Value == "3205") {
					SQL += " OR  DEPT_CODE IN ('32050000','32050100','32050200','32050300','32050500'))\t  " + "\n";
				} else {
					SQL += ")   " + "\n";
				}

				if (ddlDept.SelectedIndex < 1) {
					Title += GL.SplitString(ddlSector.Items[ddlSector.SelectedIndex].Text, ":")[1].Trim() + " ";
					//----- เลือกแสดงชื่อหน่วยงานเดียว --------
				}
			}
			if (ddlDept.SelectedIndex > 0) {
				SQL += " AND DEPT_CODE = '" + GL.SplitString(ddlDept.SelectedValue, "-")[0] + "' " + "\n";

				Title += ddlDept.Items[ddlDept.SelectedIndex].Text + " ";
				//----- เลือกแสดงชื่อหน่วยงานเดียว --------
			}
			if (!string.IsNullOrEmpty(txtName.Text)) {
				SQL += " AND (PSNL_Fullname LIKE '%" + txtName.Text.Replace("'", "''") + "%' OR PSNL_NO LIKE '%" + txtName.Text.Replace("'", "''") + "%') " + "\n";

				//Title &= " ชื่อ/รหัส '" & txtName.Text & "' " '----- เลือกแสดงชื่อหน่วยงานเดียว --------
			}
			SQL += " ORDER BY SECTOR_CODE,DEPT_CODE,ISNULL(Result,0) DESC,PNPS_CLASS DESC,POS_No";

			SqlDataAdapter DA = new SqlDataAdapter(SQL, BL.ConnectionString());
			DA.SelectCommand.CommandTimeout = 90;
			DataTable DT = new DataTable();
			DA.Fill(DT);

			Session["RPT_K_DeptSort"] = DT;
			//--------- Title --------------
			Session["RPT_K_DeptSort_Title"] = Title;
			//--------- Sub Title ----------
			Session["RPT_K_DeptSort_SubTitle"] = "รายงาน ณ วันที่ " + DateTime.Now.Day + " " + C.ToMonthNameTH(DateTime.Now.Month) + " พ.ศ." + (DateTime.Now.Year + 543);
			Session["RPT_K_DeptSort_SubTitle"] += "  พบ   " + GL.StringFormatNumber(DT.Rows.Count, 0) + "  รายการ";

			Pager.SesssionSourceName = "RPT_K_DeptSort";
			Pager.RenderLayout();

			if (DT.Rows.Count == 0) {
				lblCountPSN.Text = "ไม่พบรายการดังกล่าว";
			} else {
				lblCountPSN.Text = "พบ " + GL.StringFormatNumber(DT.Rows.Count, 0) + " รายการ";
			}
		}

		protected void Pager_PageChanging(PageNavigation Sender)
		{
			Pager.TheRepeater = rptKPI;
		}

		//------------ For Grouping -----------
		string LastSector = "";

		string LastDept = "";
		protected void rptKPI_ItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
		{
			if (e.Item.ItemType != ListItemType.AlternatingItem & e.Item.ItemType != ListItemType.Item)
				return;

            Label lblSector = (Label)e.Item.FindControl("lblSector");
            Label lblDept = (Label)e.Item.FindControl("lblDept");
            Label lblPSNNo = (Label)e.Item.FindControl("lblPSNNo");
            Label lblPSNName = (Label)e.Item.FindControl("lblPSNName");
            Label lblPSNPos = (Label)e.Item.FindControl("lblPSNPos");
            Label lblPSNClass = (Label)e.Item.FindControl("lblPSNClass");
            Label lblScore = (Label)e.Item.FindControl("lblScore");
            HtmlAnchor btnPrint = (HtmlAnchor)e.Item.FindControl("btnPrint");

            DataRowView drv = (DataRowView)e.Item.DataItem;

			if (drv["SECTOR_NAME"].ToString() != LastSector) {
				lblSector.Text = drv["SECTOR_NAME"].ToString();
				LastSector = drv["SECTOR_NAME"].ToString();
				LastDept = "";
				for (int i = 1; i <= 8; i++) {
                    HtmlTableCell td = (HtmlTableCell)e.Item.FindControl("td" + i);
					td.Style["border-top-width"] = "2px";
					td.Style["border-top-color"] = "#999999";
				}
			} else {
                HtmlTableCell td1 = (HtmlTableCell)e.Item.FindControl("td1");
				td1.Style["border-top"] = "none";
			}

			if (drv["DEPT_NAME"].ToString() != LastDept) {
				if (drv["DEPT_NAME"].ToString() != drv["SECTOR_NAME"].ToString()) {
					lblDept.Text = drv["DEPT_NAME"].ToString().Replace(drv["SECTOR_NAME"].ToString(), "").Trim();
				} else {
					lblDept.Text = drv["DEPT_NAME"].ToString();
				}
				LastDept = drv["DEPT_NAME"].ToString();
			} else {
                HtmlTableCell td2 = (HtmlTableCell)e.Item.FindControl("td2");
				td2.Style["border-top"] = "none";
			}
			lblPSNNo.Text = drv["PSNL_NO"].ToString();
			lblPSNName.Text = drv["PSNL_Fullname"].ToString();
			lblPSNPos.Text = drv["POS_Name"].ToString();
			lblPSNClass.Text = GL.CINT(drv["PNPS_CLASS"]).ToString();
			if (!GL.IsEqualNull(drv["RESULT"]) && GL.CDBL(drv["RESULT"]) > 0) {
				lblScore.Text = drv["RESULT"].ToString();
			} else {
				lblScore.Text = "-";
			}

            btnPrint.HRef = "Print/RPT_K_PNSAss.aspx?Mode=PDF&R_Year=" + drv["R_Year"].ToString() + "&R_Round=" + drv["R_Round"].ToString() + "&PSNL_NO=" + drv["PSNL_NO"].ToString() + "&Status=" + drv["ASS_Status"].ToString();
		}
		public RPT_K_DeptSort()
		{
			Load += Page_Load;
		}

	}
}

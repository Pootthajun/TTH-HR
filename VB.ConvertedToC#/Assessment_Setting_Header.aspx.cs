﻿using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Xml.Linq;
using System.Diagnostics;
using System.Collections.Specialized;
using System.Configuration;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Caching;
using System.Web.SessionState;
using System.Web.Security;
using System.Web.Profile;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;

namespace VB
{
    public partial class Assessment_Setting_Header : System.Web.UI.Page
    {

        HRBL BL = new HRBL();
        GenericLib GL = new GenericLib();

        textControlLib TC = new textControlLib();
        public int R_Year
        {
            get
            {
                try
                {
                    return GL.CINT(GL.SplitString(ddlRound.Items[ddlRound.SelectedIndex].Value, "-")[0]);
                }
                catch (Exception ex)
                {
                    return 0;
                }
            }
        }

        public int R_Round
        {
            get
            {
                try
                {
                    return GL.CINT(GL.SplitString(ddlRound.Items[ddlRound.SelectedIndex].Value, "-")[1]);
                }
                catch (Exception ex)
                {
                    return 0;
                }
            }
        }

        public string PSNL_NO
        {
            get { return lblPSNName.Attributes["PSNL_NO"]; }
            set { lblPSNName.Attributes["PSNL_NO"] = value; }
        }

        public string ASSESSOR_BY
        {
            get
            {
                try
                {
                    string[] tmp = Strings.Split(ddlASSName.Items[ddlASSName.SelectedIndex].Value, ":::");
                    return tmp[0];
                }
                catch (Exception ex)
                {
                    return "";
                }
            }
        }

        public string ASSESSOR_NAME
        {
            get
            {
                try
                {
                    return ddlASSName.Items[ddlASSName.SelectedIndex].Text;
                }
                catch (Exception ex)
                {
                    return "";
                }
            }
        }

        public string ASSESSOR_POS
        {
            get
            {
                try
                {
                    string[] tmp = Strings.Split(ddlASSName.Items[ddlASSName.SelectedIndex].Value, ":::");
                    return tmp[1];
                }
                catch (Exception ex)
                {
                    return "";
                }
            }
        }

        public string ASSESSOR_DEPT
        {
            get
            {
                try
                {
                    string[] tmp = Strings.Split(ddlASSName.Items[ddlASSName.SelectedIndex].Value, ":::");
                    return tmp[2];
                }
                catch (Exception ex)
                {
                    return "";
                }
            }
        }


        //--------------------- Get Assessor First--------------------
        public DataTable AssessorList
        {
            get { return (DataTable)Session["AssessmentSetting_KPI_PSN_Assessor"]; }
            set { Session["AssessmentSetting_KPI_PSN_Assessor"] = value; }
        }

        //--------------------- Get Assessor rptKPI--------------------
        public DataTable AssessorList_rptKPI
        {
            get { return (DataTable)Session["rptKPI_KPI_PSN_Assessor"]; }
            set { Session["rptKPI_KPI_PSN_Assessor"] = value; }
        }


        public int KPI_Status
        {
            get { return GL.CINT(ddlKPIStatus.Items[ddlKPIStatus.SelectedIndex].Value); }
            set
            {
                ddlKPIStatus.SelectedIndexChanged -= ddlKPIStatus_SelectedIndexChanged;
                ddlKPIStatus.SelectedIndex = 0;
                for (int i = 0; i <= ddlKPIStatus.Items.Count - 1; i++)
                {
                    if (Convert.ToInt32(ddlKPIStatus.Items[i].Value) == Convert.ToInt32(value))
                    {
                        ddlKPIStatus.SelectedIndex = i;
                    }
                }
                ddlKPIStatus.SelectedIndexChanged += ddlKPIStatus_SelectedIndexChanged;
            }
        }


        public DataTable Temp_tb_HR_KPI_Header
        {
            get { return (DataTable)Session["Temp_tb_HR_KPI_Header"]; }
            set { Session["Temp_tb_HR_KPI_Header"] = value; }
        }
        public DataTable Temp_tb_HR_COMP_Header
        {
            get { return (DataTable)Session["Temp_tb_HR_COMP_Header"]; }
            set { Session["Temp_tb_HR_COMP_Header"] = value; }
        }
        public DataTable Temp_tb_HR_Actual_Assessor
        {
            get { return (DataTable)Session["Temp_tb_HR_Actual_Assessor"]; }
            set { Session["Temp_tb_HR_Actual_Assessor"] = value; }
        }
        public DataTable Temp_tb_HR_Assessment_PSN
        {
            get { return (DataTable)Session["Temp_tb_HR_Assessment_PSN"]; }
            set { Session["Temp_tb_HR_Assessment_PSN"] = value; }
        }


        protected void Page_Load(object sender, EventArgs e)
        {

            //if ((Session["USER_PSNL_NO"] == null))
            //{
            //    ScriptManager.RegisterStartupScript(this.Page, typeof(string), "Alert", "showAlert('กรุณาลงชื่อเข้าใช้ระบบ');", true);
            //    Response.Redirect("Login.aspx");
            //    return;
            //}

            if (!IsPostBack) {
                // Keep Start Time
                DateTime StartTime = DateTime.Now;

                //------------ Get All Assessor ------------
                BindAllAssessor();
                BindAssessor_rptKPI();
                //------------ Do it From Team KPI ---------
                BL.BindDDlSector(ddlSector);
                //------------ Remove อยส -----------
                for (int i = 0; i <= ddlSector.Items.Count - 1; i++)
                {
                    if (ddlSector.Items[i].Value == "10")
                    {
                        ddlSector.Items.RemoveAt(i);
                        break; // TODO: might not be correct. Was : Exit For
                    }
                }
                BindFixedAssessor();

                BL.BindDDlYearRound(ddlRound);
                //----------------- ทำสองที่ Page Load กับ Update Status ---------------
                //BL.Update_KPI_Status_To_Assessment_Period("",R_Year,R_Round);

                BindPersonalList();
                pnlList.Visible = true;
                pnlEdit.Visible = false;
                pnlAddAss.Visible = false;

                //Report Time To Entry This Page
                DateTime EndTime = DateTime.Now;
                lblReportTime.Text = "";
			
            }

        }

        protected void Search(object sender, System.EventArgs e)
        {
            BindPersonalList();
        }

        private void BindAllAssessor()
        {
            string SQL = " SELECT DISTINCT R_Year,R_Round,MGR_PSNL_NO ASSESSOR_CODE,\n";
            SQL += " MGR_PSNL_Fullname ASSESSOR_NAME,PSN_PSNL_NO PSNL_NO\n";
            SQL += " FROM vw_HR_ASSESSOR_PSN\n";
            SqlDataAdapter DA = new SqlDataAdapter(SQL, BL.ConnectionString());
            DA.SelectCommand.CommandTimeout = 90;
            DataTable DT = new DataTable();
            DA.Fill(DT);

            AssessorList = DT;
        }

        private void BindAssessor_rptKPI()
        {
            string SQL = " ";
            SQL += " SELECT DISTINCT R_Year,R_Round,MGR_PSNL_NO ASSESSOR_CODE,\n";
            SQL += " MGR_PSNL_Fullname ASSESSOR_NAME,PSNL_NO\n";
            SQL += " FROM tb_HR_Actual_Assessor\n";

            SqlDataAdapter DA = new SqlDataAdapter(SQL, BL.ConnectionString());
            DataTable DT = new DataTable();
            DA.Fill(DT);

            AssessorList_rptKPI = DT;
        }

        private void BindMasterKPI()
        {
            lblRound.Text = ddlRound.Items[ddlRound.SelectedIndex].Text;
            //---------------- Set Status -------------------
            //KPI_Status = BL.GetKPIStatus(R_Year, R_Round, PSNL_NO);
            //--------------- Bind Detail----------------------
            //HRBL.DataManager DM = BL.GetKPIDetail(R_Year, R_Round, PSNL_NO);
            //DataTable DT = DM.Table;
            //SqlDataAdapter DA = DM.Adaptor;
            //rptAss.DataSource = DM.Table;
            //rptAss.DataBind();

            //SaveHeader();
        }


        private void BindPersonalList()
        {
            string SQL = "SELECT DISTINCT vw.R_Year,vw.R_Round,vw.SECTOR_CODE,vw.SECTOR_NAME,vw.DEPT_CODE,vw.DEPT_NAME,vw.PSNL_NO,vw.PSNL_Fullname,vw.PNPS_CLASS,vw.WAGE_NAME\n";
            SQL += " ,vw.POS_NO,vw.POS_Name,vw.KPI_Status,vw.KPI_Status_Name\n";
            SQL += " FROM vw_RPT_KPI_Status vw\n";
            SQL += " LEFT JOIN vw_PN_PSNL_ALL PSN ON vw.PSNL_NO=PSN.PSNL_NO  \n";
            SQL += " WHERE (ISNULL(PNPS_RESIGN_DATE,PNPS_RETIRE_DATE) > (SELECT MAX(R_End) FROM tb_HR_Round WHERE  R_Year=" + R_Year + " AND R_Round=" + R_Round + ")) \n";


            SQL += "ORDER BY SECTOR_CODE,DEPT_CODE,PNPS_CLASS DESC,POS_NO\n";


            string filter = "SECTOR_CODE<>'10' AND R_Year=" + R_Year + " AND R_Round=" + R_Round + "\n";
            // เปลี่ยนจาก Where ใน View เป็นใช้ Row Filter
            if (ddlSector.SelectedIndex > 0)
            {
                filter += " AND (SECTOR_CODE='" + ddlSector.Items[ddlSector.SelectedIndex].Value + "' \n";
                if (ddlSector.Items[ddlSector.SelectedIndex].Value == "3200")
                {
                    filter += " AND DEPT_CODE NOT IN ('32000300'))\n ";
                    //-----------ฝ่ายโรงงานผลิตยาสูบ 3
                }
                else if (ddlSector.Items[ddlSector.SelectedIndex].Value == "3203")
                {
                    filter += " OR DEPT_CODE IN ('32000300','32030000','32030100','32030200','32030300','32030500'))\n";
                    //-----------ฝ่ายโรงงานผลิตยาสูบ 4
                }
                else if (ddlSector.Items[ddlSector.SelectedIndex].Value == "3204")
                {
                    filter += " OR DEPT_CODE IN ('32040000','32040100','32040200','32040300','32040500','32040300'))\n";
                    //-----------ฝ่ายโรงงานผลิตยาสูบ 5
                }
                else if (ddlSector.Items[ddlSector.SelectedIndex].Value == "3205")
                {
                    filter += " OR DEPT_CODE IN ('32050000','32050100','32050200','32050300','32050500'))\t";
                }
                else
                {
                    filter += ")\n";
                }
            }
            if (txt_Search_Name.Text != "")
            {
                filter += " AND (PSNL_NO LIKE '%" + txt_Search_Name.Text.Replace("'", "''") + "%' \n";
                filter += " OR PSNL_Fullname LIKE '%" + txt_Search_Name.Text.Replace("'", "''") + "%' \n";
                filter += " OR POS_Name LIKE '%" + txt_Search_Name.Text.Replace("'", "''") + "%')\n";
            }
            if (txt_Search_Organize.Text != "")
            {
                filter += " AND (SECTOR_NAME LIKE '%" + txt_Search_Organize.Text.Replace("'", "''") + "%' OR \n";
                filter += " DEPT_NAME LIKE '%" + txt_Search_Organize.Text.Replace("'", "''") + "%')\n";
            }
            switch (ddlStatus.Items[ddlStatus.SelectedIndex].Value)
            {
                case "-1":
                    filter += "";
                    break;
                case "0":
                    filter += " AND (KPI_Status IS NULL OR KPI_Status IN (0,-1)) \n";
                    break;
                case "1":
                case "2":
                case "3":
                case "4":
                case "5":
                    filter += " AND KPI_Status =" + ddlStatus.Items[ddlStatus.SelectedIndex].Value + "\n";
                    break;
            }

            SqlDataAdapter DA = new SqlDataAdapter(SQL, BL.ConnectionString());
            DA.SelectCommand.CommandTimeout = 100;
            DataTable DT = new DataTable();
            DA.Fill(DT);

            string[] ditinct_col = new string[] { "SECTOR_CODE", "SECTOR_NAME", "DEPT_CODE", "DEPT_NAME", "PSNL_NO", "PSNL_Fullname", "PNPS_CLASS", "WAGE_NAME", "POS_NO", "POS_Name", "KPI_Status", "KPI_Status_Name" };
            DT.DefaultView.RowFilter = filter;

            Session["Setting_Personal"] = DT.DefaultView.ToTable(true, ditinct_col);
            Pager.SesssionSourceName = "Setting_Personal";
            Pager.RenderLayout();

            if (DT.DefaultView.Count == 0)
            {
                lblCountPSN.Text = "ไม่พบรายการดังกล่าว";
            }
            else
            {
                lblCountPSN.Text = "พบ " + GL.StringFormatNumber(DT.DefaultView.Count, 0) + " รายการ";
            }

        }

        protected void Pager_PageChanging(PageNavigation Sender)
        {
            Pager.TheRepeater = rptKPI;
        }

        protected void rptKPI_ItemCommand(object source, System.Web.UI.WebControls.RepeaterCommandEventArgs e)
        {
            switch (e.CommandName)
            {
                case "Edit":
                    Button btnPSNEdit = (Button)e.Item.FindControl("btnPSNEdit");
                    //-------------- ดึงข้อมูลบุคคล ------------
                    PSNL_NO = btnPSNEdit.CommandArgument;
                    BindPersonal();
                    KPI_Status = BL.GetKPIStatus(R_Year, R_Round, PSNL_NO);
                    BindAssessor();
                    BindMasterKPI();

                    

                    pnlList.Visible = false;
                    pnlEdit.Visible = true;


             string SQL = " SELECT * FROM tb_HR_KPI_Header\n";
            SQL += " WHERE PSNL_NO="+ PSNL_NO +" AND R_Year="+ R_Year +" AND R_Round="+ R_Round +"  \n";
            SqlDataAdapter DA = new SqlDataAdapter(SQL, BL.ConnectionString()); 
            DataTable DT = new DataTable();
            DA.Fill(DT);
            Temp_tb_HR_KPI_Header =DT;

                    //=============
            SQL = " SELECT * FROM tb_HR_COMP_Header\n";
            SQL += " WHERE PSNL_NO="+ PSNL_NO +" AND R_Year="+ R_Year +" AND R_Round="+ R_Round +"  \n";
            DA = new SqlDataAdapter(SQL, BL.ConnectionString()); 
            DT = new DataTable();
            DA.Fill(DT);
            Temp_tb_HR_COMP_Header=DT;

            //=============
            SQL = " SELECT * FROM tb_HR_Actual_Assessor\n";
            SQL += " WHERE PSNL_NO="+ PSNL_NO +" AND R_Year="+ R_Year +" AND R_Round="+ R_Round +"  \n";
            DA = new SqlDataAdapter(SQL, BL.ConnectionString()); 
            DT = new DataTable();
            DA.Fill(DT);
                        Temp_tb_HR_Actual_Assessor=DT;

                        //=============
                        SQL = " SELECT * FROM tb_HR_Assessment_PSN\n";
                        SQL += " WHERE PSNL_NO=" + PSNL_NO + " AND R_Year=" + R_Year + " AND R_Round=" + R_Round + "  \n";
                        DA = new SqlDataAdapter(SQL, BL.ConnectionString());
                        DT = new DataTable();
                        DA.Fill(DT);
                        Temp_tb_HR_Assessment_PSN=DT;

                    

                    break;
            }
        }



        //------------ For Grouping -----------
        string LastSECTOR = "";
        string LastDEPT = "";
        protected void rptKPI_ItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType != ListItemType.AlternatingItem & e.Item.ItemType != ListItemType.Item)
                return;

            Label lnkPSNSector = (Label)e.Item.FindControl("lnkPSNSector");
            Label lnkPSNDept = (Label)e.Item.FindControl("lnkPSNDept");
            Label lblPSNName = (Label)e.Item.FindControl("lblPSNName");
            Label lblPSNPos = (Label)e.Item.FindControl("lblPSNPos");
            Label lblPSNClass = (Label)e.Item.FindControl("lblPSNClass");
            Label lblPSNType = (Label)e.Item.FindControl("lblPSNType");
            Label lblPSNASSESSOR = (Label)e.Item.FindControl("lblPSNASSESSOR");
            Label lblPSNKPIStatus = (Label)e.Item.FindControl("lblPSNKPIStatus");
            Button btnPSNEdit = (Button)e.Item.FindControl("btnPSNEdit");

            DataRowView drv = (DataRowView)e.Item.DataItem;
            if (LastSECTOR != drv["SECTOR_NAME"].ToString())
            {
                lnkPSNSector.Text = drv["SECTOR_NAME"].ToString();
                LastSECTOR = drv["SECTOR_NAME"].ToString();
                lnkPSNDept.Text = drv["DEPT_NAME"].ToString();
                LastDEPT = drv["DEPT_NAME"].ToString();
            }
            else if (LastDEPT != drv["DEPT_NAME"].ToString())
            {
                lnkPSNDept.Text = drv["DEPT_NAME"].ToString();
                LastDEPT = drv["DEPT_NAME"].ToString();
            }

            lblPSNName.Text = drv["PSNL_NO"] + " : " + drv["PSNL_Fullname"];
            lblPSNPos.Text = drv["POS_Name"].ToString();
            if (!GL.IsEqualNull(drv["PNPS_CLASS"]))
            {
                lblPSNClass.Text = GL.CINT(drv["PNPS_CLASS"]).ToString();
            }
            else
            {
                lblPSNClass.Text = "-";
            }
            lblPSNType.Text = drv["WAGE_NAME"].ToString();

            //lblPSNASSESSOR
            //DataTable ASS = AssessorList.Copy();  //เดิมแสดงรายชื่อผู้ประเมินทั้งปัจจุบันและที่เคยเลือก

            DataTable ASS = AssessorList_rptKPI.Copy();

            string Filter = "(R_Year IS NULL OR R_Year=" + R_Year + ") AND (R_Round IS NULL OR R_Round=" + R_Round + ")";
            Filter += " AND PSNL_NO='" + drv["PSNL_NO"].ToString().Replace("'", "''") + "' AND ASSESSOR_CODE<>'" + drv["PSNL_NO"].ToString().Replace("'", "''") + "'";
            ASS.DefaultView.RowFilter = Filter;

            if (ASS.DefaultView.Count == 0)
            {
                ASS = AssessorList.Copy();
                ASS.DefaultView.RowFilter = Filter;
            }

            string tmp = "";
            for (int i = 0; i <= ASS.DefaultView.Count - 1; i++)
            {
                tmp += ASS.DefaultView[i]["ASSESSOR_NAME"] + "<br>";
            }
            lblPSNASSESSOR.Text = tmp;

            lblPSNKPIStatus.Text = drv["KPI_Status_Name"].ToString();


            //------ปรับสีสถานะ-----------
            //----<0
            if (GL.IsEqualNull(drv["KPI_Status"]) || GL.CINT(drv["KPI_Status"]) == HRBL.AssessmentStatus.Creating)
            {
                lblPSNKPIStatus.ForeColor = System.Drawing.Color.Red;
                //----1
            }
            else if (GL.IsEqualNull(drv["KPI_Status"]) || GL.CINT(drv["KPI_Status"]) == HRBL.AssessmentStatus.WaitCreatingApproved)
            {
                lblPSNKPIStatus.ForeColor = System.Drawing.Color.DarkRed;
                //----2
            }
            else if (GL.IsEqualNull(drv["KPI_Status"]) || GL.CINT(drv["KPI_Status"]) == HRBL.AssessmentStatus.CreatedApproved)
            {
                lblPSNKPIStatus.ForeColor = System.Drawing.Color.Orange;
                //----3
            }
            else if (GL.IsEqualNull(drv["KPI_Status"]) || GL.CINT(drv["KPI_Status"]) == HRBL.AssessmentStatus.WaitAssessment)
            {
                lblPSNKPIStatus.ForeColor = System.Drawing.Color.DarkBlue;
                //----4
            }
            else if (GL.IsEqualNull(drv["KPI_Status"]) || GL.CINT(drv["KPI_Status"]) == HRBL.AssessmentStatus.WaitConfirmAssessment)
            {
                lblPSNKPIStatus.ForeColor = System.Drawing.Color.BlueViolet;
                //----5
            }
            else if (GL.IsEqualNull(drv["KPI_Status"]) || GL.CINT(drv["KPI_Status"]) == HRBL.AssessmentStatus.AssessmentCompleted)
            {
                lblPSNKPIStatus.ForeColor = System.Drawing.Color.Green;
            }


            btnPSNEdit.CommandArgument = drv["PSNL_NO"].ToString();

        }


        private void BindPersonal()
        {
            //----------Personal Info ----------
            int ret;

            ret = BL.GetKPIStatus(R_Year, R_Round, PSNL_NO);
            KPI_Status = ret;
            HRBL.PersonalInfo PSNInfo = BL.GetAssessmentPersonalInfo(R_Year, R_Round, PSNL_NO, KPI_Status);
            lblPSNName.Text = PSNInfo.PSNL_Fullname;
            lblPSNDept.Text = PSNInfo.DEPT_NAME;
            if (!string.IsNullOrEmpty(PSNInfo.MGR_NAME))
            {
                lblPSNPos.Text = PSNInfo.MGR_NAME;
            }
            else
            {
                lblPSNPos.Text = PSNInfo.FN_NAME;
            }

        }

        private void BindAssessor()
        {
            BL.BindDDLAssessor(ddlASSName, R_Year, R_Round, PSNL_NO);
            ddlASSName_SelectedIndexChanged(null, null);
            //----------- Lock ------------
            ddlASSName.Enabled = !BL.Is_Round_Completed(R_Year, R_Round);
        }

        protected void ddlASSName_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            lblASSPos.Text = ASSESSOR_POS;
            lblASSDept.Text = ASSESSOR_DEPT;
            //SaveHeader();
        }


        protected void btnBack_Click(object sender, System.EventArgs e)
        {
            BindAllAssessor();
            BindAssessor_rptKPI();
            BindPersonalList();
            pnlList.Visible = true;
            pnlEdit.Visible = false;
        }


        	protected void btn_Fixed_Assessor_Click(object sender, System.EventArgs e)
		{
			BindFixedAssessor();
			pnlAddAss.Visible = true;
		}

		private void BindFixedAssessor()
		{
			string SQL = "SELECT *,ISNULL(MGR_NAME,FN_Name) POS_NAME FROM vw_PN_PSNL_ALL \n";

			string Filter = "";
			if (!string.IsNullOrEmpty(txt_Search_ASS_POS.Text)) {
				Filter += " (\n";
				Filter += " DEPT_NAME LIKE '%" + txt_Search_ASS_POS.Text.Replace("'", "''") + "%' OR \n";
				Filter += " DEPT_CODE LIKE '%" + txt_Search_ASS_POS.Text.Replace("'", "''") + "%' OR\n";
				Filter += " POS_NO LIKE '%" + txt_Search_ASS_POS.Text.Replace("'", "''") + "%' OR\n";
				Filter += " ISNULL(MGR_NAME,FN_NAME) LIKE '%" + txt_Search_ASS_POS.Text.Replace("'", "''") + "%'\n";
				Filter += " ) AND ";
			}
			if (!string.IsNullOrEmpty(TextBox2.Text)) {
				Filter += " (\n";
				Filter += " PSNL_Fullname LIKE '%" + TextBox2.Text.Replace("'", "''") + "%' OR \n";
				Filter += " PSNL_NO LIKE '%" + TextBox2.Text.Replace("'", "''") + "%'\n";
				Filter += " ) AND ";
			}

			if (!string.IsNullOrEmpty(Filter)) {
				SQL += " WHERE " + Filter.Substring(0, Filter.Length - 4) + "\n";
			}

			SQL += " ORDER BY DEPT_CODE,PNPS_CLASS DESC,POS_NO,PSNL_Fullname\n";
			SqlDataAdapter DA = new SqlDataAdapter(SQL, BL.ConnectionString());
			DataTable DT = new DataTable();
			DA.Fill(DT);

			Session["Setting_KPI_Search_Assessor"] = DT;
			PagerFixedAssessor.SesssionSourceName = "Setting_KPI_Search_Assessor";
			PagerFixedAssessor.RenderLayout();

			if (DT.Rows.Count == 0) {
				lblCountFixedAssessor.Text = "ไม่พบรายการดังกล่าว";
			} else {
				lblCountFixedAssessor.Text = "พบ " + GL.StringFormatNumber(DT.Rows.Count, 0) + " รายการ";
			}
		}

		protected void PagerFixedAssessor_PageChanging(PageNavigation Sender)
		{
			PagerFixedAssessor.TheRepeater = rptFixedAssessor;
		}

		protected void btnCloseFixedAss_Click(object sender, System.EventArgs e)
		{
			pnlAddAss.Visible = false;
		}


		string LastOrgranizeName = "";

		protected void rptFixedAssessor_ItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
		{
			if (e.Item.ItemType != ListItemType.Item & e.Item.ItemType != ListItemType.AlternatingItem)
				return;

			 Label lblPSNPos = (Label)e.Item.FindControl("lblPSNPos");
			 Label lblPSNClass = (Label)e.Item.FindControl("lblPSNClass");
			Label lblPSNDept =(Label) e.Item.FindControl("lblPSNDept");
			Label lblPSNName = (Label)e.Item.FindControl("lblPSNName");
			AjaxControlToolkit.ConfirmButtonExtender cfmbtnSelect =(AjaxControlToolkit.ConfirmButtonExtender) e.Item.FindControl("cfmbtnSelect");
			Button btnSelect =(Button) e.Item.FindControl("btnSelect");

            DataRowView drv = (DataRowView)e.Item.DataItem;
			if ( (drv["DEPT_NAME"]).ToString() != LastOrgranizeName) {
				lblPSNDept.Text = drv["DEPT_NAME"].ToString ();
				LastOrgranizeName = drv["DEPT_NAME"].ToString ();
			}

			lblPSNName.Text = drv["PSNL_NO"] + " : " + drv["PSNL_Fullname"];
			lblPSNPos.Text = drv["POS_Name"].ToString ();
			lblPSNClass.Text = GL.CINT (drv["PNPS_CLASS"]).ToString ();
			btnSelect.CommandArgument = drv["PSNL_NO"].ToString ();

			cfmbtnSelect.ConfirmText = "ยืนยันระบุ " + drv["PSNL_Fullname"] + " เป็นผู้ประเมิน?";
		}

		protected void rptFixedAssessor_ItemCommand(object source, System.Web.UI.WebControls.RepeaterCommandEventArgs e)
		{
			switch (e.CommandName) {
				case "Select":
					string SQL = "SELECT * FROM vw_PN_PSNL_ALL WHERE PSNL_NO='" + e.CommandArgument.ToString().Replace("'", "''") + "'";
					SqlDataAdapter DA = new SqlDataAdapter(SQL, BL.ConnectionString());
					DataTable PSN = new DataTable();
					DA.Fill(PSN);

					if (PSN.Rows.Count == 0) {
						ScriptManager.RegisterStartupScript(this.Page, typeof(string), "Alert", "alert('ไม่พบรายการที่เลือก\\n\\nกรุณาตรวจสอบข้อมูลอีกครั้ง');", true);
						return;
					}

					SQL = "SELECT * FROM tb_HR_Actual_Assessor \n";
					SQL += " WHERE R_Year=" + R_Year + " AND R_Round=" + R_Round + " AND PSNL_NO='" + PSNL_NO.Replace("'", "''") + "'";
					DA = new SqlDataAdapter(SQL, BL.ConnectionString());
					DataTable DT = new DataTable();
					DA.Fill(DT);

					DataRow DR = null;
					if (DT.Rows.Count == 0) {
						DR = DT.NewRow();
						DR["PSNL_NO"] = PSNL_NO;
						DR["R_Year"] = R_Year;
						DR["R_Round"] = R_Round;
						DT.Rows.Add(DR);
					} else {
						DR = DT.Rows[0];
					}

					DR["MGR_ASSESSOR_POS"] = PSN.Rows[0]["POS_NO"].ToString();
					//--------------- นี่คือตัว Mapping -------------

					DR["MGR_PSNL_NO"] = PSN.Rows[0]["PSNL_NO"].ToString();
					DR["MGR_PSNL_Fullname"] = PSN.Rows[0]["PSNL_Fullname"].ToString();
					DR["MGR_PNPS_CLASS"] = PSN.Rows[0]["PNPS_CLASS"].ToString();
					DR["MGR_PSNL_TYPE"] = PSN.Rows[0]["PSNL_TYPE"].ToString();
					DR["MGR_POS_NO"] = PSN.Rows[0]["POS_NO"].ToString();
					DR["MGR_WAGE_TYPE"] = PSN.Rows[0]["WAGE_TYPE"].ToString();
					DR["MGR_WAGE_NAME"] = PSN.Rows[0]["WAGE_NAME"].ToString();
					DR["MGR_DEPT_CODE"] = PSN.Rows[0]["DEPT_CODE"].ToString();
					DR["MGR_MINOR_CODE"] = PSN.Rows[0]["MINOR_CODE"].ToString();
					DR["MGR_SECTOR_CODE"] = PSN.Rows[0]["SECTOR_CODE"].ToString();
					DR["MGR_SECTOR_NAME"] = PSN.Rows[0]["SECTOR_NAME"].ToString();
					DR["MGR_DEPT_NAME"] = PSN.Rows[0]["DEPT_NAME"].ToString();
					if (!GL.IsEqualNull(PSN.Rows[0]["FN_ID"])) {
						DR["MGR_FN_ID"] = PSN.Rows[0]["FN_ID"];
					} else {
						DR["MGR_FN_ID"] = DBNull.Value;
					}
					if (!GL.IsEqualNull(PSN.Rows[0]["FLD_Name"])) {
						DR["MGR_FLD_Name"] = PSN.Rows[0]["FLD_Name"];
					} else {
						DR["MGR_FLD_Name"] = DBNull.Value;
					}
					if (!GL.IsEqualNull(PSN.Rows[0]["FN_CODE"])) {
						DR["MGR_FN_CODE"] = PSN.Rows[0]["FN_CODE"];
					} else {
						DR["MGR_FN_CODE"] = DBNull.Value;
					}
					if (!GL.IsEqualNull(PSN.Rows[0]["FN_TYPE"])) {
						DR["MGR_FN_TYPE"] = PSN.Rows[0]["FN_TYPE"];
					} else {
						DR["MGR_FN_TYPE"] = DBNull.Value;
					}
					if (!GL.IsEqualNull(PSN.Rows[0]["FN_NAME"])) {
						DR["MGR_FN_NAME"] = PSN.Rows[0]["FN_NAME"];
					} else {
						DR["MGR_FN_NAME"] = DBNull.Value;
					}
					if (!GL.IsEqualNull(PSN.Rows[0]["MGR_CODE"])) {
						DR["MGR_MGR_CODE"] = PSN.Rows[0]["MGR_CODE"];
					} else {
						DR["MGR_MGR_CODE"] = DBNull.Value;
					}
					if (!GL.IsEqualNull(PSN.Rows[0]["MGR_NAME"])) {
						DR["MGR_MGR_NAME"] = PSN.Rows[0]["MGR_NAME"];
					} else {
						DR["MGR_MGR_NAME"] = DBNull.Value;
					}
					if (!GL.IsEqualNull(PSN.Rows[0]["PNPS_RETIRE_DATE"])) {
						DR["MGR_PNPS_RETIRE_DATE"] = PSN.Rows[0]["PNPS_RETIRE_DATE"];
					} else {
						DR["MGR_PNPS_RETIRE_DATE"] = DBNull.Value;
					}

					DR["Update_By"] = Session["USER_PSNL_NO"];
					DR["Update_Time"] = DateAndTime.Now;

					SqlCommandBuilder cmd = new SqlCommandBuilder(DA);
					DA.Update(DT);

					pnlAddAss.Visible = false;
					BindAssessor();

					break;
			}
		}

        protected void ddlKPIStatus_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            //SaveHeader();
            ScriptManager.RegisterStartupScript(this.Page, typeof(string), "Alert", "showAlert('ปรับสถานะการประเมินแล้ว');", true);
        }

		public Assessment_Setting_Header()
		{
			Load += Page_Load;
		}





        // POSITION  

        public string ChooseMode
        {
            get { return lblChooseMode.Text; }
            set { lblChooseMode.Text = value.ToString(); }
        }
        //ตำแหน่งผู้ถูกประเมิน 
        //ตำแหน่งผู้ประเมิน



        protected void btn_Search_POS_Click(object sender, System.EventArgs e)
        {
            BindPOSList();
        }


        private void BindPOSList()
        {
            string SQL = "";
            //SQL += " SELECT POS.POS_NO,ISNULL(POS.MGR_NAME,POS.FN_Name) POS_NAME,PNPO_CLASS,POS.DEPT_CODE,POS.DEPT_NAME\n";
            ////SQL += " ,PSN.PSNL_NO,PSN.PSNL_Fullname\n";
            ////SQL += " ,ASS.POS_NO ASS_BY_NO,CASE ASS.POS_NO WHEN '00001' THEN  ASS.DEPT_NAME ELSE ISNULL(ASS.MGR_NAME,ASS.FN_NAME)+ ' ' + ASS.DEPT_NAME END ASS_BY_NAME\n";
            //SQL += " FROM vw_PN_Position POS\n";
            ////SQL += " LEFT JOIN vw_PN_PSNL PSN ON POS.POS_NO=PSN.POS_NO  AND  POS.PSNL_NO=PSN.PSNL_NO      \n";
            ////SQL += " LEFT JOIN tb_HR_Assessor_Pos ASS_POS ON POS.POS_NO=ASS_POS.POS_NO\n";
            ////SQL += " LEFT JOIN vw_PN_PSNL ASS ON ASS_POS.ASSESSOR_POS=ASS.POS_NO\n";

  SQL += " SELECT DISTINCT      \n";
  SQL += " DEPT.SECTOR_CODE,DEPT.DEPT_CODE,POS.*   \n";
  SQL += " FROM vw_PN_Position POS    \n";
  SQL += " INNER JOIN vw_HR_Department DEPT ON POS.DEPT_CODE=DEPT.DEPT_CODE AND POS.MINOR_CODE=DEPT.MINOR_CODE   \n";
  SQL += " WHERE POS.PNPO_CLASS IS NOT NULL   \n";

            string Filter = "";


            if (!string.IsNullOrEmpty(txt_Dialog_DEPPSN.Text))
            {
                Filter += " (POS.DEPT_CODE LIKE '%" + txt_Dialog_DEPPSN.Text.Replace("'", "''").Replace(" ", "%") + "%' OR POS.DEPT_NAME LIKE '%" + txt_Dialog_DEPPSN.Text.Replace("'", "''").Replace(" ", "%") + "%' ) AND ";
            }
            if (!string.IsNullOrEmpty(txt_Search_Class.Text))
            {
                Filter += " PNPO_CLASS='" + txt_Search_Class.Text.PadLeft(2, GL.chr0) + "' AND ";
            }
            if (!string.IsNullOrEmpty(txt_Dialog_PosPSN.Text))
            {
                Filter += " (POS.POS_NO LIKE '%" + txt_Dialog_PosPSN.Text.Replace("'", "''").Replace(" ", "%") + "%' OR ISNULL(POS.MGR_NAME,POS.FN_Name) LIKE '%" + txt_Dialog_PosPSN.Text.Replace("'", "''").Replace(" ", "%") + "%') AND ";
            }

            if (!string.IsNullOrEmpty(Filter))
            {
                SQL += "\n" + " AND " + Filter.Substring(0, Filter.Length - 4) + "\n";
            }



            SQL += " ORDER BY DEPT.SECTOR_CODE,DEPT.DEPT_CODE,POS.PNPO_CLASS DESC,POS.POS_NO   \n";

            SqlDataAdapter DA = new SqlDataAdapter(SQL, BL.ConnectionString());
            DataTable DT = new DataTable();
            DA.Fill(DT);

            Session["Assessment_Setting_Assessor_LIST"] = DT;
            PagerPOS.SesssionSourceName = "Assessment_Setting_Assessor_LIST";
            PagerPOS.RenderLayout();

            if (DT.Rows.Count == 0)
            {
                lblCountPSN.Text = "ไม่พบรายการดังกล่าว";
            }
            else
            {
                lblCountPSN.Text = "พบ " + GL.StringFormatNumber(DT.Rows.Count, 0) + " รายการ";
            }
        }

        protected void PagerPOS_PageChanging(PageNavigation Sender)
        {
            PagerPOS.TheRepeater = rptPOSDialog;
        }

        protected void rptPOSDialog_ItemCommand(object source, System.Web.UI.WebControls.RepeaterCommandEventArgs e)
        {
            Label lblClass = (Label)e.Item.FindControl("lblClass");
            Label lblPosNo = (Label)e.Item.FindControl("lblPosNo");
            Label lblFLD = (Label)e.Item.FindControl("lblFLD");
            Label lblMGR = (Label)e.Item.FindControl("lblMGR");
            Label lblSector = (Label)e.Item.FindControl("lblSector");
            Label lblDept = (Label)e.Item.FindControl("lblDept");

            string SQL;
            DataTable DT;
            SqlCommandBuilder cmd;
            SqlDataAdapter DA;
            DataRow DR;
            string SELECTED_POS_NO;

            switch (e.CommandName)
            {
                case "Select":
                    Button btnSelect = (Button)e.Item.FindControl("btnSelect");
                    SELECTED_POS_NO = btnSelect.CommandArgument;

                    //SQL = "";
                    //SQL += " SELECT POS.* \n";
                    //SQL += " FROM vw_PN_Position POS\n"; 
                    //SQL += " WHERE POS.POS_NO='" + SELECTED_POS_NO + "'\n";

                      SQL  = " SELECT DISTINCT      \n";
  SQL += " DEPT.SECTOR_CODE,DEPT.DEPT_CODE,POS.*   \n";
  SQL += " FROM vw_PN_Position POS    \n";
  SQL += " INNER JOIN vw_HR_Department DEPT ON POS.DEPT_CODE=DEPT.DEPT_CODE AND POS.MINOR_CODE=DEPT.MINOR_CODE   \n";
  SQL += " WHERE POS.PNPO_CLASS IS NOT NULL   \n";
                    SQL += " AND POS.POS_NO='" + SELECTED_POS_NO + "'\n";

                    DA = new SqlDataAdapter(SQL, BL.ConnectionString());
                    DT = new DataTable();
                    DA.Fill(DT);

                    if (DT.Rows.Count == 0)
                    {
                        pnlEdit.Visible = false;
                        return;
                    }
                     
                    switch (ChooseMode)
                    {
                        case "ตำแหน่งผู้ถูกประเมิน":

                            if (DT .Rows .Count >0)
                            {
                                lblPSNDept.Text = DT.Rows[0]["DEPT_NAME"].ToString ();
                                if (!string.IsNullOrEmpty(DT.Rows[0]["MGR_NAME"].ToString ()))
                                {
                                    lblPSNPos.Text = DT.Rows[0]["MGR_NAME"].ToString();
                                }
                                else
                                {
                                    lblPSNPos.Text = DT.Rows[0]["FN_Name"].ToString();
                                }

                                DR = Temp_tb_HR_KPI_Header.Rows[0];
                                //DR[PSNL_NO]
                                //DR[R_Year]
                                //DR[R_Round]
                                DR["PNPS_CLASS"]=DT.Rows[0]["PNPO_CLASS"];
                                DR["POS_NO"]=DT.Rows[0]["POS_NO"];
                                DR["SECTOR_CODE"]=DT.Rows[0]["SECTOR_CODE"];
                                DR["DEPT_CODE"]=DT.Rows[0]["DEPT_CODE"];
                                DR["MINOR_CODE"]=DT.Rows[0]["MINOR_CODE"];
                                DR["SECTOR_NAME"]=DT.Rows[0]["SECTOR_NAME"];
                                DR["DEPT_NAME"]=DT.Rows[0]["DEPT_NAME"];
                                DR["FN_ID"]=DT.Rows[0]["FN_ID"];
                                DR["FLD_Name"]=DT.Rows[0]["FLD_NAME"];
                                DR["FN_CODE"] = DT.Rows[0]["_FN_CODE"];
                                DR["FN_NAME"]=DT.Rows[0]["FN_Name"];
                                DR["MGR_CODE"]=DT.Rows[0]["MGR_CODE"];
                                DR["MGR_NAME"] = DT.Rows[0]["MGR_NAME"];



                                DR = Temp_tb_HR_COMP_Header.Rows[0];
                                //DR[PSNL_NO]
                                //DR[R_Year]
                                //DR[R_Round]
                                DR["PNPS_CLASS"] = DT.Rows[0]["PNPO_CLASS"];
                                DR["POS_NO"] = DT.Rows[0]["POS_NO"];
                                DR["SECTOR_CODE"] = DT.Rows[0]["SECTOR_CODE"];
                                DR["DEPT_CODE"] = DT.Rows[0]["DEPT_CODE"];
                                DR["MINOR_CODE"] = DT.Rows[0]["MINOR_CODE"];
                                DR["SECTOR_NAME"] = DT.Rows[0]["SECTOR_NAME"];
                                DR["DEPT_NAME"] = DT.Rows[0]["DEPT_NAME"];
                                DR["FN_ID"] = DT.Rows[0]["FN_ID"];
                                DR["FLD_Name"] = DT.Rows[0]["FLD_NAME"];
                                DR["FN_CODE"] = DT.Rows[0]["_FN_CODE"];
                                DR["FN_NAME"] = DT.Rows[0]["FN_Name"];
                                DR["MGR_CODE"] = DT.Rows[0]["MGR_CODE"];
                                DR["MGR_NAME"] = DT.Rows[0]["MGR_NAME"];
                                

                                // =============tb_HR_Assessment_PSN
                               DR = Temp_tb_HR_Assessment_PSN .Rows[0];
                                //DR["PSN_ASSESSOR_POS"]=
                                DR["PSN_PNPS_CLASS"]=DT.Rows[0]["PNPO_CLASS"];
                                DR["PSN_POS_NO"]=DT.Rows[0]["POS_NO"];
                                DR["PSN_DEPT_CODE"]=DT.Rows[0]["DEPT_CODE"];
                                DR["PSN_MINOR_CODE"]= DT.Rows[0]["MINOR_CODE"];
                                DR["PSN_SECTOR_CODE"]=DT.Rows[0]["SECTOR_CODE"];
                                DR["PSN_SECTOR_NAME"]=DT.Rows[0]["SECTOR_NAME"];
                                DR["PSN_DEPT_NAME"]= DT.Rows[0]["DEPT_NAME"];
                                DR["PSN_FN_ID"]=DT.Rows[0]["FN_ID"];
                                DR["PSN_FLD_Name"]= DT.Rows[0]["FLD_NAME"];
                                DR["PSN_FN_CODE"]=DT.Rows[0]["_FN_CODE"];
                                DR["PSN_FN_TYPE"] = DT.Rows[0]["_FN_Type"];
                                DR["PSN_FN_NAME"]=DT.Rows[0]["FN_Name"];
                                DR["PSN_MGR_CODE"]= DT.Rows[0]["MGR_CODE"];
                                DR["PSN_MGR_NAME"] = DT.Rows[0]["MGR_NAME"];

                                }
                            

                            break;
                        case "ตำแหน่งผู้ประเมิน":
                            if (DT.Rows.Count > 0)
                            {
                                 
                                lblASSDept.Text = DT.Rows[0]["DEPT_NAME"].ToString();                                 
                                if (!string.IsNullOrEmpty(DT.Rows[0]["MGR_NAME"].ToString()))
                                {
                                    lblASSPos.Text = DT.Rows[0]["MGR_NAME"].ToString();
                                }
                                else
                                {
                                    lblASSPos.Text = DT.Rows[0]["FN_Name"].ToString();
                                }

                                // =============tb_HR_Assessment_PSN
                                DR = Temp_tb_HR_Assessment_PSN.Rows[0];
                                DR["PSN_ASSESSOR_POS"] = DT.Rows[0]["POS_NO"];


                                // ===============tb_HR_Actual_Assessor
                                DR = Temp_tb_HR_Actual_Assessor.Rows[0];
                                //DR["PSNL_NO"]=
                                DR["MGR_ASSESSOR_POS"] = DT.Rows[0]["POS_NO"];
                                DR["MGR_PSNL_NO"] = ASSESSOR_BY;
                                DR["MGR_PSNL_Fullname"] = ASSESSOR_NAME;
                                DR["MGR_PNPS_CLASS"] = DT.Rows[0]["PNPO_CLASS"];
                                DR["MGR_POS_NO"] = DT.Rows[0]["POS_NO"];
                                DR["MGR_DEPT_CODE"] = DT.Rows[0]["DEPT_CODE"];
                                DR["MGR_MINOR_CODE"] = DT.Rows[0]["MINOR_CODE"];
                                DR["MGR_SECTOR_CODE"] = DT.Rows[0]["SECTOR_CODE"];
                                DR["MGR_SECTOR_NAME"] = DT.Rows[0]["SECTOR_NAME"];
                                DR["MGR_DEPT_NAME"] = DT.Rows[0]["DEPT_NAME"];
                                DR["MGR_FN_ID"] = DT.Rows[0]["FN_ID"];
                                DR["MGR_FLD_Name"] = DT.Rows[0]["FLD_NAME"];
                                DR["MGR_FN_CODE"] = DT.Rows[0]["_FN_CODE"];
                                DR["MGR_FN_TYPE"] = DT.Rows[0]["_FN_Type"];
                                DR["MGR_FN_NAME"] = DT.Rows[0]["FN_Name"];
                                DR["MGR_MGR_CODE"] = DT.Rows[0]["MGR_CODE"];
                                DR["MGR_MGR_NAME"] = DT.Rows[0]["MGR_NAME"];

                            }
                            break;
                       
                    }

                    pnlList.Visible = false;
                    pnlEdit.Visible = true;
                    pnlPOSDialog.Visible = false;

                    break;
            }
        }

        string LastSector = "";
        string LastDept = "";
        protected void rptPOSDialog_ItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType != ListItemType.Item & e.Item.ItemType != ListItemType.AlternatingItem)
                return;


            Label lblClass = (Label)e.Item.FindControl("lblClass");
            Label lblPosNo = (Label)e.Item.FindControl("lblPosNo");
            Label lblFLD = (Label)e.Item.FindControl("lblFLD");
            Label lblMGR = (Label)e.Item.FindControl("lblMGR");
            Label lblSector = (Label)e.Item.FindControl("lblSector");
            Label lblDept = (Label)e.Item.FindControl("lblDept"); 

            HtmlInputButton btnPreSelect = (HtmlInputButton)e.Item.FindControl("btnPreSelect");
            Button btnSelect = (Button)e.Item.FindControl("btnSelect");

            DataRowView drv = (DataRowView)e.Item.DataItem;

            if (LastSector != drv["SECTOR_NAME"].ToString())
            {
                lblSector.Text = drv["SECTOR_NAME"].ToString();
                LastSector = drv["SECTOR_NAME"].ToString();
                lblDept.Text = drv["DEPT_NAME"].ToString();
                LastDept = drv["DEPT_NAME"].ToString();
            }
            else if (LastDept != drv["DEPT_NAME"].ToString())
            {
                lblDept.Text = drv["DEPT_NAME"].ToString();
                LastDept = drv["DEPT_NAME"].ToString();
            }

            lblClass.Text = GL.CINT(drv["PNPO_CLASS"]).ToString();

            lblPosNo.Text = drv["POS_NO"].ToString();
            lblFLD.Text = drv["FLD_NAME"].ToString();
            lblMGR.Text = drv["MGR_NAME"].ToString();
             

             
                        //btnPreSelect.Attributes["onclick"] = "if(confirm('ยืนยันเลือกตำแหน่ง " + lblListPOS.Text + " เป็นผู้ประเมินหลัก ?'))document.getElementById('" + btnSelect.ClientID + "').click();";


            btnSelect.CommandArgument = drv["POS_NO"].ToString();

        }

        protected void btnCloseDialog_Click(object sender, System.EventArgs e)
        {
            pnlPOSDialog.Visible = false;
        }

        // 
        //

        protected void btn_Fixed_POS_PSN_Click(object sender, System.EventArgs e)
        {
            ChooseMode = "ตำแหน่งผู้ถูกประเมิน";
            BindPOSList();
            pnlPOSDialog.Visible = true;

        }

        protected void btn_Fixed_POS_Assessor_Click(object sender, System.EventArgs e)
        {
            ChooseMode = "ตำแหน่งผู้ประเมิน";
            BindPOSList();
            pnlPOSDialog.Visible = true;
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            DataRow DR;
            SqlCommandBuilder cmd;
            try
            {
                          

            string SQL = " SELECT * FROM tb_HR_KPI_Header\n";
            SQL += " WHERE PSNL_NO=" + PSNL_NO + " AND R_Year=" + R_Year + " AND R_Round=" + R_Round + "  \n";
            SqlDataAdapter DA = new SqlDataAdapter(SQL, BL.ConnectionString());
            DataTable DT = new DataTable();
            DA.Fill(DT);
                if (DT .Rows .Count ==1)
                {
                    DR = DT.Rows[0];
                    for (int i = 0; i < Temp_tb_HR_KPI_Header .Columns .Count -1; i++)
                    {
                        DR[Temp_tb_HR_KPI_Header.Columns[i].ToString()] = Temp_tb_HR_KPI_Header.Rows[0][i];
                    }
                } 

            cmd = new SqlCommandBuilder(DA);
            DA.Update(DT);



            //=============
            SQL = " SELECT * FROM tb_HR_COMP_Header\n";
            SQL += " WHERE PSNL_NO=" + PSNL_NO + " AND R_Year=" + R_Year + " AND R_Round=" + R_Round + "  \n";
            DA = new SqlDataAdapter(SQL, BL.ConnectionString());
            DT = new DataTable();
            DA.Fill(DT);
            DR = null;
            if (DT.Rows.Count == 1)
            {
                DR = DT.Rows[0];
                for (int i = 0; i < Temp_tb_HR_COMP_Header.Columns.Count - 1; i++)
                {
                    DR[Temp_tb_HR_COMP_Header.Columns[i].ToString()] = Temp_tb_HR_COMP_Header.Rows[0][i];
                }
            }

            cmd = new SqlCommandBuilder(DA);
            DA.Update(DT);


            //=============
            SQL = " SELECT * FROM tb_HR_Actual_Assessor\n";
            SQL += " WHERE PSNL_NO=" + PSNL_NO + " AND R_Year=" + R_Year + " AND R_Round=" + R_Round + "  \n";
            DA = new SqlDataAdapter(SQL, BL.ConnectionString());
            DT = new DataTable();
            DA.Fill(DT); 
            DR = null;
            if (DT.Rows.Count == 1)
            {
                DR = DT.Rows[0];
                for (int i = 0; i < Temp_tb_HR_Actual_Assessor.Columns.Count - 1; i++)
                {
                    DR[Temp_tb_HR_Actual_Assessor.Columns[i].ToString()] = Temp_tb_HR_Actual_Assessor.Rows[0][i];
                }
            }

            cmd = new SqlCommandBuilder(DA);
            DA.Update(DT);


            //=============
            SQL = " SELECT * FROM tb_HR_Assessment_PSN\n";
            SQL += " WHERE PSNL_NO=" + PSNL_NO + " AND R_Year=" + R_Year + " AND R_Round=" + R_Round + "  \n";
            DA = new SqlDataAdapter(SQL, BL.ConnectionString());
            DT = new DataTable();
            DA.Fill(DT); 

            DR = null;
            if (DT.Rows.Count == 1)
            {
                DR = DT.Rows[0];
                for (int i = 0; i < Temp_tb_HR_Assessment_PSN.Columns.Count - 1; i++)
                {
                    DR[Temp_tb_HR_Assessment_PSN.Columns[i].ToString()] = Temp_tb_HR_Assessment_PSN.Rows[0][i];
                }
            }

            cmd = new SqlCommandBuilder(DA);
            DA.Update(DT);

            ScriptManager.RegisterStartupScript(this.Page, typeof(string), "Alert", "showAlert('บันทึกสำเร็จ');", true);

  }
            catch (Exception)
            {
                
                throw;
            }

        }

      //  =============tb_HR_KPI_Header
      // [PSNL_NO]
      //,[R_Year]
      //,[R_Round]
      //,[PNPS_CLASS]
      //,[PSNL_TYPE]
      //,[POS_NO]
      //,[WAGE_TYPE]
      //,[WAGE_NAME]
      //,[SECTOR_CODE]
      //,[DEPT_CODE]
      //,[MINOR_CODE]
      //,[SECTOR_NAME]
      //,[DEPT_NAME]
      //,[FN_ID]
      //,[FLD_Name]
      //,[FN_CODE]
      //,[FN_NAME]
      //,[MGR_CODE]
      //,[MGR_NAME]

      //  =============tb_HR_COMP_Header
      // [PSNL_NO]
      //,[R_Year]
      //,[R_Round]
      //,[PNPS_CLASS]
      //,[PSNL_TYPE]
      //,[POS_NO]
      //,[WAGE_TYPE]
      //,[WAGE_NAME]
      //,[SECTOR_CODE]
      //,[DEPT_CODE]
      //,[MINOR_CODE]
      //,[SECTOR_NAME]
      //,[DEPT_NAME]
      //,[FN_ID]
      //,[FLD_Name]
      //,[FN_CODE]
      //,[FN_NAME]
      //,[MGR_CODE]
      //,[MGR_NAME]

      // ===============tb_HR_Actual_Assessor
      // [PSNL_NO]
      //,[R_Year]
      //,[R_Round]
      //,[MGR_ASSESSOR_POS]
      //,[MGR_PSNL_NO]
      //,[MGR_PSNL_Fullname]
      //,[MGR_PNPS_CLASS]
      //,[MGR_PSNL_TYPE]
      //,[MGR_POS_NO]
      //,[MGR_WAGE_TYPE]
      //,[MGR_WAGE_NAME]
      //,[MGR_DEPT_CODE]
      //,[MGR_MINOR_CODE]
      //,[MGR_SECTOR_CODE]
      //,[MGR_SECTOR_NAME]
      //,[MGR_DEPT_NAME]
      //,[MGR_FN_ID]
      //,[MGR_FLD_Name]
      //,[MGR_FN_CODE]
      //,[MGR_FN_TYPE]
      //,[MGR_FN_NAME]
      //,[MGR_MGR_CODE]
      //,[MGR_MGR_NAME]


      // =============tb_HR_Assessment_PSN
      // [PSNL_NO]
      //,[R_Year]
      //,[R_Round]
      //,[PSN_ASSESSOR_POS]
      //,[PSN_PSNL_NO]
      //,[PSN_PSNL_Fullname]
      //,[PSN_PNPS_CLASS]
      //,[PSN_PSNL_TYPE]
      //,[PSN_POS_NO]
      //,[PSN_WAGE_TYPE]
      //,[PSN_WAGE_NAME]
      //,[PSN_DEPT_CODE]
      //,[PSN_MINOR_CODE]
      //,[PSN_SECTOR_CODE]
      //,[PSN_SECTOR_NAME]
      //,[PSN_DEPT_NAME]
      //,[PSN_FN_ID]
      //,[PSN_FLD_Name]
      //,[PSN_FN_CODE]
      //,[PSN_FN_TYPE]
      //,[PSN_FN_NAME]
      //,[PSN_MGR_CODE]
      //,[PSN_MGR_NAME]



    }
}
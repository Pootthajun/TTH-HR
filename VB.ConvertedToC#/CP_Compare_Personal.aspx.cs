using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Xml.Linq;
using System.Diagnostics;
using System.Collections.Specialized;
using System.Configuration;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Caching;
using System.Web.SessionState;
using System.Web.Security;
using System.Web.Profile;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using System.Data.SqlClient;
namespace VB
{

	public partial class CP_Compare_Personal : System.Web.UI.Page
	{


		HRBL BL = new HRBL();

		GenericLib GL = new GenericLib();
		protected void Page_Load(object sender, System.EventArgs e)
		{
			if ((Session["USER_PSNL_NO"] == null)) {
				ScriptManager.RegisterStartupScript(this.Page, typeof(string), "Alert", "showAlert('กรุณาลงชื่อเข้าใช้ระบบ');", true);
				Response.Redirect("Login.aspx");
				return;
			}
			if (!IsPostBack) {
				ClearForm();
				BL.BindDDlSector(ddlSector);
				BL.BindDDlSector(ddlSector_Pos);
				BindCompareDetail();
			}
		}

		//---------ตำแหน่งหลักที่ต้องการเปรียบเทียบ-------------
		public string POS_NO {
			get { return lblPos.Attributes["POS_NO"]; }
			set { lblPos.Attributes["POS_NO"] = value; }
		}

		//---------ตำแหน่งพนักงานที่สามารถเข้ารับดำรงตำแหน่งได้-------------
		public string POS_NO_List {
			get { return lblPos.Attributes["PSNL_NO_List"]; }
			set { lblPos.Attributes["PSNL_NO_List"] = value; }
		}

		//-----------รหัสพนักงานที่เข้าเปรียบเทียบ----------------
		public string PSNL_NO {
			get { return lblPos.Attributes["PSNL_NO"]; }
			set { lblPos.Attributes["PSNL_NO"] = value; }
		}

		//--------------------------
		public string PSLN_NO_Current_Compare {
			get { return lblPosCurrent.Attributes["PSLN_NO_Current_Compare"]; }
			set { lblPosCurrent.Attributes["PSLN_NO_Current_Compare"] = value; }
		}


		//-----------ตรวจเกณฑ์การเข้าสู่ตำแหน่ง----------------------

		//---------1 คะแนนประเมิน-------------
		public string Check_Ass_MIN_Score {
			get { return lblPos.Attributes["Check_Ass_MIN_Score"]; }
			set { lblPos.Attributes["Check_Ass_MIN_Score"] = value; }
		}

		//---------2 คะแนนวันลา-------------
		public string Check_Leave {
			get { return lblPos.Attributes["Check_Leave"]; }
			set { lblPos.Attributes["Check_Leave"] = value; }
		}

		//---------3 การฝึกอบรม-------------
		public string Check_Course {
			get { return lblPos.Attributes["Check_Course"]; }
			set { lblPos.Attributes["Check_Course"] = value; }
		}

		//---------4 การสอบ-------------
		public string Check_Test {
			get { return lblPos.Attributes["Check_Test"]; }
			set { lblPos.Attributes["Check_Test"] = value; }
		}

		//---------5 โทษทางวินัย-------------
		public string Check_Punishment {
			get { return lblPos.Attributes["Check_Punishment"]; }
			set { lblPos.Attributes["Check_Punishment"] = value; }
		}

		private void ClearForm()
		{
			ModalAddPSNL_NO.Visible = false;
			ModalPost.Visible = false;
			//------------Header-----------
			lblHeader_POS.Text = "";
			lblHeader_POS_Class.Text = "";
			POS_NO = "";
			POS_NO_List = "";
			PSNL_NO = "";
			PSLN_NO_Current_Compare = "";
			//---------Check-----------
			Check_Ass_MIN_Score = "";
			Check_Leave = "";
			Check_Course = "";
			Check_Test = "";
			Check_Punishment = "";
		}

		private void BindCompareDetail()
		{
			string Setting_POS_NO = "";
			if (string.IsNullOrEmpty(POS_NO)) {
				Setting_POS_NO = " WHERE  0=1 ";
			} else {
				Setting_POS_NO = "  WHERE POS_No ='" + POS_NO + "'";
			}

			string Setting_PSNL_NO = "";
			if (string.IsNullOrEmpty(PSNL_NO)) {
				Setting_PSNL_NO = " WHERE  0=1 ";
			} else {
				Setting_PSNL_NO = "  WHERE  PSN.PSNL_NO IS NOT NULL AND PSN.PSNL_NO IN (" + PSNL_NO + ")";
			}

			string SQL = "";
			if (string.IsNullOrEmpty(POS_NO) & string.IsNullOrEmpty(PSNL_NO)) {
				SQL = "";
				SQL += "   SELECT    'A_Property' Property,'' PSNL_NO,'เลือกตำแหน่งเพื่อเปรียบเทียบ....' PSNL_Fullname, 0 Year_Score, 0 LEAVE_All\n";
				SQL += "   ,'' History_Status_PUNISH,'' History_PUNISH  \n";
			} else if ((!string.IsNullOrEmpty(POS_NO)) & string.IsNullOrEmpty(PSNL_NO)) {
				SQL = "";
				SQL += "   SELECT ";
				SQL += "   'A_Property' Property,";
				SQL += "   POS_No PSNL_NO,";
				SQL += "   'คุณสมบัติตำแหน่ง....' PSNL_Fullname,  ";
				SQL += "   Ass_Min_Score Year_Score,";
				SQL += "   Leave_Score LEAVE_All,";
				SQL += "   Punishment History_Status_PUNISH,";
				SQL += "   CASE WHEN Punishment=1 THEN 'ไม่เคยรับโทษ' ELSE 'เคยรับโทษ' END History_PUNISH ";
				SQL += "   FROM tb_Path_Setting ";
				SQL += "   " + Setting_POS_NO + "\n";
				SqlDataAdapter DA_Check = new SqlDataAdapter(SQL, BL.ConnectionString());
				DataTable DT_Check = new DataTable();
				DA_Check.Fill(DT_Check);

				if (DT_Check.Rows.Count == 1) {
					//---------Check-----------
					if (GL.IsEqualNull(DT_Check.Rows[0]["Year_Score"])) {
						Check_Ass_MIN_Score = GL.StringFormatNumber(0);
					} else {
                        Check_Ass_MIN_Score = DT_Check.Rows[0]["Year_Score"].ToString();
					}
					if (GL.IsEqualNull(DT_Check.Rows[0]["LEAVE_All"])) {
						Check_Leave = GL.StringFormatNumber(0);
					} else {
                        Check_Leave = DT_Check.Rows[0]["LEAVE_All"].ToString();
					}
                    Check_Punishment = DT_Check.Rows[0]["History_Status_PUNISH"].ToString();

				} else {
				}
			} else if ((!string.IsNullOrEmpty(POS_NO)) & !string.IsNullOrEmpty(PSNL_NO)) {
				SQL = "";
				SQL += "   DECLARE @R_Year As Int=2558\n";
				SQL += "   DECLARE @R_Round As Int=1\n";
				SQL += "   SELECT    'A_Property' Property,   POS_No PSNL_NO,   'คุณสมบัติตำแหน่ง....' PSNL_Fullname\n";
				SQL += "   ,     Ass_Min_Score Year_Score,   Leave_Score LEAVE_All\n";
				SQL += "   ,Punishment History_Status_PUNISH";
				SQL += "   ,   CASE WHEN Punishment=1 THEN 'ไม่เคยรับโทษ' ELSE 'เคยรับโทษ' END History_PUNISH    FROM tb_Path_Setting      \n";
				SQL += "   " + Setting_POS_NO + "\n";
				SQL += "            UNION \n";

				SQL += "   Select  'B_Employee'  Property,\n";
				SQL += "   Header.PSNL_NO, Header.PSNL_Fullname PSNL_Fullname\n";
				SQL += "   ,ISNULL(Yearly_Result.Year_Score,0) Year_Score\n";
				SQL += "   ,ISNULL(Yearly_Result.Leave_Day  ,0) LEAVE_All\n";
				SQL += "   ,CASE WHEN History_PUNISH.Count_PUNISH > 0 THEN 0 ELSE 1 END  History_Status_PUNISH\n";
				SQL += "   ,CASE WHEN History_PUNISH.Count_PUNISH > 0 THEN 'เคยรับโทษ' ELSE 'ไม่เคยรับโทษ' END  History_PUNISH\n";
				SQL += "   FROM vw_PN_PSNL PSN\n";
				SQL += "   INNER JOIN vw_RPT_KPI_Status Header ON PSN .PSNL_NO =Header.PSNL_NO \n";
				SQL += "   \t\t\t\t\t\tAND Header.R_Year=@R_Year AND Header.R_Round=@R_Round\n";

				SQL += "    -----------------------History_PUNISH---------------------------------------\n";
				SQL += "   LEFT JOIN vw_Path_Count_History_PUNISH History_PUNISH ON PSN.PSNL_NO=History_PUNISH.PSNL_NO\n";
				SQL += "   LEFT JOIN vw_RPT_Yearly_Result Yearly_Result  ON PSN.PSNL_NO=Yearly_Result.PSNL_NO AND Yearly_Result.R_Year =2557\n";

				SQL += "   " + Setting_PSNL_NO;
				SQL += "   ORDER by Property";

			}
			SqlDataAdapter DA = new SqlDataAdapter(SQL, BL.ConnectionString());
			DataTable DT = new DataTable();
			DA.Fill(DT);

			if (string.IsNullOrEmpty(POS_NO)) {
				rpttd_Header.DataSource = DT;
				rpttd_Header.DataBind();
			} else {
				//------เพิ่มแถว สำหรับ Add เพิ่มเติม---------
				DataRow DR = null;
				if (DT.Rows.Count >= 1) {
					DR = DT.NewRow();
					DR["Property"] = "C_Add";
					DR["PSNL_NO"] = "";
					DR["PSNL_Fullname"] = " เพิ่มพนักงาน";
					DR["Year_Score"] = DBNull.Value;
					DR["LEAVE_All"] = DBNull.Value;
					//DR("History_Status_PUNISH") = DBNull.Value
					DR["History_PUNISH"] = "";
					DT.Rows.Add(DR);
				}

				rpttd_Header.DataSource = DT;
				rpttd_Header.DataBind();
				rpttd_Ass_MIN_Score.DataSource = DT;
				rpttd_Ass_MIN_Score.DataBind();
				rpttd_Leave.DataSource = DT;
				rpttd_Leave.DataBind();

				//-------Course-------
				rpttd_Course.DataSource = DT;
				rpttd_Course.DataBind();

				//-------Test-------
				rpttd_Test.DataSource = DT;
				rpttd_Test.DataBind();

				rpttd_Punishment.DataSource = DT;
				rpttd_Punishment.DataBind();

				rpttd_btn.DataSource = DT;
				rpttd_btn.DataBind();
			}
		}


		private void BindPostList()
		{
			string SQL = "";

			SQL += "  SELECT \n";
			SQL += "   POS.SECTOR_NAME,POS.DEPT_CODE,POS.DEPT_NAME ,POS.POS_NO,ISNULL(POS.MGR_NAME,'-') MGR_NAME ,ISNULL(POS.FLD_NAME,'-') FLD_NAME,\n";
			SQL += "       POS.PNPO_CLASS, PSN.PSNL_NO, PSN.PSNL_Fullname, POS.Update_Time, Setting_Status.Status_Path\n";
			SQL += "  FROM vw_PN_Position POS \n";
			SQL += "  LEFT JOIN vw_PN_PSNL PSN ON POS.POS_NO=PSN.POS_NO\n";
			SQL += "  INNER JOIN vw_Path_Setting_Status Setting_Status ON Setting_Status.POS_No = POS.POS_NO\n";
			SQL += "       WHERE POS.PNPO_CLASS Is Not NULL \n";

			if (ddlSector_Pos.SelectedIndex > 0) {
				SQL += " AND ( LEFT(POS.DEPT_CODE,2)='" + ddlSector_Pos.Items[ddlSector_Pos.SelectedIndex].Value + "' \n";
				SQL += " OR LEFT(POS.DEPT_CODE,4)='" + ddlSector_Pos.Items[ddlSector_Pos.SelectedIndex].Value + "' \n";

				if (GL.CINT (ddlSector_Pos.Items[ddlSector_Pos.SelectedIndex].Value) == 3200) {
					SQL += " AND POS.DEPT_CODE NOT IN ('32000300'))\n";
				} else if (GL.CINT (ddlSector_Pos.Items[ddlSector_Pos.SelectedIndex].Value) == 3203) {
					SQL += " OR POS.DEPT_CODE IN ('32000300'))\n";
				} else {
					SQL += " ) ";
				}
			}
			if (!string.IsNullOrEmpty(txtSearchPos.Text)) {
				SQL += " AND (POS.SECTOR_NAME LIKE '%" + txtSearchPos.Text.Replace("'", "''") + "%' OR \n";
				SQL += " POS.DEPT_NAME LIKE '%" + txtSearchPos.Text.Replace("'", "''") + "%' OR \n";
				SQL += " POS.POS_NO LIKE '%" + txtSearchPos.Text.Replace("'", "''") + "%' OR \n";
				SQL += " POS.MGR_NAME LIKE '%" + txtSearchPos.Text.Replace("'", "''") + "%' OR \n";
				SQL += " POS.FLD_NAME LIKE '%" + txtSearchPos.Text.Replace("'", "''") + "%' )\n";
			}
			SQL += "  AND (Setting_Status.Status_Path IN (1))  \n";
			SQL += "       Group BY \n";
			SQL += "       POS.POS_NO, POS.FLD_NAME, POS.MGR_NAME, POS.PNPO_CLASS, PSN.PSNL_NO, PSN.PSNL_Fullname, POS.Update_Time, POS.SECTOR_NAME, POS.DEPT_CODE, POS.DEPT_NAME\n";
			SQL += "  ,Setting_Status.Status_Path\n";
			SQL += "  ORDER BY    POS.DEPT_CODE,POS.DEPT_NAME ,POS.SECTOR_NAME,POS.PNPO_CLASS DESC,POS.POS_NO\n";

			SqlDataAdapter DA = new SqlDataAdapter(SQL, BL.ConnectionString());
			DataTable DT = new DataTable();
			DA.Fill(DT);
			if (DT.Rows.Count > 0) {
				lblCountPosNo.Text = "พบ " + DT.Rows.Count + " รายการ";
			} else {
				lblCountPosNo.Text = "ไม่พบตำแหน่ง";
			}

			Session["Compare_Personal_POS"] = DT;
			Pager_Pos.SesssionSourceName = "Compare_Personal_POS";
			Pager_Pos.RenderLayout();

			if (DT.Rows.Count == 0) {
				lblCountPosNo.Text = "ไม่พบรายการดังกล่าว";
			} else {
				lblCountPosNo.Text = "พบ " + GL.StringFormatNumber(DT.Rows.Count, 0) + " รายการ";
			}
		}
		protected void Pager_Pos_PageChanging(PageNavigation Sender)
		{
			Pager_Pos.TheRepeater = rptPosDialog;
		}

		string LastSectorPos = "";

		string LastDeptPos = "";
		protected void rptPosDialog_ItemCommand(object source, System.Web.UI.WebControls.RepeaterCommandEventArgs e)
		{
			switch (e.CommandName) {
				case "Select":
					Button btnPosSelect = (Button) e.Item.FindControl("btnPosSelect");
					string POS_No_Select = btnPosSelect.CommandArgument;
					if (POS_NO != POS_No_Select) {
						PSNL_NO = "";
						POS_NO = POS_No_Select;
						//----------แสดงข้อมูลตารางเปรียบเทียบพนักงาน----------------
						BindCompareDetail();
						//----------ตารางรหัสพนักงานที่ ดึงขึ้นมาเปรียบเทียบ----------------
						CurrentDataTable();

					} else {
					}
					ModalPost.Visible = false;

					break;
			}
		}
		protected void rptPosDialog_ItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
		{
			if (e.Item.ItemType != ListItemType.AlternatingItem & e.Item.ItemType != ListItemType.Item)
				return;
			Label lblSector =(Label) e.Item.FindControl("lblSector");
			Label lblDept =(Label) e.Item.FindControl("lblDept");
			Label lblClass = (Label) e.Item.FindControl("lblClass");
			Label lblPosNo =(Label) e.Item.FindControl("lblPosNo");
			Label lblMGR =(Label) e.Item.FindControl("lblMGR");
			Label lblPSN = (Label) e.Item.FindControl("lblPSN");
			Button btnPosSelect = (Button) e.Item.FindControl("btnPosSelect");
            DataRowView drv = (DataRowView)e.Item.DataItem;
			if (LastSectorPos != drv["SECTOR_NAME"].ToString()) {
				lblSector.Text = drv["SECTOR_NAME"].ToString();
				LastSectorPos = drv["SECTOR_NAME"].ToString();
				lblDept.Text = drv["DEPT_NAME"].ToString();
				LastDeptPos = drv["DEPT_NAME"].ToString();
			} else if (LastDeptPos != drv["DEPT_NAME"].ToString()) {
				lblDept.Text = drv["DEPT_NAME"].ToString();
				LastDeptPos = drv["DEPT_NAME"].ToString();
			}
			lblClass.Text = GL.CINT (drv["PNPO_CLASS"]).ToString ();
			if (GL.IsEqualNull(drv["POS_NO"])) {
				lblPosNo.Text = "-";
			} else {
				lblPosNo.Text = drv["POS_NO"].ToString() + ":" + drv["FLD_NAME"].ToString();
			}
			lblMGR.Text = drv["MGR_NAME"].ToString();
            btnPosSelect.CommandArgument = drv["POS_NO"].ToString();

		}

		//------------Header----------------

		protected void rpttd_Header_ItemCommand(object source, System.Web.UI.WebControls.RepeaterCommandEventArgs e)
		{
            LinkButton btnAdd_POS_NO = (LinkButton)e.Item.FindControl("btnAdd_POS_NO");
            LinkButton btnAdd_PSNL_NO = (LinkButton)e.Item.FindControl("btnAdd_PSNL_NO");

			switch (e.CommandName) {
				case "lnk_add_PSNL_NO":
					
					//-----รหัสพนักงานที่กำลังดึงข้อมูล------
					PSLN_NO_Current_Compare = e.CommandArgument.ToString ();

					if (string.IsNullOrEmpty(POS_NO)) {
						ScriptManager.RegisterStartupScript(this.Page, typeof(string), "Alert", "showAlert('กำหนดตำแหน่งเปรียบเทียบ  ก่อนเลือกพนักงาน');", true);
						return;
					}

					string SQL = "";
					SQL += "  SELECT To_POS_No  From_POS_No \n";
					SQL += "  ,  From_POS_No To_POS_No \n";
					SQL += "  ,vw_PN_Position.POS_NO\n";
					SQL += "  ,vw_PN_Position.DEPT_NAME\n";
					SQL += "  ,vw_PN_Position.PNPO_CLASS\n";
					SQL += "  ,vw_PN_Position.MGR_NAME\n";
					SQL += "  FROM  tb_Path_Setting_Route Setting_Route\n";
					SQL += "  LEFT JOIN tb_Path_Setting Path_Setting  ON Path_Setting.POS_No=Setting_Route.To_POS_No\n";
					SQL += "  INNER JOIN vw_PN_Position ON vw_PN_Position.POS_NO=Setting_Route.From_POS_No\n";
					SQL += "  WHERE To_POS_No =" + POS_NO + "\n";
					SqlDataAdapter DA = new SqlDataAdapter(SQL, BL.ConnectionString());
					DataTable DT = new DataTable();
					DA.Fill(DT);

					//------------หาตำแหน่งของพนักงานที่ สามารถเข้ารับดำรงตำแหน่งได้------
					string List = "";
					POS_NO_List = "";
					if (DT.Rows.Count > 0) {

						for (int i = 0; i <= DT.Rows.Count - 1; i++) {
							List += "'" + DT.Rows[i]["POS_NO"] + "'" + ",";
						}
						if (!string.IsNullOrEmpty(List)) {
							POS_NO_List += List.Substring(0, List.Length - 1);
						} else {
							POS_NO_List += "";
						}
					}

					BindPersonal();
					ModalAddPSNL_NO.Visible = true;
					ScriptManager.RegisterStartupScript(this.Page, typeof(string), "Focus", "document.getElementById('" + txt_Search_Organize_Add.ClientID + "').focus();", true);

					break;

				case "lnk_add_POS_NO":
					
					//------------- Get Existing  List ---------------
					BindPostList();
					ModalPost.Visible = true;
					ScriptManager.RegisterStartupScript(this.Page, typeof(string), "Focus", "document.getElementById('" + txtSearchPos.ClientID + "').focus();", true);

					break;
			}

		}

		protected void rpttd_Header_ItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
		{
			if (e.Item.ItemType != ListItemType.AlternatingItem & e.Item.ItemType != ListItemType.Item)
				return;
			Label lblHeader =(Label) e.Item.FindControl("lblHeader");
			Label lblHeader_Detail =(Label)  e.Item.FindControl("lblHeader_Detail");
			Panel pnl_icon_user =(Panel) e.Item.FindControl("pnl_icon_user");

			LinkButton btnAdd_POS_NO =(LinkButton) e.Item.FindControl("btnAdd_POS_NO");
			LinkButton btnAdd_PSNL_NO =(LinkButton) e.Item.FindControl("btnAdd_PSNL_NO");
			LinkButton btnEDIT_PSNL_NO =(LinkButton) e.Item.FindControl("btnEDIT_PSNL_NO");
			HtmlTableCell cel_header =(HtmlTableCell) e.Item.FindControl("cel_header");
            DataRowView drv = (DataRowView)e.Item.DataItem;


			//------------ตำแหน่งหลักเพื่อเปรียบเทียบ----------
			if ((drv["Property"].ToString ()) == "A_Property") {
				if (string.IsNullOrEmpty(POS_NO)) {
					lblHeader.Text = " เพิ่มตำแหน่ง.....";
				} else {
					DataTable DT = null;
					DT = BindDetail_POS(drv["PSNL_NO"].ToString());
					if (DT.Rows.Count > 0) {
						//lblHeader.Text = "<B>" & DT.Rows(0).Item("POS_NO").ToString() & " : " & DT.Rows(0).Item("MGR_NAME").ToString
						//lblHeader_Detail.Text = "</B>" & " ระดับ : " & CInt(DT.Rows(0).Item("PNPO_CLASS")) & " " & DT.Rows(0).Item("DEPT_NAME").ToString

						lblHeader.Text = " เปลี่ยนตำแหน่ง.....";
						//------------Header-----------
						lblHeader_POS.Text = "<B>" + " เปรียบเทียบตำแหน่ง " + DT.Rows[0]["POS_NO"].ToString() + " : " + DT.Rows[0]["MGR_NAME"].ToString();
						lblHeader_POS_Class.Text = "       " + " ระดับ : " + Convert.ToInt32(DT.Rows[0]["PNPO_CLASS"]) + " " + DT.Rows[0]["DEPT_NAME"].ToString();
					}
				}

				pnl_icon_user.Visible = false;
				btnAdd_POS_NO.Attributes["btnAdd_PSNL_NO"] = drv["PSNL_NO"].ToString ();
                cel_header.Attributes["onclick"] = "document.getElementById('" + btnAdd_POS_NO.ClientID + "').click();";
				btnAdd_POS_NO.ToolTip = "เลือกตำแหน่งเพื่อเปรียบเทียบ";
				btnAdd_POS_NO.Visible = true;
				btnEDIT_PSNL_NO.Visible = false;
				btnAdd_PSNL_NO.Visible = false;


                cel_header.Style["background-color"] = "#999999";
                cel_header.Style["text-align"] = "center";
                cel_header.Attributes["class"] = "pricing-head";
			//------------พนักงานที่เลือกเพื่อจะเปรียบเทียบ----------
            }
            else if ((drv["Property"].ToString()) == "B_Employee")
            {
				//----------รายละเอียดคำแหน่งพนักงาน-------------
				lblHeader.Text = drv["PSNL_NO"].ToString() + " : " + drv["PSNL_Fullname"].ToString();
				DataTable DT = null;
                DT = BindDetail_POS_PSNL(drv["PSNL_NO"].ToString());

				if (DT.Rows.Count > 0) {
					lblHeader_Detail.Text = "</br>" + "<B>" + DT.Rows[0]["POS_NO"].ToString() + " : " + DT.Rows[0]["POS_Name"].ToString() + "</B>" + "</br>" + " ระดับ : " + Convert.ToInt32(DT.Rows[0]["PNPS_CLASS"]) + DT.Rows[0]["DEPT_NAME"].ToString();
                    cel_header.Style["color"] = "#D3D3D3";
				}



				pnl_icon_user.Visible = true;
				btnAdd_PSNL_NO.Attributes["btnAdd_PSNL_NO"] = drv["PSNL_NO"].ToString ();
				//cel_Header.Attributes("onclick") = "document.getElementById('" & btnAdd_PSNL_NO.ClientID & "').click();"
				//btnAdd_PSNL_NO.ToolTip = "เปลี่ยน"
				btnAdd_POS_NO.Visible = false;
				btnEDIT_PSNL_NO.Visible = false;
				btnAdd_PSNL_NO.Visible = false;

				lblHeader.Style["color"] = "white";
				btnEDIT_PSNL_NO.Style["color"] = "white";
                cel_header.Style["text-align"] = "center";
                cel_header.Style["background-color"] = "#104E8B";

			//------------เพิ่มพนักงาน----------
			} else {
				lblHeader.Text = " เพิ่มพนักงาน ";
				pnl_icon_user.Visible = false;
				btnAdd_PSNL_NO.Attributes["btnAdd_PSNL_NO"] = drv["PSNL_NO"].ToString ();
                cel_header.Attributes["onclick"] = "document.getElementById('" + btnAdd_PSNL_NO.ClientID + "').click();";
				btnAdd_PSNL_NO.ToolTip = "เพิ่มพนักงาน";
				btnAdd_POS_NO.Visible = false;
				btnEDIT_PSNL_NO.Visible = false;
				btnAdd_PSNL_NO.Visible = true;

                cel_header.Style["text-align"] = "right";
                cel_header.Style["background-color"] = "#999999";
			}

		}

		public DataTable BindDetail_POS_PSNL(string PSNL_NO)
		{
			string filter = "";
			if (!string.IsNullOrEmpty(PSNL_NO)) {
				filter = " AND PSNL_NO='" + PSNL_NO + "'";
			}
			string SQL = "";
			SQL += "SELECT * FROM vw_Path_RPT_Yearly_Result \n";
			SQL += " WHERE PSNL_NO IS NOT NULL \n";
			SQL += "" + filter + "\n";
			SQL += "ORDER BY SECTOR_NAME ASC ,SECTOR_CODE ASC,DEPT_CODE ASC ,PNPS_CLASS DESC  ,Year_Score  DESC\n";
			SqlDataAdapter DA = new SqlDataAdapter(SQL, BL.ConnectionString());
			DataTable DT = new DataTable();
			DA.Fill(DT);
			return DT;

		}

		public DataTable BindDetail_POS(string PSNL_NO)
		{
			string filter = "";
			if (!string.IsNullOrEmpty(PSNL_NO)) {
				filter = " AND POS.POS_NO='" + PSNL_NO + "'";
			}
			string SQL = "";
			SQL += " SELECT DEPT.SECTOR_CODE,DEPT.SECTOR_NAME,DEPT.DEPT_CODE,DEPT.DEPT_NAME,POS.POS_NO,ISNULL(POS.MGR_NAME,'-') MGR_NAME,ISNULL(POS.FLD_NAME,'-') FLD_NAME,\n";
			SQL += "  POS.PNPO_CLASS,PSN.PSNL_NO,PSN.PSNL_Fullname,POS.Update_Time \n";
			SQL += "  ,Setting_Status.Status_Path\n";
			SQL += "  FROM vw_PN_Position POS \n";
			SQL += "  INNER JOIN vw_HR_Department DEPT ON POS.DEPT_CODE=DEPT.DEPT_CODE AND POS.MINOR_CODE=DEPT.MINOR_CODE\n";
			SQL += "  LEFT JOIN vw_PN_PSNL PSN ON POS.POS_NO=PSN.POS_NO\n";
			SQL += "  INNER JOIN vw_Path_Setting_Status Setting_Status ON Setting_Status.POS_No = POS.POS_NO\n";
			SQL += "  WHERE POS.PNPO_CLASS IS NOT NULL   \n";
			SQL += "" + filter + "\n";
			SQL += "  GROUP BY DEPT.SECTOR_NAME,DEPT.DEPT_NAME,POS.POS_NO,POS.FLD_NAME,POS.MGR_NAME,\n";
			SQL += "  POS.PNPO_CLASS, PSN.PSNL_NO, PSN.PSNL_Fullname, POS.Update_Time, DEPT.SECTOR_CODE, DEPT.DEPT_CODE, POS.PNPO_CLASS, POS.POS_NO\n";
			SQL += "  , Setting_Status.Status_Path\n";
			SQL += "  ORDER BY DEPT.SECTOR_CODE,DEPT.DEPT_CODE,POS.PNPO_CLASS DESC,POS.POS_NO\n";

			SqlDataAdapter DA = new SqlDataAdapter(SQL, BL.ConnectionString());
			DataTable DT = new DataTable();
			DA.Fill(DT);
			return DT;

		}


		//------------KPI/Competency----------------
		protected void rpttd_Ass_MIN_Score_ItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
		{
			if (e.Item.ItemType != ListItemType.AlternatingItem & e.Item.ItemType != ListItemType.Item)
				return;
			Label lblAss_Min_Score = (Label) e.Item.FindControl("lblAss_Min_Score");
			Panel pnl_Ass_MIN_Score = (Panel)e.Item.FindControl("pnl_Ass_MIN_Score");
            Image img_Check = (Image)e.Item.FindControl("img_Check");

            HtmlTableCell cel_KPI = (HtmlTableCell)e.Item.FindControl("cel_KPI");

            Panel icon_Ass_Black = (Panel)e.Item.FindControl("icon_Ass_Black");
            Panel icon_Ass_green = (Panel)e.Item.FindControl("icon_Ass_green");
            Panel icon_Ass_Red = (Panel)e.Item.FindControl("icon_Ass_Red");


            DataRowView drv = (DataRowView)e.Item.DataItem;
			
			//------------ตำแหน่งหลักเพื่อเปรียบเทียบ----------
			if ((drv ["Property"].ToString ()) == "A_Property") {
				if (Information.IsNumeric(drv["Year_Score"])) {
					if (GL.CINT (drv["Year_Score"]) ==  Convert.ToInt32(0)) {
                        lblAss_Min_Score.Text = " ไม่ระบุคะแนน";
					} else {
                        lblAss_Min_Score.Text = "ไม่น้อยกว่า " + GL.StringFormatNumber(GL.CDBL (drv["Year_Score"])) + " คะแนน";
					}
				} else {
                    lblAss_Min_Score.Text = " ไม่ระบุคะแนน";
				}

				img_Check.Visible = false;
				pnl_Ass_MIN_Score.Visible = true;

				if (string.IsNullOrEmpty(POS_NO))
					pnl_Ass_MIN_Score.Visible = false;
				else
					pnl_Ass_MIN_Score.Visible = true;
				cel_KPI.Style["background-color"] = "#f8f8f8";


				icon_Ass_Black.Visible = true;
				icon_Ass_green.Visible = false;
				icon_Ass_Red.Visible = false;

			//------------พนักงานที่เลือกเพื่อจะเปรียบเทียบ----------
			} else if ((drv["Property"].ToString ()) == "B_Employee") {
				if (Information.IsNumeric(drv["Year_Score"])) {
                    lblAss_Min_Score.Text = " " + GL.StringFormatNumber(GL.CDBL (drv["Year_Score"])) + " คะแนน";
				} else {
                    lblAss_Min_Score.Text = GL.StringFormatNumber(0, 2) + " คะแนน";
				}

				pnl_Ass_MIN_Score.Visible = true;

				if (GL.CDBL  (Check_Ass_MIN_Score) <= GL.CDBL ((drv["Year_Score"]))) {
					img_Check.Visible = false;
					cel_KPI.Style["background-color"] = "#E3F9E5";

					icon_Ass_Black.Visible = false;
					icon_Ass_green.Visible = true;
					icon_Ass_Red.Visible = false;
				} else {
					img_Check.Visible = false;
					cel_KPI.Style["background-color"] = "#FEE4E4";
					icon_Ass_Black.Visible = false;
					icon_Ass_green.Visible = false;
					icon_Ass_Red.Visible = true;
				}
				cel_KPI.Style["text-align"] = "Left";

                

			//------------เพิ่มพนักงาน----------
			} else {

				img_Check.Visible = false;
				pnl_Ass_MIN_Score.Visible = false;
			}

		}
		//------------LEAVE----------------
		protected void rpttd_Leave_ItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
		{
			if (e.Item.ItemType != ListItemType.AlternatingItem & e.Item.ItemType != ListItemType.Item)
				return;
            Label lblLeave = (Label)e.Item.FindControl("lblLeave");
            Panel pnl_Leave = (Panel)e.Item.FindControl("pnl_Leave");
            Image img_Check = (Image)e.Item.FindControl("img_Check");
            HtmlTableCell cel_Leave = (HtmlTableCell)e.Item.FindControl("cel_Leave");

            Panel icon_Leave_Black = (Panel)e.Item.FindControl("icon_Leave_Black");
            Panel icon_Leave_green = (Panel)e.Item.FindControl("icon_Leave_green");
            Panel icon_Leave_Red = (Panel)e.Item.FindControl("icon_Leave_Red");

            DataRowView drv = (DataRowView)e.Item.DataItem;
			
			//------------ตำแหน่งหลักเพื่อเปรียบเทียบ----------
			if ((drv ["Property"].ToString ()) == "A_Property") {
				if (Information.IsNumeric(drv["LEAVE_All"])) {
					if (GL.CINT (drv["LEAVE_All"]) == Convert.ToInt32(0)) {
						lblLeave.Text = " ไม่ระบุจำนวนวันลา(กิจ/ป่วย)";
					} else {
						lblLeave.Text = "ไม่เกิน " + GL.StringFormatNumber(GL.CDBL (drv["LEAVE_All"])) + " วัน";
					}
				} else {
					lblLeave.Text = " ไม่ระบุจำนวนวันลา";
				}

				img_Check.Visible = false;
				pnl_Leave.Visible = true;
				cel_Leave.Style["background-color"] = "#f8f8f8";
				if (string.IsNullOrEmpty(POS_NO))
					pnl_Leave.Visible = false;
				else
					pnl_Leave.Visible = true;

				icon_Leave_Black.Visible = true;
				icon_Leave_green.Visible = false;
				icon_Leave_Red.Visible = false;

			//------------พนักงานที่เลือกเพื่อจะเปรียบเทียบ----------
			} else if ((drv["Property"].ToString ()) == "B_Employee") {
				if (Information.IsNumeric(drv["LEAVE_All"])) {
					lblLeave.Text = GL.StringFormatNumber(GL.CDBL(drv["LEAVE_All"])) + " วัน";
				} else {
					lblLeave.Text = " ไม่มีวันลา ";
					//GL.StringFormatNumber(0, 2) & " วัน"
				}
				pnl_Leave.Visible = true;

				if (GL.CDBL (Check_Leave )>= GL.CINT (drv["LEAVE_All"]) ){
					img_Check.Visible = false;
					cel_Leave.Style["background-color"] = "#E3F9E5";

					icon_Leave_Black.Visible = false;
					icon_Leave_green.Visible = true;
					icon_Leave_Red.Visible = false;
				} else {
					img_Check.Visible = false;
					cel_Leave.Style["background-color"] = "#FEE4E4";

					icon_Leave_Black.Visible = false;
					icon_Leave_green.Visible = false;
					icon_Leave_Red.Visible = true;
				}
				cel_Leave.Style["text-align"] = "Left";
			//------------เพิ่มพนักงาน----------
			} else {

				img_Check.Visible = false;
				pnl_Leave.Visible = false;

			}

		}

		//------------Course----------------
		//-----------Property----------------
		public DataTable BindCourse_Property(string PSNL_NO)
		{
			string Setting_POS_NO = "";
			if (string.IsNullOrEmpty(POS_NO)) {
				Setting_POS_NO = " WHERE  0=1 ";
			} else {
				Setting_POS_NO = "  WHERE Training.POS_NO ='" + POS_NO + "'";
			}

			string SQL = "";
			SQL += "   SELECT Training.POS_No,COURSE.COURSE_ID,COURSE.COURSE_DESC , '2' STSTUS_Course \n";
			SQL += "   FROM tb_Path_Setting_Training Training\n";
			SQL += "   LEFT JOIN tb_PK_COURSE COURSE ON Training.COURSE_ID=COURSE.COURSE_ID\n";
			SQL += "   " + Setting_POS_NO + "\n";
			SQL += "   ORDER BY Training.POS_No,COURSE.COURSE_ID,COURSE.COURSE_DESC\n";
			SqlDataAdapter DA = new SqlDataAdapter(SQL, BL.ConnectionString());
			DataTable DT = new DataTable();
			DA.Fill(DT);

            Check_Course = DT.Rows.Count.ToString();
			//Check_Test = DT_Check.Rows(0).Item("")


			return DT;

		}
		//-----------Compare----------------
		public DataTable BindCourse_Compare(string PSNL_NO)
		{
			string Setting_POS_NO = "";
			if (string.IsNullOrEmpty(POS_NO)) {
				Setting_POS_NO = " WHERE  0=1 ";
			} else {
				Setting_POS_NO = "  WHERE tb_Path_Setting_Training.POS_No='" + POS_NO + "'";
			}

			string Setting_PSNL_NO = "";
			if (string.IsNullOrEmpty(PSNL_NO)) {
				Setting_PSNL_NO = " WHERE  0=1 ";
			} else {
				Setting_PSNL_NO = "  WHERE tb_PK_EMP_SELECT.PNPS_PSNL_NO='" + PSNL_NO + "'";
			}

			string SQL = "";
			SQL += " SELECT \n";
			SQL += " tb_Path_Setting_Training.POS_No \n";
			SQL += " ,tb_Path_Setting_Training.Course_ID\n";
			SQL += " , Course.COURSE_DESC\n";
			SQL += " ,TB.PNPS_PSNL_NO\n";
			SQL += " ,CASE WHEN TB.PNPS_PSNL_NO IS NOT NULL  THEN 1 ELSE 0 END STSTUS_Course\n";
			SQL += " FROM tb_Path_Setting_Training LEFT JOIN \n";
			SQL += " \n";
			SQL += " (\n";
			SQL += " SELECT \n";
			SQL += " Training.POS_No \n";
			SQL += " ,Training.Course_ID \n";
			SQL += " ,tb_PK_EMP_SELECT.PNPS_PSNL_NO\n";
			SQL += " ,CASE WHEN Training.Course_ID=tb_PK_EMP_SELECT.COURSE_ID THEN 1 ELSE 0 END STSTUS_Course\n";
			SQL += " \n";
			SQL += " FROM tb_PK_EMP_SELECT \n";
			SQL += " LEFT  JOIN tb_Path_Setting_Training Training  ON Training.Course_ID=tb_PK_EMP_SELECT.COURSE_ID\n";
			SQL += " \n";
			//SQL &= " WHERE tb_PK_EMP_SELECT.PNPS_PSNL_NO=" & PSNL_NO & vbLf
			SQL += " " + Setting_PSNL_NO + "\n";
			SQL += " Group BY \n";
			SQL += " Training.POS_No \n";
			SQL += " ,Training.Course_ID \n";
			SQL += " ,tb_PK_EMP_SELECT.PNPS_PSNL_NO\n";
			SQL += " ,tb_PK_EMP_SELECT.COURSE_ID\n";
			SQL += " ) AS TB\n";
			SQL += " ON tb.Course_ID=tb_Path_Setting_Training.Course_ID\n";
			SQL += " Left JOIN  tb_PK_COURSE Course ON tb_Path_Setting_Training.Course_ID=Course.COURSE_ID \n";
			SQL += " \n";
			//SQL &= " WHERE tb_Path_Setting_Training.POS_No=" & POS_NO & vbLf
			SQL += " " + Setting_POS_NO + "\n";
			SQL += " GROUP BY tb_Path_Setting_Training.POS_No,tb_Path_Setting_Training.Course_ID,TB.PNPS_PSNL_NO,TB.STSTUS_Course, Course.COURSE_DESC\n";
			SQL += " ";

			SqlDataAdapter DA = new SqlDataAdapter(SQL, BL.ConnectionString());
			DataTable DT = new DataTable();
			DA.Fill(DT);
			return DT;

		}


		protected void rpttd_Course_ItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
		{
			if (e.Item.ItemType != ListItemType.AlternatingItem & e.Item.ItemType != ListItemType.Item)
				return;
			//---------------Course-----------------------
			Label lblCount_Course =(Label) e.Item.FindControl("lblCount_Course");
			Repeater rptDetail_Course =(Repeater) e.Item.FindControl("rptDetail_Course");

			Panel pnl_Course =(Panel) e.Item.FindControl("pnl_Course");
			Image img_Check =(Image) e.Item.FindControl("img_Check");
			HtmlTableCell cel_Course =(HtmlTableCell) e.Item.FindControl("cel_Course");

			Panel icon_Course_Black =(Panel) e.Item.FindControl("icon_Course_Black");
			Panel icon_Course_green =(Panel) e.Item.FindControl("icon_Course_green");
			Panel icon_Course_Red =(Panel) e.Item.FindControl("icon_Course_Red");

			DataTable DT_Course_Compare = null;
            DataRowView drv = (DataRowView)e.Item.DataItem;

			//------------ตำแหน่งหลักเพื่อเปรียบเทียบ----------
			if ((drv ["Property"].ToString ()) == "A_Property") {
                DT_Course_Compare = BindCourse_Property(drv["PSNL_NO"].ToString());

				img_Check.Visible = false;
				pnl_Course.Visible = true;
				if (string.IsNullOrEmpty(POS_NO))
					pnl_Course.Visible = false;
				else
					pnl_Course.Visible = true;
				cel_Course.Style["background-color"] = "#f8f8f8";
			//------------พนักงานที่เลือกเพื่อจะเปรียบเทียบ----------
			} else if ((drv["Property"].ToString ()) == "B_Employee") {
                DT_Course_Compare = BindCourse_Compare(drv["PSNL_NO"].ToString());

				pnl_Course.Visible = true;
				cel_Course.Style["text-align"] = "Left";
			//------------เพิ่มพนักงาน----------
			} else {
                DT_Course_Compare = BindCourse_Compare(drv["PSNL_NO"].ToString());
				img_Check.Visible = false;
				pnl_Course.Visible = false;
			}

            lblCount_Course.Text = Conversion.Val(DT_Course_Compare.Rows.Count).ToString();
			rptDetail_Course.ItemDataBound += rptDetail_Course_ItemDataBound;
			rptDetail_Course.DataSource = DT_Course_Compare;
			rptDetail_Course.DataBind();

			if ((drv ["Property"].ToString ()) == "A_Property") {
                Check_Course = DT_Course_Compare.Rows.Count.ToString();
				icon_Course_Black.Visible = true;
				icon_Course_green.Visible = false;
				icon_Course_Red.Visible = false;
			//------------พนักงานที่เลือกเพื่อจะเปรียบเทียบ----------
			} else if ((drv["Property"].ToString ()) == "B_Employee") {
				DT_Course_Compare.DefaultView.RowFilter = " STSTUS_Course=1";
				if (GL.CINT (DT_Course_Compare.DefaultView.Count) == GL.CINT (Check_Course)) {
					img_Check.Visible = false;
					cel_Course.Style["background-color"] = "#E3F9E5";
					icon_Course_Black.Visible = false;
					icon_Course_green.Visible = true;
					icon_Course_Red.Visible = false;
				} else {
					img_Check.Visible = false;
					cel_Course.Style["background-color"] = "#FEE4E4";
					icon_Course_Black.Visible = false;
					icon_Course_green.Visible = false;
					icon_Course_Red.Visible = true;
				}

			}

		}

		protected void rptDetail_Course_ItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
		{
			if (e.Item.ItemType != ListItemType.AlternatingItem & e.Item.ItemType != ListItemType.Item)
				return;
            Label lblCourse = (Label)e.Item.FindControl("lblCourse");
            Label lblCourse_Status = (Label)e.Item.FindControl("lblCourse_Status");
            DataRowView drv = (DataRowView)e.Item.DataItem;
			lblCourse.Text = drv["COURSE_DESC"].ToString();
			if (GL.CINT (drv["STSTUS_Course"]) == 0) {
				lblCourse_Status.Text = " (ไม่ผ่าน)";
				lblCourse_Status.Style["color"] = "Red";
				//class="icon-book" style="color:#c42f0a;"
			} else if (GL.CINT (drv["STSTUS_Course"]) == 1) {
				lblCourse_Status.Text = " (ผ่าน)";
				lblCourse_Status.Style["color"] = "Green";
			} else {
				lblCourse_Status.Text = "";
			}
		}


		//------------Test----------------
		//-----------Property----------------
		public DataTable BindTest_Property(string PSNL_NO)
		{
			string Setting_POS_NO = "";
			if (string.IsNullOrEmpty(POS_NO)) {
				Setting_POS_NO = " WHERE  0=1 ";
			} else {
				Setting_POS_NO = "  WHERE POS_NO ='" + POS_NO + "'";
			}

			string SQL = "";
			SQL += " SELECT Setting_Test.POS_No,Test.CPSM_SUBJECT_CODE,Test.CPSM_SUBJECT_NAME,Setting_Test.Min_Score, '2' Pass_Score\n";
			SQL += " FROM tb_Path_Setting_Test Setting_Test\n";
			SQL += " LEFT JOIN tb_CP_SUBJECT_MASTER Test ON Setting_Test.SUBJECT_CODE=Test.CPSM_SUBJECT_CODE\n";
			//SQL &= " WHERE POS_NO =" & POS_NO & vbLf
			SQL += " " + Setting_POS_NO + "\n";
			SQL += " ORDER BY Setting_Test.POS_No,Test.CPSM_SUBJECT_CODE,Test.CPSM_SUBJECT_NAME\n";
			SqlDataAdapter DA = new SqlDataAdapter(SQL, BL.ConnectionString());
			DataTable DT = new DataTable();
			DA.Fill(DT);
			return DT;
		}
		//-----------Compare----------------
		public DataTable BindTest_Compare(string PSNL_NO)
		{
			string Setting_POS_NO = "";
			if (string.IsNullOrEmpty(POS_NO)) {
				Setting_POS_NO = " WHERE  0=1 ";
			} else {
				Setting_POS_NO = " WHERE  tb_Path_Setting_Test.POS_No='" + POS_NO + "'";
			}

			string Setting_PSNL_NO = "";
			if (string.IsNullOrEmpty(PSNL_NO)) {
				Setting_PSNL_NO = " WHERE  0=1 ";
			} else {
				Setting_PSNL_NO = " WHERE tb_CP_SUBJECT_PERSONAL.CPSP_PERSONAL_NO='" + PSNL_NO + "'";
			}

			string SQL = "";
			SQL += " SELECT  \n";
			SQL += " --*  \n";
			SQL += " tb_Path_Setting_Test.POS_No \n";
			SQL += " ,tb_Path_Setting_Test.SUBJECT_CODE \n";
			SQL += " , SUBJECT_MASTER.CPSM_SUBJECT_NAME \n";
			SQL += " ,isnull(TB.CPSP_POINT,0) CPSP_POINT \n";
			SQL += " ,CASE WHEN ISNULL(TB.CPSP_POINT,0) >= ISNULL(tb_Path_Setting_Test.Min_Score,0) THEN 1 ELSE 0 END Pass_Score \n";
			SQL += " ,TB.CPSP_PERSONAL_NO \n";
			SQL += " ,CASE WHEN TB.CPSP_PERSONAL_NO IS NOT NULL  THEN 1 ELSE 0 END STSTUS_Test \n";
			SQL += " FROM tb_Path_Setting_Test LEFT JOIN  \n";
			SQL += "  \n";
			SQL += " ( \n";
			SQL += " SELECT  \n";
			SQL += " Test.POS_No \n";
			SQL += " ,Test.SUBJECT_CODE \n";
			SQL += " ,isnull(tb_CP_SUBJECT_PERSONAL.CPSP_POINT,0) CPSP_POINT \n";
			SQL += " ,tb_CP_SUBJECT_PERSONAL.CPSP_PERSONAL_NO \n";
			SQL += " ,CASE WHEN Test.SUBJECT_CODE=tb_CP_SUBJECT_PERSONAL.CPSP_SUBJECT_CODE THEN 1 ELSE 0 END STSTUS_Course \n";
			SQL += "  \n";
			SQL += " FROM tb_CP_SUBJECT_PERSONAL \n";
			SQL += " LEFT  JOIN tb_Path_Setting_Test Test  ON Test.SUBJECT_CODE=tb_CP_SUBJECT_PERSONAL.CPSP_SUBJECT_CODE \n";
			SQL += "  \n";
			//SQL &= " WHERE tb_CP_SUBJECT_PERSONAL.CPSP_PERSONAL_NO=" & PSNL_NO & vbLf
			SQL += " " + Setting_PSNL_NO + "\n";
			SQL += " GROUP BY  \n";
			SQL += " Test.POS_No \n";
			SQL += " ,Test.SUBJECT_CODE  \n";
			SQL += " ,tb_CP_SUBJECT_PERSONAL.CPSP_POINT \n";
			SQL += " ,tb_CP_SUBJECT_PERSONAL.CPSP_PERSONAL_NO \n";
			SQL += " ,tb_CP_SUBJECT_PERSONAL.CPSP_SUBJECT_CODE \n";
			SQL += " ) AS TB \n";
			SQL += " ON tb.SUBJECT_CODE=tb_Path_Setting_Test.SUBJECT_CODE \n";
			SQL += " Left JOIN  tb_CP_SUBJECT_MASTER SUBJECT_MASTER ON tb_Path_Setting_Test.SUBJECT_CODE=SUBJECT_MASTER.CPSM_SUBJECT_CODE  \n";
			SQL += "  \n";
			//SQL &= " WHERE  tb_Path_Setting_Test.POS_No=" & POS_NO & vbLf
			SQL += " " + Setting_POS_NO + "\n";
			SQL += "  \n";
			SQL += " GROUP BY  \n";
			SQL += " tb_Path_Setting_Test.POS_No \n";
			SQL += " ,tb_Path_Setting_Test.SUBJECT_CODE \n";
			SQL += " , SUBJECT_MASTER.CPSM_SUBJECT_NAME \n";
			SQL += " ,TB.CPSP_PERSONAL_NO \n";
			SQL += " ,tb_Path_Setting_Test.Min_Score \n";
			SQL += " ,TB.CPSP_POINT \n";

			SqlDataAdapter DA = new SqlDataAdapter(SQL, BL.ConnectionString());
			DataTable DT = new DataTable();
			DA.Fill(DT);
			return DT;

		}

		protected void rpttd_Test_ItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
		{
			if (e.Item.ItemType != ListItemType.AlternatingItem & e.Item.ItemType != ListItemType.Item)
				return;
			//---------------Test-----------------------
			Label lblCount_Test =(Label)e.Item.FindControl("lblCount_Test");
			Repeater rptDetail_Test =(Repeater) e.Item.FindControl("rptDetail_Test");
			Panel pnl_Test =(Panel)  e.Item.FindControl("pnl_Test");
			Image img_Check =(Image)  e.Item.FindControl("img_Check");
			HtmlTableCell cel_Test =(HtmlTableCell)  e.Item.FindControl("cel_Test");

			Panel icon_Test_Black =(Panel) e.Item.FindControl("icon_Test_Black");
			Panel icon_Test_green =(Panel) e.Item.FindControl("icon_Test_green");
			Panel icon_Test_Red =(Panel) e.Item.FindControl("icon_Test_Red");

            DataRowView drv = (DataRowView)e.Item.DataItem;
			
			DataTable DT_Test_Compare = null;
			//------------ตำแหน่งหลักเพื่อเปรียบเทียบ----------
			if ((drv ["Property"].ToString ()) == "A_Property") {
                DT_Test_Compare = BindTest_Property(drv["PSNL_NO"].ToString());

				img_Check.Visible = false;
				pnl_Test.Visible = true;
				if (string.IsNullOrEmpty(POS_NO))
					pnl_Test.Visible = false;
				else
					pnl_Test.Visible = true;
				cel_Test.Style["background-color"] = "#f8f8f8";

				icon_Test_Black.Visible = true;
				icon_Test_green.Visible = false;
				icon_Test_Red.Visible = false;

			//------------พนักงานที่เลือกเพื่อจะเปรียบเทียบ----------
			} else if ((drv["Property"].ToString ()) == "B_Employee") {
                DT_Test_Compare = BindTest_Compare(drv["PSNL_NO"].ToString());

				pnl_Test.Visible = true;
				cel_Test.Style["text-align"] = "Left";
			//------------เพิ่มพนักงาน----------
			} else {
                DT_Test_Compare = BindTest_Compare(drv["PSNL_NO"].ToString());
				img_Check.Visible = false;
				pnl_Test.Visible = false;
			}

            lblCount_Test.Text = Conversion.Val(DT_Test_Compare.Rows.Count).ToString();
			rptDetail_Test.ItemDataBound += rptDetail_Test_ItemDataBound;
			rptDetail_Test.DataSource = DT_Test_Compare;
			rptDetail_Test.DataBind();


			if ((drv ["Property"].ToString ()) == "A_Property") {
                Check_Test = DT_Test_Compare.Rows.Count.ToString();
			//------------พนักงานที่เลือกเพื่อจะเปรียบเทียบ----------
			} else if ((drv["Property"].ToString ()) == "B_Employee") {
				DT_Test_Compare.DefaultView.RowFilter = " STSTUS_Test=1";
				if (GL.CINT (DT_Test_Compare.DefaultView.Count) == GL.CINT (Check_Test)) {
					img_Check.Visible = false;
					cel_Test.Style["background-color"] = "#E3F9E5";
					icon_Test_Black.Visible = false;
					icon_Test_green.Visible = true;
					icon_Test_Red.Visible = false;
				} else {
					img_Check.Visible = false;
					cel_Test.Style["background-color"] = "#FEE4E4";
					icon_Test_Black.Visible = false;
					icon_Test_green.Visible = false;
					icon_Test_Red.Visible = true;
				}
			}
		}

		protected void rptDetail_Test_ItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
		{
			if (e.Item.ItemType != ListItemType.AlternatingItem & e.Item.ItemType != ListItemType.Item)
				return;
            Label lblTest = (Label)e.Item.FindControl("lblTest");

            DataRowView drv = (DataRowView)e.Item.DataItem;
			
			lblTest.Text = drv["CPSM_SUBJECT_NAME"].ToString();

            Label lblMin_Score = (Label)e.Item.FindControl("lblMin_Score");

            if (GL.CINT(drv["Pass_Score"]) == Convert.ToInt32 (0)) {
				lblMin_Score.Text = " (ไม่ผ่าน)";
				lblMin_Score.Style["color"] = "Red";
			} else if (GL.CINT(drv["Pass_Score"]) == Convert.ToInt32 (1)) {
				lblMin_Score.Text = GL.StringFormatNumber(GL.CDBL (drv["CPSP_POINT"])) + " คะแนน";
				lblMin_Score.Style["color"] = "Green";
			} else {
				lblMin_Score.Text = GL.StringFormatNumber(GL.CDBL (drv["Min_Score"])) + " คะแนน";

			}
		}


		//------------PUNISH----------------
		protected void rpttd_Punishment_ItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
		{
			if (e.Item.ItemType != ListItemType.AlternatingItem & e.Item.ItemType != ListItemType.Item)
				return;
			Label lblPunishment =(Label) e.Item.FindControl("lblPunishment");
            Panel pnl_Punishment = (Panel)e.Item.FindControl("pnl_Punishment");
            Image img_Check = (Image)e.Item.FindControl("img_Check");
            HtmlTableCell cel_Punishment = (HtmlTableCell)e.Item.FindControl("cel_Punishment");

            Panel icon_Punis_Black = (Panel)e.Item.FindControl("icon_Punis_Black");
            Panel icon_Punis_green = (Panel)e.Item.FindControl("icon_Punis_green");
            Panel icon_Punis_Red = (Panel)e.Item.FindControl("icon_Punis_Red");

            DataRowView drv = (DataRowView)e.Item.DataItem;

			//------------ตำแหน่งหลักเพื่อเปรียบเทียบ----------
			if ((drv["Property"].ToString ()) == "A_Property") {
				switch (drv["History_PUNISH"].ToString ()) {
					case "ไม่เคยรับโทษ":
						lblPunishment.Text = " ต้องไม่มีโทษ 1 ปี ย้อนหลัง ";
						break;
					case "เคยรับโทษ":
						lblPunishment.Text = " ไม่ระบุโทษทางวินัย";
						break;
				}

				img_Check.Visible = false;
				pnl_Punishment.Visible = true;
				if (string.IsNullOrEmpty(POS_NO))
					pnl_Punishment.Visible = false;
				else
					pnl_Punishment.Visible = true;
				cel_Punishment.Style["background-color"] = "#f8f8f8";

				icon_Punis_Black.Visible = true;
				icon_Punis_green.Visible = false;
				icon_Punis_Red.Visible = false;
			//------------พนักงานที่เลือกเพื่อจะเปรียบเทียบ----------
			} else if ((drv["Property"].ToString ()) == "B_Employee") {
				switch ((drv["History_PUNISH"]).ToString ()) {
					case "":
						lblPunishment.Text = "  ";
						break;
					case "ไม่เคยรับโทษ":
						lblPunishment.Text = " ไม่เคยรับโทษ ";
						lblPunishment.ForeColor = System.Drawing.Color.Green;
						break;
					case "เคยรับโทษ":
						lblPunishment.Text = " เคยรับโทษ ";
						lblPunishment.ForeColor = System.Drawing.Color.Red;
						break;
				}
				pnl_Punishment.Visible = true;
				if (Check_Punishment.ToString() == "True") {
                    if (drv["History_Status_PUNISH"].ToString() == "False")
                    {
						img_Check.Visible = false;
						cel_Punishment.Style["background-color"] = "#FEE4E4";
						icon_Punis_Black.Visible = false;
						icon_Punis_green.Visible = false;
						icon_Punis_Red.Visible = true;
					} else {
						img_Check.Visible = false;
						cel_Punishment.Style["background-color"] = "#E3F9E5";
						icon_Punis_Black.Visible = false;
						icon_Punis_green.Visible = true;
						icon_Punis_Red.Visible = false;
					}
				} else {
					//img_Check.Visible = True
					img_Check.Visible = false;
					cel_Punishment.Style["background-color"] = "#FEE4E4";
					icon_Punis_Black.Visible = false;
					icon_Punis_green.Visible = false;
					icon_Punis_Red.Visible = true;
				}
				cel_Punishment.Style["text-align"] = "Left";

			//------------เพิ่มพนักงาน----------
			} else {
				img_Check.Visible = false;
				pnl_Punishment.Visible = false;
			}

		}



		private void BindPersonal()
		{
			string SQL = "";

			SQL += "  DECLARE @R_Year As Int=2558\n";
			SQL += "  DECLARE @R_Round As Int=1\n";
			SQL += "  Select DISTINCT\n";
			SQL += "  Header.SECTOR_CODE, Header.SECTOR_NAME\n";
			SQL += "  ,Header.DEPT_CODE, Header.DEPT_Name, Header.PSNL_NO, Header.PSNL_Fullname\n";
			SQL += "  ,PSN.PNPS_CLASS  PNPS_CLASS\n";
			SQL += "  ,Header.POS_NO,Header.POS_Name\n";
			SQL += "  ,Header.FLD_ID FN_ID,Header.FLD_Name  FLD_Name\n";
			SQL += "  ,CASE WHEN History_PUNISH.Count_PUNISH > 0 THEN 'เคยรับโทษ' ELSE 'ไม่เคยรับโทษ' END  History_PUNISH\n";

			SQL += "  --History_Status\n";
			SQL += "  ,ISNULL(Yearly_Result.Year_Score,0) Year_Score\n";
			SQL += "  ,ISNULL(Yearly_Result.Leave_Day  ,0) Leave_ALL\n";

			SQL += "  FROM vw_PN_PSNL PSN\n";
			SQL += "  INNER JOIN vw_RPT_KPI_Status Header ON PSN .PSNL_NO =Header.PSNL_NO \n";
			SQL += "  \t\t\t\t\t\tAND Header.R_Year=@R_Year AND Header.R_Round=@R_Round\n";

			SQL += "   -----------------------History_PUNISH---------------------------------------\n";
			SQL += "  LEFT JOIN vw_Path_Count_History_PUNISH History_PUNISH ON PSN.PSNL_NO=History_PUNISH.PSNL_NO\n";
			SQL += "  LEFT JOIN vw_RPT_Yearly_Result Yearly_Result  ON PSN.PSNL_NO=Yearly_Result.PSNL_NO AND Yearly_Result.R_Year =2557\n";
			SQL += "        WHERE PSN.PSNL_NO Is Not NULL \n";

			if (!string.IsNullOrEmpty(POS_NO_List)) {
				SQL += " AND PSN.POS_NO IN (" + POS_NO_List + ") \n";
			} else {
				SQL += " AND 0=1\n";
			}

			if (!string.IsNullOrEmpty(PSNL_NO)) {
				SQL += " AND PSN.PSNL_NO NOT IN (" + PSNL_NO + ") \n";
			} else {
			}

			if (ddlSector.SelectedIndex > 0) {

				SQL += " AND  ( LEFT(Header.DEPT_CODE,2)='" + ddlSector.Items[ddlSector.SelectedIndex].Value + "' \n";
				SQL += " OR LEFT(Header.DEPT_CODE,4)='" + ddlSector.Items[ddlSector.SelectedIndex].Value + "'   \n";

				if (ddlSector.Items[ddlSector.SelectedIndex].Value == "3200") {
					SQL += " AND  Header.DEPT_CODE NOT IN ('32000300'))    \n";
				//-----------ฝ่ายโรงงานผลิตยาสูบ 3
				} else if (ddlSector.Items[ddlSector.SelectedIndex].Value == "3203") {
					SQL += " OR  Header.DEPT_CODE IN ('32000300','32030000','32030100','32030200','32030300','32030500'))  \n";
				//-----------ฝ่ายโรงงานผลิตยาสูบ 4
				} else if (ddlSector.Items[ddlSector.SelectedIndex].Value == "3204") {
					SQL += " OR  Header.DEPT_CODE IN ('32040000','32040100','32040200','32040300','32040500','32040300'))  \n";
				//-----------ฝ่ายโรงงานผลิตยาสูบ 5
				} else if (ddlSector.Items[ddlSector.SelectedIndex].Value == "3205") {
					SQL += " OR  Header.DEPT_CODE IN ('32050000','32050100','32050200','32050300','32050500'))\t  \n";
				} else {
					SQL += ")   \n";
				}

			}

			if (!string.IsNullOrEmpty(txt_Search_Organize_Add.Text)) {
				SQL += " AND (Header.SECTOR_NAME LIKE '%" + txt_Search_Organize_Add.Text.Replace("'", "''") + "%' OR \n";
				SQL += " Header.DEPT_NAME LIKE '%" + txt_Search_Organize_Add.Text.Replace("'", "''") + "%')\n";
			}
			if (!string.IsNullOrEmpty(txt_Search_Name.Text)) {
				SQL += " AND (Header.PSNL_NO LIKE '%" + txt_Search_Name.Text.Replace("'", "''") + "%'\n";
				SQL += " OR Header.PSNL_Fullname LIKE '%" + txt_Search_Name.Text.Replace("'", "''") + "%')\n";
			}
			if (!string.IsNullOrEmpty(txt_Search_POS.Text)) {
				SQL += " AND (Header.POS_No LIKE '%" + txt_Search_Name.Text.Replace("'", "''") + "%'\n";
				SQL += " OR Header.POS_Name LIKE '%" + txt_Search_Name.Text.Replace("'", "''") + "%')\n";
			}

			SQL += "  ORDER BY Header.DEPT_CODE,PNPS_CLASS DESC,Header.POS_NO";



			SqlDataAdapter DA = new SqlDataAdapter(SQL, BL.ConnectionString());
			DataTable DT = new DataTable();
			DA.Fill(DT);

			Session["View_Personal_Compare"] = DT;
			Pager_PSNL_NO_List.SesssionSourceName = "View_Personal_Compare";
			Pager_PSNL_NO_List.RenderLayout();

			if (DT.Rows.Count == 0) {
				lblCountPosNo.Text = "ไม่พบรายการดังกล่าว";
			} else {
				lblCountPosNo.Text = "พบ " + GL.StringFormatNumber(DT.Rows.Count, 0) + " รายการ";
			}
		}

		protected void Pager_PSNL_NO_List_PageChanging(PageNavigation Sender)
		{
			Pager_PSNL_NO_List.TheRepeater = rptPersonal;
		}
		protected void btnDisplay_AddPSNL_NO_Click(object sender, System.EventArgs e)
		{
			if (string.IsNullOrEmpty(POS_NO)) {
				ScriptManager.RegisterStartupScript(this.Page, typeof(string), "Alert", "showAlert('กำหนดตำแหน่งเปรียบเทียบ  ก่อนเลือกพนักงาน');", true);
				return;
			}
			string SQL = "";

			SQL += "  SELECT To_POS_No  From_POS_No \n";
			SQL += "  ,  From_POS_No To_POS_No \n";
			SQL += "  ,vw_PN_Position.POS_NO\n";
			SQL += "  ,vw_PN_Position.DEPT_NAME\n";
			SQL += "  ,vw_PN_Position.PNPO_CLASS\n";
			SQL += "  ,vw_PN_Position.MGR_NAME\n";
			SQL += "  FROM  tb_Path_Setting_Route Setting_Route\n";
			SQL += "  LEFT JOIN tb_Path_Setting Path_Setting  ON Path_Setting.POS_No=Setting_Route.To_POS_No\n";
			SQL += "  INNER JOIN vw_PN_Position ON vw_PN_Position.POS_NO=Setting_Route.From_POS_No\n";
			SQL += "  WHERE To_POS_No =" + POS_NO + "\n";

			SqlDataAdapter DA = new SqlDataAdapter(SQL, BL.ConnectionString());
			DataTable DT = new DataTable();
			DA.Fill(DT);
			string To_POS_No = "";
			PSLN_NO_Current_Compare = "";
			if (DT.Rows.Count > 0) {

				for (int i = 0; i <= DT.Rows.Count - 1; i++) {
					To_POS_No += "'" + DT.Rows[i]["POS_NO"] + "'" + ",";
				}

				if (!string.IsNullOrEmpty(To_POS_No)) {
					PSLN_NO_Current_Compare += To_POS_No.Substring(0, To_POS_No.Length - 1);
				} else {
					PSLN_NO_Current_Compare += "";
				}
			}

			BindPersonal();
			ModalAddPSNL_NO.Visible = true;
			ScriptManager.RegisterStartupScript(this.Page, typeof(string), "Focus", "document.getElementById('" + txt_Search_Organize_Add.ClientID + "').focus();", true);

		}

		protected void rptPersonal_ItemCommand(object source, System.Web.UI.WebControls.RepeaterCommandEventArgs e)
		{
			switch (e.CommandName) {
				case "SelectPosAll":
                    Button btnView = (Button)e.Item.FindControl("btnView");
					string Pos_Select = "";
					Pos_Select = btnView.CommandArgument;
					//--------คลิกเลือกพนักงานเพือเปรียบเทียบ----------
					if (string.IsNullOrEmpty(PSNL_NO)) {
						PSNL_NO = Pos_Select;
					} else {
						PSNL_NO += "," + Pos_Select;
					}
					ModalAddPSNL_NO.Visible = false;
					BindCompareDetail();
					break;
			}

		}

		string LastSector = "";
		string LastDept = "";
		protected void rptPersonal_ItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
		{
			if (e.Item.ItemType != ListItemType.Item & e.Item.ItemType != ListItemType.AlternatingItem)
				return;
			Label lblPSNDept =(Label) e.Item.FindControl("lblPSNDept");
			Label lblPSNName = (Label)e.Item.FindControl("lblPSNName");
			 Label lblPSNPos = (Label)e.Item.FindControl("lblPSNPos");
			 Label lblPSNClass = (Label)e.Item.FindControl("lblPSNClass");
             Label lblHistory = (Label)e.Item.FindControl("lblHistory");
             Label lblTMP_PSNDept = (Label)e.Item.FindControl("lblTMP_PSNDept");
             Button btnView = (Button)e.Item.FindControl("btnView");
            Label lblHistory_PUNISH = (Label)e.Item.FindControl("lblHistory_PUNISH");

			string lblYear_Score = "";
			string lblLeave_Day = "";
			string lblHistory_Year = "";
			string lblHistory_Pos = "";
			string lblHistory_Course = "";

            DataRowView drv = (DataRowView)e.Item.DataItem;

			lblTMP_PSNDept.Text = drv["DEPT_NAME"].ToString();
			if (LastDept != drv["DEPT_NAME"].ToString()) {
				lblPSNDept.Text = drv["DEPT_NAME"].ToString();

				LastDept = drv["DEPT_NAME"].ToString();
			}
			if (!GL.IsEqualNull(drv["PSNL_Fullname"])) {
				lblPSNName.Text = drv["PSNL_NO"] + " : " + drv["PSNL_Fullname"];
			} else {
				lblPSNName.Text = "-";
			}
			lblPSNPos.Text = drv["POS_Name"].ToString();
			lblPSNClass.Text = GL.CINT (drv["PNPS_CLASS"]).ToString ();

			if (GL.CINT (drv["Year_Score"]) > 0) {
				lblYear_Score = " ผลการประเมิน KPI/COMP ย้อนหลัง 3 รอบ ได้เฉลี่ย " + GL.StringFormatNumber(GL.CDBL (drv["Year_Score"])) + " คะแนน |";
			} else {
				lblYear_Score = " ไม่มีผลการประเมิน KPI/COMP ย้อนหลัง 3 รอบล่าสุด |";
			}
			//---4---
			if (GL.CINT (drv["LEAVE_All"]) > 0) {
				lblLeave_Day = " มีจำนวนวันลา(ป่วย/กิจ) 3 ปีงบประมาณย้อนหลัง รวม " + GL.StringFormatNumber(GL.CDBL (drv["LEAVE_All"])) + " วัน |";
			} else {
				lblLeave_Day = " ไม่มีจำนวนวันลา(ป่วย/กิจ) 3 ปีงบประมาณย้อนหลัง |";
			}
			//---5---
			lblHistory_Course = "";
			//If drv["History_Course"] > 0 Then
			//    lblHistory_Course = " ผ่านการฝึกอบรม " & drv["History_Course"].ToString() & " หลักสูตร "
			//Else
			//    lblHistory_Course = " ไม่เคยเข้าร่วมการฝึกอบรม "
			//End If
			//---6---
			lblHistory_PUNISH.Text = "  ";
			switch ((drv["History_PUNISH"]).ToString ()) {
				case "ไม่เคยรับโทษ":
					lblHistory_PUNISH.Text = " ไม่เคยรับโทษ ";
					lblHistory_PUNISH.ForeColor = System.Drawing.Color.Green;
					break;
				case "เคยรับโทษ":
					lblHistory_PUNISH.Text = " เคยรับโทษ ";
					lblHistory_PUNISH.ForeColor = System.Drawing.Color.Red;
					break;
			}
			lblHistory.Text = lblHistory_Year + lblHistory_Pos + lblYear_Score + "</br>" + lblLeave_Day + lblHistory_Course;
			btnView.CommandArgument = drv["PSNL_NO"].ToString();
		}
		//Protected Sub btnClose_Pos_Click(sender As Object, e As System.EventArgs) Handles btnClose_Pos.Click
		//    ModalPost.Visible = False
		//End Sub

		//Protected Sub btnClose_PSNL_NO_Click(sender As Object, e As System.EventArgs) Handles btnClose_PSNL_NO.Click
		//    ModalAddPSNL_NO.Visible = False
		//End Sub

		protected void rpttd_btn_ItemCommand(object source, System.Web.UI.WebControls.RepeaterCommandEventArgs e)
		{
			switch (e.CommandName) {
				case "Remove":
					DataTable DT = CurrentDataTable();
					DT.Rows.RemoveAt(e.Item.ItemIndex);
					if (DT.Rows.Count > 0) {
						PSNL_NO = "";
						for (int i = 0; i <= DT.Rows.Count - 2; i++) {
							if (i > 0) {
								if (string.IsNullOrEmpty(PSNL_NO)) {
									PSNL_NO += "'" + DT.Rows[i]["PSNL_NO"].ToString() + "'";
								} else {
									PSNL_NO += "," + "'" + DT.Rows[i]["PSNL_NO"].ToString() + "'";
								}
							}
						}
					}
					BindCompareDetail();

					break;
			}
		}

		protected void rpttd_btn_ItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
		{
            HtmlTableCell cel_btn = (HtmlTableCell)e.Item.FindControl("cel_btn");
            LinkButton btn_Remove = (LinkButton)e.Item.FindControl("btn_Remove");
            HtmlAnchor lnk_ShowDetail = (HtmlAnchor)e.Item.FindControl("lnk_ShowDetail");

            Label lblTMP_POS_NO = (Label)e.Item.FindControl("lblTMP_POS_NO");

            DataRowView drv = (DataRowView)e.Item.DataItem;

			lblTMP_POS_NO.Text = drv["PSNL_NO"].ToString();
			lblTMP_POS_NO.Visible = false;
			lnk_ShowDetail.HRef = "CP_View_Personal.aspx?PSNL_NO=" + drv["PSNL_NO"].ToString();
			cel_btn.Style["text-align"] = "center";
			//------------ตำแหน่งหลักเพื่อเปรียบเทียบ----------
            if (drv["Property"].ToString() == "A_Property")
            {
				btn_Remove.Visible = false;
				lnk_ShowDetail.Visible = false;
			//------------พนักงานที่เลือกเพื่อจะเปรียบเทียบ----------
            }
            else if (drv["Property"].ToString() == "B_Employee")
            {
				btn_Remove.Visible = true;
				lnk_ShowDetail.Visible = true;

			//------------เพิ่มพนักงาน----------
			} else {
				btn_Remove.Visible = false;
				lnk_ShowDetail.Visible = false;
			}
		}

		private DataTable CurrentDataTable()
		{
			DataTable DT = new DataTable();
			DT.Columns.Add("PSNL_NO");
			DT.Columns.Add("NO");
			int i = 0;
			foreach (RepeaterItem Item in rpttd_btn.Items) {
				if (Item.ItemType != ListItemType.AlternatingItem & Item.ItemType != ListItemType.Item)
					continue;
				Label lblTMP_POS_NO =(Label)Item.FindControl("lblTMP_POS_NO");
				DataRow DR = DT.NewRow();
				DR["PSNL_NO"] = lblTMP_POS_NO.Text;
				DR["NO"] = i + 1;
				DT.Rows.Add(DR);
				i = i + 1;
			}

			return (DT);
		}




		protected void btnSearch_PSNL_NO_Click(object sender, System.EventArgs e)
		{
			BindPersonal();
		}

		protected void btnModalPost_Close_ServerClick(object sender, System.EventArgs e)
		{
			ModalPost.Visible = false;

		}

		protected void btnModalAddPSNL_NO_Close_ServerClick(object sender, System.EventArgs e)
		{
			ModalAddPSNL_NO.Visible = false;
		}

		protected void txtSearchPos_TextChanged(object sender, System.EventArgs e)
		{
			BindPostList();
		}
		public CP_Compare_Personal()
		{
			Load += Page_Load;
		}


	}
}

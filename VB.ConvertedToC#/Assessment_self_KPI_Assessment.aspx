﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPage.Master" CodeBehind="Assessment_self_KPI_Assessment.aspx.cs" Inherits="VB.Assessment_self_KPI_Assessment" %>

<%@ Register src="WUC_DateReporter.ascx" tagname="WUC_DateReporter" tagprefix="uc1" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<asp:UpdatePanel ID="UpdatePanel1" runat="server">      
<ContentTemplate>

<asp:ToolkitScriptManager ID="toolkit1" runat="server"></asp:ToolkitScriptManager>

<div class="container-fluid">
				<!-- BEGIN PAGE HEADER-->
				<div class="row-fluid" id="fixedPageHeader">
					<div class="span12"> 
						<!-- BEGIN PAGE TITLE & BREADCRUMB-->
						<h3 class="page-user-md">การประเมินผลตัวชี้วัดตนเอง (KPI) <font color="blue">(ScreenID : S-EMP-03)</font>
                        <asp:Label ID="lblReportTime" Font-Size="14px" ForeColor="Green" runat="Server"></asp:Label>
                        </h3>					
									
						<ul class="breadcrumb">
                            <li>
                            	<i class="icon-user"></i> <a href="javascript:;">การประเมินตนเอง</a><i class="icon-angle-right"></i>
                            </li>
                        	<li>
                        	    <i class="icon-check"></i> <a href="javascript:;">การประเมินผลตัวชี้วัด (KPI)</a>
                        	</li>                        	
                        	
                            <uc1:WUC_DateReporter ID="WUC_DateReporter1" runat="server" />
                        </ul>
						<!-- END PAGE TITLE & BREADCRUMB-->
									        
				     </div>

				
			    </div>
		        <!-- END PAGE HEADER-->
				
                                
				<div class="portlet-body form">										
			        <div class="btn-group pull-right"> 
			                <div class="btn-group">
                                <a data-toggle="dropdown" class="btn  red dropdown-toggle"  data-trigger="hover" data-container="body" data-placement="top" data-content="Popover body goes here! Popover body goes here!" data-original-title="Popover in top">แก้ไข <i class="icon-angle-down"></i></a>										
			                    <ul class="dropdown-menu pull-right" >                     
                                    <li><a ><asp:LinkButton id="lnkCopyAll" OnClick="lnkCopyAll_Click" runat ="server"  ><i class="icon-copy"></i> คัดลอกทั้งหมด</asp:LinkButton></a></li>	
                                    <%--<li><a ><asp:LinkButton id="lnkPasteAll" OnClick="lnkPasteAll_Click" runat ="server" ><i class="icon-paste"></i> วางแทนที่ทั้งหมด</asp:LinkButton></a></li>--%>											
			                    </ul>
                            </div>

                        <div class="btn-group">                                 
			                <a data-toggle="dropdown" class="btn dropdown-toggle">พิมพ์ <i class="icon-angle-down"></i></a>										
			                <ul class="dropdown-menu pull-right">                                                       
                                <li><a id="btnPDF" runat="server" target="_blank">รูปแบบ PDF</a></li>
                                <li><a id="btnExcel" runat="server" target="_blank">รูปแบบ Excel</a></li>											
			                </ul>
                        </div>  
		            </div>
			        <div id="boxAlertEdit" runat ="server"  class="alert" style =" margin-top :-10px;">
							<li>
								สำหรับ <b style ="">การคัดลอกและวาง</b> แบบประเมินตัวชี้วัด (KPI)  ทั้งตาราง
							</li>
							<li>
								โดยสามารถ เปิดหน้าแบบประเมินที่ต้องการหรือเลือกจากรอบประเมินที่ต้องการคัดลอก คลิกปุ่ม <b>แก้ไข > คัดลอกทั้งหมด</b>
							</li>
							<li>
								<u>วิธีการวาง</u> เปิดหน้า กำหนดแบบประเมินตัวชี้วัด (KPI) ที่ต้องการ วางแบบประเมิน คลิกปุ่ม <b>แก้ไข > วางแทนที่ทั้งหมด</b>
							</li>
					</div>


			        <h3 class="form-section"><i class="icon-th-list"></i> 			        
		                <asp:Label ID="lblName" runat="server"></asp:Label>
		                    สำหรับรอบการประเมิน
		                    <asp:DropDownList ID="ddlRound" OnSelectedIndexChanged="ddlRound_SelectedIndexChanged" runat="server" AutoPostBack="true" CssClass="medium m-wrap header" style="font-size:20px;">
				            </asp:DropDownList>
				            (<asp:Label ID="lblKPIStatus" runat="server" KPI_Status="0"></asp:Label>)        				                                            
		                </h3>
			        
					<div class="form-horizontal form-view">
    			        
    			        
					     <div class="row-fluid" style="border-bottom:1px solid #EEEEEE;">
					        <div class="span4 ">
					            <div class="control-group">
								    <label class="control-label" style="width:100px;">ผู้รับการประเมิน :</label>
								    <div class="controls" style="margin-left: 120px;">
									    <asp:Label ID="lblPSNName" runat="server" CssClass="text bold"></asp:Label>
								    </div>
							    </div>
					        </div>
					        <div class="span3 ">
					            <div class="control-group">
								    <label class="control-label" style="width:100px;">ตำแหน่ง :</label>
								    <div class="controls" style="margin-left: 120px;">
									    <asp:Label ID="lblPSNPos" runat="server" CssClass="text bold"></asp:Label>
								    </div>
							    </div>
					        </div>
					        <div class="span5 ">
					            <div class="control-group">
								    <label class="control-label" style="width:100px;">สังกัด :</label>
								    <div class="controls" style="margin-left: 120px;">
									    <asp:Label ID="lblPSNDept" runat="server" CssClass="text bold"></asp:Label>
								    </div>
							    </div>
					        </div>
					     </div>
					     
					      <div class="row-fluid" style="margin-top:10px;">
					        <div class="span4 ">
					            <div class="control-group">
								    <label class="control-label" style="width:100px;">ผู้ประเมิน :</label>
								    <div class="controls" style="margin-left: 120px;">
									    <asp:DropDownList ID="ddlASSName" OnSelectedIndexChanged="ddlASSName_SelectedIndexChanged" runat="Server" AutoPostBack="True" style="font-weight:bold; color:Black; border:none;">									        
									    </asp:DropDownList>
									</div>
							    </div>
					        </div>
					        <div class="span3 ">
					            <div class="control-group">
								    <label class="control-label" style="width:100px;">ตำแหน่ง :</label>
								    <div class="controls" style="margin-left: 120px;">
									    <asp:Label ID="lblASSPos" runat="server" CssClass="text bold"></asp:Label>
								    </div>
							    </div>
					        </div>
					        <div class="span5 ">
					            <div class="control-group">
								    <label class="control-label" style="width:100px;">สังกัด :</label>
								    <div class="controls" style="margin-left: 120px;">
									    <asp:Label ID="lblASSDept" runat="server" CssClass="text bold"></asp:Label>
								    </div>
							    </div>
					        </div>
					     </div>
    			    </div>
    			
    			    <!-- BEGIN FORM-->
				    <div class="portlet-body no-more-tables">
				               
				                        <table class="table table-bordered no-more-tables" style="border-bottom:none; background-color:White;">
									        <thead>
										        <tr>
											        <th rowspan="2" style="text-align:center; background-color:Silver;"> ลำดับ</th>
											        <th rowspan="2" style="text-align:center; background-color:Silver;"> งาน</th>
											        <th rowspan="2" style="text-align:center; background-color:Silver;"> ตัวชี้วัดผลงาน</th>
											        <th colspan="5" style="text-align:center; background-color:Silver;"> คะแนนตามระดับค่าเป้าหมาย</th>
											        <th rowspan="2" style="text-align:center; background-color:Silver;"> คะแนน</th>
											        <th rowspan="2" style="text-align:center; background-color:Silver;"> น้ำหนัก(%)</th>											        
										        </tr>
                                                <tr>
										          <th class="AssLevel1" style="text-align:center; font-weight:bold; vertical-align:middle;">1</th>
										          <th class="AssLevel2" style="text-align:center; font-weight:bold; vertical-align:middle;">2</th>
										          <th class="AssLevel3" style="text-align:center; font-weight:bold; vertical-align:middle;">3</th>
										          <th class="AssLevel4" style="text-align:center; font-weight:bold; vertical-align:middle;">4</th>
										          <th class="AssLevel5" style="text-align:center; font-weight:bold; vertical-align:middle;">5</th>
								              </tr>
									        </thead>
									        <tbody>

                                            <asp:Panel ID="pnlKPI" runat="server">
									            <asp:Repeater ID="rptAss" OnItemCommand="rptAss_ItemCommand" OnItemDataBound="rptAss_ItemDataBound" runat="server">
									                <ItemTemplate>
									                        <tr>
										                      <td data-title="ลำดับ" rowspan="6" id="ass_no" runat="server" style="text-align:center; font-weight:bold; vertical-align:top;">001</td>
										                      <td data-title="งาน" id="job" class="TextLevel0" runat="server" style="background-color:#f8f8f8; ">xxxxxxxxxxxx xxxxxxxxxxxx</td>
										                      <td data-title="ตัวชี้วัดผลงาน" id="target" runat="server" class="TextLevel0" style="background-color:#f8f8f8; ">xxxxxxxxxxxx xxxxxxxxxxxx</td>
										                      <td data-title="คลิกเพื่อประเมิน 1 คะแนน" title="Click เพื่อเลือก" id="choice1" class="TextLevel0" runat="server" style="cursor:pointer;">xxxxxxxxxxxx xxxxxxxxxxxx xxxxxxxxxxxx xxxxxxxxxxxx xxxxxxxxxxxx xxxxxxxxxxxx</td>
                                                              <td data-title="คลิกเพื่อประเมิน 2 คะแนน" title="Click เพื่อเลือก" id="choice2" class="TextLevel0" runat="server" style="cursor:pointer;">xxxxxxxxxxxx xxxxxxxxxxxx xxxxxxxxxxxx xxxxxxxxxxxx xxxxxxxxxxxx xxxxxxxxxxxx</td>
                                                              <td data-title="คลิกเพื่อประเมิน 3 คะแนน" title="Click เพื่อเลือก" id="choice3" class="TextLevel0" runat="server" style="cursor:pointer;">xxxxxxxxxxxx xxxxxxxxxxxx xxxxxxxxxxxx xxxxxxxxxxxx xxxxxxxxxxxx xxxxxxxxxxxx</td>
                                                              <td data-title="คลิกเพื่อประเมิน 4 คะแนน" title="Click เพื่อเลือก" id="choice4" class="TextLevel0" runat="server" style="cursor:pointer;">xxxxxxxxxxxx xxxxxxxxxxxx xxxxxxxxxxxx xxxxxxxxxxxx xxxxxxxxxxxx xxxxxxxxxxxx</td>
                                                              <td data-title="คลิกเพื่อประเมิน 5 คะแนน" title="Click เพื่อเลือก" id="choice5" class="TextLevel0" runat="server" style="cursor:pointer;">xxxxxxxxxxxx xxxxxxxxxxxx xxxxxxxxxxxx xxxxxxxxxxxx xxxxxxxxxxxx xxxxxxxxxxxx</td>
										                      <td data-title="คะแนน" rowspan="5" title="คะแนน" id="mark" runat="server" style="text-align:center; font-size:16px; font-weight:bold; vertical-align:middle;">-</td>
										                      <td data-title="น้ำหนัก" rowspan="5" title="น้ำหนัก" id="weight" runat="server" style="text-align:center; font-size:16px; font-weight:bold; vertical-align:middle;">10%</td>
            										        </tr>
									                        <tr>
									                           <td colspan="2" data-title="หมายเหตุ (ผู้ถูกประเมิน)" style=" font:inherit; background-color:#f8f8f8; text-align:center; font-size:14px;">หมายเหตุ (ผู้ถูกประเมิน)
									                           <asp:Button ID="btn_Remark" runat="server" style="display:none; " CommandName="Remark" />
									                           </td>
									                           <td colspan="5" data-title="หมายเหตุ (ผู้ถูกประเมิน)" style="padding:0px; background-color:White;"> <asp:Textbox MaxLength="2000" ID="txtPSN"  runat="server" style="width:96%; padding:5px; background-color:Transparent; height:53px; border:0px none transparent;margin:0px;" TextMode="Multiline" Placeholder="ถ้ามี"></asp:Textbox></td>
									                        </tr>
        									                <tr>
        									                    <td data-title="หมายเหตุ (ผู้ประเมิน)" colspan="2" style=" font:inherit; background-color:#f8f8f8; text-align:center; font-size:14px;">หมายเหตุ <br>(ผู้ประเมิน)</td>
									                            <td data-title="หมายเหตุ (ผู้ประเมิน)" colspan="5" style="padding:0px; background-color:White;"><asp:Textbox  MaxLength="2000" ID="txtMGR" runat="server" style="width:96%; padding:5px; background-color:Transparent; height:53px; border:0px none transparent;  margin:0px;" TextMode="Multiline" ReadOnly="True" Placeholder="ถ้ามี"></asp:Textbox></td>
        									                </tr>
									                        <tr>
									                              <td id="cel_officer" runat="server" class="TextLevel0" colspan="2" style="text-align:center; font-weight:bold; padding:0px !important; height:10px !important;" >การประเมินตนเอง</td>
									                              <td data-title="คลิกเพื่อประเมิน 1 คะแนน" title="คะแนนประเมินตนเอง" id="selOfficialColor1" runat="server" style="cursor:pointer; padding:0px !important; height:10px !important; border-right:0px none;"><asp:Button ID="btn1" runat="server" style="display:none;" CommandName="Select" /></td>
                                                                  <td data-title="คลิกเพื่อประเมิน 2 คะแนน" title="คะแนนประเมินตนเอง" id="selOfficialColor2" runat="server" style="cursor:pointer; padding:0px !important; height:10px !important; border-right:0px none; border-left:0px none;"><asp:Button ID="btn2" runat="server" style="display:none;" CommandName="Select" /></td>
                                                                  <td data-title="คลิกเพื่อประเมิน 3 คะแนน" title="คะแนนประเมินตนเอง" id="selOfficialColor3" runat="server" style="cursor:pointer; padding:0px !important; height:10px !important; border-right:0px none; border-left:0px none;"><asp:Button ID="btn3" runat="server" style="display:none;" CommandName="Select" /></td>
                                                                  <td data-title="คลิกเพื่อประเมิน 4 คะแนน" title="คะแนนประเมินตนเอง" id="selOfficialColor4" runat="server" style="cursor:pointer; padding:0px !important; height:10px !important; border-right:0px none; border-left:0px none;"><asp:Button ID="btn4" runat="server" style="display:none;" CommandName="Select" /></td>
                                                                  <td data-title="คลิกเพื่อประเมิน 5 คะแนน" title="คะแนนประเมินตนเอง" id="selOfficialColor5" runat="server" style="cursor:pointer; padding:0px !important; height:10px !important; border-left:0px none;"><asp:Button ID="btn5" runat="server" style="display:none;" CommandName="Select" /></td>
                                                            </tr>
                                                            <tr id="rowHeader" runat="server">
									                              <td id="cel_header" runat="server" class="TextLevel0" colspan="2" style="text-align:center; font-weight:bold; padding:0px !important; height:10px !important;" >ผู้บังคับบัญชาประเมิน</td>
									                              <td data-title="ผู้ประเมิน" title="คะแนนที่ได้จากผู้ประเมิน" id="selHeaderColor1" runat="server" style="padding:0px !important; height:10px !important; border-right:0px none;">&nbsp;</td>
                                                                  <td data-title="ผู้ประเมิน" title="คะแนนที่ได้จากผู้ประเมิน" id="selHeaderColor2" runat="server" style="padding:0px !important; height:10px !important; border-right:0px none; border-left:0px none;">&nbsp;</td>
                                                                  <td data-title="ผู้ประเมิน" title="คะแนนที่ได้จากผู้ประเมิน" id="selHeaderColor3" runat="server" style="padding:0px !important; height:10px !important; border-right:0px none; border-left:0px none;">&nbsp;</td>
                                                                  <td data-title="ผู้ประเมิน" title="คะแนนที่ได้จากผู้ประเมิน" id="selHeaderColor4" runat="server" style="padding:0px !important; height:10px !important; border-right:0px none; border-left:0px none;">&nbsp;</td>
                                                                  <td data-title="ผู้ประเมิน" title="คะแนนที่ได้จากผู้ประเมิน" id="selHeaderColor5" runat="server" style="padding:0px !important; height:10px !important; border-left:0px none;">&nbsp;</td>
                                                            </tr>
									                </ItemTemplate>
									                <FooterTemplate>
									                
									                 <tfoot>
									                        <tr>
									                            <td colspan="8" style="text-align:center; background-color:#EEEEEE; font-size:14px; font-weight:bold;">
									                            ผลรวมการประมิน
									                            </td>
									                            <td colspan="2" style="text-align:center; background-color:#EEEEEE; font-size:14px; font-weight:bold;">
									                            คะแนนที่ได้
									                            </td>
									                        </tr>
									                        <tr>
									                            <th colspan="3" style="text-align:center; background-color:#EEEEEE; border-bottom:1px solid #DDDDDD; font-size:14px; font-weight:bold;" id="cell_total" runat="server">
									                           รวม
									                            </th>
									                              <th id="cell_sum_1" runat="server" style="text-align:center; border-bottom:1px solid #DDDDDD;; font-size:14px; font-weight:bold; vertical-align:middle;">-</th>
										                          <th id="cell_sum_2" runat="server" style="text-align:center; border-bottom:1px solid #DDDDDD;; font-size:14px; font-weight:bold; vertical-align:middle;">-</th>
										                          <th id="cell_sum_3" runat="server" style="text-align:center; border-bottom:1px solid #DDDDDD;; font-size:14px; font-weight:bold; vertical-align:middle;">-</th>
										                          <th id="cell_sum_4" runat="server" style="text-align:center; border-bottom:1px solid #DDDDDD;; font-size:14px; font-weight:bold; vertical-align:middle;">-</th>
										                          <th id="cell_sum_5" runat="server" style="text-align:center; border-bottom:1px solid #DDDDDD;; font-size:14px; font-weight:bold; vertical-align:middle;">-</th>
								                                  <th id="cell_sum_raw" runat="server" style="text-align:center; border-bottom:1px solid #DDDDDD; font-size:16px; font-weight:bold;" colspan="2">-</th>
									                        </tr>
									                        <tr>
									                            <th colspan="8" style="background-color:#FFFFFF; border:none;"></th>
									                            <th style="text-align:center; background-color:#EEEEEE; font-size:14px; font-weight:bold;" colspan="2" >คิดเป็น</th>									                          
									                        </tr>
									                        <tr>
									                            <th colspan="8" style="background-color:#FFFFFF; border:none;"></th>
									                            <th id="cell_sum_mark" runat="server" style="text-align:center; font-size:24px; font-weight:bold;" colspan="2">-</th>									                          
									                        </tr>
									                    </tfoot>
									                
									                </FooterTemplate>
									            </asp:Repeater>
                                            </asp:Panel>
									        </tbody>
									        
								        </table>
								        
								                <asp:Panel CssClass="form-actions" id="pnlActivity" runat="server">
								                    
								                    <asp:Button ID="btnPreSend" OnClick="btnPreSend_Click" runat="server" CssClass="btn green" Text="ยืนยันส่งผลการประเมินให้หัวหน้า/ผู้ประเมิน (หลังจากส่งผลการประเมินแล้วคุณไม่สามารถแก้ไขได้)" />													
								                    <asp:Button ID="btnSend" OnClick="btnSend_Click"  runat="server" style="display:none" Text="" />
								                   
								                </asp:Panel>
												    
												
							</div>
						
				    <!-- END FORM-->  
				   
			    </div>	
						 
</div>	

</ContentTemplate>           
</asp:UpdatePanel>					 
</asp:Content>


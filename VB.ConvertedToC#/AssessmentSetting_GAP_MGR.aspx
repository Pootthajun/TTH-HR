﻿<%@ Page  Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPage.Master" CodeBehind="AssessmentSetting_GAP_MGR.aspx.cs" Inherits="VB.AssessmentSetting_GAP_MGR" %>

<%@ Register src="WUC_DateReporter.ascx" tagname="WUC_DateReporter" tagprefix="uc1" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<%@ Register src="PageNavigation.ascx" tagname="PageNavigation" tagprefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<asp:ToolkitScriptManager ID="toolkit1" runat="server"></asp:ToolkitScriptManager>
<asp:UpdatePanel ID="UpdatePanel1" runat="server">
<ContentTemplate>
<div class="container-fluid">
				<!-- BEGIN PAGE HEADER-->
				<div class="row-fluid" id="fixedPageHeader">
					<div class="span12"> 
						<!-- BEGIN PAGE TITLE & BREADCRUMB-->
						<h3 class="page-user-md">หลักสูตรการพัฒนาสำหรับช่องว่างสมรรถนะตามสายระดับ (Managerial) <font color="blue">(ScreenID : S-HDR-16)</font></h3>					
									
						<ul class="breadcrumb">
                            
                        	<li>
                        	    <i class="icon-book"></i> <a href="javascript:;">หลักสูตรการพัฒนา</a><i class="icon-angle-right"></i>
                        	</li>
                        	<li>
                        	    <i class="icon-th-list"></i> <a href="javascript:;">สมรรถนะตามสายระดับ (Managerial)</a></i>
                        	</li>
                        	
                            <uc1:WUC_DateReporter ID="WUC_DateReporter1" runat="server" />
                        </ul>
						<!-- END PAGE TITLE & BREADCRUMB-->
									        
				     </div>

				
			    </div>
		        <!-- END PAGE HEADER-->
				
				
		    <asp:Panel ID="pnlList" runat="server" Visible="True" DefaultButton="btnSearch">
                <div class="row-fluid">
					
					<div class="span12">
						<!-- BEGIN SAMPLE TABLE PORTLET-->
						            	        <div class="row-fluid form-horizontal">
												     <div class="span4 ">
														<div class="control-group">
													        <label class="control-label"><i class="icon-retweet"></i> สำหรับรอบการประเมิน</label>
													        <div class="controls">
														            <asp:DropDownList ID="ddlRound" OnSelectedIndexChanged="ddlRound_SelectedIndexChanged" runat="server" AutoPostBack="true" CssClass="medium m-wrap">
														            </asp:DropDownList>
													        </div>
												        </div>
													</div>
													
													<div class="span6 ">
														<div class="control-group">
													        <label class="control-label"><i class="icon-male"></i> ประเภทพนักงาน</label>
													        <div class="controls">
														            <asp:DropDownList ID="ddlPSNType" OnSelectedIndexChanged="Search" runat="server" AutoPostBack="true" CssClass="medium m-wrap">
														             </asp:DropDownList>														       
													        </div>
												        </div>
													</div>									   
												</div>
												
												<div class="row-fluid form-horizontal">												    
												    <div class="span4 ">
														<div class="control-group">
													        <label class="control-label"><i class="icon-list-ol"></i> กลุ่มระดับ</label>
													        <div class="controls">
														        <asp:DropDownList ID="ddlClassGroup" OnSelectedIndexChanged="Search" runat="server" AutoPostBack="true" CssClass="medium m-wrap">
														        </asp:DropDownList>
													        </div>
												        </div>
													</div>
													<div class="span6 ">
														<div class="control-group">
													        <label class="control-label">ชื่อหลักสูตร</label>
													        <div class="controls">
													            <asp:TextBox ID="txtGAP" runat="server" CssClass="m-wrap medium" placeholder="ค้นหาจากชื่อหลักสูตร" AutoPostBack="True"></asp:TextBox>
													            <asp:Button ID="btnSearch" OnClick="Search" runat="server" Text="" style="display:none;" />
														    </div>
												        </div>
													</div>	
												</div>
												<div class="row-fluid form-horizontal">
												    <div class="span4 ">
														<div class="control-group">
													        <label class="control-label"></label>
													        <div class="controls">
														        <asp:CheckBox ID="chkBlank" OnCheckedChanged="Search" runat="server" Text="" AutoPostBack="true" /> 
														        <span style="font-size:14px; position:relative; top:3px;">แสดงเฉพาะที่ยังไม่ได้กำหนด</span>
													        </div>
												        </div>
													</div>
												</div>
								            
							<div class="portlet-body no-more-tables">
								<asp:Label ID="lblCountList" runat="server" Font-Size="14px" Font-Bold="true"></asp:Label>
								<table class="table table-full-width table-advance dataTable no-more-tables table-hover">
									<thead>
										<tr>
											<th><i class="icon-list-ol"></i> กลุ่มระดับ</th>
											<th><i class="icon-male"></i> ประเภทพนักงาน</th>
											<th><i class="icon-th-list"></i> การประเมิน(ข้อ)</th>
											<th><i class="icon-book"></i> หลักสูตรการพัฒนา</th>
											<th><i class="icon-bolt"></i> ดำเนินการ</th>
										</tr>
									</thead>
									<tbody>
										<asp:Repeater ID="rptList" OnItemCommand="rptList_ItemCommand" OnItemDataBound="rptList_ItemDataBound" runat="server">
										    <ItemTemplate>
										            <tr>
											            <td data-title="กลุ่มระดับ"><asp:Label ID="lblClassGroup" runat="server"></asp:Label></td>
											            <td data-title="ประเภทพนักงาน"><asp:Label ID="lblPSNType" runat="server"></asp:Label></td>
											            <td data-title="การประเมิน(ข้อ)"><asp:Label ID="lblTotalCOMP" runat="server"></asp:Label></td>
											            <td data-title="หลักสูตรการพัฒนา"><i class="icon-ok-sign" id="iconGAP" runat="server"></i></td>
											            <td data-title="ดำเนินการ">
											            <asp:Button ID="btnEdit" runat="server" CssClass="btn mini blue" Text="กำหนดหลักสูตร" CommandName="Edit" /> 
											            <asp:Button ID="btnDelete" runat="server" CssClass="btn red mini"  Text="ยกเลิกหลักสูตร" CommandName="Delete" />
											            <asp:ConfirmButtonExtender TargetControlID="btnDelete" ID="cfm_Delete" runat="server"></asp:ConfirmButtonExtender>
											            </td>
										            </tr>
										    </ItemTemplate>
										</asp:Repeater>
									</tbody>
								</table>
								
								<asp:PageNavigation ID="Pager" OnPageChanging="Pager_PageChanging" MaximunPageCount="10" PageSize="20" runat="server" />
								
							</div>
						
						<!-- END SAMPLE TABLE PORTLET-->						
					</div>
                </div>
              </asp:Panel>
  
            <asp:Panel ID="pnlEdit" runat="server" >                
				<div class="portlet-body form">	
                    <div class="btn-group pull-left">
                        <asp:Button ID="btnBack_TOP" OnClick="btnCancel_Click" runat="server" CssClass="btn" Text="ย้อนกลับ" />
                    </div>	<br />										
			        <h3 class="form-section"><i class="icon-th-list"></i> <asp:Label ID="lblYearRound" runat="server"></asp:Label>
			        สำหรับพนักงาน<asp:Label ID="lblPSNType" runat="server" PSNL_Type_Code="-1"></asp:Label>
			        ระดับ <asp:Label ID="lblClassName" runat="server" CLSGP_ID="0"></asp:Label>
			        </h3>    					
				    <!-- BEGIN FORM-->
				        <div class="portlet-body no-more-tables" id="divView" runat="server">
				                        <table class="table table-bordered no-more-tables" style="background-color:White;">
									        <thead>
										        <tr>
											        <th rowspan="2" style="text-align:center;"> รหัส</th>
											        <th rowspan="2" style="text-align:center;"> สมรรถนะ</th>
											        <th rowspan="2" style="text-align:center;"> มาตรฐานพฤติกรรม</th>
											        <%--<th colspan="5" style="text-align:center;"> คะแนนตามระดับค่าเป้าหมาย</th>--%>
											        <th rowspan="2" style="text-align:center;"> หลักสูตรการพัฒนา</th>
											        
										        </tr>
                                                <%--<tr>
										          <th style="text-align:center;">1</th>
										          <th style="text-align:center;">2</th>
										          <th style="text-align:center;">3</th>
										          <th style="text-align:center;">4</th>
										          <th style="text-align:center;">5</th>
								              </tr>--%>
									        </thead>
									        <tbody>										        
										        <asp:Repeater ID="rptCOMP" OnItemCommand="rptCOMP_ItemCommand" OnItemDataBound="rptCOMP_ItemDataBound" runat="server">
										            <ItemTemplate>
							                                <tr>
										                      <td data-title="รหัส" id="ass_no" runat="server" style="text-align:center; font-weight:bold; vertical-align:top;">001</td>
										                      <td data-title="สมรรถนะ" id="COMP_Name" runat="server">xxxxxxxxxxxx xxxxxxxxxxxx</td>
										                      <td data-title="มาตรฐานพฤติกรรม" id="target" runat="server">xxxxxxxxxxxx xxxxxxxxxxxx</td>
										                      <%--<td data-title="ได้ 1 คะแนน" id="choice1" runat="server">xxxxxxxxxxxx xxxxxxxxxxxx xxxxxxxxxxxx xxxxxxxxxxxx xxxxxxxxxxxx xxxxxxxxxxxx</td>
                                                              <td data-title="ได้ 2 คะแนน" id="choice2" runat="server">xxxxxxxxxxxx xxxxxxxxxxxx xxxxxxxxxxxx xxxxxxxxxxxx xxxxxxxxxxxx xxxxxxxxxxxx</td>
                                                              <td data-title="ได้ 3 คะแนน" id="choice3" runat="server">xxxxxxxxxxxx xxxxxxxxxxxx xxxxxxxxxxxx xxxxxxxxxxxx xxxxxxxxxxxx xxxxxxxxxxxx</td>
                                                              <td data-title="ได้ 4 คะแนน" id="choice4" runat="server">xxxxxxxxxxxx xxxxxxxxxxxx xxxxxxxxxxxx xxxxxxxxxxxx xxxxxxxxxxxx xxxxxxxxxxxx</td>
                                                              <td data-title="ได้ 5 คะแนน" id="choice5" runat="server">xxxxxxxxxxxx xxxxxxxxxxxx xxxxxxxxxxxx xxxxxxxxxxxx xxxxxxxxxxxx xxxxxxxxxxxx</td>--%>
										                      <td data-title="หลักสูตรการพัฒนา">
										                        <asp:Repeater ID="rptGAP" runat="server"  OnItemCommand="rptGAP_ItemCommand" >
								                                    <ItemTemplate>									                    
											                                <div style="width:100%; min-width:200px; vertical-align:top; margin-bottom:10px;">
									                                                <asp:Label ID="lblName" runat="server" style="text-decoration:none; width:80%; float:left;"></asp:Label>
													                                <asp:Button ID="btnDelete" runat="server" CommandName="Delete" Text ="ลบ"  class="btn mini red" />
									                                                <a ID="lblDelete"  runat="server" class="btn mini red" style="float:right;display:none;">ลบ</a>
									                                        </div><br style="border-bottom:1px solid #eeeeee;">
                                                                            <asp:TextBox ID="txtDeleteID" runat="server" style="display:none;"></asp:TextBox>
								                                    </ItemTemplate>
								                                </asp:Repeater>
										                        <asp:Button CssClass="btn green mini" ID="btnAdd" runat="server" Text="เพิ่มหลักสูตร" CommandName="Add" />
										                      </td>										                      
									                        </tr>
							                        </ItemTemplate>
										        </asp:Repeater>					
									        </tbody>
								        </table>								        
						                <div class="form-actions">
										    <asp:Button CssClass="btn" runat="server" ID="btnCancel" OnClick="btnCancel_Click" Text="ย้อนกลับ" />
										</div>				                  								                
					</div>
						
				    <!-- END FORM-->  
				    
			        
			    </div>	
		    </asp:Panel>
		    
		                <asp:Panel CssClass="modal" style="top:5%; position:fixed; width:auto;" id="ModalGAP" runat="server" DefaultButton="btnSearchGAP"  >
				            <div class="modal-header">										
					            <h3>หลักสูตรการพัฒนา </h3>
					            <asp:Label ID="lblCOMPNo" runat="server" style="display:none;"></asp:Label>
				            </div>
				            <div class="modal-body">					            
					            <div class="row-fluid form-horizontal">
					                 <div class="span12 ">
									    <div class="control-group">
								            <label class="control-label" style="width:120px;"><i class="icon-book"></i> ชื่อหลักสูตร</label>
								            <div class="controls">
									            <asp:TextBox ID="txtSearchGAP" OnTextChanged="SearchGAP" runat="server" AutoPostBack="true" CssClass="m-wrap large" placeholder="ค้นหาจากรหัส/ชื่อหลักสูตร"></asp:TextBox>
									            <asp:Button ID="btnSearchGAP" OnClick="SearchGAP" runat="server" Text="" style="display:none;" />
								            </div>
							            </div>
								    </div>								       
					            </div>
					            <div class="portlet-body no-more-tables">
    								
								    <asp:Label ID="lblCountGAP" runat="server" Font-Size="14px" Font-Bold="true"></asp:Label>
								    <table class="table table-full-width  no-more-tables table-hover">
									    <thead>
										    <tr>
											    <th style="text-align:center;"><i class="icon-caret-right"></i> รหัส</th>
											    <th style="text-align:center;"><i class="icon-book"></i> หลักสูตร</th>											    
											    <th style="text-align:center;"><i class="icon-check"></i> เลือก</th>
										    </tr>										    
									    </thead>
									    <tbody>
										    <asp:Repeater ID="rptGAPDialog" OnItemCommand="rptGAPDialog_ItemCommand" OnItemDataBound="rptGAPDialog_ItemDataBound" runat="server">
									            <ItemTemplate>
    									            <tr>                                        
											            <td data-title="รหัส" style="text-align:center;"><asp:Label ID="lblNo" runat="server"></asp:Label></td>
											            <td data-title="หลักสูตร"><asp:Label ID="lblName" runat="server"></asp:Label></td>											           
											            <td data-title="ดำเนินการ" style="text-align:center;">
											                <asp:Button ID="btnGAPSelect" runat="server" CssClass="btn mini blue" CommandName="Select" Text="เลือก" /> 
										                </td>
										            </tr>	
									            </ItemTemplate>
									        </asp:Repeater>					
									    </tbody>
								    </table>
    								
								    <asp:PageNavigation ID="PagerGAP" OnPageChanging="PagerGAP_PageChanging" MaximunPageCount="10" PageSize="5" runat="server" />
							    </div>				            
				            </div>
				            <div class="modal-footer">
				                <asp:Button ID="btnClose" OnClick="btnClose_Click" runat="server" CssClass="btn" Text="ปิด" />									
				            </div>
			            </asp:Panel>
</div>	

</ContentTemplate>				                
</asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ScriptPlaceHolder" Runat="Server">
</asp:Content>


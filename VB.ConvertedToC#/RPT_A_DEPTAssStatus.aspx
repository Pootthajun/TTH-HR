﻿<%@ Page  Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPage.Master" CodeBehind="RPT_A_DEPTAssStatus.aspx.cs" Inherits="VB.RPT_A_DEPTAssStatus" %>

<%@ Register src="WUC_DateReporter.ascx" tagname="WUC_DateReporter" tagprefix="uc1" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<%@ Register src="PageNavigation.ascx" tagname="PageNavigation" tagprefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<asp:ToolkitScriptManager ID="toolkit1" runat="server"></asp:ToolkitScriptManager>

<asp:UpdatePanel ID="UpdatePanel1" runat="server">      
<ContentTemplate>
<div class="container-fluid">
				<!-- BEGIN PAGE HEADER-->
				<div class="row-fluid" id="fixedPageHeader">
					<div class="span12"> 
						<!-- BEGIN PAGE TITLE & BREADCRUMB-->
						<h3>สถานะการประเมินของหน่วยงาน (KPI+Competency)<font color="blue">(ScreenID : R-ALL-01)</font></h3>						
						<ul class="breadcrumb">
                            <li>
                            	<i class="icon-copy"></i> <a href="javascript:;">รายงาน</a><i class="icon-angle-right"></i>
                            </li>
                            <li>
                            	<i class="icon-copy"></i> <a href="javascript:;">การประเมินผลตัวชี้วัดและสมรรถนะ</a><i class="icon-angle-right"></i>
                            </li>
                            <li>
                        	    <i class="icon-check"></i> <a href="javascript:;">สถานะการประเมินของหน่วยงาน</a>
                        	</li>                      	
                        	
                            <uc1:WUC_DateReporter ID="WUC_DateReporter1" runat="server" />
                        </ul>
						<!-- END PAGE TITLE & BREADCRUMB-->
									        
				     </div>

				
			    </div>
		        <!-- END PAGE HEADER-->
				
                <asp:Panel ID="pnlList" runat="server" Visible="True" DefaultButton="btnSearch">
				<div class="row-fluid">
					
					<div class="span12">
						<!-- BEGIN SAMPLE TABLE PORTLET-->
                                <div class="row-fluid form-horizontal">
                			             <div class="btn-group pull-left">
                                            <asp:Button ID="btnSearch" OnClick="btnSearch_Click"  runat="server" CssClass="btn green" Text="ค้นหา" />
                                         </div>
						                 <div class="btn-group pull-right">                                    
								            <button data-toggle="dropdown" class="btn dropdown-toggle" id="Button1">พิมพ์ <i class="icon-angle-down"></i></button>										
								            <ul class="dropdown-menu pull-right">
                                               
                                                <li><a href="Print/RPT_A_DEPTAssStatus.aspx?Mode=PDF" target="_blank">รูปแบบ PDF</a></li>
                                                <li><a href="Print/RPT_A_DEPTAssStatus.aspx?Mode=EXCEL" target="_blank">รูปแบบ Excel</a></li>												
								            </ul>
							            </div>
							    </div>
						                 <div class="row-fluid form-horizontal">
						                             <div class="span5 ">
														<div class="control-group">
													        <label class="control-label"> รอบการประเมิน</label>
													        <div class="controls">
														        <asp:DropDownList ID="ddlRound" runat="server" CssClass="medium m-wrap">
							                                    </asp:DropDownList>
													        </div>
												        </div>
													</div>
																		   
    										</div>
    										<div class="row-fluid form-horizontal">		
												     <div class="span5 ">
														<div class="control-group">
													        <label class="control-label"> ฝ่าย</label>
													        <div class="controls">
														        <asp:DropDownList ID="ddlSector" runat="server" CssClass="medium m-wrap">
														        </asp:DropDownList>
													        </div>
												        </div>
													</div>	
													
													<div class="span5 ">
														<div class="control-group">
													        <label class="control-label"> ชื่อหน่วยงาน</label>
													        <div class="controls">
														        <asp:TextBox ID="txtDeptName" runat="server" CssClass="medium m-wrap" Placeholder="ค้นหาชื่อฝ่าย/หน่วยงาน"></asp:TextBox>		
													        </div>
												        </div>
													</div>
											</div>
                                            	
											<div class="row-fluid form-horizontal">
											    
											    
											        <div class="span5">
											            <div class="control-group">
										                    <label class="control-label">การประเมินตัวชี้วัด (KPI)</label>
										                    <div class="controls">
											                    <label class="checkbox">
                                                                <asp:CheckBox ID="chkKPINo" runat="server" Text=""/>&nbsp;ยังประเมินไม่เสร็จ
											                    </label>
											                    <label class="checkbox">
                                                                <asp:CheckBox ID="chkKPIYes" runat="server" Text=""/>&nbsp;ประเมินเสร็จสมบูรณ์
											                    </label>
										                    </div>
									                    </div>
											        </div>
											       
												    <div class="span6">
												        <div class="control-group">
													        <label class="control-label"> การประเมินสมรรถนะ (Competency)</label>
													        <div class="controls">
											                    <label class="checkbox">
                                                                <asp:CheckBox ID="chkCOMPNo" runat="server" Text=""/>&nbsp;ยังประเมินไม่เสร็จ
											                    </label>
											                    <label class="checkbox">
                                                                <asp:CheckBox ID="chkCOMPYes" runat="server" Text=""/>&nbsp;ประเมินเสร็จสมบูรณ์
											                    </label>
										                    </div>
												        </div>
													</div>
											</div>
											
								    
								            
							<div class="portlet-body no-more-tables">
								<asp:Label ID="lblCountPSN" runat="server" Font-Size="14px" Font-Bold="true"></asp:Label>
								&nbsp;
								<asp:Label ID="lblTitle" runat="server" Font-Size="14px" Font-Bold="true" Width="100%" style="text-align:center"></asp:Label>
								<table class="table table-full-width table-bordered dataTable table-hover">
									<thead>
										<tr>
											<th rowspan="3" style="text-align:center;"> ฝ่าย</th>
											<th rowspan="3" style="text-align:center;"> หน่วยงาน</th>
											<th colspan="5" style="text-align:center;"> จำนวนพนักงาน(คน)</th>	
										</tr>
										<tr>
										    <th rowspan="2" style="text-align:center;">ทั้งหมด</th>
										    <th colspan="2" style="text-align:center;">การประเมินตัวชี้วัด (KPI)</th>
											<th colspan="2" style="text-align:center;">การประเมินสมรรถนะ (Competency)</th>
										</tr>
										<tr>
										    <th style="text-align:center; color:green;"><i class="icon-ok"></i>เสร็จสมบูรณ์</th>
											<th style="text-align:center; color:Red;"><i class="icon-remove"></i>ยังไม่อนุมัติผล</th>
											<th style="text-align:center; color:green;"><i class="icon-ok"></i>เสร็จสมบูรณ์</th>
											<th style="text-align:center; color:Red;"><i class="icon-remove"></i>ยังไม่อนุมัติผล</th>
										</tr>
									</thead>
									<tbody>
										<asp:Repeater ID="rptASS" OnItemDataBound="rptASS_ItemDataBound" runat="server">
									        <ItemTemplate>
									            <tr>    
									                <td data-title="ฝ่าย" id="td1" runat="server"><asp:Label ID="lblSector" runat="server"></asp:Label></td> 
									                <td data-title="หน่วยงาน" id="td2" runat="server"><asp:Label ID="lblDept" runat="server"></asp:Label></td>                                    
											        <td data-title="รวม" id="td3" runat="server" style="text-align:center;"><asp:Label ID="lblTotal" runat="server"></asp:Label></td>
											        <td data-title="ตัวชี้วัดเสร็จสมบูรณ์" id="td4" runat="server" style="text-align:center;"><asp:Label ID="lbl_KPI_Yes" runat="server" ForeColor="green"></asp:Label></td>
											        <td data-title="ยังไม่อนุมัติผลตัวชี้วัด" id="td5" runat="server" style="text-align:center;"><asp:Label ID="lbl_KPI_No" runat="server" ForeColor="red"></asp:Label></td>
											        <td data-title="สมรรถนะเสร็จสมบูรณ์" id="td6" runat="server" style="text-align:center;"><asp:Label ID="lbl_COMP_Yes" runat="server" ForeColor="green"></asp:Label></td>
											        <td data-title="ยังไม่อนุมัติผลสมรรถนะ" id="td7" runat="server" style="text-align:center;"><asp:Label ID="lbl_COMP_No" runat="server" ForeColor="red"></asp:Label></td>
											    </tr>
									        </ItemTemplate>
										</asp:Repeater>
																							
									</tbody>
								</table>
								<asp:PageNavigation ID="Pager" OnPageChanging="Pager_PageChanging" MaximunPageCount="10" PageSize="20" runat="server" />
							</div>
					</div>
                </div>  
                </asp:Panel>  


</div>

</ContentTemplate>           
</asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ScriptPlaceHolder" Runat="Server">
</asp:Content>


﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Assessment_self_COMP_Assessment.aspx.vb" MasterPageFile="~/MasterPage.Master" Inherits="VB.Assessment_self_COMP_Assessment" %>

<%@ Register src="WUC_DateReporter.ascx" tagname="WUC_DateReporter" tagprefix="uc1" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style>
.Buttonfooter {
   position: sticky;
   left: 0;
   bottom: 0;
   width: 100%;
   background-color:  Gray  ;
   color: white;
   text-align: center;
}
</style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">      

<ContentTemplate>
<asp:ToolkitScriptManager ID="toolkit1" runat="server"></asp:ToolkitScriptManager>

<div class="container-fluid">
				<!-- BEGIN PAGE HEADER-->
				<div class="row-fluid" id="fixedPageHeader">
					<div class="span12"> 
						<!-- BEGIN PAGE TITLE & BREADCRUMB-->
						<h3 class="page-user-md">การประเมินผลสมรรถนะตนเอง (Competency)  <font color="blue">(ScreenID : S-EMP-04)</font>
                        <asp:Label ID="lblReportTime" Font-Size="14px" ForeColor="Green" runat="Server"></asp:Label>
                        </h3>					
									
						<ul class="breadcrumb">
                            <li>
                            	<i class="icon-user"></i> <a href="javascript:;">การประเมินตนเอง</a><i class="icon-angle-right"></i>
                            </li>
                        	<li>
                        	    <i class="icon-check"></i> <a href="javascript:;">การประเมินผลสมรรถนะ (Competency)</a>
                        	</li>                        	
                        	
                            <uc1:WUC_DateReporter ID="WUC_DateReporter1" runat="server" />
                        </ul>
						<!-- END PAGE TITLE & BREADCRUMB-->
									        
				     </div>
			    </div>
		        <!-- END PAGE HEADER-->
				
                                
				<div class="portlet-body form">										
			        
			        <div class="btn-group pull-right">                                    
			            <a data-toggle="dropdown" class="btn dropdown-toggle">พิมพ์ <i class="icon-angle-down"></i></a>										
			                <ul class="dropdown-menu pull-right">                                                       
                                <li><a id="btnCOMPFormPDF" runat="server" target="_blank">ใบประเมิน รูปแบบ PDF</a></li>
                                <li><a id="btnCOMPFormExcel" runat="server" target="_blank">ใบประเมิน รูปแบบ Excel</a></li>	
                                <li><a id="btnIDPFormPDF" runat="server" target="_blank">แผนพัฒนารายบุคคล รูปแบบ PDF</a></li>
                                <li><a id="btnIDPFormExcel" runat="server" target="_blank">แผนพัฒนารายบุคคล รูปแบบ Excel</a></li>	
                                <li><a id="btnIDPResultFormPDF" runat="server" target="_blank">ผลการพัฒนาตามแผนพัฒนารายบุคคล รูปแบบ PDF</a></li>
                                <li><a id="btnIDPResultFormExcel" runat="server" target="_blank">ผลการพัฒนาตามแผนพัฒนารายบุคคล รูปแบบ Excel</a></li>
                                <li><a id="btnIDPRemark_Plan" href="Doc/RemarkIDP/Remark_PlanIDP.pdf" runat="server" target="_blank">Download คำชี้แจง แผนการพัฒนารายบุคคล</a></li>
                                <li><a id="btnIDPRemark_Result" href="Doc/RemarkIDP/Remark_ResultIDP.pdf" runat="server" target="_blank">Download คำชี้แจง ผลการพัฒนาตามแผนพัฒนารายบุคคล</a></li>					                                </ul>
							
			                </ul>
		            </div>
		            
			        <h3 class="form-section"><i class="icon-th-list"></i>
			            <asp:Label ID="lblName" runat="server"></asp:Label>
			            สำหรับรอบการประเมิน
			            <asp:DropDownList ID="ddlRound" OnSelectedIndexChanged="ddlRound_SelectedIndexChanged" runat="server" AutoPostBack="true" CssClass="medium m-wrap header" style="font-size:20px;">
					    </asp:DropDownList>
					    (<asp:Label ID="lblCOMPStatus" runat="server" KPI_Status="0"></asp:Label>)
			        </h3>
			        
    			    <div class="form-horizontal form-view">
    			        
    			       <div class="row-fluid" style="border-bottom:1px solid #EEEEEE;">
					        <div class="span6 ">
					            <div class="control-group">
								    <label class="control-label" style="width:100px;">ประเภท :</label>
								    <div class="controls" style="margin-left: 120px;">
									    <asp:Label ID="lblPSNType" runat="server" CssClass="text bold"></asp:Label>
								    </div>
							    </div>
					        </div>					        
					     </div>
    			        <div class="row-fluid" style="border-bottom:1px solid #EEEEEE;">
					        <div class="span4 ">
					            <div class="control-group">
								    <label class="control-label" style="width:100px;">ผู้รับการประเมิน :</label>
								    <div class="controls" style="margin-left: 120px;">
									    <asp:Label ID="lblPSNName" runat="server" CssClass="text bold"></asp:Label>
								    </div>
							    </div>
					        </div>
					        <div class="span3 ">
					            <div class="control-group">
								    <label class="control-label" style="width:100px;">ตำแหน่ง :</label>
								    <div class="controls" style="margin-left: 120px;">
									    <asp:Label ID="lblPSNPos" runat="server" CssClass="text bold"></asp:Label>
								    </div>
							    </div>
					        </div>
					        <div class="span5 ">
					            <div class="control-group">
								    <label class="control-label" style="width:100px;">สังกัด :</label>
								    <div class="controls" style="margin-left: 120px;">
									    <asp:Label ID="lblPSNDept" runat="server" CssClass="text bold"></asp:Label>
								    </div>
							    </div>
					        </div>
					     </div>
					     
					     <div class="row-fluid" style="margin-top:10px;">
					        <div class="span4 ">
					            <div class="control-group">
								    <label class="control-label" style="width:100px;">ผู้ประเมิน :</label>
								    <div class="controls" style="margin-left: 120px;">
									    <asp:DropDownList ID="ddlASSName" OnSelectedIndexChanged="ddlASSName_SelectedIndexChanged" runat="Server" AutoPostBack="True" style="font-weight:bold; color:Black; border:none;">									        
									    </asp:DropDownList>
									</div>
							    </div>
					        </div>
					        <div class="span3 ">
					            <div class="control-group">
								    <label class="control-label" style="width:100px;">ตำแหน่ง :</label>
								    <div class="controls" style="margin-left: 120px;">
									    <asp:Label ID="lblASSPos" runat="server" CssClass="text bold"></asp:Label>
								    </div>
							    </div>
					        </div>
					        <div class="span5 ">
					            <div class="control-group">
								    <label class="control-label" style="width:100px;">สังกัด :</label>
								    <div class="controls" style="margin-left: 120px;">
									    <asp:Label ID="lblASSDept" runat="server" CssClass="text bold"></asp:Label>
								    </div>
							    </div>
					        </div>
					     </div>
					     
    			    </div>
    														
				    <!-- BEGIN FORM-->
				    <div class="portlet-body no-more-tables">
				
				                        <table class="table table-bordered no-more-tables" style="background-color:White;">
									        <thead>
										        <tr>
											        <th rowspan="2" style="text-align:center; background-color:Silver;"> ลำดับ</th>
											        <th rowspan="2" style="text-align:center; background-color:Silver;"> สมรรถนะ</th>
											        <th rowspan="2" style="text-align:center; background-color:Silver;"> มาตรฐานพฤติกรรม</th>
											        <th colspan="5" style="text-align:center; background-color:Silver;"> คะแนนตามระดับค่าเป้าหมาย</th>
											        <th rowspan="2" style="text-align:center; background-color:Silver;"> คะแนน</th>
											        <th rowspan="2" style="text-align:center; background-color:Silver;"> น้ำหนัก(%)</th>											        
										        </tr>
                                                <tr>
										          <th class="AssLevel1" style="text-align:center; font-weight:bold; vertical-align:middle;">1</th>
										          <th class="AssLevel2" style="text-align:center; font-weight:bold; vertical-align:middle;">2</th>
										          <th class="AssLevel3" style="text-align:center; font-weight:bold; vertical-align:middle;">3</th>
										          <th class="AssLevel4" style="text-align:center; font-weight:bold; vertical-align:middle;">4</th>
										          <th class="AssLevel5" style="text-align:center; font-weight:bold; vertical-align:middle;">5</th>
								              </tr>
									        </thead>
									        <tbody>
									            <asp:Panel ID="pnlCOMP" runat="server">
									            
									                <asp:TextBox ID="txtCOMPTypeID" runat="server" style="display:none;" Text="-1"></asp:TextBox>
				                                    <asp:TextBox ID="txtMasterNo" runat="server" style="display:none;" Text="0"></asp:TextBox>
				                                    <asp:Textbox ID="txtBehaviorNo" runat="server" style="display:none;" Text="0"></asp:Textbox>
				                                    <asp:Button ID="btnBehaviorDialog" OnClick="btnBehaviorDialog_Click" runat="server" style="display:none;"/>
				                                    <asp:Button ID="btnBehaviorDelete" OnClick="btnBehaviorDelete_Click" runat="server" style="display:none;"/>
				                                    
									                <asp:Repeater ID="rptAss"  OnItemCommand="rptAss_ItemCommand" OnItemDataBound="rptAss_ItemDataBound" runat="server">
									                    <ItemTemplate>
									                    <tr id="row_COMP_type" runat="server">
									                        <td colspan="10" style="font-weight:bold; font-size:18px; background-color:#AAAAAA;" id="cell_COMP_type" runat="server">สมรรถนะหลัก Core Competency, สมรรถนะตามสายงาน Functional Competency, สมรรถนะตามสายระดับ Managerial Competency</td>
									                    </tr>
    									                
									                    <tr>
										                  <td data-title="ลำดับ" rowspan="5" id="ass_no" runat="server" style="text-align:center; font-weight:bold; vertical-align:top;">001</td>
										                  <td data-title="สมรรถนะ" id="COMP_Name" class="TextLevel0" runat="server" style="background-color:#f8f8f8; ">&nbsp;</td>
										                  <td data-title="มาตรฐานพฤติกรรม" id="target" runat="server" class="TextLevel0" style="background-color:#f8f8f8; ">&nbsp;</td>
										                  <td data-title="คลิกเพื่อประเมินระดับที่ 1" title="Click เพื่อเลือก" id="choice1" class="TextLevel0" runat="server" style="cursor:pointer; background-color:White;">&nbsp;</td>
                                                          <td data-title="คลิกเพื่อประเมินระดับที่ 2" title="Click เพื่อเลือก" id="choice2" class="TextLevel0" runat="server" style="cursor:pointer; background-color:White;">&nbsp;</td>
                                                          <td data-title="คลิกเพื่อประเมินระดับที่ 3" title="Click เพื่อเลือก" id="choice3" class="TextLevel0" runat="server" style="cursor:pointer; background-color:White;">&nbsp;</td>
                                                          <td data-title="คลิกเพื่อประเมินระดับที่ 4" title="Click เพื่อเลือก" id="choice4" class="TextLevel0" runat="server" style="cursor:pointer; background-color:White;">&nbsp;</td>
                                                          <td data-title="คลิกเพื่อประเมินระดับที่ 5" title="Click เพื่อเลือก" id="choice5" class="TextLevel0" runat="server" style="cursor:pointer; background-color:White;">&nbsp;</td>
										                  <td data-title="คะแนน" rowspan="5" title="คะแนน" id="mark" runat="server" style="text-align:center; font-size:16px; font-weight:bold; vertical-align:middle;">-</td>
										                  <td data-title="น้ำหนัก" rowspan="5" id="cell_weight" runat="server" title="น้ำหนัก" style="text-align:center; font-size:16px; font-weight:bold; vertical-align:middle;">
										                    <asp:Label id="weight" runat="server"></asp:Label> %
										                  </td>
        										          
									                    </tr>
									                    <tr id="trBehavior" runat="server">
									                       <td colspan="2" data-title="พฤติกรรมบ่งชี้" id="cell_Add_Behavior" style=" font:inherit; background-color:#f8f8f8; text-align:center; font-size:14px;">
									                       พฤติกรรมบ่งชี้  <i class="icon-long-arrow-right"></i><br>
									                       <asp:Button ID="btnAddBehavior" runat="server" Text="(Click เพื่อเพิ่มรายการ)" style=" background-color:white !important; border:none; text-decoration:none; cursor:pointer; color:Gray; background-color:#f8f8f8;" CommandName="AddBehavior"></asp:Button>
									                       <br><br><asp:Image ID="imgAlert" runat="server" ImageUrl="~/images/alert.gif" Visible="false" />
									                       <br><asp:Label ID="lblAlert" runat="server" ForeColor="Red"></asp:Label>
									                       </td>
									                       <td colspan="5"> 
									                       
									                       <asp:Repeater ID="rptBehavior" runat="server">
									                           <ItemTemplate>
									                                    <div style="width:100%; vertical-align:top; margin-bottom:10px;">
									                                        <a tabindex="-1" class="select2-search-choice-close" onclick="return false;" href="#"></a>
									                                        <a ID="aBehavior" runat="server" style="text-decoration:none; width:80%; float:left;" href="javascript:;"></a>
									                                        <a ID="aDelete"  runat="server" class="btn mini" style="float:right;" href="javascript:;">ลบ</a>
									                                    </div><br>
									                           </ItemTemplate>
									                       </asp:Repeater>
									                       
									                       </td>
									                       
    									                </tr>
    									                <tr>
    									                   <td colspan="2" data-title="หมายเหตุ (ผู้ถูกประเมิน)" style=" font:inherit; background-color:#f8f8f8; text-align:center; font-size:14px;">หมายเหตุ (ผู้ถูกประเมิน)</td>
									                       <td colspan="5" data-title="หมายเหตุ (ผู้ถูกประเมิน)" style="padding:0px;">
									                           <asp:Textbox   ID="txtPSN" runat="server" style="width:96%; padding:5px; background-color:Transparent; border:0px none transparent;  margin:0px;" TextMode="Multiline" Text="" PlaceHolder="(ถ้ามี)"  Columns="50" onkeypress="return this.value.length<4000" ></asp:Textbox>
									                           <asp:Button ID="btnRemark" runat="server" style="display:none;" CommandName="Remark" />
									                       </td>
    									                </tr>
									                    <tr>
									                        <td colspan="2" data-title="หมายเหตุ (หัวหน้างาน/ผู้ประเมิน)" style=" font:inherit; background-color:#f8f8f8; text-align:center; font-size:14px;">หมายเหตุ (ผู้ประเมิน)</td>
									                        <td colspan="5"  data-title="หมายเหตุ (ผู้ประเมิน/หัวหน้างาน)" style="padding:0px;">
									                        <asp:Textbox ID="txtMGR" ReadOnly="true"    runat="server" style="width:96%; padding:5px; background-color:Transparent; height:53px; border:0px none transparent;  margin:0px;" TextMode="Multiline" PlaceHolder="(ถ้ามี)" Text=""  Columns="50" onkeypress="return this.value.length<4000" ></asp:Textbox>
									                        </td>
									                    </tr>
									                    <tr>
									                    
									                    </tr>
									                    <tr>
									                          <td id="cel_officer" runat="server" class="TextLevel0" colspan="2" style="text-align:center; font-weight:bold; padding:0px !important; height:10px !important;" >การประเมินตนเอง</td>
									                          <td data-title="คลิกเพื่อประเมินระดับที่ 1" title="คะแนนประเมินตนเอง" id="selOfficialColor1" runat="server" style="cursor:pointer; padding:0px !important; height:10px !important; border-right:0px none;"><asp:Button ID="btn1" runat="server" style="display:none;" CommandName="Select" /></td>
                                                              <td data-title="คลิกเพื่อประเมินระดับที่ 2" title="คะแนนประเมินตนเอง" id="selOfficialColor2" runat="server" style="cursor:pointer; padding:0px !important; height:10px !important; border-right:0px none; border-left:0px none;"><asp:Button ID="btn2" runat="server" style="display:none;" CommandName="Select" /></td>
                                                              <td data-title="คลิกเพื่อประเมินระดับที่ 3" title="คะแนนประเมินตนเอง" id="selOfficialColor3" runat="server" style="cursor:pointer; padding:0px !important; height:10px !important; border-right:0px none; border-left:0px none;"><asp:Button ID="btn3" runat="server" style="display:none;" CommandName="Select" /></td>
                                                              <td data-title="คลิกเพื่อประเมินระดับที่ 4" title="คะแนนประเมินตนเอง" id="selOfficialColor4" runat="server" style="cursor:pointer; padding:0px !important; height:10px !important; border-right:0px none; border-left:0px none;"><asp:Button ID="btn4" runat="server" style="display:none;" CommandName="Select" /></td>
                                                              <td data-title="คลิกเพื่อประเมินระดับที่ 5" title="คะแนนประเมินตนเอง" id="selOfficialColor5" runat="server" style="cursor:pointer; padding:0px !important; height:10px !important; border-left:0px none;"><asp:Button ID="btn5" runat="server" style="display:none;" CommandName="Select" /></td>
                                                        </tr>
                                                        <tr id="rowHeader" runat="server" >
									                          <td id="cel_header" runat="server" class="TextLevel0" colspan="2" style="text-align:center; font-weight:bold; padding:0px !important; height:10px !important;" >ผู้บังคับบัญชาประเมิน</td>
									                          <td data-title="ผู้ประเมิน" title="คะแนนที่ได้จากผู้ประเมิน" id="selHeaderColor1" runat="server" style="padding:0px !important; height:10px !important; border-right:0px none;"><asp:Button ID="Button1" runat="server" style="display:none;" CommandName="Select" /></td>
                                                              <td data-title="ผู้ประเมิน" title="คะแนนที่ได้จากผู้ประเมิน" id="selHeaderColor2" runat="server" style="padding:0px !important; height:10px !important; border-right:0px none; border-left:0px none;"><asp:Button ID="Button2" runat="server" style="display:none;" CommandName="Select" /></td>
                                                              <td data-title="ผู้ประเมิน" title="คะแนนที่ได้จากผู้ประเมิน" id="selHeaderColor3" runat="server" style="padding:0px !important; height:10px !important; border-right:0px none; border-left:0px none;"><asp:Button ID="Button3" runat="server" style="display:none;" CommandName="Select" /></td>
                                                              <td data-title="ผู้ประเมิน" title="คะแนนที่ได้จากผู้ประเมิน" id="selHeaderColor4" runat="server" style="padding:0px !important; height:10px !important; border-right:0px none; border-left:0px none;"><asp:Button ID="Button4" runat="server" style="display:none;" CommandName="Select" /></td>
                                                              <td data-title="ผู้ประเมิน" title="คะแนนที่ได้จากผู้ประเมิน" id="selHeaderColor5" runat="server" style="padding:0px !important; height:10px !important; border-left:0px none;"><asp:Button ID="Button5" runat="server" style="display:none;" CommandName="Select" /></td>
                                                        </tr>
									                    </ItemTemplate>
									                    <FooterTemplate>    									                
									                        <tfoot>
									                            <tr>
									                                <td colspan="8" style="text-align:center; background-color:#EEEEEE; font-size:14px; font-weight:bold;">
									                                ผลรวมการประมิน
									                                </td>
									                                <td colspan="2" style="text-align:center; background-color:#EEEEEE; font-size:14px; font-weight:bold;">
									                                คะแนนที่ได้
									                                </td>
									                            </tr>
									                            <tr>
									                                <th colspan="3" style="text-align:center; background-color:#EEEEEE; border-bottom:1px solid #DDDDDD; font-size:14px; font-weight:bold;" id="cell_total" runat="server">
									                               รวม
									                                </th>
									                                  <th id="cell_sum_1" runat="server" style="text-align:center; border-bottom:1px solid #DDDDDD;; font-size:14px; font-weight:bold; vertical-align:middle;">-</th>
										                              <th id="cell_sum_2" runat="server" style="text-align:center; border-bottom:1px solid #DDDDDD;; font-size:14px; font-weight:bold; vertical-align:middle;">-</th>
										                              <th id="cell_sum_3" runat="server" style="text-align:center; border-bottom:1px solid #DDDDDD;; font-size:14px; font-weight:bold; vertical-align:middle;">-</th>
										                              <th id="cell_sum_4" runat="server" style="text-align:center; border-bottom:1px solid #DDDDDD;; font-size:14px; font-weight:bold; vertical-align:middle;">-</th>
										                              <th id="cell_sum_5" runat="server" style="text-align:center; border-bottom:1px solid #DDDDDD;; font-size:14px; font-weight:bold; vertical-align:middle;">-</th>
								                                      <th id="cell_sum_raw" runat="server" style="text-align:center; border-bottom:1px solid #DDDDDD; font-size:16px; font-weight:bold;" colspan="2">-</th>
									                            </tr>
									                            <tr>
									                                <th colspan="8" style="background-color:#FFFFFF; border:none;"></th>
									                                <th style="text-align:center; background-color:#EEEEEE; font-size:14px; font-weight:bold;" colspan="2" >คิดเป็น</th>									                          
									                            </tr>
									                            <tr>
									                                <th colspan="8" style="background-color:#FFFFFF; border:none;"></th>
									                                <th id="cell_sum_mark" runat="server" style="text-align:center; font-size:24px; font-weight:bold;" colspan="2">-</th>									                          
									                            </tr>
									                        </tfoot>    									                
									                    </FooterTemplate>
									                </asp:Repeater>	
									            
									            </asp:Panel>
									        </tbody>									        
								        </table>
								        <div id="divFooter" runat ="server"  class="Buttonfooter">
								        <asp:Panel CssClass="form-actions" id="pnlActivity" runat="server">
									        <asp:Button ID="btnSaveRemark" runat="server"  OnClick="btnSaveRemark_Click"  CssClass="btn blue" Text="บันทึกข้อมูล"   />
						                    <asp:Button ID="btnPreSend" OnClick="btnPreSend_Click" runat="server" CssClass="btn green" Text="ยืนยันส่งผลการประเมินให้หัวหน้า/ผู้ประเมิน  (หลังจากส่งแบบประเมินแล้วคุณไม่สามารถแก้ไขได้)" />						                    

						                    <asp:Button ID="btnSend"  OnClick="btnSend_Click" runat="server" Text="" style="display:none;" />


						                </asp:Panel>
                                        </div>
							</div>
						
				    <!-- END FORM-->  
				   
			    </div>	        
				                <asp:Panel CssClass="modal" style="top:20%; width:400px; position:fixed;" id="ModalBehavior" runat="server" Visible="False" >
									<div class="modal-header">										
										<h3>ลำดับที่ <asp:Label ID="lblBehaviorNo" runat="server" Text="0"></asp:Label></h3>
									</div>
									<div class="modal-body">
										<div style="height:120px">
											<div class="row-fluid">
											        <asp:TextBox TextMode="MultiLine" ID="txtBehavior" runat="server"
											         CssClass="m-wrap" Width="95%" Height="110px"></asp:TextBox>														       
											</div>
										</div>
									</div>
									<div class="modal-footer">
									    <asp:Button ID="btnOK"  OnClick="btnOK_Click" runat="server" CssClass="btn blue" Text="บันทึก" />
									    <asp:Button ID="btnClose" OnClick="btnClose_Click" runat="server" CssClass="btn" Text="ยกเลิก" />																		
									</div>
								</asp:Panel>
								            
                  		 
</div>

</ContentTemplate>           
</asp:UpdatePanel>						 
</asp:Content>
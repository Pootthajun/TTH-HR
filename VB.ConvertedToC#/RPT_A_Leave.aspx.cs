using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Xml.Linq;
using System.Diagnostics;
using System.Collections.Specialized;
using System.Configuration;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Caching;
using System.Web.SessionState;
using System.Web.Security;
using System.Web.Profile;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using System.Data.SqlClient;
namespace VB
{

	public partial class RPT_A_Leave : System.Web.UI.Page
	{


		HRBL BL = new HRBL();
		GenericLib GL = new GenericLib();

		Converter C = new Converter();
		protected void Page_Load(object sender, System.EventArgs e)
		{
			if ((Session["USER_PSNL_NO"] == null)) {
				ScriptManager.RegisterStartupScript(this.Page, typeof(string), "Alert", "showAlert('กรุณาลงชื่อเข้าใช้ระบบ');", true);
				Response.Redirect("Login.aspx");
				return;
			}


			if (!IsPostBack) {
				BindddlMonth();

				BL.BindDDlSector(ddlSector);
				BL.BindDDlPSNType(ddlPSNType);
				BindPersonalList();
			}
		}

        protected void btnSearch_Click(object sender, System.EventArgs e)
		{
			BindPersonalList();
		}


		private void BindddlMonth()
		{
			GenericLib GL = new GenericLib();
			//----------------- Year ----------------
			ddlStart_Year.Items.Clear();
			ddlEnd_Year.Items.Clear();

			for (int Y = 2556; Y <= (DateTime.Now.Year + 543); Y++) {
				ddlStart_Year.Items.Add(new ListItem(Y.ToString(), Y.ToString().Substring(2)));
				ddlEnd_Year.Items.Add(new ListItem(Y.ToString(), Y.ToString().Substring(2)));
			}

			//----------------- Month ----------------
			ddlStart_Month.Items.Clear();
			ddlEnd_Month.Items.Clear();
			for (int M = 1; M <= 12; M++) {
				ddlStart_Month.Items.Add(new ListItem(GL.ReportMonthThai(M), M.ToString().PadLeft(2, GL.chr0)));
				ddlEnd_Month.Items.Add(new ListItem(GL.ReportMonthThai(M), M.ToString().PadLeft(2, GL.chr0)));
			}

			ddlStart_Year.SelectedIndex = 0;
			ddlEnd_Year.SelectedIndex = ddlEnd_Year.Items.Count - 1;
			ddlStart_Month.SelectedIndex = 0;
			ddlEnd_Month.SelectedIndex = DateTime.Now.Month - 1;

		}


		private void BindPersonalList()
		{
			string SQL = "SELECT PSN.PSNL_NO,PSNL_Fullname,PNPS_CLASS,POS_NO,WAGE_TYPE,WAGE_NAME" + "\n";
			SQL += " ,DEPT_CODE,SECTOR_CODE,SECTOR_NAME,DEPT_NAME,ISNULL(MGR_NAME,FN_NAME) POS_Name" + "\n";
			SQL += " ,ISNULL(SUM(LEAVE.L_ACTIVITY),0) L_ACTIVITY,ISNULL(SUM(LEAVE.L_HEALTH),0) L_HEALTH,ISNULL(SUM(LEAVE.L_Day),0) L_Day" + "\n";
			SQL += " FROM vw_PN_PSNL PSN \n LEFT JOIN vw_PN_LEAVE LEAVE ON PSN.PSNL_NO=LEAVE.PSNL_NO" + "\n";
			SQL += " AND L_YYMM BETWEEN '" + ddlStart_Year.Items[ddlStart_Year.SelectedIndex].Value + ddlStart_Month.Items[ddlStart_Month.SelectedIndex].Value + "' AND ";
			SQL += " '" + ddlEnd_Year.Items[ddlEnd_Year.SelectedIndex].Value + ddlEnd_Month.Items[ddlEnd_Month.SelectedIndex].Value + "'" + "\n";

			string Filter = "";
			string Title = "รายงานสรุปวันลา <br>";

			if (ddlPSNType.SelectedIndex > 0) {
				Filter += " WAGE_TYPE = " + ddlPSNType.Items[ddlPSNType.SelectedIndex].Value + " AND " + "\n";
				Title += " พนักงาน" + ddlPSNType.Items[ddlPSNType.SelectedIndex].Text;
			}
			if (ddlSector.SelectedIndex > 0) {
				Filter += " SECTOR_CODE = '" + ddlSector.SelectedValue + "' AND " + "\n";

				SQL += " AND ( SECTOR_CODE='" + ddlSector.Items[ddlSector.SelectedIndex].Value + "' " + "\n";
				if (ddlSector.Items[ddlSector.SelectedIndex].Value == "3200") {
					SQL += " AND  DEPT_CODE NOT IN ('32000300'))    " + "\n";
				//-----------ฝ่ายโรงงานผลิตยาสูบ 3
				} else if (ddlSector.Items[ddlSector.SelectedIndex].Value == "3203") {
					SQL += " OR  DEPT_CODE IN ('32000300','32030000','32030100','32030200','32030300','32030500'))  " + "\n";
				//-----------ฝ่ายโรงงานผลิตยาสูบ 4
				} else if (ddlSector.Items[ddlSector.SelectedIndex].Value == "3204") {
					SQL += " OR  DEPT_CODE IN ('32040000','32040100','32040200','32040300','32040500','32040300'))  " + "\n";
				//-----------ฝ่ายโรงงานผลิตยาสูบ 5
				} else if (ddlSector.Items[ddlSector.SelectedIndex].Value == "3205") {
					SQL += " OR  DEPT_CODE IN ('32050000','32050100','32050200','32050300','32050500'))\t  " + "\n";
				} else {
					SQL += ")   " + "\n";
				}

				Title += " " + ddlSector.Items[ddlSector.SelectedIndex].Text.Substring(3);
			}
			if (!string.IsNullOrEmpty(txtDeptName.Text)) {
				Filter += " (DEPT_NAME LIKE '%" + txtDeptName.Text.Replace("'", "''") + "%' OR SECTOR_NAME LIKE '%" + txtDeptName.Text.Replace("'", "''") + "%') AND " + "\n";
				//Title &= " หน่วยงาน'" & txtDeptName.Text & "'"
			}
			if (!string.IsNullOrEmpty(txtName.Text)) {
				Filter += " (PSNL_Fullname LIKE '%" + txtName.Text.Replace("'", "''") + "%' OR PSN.PSNL_NO LIKE '%" + txtName.Text.Replace("'", "''") + "%') AND " + "\n";
				//Title &= " ของพนักงานชื่อ/รหัส:'" & txtName.Text & "'"
			}

			Filter += " PSNL_TYPE<>'3' AND " + "\n";

			Title += "ช่วง" + ddlStart_Month.Items[ddlStart_Month.SelectedIndex].Text + " ปี 25" + ddlStart_Year.Items[ddlStart_Year.SelectedIndex].Value;
			Title += " ถึง ";
			Title += ddlEnd_Month.Items[ddlEnd_Month.SelectedIndex].Text + " ปี 25" + ddlEnd_Year.Items[ddlEnd_Year.SelectedIndex].Value;

			SQL += " WHERE " + Filter.Substring(0, Filter.Length - 5) + "\n";

			SQL += " GROUP BY PSN.PSNL_NO,PSNL_Fullname,PNPS_CLASS,POS_NO,WAGE_TYPE,WAGE_NAME\n";
			SQL += " ,DEPT_CODE,SECTOR_CODE,SECTOR_NAME,DEPT_NAME,MGR_NAME,FN_NAME\n";


			if ((chkYes.Checked & chkNo.Checked) | (!chkYes.Checked & !chkNo.Checked)) {
			} else if (chkYes.Checked) {
				SQL += " HAVING ISNULL(SUM(LEAVE.L_Day),0)>0 AND (ISNULL(SUM(LEAVE.L_ACTIVITY),0)>0 OR ISNULL(SUM(LEAVE.L_HEALTH),0)>0) " + "\n";
				Title += " เฉพาะที่มีการลา";
			} else if (chkNo.Checked) {
				SQL += " HAVING ISNULL(SUM(LEAVE.L_Day),0)=0 AND ISNULL(SUM(LEAVE.L_ACTIVITY),0)=0 AND ISNULL(SUM(LEAVE.L_HEALTH),0)=0 " + "\n";
				Title += " เฉพาะที่ไม่มีการลา";
			}

			SQL += " ORDER BY SECTOR_CODE,DEPT_CODE,PNPS_CLASS DESC,POS_NO" + "\n";

			SqlDataAdapter DA = new SqlDataAdapter(SQL, BL.ConnectionString());
			DataTable DT = new DataTable();
			DA.Fill(DT);

			Session["RPT_A_Leave"] = DT;
			Pager.SesssionSourceName = "RPT_A_Leave";
			Pager.RenderLayout();

			lblTitle.Text = Title;

			//--------- Title --------------
			Session["RPT_A_LEAVE_Title"] = Title.Replace("<br>", "\n");
			//--------- Sub Title ----------
			Session["RPT_A_LEAVE_SubTitle"] = "รายงาน ณ วันที่ " + DateTime.Now.Day + " " + C.ToMonthNameTH(DateTime.Now.Month) + " พ.ศ." + (DateTime.Now.Year + 543);
			Session["RPT_A_LEAVE_SubTitle"] += "  พบ   " + GL.StringFormatNumber(DT.Rows.Count, 0) + " รายการ ";

			if (DT.Rows.Count == 0) {
				lblTitle.Text += " ไม่พบรายการดังกล่าว";
			} else {
				lblTitle.Text += " พบ " + GL.StringFormatNumber(DT.Rows.Count, 0) + " รายการ";
			}

		}

		protected void Pager_PageChanging(PageNavigation Sender)
		{
			Pager.TheRepeater = rptAss;
		}

		//------------ For Grouping -----------
		string LastSectorName = "";
		string LastOrgranizeName = "";
		protected void rptAss_ItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
		{
			if (e.Item.ItemType != ListItemType.AlternatingItem & e.Item.ItemType != ListItemType.Item)
				return;

            Label lblSector = (Label)e.Item.FindControl("lblSector");
            Label lblPSNOrganize = (Label)e.Item.FindControl("lblPSNOrganize");
            Label lblPSNName = (Label)e.Item.FindControl("lblPSNName");
            Label lblPSNPos = (Label)e.Item.FindControl("lblPSNPos");
            Label lblPSNClass = (Label)e.Item.FindControl("lblPSNClass");
            Label lblPSNType = (Label)e.Item.FindControl("lblPSNType");

            Label lblLeave = (Label)e.Item.FindControl("lblLeave");
            Label lblHealth = (Label)e.Item.FindControl("lblHealth");
            Label lblActivity = (Label)e.Item.FindControl("lblActivity");

            HtmlTableCell td1 = (HtmlTableCell)e.Item.FindControl("td1");
            HtmlTableCell td2 = (HtmlTableCell)e.Item.FindControl("td2");

            DataRowView drv = (DataRowView)e.Item.DataItem;

			if (drv["SECTOR_NAME"].ToString() != LastSectorName) {
				lblSector.Text = drv["SECTOR_NAME"].ToString();
				LastSectorName = drv["SECTOR_NAME"].ToString();
			} else {
				td1.Style["border-top"] = "none";
			}
			if (drv["DEPT_NAME"].ToString() != LastOrgranizeName) {
				if (drv["DEPT_NAME"].ToString() != drv["SECTOR_NAME"].ToString()) {
					lblPSNOrganize.Text = drv["DEPT_NAME"].ToString().Replace(drv["SECTOR_NAME"].ToString(), "").Trim();
				} else {
					lblPSNOrganize.Text = drv["DEPT_NAME"].ToString();
				}
				LastOrgranizeName = drv["DEPT_NAME"].ToString();
			} else {
				td2.Style["border-top"] = "none";
			}
			lblPSNName.Text = drv["PSNL_Fullname"].ToString();
			lblPSNPos.Text = drv["POS_Name"].ToString();
			if (!GL.IsEqualNull(drv["PNPS_CLASS"])) {
				lblPSNClass.Text = GL.CINT (drv["PNPS_CLASS"]).ToString();
			} else {
				lblPSNClass.Text = "";
			}
			lblPSNType.Text =drv["WAGE_NAME"].ToString();

			string L_Day = GL.StringFormatNumber(drv["L_Day"], 1);
            double _tmp = Conversion.Val(L_Day);
            L_Day = _tmp.ToString();
			if (L_Day == "0")
				L_Day = "-";
			lblLeave.Text = L_Day;

			string L_Health = GL.StringFormatNumber(drv["L_Health"], 1);
            _tmp = Conversion.Val(L_Health);
            L_Health = _tmp.ToString();
			if (L_Health == "0")
				L_Health = "-";
			lblHealth.Text = L_Health;

			string L_Activity = GL.StringFormatNumber(drv["L_Activity"], 1);
            _tmp = Conversion.Val(L_Activity);
            L_Activity = _tmp.ToString();
			if (L_Activity == "0")
				L_Activity = "-";
			lblActivity.Text = L_Activity;

		}
		public RPT_A_Leave()
		{
			Load += Page_Load;
		}
	}
}

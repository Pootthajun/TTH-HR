﻿<%@ Page  Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPage.Master" CodeBehind="RPT_A_Leave.aspx.cs" Inherits="VB.RPT_A_Leave" %>

<%@ Register src="WUC_DateReporter.ascx" tagname="WUC_DateReporter" tagprefix="uc1" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<%@ Register src="PageNavigation.ascx" tagname="PageNavigation" tagprefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<asp:ToolkitScriptManager ID="toolkit1" runat="server" ></asp:ToolkitScriptManager>

<asp:UpdatePanel ID="UpdatePanel1" runat="server">      
<ContentTemplate>
<div class="container-fluid">
				<!-- BEGIN PAGE HEADER-->
				<div class="row-fluid" id="fixedPageHeader">
					<div class="span12"> 
						<!-- BEGIN PAGE TITLE & BREADCRUMB-->
						<h3>จำนวนวันลาของพนักงาน<font color="blue">(ScreenID : R-ALL-04)</font></h3>						
						<ul class="breadcrumb">
                            <li>
                            	<i class="icon-copy"></i> <a href="javascript:;">รายงาน</a><i class="icon-angle-right"></i>
                            </li>
                            <li>
                            	<i class="icon-copy"></i> <a href="javascript:;">การประเมินผลตัวชี้วัดและสมรรถนะ</a><i class="icon-angle-right"></i>
                            </li>
                            <li>
                        	    <i class="icon-check"></i> <a href="javascript:;">จำนวนวันลาของพนักงาน</a>
                        	</li>                      	
                        	
                            <uc1:WUC_DateReporter ID="WUC_DateReporter1" runat="server" />
                        </ul>
						<!-- END PAGE TITLE & BREADCRUMB-->
									        
				     </div>

				
			    </div>
		        <!-- END PAGE HEADER-->
				
                <asp:Panel ID="pnlList" runat="server" Visible="True" DefaultButton="btnSearch">
				<div class="row-fluid">
					
					<div class="span12">
						<!-- BEGIN SAMPLE TABLE PORTLET-->
                                        <div class="row-fluid form-horizontal">
                			                  <div class="btn-group pull-left">
                                                    <asp:Button ID="btnSearch" OnClick="btnSearch_Click"  runat="server" CssClass="btn green" Text="ค้นหา" />
                                              </div> 
						                     <div class="btn-group pull-right">                                    
								                <button data-toggle="dropdown" class="btn dropdown-toggle" id="Button1">พิมพ์ <i class="icon-angle-down"></i></button>										
								                <ul class="dropdown-menu pull-right">
                                               
                                                    <li><a href="Print/RPT_A_Leave.aspx?Mode=PDF" target="_blank">รูปแบบ PDF</a></li>
                                                    <li><a href="Print/RPT_A_Leave.aspx?Mode=EXCEL" target="_blank">รูปแบบ Excel</a></li>												
								                </ul>
							                </div>
                                        </div>							            
						                 <div class="row-fluid form-horizontal">
						                             <div class="span3 ">
														<div class="control-group">
													        <label class="control-label" style="width:100px;"> ฝ่าย</label>
													        <div class="controls" style="margin-left:120px;">
														        <asp:DropDownList ID="ddlSector" runat="server" CssClass="medium m-wrap">
							                                    </asp:DropDownList>														        
													        </div>
												        </div>
													</div>
													
													<div class="span8 ">
														<div class="control-group">
													        <label class="control-label"> หน่วยงาน</label>
													        <div class="controls">
														        <asp:TextBox ID="txtDeptName" runat="server" CssClass="medium m-wrap" Placeholder="ค้นหาชื่อฝ่าย/หน่วยงาน"></asp:TextBox>		
													        </div>
												        </div>
													</div>
    										</div>
    										<div class="row-fluid form-horizontal">		
												     <div class="span3 ">
														<div class="control-group">
													        <label class="control-label" style="width:100px;"> ชื่อพนักงาน</label>
													        <div class="controls" style="margin-left:120px;">
														        <asp:TextBox ID="txtName" runat="server" CssClass="medium m-wrap" Placeholder="ค้นหาชื่อพนักงาน/เลขประจำตัว"></asp:TextBox>		
													        </div>
												        </div>
													</div>
													<div class="span8 ">
														<div class="control-group">
													        <label class="control-label"> ประเภทพนักงาน</label>
													        <div class="controls">
														         <asp:DropDownList ID="ddlPSNType" runat="server" CssClass="medium m-wrap">
												                </asp:DropDownList>
													        </div>
												        </div>
													</div>
													
											</div>
											<div class="row-fluid form-horizontal">
											        <div class="span3">
											                <div class="control-group">
												                <label class="control-label" style="width:100px;"> เงื่อนไข</label>
												                <div class="controls" style="padding-top:8px; margin-left:120px;">											                            
										                                    <asp:CheckBox ID="chkYes" runat="server" Text="" Checked="true" />มีการลา &nbsp; &nbsp;
										                                    <asp:CheckBox ID="chkNo" runat="server" Text="" Checked="true" />ไม่มีการลา
										                                    
										                        </div>
											                </div>
											        </div>
											        <div class="span8">
											                <div class="control-group">
												                <label class="control-label"> ช่วง</label>
												                <div class="controls" style="font-size:14px;">											                            
										                                <asp:DropDownList ID="ddlStart_Month" runat="server" CssClass="m-wrap" Width="100px"></asp:DropDownList>
										                                <asp:DropDownList ID="ddlStart_Year" runat="server" CssClass="m-wrap" Width="80px"></asp:DropDownList>										                                
										                                <span style="padding-top:8px;"> &nbsp; ถึง &nbsp; </span>	
										                                <asp:DropDownList ID="ddlEnd_Month" runat="server" CssClass="m-wrap" Width="100px"></asp:DropDownList>
										                                <asp:DropDownList ID="ddlEnd_Year" runat="server" CssClass="m-wrap" Width="80px"></asp:DropDownList>	
										                                		                                
										                        </div>
											                </div>
											        </div>
											</div>
											
							<div class="portlet-body no-more-tables">
								
								<asp:Label ID="lblTitle" runat="server" Font-Size="14px" Font-Bold="true" Width="100%" style="text-align:center"></asp:Label>
								<table class="table table-full-width table-bordered dataTable table-hover">
									<thead>
										<tr>
											<th style="text-align:center;" rowspan="2"> ฝ่าย</th>
											<th style="text-align:center;" rowspan="2"> หน่วยงาน</th>
											<th style="text-align:center;" rowspan="2"> ชื่อพนักงาน</th>
											<th style="text-align:center;" rowspan="2"> ตำแหน่ง</th>
											<th style="text-align:center;" rowspan="2"> ระดับ</th>
											<th style="text-align:center;" rowspan="2"> ประเภท</th>
											<th style="text-align:center;" colspan="3"> วันลา <br>(ที่มีผลกับคะแนนประเมิน)</th>
										</tr>
										<tr>
										    <th style="text-align:center;"> ลาทั้งหมด (วัน)</th>
										    <th style="text-align:center;"> ลาป่วย(ครั้ง)</th>
											<th style="text-align:center;"> ลากิจ(ครั้ง)</th>											
										</tr>
									</thead>
									<tbody>
										<asp:Repeater ID="rptAss" OnItemDataBound="rptAss_ItemDataBound" runat="server">
									        <ItemTemplate>
									            <tr>    
									                <td data-title="ฝ่าย" id="td1" runat="server"><asp:Label ID="lblSector" runat="server"></asp:Label></td>                                    
											        <td data-title="หน่วยงาน" id="td2" runat="server"><asp:Label ID="lblPSNOrganize" runat="server"></asp:Label></td>
											        <td data-title="ชื่อพนักงาน"><asp:Label ID="lblPSNName" runat="server"></asp:Label></td>
											        <td data-title="ตำแหน่ง"><asp:Label ID="lblPSNPos" runat="server"></asp:Label></td>
											        <td data-title="ระดับ"><asp:Label ID="lblPSNClass" runat="server"></asp:Label></td>
											        <td data-title="ประเภท"><asp:Label ID="lblPSNType" runat="server"></asp:Label></td>
											        <td data-title="ลาทั้งหมด (วัน)" style="text-align:center;"><asp:Label ID="lblLeave" runat="server" Font-Bold="True" ForeColor="Red"></asp:Label></td>
											        <td data-title="ลาป่วย(ครั้ง)" style="text-align:center;"><asp:Label ID="lblHealth" runat="server"></asp:Label></td>
											        <td data-title="ลากิจ(ครั้ง)" style="text-align:center;"><asp:Label ID="lblActivity" runat="server"></asp:Label></td>
										        </tr>	
									        </ItemTemplate>
										</asp:Repeater>
																							
									</tbody>
								</table>
								<asp:PageNavigation ID="Pager" OnPageChanging="Pager_PageChanging" MaximunPageCount="10" PageSize="20" runat="server" />
							</div>
					</div>
                </div>  
                </asp:Panel>  


</div>

</ContentTemplate>           
</asp:UpdatePanel>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ScriptPlaceHolder" Runat="Server">
</asp:Content>


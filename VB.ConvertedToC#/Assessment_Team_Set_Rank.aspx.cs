using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Xml.Linq;
using System.Diagnostics;
using System.Collections.Specialized;
using System.Configuration;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Caching;
using System.Web.SessionState;
using System.Web.Security;
using System.Web.Profile;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
namespace VB
{

	public partial class Assessment_Team_Set_Rank : System.Web.UI.Page
	{


		HRBL BL = new HRBL();
		GenericLib GL = new GenericLib();

		textControlLib TC = new textControlLib();
		public int R_Year {
			get {
				try {
					return GL.CINT( GL.SplitString(ddlRound.Items[ddlRound.SelectedIndex].Value, "-")[0]);
				} catch (Exception ex) {
					return 0;
				}
			}
		}

		public int R_Round {
			get {
				try {
					return GL.CINT(GL.SplitString(ddlRound.Items[ddlRound.SelectedIndex].Value, "-")[1]);
				} catch (Exception ex) {
					return 0;
				}
			}
		}

		public string DEPT_CODE {
			get {
				try {
					return GL.SplitString(ddlOrganize.Items[ddlOrganize.SelectedIndex].Value, "-")[0];
				} catch (Exception ex) {
					return "";
				}
			}
		}

		public string MINOR_CODE {
			get {
				try {
					return GL.SplitString(ddlOrganize.Items[ddlOrganize.SelectedIndex].Value, "-")[1];
				} catch (Exception ex) {
					return "";
				}
			}
		}


		public string PSNL_NO {
			get {
				try {
					return Session["USER_PSNL_NO"].ToString ();
				} catch (Exception ex) {
					return "";
				}
			}
		}

		protected void Page_Load(object sender, System.EventArgs e)
		{
			if ((Session["USER_PSNL_NO"] == null)) {
				ScriptManager.RegisterStartupScript(this.Page, typeof(string), "Alert", "showAlert('กรุณาลงชื่อเข้าใช้ระบบ');", true);
				Response.Redirect("Login.aspx");
				return;
			}

			if (!IsPostBack) {
				BL.BindDDlYearRound(ddlRound);
				BL.BindDDlTeamDepartment(ddlOrganize, R_Year, R_Round, PSNL_NO);
				try {
					ddlOrganize.Items.RemoveAt(0);
					ddlOrganize.SelectedIndex = 0;
				} catch (Exception ex) {
				}
				BindRank();
			}
		}

		protected void ddl_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			BindRank();
		}


		private void BindRank()
		{
			string SQL = "";
			SQL += " SELECT  DEPT_CODE,MINOR_CODE,Master_Rank.RANK_No,Master_Rank.RANK_Name,\n";
			SQL += " CASE Master_Rank.RANK_No WHEN 1 THEN 0 ELSE RANK_Start END RANK_Start,\n";
			SQL += " CASE Master_Rank.RANK_No WHEN 5 THEN 500 ELSE RANK_End END RANK_End\n";
			SQL += "  FROM \n";
			SQL += "  (\n";
			SQL += "  SELECT 5 AS RANK_No,'2.0 ขั้น' RANK_Name\n";
			SQL += "  UNION ALL SELECT 4 AS RANK_No,'1.5 ขั้น' RANK_Name\n";
			SQL += "  UNION ALL SELECT 3 AS RANK_No,'1.0 ขั้น' RANK_Name\n";
			SQL += "  UNION ALL SELECT 2 AS RANK_No,'0.5 ขั้น' RANK_Name\n";
			SQL += "  UNION ALL SELECT 1 AS RANK_No,'ไม่ผ่านเกณฑ์' RANK_Name\n";
			SQL += "  ) Master_Rank\n";
			SQL += "  LEFT JOIN tb_HR_Assessment_Rank DEPT_Rank ON Master_Rank.RANK_No=DEPT_Rank.RANK_No\n";
			SQL += "                                        AND R_Year = " + R_Year + " And R_Round = " + R_Round + "\n";
			SQL += "                                        AND DEPT_CODE='" + DEPT_CODE.Replace("'", "''") + "' AND MINOR_CODE='" + MINOR_CODE.Replace("'", "''") + "' \n";
			SQL += " ORDER BY Master_Rank.RANK_No DESC\n";

			SqlDataAdapter DA = new SqlDataAdapter(SQL, BL.ConnectionString());
			DataTable DT = new DataTable();
			DA.Fill(DT);

			rptRank.DataSource = DT;
			rptRank.DataBind();
			pnlAction.Visible = ddlRound.SelectedIndex > 0 & !string.IsNullOrEmpty(ddlOrganize.Text);

		}

		protected void rptRank_ItemCommand(object source, System.Web.UI.WebControls.RepeaterCommandEventArgs e)
		{
			if (e.Item.ItemType != ListItemType.Item & e.Item.ItemType != ListItemType.AlternatingItem)
				return;
			DataTable DT = CurrentRANKData();
			DT.Columns.RemoveAt(e.Item.ItemIndex);
		}

		protected void rptRank_ItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
		{
			if (e.Item.ItemType != ListItemType.Item & e.Item.ItemType != ListItemType.AlternatingItem)
				return;

			Label lblNo =(Label) e.Item.FindControl("lblNo");
			Label lblName =(Label) e.Item.FindControl("lblName");
			TextBox txtStart =(TextBox) e.Item.FindControl("txtStart");
			TextBox txtEnd =(TextBox) e.Item.FindControl("txtEnd");

            DataRowView drv = (DataRowView)e.Item.DataItem;
			lblName.Text = drv["RANK_Name"].ToString ();
			lblNo.Text = drv["Rank_No"].ToString ();
			if (!GL.IsEqualNull(drv["RANK_Start"])) {
				txtStart.Text = GL.StringFormatNumber(drv["RANK_Start"], 0);
			}
			TC.ImplementJavaIntegerText(txtStart);
			txtStart.Style["text-align"] = "center";

			if (!GL.IsEqualNull(drv["RANK_End"])) {
				txtEnd.Text = GL.StringFormatNumber(drv["RANK_End"], 0);
			}
			TC.ImplementJavaIntegerText(txtEnd);
			txtEnd.Style["text-align"] = "center";

		}

		public DataTable CurrentRANKData()
		{
			DataTable DT = new DataTable();
			DT.Columns.Add("RANK_No", typeof(int));
			DT.Columns.Add("RANK_Start", typeof(int));
			DT.Columns.Add("RANK_End", typeof(int));
			foreach (RepeaterItem Item in rptRank.Items) {
				if (Item.ItemType != ListItemType.Item & Item.ItemType != ListItemType.AlternatingItem)
					continue;
				Label lblNo =(Label) Item.FindControl("lblNo");
				TextBox txtStart =(TextBox) Item.FindControl("txtStart");
				TextBox txtEnd = (TextBox) Item.FindControl("txtEnd");
				DataRow DR = DT.NewRow();
				DR["RANK_No"] = lblNo.Text;
				if (!string.IsNullOrEmpty(txtStart.Text)) {
					DR["RANK_Start"] = txtStart.Text;
				}
				if (!string.IsNullOrEmpty(txtEnd.Text)) {
					DR["RANK_End"] = txtEnd.Text;
				}
				DT.Rows.Add(DR);
			}
			return DT;
		}

		protected void btnSave_Click(object sender, System.EventArgs e)
		{
			DataTable TT = CurrentRANKData();
			TT.DefaultView.Sort = "RANK_No ASC";

			DataTable DT = TT.DefaultView.ToTable();

			//If DT.Rows.Count = 0 Then
			//    ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "showAlert('กรอกข้อมูลให้สมบูรณ์');", True)
			//    Exit Sub
			//End If

			int mn = 0;
			int mx = 500;

			if (GL.IsEqualNull(DT.Rows[DT.Rows.Count - 1]["RANK_End"]) || GL.CDBL ( DT.Rows[DT.Rows.Count - 1]["RANK_End"]) > mx) {
				rptRank.Items[DT.Rows.Count - 1].FindControl("txtEnd").Focus();
				ScriptManager.RegisterStartupScript(this.Page, typeof(string), "Alert", "showAlert('คะแนนสูงสุดคือ " + mx + "');", true);
				return;
			}

			if (GL.IsEqualNull(DT.Rows[0]["Rank_Start"]) || GL.CDBL (DT.Rows[0]["Rank_Start"]) != mn) {
				rptRank.Items[0].FindControl("txtStart").Focus();
				ScriptManager.RegisterStartupScript(this.Page, typeof(string), "Alert", "showAlert('คะแนนต่ำสุดคือ " + mn + "');", true);
				return;
			}

			for (int i = 0; i <= DT.Rows.Count - 1; i++) {
				if (GL.IsEqualNull(DT.Rows[i]["Rank_Start"])) {
					rptRank.Items[i].FindControl("txtStart").Focus();
					ScriptManager.RegisterStartupScript(this.Page, typeof(string), "Alert", "showAlert('กรอกช่วงคะแนนให้สมบูรณ์');", true);
					return;
				}
				if (GL.IsEqualNull(DT.Rows[i]["Rank_End"])) {
					rptRank.Items[i].FindControl("txtEnd").Focus();
					ScriptManager.RegisterStartupScript(this.Page, typeof(string), "Alert", "showAlert('กรอกช่วงคะแนนให้สมบูรณ์');", true);
					return;
				}

				if (i > 0 && GL.CDBL (DT.Rows[i]["Rank_Start"]) != GL.CDBL ( DT.Rows[i - 1]["Rank_End"]) + 1) {
					rptRank.Items[i].FindControl("txtStart").Focus();
					ScriptManager.RegisterStartupScript(this.Page, typeof(string), "Alert", "showAlert('การไล่ลำดับช่วงคะแนนไม่ถูกต้อง กรุณาตรวจสอบอีกครั้ง');", true);
					return;
				}

				if (GL.CDBL (DT.Rows[i]["Rank_End"]) < GL.CDBL (DT.Rows[i]["RANK_Start"])) {
					rptRank.Items[i].FindControl("txtEnd").Focus();
					ScriptManager.RegisterStartupScript(this.Page, typeof(string), "Alert", "showAlert('การไล่ลำดับช่วงคะแนนไม่ถูกต้อง กรุณาตรวจสอบอีกครั้ง');", true);
					return;
				}
			}

			//----------------- Remove Old Row---------------
			SqlConnection Conn = new SqlConnection(BL.ConnectionString());
			Conn.Open();
			SqlCommand Comm = new SqlCommand();
			var _with1 = Comm;
			_with1.Connection = Conn;
			_with1.CommandType = CommandType.Text;
			_with1.CommandText = "DELETE FROM tb_HR_Assessment_Rank WHERE R_Year = " + R_Year + " And R_Round = " + R_Round + " AND DEPT_CODE='" + DEPT_CODE.Replace("'", "''") + "' AND MINOR_CODE='" + MINOR_CODE.Replace("'", "''") + "'";
			_with1.ExecuteNonQuery();
			_with1.Dispose();
			//----------------- Create New Row---------------
			string SQL = "SELECT *\n";
			SQL += " FROM tb_HR_Assessment_Rank \n";
			SQL += " WHERE R_Year = " + R_Year + " And R_Round = " + R_Round + "\n";
			SqlDataAdapter CA = new SqlDataAdapter(SQL, Conn);
			DataTable CT = new DataTable();
			CA.Fill(CT);
			for (int i = 0; i <= DT.Rows.Count - 1; i++) {
				DataRow CR = CT.NewRow();
				CR["R_Year"] = R_Year;
				CR["R_Round"] = R_Round;
				CR["DEPT_CODE"] = DEPT_CODE;
				CR["MINOR_CODE"] = MINOR_CODE;
				CR["RANK_No"] = i + 1;
				CR["RANK_Start"] = DT.Rows[i]["RANK_Start"];
				CR["RANK_End"] = DT.Rows[i]["RANK_End"];
				CR["Update_By"] = PSNL_NO;
				CR["Update_Time"] = DateAndTime.Now;
				CT.Rows.Add(CR);
			}

			SqlCommandBuilder cmd = new SqlCommandBuilder(CA);
			CA.Update(CT);
			Conn.Close();
			Conn.Dispose();

			ScriptManager.RegisterStartupScript(this.Page, typeof(string), "alert", "showAlert('บันทึกสำเร็จ');", true);
			BindRank();

		}
		public Assessment_Team_Set_Rank()
		{
			Load += Page_Load;
		}


	}
}

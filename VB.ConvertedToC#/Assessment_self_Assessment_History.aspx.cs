using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Xml.Linq;
using System.Diagnostics;
using System.Collections.Specialized;
using System.Configuration;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Caching;
using System.Web.SessionState;
using System.Web.Security;
using System.Web.Profile;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
namespace VB
{

	public partial class Assessment_self_Assessment_History : System.Web.UI.Page
	{

		HRBL BL = new HRBL();

		GenericLib GL = new GenericLib();
		public int R_Year {
			get {
				try {
					return GL.CINT( GL.SplitString(ddlRound.Items[ddlRound.SelectedIndex].Value, "-")[0]);
				} catch (Exception ex) {
					return 0;
				}
			}
		}

		public int R_Round {
			get {
				try {
					return GL.CINT(GL.SplitString(ddlRound.Items[ddlRound.SelectedIndex].Value, "-")[1]);
                }
                catch (Exception ex)
                {
					return 0;
				}
			}
		}

		public string PSNL_NO {
			get {
				try {
					return Session["USER_PSNL_NO"].ToString ();
				} catch (Exception ex) {
					return "";
				}
			}
		}

		protected void Page_Load(object sender, System.EventArgs e)
		{
			if ((Session["USER_PSNL_NO"] == null)) {
				ScriptManager.RegisterStartupScript(this.Page, typeof(string), "Alert", "showAlert('กรุณาลงชื่อเข้าใช้ระบบ');", true);
				Response.Redirect("Login.aspx");
				return;
			}

			if (!IsPostBack) {
				//----------------- ทำสองที่ Page Load กับ Update Status ---------------
				BL.Update_KPI_Status_To_Assessment_Period(PSNL_NO);
				//----------------- ทำสองที่ Page Load กับ Update Status ---------------
				BL.BindDDlYearRound(ddlRound);

                BL.BindDDlYearRound_ForTYPE(ddlRound, R_Year, R_Round, PSNL_NO);     //------------แสดงเฉพาะรอบที่มีสิทธิ์การเข้าประเมิน
				ddlRound.Items[0].Text = "ทั้งหมด";

				BindAssessment();
			}
		}


		private void BindAssessment()
		{
			string SQL = "";

			SQL += " DECLARE @R_Year As INT";
			if (R_Year != 0) {
				SQL += "=" + R_Year;
			}
			SQL += "\n";
			SQL += " DECLARE @R_Round As INT";
			if (R_Round != 0) {
				SQL += "=" + R_Round;
			}
			SQL += "\n";
			SQL += " DECLARE @PSNL_NO As nvarchar(50)='" + PSNL_NO.Replace("'", "''") + "'\n";
			SQL += " SELECT R_Year,R_Round,PSNL_NO,PSNL_Fullname,WAGE_NAME\n";
			SQL += " \t  ,PNPS_CLASS,POS_Name,ASS_Status,ASS_Status_Name,AssType\n";
			SQL += " \t  ,TOTAL,Weight,RESULT\n";
			SQL += " FROM\n";
			SQL += " (\n";
			SQL += " \tSELECT R_Year,R_Round,PSNL_NO,PSNL_Fullname,WAGE_NAME\n";
			SQL += " \t\t  ,PNPS_CLASS,POS_Name,ASS_Status,ASS_Status_Name,AssType\n";
			SQL += " \t\t  ,TOTAL,Weight,RESULT\n";
			SQL += " \tFROM vw_RPT_KPI_Result\n";
			SQL += " \tWHERE PSNL_NO=@PSNL_NO AND (R_Year=@R_Year OR @R_Year IS NULL) AND (R_Round=@R_Round OR @R_Round IS NULL)\n";
			SQL += " \tUNION ALL\n";
			SQL += " \tSELECT R_Year,R_Round,PSNL_NO,PSNL_Fullname,WAGE_NAME\n";
			SQL += " \t\t  ,PNPS_CLASS,POS_Name,ASS_Status,ASS_Status_Name,AssType\n";
			SQL += " \t\t  ,TOTAL,Weight,RESULT\n";
			SQL += " \tFROM vw_RPT_COMP_Result\n";
			SQL += " \tWHERE PSNL_NO=@PSNL_NO AND (R_Year=@R_Year OR @R_Year IS NULL) AND (R_Round=@R_Round OR @R_Round IS NULL)\n";
			SQL += " ) ASS\n";
			switch (ddlAssType.SelectedIndex) {
				case 0:
					//KPI+COMP
					break;

				case 1:
					//KPI
					SQL += " WHERE AssType='KPI'\n";
					break;
				case 2:
					//Competency
					SQL += " WHERE AssType='COMP'\n";
					break;
			}
			SQL += " ORDER BY R_Year,R_Round\n";

			SqlDataAdapter DA = new SqlDataAdapter(SQL, BL.ConnectionString());
			DataTable DT = new DataTable();
			DA.Fill(DT);

			Session["Assessment_self_Assessment_History"] = DT;
			Pager.SesssionSourceName = "Assessment_self_Assessment_History";
			Pager.RenderLayout();

			if (DT.Rows.Count == 0) {
				lblCountASS.Text = "ไม่พบรายการดังกล่าว";
			} else {
                lblCountASS.Text = "พบ " + GL.StringFormatNumber(DT.Rows.Count.ToString(), 0) + " รายการ";
			}
		}

		protected void ddl_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			BindAssessment();
		}

		protected void Pager_PageChanging(PageNavigation Sender)
		{
			Pager.TheRepeater = rptAss;
		}

		string LastRound = "";
		protected void rptAss_ItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
		{
			if (e.Item.ItemType != ListItemType.AlternatingItem & e.Item.ItemType != ListItemType.Item)
				return;

            DataRowView drv = (DataRowView)e.Item.DataItem;

            Label lblRound = (Label)e.Item.FindControl("lblRound");
            Label lblType = (Label)e.Item.FindControl("lblType");
            Label lblResult = (Label)e.Item.FindControl("lblResult");
            Label lblPercent = (Label)e.Item.FindControl("lblPercent");
            Label lblStatus = (Label)e.Item.FindControl("lblStatus");
            HtmlAnchor lnkView = (HtmlAnchor)e.Item.FindControl("lnkView");

            string Round = "ปี " + drv["R_Year"] + " รอบ " + drv["R_Round"];
			if (Round != LastRound) {
				lblRound.Text = Round;
				LastRound = Round;
			}

			switch (drv["AssType"].ToString()) {
				case "KPI":
					lblType.Text = "ตัวชี้วัด (KPI)";
					lnkView.HRef = "Print/RPT_K_PNSAss.aspx?MODE=PDF&R_Year=" + drv["R_Year"] + "&R_Round=" + drv["R_Round"] + "&PSNL_No=" + PSNL_NO + "&Status=" + drv["ASS_Status"];
					break;
				case "COMP":
					lblType.Text = "สมรรถนะ (Competency)";
					lnkView.HRef = "Print/RPT_C_PNSAss.aspx?MODE=PDF&R_Year=" + drv["R_Year"] + "&R_Round=" + drv["R_Round"] + "&PSNL_No=" + PSNL_NO + "&Status=" + drv["ASS_Status"];
					break;
			}

			if (!GL.IsEqualNull(drv["RESULT"])) {
				lblResult.Text = drv["RESULT"].ToString();
				lblPercent.Text = GL.StringFormatNumber(  (GL.CDBL(drv["RESULT"].ToString()) / 5).ToString(), 2) + " %";
			} else {
				lblResult.Text = "-";
				lblPercent.Text = "-";
			}

			//If Not GL.IsEqualNull(drv["ASS_Status"]) AndAlso drv["ASS_Status"] = 5 Then
			//    lblStatus.ForeColor = Drawing.Color.Green
			//Else
			//    lblStatus.ForeColor = Drawing.Color.Red
			//End If

			//------ปรับสีสถานะ-----------
			//----<0
            if (GL.IsEqualNull(drv["ASS_Status"]) || GL.CINT(drv["ASS_Status"]) == HRBL.AssessmentStatus.Creating)
            {
				lblStatus.ForeColor = System.Drawing.Color.Red;
			//----1
            }
            else if (GL.IsEqualNull(drv["ASS_Status"]) || GL.CINT(drv["ASS_Status"]) == HRBL.AssessmentStatus.WaitCreatingApproved)
            {
				lblStatus.ForeColor = System.Drawing.Color.DarkRed;
			//----2
            }
            else if (GL.IsEqualNull(drv["ASS_Status"]) || GL.CINT(drv["ASS_Status"]) == HRBL.AssessmentStatus.CreatedApproved)
            {
				lblStatus.ForeColor = System.Drawing.Color.Orange;
			//----3
            }
            else if (GL.IsEqualNull(drv["ASS_Status"]) || GL.CINT(drv["ASS_Status"]) == HRBL.AssessmentStatus.WaitAssessment)
            {
				lblStatus.ForeColor = System.Drawing.Color.DarkBlue;
			//----4
            }
            else if (GL.IsEqualNull(drv["ASS_Status"]) || GL.CINT(drv["ASS_Status"]) == HRBL.AssessmentStatus.WaitConfirmAssessment)
            {
				lblStatus.ForeColor = System.Drawing.Color.BlueViolet;
			//----5
            }
            else if (GL.IsEqualNull(drv["ASS_Status"]) || GL.CINT(drv["ASS_Status"]) == HRBL.AssessmentStatus.AssessmentCompleted)
            {
				lblStatus.ForeColor = System.Drawing.Color.Green;
			}

            lblStatus.Text = drv["ASS_Status_Name"].ToString();

		}
	}
}

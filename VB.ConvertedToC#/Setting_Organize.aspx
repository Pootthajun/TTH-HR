﻿<%@ Page  Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPage.Master" CodeBehind="Setting_Organize.aspx.cs" Inherits="VB.Setting_Organize" %>

<%@ Register src="WUC_DateReporter.ascx" tagname="WUC_DateReporter" tagprefix="uc1" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<%@ Register src="PageNavigation.ascx" tagname="PageNavigation" tagprefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<asp:ToolkitScriptManager ID="toolkit1" runat="server"></asp:ToolkitScriptManager>
<asp:UpdatePanel ID="UpdatePanel1" runat="server">				           
<ContentTemplate>


<div class="container-fluid">
				<!-- BEGIN PAGE HEADER-->
				<div class="row-fluid" id="fixedPageHeader">
					<div class="span12"> 
						<!-- BEGIN PAGE TITLE & BREADCRUMB-->
						<h3 class="page-user-md">หน่วยงาน (ข้อมูลจาก Oracle) <font color="blue">(ScreenID : S-BAS-02)</font></h3>		
						<ul class="breadcrumb">
                            <li>
                            	<i class="icon-cogs"></i><a href="javascript:;">จัดการระบบ/ข้อมูลพื้นฐาน</a><i class="icon-angle-right"></i>
                            </li>
                        	<li><i class="icon-sitemap"></i> <a href="javascript:;">หน่วยงาน</a></li>                        	
                            <uc1:WUC_DateReporter ID="WUC_DateReporter1" runat="server" />
                        </ul>
						<!-- END PAGE TITLE & BREADCRUMB-->									        
				     </div>				
			    </div>
		        <!-- END PAGE HEADER-->
				
				<asp:Panel ID="pnlList" runat="server" DefaultButton="btnSearch">
                   <div class="row-fluid">					
					    <div class="span12">
						    <!-- BEGIN SAMPLE TABLE PORTLET-->
			                    <div class="row-fluid form-horizontal">
	                                <div class="span4 ">
									    <div class="control-group">
								            <label class="control-label"><i class="icon-sitemap"></i> ฝ่าย</label>
								            <div class="controls">
								                <asp:DropDownList ID="ddlSector" OnSelectedIndexChanged="Search_Changed" runat="server" AutoPostBack="true" CssClass="medium m-wrap">
								                </asp:DropDownList>
								            </div>
							            </div>
								    </div>
								    <div class="span6 ">
									    <div class="control-group">
								            <label class="control-label"><i class="icon-sitemap"></i> ค้นหา</label>
								            <div class="controls">
									            <asp:TextBox  ID="txtFilter" OnTextChanged="Search_Changed" runat="server" AutoPostBack="true" CssClass="m-wrap large" placeholder="ชื่อหน่วยงาน"></asp:TextBox> 														       
								            </div>
							            </div>
								    </div>	
								    <asp:Button ID="btnSearch" OnClick="Search_Changed" runat="server" Text="" style="display:none;" />
	                            </div>    							                        	
						        <div class="portlet-body no-more-tables" style="width:auto  ">
						            <asp:Label ID="lblCountList" runat="server" Font-Size="14px" Font-Bold="true"></asp:Label>
								    <table class="table table-bordered  table-full-width table-advance dataTable no-more-tables table-hover">
									    <thead style="text-align:center;">                                        
										    <tr>											
											    <th style="text-align:center;"><i class="icon-sitemap"></i> ฝ่าย</th>
											    <th style="text-align:center;"><i class="icon-sitemap"></i> หน่วยงาน</th>
											    <th style="text-align:center;"><i class="icon-sitemap"></i> รหัสหน่วยงาน</th>                                                
											    <th style="text-align:center;"><i class="icon-group"></i> จำนวนบุคลากร</th>
                                                <th style="text-align:center;"><i class="icon-user-md"></i> ระดับหัวหน้า</th>
                                                <th style="text-align:center;"><i class="icon-user-md"></i> ระดับรองหัวหน้า</th>
                                                <th style="text-align:center;"><i class="icon-download-alt"></i> ข้อมูลล่าสุด ณ วันที่</th>
										    </tr>
									    </thead>
									    <tbody style="text-align:center; background-color:White ;">
										    <asp:Repeater ID="rptList" OnItemDataBound="rptList_ItemDataBound" runat="server">
										        <ItemTemplate>
    										            <tr>
                                                            <td data-title="ฝ่าย"><asp:Label ID="lblSector" runat="server"></asp:Label></td>
											                <td data-title="หน่วยงาน"><asp:Label ID="lblDept" runat="server"></asp:Label></td>
                                                            <td data-title="รหัสหน่วยงาน"><asp:Label ID="lblDeptCode" runat="server"></asp:Label></td>											                
                                                            <td data-title="จำนวนบุคลากร"><asp:Label ID="lblPSN" runat="server"></asp:Label></td>
                                                            <td data-title="ระดับหัวหน้า"><asp:Label ID="lblMGR" runat="server"></asp:Label></td>
                                                            <td data-title="ระดับรองหัวหน้า"><asp:Label ID="lblVICE" runat="server"></asp:Label></td>
                                                            <td data-title="ข้อมูลล่าสุด ณ วันที่"><asp:Label ID="lblUpdate" runat="server"></asp:Label></td>
                                                        </tr>
										        </ItemTemplate>
										    </asp:Repeater>
                                        </tbody>
                                    </table>
                                    <asp:PageNavigation ID="Pager" OnPageChanging="Pager_PageChanging" MaximunPageCount="5" PageSize="20" runat="server" />
                                    <div class="form-horizontal form-view" style="display:none;">
                                        <div class="form-actions">
									        <asp:Button ID="btn" runat="server" CssClass="btn green" Text="ดึงข้อมูลล่าสุดจากระบบทรัพยากรบุคคล(Oracle)" />
								        </div>
                                    </div>								

							    </div>
    						
						    <!-- END SAMPLE TABLE PORTLET-->
    						
					    </div>
                    </div>
               </asp:Panel>
               
							 
</ContentTemplate>				                
</asp:UpdatePanel>	

</asp:Content>


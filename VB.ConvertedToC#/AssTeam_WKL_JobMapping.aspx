﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPage.Master" CodeBehind="AssTeam_WKL_JobMapping.aspx.cs" Inherits="VB.AssTeam_WKL_JobMapping" %>

<%@ Register src="WUC_DateReporter.ascx" tagname="WUC_DateReporter" tagprefix="uc1" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<asp:UpdatePanel ID="UpdatePanel1" runat="server">
<ContentTemplate>
<asp:ToolkitScriptManager ID="toolkit1" runat="server"></asp:ToolkitScriptManager>

<div class="container-fluid">
				<!-- BEGIN PAGE HEADER-->
				<div class="row-fluid" id="fixedPageHeader">
					<div class="span12"> 
						<!-- BEGIN PAGE TITLE & BREADCRUMB-->
						<h3 class="page-title">แผนที่งาน (Job Mapping) <font color="blue">(ScreenID : S-MGR-07)</font></h3>
						<ul class="breadcrumb">
                            <li>
                            	<i class="icon-dashboard"></i><a href="javascript:;">การบริหารอัตรากำลัง</a><i class="icon-angle-right"></i>
                            </li>
                        	<li>
                        	    <i class="icon-table"></i> <a href="javascript:;">แผนที่งาน (Job Mapping)</a>
                        	</li>
                            <uc1:WUC_DateReporter ID="WUC_DateReporter1" runat="server" />
                        </ul>
						<!-- END PAGE TITLE & BREADCRUMB-->									        
				     </div>				
			    </div>
		        <!-- END PAGE HEADER-->
				
			<div class="portlet-body form">
											
					<!-- BEGIN HEADER-->
					     
						<h3 class="form-section"><i class="icon-table"></i> สำหรับ 
						    <asp:DropDownList ID="ddlOrganize" OnSelectedIndexChanged="ddlOrganize_SelectedIndexChanged" runat="server" AutoPostBack="true" CssClass="m-wrap" style="font-size:20px; width:auto !important; padding:0px;">
						    </asp:DropDownList>
						</h3>					
					
					<!-- END HEADER-->  
					
					 <!-- BEGIN FORM-->
				              
						<div class="portlet-body no-more-tables">
								<table class="table table-bordered table-hover" style="background-color:White; width:100%;">
									        <thead>
										        <tr>
											        <th style="text-align:center; font-size:12px; background-color:#CCCCCC;"> งานหลัก (Task)</th>
											        <th style="text-align:center; font-size:12px; background-color:#CCCCCC;"> งาน</th>
											        <asp:Repeater ID="rptHeaderClass" OnItemDataBound="rptHeaderClass_ItemDataBound" runat="server">
											            <ItemTemplate>
											                <th style="text-align:center; font-size:12px; background-color:#CCCCCC;">
											                <asp:Label ID="lblClassName" runat="server"></asp:Label>
											                </th>
											            </ItemTemplate>
											        </asp:Repeater>
											        <th style="text-align:center; font-size:12px; background-color:#CCCCCC;"> ดำเนินการ</th>
									            </tr>                                               
									        </thead>
									        <tbody>
									            <asp:Repeater ID="rptJob" OnItemCommand="rptJob_ItemCommand" OnItemDataBound="rptJob_ItemDataBound" runat="server">
									                <ItemTemplate>
									                    <tr>
										                  <td id="tdIndent" runat="server">&nbsp;</td>
										                  <td id="tdLevel" runat="Server" style="padding:5px; cursor:pointer;" >
										                   <asp:Label ID="lblDisplayName" runat="Server"></asp:Label>
										                   <asp:Label ID="lblName" runat="Server" style="display:none;"></asp:Label>
										                  </td>
										                  <asp:Repeater ID="rptSub" runat="server">
										                    <ItemTemplate>
										                        <td style="padding:5px; cursor:pointer;" id="tdTask" runat="server">
										                           <asp:Label ID="lblTask" runat="Server"></asp:Label>
										                        </td>
										                    </ItemTemplate>
										                  </asp:Repeater>
										                  <td style="text-align:left; width:80px;">
										                      <div class="btn-group">										                       
										                       <a class="btn dropdown-toggle mini" data-toggle="dropdown">ดำเนินการ <i class="icon-angle-down"></i></a>
									                            <ul class="dropdown-menu pull-right">
										                          <li><a ID="btnInsert" runat="Server" href="javascript:;"><i class="icon-indent-right"></i> แทรกรายการก่อนหน้า</a></li>
										                          <li><a ID="btnAppend" runat="Server" href="javascript:;"><i class="icon-indent-right"></i> เพิ่มรายการถัดไป</a></li>
										                          <li><asp:LinkButton ID="btnAddSub" runat="Server" CommandName="AddSub"><i class="icon-circle-arrow-down"></i> เพิ่มงานย่อย</asp:LinkButton></li>
										                          <li><asp:LinkButton ID="btnUp" runat="Server" CommandName="Up"><i class="icon-chevron-up"></i> เลื่อนขึ้น</asp:LinkButton></li>
										                          <li><asp:LinkButton ID="btnDown" runat="Server" CommandName="Down"><i class="icon-chevron-down"></i> เลื่อนลง</asp:LinkButton></li>
										                          <li><asp:LinkButton ID="btnDelete" runat="Server" CommandName="Delete"><i class="icon-trash"></i>ลบหมวดนี้</asp:LinkButton></li>										                          
										                          <asp:ConfirmButtonExtender TargetControlID="btnDelete" ID="cfmBtnDelete" ConfirmText="ยืนยันลบหมวดนี้" runat="server"></asp:ConfirmButtonExtender>
									                            </ul>									                          
									                          </div>
										                  </td>
									                    </tr>
									                </ItemTemplate>
									            </asp:Repeater>									            					
									        </tbody>
								        </table>
							</div>
				    <!-- END FORM-->  
				    
				    
                            <asp:Panel CssClass="form-actions" ID="pnlAction" runat="server">
							    
							    <a class="btn blue" onclick="showDialog(0,1,0,0,0,'','AppendJob','เพิ่มงานหลัก (Task)');">เพิ่มงานหลัก (Task)</a>
							    <asp:Button ID="btnClear" OnClick="btnClear_Click" runat="server" CssClass="btn red" Text="ลบแผนที่งานทั้งหมด" />
							    <asp:ConfirmButtonExtender TargetControlID="btnClear" ID="cfmBtnClear" ConfirmText="ยืนยันลบแผนที่งานทั้งหมด" runat="server"></asp:ConfirmButtonExtender>
							</asp:Panel>
				    
				</div>
          
          <div class="modal" id="divModal" runat="server" style="width:400px; top:20%; visibility:hidden;" >
			<div class="modal-header">
				<h3><asp:Label ID="lblHeader" runat="server"></asp:Label></h3>
			</div>
			<div class="modal-body" style="padding:0px; overflow:hidden;">
			<asp:TextBox ID="txtDialog" MaxLength="1000" TextMode="MultiLine" runat="server" Rows="4" CssClass="large m-wrap" style="width:100% !important; height:100%; margin:0px;"></asp:TextBox>			        
			
			</div>
			<div class="modal-footer">
			    <asp:Button ID="btnOK" OnClick="btnOK_Click" runat="server" CssClass="btn blue" Text="บันทึก" />
			    <a class="btn" onClick="document.getElementById('ctl00_ContentPlaceHolder1_divModal').style.visibility='hidden';">ยกเลิก</a>			    
			</div>			
			
			<asp:TextBox ID="txt_Job_LEVEL" runat="server" style="width:0px; height:0px; display:none;" Text="0"></asp:TextBox>
            <asp:TextBox ID="txt_Job_ID" runat="server" style="width:0px; height:0px; display:none;" Text="0"></asp:TextBox>
            <asp:TextBox ID="txt_Job_Parent" runat="server" style="width:0px; height:0px; display:none;" Text="0"></asp:TextBox>
            <asp:TextBox ID="txt_Job_No" runat="server" style="width:0px; height:0px; display:none;" Text="0"></asp:TextBox>
            <asp:TextBox ID="txt_Class_ID" runat="server" style="width:0px; height:0px; display:none;" Text="0"></asp:TextBox>
            <asp:TextBox ID="txt_Mode" runat="server" style="width:0px; height:0px; display:none;" Text="0"></asp:TextBox>
		</div>
         
</div>

    
										
</ContentTemplate>           
</asp:UpdatePanel>

<script language="javascript" type="text/javascript">

    function showDialog(Job_ID, Job_LEVEL, Job_Parent, Job_No, Class_ID, Job_Name, EditMode, HeaderText) {
        document.getElementById("ctl00_ContentPlaceHolder1_txt_Job_LEVEL").value = Job_LEVEL;
        document.getElementById("ctl00_ContentPlaceHolder1_txt_Job_ID").value = Job_ID;
        document.getElementById("ctl00_ContentPlaceHolder1_txt_Job_Parent").value = Job_Parent;
        document.getElementById("ctl00_ContentPlaceHolder1_txt_Job_No").value = Job_No;
        document.getElementById("ctl00_ContentPlaceHolder1_txt_Class_ID").value = Class_ID;
        document.getElementById("ctl00_ContentPlaceHolder1_txt_Mode").value = EditMode;
        document.getElementById("ctl00_ContentPlaceHolder1_txtDialog").value = Job_Name;
        document.getElementById("ctl00_ContentPlaceHolder1_lblHeader").innerHTML = HeaderText;
        document.getElementById("ctl00_ContentPlaceHolder1_divModal").style.visibility = 'visible';
        document.getElementById("ctl00_ContentPlaceHolder1_txtDialog").focus();
    }
        
</script>
	
</asp:Content>



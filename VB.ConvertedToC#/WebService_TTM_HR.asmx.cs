
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Xml.Linq;
using System.Diagnostics;
using System.Collections.Specialized;
using System.Configuration;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Caching;
using System.Web.SessionState;
using System.Web.Security;
using System.Web.Profile;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.ComponentModel;

using System.Data.SqlClient;
namespace VB
{




	// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line.
	// <System.Web.Script.Services.ScriptService()> _
	[System.Web.Services.WebService(Namespace = "http://tempuri.org/")]
	[System.Web.Services.WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
	[ToolboxItem(false)]
	public class WebService_TTM_HR : System.Web.Services.WebService
	{

        GenericLib GL=new GenericLib();

		public struct KPICompetncyResult
		{
			public int ReturnCode;
			public string Status;
			public double KPI;
			public double Competency;
			public double LessLeave;

			public double Score;
			public void Clear()
			{
				ReturnCode = -1;
				Status = "";
				KPI = 0;
				Competency = 0;
				LessLeave = 0;
				Score = 0;
			}
		}

		KPICompetncyResult result;
		HRBL BL = new HRBL();
		[WebMethod()]
		public KPICompetncyResult GetKPICompetencyResult(int R_Year, int R_Round, string PNPS_PSNL_NO)
		{

			string SQL = " ";
			SQL += " SELECT vw_RPT_Yearly_Result.* , vw_PN_PSNL_ALL.STAT_NAME ";
			SQL += " ,CASE WHEN KPI_Status_1=1 AND COMP_Status_1=1 AND KPI_Status_2=1 AND COMP_Status_2=1 THEN 'ประเมินเสร็จสมบูรณ์' ELSE 'ประเมินยังไม่เสร็จ' END Status\n";
			SQL += " FROM vw_RPT_Yearly_Result \n";
			SQL += " INNER JOIN vw_PN_PSNL_ALL ON vw_RPT_Yearly_Result.PSNL_NO = vw_PN_PSNL_ALL.PSNL_NO \n";
			SQL += "  WHERE R_Year =" + R_Year + "\n";
			SQL += " AND vw_RPT_Yearly_Result.PSNL_NO ='" + PNPS_PSNL_NO + "'\n";
			SQL += " ORDER BY SECTOR_NAME ASC ,SECTOR_CODE ASC,DEPT_CODE ASC ,PNPS_CLASS DESC  ,Year_Score  DESC\n";
			SqlDataAdapter DA = new SqlDataAdapter(SQL, BL.ConnectionString());
			DataTable DT = new DataTable();
			DA.Fill(DT);

			result.Clear();
			if (DT.Rows.Count > 0) {
				result.ReturnCode = 0;
				result.Status = DT.Rows[0]["Status"].ToString();
				if (R_Round == 1) {
					result.KPI = GL.CDBL(DT.Rows[0]["KPI_Result_1"]);
					result.Competency = GL.CDBL(DT.Rows[0]["Comp_Result_1"]);
					result.LessLeave =GL.CDBL( DT.Rows[0]["Leave_Score"]);
					result.Score =GL.CDBL( DT.Rows[0]["Ass_Score_1"]);
				} else {
					result.KPI =GL.CDBL( DT.Rows[0]["KPI_Result_2"]);
					result.Competency =GL.CDBL( DT.Rows[0]["Comp_Result_2"]);
					result.LessLeave = GL.CDBL(DT.Rows[0]["Leave_Score"]);
					result.Score = GL.CDBL(DT.Rows[0]["Ass_Score_2"]);
				}
			}

			return result;

		}

	}
}

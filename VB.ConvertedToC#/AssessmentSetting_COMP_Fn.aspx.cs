using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Xml.Linq;
using System.Diagnostics;
using System.Collections.Specialized;
using System.Configuration;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Caching;
using System.Web.SessionState;
using System.Web.Security;
using System.Web.Profile;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
namespace VB
{

	public partial class AssessmentSetting_COMP_Fn : System.Web.UI.Page
	{

		HRBL BL = new HRBL();
		GenericLib GL = new GenericLib();

		textControlLib TC = new textControlLib();
		public int R_Year {
			get {
				try {
					return GL.CINT( GL.SplitString(ddlRound.Items[ddlRound.SelectedIndex].Value, "-")[0]);
				} catch (Exception ex) {
					return 0;
				}
			}
		}

		public int R_Round {
			get {
				try {
					return GL.CINT(GL.SplitString(ddlRound.Items[ddlRound.SelectedIndex].Value, "-")[1]);
				} catch (Exception ex) {
					return 0;
				}
			}
		}

		public int PSNL_CLASS_GROUP {
			get {
				try {
					return Convert.ToInt32(Conversion.Val(lblClassName.Attributes["CLSGP_ID"]));
				} catch (Exception ex) {
					return 0;
				}
			}
            set { lblClassName.Attributes["CLSGP_ID"] = value.ToString(); }
		}

		public int PSNL_TYPE {
			get {
				try {
					return Convert.ToInt32(Conversion.Val(lblPSNType.Attributes["PSNL_Type_Code"]));
				} catch (Exception ex) {
					return -1;
				}
			}
            set { lblPSNType.Attributes["PSNL_Type_Code"] = value.ToString(); }
		}

		public string FN_ID {
			get {
				try {
					return lblFNName.Attributes["FN_ID"];
				} catch (Exception ex) {
					return "0";
				}
			}
			set { lblFNName.Attributes["FN_ID"] = value; }
		}

		public string FN_Type {
			get {
				try {
					return lblFNName.Attributes["FN_Type"];
				} catch (Exception ex) {
					return "0";
				}
			}
			set { lblFNName.Attributes["FN_Type"] = value; }
		}

		public int FN_No {
			get {
				try {
					return Convert.ToInt32(Conversion.Val(lblCOMPNo.Attributes["FN_No"]));
				} catch (Exception ex) {
					return 0;
				}
			}
			set {
                lblCOMPNo.Attributes["FN_No"] = value.ToString();
                lblCOMPNo.Text = value.ToString();
			}
		}


		protected void Page_Load(object sender, System.EventArgs e)
		{
			if ((Session["USER_PSNL_NO"] == null)) {
				ScriptManager.RegisterStartupScript(this.Page, typeof(string), "Alert", "showAlert('กรุณาลงชื่อเข้าใช้ระบบ');", true);
				Response.Redirect("Login.aspx");
				return;
			}

			if (!IsPostBack) {
				BL.BindDDlYearRound(ddlRound);
				ddlRound.Items.RemoveAt(0);
				BL.BindDDlCOMPClassGroup(ddlClassGroup, R_Year, R_Round);
				BL.BindDDlPSNType(ddlPSNType);
				BL.BindDDlFLDCode(ddlFN);
				BindCOMPList();
				pnlList.Visible = true;
				pnlEdit.Visible = false;
			}
		}

		private void BindCOMPList()
		{
			string SQL = "";
			SQL += " SELECT CLSGP_ID,FN_ID,FN_Type,FN_Name\n";
			SQL += " ,PNPO_CLASS_Start,PNPO_CLASS_End\n";
			SQL += " ,PSNL_Type_Code,PSNL_Type_Name\n";
			SQL += " ,TOTAL_COMP,TOTAL_GAP\n";
			SQL += " FROM vw_HR_COMP_Master_FN_Header \n";
			SQL += " WHERE R_Year=" + R_Year + " AND R_Round=" + R_Round + "\n";

			if (ddlClassGroup.SelectedIndex > 0) {
				SQL += " AND CLSGP_ID=" + ddlClassGroup.Items[ddlClassGroup.SelectedIndex].Value + "\n";
			}
			if (ddlPSNType.SelectedIndex > 0) {
				SQL += " AND PSNL_Type_Code=" + ddlPSNType.Items[ddlPSNType.SelectedIndex].Value + "\n";
			}
			if (ddlFN.SelectedIndex > 0) {
				SQL += " AND FN_Name='" + ddlFN.Items[ddlFN.SelectedIndex].Text.Replace("'", "''") + "'\n";
			}
			if (chkBlank.Checked) {
				SQL += " AND Total_COMP=0 \n";
			}

			SQL += " ORDER BY FN_Name,PNPO_CLASS_Start,PSNL_Type_Code DESC";

			SqlDataAdapter DA = new SqlDataAdapter(SQL, BL.ConnectionString());
			DataTable DT = new DataTable();
			DA.Fill(DT);

			Session["AssessmentSetting_COMP_FN"] = DT;
			Pager.SesssionSourceName = "AssessmentSetting_COMP_FN";
			Pager.RenderLayout();

			if (DT.Rows.Count == 0) {
				lblCountList.Text = "ไม่พบรายการดังกล่าว";
			} else {
				lblCountList.Text = "พบ " + GL.StringFormatNumber(DT.Rows.Count, 0) + " รายการ";
			}

		}

		protected void Pager_PageChanging(PageNavigation Sender)
		{
			Pager.TheRepeater = rptList;
		}

		protected void Search(object sender, System.EventArgs e)
		{
			BindCOMPList();
		}

		protected void ddlRound_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			BL.BindDDlCOMPClassGroup(ddlClassGroup, R_Year, R_Round);
			BindCOMPList();
		}

		protected void rptList_ItemCommand(object source, System.Web.UI.WebControls.RepeaterCommandEventArgs e)
		{

Button btnEdit =(Button) e.Item.FindControl("btnEdit");
			switch (e.CommandName) {
				case "Edit":
					

					PSNL_CLASS_GROUP = GL.CINT ( btnEdit.Attributes["CLSGP_ID"]);
					PSNL_TYPE = GL.CINT (btnEdit.Attributes["PSNL_Type_Code"]);
					FN_ID = btnEdit.Attributes["FN_ID"].ToString ();
					FN_Type = btnEdit.Attributes["FN_Type"];

					//----------------Bind COMP Header-----------------
					DataTable DT = BL.GetCOMPFNHeader(GL.CINT(R_Year), GL.CINT(R_Round), GL.CINT(PSNL_CLASS_GROUP), GL.CINT(PSNL_TYPE), FN_ID.ToString (), FN_Type.ToString ());
					if (DT.Rows.Count == 0) {
						ScriptManager.RegisterStartupScript(this.Page, typeof(string), "Alert", "showAlert('กำลังปรับปรุงข้อมูลล่าสุด');", true);
						BindCOMPList();
						return;
					}
					lblPSNType.Text = DT.Rows[0]["PSNL_Type_Name"].ToString ();
					int mn = Convert.ToInt32(DT.Rows[0]["PNPO_CLASS_Start"]);
					int mx = Convert.ToInt32(DT.Rows[0]["PNPO_CLASS_End"]);
					if (mn == mx) {
						lblClassName.Text = GL.CINT (mn).ToString ();
					} else {
						lblClassName.Text = mn + " - " + mx;
					}
					lblYearRound.Text = "ปี " + R_Year + " รอบ " + R_Round;
					lblFNName.Text = DT.Rows[0]["FN_Name"].ToString ();

					BindCOMPDetail();
					//------------ Toggle show----------
					pnlList.Visible = false;
					pnlEdit.Visible = true;
					divView.Visible = true;
					divEdit.Visible = false;

					break;
				case "Delete":

					
					int CLSGP_ID =GL.CINT ( btnEdit.Attributes["CLSGP_ID"]);
					string PSNL_Type_Code = btnEdit.Attributes["PSNL_Type_Code"];
                    string FN_ID_Delete = btnEdit.Attributes["FN_ID"];
					string FN_Type_Delete = btnEdit.Attributes["FN_Type"];

					string SQL = "DELETE FROM tb_HR_COMP_Master_FN_GAP \n";
					SQL += " WHERE R_Year=" + R_Year + " AND R_Round=" + R_Round + "\n";
					SQL += " AND CLSGP_ID=" + CLSGP_ID + " AND PNPO_TYPE='" + PSNL_Type_Code + "'\n";
                    SQL += " AND FN_ID='" + FN_ID_Delete + "' AND FN_Type='" + FN_Type_Delete + "'\n\n";

					SQL += "DELETE FROM tb_HR_COMP_Master_FN \n";
					SQL += " WHERE R_Year=" + R_Year + " AND R_Round=" + R_Round + "\n";
					SQL += " AND CLSGP_ID=" + CLSGP_ID + " AND PNPO_TYPE='" + PSNL_Type_Code + "'\n";
                    SQL += " AND FN_ID='" + FN_ID_Delete + "' AND FN_Type='" + FN_Type_Delete + "'\n";

					SqlConnection Conn = new SqlConnection(BL.ConnectionString());
					Conn.Open();
					SqlCommand Comm = new SqlCommand();
					var _with1 = Comm;
					_with1.Connection = Conn;
					_with1.CommandType = CommandType.Text;
					_with1.CommandText = SQL;
					_with1.ExecuteNonQuery();
					_with1.Dispose();
					Conn.Close();
					Conn.Dispose();

					BindCOMPList();

					break;
			}
		}

		private void BindCOMPDetail()
		{
			//----------------Bind COMP Detail-----------------
			HRBL.DataManager DM = BL.GetCOMPMasterFNDetail(GL.CINT(R_Year), GL.CINT(R_Round), GL.CINT(PSNL_CLASS_GROUP), GL.CINT(PSNL_TYPE), FN_ID.ToString (), FN_Type.ToString ());;
			DataTable DT = DM.Table;
			//-------- Keep GAP-----------
			GAP = BL.GetCOMPMasterFNGAP(GL.CINT(R_Year), GL.CINT(R_Round), GL.CINT(PSNL_CLASS_GROUP), GL.CINT(PSNL_TYPE), FN_ID.ToString (), FN_Type.ToString ()).Table;
			rptCOMP.DataSource = DT;
			rptCOMP.DataBind();
		}


		private void ClearEditForm()
		{
			FN_No = 0;
			txtCOMP.Text = "";
			txtSTD.Text = "";
			txtChoice1.Text = "";
			txtChoice2.Text = "";
			txtChoice3.Text = "";
			txtChoice4.Text = "";
			txtChoice5.Text = "";

			rptGAP.DataSource = null;
			rptGAP.DataBind();

		}

		//------------ For Grouping -----------
		string LastFN = "";
		string LastClass = "";
		protected void rptList_ItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
		{
			if (e.Item.ItemType != ListItemType.AlternatingItem & e.Item.ItemType != ListItemType.Item)
				return;

			Label lblFLD =(Label) e.Item.FindControl("lblFLD");
			Label lblClassGroup =(Label) e.Item.FindControl("lblClassGroup");
			Label lblPSNType =(Label) e.Item.FindControl("lblPSNType");
			Label lblTotalCOMP =(Label) e.Item.FindControl("lblTotalCOMP");
			HtmlGenericControl iconGAP = (HtmlGenericControl) e.Item.FindControl("iconGAP");
			Button btnEdit =(Button) e.Item.FindControl("btnEdit");
			Button btnDelete =(Button) e.Item.FindControl("btnDelete");
			AjaxControlToolkit.ConfirmButtonExtender cfm_Delete =(AjaxControlToolkit.ConfirmButtonExtender) e.Item.FindControl("cfm_Delete");
            
            DataRowView drv = (DataRowView)e.Item.DataItem;
			int mn = GL.CINT (drv["PNPO_CLASS_Start"]);
			int mx = Convert.ToInt32(drv["PNPO_CLASS_End"]);
			string ClassName = "";
			if (mn == mx) {
				ClassName =GL.CINT ( mn).ToString ();
			} else {
				ClassName = mn + " - " + mx;
			}

			//--------- คนละสายอาชีพ ----------
			if (drv["FN_Name"].ToString () != LastFN) {
				LastFN = drv["FN_Name"].ToString ();
				lblFLD.Text = LastFN;
				LastClass = ClassName;
				lblClassGroup.Text = ClassName;
			//--------- สายอาชีพเดียวกัน เปลี่ยนกลุ่ม ----------
			} else if (LastClass != ClassName) {
				LastClass = ClassName;
				lblClassGroup.Text = ClassName;
			}

			lblPSNType.Text = drv["PSNL_Type_Name"].ToString ();
			lblTotalCOMP.Text = drv["TOTAL_COMP"].ToString ();
			if (!GL.IsEqualNull(drv["TOTAL_GAP"]) && GL.CINT ( drv["TOTAL_GAP"]) > 0) {
				iconGAP.Attributes["class"] = "icon-ok";
				iconGAP.Style["color"] = "green";
			} else {
				iconGAP.Attributes["class"] = "icon-minus";
				iconGAP.Style["color"] = "silver";
			}

			cfm_Delete.ConfirmText = "ยืนยันลบแบบประเมินสมรรถนะหลักของ พนักงาน" + lblPSNType.Text + " สายงาน" + drv["FN_Name"] + " กลุ่มระดับ " + ClassName;

			btnDelete.Visible = GL.CINT ( drv["TOTAL_COMP"]) > 0;

			btnEdit.Attributes["CLSGP_ID"] = drv["CLSGP_ID"].ToString ();
			btnEdit.Attributes["PSNL_Type_Code"] = drv["PSNL_Type_Code"].ToString ();
			btnEdit.Attributes["FN_ID"] = drv["FN_ID"].ToString ();
			btnEdit.Attributes["FN_Type"] = drv["FN_Type"].ToString ();

		}

		protected void rptCOMP_ItemCommand(object source, System.Web.UI.WebControls.RepeaterCommandEventArgs e)
		{

                    Button btnDelete =(Button) e.Item.FindControl("btnDelete");
			switch (e.CommandName) {
				case "Edit":
					

					ClearEditForm();
					FN_No = GL.CINT ( btnDelete.CommandArgument);
					FN_ID = btnDelete.Attributes["FN_ID"];
					FN_Type = btnDelete.Attributes["FN_Type"];

					DataTable DT = BL.GetCOMPMasterFNDetail(R_Year, R_Round, PSNL_CLASS_GROUP, PSNL_TYPE, FN_ID, FN_Type, FN_No).Table;
					if (DT.Rows.Count == 0) {
						ScriptManager.RegisterStartupScript(this.Page, typeof(string), "Alert", "showAlert('กำลังปรับปรุงข้อมูลล่าสุด');", true);
						BindCOMPDetail();
						return;
					}

					txtCOMP.Text = DT.Rows[0]["FN_Comp"].ToString().Replace("\n", "\n");
					txtSTD.Text = DT.Rows[0]["FN_Std"].ToString().Replace("\n", "\n");
					txtChoice1.Text = DT.Rows[0]["FN_Choice_1"].ToString().Replace("\n", "\n");
					txtChoice2.Text = DT.Rows[0]["FN_Choice_2"].ToString().Replace("\n", "\n");
					txtChoice3.Text = DT.Rows[0]["FN_Choice_3"].ToString().Replace("\n", "\n");
					txtChoice4.Text = DT.Rows[0]["FN_Choice_4"].ToString().Replace("\n", "\n");
					txtChoice5.Text = DT.Rows[0]["FN_Choice_5"].ToString().Replace("\n", "\n");



					//---------------- Bind GAP ----------------
					DT = BL.GetCOMPMasterFNGAP(R_Year, R_Round, PSNL_CLASS_GROUP, PSNL_TYPE, FN_ID, FN_Type, FN_No).Table;
					rptGAP.DataSource = DT;
					rptGAP.DataBind();

					divEdit.Visible = true;
					divView.Visible = false;

					break;
				case "Delete":


                    int FN_No_Delete =GL.CINT ( btnDelete.CommandArgument);
					string FN_ID_Delete = btnDelete.Attributes["FN_ID"];
                    string FN_Type_Delete = btnDelete.Attributes["FN_Type"];

					string SQL = "DELETE FROM tb_HR_COMP_Master_FN_GAP \n";
					SQL += " WHERE R_Year=" + R_Year + " AND R_Round=" + R_Round + "\n";
					SQL += " AND CLSGP_ID=" + PSNL_CLASS_GROUP + " AND PNPO_TYPE='" + PSNL_TYPE + "'\n";
                    SQL += " AND FN_ID='" + FN_ID_Delete + "' AND FN_Type='" + FN_Type_Delete + "' AND FN_No=" + FN_No_Delete + "\n\n";


					SQL += "DELETE FROM tb_HR_COMP_Master_FN \n";
					SQL += " WHERE R_Year=" + R_Year + " AND R_Round=" + R_Round + "\n";
					SQL += " AND CLSGP_ID=" + PSNL_CLASS_GROUP + " AND PNPO_TYPE='" + PSNL_TYPE + "'\n";
                    SQL += " AND FN_ID='" + FN_ID_Delete + "' AND FN_Type='" + FN_Type_Delete + "' AND FN_No=" + FN_No_Delete + "\n\n";

					SqlConnection Conn = new SqlConnection(BL.ConnectionString());
					Conn.Open();
					SqlCommand Comm = new SqlCommand();
					var _with2 = Comm;
					_with2.Connection = Conn;
					_with2.CommandType = CommandType.Text;
					_with2.CommandText = SQL;
					_with2.ExecuteNonQuery();
					_with2.Dispose();
					Conn.Close();
					Conn.Dispose();

					BindCOMPDetail();

					break;
			}
		}

		DataTable GAP = null;
		protected void rptCOMP_ItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
		{
			if (e.Item.ItemType != ListItemType.AlternatingItem & e.Item.ItemType != ListItemType.Item)
				return;

			HtmlTableCell ass_no =(HtmlTableCell)  e.Item.FindControl("ass_no");
			HtmlTableCell COMP_Name =(HtmlTableCell)  e.Item.FindControl("COMP_Name");
			HtmlTableCell target =(HtmlTableCell) e.Item.FindControl("target");
			HtmlTableCell GAPList =(HtmlTableCell) e.Item.FindControl("GAPList");
			Button btnEdit =(Button) e.Item.FindControl("btnEdit");
			Button btnDelete =(Button) e.Item.FindControl("btnDelete");
			AjaxControlToolkit.ConfirmButtonExtender cfm_Delete =(AjaxControlToolkit.ConfirmButtonExtender) e.Item.FindControl("cfm_Delete");

            DataRowView drv = (DataRowView)e.Item.DataItem;
			ass_no.InnerHtml = drv["FN_No"].ToString ();
			//.ToString().PadLeft(2, GL.chr0)
			COMP_Name.InnerHtml = drv["FN_Comp"].ToString().Replace("\n", "<br>");
			target.InnerHtml = drv["FN_Std"].ToString().Replace("\n", "<br>");
			for (int i = 1; i <= 5; i++) {
				HtmlTableCell choice =(HtmlTableCell) e.Item.FindControl("choice" + i);
				choice.InnerHtml = drv["FN_Choice_" + i].ToString().Replace("\n", "<br>");
			}

			//-------------- Bind GAP ----------------
			string tmp = "";
			GAP.DefaultView.RowFilter = "FN_No=" + drv["FN_No"];
			for (int i = 0; i <= GAP.DefaultView.Count - 1; i++) {
				tmp += "<p><i class='icon-book'></i> " + GAP.DefaultView[i]["GAP_Name"] + "</p>";
			}
			GAPList.InnerHtml = tmp;

			//----------------- Assign FN No-------------------
			btnDelete.CommandArgument = drv["FN_No"].ToString ();
			//*****************
			btnDelete.Attributes["FN_ID"] = drv["FN_ID"].ToString ();
			//*****************
			btnDelete.Attributes["FN_Type"] = drv["FN_Type"].ToString ();
			//*****************

			cfm_Delete.ConfirmText = "ยืนยันลบแบบประเมินลำดับ " + drv["FN_No"];

			//----------------- Add Click Action -----------------
			ass_no.Attributes["onclick"] = "document.getElementById('" + btnEdit.ClientID + "').click();";
			ass_no.Attributes["title"] = "Click เพื่อแก้ไข";
			ass_no.Style["cursor"] = "pointer";

			COMP_Name.Attributes["onclick"] = "document.getElementById('" + btnEdit.ClientID + "').click();";
			COMP_Name.Attributes["title"] = "Click เพื่อแก้ไข";
			COMP_Name.Style["cursor"] = "pointer";

			target.Attributes["onclick"] = "document.getElementById('" + btnEdit.ClientID + "').click();";
			target.Attributes["title"] = "Click เพื่อแก้ไข";
			target.Style["cursor"] = "pointer";

			GAPList.Attributes["onclick"] = "document.getElementById('" + btnEdit.ClientID + "').click();";
			GAPList.Attributes["title"] = "Click เพื่อแก้ไข";
			GAPList.Style["cursor"] = "pointer";

			for (int i = 1; i <= 5; i++) {
				HtmlTableCell choice =(HtmlTableCell) e.Item.FindControl("choice" + i);
				choice.Attributes["onclick"] = "document.getElementById('" + btnEdit.ClientID + "').click();";
				choice.Attributes["title"] = "Click เพื่อแก้ไข";
				choice.Style["cursor"] = "pointer";
			}

		}

		protected void rptGAP_ItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
		{
			if (e.Item.ItemType != ListItemType.AlternatingItem & e.Item.ItemType != ListItemType.Item)
				return;
            DataRowView drv = (DataRowView)e.Item.DataItem;
			Label lblGAP =(Label) e.Item.FindControl("lblGAP");
			lblGAP.Text = (e.Item.ItemIndex + 1) + "." + drv["GAP_Name"].ToString ();
		}



		protected void btnAdd_Click(object sender, System.EventArgs e)
		{
			ClearEditForm();
            int NewFNNo = BL.GetNewFNNo(R_Year, R_Round, PSNL_CLASS_GROUP, PSNL_TYPE.ToString(), FN_ID.ToString(), FN_Type.ToString());
			FN_No = NewFNNo;

			divEdit.Visible = true;
			divView.Visible = false;

		}

		protected void btnClose_Click(object sender, System.EventArgs e)
		{
			BindCOMPDetail();
			//------------ Toggle show----------
			divView.Visible = true;
			divEdit.Visible = false;
		}


		protected void btnOK_Click(object sender, System.EventArgs e)
		{
            ////--PSNL_CLASS_GROUP
            //for (int i = 0; i <= 4; i++) {
            //    //--PSNL_TYPE
            //    for (int j = 0; j <= 2; j++) {

					//------------------ Save COMP Detail-----------------
                    //HRBL.DataManager DM = BL.GetCOMPMasterFNDetail(R_Year, R_Round, i + 1, j, FN_ID, FN_Type, FN_No);
            HRBL.DataManager DM = BL.GetCOMPMasterFNDetail(R_Year, R_Round, PSNL_CLASS_GROUP, PSNL_TYPE, FN_ID, FN_Type, FN_No);
             

					DataTable DT = DM.Table;
					SqlDataAdapter DA = DM.Adaptor;

					DataRow DR = null;
					if (DT.Rows.Count == 0) {
						DR = DT.NewRow();
                        DR["R_Year"] = R_Year;
                        DR["R_Round"] = R_Round;
                        DR["CLSGP_ID"] = PSNL_CLASS_GROUP;
						DR["PNPO_TYPE"] = PSNL_TYPE ;
						DR["FN_ID"] = FN_ID;
						DR["FN_Type"] = FN_Type;
						DR["FN_No"] = FN_No;
						DR["Create_By"] = Session["USER_PSNL_NO"];
						DR["Create_Time"] = DateAndTime.Now;
					} else {
						DR = DT.Rows[0];
					}
					DR["FN_Comp"] = txtCOMP.Text;
					DR["FN_Std"] = txtSTD.Text;
					DR["FN_Choice_1"] = txtChoice1.Text;
					DR["FN_Choice_2"] = txtChoice2.Text;
					DR["FN_Choice_3"] = txtChoice3.Text;
					DR["FN_Choice_4"] = txtChoice4.Text;
					DR["FN_Choice_5"] = txtChoice5.Text;

					DR["Update_By"] = Session["USER_PSNL_NO"];
					DR["Update_Time"] = DateAndTime.Now;

					if (DT.Rows.Count == 0)
						DT.Rows.Add(DR);
					SqlCommandBuilder cmd = new SqlCommandBuilder();
					try {
						cmd = new SqlCommandBuilder(DA);
						DA.Update(DT);
					} catch (Exception ex) {
						ScriptManager.RegisterStartupScript(this.Page, typeof(string), "Alert1", "showAlert('ไม่สามารถบันทึกข้อมูลได้');", true);
						ScriptManager.RegisterStartupScript(this.Page, typeof(string), "Alert2", "showAlert('" + ex.Message.Replace("'", "\"") + "');", true);
						return;
					}


            //    }
            //}


            

			//'------------------ Save COMP Detail-----------------
			//Dim DM As HRBL.DataManager = BL.GetCOMPMasterFNDetail(R_Year, R_Round, PSNL_CLASS_GROUP, PSNL_TYPE, FN_ID, FN_Type, FN_No)
			//Dim DT As DataTable = DM.Table
			//Dim DA As SqlDataAdapter = DM.Adaptor

			//Dim DR As DataRow
			//If DT.Rows.Count = 0 Then
			//    DR = DT.NewRow
			//    DR("R_Year") = R_Year
			//    DR("R_Round") = R_Round
			//    DR("CLSGP_ID") = PSNL_CLASS_GROUP
			//    DR("PNPO_TYPE") = PSNL_TYPE
			//    DR("FN_ID") = FN_ID
			//    DR("FN_Type") = FN_Type
			//    DR("FN_No") = FN_No
			//    DR("Create_By") = Session("USER_PSNL_NO")
			//    DR("Create_Time") = Now
			//Else
			//    DR = DT.Rows(0)
			//End If
			//DR("FN_Comp") = txtCOMP.Text
			//DR("FN_Std") = txtSTD.Text
			//DR("FN_Choice_1") = txtChoice1.Text
			//DR("FN_Choice_2") = txtChoice2.Text
			//DR("FN_Choice_3") = txtChoice3.Text
			//DR("FN_Choice_4") = txtChoice4.Text
			//DR("FN_Choice_5") = txtChoice5.Text

			//DR("Update_By") = Session("USER_PSNL_NO")
			//DR("Update_Time") = Now

			//If DT.Rows.Count = 0 Then DT.Rows.Add(DR)
			//Dim cmd As New SqlCommandBuilder()
			//Try
			//    cmd = New SqlCommandBuilder(DA)
			//    DA.Update(DT)
			//Catch ex As Exception
			//    ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert1", "showAlert('ไม่สามารถบันทึกข้อมูลได้');", True)
			//    ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert2", "showAlert('" & ex.Message.Replace("'", """") & "');", True)
			//    Exit Sub
			//End Try

			BindCOMPDetail();
			//------------ Toggle show----------
			divView.Visible = true;
			divEdit.Visible = false;
		}

		protected void btnCancel_Click(object sender, System.EventArgs e)
		{
			BindCOMPList();
			pnlEdit.Visible = false;
			pnlList.Visible = true;
		}
		public AssessmentSetting_COMP_Fn()
		{
			Load += Page_Load;
		}

	}
}

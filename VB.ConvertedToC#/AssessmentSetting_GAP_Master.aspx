﻿<%@ Page  Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPage.Master" CodeBehind="AssessmentSetting_GAP_Master.aspx.cs" Inherits="VB.AssessmentSetting_GAP_Master" %>

<%@ Register src="WUC_DateReporter.ascx" tagname="WUC_DateReporter" tagprefix="uc1" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<%@ Register src="PageNavigation.ascx" tagname="PageNavigation" tagprefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<asp:ToolkitScriptManager ID="toolkit1" runat="server"></asp:ToolkitScriptManager>
<asp:UpdatePanel ID="UpdatePanel1" runat="server">
<ContentTemplate>
<div class="container-fluid">
                <!-- BEGIN PAGE HEADER-->
                <div class="row-fluid" id="fixedPageHeader">
					<div class="span12"> 
						<!-- BEGIN PAGE TITLE & BREADCRUMB-->
						<h3>หัวข้อหลักสูตรการพัฒนา <font color="blue">(ScreenID : S-HDR-13)</font></h3>					
						
						<ul class="breadcrumb">
                            <li>
                            	<i class="icon-book"></i> <a href="javascript:;">หลักสูตรการพัฒนา</a><i class="icon-angle-right"></i>
                            </li>
                        	<li>
                        	    <i class="icon-book"></i> <a href="javascript:;">กำหนดหัวข้อ</a>
                        	</li>                        	
                        	
                            <uc1:WUC_DateReporter ID="WUC_DateReporter1" runat="server" />
                        </ul>
						<!-- END PAGE TITLE & BREADCRUMB-->
									        
				     </div>			
			    </div>
		        <!-- END PAGE HEADER-->
		        
		        <asp:Panel ID="pnlList" runat="server" Visible="True" DefaultButton="btnSearch"> 
                    <div class="row-fluid">
    					
					    <div class="span12">
						    <!-- BEGIN SAMPLE TABLE PORTLET-->
    				                    
									    <div class="row-fluid form-horizontal">
									        <div class="span4 ">
											    <div class="control-group">
										            <label class="control-label"><i class="icon-book"></i> ชื่อหลักสูตร</label>
										            <div class="controls">
											            <asp:TextBox ID="txtSearchName" OnTextChanged="Search" runat="server" AutoPostBack="true" CssClass="m-wrap medium" placeholder="ค้นหาจากรหัส/ชื่อหลักสูตร"></asp:TextBox>
										            </div>
									            </div>
										    </div>
										    <div class="span3 ">
											    <div class="control-group">
										            <label class="control-label"><i class="icon-check"></i> กำหนดใช้ในปัจจุบัน</label>
										            <div class="controls">
											            <asp:DropDownList ID="ddlSearchStatus" OnSelectedIndexChanged="Search" runat="server" AutoPostBack="true" CssClass="medium m-wrap">
											                <asp:ListItem Text="ทั้งหมด" ></asp:ListItem>
											                <asp:ListItem Text="ใช้" Selected="True"></asp:ListItem>
											                <asp:ListItem Text="ไม่ใช้แล้ว"></asp:ListItem>
											            </asp:DropDownList>
										            </div>
									            </div>
										    </div>
									    </div>
								        <asp:Button ID="btnSearch" OnClick="Search" runat="server" Text="" style="display:none;" />
    								    
							    <div class="portlet-body no-more-tables">
    								
								    <asp:Label ID="lblCountGAP" runat="server" Font-Size="14px" Font-Bold="true"></asp:Label>
								    <table class="table table-full-width  no-more-tables table-hover">
									    <thead>
										    <tr>
											    <th rowspan="2" style="text-align:center;"><i class="icon-caret-right"></i> รหัส</th>
											    <th rowspan="2" style="text-align:center;"><i class="icon-book"></i> หลักสูตร</th>
											    <th colspan="3" style="text-align:center;"><i class="icon-check"></i> ใช้เติมช่องว่างสรรถนะ</th>
											    <th rowspan="2" style="text-align:center;"><i class="icon-check"></i> กำหนดใช้</th>
											    <th rowspan="2" style="text-align:center;"><i class="icon-bolt"></i> กำหนด</th>
										    </tr>
										    <tr>											    
											    <th style="text-align:center;"><i class="icon-indent-right"></i> หลัก (Core)</th>
											    <th style="text-align:center;"><i class="icon-indent-right"></i> สายงาน (Functional)</th>
											    <th style="text-align:center;"><i class="icon-indent-right"></i> สายระดับ (Managerial)</th>											    
										    </tr>
									    </thead>
									    <tbody>
										    <asp:Repeater ID="rptGAP" OnItemCommand="rptGAP_ItemCommand" OnItemDataBound="rptGAP_ItemDataBound" runat="server">
									            <ItemTemplate>
    									            <tr>                                        
											            <td data-title="รหัส" style="text-align:center;"><asp:Label ID="lblNo" runat="server"></asp:Label></td>
											            <td data-title="หลักสูตร"><asp:Label ID="lblName" runat="server"></asp:Label></td>
											            <td data-title="สมรรถนะหลัก" style="text-align:center;"><asp:Label ID="lblCore" runat="server"></asp:Label></td>
											            <td data-title="สมรรถนะตามสายงาน" style="text-align:center;"><asp:Label ID="lblFN" runat="server"></asp:Label></td>
											            <td data-title="สมรรถนะตามสายระดับ" style="text-align:center;"><asp:Label ID="lblMGR" runat="server"></asp:Label></td>
											            <td data-title="กำหนดใช้" style="text-align:center;">
											                <asp:ImageButton ID="btnToggle" runat="server" ImageUrl="images/check.png" Height="24px" CommandName="Active" ToolTip="Click เพื่อเปลี่ยน" />
											            </td>
											            <td data-title="ดำเนินการ" style="text-align:center;">
											                <asp:Button ID="btnGAPEdit" runat="server" CssClass="btn mini blue" CommandName="Edit" Text="กำหนด" /> 
											                <asp:Button ID="btnGAPDelete" runat="server" CssClass="btn mini red" CommandName="Delete" Text="ลบ" />
											                <asp:ConfirmButtonExtender TargetControlID="btnGAPDelete" ID="cfm_Delete" runat="server"></asp:ConfirmButtonExtender>
										                </td>
										            </tr>	
									            </ItemTemplate>
									        </asp:Repeater>					
									    </tbody>
								    </table>
    								
								    <asp:PageNavigation ID="Pager" OnPageChanging="Pager_PageChanging" MaximunPageCount="10" PageSize="20" runat="server" />
							    </div>
						    <!-- END SAMPLE TABLE PORTLET-->						
					    </div>
                    </div>
                </asp:Panel>
                
                            <asp:Panel CssClass="modal" id="pnlModal" runat="server" style="top:15%; position:fixed; width:auto;" visible="False" >
									<div class="modal-header" >										
										<h3 style=" margin-right:30px;">กำหนดหลักสูตรการพัฒนา </h3>
									</div>
									<div class="modal-body">
										
											<div class="row-fluid" >
												<div class="form-horizontal">												        
												        <div class="control-group">
													        <label class="control-label">รหัส</label>
													        <div class="controls">
														        <asp:TextBox CssClass="m-wrap large" ID="txtCode" GAP_ID="0" MaxLength="20" runat="server" Enabled="false"></asp:TextBox>
													        </div>
												        </div>											        												        
											   </div>
											</div>
											<div class="row-fluid">
												<div class="form-horizontal">												        
												        <div class="control-group">
													        <label class="control-label">ชื่อหลักสูตร</label>
													        <div class="controls">
														        <asp:TextBox CssClass="m-wrap large" ID="txtName" MaxLength="200" runat="server" ></asp:TextBox>
													        </div>
												        </div>											        												        
											   </div>
											</div>
											<div class="row-fluid">
												<div class="form-horizontal">												        
												        <div class="control-group">
													        <label class="control-label">รายละเอียด</label>
													        <div class="controls">
														        <asp:TextBox CssClass="m-wrap large" TextMode="MultiLine" ID="txtDesc" MaxLength="2000" runat="server"></asp:TextBox>
													        </div>
												        </div>											        												        
											   </div>
											</div>
											<div class="row-fluid">
												<div class="form-horizontal">												        
												        <div class="control-group">
												            <label class="control-label">กำหนดใช้</label>													        
													        <div class="controls">
								                                <asp:ImageButton ID="btnActive" OnClick="btnActive_Click" runat="server" ImageUrl="images/check.png" Height="24px" ToolTip="" />
							                                </div>													        
												        </div>										        												        
											   </div>
											</div>
										
									</div>
									<div class="modal-footer">
									    <asp:Button ID="btnOK" OnClick="btnOK_Click" runat="server" CssClass="btn blue" Text="บันทึก" />
									    <asp:Button ID="btnClose" OnClick="btnClose_Click" runat="server" CssClass="btn" Text="ยกเลิก" />																				
									</div>
								</asp:Panel>
								
                        <%-- เลือกหลักสูตรที่กำหนด --%>
		                <asp:Panel CssClass="modal" style="top:5%; position:fixed; width:auto;" id="ModalTraining" runat="server" DefaultButton="btnSearchTraining"  >
				            <div class="modal-header">										
					            <h3>หลักสูตรการพัฒนา </h3>
					            <asp:Label ID="lblTrainingNo" runat="server" style="display:none;"></asp:Label>
				            </div>
				            <div class="modal-body">					            
					            <div class="row-fluid form-horizontal">
					                 <div class="span12 ">
									    <div class="control-group">
								            <label class="control-label" style="width:120px;"><i class="icon-book"></i> ชื่อหลักสูตร</label>
								            <div class="controls">
									            <asp:TextBox ID="txtSearchTraining" OnTextChanged="SearchTraining" runat="server" AutoPostBack="true" CssClass="m-wrap large" placeholder="ค้นหาจากชื่อหลักสูตร"></asp:TextBox>
									            <asp:Button ID="btnSearchTraining" OnClick="SearchTraining" runat="server" Text="" style="display:none;" />
								            </div>
							            </div>
								    </div>								       
					            </div>
					            <div class="portlet-body no-more-tables">
    								
								    <asp:Label ID="lblCountTraining" runat="server" Font-Size="14px" Font-Bold="true"></asp:Label>
								    <table class="table table-full-width  no-more-tables table-hover">
									    <thead>
										    <tr>
											    <th style="text-align:center;"><i class="icon-caret-right"></i> รหัส</th>
											    <th style="text-align:center;"><i class="icon-book"></i> หลักสูตร</th>											    
											    <th style="text-align:center;"><i class="icon-check"></i> เลือก</th>
										    </tr>										    
									    </thead>
									    <tbody>
										    <asp:Repeater ID="rptTrainingDialog" OnItemCommand="rptTrainingDialog_ItemCommand" OnItemDataBound="rptTrainingDialog_ItemDataBound" runat="server">
									            <ItemTemplate>
    									            <tr>                                        
											            <td data-title="รหัส" style="text-align:center;"><asp:Label ID="lblNo" runat="server"></asp:Label></td>
											            <td data-title="หลักสูตร"><asp:Label ID="lblName" runat="server"></asp:Label></td>											           
											            <td data-title="ดำเนินการ" style="text-align:center;">
											                <asp:Button ID="btnTrainingSelect" runat="server" CssClass="btn mini blue" CommandName="Select" Text="เลือก" /> 
										                </td>
										            </tr>	
									            </ItemTemplate>
									        </asp:Repeater>					
									    </tbody>
								    </table>
    								
								    <asp:PageNavigation ID="Pager_Training" OnPageChanging="Pager_Training_PageChanging" MaximunPageCount="10" PageSize="5" runat="server" />
							    </div>				            
				            </div>
				            <div class="modal-footer">
				                <asp:Button ID="btnClose_Training" OnClick="btnClose_Training_Click" runat="server" CssClass="btn" Text="ปิด" />								
				            </div>
			            </asp:Panel>


						        <div class="form-actions">
								    <asp:Button ID="btnAdd" OnClick="btnAdd_Click" runat="server" CssClass="btn purple" Text="เพิ่มหลักสูตร" />
								</div>
</div>
</ContentTemplate>
</asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ScriptPlaceHolder" Runat="Server">
</asp:Content>


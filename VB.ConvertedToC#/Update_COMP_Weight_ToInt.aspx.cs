﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Web;
//using System.Web.UI;
//using System.Web.UI.WebControls;
//using System.Data;
//using System.Data.SqlClient;

using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Xml.Linq;
using System.Diagnostics;
using System.Collections.Specialized;
using System.Configuration;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Caching;
using System.Web.SessionState;
using System.Web.Security;
using System.Web.Profile;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
namespace VB
{
    public partial class Update_COMP_Weight_ToInt : System.Web.UI.Page
    {

        HRBL BL = new HRBL();
        GenericLib GL = new GenericLib();

        protected void Page_Load(object sender, EventArgs e)
        {


            if (!IsPostBack) {
                BindPersonalList();
                Update_Weight();
            
            }
        }


        private void BindPersonalList()
        {
            string SQL = "SELECT DISTINCT vw.SECTOR_CODE,vw.SECTOR_NAME,vw.DEPT_CODE,vw.DEPT_NAME,vw.PSNL_NO,vw.PSNL_Fullname,vw.PNPS_CLASS,vw.WAGE_NAME\n";
            SQL += " ,vw.POS_NO,vw.POS_Name,vw.COMP_Status,vw.COMP_Status_Name\n";
            SQL += " FROM vw_RPT_COMP_Status vw\n";
            SQL += " WHERE vw.SECTOR_CODE<>'10' AND R_Year=2558 AND R_Round=2 \n";
            SQL += " AND vw.PNPS_CLASS BETWEEN 7 AND 9                 \n";
            SQL += "ORDER BY vw.SECTOR_CODE,vw.DEPT_CODE,vw.PNPS_CLASS DESC,vw.POS_NO\n";

            SqlDataAdapter DA = new SqlDataAdapter(SQL, BL.ConnectionString());
            DA.SelectCommand.CommandTimeout = 90;
            DataTable DT = new DataTable();
            DA.Fill(DT);

            Session["Update_COMP_Weight_ToInt"] = DT;
                      
        }

        private void Update_Weight()
        {

            DataTable DT = new DataTable();
            DT = (DataTable)Session["Update_COMP_Weight_ToInt"];
            if (DT.Rows.Count > 0) {
                for (int i = 0; i <= DT.Rows.Count - 1; i++) {

                    BL.Update_COMP_Weight_Float_To_COMP_Weight_Int(DT.Rows[i]["PSNL_NO"].ToString(), 2558, 2, true );
                
                
                
                }
                ScriptManager.RegisterStartupScript(this.Page, typeof(string), "Alert", "showAlert('Update เรียบร้อยแล้ว');", true);
            
            }
            else {
                    ScriptManager.RegisterStartupScript(this.Page, typeof(string), "Alert", "showAlert('ไม่มีรายการ Update');", true);
                }



        }

    }
}
using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Xml.Linq;
using System.Diagnostics;
using System.Collections.Specialized;
using System.Configuration;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Caching;
using System.Web.SessionState;
using System.Web.Security;
using System.Web.Profile;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
namespace VB
{

	public partial class RPT_A_Round_Result : System.Web.UI.Page
	{


		HRBL BL = new HRBL();
		GenericLib GL = new GenericLib();

		Converter C = new Converter();
		public int R_Year {
			get {
				try {
					return GL.CINT( GL.SplitString(ddlRound.Items[ddlRound.SelectedIndex].Value, "-")[0]);
				} catch (Exception ex) {
					return 0;
				}
			}
		}

		public int R_Round {
			get {
				try {
					return GL.CINT(GL.SplitString(ddlRound.Items[ddlRound.SelectedIndex].Value, "-")[1]);
				} catch (Exception ex) {
					return 0;
				}
			}
		}

		protected void Page_Load(object sender, System.EventArgs e)
		{
			if ((Session["USER_PSNL_NO"] == null)) {
				ScriptManager.RegisterStartupScript(this.Page, typeof(string), "Alert", "showAlert('กรุณาลงชื่อเข้าใช้ระบบ');", true);
				Response.Redirect("Login.aspx");
				return;
			}

			if (!IsPostBack) {
				//----------------- ทำสองที่ Page Load กับ Update Status ---------------
				BL.BindDDlYearRound(ddlRound);
				ddlRound.Items.RemoveAt(0);
				BL.BindDDlSector(ddlSector);
				BindPersonalList();
				//Else
				//    ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "InitApp", "App.initUniform();", True)
			}

			SetCheckboxStyle();
		}

		private void SetCheckboxStyle()
		{
			if (chkKPIYes.Checked) {
				chkKPIYes.CssClass = "checked";
			} else {
				chkKPIYes.CssClass = "";
			}
			if (chkKPINo.Checked) {
				chkKPINo.CssClass = "checked";
			} else {
				chkKPINo.CssClass = "";
			}
			if (chkCOMPYes.Checked) {
				chkCOMPYes.CssClass = "checked";
			} else {
				chkCOMPYes.CssClass = "";
			}
			if (chkCOMPNo.Checked) {
				chkCOMPNo.CssClass = "checked";
			} else {
				chkCOMPNo.CssClass = "";
			}
		}

        protected void btnSearch_Click(object sender, System.EventArgs e)
		{
			BindPersonalList();
		}

		private void BindPersonalList()
		{
			string SQL = "";
			SQL += " SELECT KPI.R_Year,KPI.R_Round,KPI.SECTOR_CODE, KPI.SECTOR_NAME,KPI.DEPT_CODE,KPI.DEPT_NAME," + "\n";
			SQL += "  KPI.PSNL_NO,KPI.PSNL_Fullname,KPI.POS_Name,KPI.PNPS_CLASS,KPI.WAGE_TYPE,KPI.WAGE_NAME,KPI.POS_No,KPI.Ass_Status KPI_Status,COMP.ASS_Status COMP_Status," + "\n";
			SQL += "  KPI.RESULT KPI_Result,COMP.RESULT COMP_Result,R.Weight_KPI,R.Weight_COMP," + "\n";
			SQL += "  ((ISNULL(KPI.RESULT,0)*R.Weight_KPI)+(ISNULL(COMP.RESULT,0)*R.Weight_COMP))/(R.Weight_KPI+R.Weight_COMP) Total_Score," + "\n";
			SQL += "  ISNULL(SUM(L_Day),0) L_Day" + "\n";

			SQL += "  FROM vw_RPT_KPI_Result KPI " + "\n";
			SQL += "  LEFT JOIN vw_RPT_COMP_Result COMP ON KPI.R_Year=COMP.R_Year AND KPI.R_Round=COMP.R_Round AND KPI.PSNL_NO=COMP.PSNL_NO" + "\n";
			SQL += "  LEFT JOIN tb_HR_Round R ON KPI.R_Year=R.R_Year AND KPI.R_Round=R.R_Round" + "\n";
			SQL += "   LEFT JOIN vw_PN_LEAVE LEAVE ON KPI.PSNL_NO=LEAVE.PSNL_NO" + "\n";
			SQL += " \t\t\t\t\t\t\tAND L_YYMM BETWEEN RIGHT(Year(R.R_Start)+543,2)+CASE WHEN MONTH(R.R_Start)<10 THEN '0' ELSE '' END+CAST(MONTH(R.R_Start) AS nvarchar)" + "\n";
			SQL += " \t\t\t\t\t\t\t\t\t\tAND RIGHT(Year(R.R_End)+543,2)+CASE WHEN MONTH(R.R_End)<10 THEN '0' ELSE '' END+CAST(MONTH(R.R_End) AS nvarchar)" + "\n";

			SQL += " WHERE KPI.R_Year=" + R_Year + "\n";
			SQL += " AND KPI.R_Round=" + R_Round + "\n";

			string Title = ddlRound.Items[ddlRound.SelectedIndex].Text.Replace("รอบ", "ครั้งที่") + " ";


			if (ddlSector.SelectedIndex > 0) {
				SQL += " AND ( KPI.SECTOR_CODE='" + ddlSector.Items[ddlSector.SelectedIndex].Value + "' " + "\n";
				if (ddlSector.Items[ddlSector.SelectedIndex].Value == "3200") {
					SQL += " AND  KPI.DEPT_CODE NOT IN ('32000300'))    " + "\n";
				//-----------ฝ่ายโรงงานผลิตยาสูบ 3
				} else if (ddlSector.Items[ddlSector.SelectedIndex].Value == "3203") {
					SQL += " OR  KPI.DEPT_CODE IN ('32000300','32030000','32030100','32030200','32030300','32030500'))  " + "\n";
				//-----------ฝ่ายโรงงานผลิตยาสูบ 4
				} else if (ddlSector.Items[ddlSector.SelectedIndex].Value == "3204") {
					SQL += " OR  KPI.DEPT_CODE IN ('32040000','32040100','32040200','32040300','32040500','32040300'))  " + "\n";
				//-----------ฝ่ายโรงงานผลิตยาสูบ 5
				} else if (ddlSector.Items[ddlSector.SelectedIndex].Value == "3205") {
					SQL += " OR  KPI.DEPT_CODE IN ('32050000','32050100','32050200','32050300','32050500'))\t  " + "\n";
				} else {
					SQL += ")   " + "\n";
				}


				Title += GL.SplitString(ddlSector.Items[ddlSector.SelectedIndex].Text, ":")[1].Trim() + " ";
			}
			if (!string.IsNullOrEmpty(txtDeptName.Text)) {
				SQL += " AND (KPI.DEPT_Name LIKE '%" + txtDeptName.Text.Replace("'", "''") + "%' OR KPI.Sector_Name LIKE '%" + txtDeptName.Text.Replace("'", "''") + "%') " + "\n";
				Title += " หน่วยงาน '" + txtDeptName.Text + "' ";
			}
			if (!string.IsNullOrEmpty(txtName.Text)) {
				SQL += " AND (KPI.PSNL_Fullname LIKE '%" + txtName.Text.Replace("'", "''") + "%' OR KPI.PSNL_NO LIKE '%" + txtName.Text.Replace("'", "''") + "%') " + "\n";
				Title += " พนักงาน '" + txtName.Text + "' ";
			}

			int KPIStat = 0;
		    if ((chkKPIYes.Checked & chkKPINo.Checked) || (!chkKPIYes.Checked & !chkKPINo.Checked))
            {
                KPIStat = -1;
            }
            else if (chkKPIYes.Checked)
            {
                SQL += " AND KPI.ASS_Status=5 " + "\n";
                KPIStat = 1;
            }
            else if (chkKPINo.Checked)
            {
                SQL += " AND ISNULL(KPI.ASS_Status,0)<5 " + "\n";
                KPIStat = 0;
            }

			int COMPStat = 0;
            if((chkCOMPYes.Checked & chkCOMPNo.Checked) || (!chkCOMPYes.Checked & !chkCOMPNo.Checked))
            {
                COMPStat = -1;
            }
            else if(chkCOMPYes.Checked)
            {
                SQL += " AND COMP.ASS_Status=5 " + "\n";
				COMPStat = 1;
            }
            else if( chkCOMPNo.Checked)
            {
            	SQL += " AND ISNULL(COMP.ASS_Status,0)<5 " + "\n";
				COMPStat = 0;
            }
			
			if (KPIStat == COMPStat) {
				switch (KPIStat) {
					case -1:

						break;
					case 0:
						Title += "ที่การประเมินตัวชี้วัดและสมรรรถนะยังไม่เสร็จ";
						break;
					case 1:
						Title += "การประเมินเสร็จสมบูรณ์";
						break;
				}
			} else if (KPIStat == -1) {
				switch (COMPStat) {
					case 0:
						Title += "ที่การประเมินสมรรถนะยังไม่เสร็จ";
						break;
					case 1:
						Title += "ที่การประเมินสมรรถนะเสร็จสมบูรณ์";
						break;
				}
			} else if (KPIStat == 0) {
				switch (COMPStat) {
					case -1:
						Title += "ที่การประเมินตัวชี้วัดยังไม่เสร็จ";
						break;
					case 1:
						Title += "ที่การประเมินสมรรถนะเสร็จแล้วแต่ตัวชี้วัดยังไม่เสร็จ";
						break;
				}
			} else if (KPIStat == 1) {
				switch (COMPStat) {
					case -1:
						Title += "ที่การประเมินตัวชี้วัดสมบูรณ์";
						break;
					case 0:
						Title += "ที่การประเมินตัวชี้วัดเสร็จแล้วแต่สมรรถนะยังไม่เสร็จ";
						break;
				}
			}

			SQL += "  GROUP BY KPI.R_Year,KPI.R_Round,KPI.SECTOR_CODE,KPI.SECTOR_NAME,KPI.DEPT_CODE,KPI.DEPT_NAME," + "\n";
			SQL += "  KPI.PSNL_NO,KPI.PSNL_Fullname,KPI.POS_Name,KPI.POS_No,KPI.PNPS_CLASS,KPI.WAGE_TYPE,KPI.WAGE_NAME,KPI.Ass_Status,COMP.ASS_Status," + "\n";
			SQL += "  KPI.RESULT,COMP.RESULT,R.Weight_KPI,R.Weight_COMP" + "\n" + "\n";
			SQL += "  ORDER BY KPI.R_Year,KPI.R_Round,KPI.SECTOR_CODE,KPI.DEPT_CODE,KPI.PNPS_CLASS DESC,KPI.POS_No" + "\n";

			SqlDataAdapter DA = new SqlDataAdapter(SQL, BL.ConnectionString());
			DataTable DT = new DataTable();
			DA.Fill(DT);

			Session["RPT_A_Round_Result"] = DT;
			//--------- Title --------------
			Session["RPT_A_Round_Result_Title"] = Title;
			//--------- Sub Title ----------
			Session["RPT_A_Round_Result_SubTitle"] = "รายงาน ณ วันที่ " + DateTime.Now.Day + " " + C.ToMonthNameTH(DateTime.Now.Month) + " พ.ศ." + (DateTime.Now.Year + 543);
			Session["RPT_A_Round_Result_SubTitle"] += "  พบ   " + GL.StringFormatNumber(DT.Rows.Count.ToString(), 0) + " รายการ ";

			Pager.SesssionSourceName = "RPT_A_Round_Result";
			Pager.RenderLayout();
			lblTitle.Text = Title;
			if (DT.Rows.Count == 0) {
				lblCountPSN.Text = "ไม่พบรายการดังกล่าว";
			} else {
				lblCountPSN.Text = "พบ " + GL.StringFormatNumber(DT.Rows.Count.ToString(), 0) + " รายการ";
			}

		}

		protected void Pager_PageChanging(PageNavigation Sender)
		{
			Pager.TheRepeater = rptASS;
		}

		//------------ For Grouping -----------
		string LastSector = "";
		string LastDept = "";
		protected void rptASS_ItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
		{
			if (e.Item.ItemType != ListItemType.AlternatingItem & e.Item.ItemType != ListItemType.Item)
				return;

            Label lblSector = (Label)e.Item.FindControl("lblSector");
            Label lblDept = (Label)e.Item.FindControl("lblDept");
            Label lblPSNNo = (Label)e.Item.FindControl("lblPSNNo");
            Label lblPSNName = (Label)e.Item.FindControl("lblPSNName");
            Label lblPSNPos = (Label)e.Item.FindControl("lblPSNPos");
            Label lblPSNClass = (Label)e.Item.FindControl("lblPSNClass");
            Label lblPSNType = (Label)e.Item.FindControl("lblPSNType");
            HtmlAnchor lbl_KPI_Status = (HtmlAnchor)e.Item.FindControl("lbl_KPI_Status");
            HtmlAnchor lbl_KPI_Result = (HtmlAnchor)e.Item.FindControl("lbl_KPI_Result");
            HtmlAnchor lbl_COMP_Status = (HtmlAnchor)e.Item.FindControl("lbl_COMP_Status");
            HtmlAnchor lbl_COMP_Result = (HtmlAnchor)e.Item.FindControl("lbl_COMP_Result");
            Label lbl_Ass_Score = (Label)e.Item.FindControl("lbl_Ass_Score");
            Label lbl_Leave_Day = (Label)e.Item.FindControl("lbl_Leave_Day");
            Label lbl_Leave_Score = (Label)e.Item.FindControl("lbl_Leave_Score");
            Label lbl_Result = (Label)e.Item.FindControl("lbl_Result");

            HtmlTableRow tdSector = (HtmlTableRow)e.Item.FindControl("tdSector");
            HtmlTableRow tdDept = (HtmlTableRow)e.Item.FindControl("tdDept");

            DataRowView drv = (DataRowView)e.Item.DataItem;

			//---------- แยกหน่วยงาน---------
			if (drv["SECTOR_NAME"].ToString() != LastSector) {
				LastSector = drv["SECTOR_NAME"].ToString();
				lblSector.Text = LastSector;
				LastDept = drv["DEPT_NAME"].ToString();
				lblDept.Text = LastDept;
			//---------- แยกสมรรถนะ---------
			} else if (LastDept != drv["DEPT_NAME"].ToString()) {
				tdSector.Visible = false;
				LastDept = drv["DEPT_NAME"].ToString();
				lblDept.Text = LastDept;
			} else {
				tdSector.Visible = false;
				tdDept.Visible = false;
			}

			lblPSNNo.Text = drv["PSNL_NO"].ToString();
			lblPSNName.Text = drv["PSNL_Fullname"].ToString();
			lblPSNPos.Text = drv["POS_Name"].ToString();
			if (!GL.IsEqualNull(drv["PNPS_CLASS"])) {
				lblPSNClass.Text = GL.CINT (drv["PNPS_CLASS"]).ToString();
			}
			lblPSNType.Text =drv["WAGE_NAME"].ToString();


			if (!GL.IsEqualNull(drv["KPI_Status"]) && GL.CINT(drv["KPI_Status"]) == 5) {
				lbl_KPI_Status.InnerHtml = "<img src='images/check.png' title='ประเมินเสร็จสมบูรณ์'>";
			//check
			} else {
				lbl_KPI_Status.InnerHtml = "<img src='images/none.png' title='ยังไม่อนุมัติแบบประเมิน'>";
			}
            if (!GL.IsEqualNull(drv["KPI_Result"]) && GL.CINT(drv["KPI_Result"]) != 0)
            {
				lbl_KPI_Result.InnerHtml = GL.StringFormatNumber(drv["KPI_Result"]);
			} else {
				lbl_KPI_Result.InnerHtml = "-";
			}
            if (!GL.IsEqualNull(drv["COMP_Status"]) && GL.CINT(drv["COMP_Status"]) == 5)
            {
				lbl_COMP_Status.InnerHtml = "<img src='images/check.png' title='ประเมินเสร็จสมบูรณ์'>";
			} else {
				lbl_COMP_Status.InnerHtml = "<img src='images/none.png' title='ยังไม่อนุมัติแบบประเมิน'>";
			}
            if (!GL.IsEqualNull(drv["COMP_Result"]) && GL.CINT(drv["COMP_Result"]) != 0)
            {
				lbl_COMP_Result.InnerHtml = GL.StringFormatNumber(drv["COMP_Result"]);
			} else {
				lbl_COMP_Result.InnerHtml = "-";
			}

			double Total_Score = 0;
            if (!GL.IsEqualNull(drv["Total_Score"]) && GL.CINT(drv["Total_Score"]) != 0)
            {
				Total_Score = (double)drv["Total_Score"];
				lbl_Ass_Score.Text = GL.StringFormatNumber(Total_Score);
			} else {
				lbl_Ass_Score.Text = "-";
			}

			double DisScrore = 0;
			if ((double)drv["L_Day"] != 0) {
				DisScrore = (double)drv["L_Day"] * 0.5;
				lbl_Leave_Day.Text = GL.StringFormatNumber(drv["L_Day"], 2);
				lbl_Leave_Score.Text = GL.StringFormatNumber(-DisScrore, 2);

				lbl_Leave_Day.Font.Bold = true;
				lbl_Leave_Score.Font.Bold = true;
				lbl_Leave_Day.ForeColor = System.Drawing.Color.Red;
				lbl_Leave_Score.ForeColor = System.Drawing.Color.Red;
			} else {
				lbl_Leave_Day.Text = "-";
				lbl_Leave_Score.Text = "-";
			}

			if (Total_Score > 0) {
				lbl_Result.Text = GL.StringFormatNumber(Total_Score - DisScrore, 2);
				lbl_Result.Font.Bold = true;
				lbl_Result.ForeColor = System.Drawing.Color.Green;
			} else {
				lbl_Result.Text = "-";
			}


            lbl_KPI_Status.HRef = "Print/RPT_K_PNSAss.aspx?Mode=PDF&R_Year=" + drv["R_Year"].ToString() + "&R_Round=" + drv["R_Round"].ToString() + "&PSNL_NO=" + drv["PSNL_NO"].ToString();
            lbl_KPI_Result.HRef = "Print/RPT_K_PNSAss.aspx?Mode=PDF&R_Year=" + drv["R_Year"].ToString() + "&R_Round=" + drv["R_Round"].ToString() + "&PSNL_NO=" + drv["PSNL_NO"].ToString();
            lbl_COMP_Status.HRef = "Print/RPT_C_PNSAss.aspx?Mode=PDF&R_Year=" + drv["R_Year"].ToString() + "&R_Round=" + drv["R_Round"].ToString() + "&PSNL_NO=" + drv["PSNL_NO"].ToString();
            lbl_COMP_Result.HRef = "Print/RPT_C_PNSAss.aspx?Mode=PDF&R_Year=" + drv["R_Year"].ToString() + "&R_Round=" + drv["R_Round"].ToString() + "&PSNL_NO=" + drv["PSNL_NO"].ToString();
		}
		public RPT_A_Round_Result()
		{
			Load += Page_Load;
		}

	}
}
